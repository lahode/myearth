<?php

  // Gestion des flux RSS
  class RSSManager {

    var $file;
    var $title;
    var $link;
    var $owner;
    var $msgHandler;

    // Constructeur
    function __construct ($file, $title, $link, $owner, $msgHandler) {
      $this->file = $file;
      $this->title = $title;
      $this->link = $link;
      $this->owner = $owner;
      $this->msgHandler = $msgHandler;
    }


    // Crée un fichier de flux RSS
    function create () {
      if (!file_exists($this->file)){
        $string = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n<rss version=\"2.0\">\n  <channel>\n    <title>".$this->title."</title>\n    <link>http://www.myearth.ch</link>\n    <description>".$this->msgHandler->get('theNewsOf').$this->owner."</description>\n  </channel>\n</rss>";
        $handle = fopen ($this->file.'.xml', 'w');
        fwrite($handle, $string);
        fclose($handle);
        @chmod ($this->file, 0777);
      }
    }//create


    // Supprime un flux RSS existant
    function delete ($id, $type) {
      if ($id<>'') {
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();

        // Si un ancien fichier d'archive n'existe pas au préalable, crée un nouveau fichier d'archive avec les en-têtes
        if (file_exists($this->file.'.xml')) {
          @$domAlbum->load($this->file.'.xml');
          // Recherche les elements categories du fichier
          $xpath = new DOMXpath($domAlbum);
          $nodelist = $xpath->query("/rss[@version='2.0']/channel/item[guid='".$type."#".$id."']");
          $node = $nodelist->item(0);
          if ($node) {
            $node->parentNode->removeChild($node);
          }
          // Sauvegarde le nouveau fichier
          @$domAlbum->save($this->file.'.xml');
        }
      }
    }//delete


    // Insert un nouveau flux RSS
    function insert ($id, $type, $subject, $text) {
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      if (!file_exists($this->file.'.xml')) $this->create();
      if (file_exists($this->file.'.xml')) {
        $domRSS = new DOMDocument();
        $RSS_node = $domRSS->createElement('item');
        $RSS_node->appendChild ($domRSS->createElement('title', convertToUTF8(stripslashes($this->msgHandler->get($type).$subject))));
        $RSS_node->appendChild ($domRSS->createElement('link', 'http://'.$this->link));
        $guid_node = $RSS_node->appendChild ($domRSS->createElement('guid', $type.'#'.$id));
        $guid_node->setAttribute ('isPermaLink', 'false');
        $RSS_node->appendChild ($domRSS->createElement('description', convertToUTF8(stripslashes(shorten ($text, 150)))));
        $RSS_node->appendChild ($domRSS->createElement('pubDate', date(DATE_RFC822)));
        $domRSS->appendChild($RSS_node);
        @$domAlbum->load($this->file.'.xml');
        //if (!$domAlbum->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

        // Recherche les elements invoice de l'ancien fichier
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/rss[@version='2.0']/channel");
        if ($resultVoyage->item(0)) {
          $newnode = $domAlbum->importNode($domRSS->documentElement, true);
          $resultVoyage->item(0)->appendChild($newnode);
        }
        // Sauvegarde le nouveau fichier d'archive
        @$domAlbum->save($this->file.'.xml');
      }
    }//insertRSS
  
  }//class RSSManager

?>
