<?php

  // Gestion des différent type de média pour la construction des formulaire, reception et sauvegarde du media
  class Media {
    var $name;
    var $msgHandler;
    var $fileManager;
    var $albumManager;
    var $quota_path;
    var $path;
    var $displayAlbum;
    var $maximum;
    var $defaultMapSizeX = 460;
    var $defaultMapSizeY = 234;


    // Constructeur
    function __construct ($name, $msgHandler, $fileManager, $albumManager, $quota_path, $path, $displayAlbum, $maximum=0) {
      $this->name = $name;
      $this->msgHandler = $msgHandler;
      $this->fileManager = $fileManager;
      $this->albumManager = $albumManager;
      $this->quota_path = $quota_path;
      $this->path = $path;
      $this->displayAlbum = $displayAlbum;
      $this->maximum = $maximum;
    }


    // Retourne la taille à laquelle la carte doit être sauvegardée
    function getMapSize ($srcName) {
      $returnedSize['x'] = 0;
      $returnedSize['y'] = 0;
      $returnedSize['quality'] = 80;
      if(!list($width_orig, $height_orig, $ext) = getimagesize($srcName)) {
        return false;
      }
      if ($width_orig/$this->defaultMapSizeX < $height_orig/$this->defaultMapSizeY && $width_orig < $this->defaultMapSizeX)
        $returnedSize['x'] = $this->defaultMapSizeX;
      elseif ($height_orig/$this->defaultMapSizeY < $width_orig/$this->defaultMapSizeX && $height_orig < $this->defaultMapSizeY)
        $returnedSize['y'] = $this->defaultMapSizeY;
      elseif ($width_orig/$this->defaultMapSizeX < $height_orig/$this->defaultMapSizeY)
        $returnedSize['quality'] = (50 / round ($height_orig/$this->defaultMapSizeY)) + 40;
      else
        $returnedSize['quality'] = (50 / round ($width_orig/$this->defaultMapSizeX)) + 40;
      return $returnedSize;
    }//getMapSize


    // Récupère une image du formulaire et l'enregistre dans la session des fichiers temporaires
    function addImage ($description, $deleteOld=false, $tailleX=0, $tailleY=300) {
      global $_FILES;
      global $_POST;
      if ($this->fileManager && $this->albumManager && $this->quota_path<>'' && $this->path<>'') {
        $error = "";
        if (isset ($_FILES['file'.$this->name]) && isset ($_FILES['file'.$this->name]['name']) && $_FILES['file'.$this->name]['name'] <> '') {
          if (getFreeSpace($this->albumManager->getQuota(), $this->quota_path)-$_FILES['file'.$this->name]['size'] > 0) {
            if ($deleteOld) {$old = $this->fileManager->getTempFiles ($this->name);$this->fileManager->deleteTempFiles ($this->name);}
            $resultAdd = $this->fileManager->addTempImage ($this->name, $_FILES['file'.$this->name], addslash($description), false, $tailleX, $tailleY);
            if (is_string ($resultAdd)) {$error = $resultAdd;if ($deleteOld) $this->fileManager->setTempFiles ($this->name, $old);}
          } else {
            $error = $this->msgHandler->getError('quotaExceeded');
          }
        } elseif ($_POST['album'.$this->name]<>'' && $this->displayAlbum) {
          if ($deleteOld) $this->fileManager->deleteTempFiles ($this->name);
          $this->fileManager->addImage ($this->name, $this->path, $_POST['album'.$this->name], addslash($description));
        }
      }
      return array ($this->fileManager->getTempFiles ($this->name), $error);
    }//addImage


    // Récupère une carte du formulaire et l'enregistre dans la session des fichiers temporaires
    function addMap ($description, $deleteOld=false) {
      global $_FILES;
      global $_POST;
      if ($this->fileManager && $this->albumManager && $this->quota_path<>'' && $this->path<>'') {
        $error = "";
        if (isset ($_FILES['file'.$this->name]) && isset ($_FILES['file'.$this->name]['name']) && $_FILES['file'.$this->name]['name'] <> '') {
          if (getFreeSpace($this->albumManager->getQuota(), $this->quota_path)-$_FILES['file'.$this->name]['size'] > 0) {
            if ($deleteOld) {$old = $this->fileManager->getTempFiles ($this->name);$this->fileManager->deleteTempFiles ($this->name);}
            $returnedSize = $this->getMapSize ($_FILES['file'.$this->name]['tmp_name']);
            if (is_array ($returnedSize)) {
              $resultAdd = $this->fileManager->addTempImage ($this->name, $_FILES['file'.$this->name], addslash($description), false, $returnedSize['x'], $returnedSize['y'], $returnedSize['quality']);
            } else {
              $resultAdd = $this->msgHandler->getError ('pictureIncorrect');
            }
            if (is_string ($resultAdd)) {$error = $resultAdd;if ($deleteOld) $this->fileManager->setTempFiles ($this->name, $old);}
          } else {
            $error = $this->msgHandler->getError('quotaExceeded');
          }
        } elseif ($_POST['album'.$this->name]<>'' && $this->displayAlbum) {
          if ($deleteOld) $this->fileManager->deleteTempFiles ($this->name);
          $this->fileManager->addImage ($this->name, $this->path, $_POST['album'.$this->name], addslash($description));
        }
      }
      return array ($this->fileManager->getTempFiles ($this->name), $error);
    }//addMap


    // Récupère un son du formulaire et l'enregistre dans la session des fichiers temporaires
    function addSound ($deleteOld=false) {
      global $_FILES;
      global $_POST;
      if ($this->fileManager && $this->albumManager && $this->quota_path<>'' && $this->path<>'') {
        $error = "";
        if (isset ($_FILES['file'.$this->name]) && isset ($_FILES['file'.$this->name]['name']) && $_FILES['file'.$this->name]['name'] <> '') {
          if (getFreeSpace($this->albumManager->getQuota(), $this->quota_path)-$_FILES['file'.$this->name]['size'] > 0) {
            if ($deleteOld) {$old = $this->fileManager->getTempFiles ($this->name);$this->fileManager->deleteTempFiles ($this->name);}
            $resultAdd = $this->fileManager->addTempSound ($this->name, $_FILES['file'.$this->name]);
            if (is_string ($resultAdd)) {$error = $resultAdd;if ($deleteOld) $this->fileManager->setTempFiles ($this->name, $old);}
          } else {
            $error = $this->msgHandler->getError('quotaExceeded');
          }
        } elseif ($_POST['album'.$this->name]<>'' && $this->displayAlbum) {
          if ($deleteOld) $this->fileManager->deleteTempFiles ($this->name);
          $this->fileManager->addFile ($this->name, $this->path, $_POST['album'.$this->name]);
        }
      }
      return array ($this->fileManager->getTempFiles ($this->name), $error);
    }//addSound


    // Genère le code HTML pour la gestion des fichiers
    function output ($type, $showPicture='', $uploaded = false) {
      $nbImage = count ($this->fileManager->getTempFiles ($this->name) ?? []);
      $out = '<input type="hidden" name="album'.$this->name.'" id="album'.$this->name.'" />'."\n";
      if ($showPicture<>'' && $type<>'sound' && $type<>'video')
        $out.= '<img src="'.$this->path.$showPicture.'" name="showImage'.$this->name.'" id="showImage'.$this->name.'" height="22" border="0" class="middle" />'."\n";
      elseif ($showPicture<>'' && $type=='sound')
        $out.= '<img src="'.PICS_PATH.'music.jpg" name="showImage'.$this->name.'" id="showImage'.$this->name.'" height="22" border="0" class="middle" />'."\n";
      elseif ($uploaded)
        $out.= '<img src="'.PICS_PATH.'uploaded.gif" name="showImage'.$this->name.'" id="showImage'.$this->name.'" height="22" border="0" class="middle" />'."\n";
      else
        $out.= '<img src="'.PICS_PATH.'blank.gif" name="showImage'.$this->name.'" id="showImage'.$this->name.'" height="22" border="0" class="middle" />'."\n";
      if ($this->maximum==0 || $nbImage < $this->maximum)
        $out.= '<input type="file" name="file'.$this->name.'" id="file'.$this->name.'" onchange="document.getElementById(\'showImage'.$this->name.'\').src=\''.PICS_PATH.'uploaded.gif\';" />'."\n";
      else
        $out.= '<input type="file" name="file'.$this->name.'" id="file'.$this->name.'" disabled="disabled" onchange="document.getElementById(\'showImage'.$this->name.'\').src=\''.PICS_PATH.'uploaded.gif\';" />'."\n";
      if ($this->displayAlbum && ($this->maximum==0 || $nbImage < $this->maximum))
        if ($type=='sound')
        $out.= '<span style="cursor:pointer" title="'.$this->msgHandler->get('infoselectpics').'" onclick="window.open(\''.PAGE_FILEVIEWER.'?actions=selectSound&id=album'.$this->name.'&show=showImage'.$this->name.'&clean=file'.$this->name.'\', null, \'height=480,width=640,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes\');"><img src="'.PICS_PATH.'admin/download.png" border="0" width="24" height="24" class="middle" /></span>'."\n";
        elseif ($type=='video')
        $out.= '<span style="cursor:pointer" title="'.$this->msgHandler->get('infoselectpics').'" onclick="window.open(\''.PAGE_FILEVIEWER.'?actions=selectVideo&id=album'.$this->name.'&show=showImage'.$this->name.'&clean=file'.$this->name.'\', null, \'height=480,width=640,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes\');"><img src="'.PICS_PATH.'admin/download.png" border="0" width="24" height="24" class="middle" /></span>'."\n";
        else
        $out.= '<span style="cursor:pointer" title="'.$this->msgHandler->get('infoselectpics').'" onclick="window.open(\''.PAGE_FILEVIEWER.'?actions=selectPicture&id=album'.$this->name.'&show=showImage'.$this->name.'&clean=file'.$this->name.'&type='.$type.'\', null, \'height=480,width=640,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes\');"><img src="'.PICS_PATH.'admin/download.png" border="0" width="24" height="24" class="middle" /></span>'."\n";
      return $out;
    }//output

  }//class Media

?>
