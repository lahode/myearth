<?php

  // Gestion du chat en Ajax (menu Chat)
  class ChatMessage {

    var $file;

    // Constructeur
    function __construct ($file) {
      $this->file = $file;
    }


    // Vérifie si l'utilisateur fait partie du chat
    function isUser ($user) {
      // Ouvre le fichier XML
      $return = false;
      $domAlbum = new DOMDocument();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $nodelist = $xpath->query("/users/user[@name='".convertToUTF8 (stripslashes($user))."']");
        $node = $nodelist->item(0);
        if ($node) {
          $return = $node->parentNode->removeChild($node);
        }
      }
      return $return;
    }//isUser


    // Récupère toutes les utilisateurs du chat
    function getUsers () {
      $users = array();
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/users");

        if ($resultVoyage->item(0)) {
          $resultAllUsers = $xpath->query("user", $resultVoyage->item(0));
          if ($resultAllUsers && $resultAllUsers->length > 0) {
            foreach ($resultAllUsers as $keyUser => $user) {
              $users[$keyUser]['time'] = $user->getAttribute('time');
              $users[$keyUser]['name'] = convertFromUTF8 ($user->getAttribute('name'));
            }
          }
        }
      }
      return $users;
    }//getUsers


    // Récupère toutes les appels du chat
    function getCalls () {
      $calls = array();
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/calls");

        if ($resultVoyage->item(0)) {
          $resultAllCalls = $xpath->query("call", $resultVoyage->item(0));
          if ($resultAllCalls && $resultAllCalls->length > 0) {
            foreach ($resultAllCalls as $keyCall => $call) {
              $calls[$keyCall]['from'] = convertFromUTF8 ($call->getAttribute('from'));
              $calls[$keyCall]['to'] = convertFromUTF8 ($call->getAttribute('to'));
              $calls[$keyCall]['new'] = convertFromUTF8 ($call->getAttribute('new'));
              $calls[$keyCall]['time'] = $call->getAttribute('time');
            }
          }
        }
      }
      return $calls;
    }//getCalls


    // Insert un message
    function insertMessage ($user, $message, $maxlines=100) {
      if ($user<>'' && $message<>'') {
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();
        if (file_exists($this->file.'.xml')) {
          $domMessage = new DOMDocument();
          $msg_node = $domMessage->createElement('message', convertToUTF8(stripslashes($message)));
          $msg_node->setAttribute ('user', convertToUTF8(stripslashes($user)));
          $msg_node->setAttribute ('time', time());
        }
        $domMessage->appendChild($msg_node);
        @$domAlbum->load($this->file.'.xml');
        //if (!$domAlbum->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

        // Recherche les elements invoice de l'ancien fichier
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/messages");
        if ($resultVoyage->item(0)) {
          $resultAllMessages = $xpath->query("message", $resultVoyage->item(0));
          if ($resultAllMessages && $resultAllMessages->length > $maxlines) {
            $nodeToDelete = $resultAllMessages->item(0);
            if ($nodeToDelete) {
              $nodeToDelete->parentNode->removeChild($nodeToDelete);
            }
          }
          $newnode = $domAlbum->importNode($domMessage->documentElement, true);
          $resultVoyage->item(0)->appendChild($newnode);
        }
        // Sauvegarde le nouveau fichier d'archive
        @$domAlbum->save($this->file.'.xml');
      }
    }//insertMessage


    // Insert un appel
    function editCall ($from, $to, $newConnection=0) {
      if ($from<>'' && $to<>'') {
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();
        if (file_exists($this->file.'.xml')) {
          $domCall = new DOMDocument();
          $call_node = $domCall->createElement('call');
          $call_node->setAttribute ('from', convertToUTF8(stripslashes($from)));
          $call_node->setAttribute ('to', convertToUTF8(stripslashes($to)));
          $call_node->setAttribute ('new', convertToUTF8(stripslashes($newConnection)));
          $call_node->setAttribute ('time', time());
        }
        $domCall->appendChild($call_node);
        @$domAlbum->load($this->file.'.xml');
        //if (!$domAlbum->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

        // Recherche les elements invoice de l'ancien fichier
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/calls");
          if ($resultVoyage->item(0)) {
            $nodelist = $xpath->query("call", $resultVoyage->item(0));
            if ($nodelist->item(0)) {
              // Remplace les anciens noeuds existant par les nouveau ou rajoute les nouveau si inexistant
              $nodeModified = false;
              foreach ($nodelist as $keyNode => $node) {
                if (convertFromUTF8 ($node->getAttribute('from')) == convertFromUTF8 ($call_node->getAttribute('from')) &&
                    convertFromUTF8 ($node->getAttribute('to')) == convertFromUTF8 ($call_node->getAttribute('to'))) {
                  $oldnode = $nodelist->item($keyNode);
                  $newnode = $domAlbum->importNode($domCall->documentElement, true);
                  $oldnode->parentNode->replaceChild($newnode, $oldnode);
                  $nodeModified = true;
                }
              }
              if (!$nodeModified) {
                $oldnode = $nodelist->item(0);
                $newnode = $domAlbum->importNode($domCall->documentElement, true);
                $oldnode->parentNode->appendChild($newnode);
              }
            } else {
              $newnode = $domAlbum->importNode($domCall->documentElement, true);
              $resultVoyage->item(0)->appendChild($newnode);
            }
          }
          // Sauvegarde le nouveau fichier d'archive
          @$domAlbum->save($this->file.'.xml');
        }
    }//insertCall


    // Supprime un utilisateur
    function deleteUser ($user, $chatMessage=null, $message='') {
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();

      // Si un ancien fichier d'archive n'existe pas au préalable, crée un nouveau fichier d'archive avec les en-têtes
      if (file_exists($this->file.'.xml')) {
        @$domAlbum->load($this->file.'.xml');
        // Recherche les elements categories du fichier
        $xpath = new DOMXpath($domAlbum);
        $nodelist = $xpath->query("/users/user[@name='".convertToUTF8(stripslashes($user))."']");
        $node = $nodelist->item(0);
        if ($node) {
          $node->parentNode->removeChild($node);
          if ($chatMessage && $message<>'') {
            $chatMessage->insertMessage($user.' '.$message, ' ');
          }
        }
        // Sauvegarde le nouveau fichier
        @$domAlbum->save($this->file.'.xml');
      }
    }//deleteUser


    // Nettoie les anciens utilisateurs
    function cleanUsers ($chatMessage=null, $message='') {
      $users = $this->getUsers();
      if (count($users) > 0) {
        foreach ($users as $user) {
          if (intval($user['time'])+25 < time()) $this->deleteUser($user['name'], $chatMessage, $message);
        }
      }
    }//cleanUsers


    // Supprime un appel
    function deleteCall ($from, $to) {
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();

      // Si un ancien fichier d'archive n'existe pas au préalable, crée un nouveau fichier d'archive avec les en-têtes
      if (file_exists($this->file.'.xml')) {
        @$domAlbum->load($this->file.'.xml');
        // Recherche les elements categories du fichier
        $xpath = new DOMXpath($domAlbum);
        $nodelist = $xpath->query("/calls/call[@from='".convertToUTF8(stripslashes($from))."' and @to='".convertToUTF8(stripslashes($to))."']");
        $node = $nodelist->item(0);
        if ($node) {
          $node->parentNode->removeChild($node);
        }
        // Sauvegarde le nouveau fichier
        @$domAlbum->save($this->file.'.xml');
      }
    }//deleteCall


    // Nettoie les anciens appels
    function cleanCalls () {
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();

      // Si un ancien fichier d'archive n'existe pas au préalable, crée un nouveau fichier d'archive avec les en-têtes
      if (file_exists($this->file.'.xml')) {
        @$domAlbum->load($this->file.'.xml');
        // Recherche les elements categories du fichier
        $xpath = new DOMXpath($domAlbum);
        $nodelist = $xpath->query("/calls/call[@time<='".(time()-15)."']");
        $node = $nodelist->item(0);
        if ($node) {
          $node->parentNode->removeChild($node);
        }
        // Sauvegarde le nouveau fichier
        @$domAlbum->save($this->file.'.xml');
      }
    }//cleanCalls


    // Edite un utilisateur du chat
    function editUser ($user) {
      if ($user<>'') {
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();
        if (file_exists($this->file.'.xml')) {
          $domUser = new DOMDocument();
          $user_node = $domUser->createElement('user');
          $user_node->setAttribute ('name', convertToUTF8(stripslashes($user)));
          $user_node->setAttribute ('time', time());
        }
        $domUser->appendChild($user_node);
        @$domAlbum->load($this->file.'.xml');
        //if (!$domAlbum->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

          // Recherche les elements invoice de l'ancien fichier
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/users");
          if ($resultVoyage->item(0)) {
            $nodelist = $xpath->query("user", $resultVoyage->item(0));
            if ($nodelist->item(0)) {
              // Remplace les anciens noeuds existant par les nouveau ou rajoute les nouveau si inexistant
              $nodeModified = false;
              foreach ($nodelist as $keyNode => $node) {
                if (convertFromUTF8 ($node->getAttribute('name')) == convertFromUTF8 ($user_node->getAttribute('name'))) {
                  $oldnode = $nodelist->item($keyNode);
                  $newnode = $domAlbum->importNode($domUser->documentElement, true);
                  $oldnode->parentNode->replaceChild($newnode, $oldnode);
                  $nodeModified = true;
                }
              }
              if (!$nodeModified) {
                $oldnode = $nodelist->item(0);
                $newnode = $domAlbum->importNode($domUser->documentElement, true);
                $oldnode->parentNode->appendChild($newnode);
              }
            } else {
              $newnode = $domAlbum->importNode($domUser->documentElement, true);
              $resultVoyage->item(0)->appendChild($newnode);
            }
          }
          // Sauvegarde le nouveau fichier d'archive
          @$domAlbum->save($this->file.'.xml');
        }
    }//editUser

    
    // Met à jour l'affichage de la date, lors de la connexion au chat
    function updateDate($filename) {
      if (file_exists($filename)) {
        $handle = fopen($filename, "r");
        $content = fread ($handle, filesize ($filename));
        fclose($handle);
      } else {
        $content = '';
      }
      if ($content =='' || date("Ymd", $content)<>date("Ymd")) {
        $handle = fopen($filename, "w");
        $content = fwrite ($handle, time());
        fclose($handle);
        @chmod ($filename, 0777);
        $this->insertMessage('#time#', '-----------------'.date ('d-m-Y', time()).'-----------------', MAXLINESCHAT);
      }
    }//updateDate

  }//class ChatMessage

?>
