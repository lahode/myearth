<?php

  // Gestion des textures
  class SkinManager {

    var $file;
    var $name;
    var $type;

    // Constructeur
    function __construct ($path, $file) {
      $this->path = $path;
      $this->file = $file;
    }


    // Retourne les textures personalisées
    function getSkins () {
      $tabSkins = null;
      $content = @file($this->path.$this->file);
      if ($content) {
        foreach ($content as $keyContent => $itemContent) {
          $tab = null;
          $tab = explode ('=', $itemContent);
          if (count ($tab) > 1) {
            $name = substr($tab[1], 0, strlen($tab[1])-2);
            $tabSkins[$name] = new Skin ($this->path.$tab[0].'/', '', '');
          }
        }
      }
      return $tabSkins;
    }//getSkins


    // Retourne le nom d'une texture
    function getSkin ($name) {
      $result = '';
      $content = @file($this->path.$this->file);
      if ($content) {
        foreach ($content as $keyContent => $itemContent) {
          $tab = null;
          $tab = explode ('=', $itemContent);
          if ($name<>'' && $name==substr($tab[1], 0, strlen($tab[1])-2)) $result = $tab[0];
        }
      }
      return $result;
    }//getSkin


    // Retourne les textures par défaut
    function getDefaultSkins () {
      return array ('Explorator' => new Skin ('../lib/skins/explorator/', 'logout.gif', 'admin.gif'),
                    'Space' => new Skin ('../lib/skins/space/', '', ''),
                    'Design' => new Skin ('../lib/skins/design/', '', ''));
    }//getDefaultSkins


    // Retourne toutes les textures
    function getAllSkins () {
      if ($this->getSkins()) return $this->getSkins()+$this->getDefaultSkins();
      else return $this->getDefaultSkins();
    }//getAllSkins



    // Ajoute une texture
    function addSkin ($name) {
      if ($name<>'') {
        do {
          $id = substr (md5(uniqid()), 0, 8);
        } while (is_dir ($this->path.$id));
        mkdir ($this->path.$id);
        @chmod ($this->path.$id, 0777);
        $handle = fopen($this->path.$this->file, "a+");
        $contents = fwrite($handle, $id.'='.$name."\r\n");
        fclose($handle);
        @chmod ($this->path.$this->file, 0777);
      }
      return $this->path.$id.'/';
    }//addSkin


    // Supprime une texture
    function deleteSkin ($name) {
      if ($name<>'') {
        $path = $this->getSkin($name);
        rmdirtree ($this->path.$path);
        $content = @file($this->path.$this->file);
        if ($content && $path) {
          $newContent = "";
          foreach ($content as $keyContent => $itemContent) {
            if ($itemContent<>$path.'='.$name."\r\n") {
              $newContent.=$itemContent;
            }
          }
          $handle = fopen($this->path.$this->file, "w");
          $contents = fwrite($handle, $newContent);
          fclose($handle);
          @chmod ($this->path.$this->file, 0777);
        }
      }
    }//deleteSkin

  }//class SkinManager

?>
