<?php

  // Gestion des messages standard avec flux RSS
  class MessageManager {

    var $file;
    var $name;
    var $type;
    var $rssManager;
    var $rssMessage;

    // Constructeur
    function __construct ($file, $name, $type, $rssManager=null, $rssMessage="") {
      $this->file = $file;
      $this->name = $name;
      $this->type = $type;
      $this->rssManager = $rssManager;
      $this->rssMessage = $rssMessage;
    }

    
    // Retourne un id pour le message
    function getNewID () {
      do {
        $id = genID();
      } while ($this->getMessage($id));
      return $id;
    }//getNewID


    // Récupère le nombre de message
    function count ($admin=false) {
      // Ouvre le fichier XML
      $result = array();
      $domAlbum = new DOMDocument();
      $resultAllMsg = array();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $result = $xpath->query("/".$this->type."[@name='".$this->name."']/messages/message");
        if ($result) {
          return $result->length;
        }
        else
          return 0;
      } else
        return 0;
    }//count


    // Récupère un message d'après son identifiant
    function getMessage ($id) {
      // Ouvre le fichier XML
      $result = null;
      $domAlbum = new DOMDocument();
      $resultAllMsg = array();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/".$this->type."[@name='".$this->name."']/messages");

        if ($resultVoyage->item(0)) {
          $resultMsg = $xpath->query("message[@id='".$id."']", $resultVoyage->item(0));
          if ($resultMsg->item(0)) {
            $message = $resultMsg->item(0);
            $images=null;
            $resultImages = $xpath->query("image", $message);
            if ($resultImages && $resultImages->length > 0) {
              foreach ($resultImages as $keyImage => $image) {
                if ($image->getAttribute('name') <> '') {
                  $images[$keyImage] = new ImageObj (convertFromUTF8($image->getAttribute('name')), convertFromUTF8($image->getAttribute('description')));
                }
              }
            }
            $result = new MessageObj (
              convertFromUTF8($message->getAttribute('id')),
              convertFromUTF8($message->getAttribute('date')),
              convertFromUTF8($message->getAttribute('access')),
              convertFromUTF8($message->getAttribute('subject')),
              convertFromUTF8($message->getElementsByTagName('text')->item(0)->nodeValue),
              $images
            );
          }
        }
      }
      return $result;
    }//getMessage


    // Récupère toutes les images
    function getImages () {
      // Ouvre le fichier XML
      $images = array();
      $domAlbum = new DOMDocument();
      $resultAllMsg = array();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/".$this->type."[@name='".$this->name."']/messages");

        if ($resultVoyage->item(0)) {
          $resultAllMsg = $xpath->query("message", $resultVoyage->item(0));
          if ($resultAllMsg && $resultAllMsg->length > 0) {
            foreach ($resultAllMsg as $keyMsg => $message) {
              $resultImages = $xpath->query("image", $message);
              if ($resultImages && $resultImages->length > 0) {
                foreach ($resultImages as $keyImage => $image) {
                  if ($image->getAttribute('name') <> '') {
                    $images[] = $image->getAttribute('name');
                  }
                }
              }
            }
          }
        }
      }
      return $images;
    }//getImages


    // Récupère toutes les messages
    function getMessages ($first=-1, $length=-1, $restricted=false, $sortBy=false) {
      // Ouvre le fichier XML
      $messages = array();
      $result = array();
      $domAlbum = new DOMDocument();
      $resultAllMsg = array();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/".$this->type."[@name='".$this->name."']/messages");

        if ($resultVoyage->item(0)) {
          $resultAllMsg = $xpath->query("message", $resultVoyage->item(0));
          if ($resultAllMsg && $resultAllMsg->length > 0) {
            if ($first<>-1 && $length<>-1 && $this->getNbMessages($this->file, $this->name) < $first) return false;
            $sort = array();
            foreach ($resultAllMsg as $keyMsg => $message) {
              if ($restricted || $message->getAttribute('access')=='public') {
                $images=null;
                $resultImages = $xpath->query("image", $message);
                if ($resultImages && $resultImages->length > 0) {
                  foreach ($resultImages as $keyImage => $image) {
                    if ($image->getAttribute('name') <> '') {
                      $images[$keyImage] = new ImageObj (convertFromUTF8($image->getAttribute('name')), convertFromUTF8($image->getAttribute('description')));
                    }
                  }
                }
                $messages[$keyMsg] = new MessageObj (
                  convertFromUTF8($message->getAttribute('id')),
                  convertFromUTF8($message->getAttribute('date')),
                  convertFromUTF8($message->getAttribute('access')),
                  convertFromUTF8($message->getAttribute('subject')),
                  convertFromUTF8($message->getElementsByTagName('text')->item(0)->nodeValue),
                  $images
                );
                if ($sortBy) $sort[$keyMsg] = $message->getAttribute($sortBy);
              }
            }
            // Selectionne et trie les messages à afficher
            if (count ($messages) > 0) {
              if ($sortBy) array_multisort($sort, SORT_DESC, $messages);
              $i=0;
              foreach ($messages as $key => $item) {
                if (($first==-1 && $length==-1) || ($i >= $first &&  $i < $first+$length)) {
                  $result[$i] = $item;
                }
                $i++;
              }
            }
          }
        }
      }
      return $result;
    }//getMessages


    // Récupère le titre, l'accès et la date de tous les messages
    function getTitles ($first=-1, $length=-1, $restricted=false, $sortBy=false) {
      // Ouvre le fichier XML
      $messages = array();
      $result = array();
      $domAlbum = new DOMDocument();
      $resultAllMsg = array();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/".$this->type."[@name='".$this->name."']/messages");

        if ($resultVoyage->item(0)) {
          $resultAllMsg = $xpath->query("message", $resultVoyage->item(0));
          if ($resultAllMsg && $resultAllMsg->length > 0) {
            if ($first<>-1 && $length<>-1 && $this->getNbMessages($this->file, $this->name) < $first) return false;
            $sort = array();
            foreach ($resultAllMsg as $keyMsg => $message) {
              if ($restricted || $message->getAttribute('access')=='public') {
                $images=null;
                $resultImages = $xpath->query("image", $message);
                if ($resultImages && $resultImages->length > 0) {
                  foreach ($resultImages as $keyImage => $image) {
                    if ($image->getAttribute('name') <> '') {
                      $images[$keyImage] = new ImageObj (convertFromUTF8($image->getAttribute('name')), convertFromUTF8($image->getAttribute('description')));
                    }
                  }
                }
                $messages[$keyMsg] = new MessageObj (
                  convertFromUTF8($message->getAttribute('id')),
                  convertFromUTF8($message->getAttribute('date')),
                  convertFromUTF8($message->getAttribute('access')),
                  convertFromUTF8($message->getAttribute('subject')),
                  '',
                  null
                );
                if ($sortBy) $sort[$keyMsg] = $message->getAttribute($sortBy);
              }
            }
            // Selectionne et trie les messages à afficher
            if (count ($messages) > 0) {
              if ($sortBy) array_multisort($sort, SORT_DESC, $messages);
              $i=0;
              foreach ($messages as $key => $item) {
                if (($first==-1 && $length==-1) || ($i >= $first &&  $i < $first+$length)) {
                  $result[$i] = $item;
                }
                $i++;
              }
            }
          }
        }
      }
      return $result;
    }//getTitles


    // Récupère toutes les messages
    function getNbMessages ($restricted=false) {
      $i=0;
      $resultAllMsg = array();
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/".$this->type."[@name='".$this->name."']/messages");

        if ($resultVoyage->item(0)) {
          $resultAllMsg = $xpath->query("message", $resultVoyage->item(0));
          if ($resultAllMsg && $resultAllMsg->length > 0) {
            foreach ($resultAllMsg as $keyMsg => $message) {
              if ($restricted || $resultAllMsg->item($keyMsg)->getAttribute('access')=='public')
              $i++;
            }
          }
        }
      }
      return $i;
    }//getNbMessages


    // Supprime un message
    function deleteMessage ($id) {
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();

      // Si un ancien fichier d'archive n'existe pas au pr�alable, cr�e un nouveau fichier d'archive avec les en-t�tes
      if (file_exists($this->file.'.xml')) {
        @$domAlbum->load($this->file.'.xml');
        // Recherche les elements categories du fichier
        $xpath = new DOMXpath($domAlbum);
        $nodelist = $xpath->query("/".$this->type."[@name='".$this->name."']/messages/message[@id='".$id."']");
        $node = $nodelist->item(0);
        if ($node) {
          $node->parentNode->removeChild($node);
        }
        // Sauvegarde le nouveau fichier
        @$domAlbum->save($this->file.'.xml');
        if ($this->rssManager) $this->rssManager->delete ($id, $this->rssMessage);
      }
    }//deleteMessage


    // Edite un message
    function editMessage ($message) {
      if ($message) {
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();
        if (file_exists($this->file.'.xml')) {
          $domMessage = new DOMDocument();
          $message_node = $domMessage->createElement('message');
          $message_node->setAttribute ('id', convertToUTF8($message->id));
          $message_node->setAttribute ('date', convertToUTF8($message->date));
          $message_node->setAttribute ('access', convertToUTF8($message->access));
          $message_node->setAttribute ('subject', convertToUTF8($message->subject));
          $message_node->appendChild($domMessage->createElement('text', convertToUTF8($message->text)));
          if (count ($message->images) > 0) {
            foreach ($message->images as $image) {
              $image_node = $message_node->appendChild($domMessage->createElement('image'));
              $image_node->setAttribute ('name', convertToUTF8($image->name));
              $image_node->setAttribute ('description', convertToUTF8($image->description));
            }
          }
          $domMessage->appendChild($message_node);
          @$domAlbum->load($this->file.'.xml');
          //if (!$domAlbum->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

          // Recherche les elements invoice de l'ancien fichier
          $xpath = new DOMXpath($domAlbum);
          $resultVoyage = $xpath->query("/".$this->type."[@name='".$this->name."']/messages");
          if ($resultVoyage->item(0)) {
            $nodelist = $xpath->query("message", $resultVoyage->item(0));
            if ($nodelist->item(0)) {
              // Remplace les anciens noeuds existant par les nouveau ou rajoute les nouveau si inexistant
              $nodeModified = false;
              foreach ($nodelist as $keyNode => $node) {
                if (convertFromUTF8 ($node->getAttribute('id')) == convertFromUTF8 ($message_node->getAttribute('id'))) {
                  $oldnode = $nodelist->item($keyNode);
                  $domMessage->getElementsByTagName('message')->item(0)->setAttribute('date', $node->getAttribute('date'));
                  $newnode = $domAlbum->importNode($domMessage->documentElement, true);
                  $oldnode->parentNode->replaceChild($newnode, $oldnode);
                  $nodeModified = true;
                  if ($this->rssManager) {
                    $this->rssManager->delete ($message->id, $this->rssMessage);
                    if ($message->access=='public') $this->rssManager->insert ($message->id, $this->rssMessage, '', $message->text);
                  }
                }
              }
              if (!$nodeModified) {
                $oldnode = $nodelist->item(0);
                $newnode = $domAlbum->importNode($domMessage->documentElement, true);
                $oldnode->parentNode->appendChild($newnode);
                if ($this->rssManager) {
                  $this->rssManager->delete ($message->id, $this->rssMessage);
                  if ($message->access=='public') $this->rssManager->insert ($message->id, $this->rssMessage, '', $message->text);
                }
              }
            } else {
              $newnode = $domAlbum->importNode($domMessage->documentElement, true);
              $resultVoyage->item(0)->appendChild($newnode);
              if ($this->rssManager) $this->rssManager->insert ($message->id, $this->rssMessage, '', $message->text);
            }
          }
          // Sauvegarde le nouveau fichier d'archive
          @$domAlbum->save($this->file.'.xml');
        }
      }
    }//editMessage

  }//class MessageManager

?>
