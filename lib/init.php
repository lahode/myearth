<?php
  include_once ('../lib/GlobalClasses.php');
  include_once ('../lib/Language.php');
  include_once ('../lib/functions.php');
  include_once ('../lib/declare.php');
  include_once ('../lib/DatabaseQuery.php');
  include_once ('../lib/RSSManager.php');
  include_once ('../lib/MediaManager.php');
  include_once ('../lib/AlbumManager.php');
  include_once ('../lib/RouteManager.php');
  include_once ('../lib/MessageManager.php');
  include_once ('../lib/NewsManager.php');
  include_once ('../lib/ContactsManager.php');
  include_once ('../lib/SkinManager.php');
  include_once ('../lib/EmailManager.php');
  include_once ('../lib/CostsManager.php');
  include_once ('../lib/ForumManager.php');
  include_once ('../lib/TaskManager.php');
  include_once ('../lib/MailReceiver.php');
  include_once ('../lib/MailSender.php');
  include_once ('../lib/FileManager.php');
  include_once ('../lib/RankManager.php');
  include_once ('../lib/Media.php');
  include_once ('Header.php');


  // Création de la session
  initSession (SESSION_NAME);


  // Récupère la liste des pays visités
  function getCountriesList ($dbQuery, $categories, $routeManager, $nbCountry) {
    $countriesList = array();
    $lastCountry = null;
    $i = 0;
    $k = 0;
    $totalDest = 0;
    if (count ($categories) > 0) {
      $dbQuery->connect();
      if ($nbCountry == -1) $nbCountry = count ($categories);
      foreach ($categories as $keyCat => $itemCat) {
        if (count ($categories)-$i <= $nbCountry && $routeManager->countDest($itemCat->id) > 0) {
          $countryObj = new Country('', 0, '', '');
          $countryObj->sound = $itemCat->sound;
          $countryObj->name = $dbQuery->getCountryName($itemCat->country);
          $countryObj->cat = $itemCat->id;
          $countryObj->nbdest = $routeManager->countDest($itemCat->id);
          $countriesList[$keyCat] = $countryObj;
          $totalDest+= $countryObj->nbdest;
        }
        $i++;
      }
      $lastDestinations = $routeManager->getLastXDestination (4);
      if ($lastDestinations) {
        foreach ($lastDestinations as $lastDestination) {
          if ($lastDestination->cityName=='') {
            $tempCat = $routeManager->getCategoryBy ('id', $lastDestination->category);
            $city = $dbQuery->getCityName($tempCat->country, $lastDestination->city);
          } else {
            $city = $lastDestination->cityName;
          }
          $k--;
          $countriesList[] = new Country($lastDestination->id, $k, $city, $countryObj->sound);
        }
      }
      $dbQuery->disconnect();
      $countriesList[] = new Country('', $totalDest, '', '');
    }
    return $countriesList;
  }//getCountriesList


  // Formate une date
  function showdate ($date) {
    if (!$date || !ctype_digit($date)) return $date;
    switch ($_SESSION['dateFormat']) {
      case 'Y-m-d' : {$resultDate = date ('Y', $date).' '.$_SESSION['tabMonth'][date ('m', $date)].' '.date ('d', $date);break;}
      case 'm-d-Y' : {$_SESSION['tabMonth'][date ('m', $date)].' '.$resultDate = date ('d', $date).' '.date ('Y', $date);break;}
      case 'd-m-Y' : {$resultDate = date ('d', $date).' '.$_SESSION['tabMonth'][date ('m', $date)].' '.date ('Y', $date);break;}
      default :      {$dayValue=0;$monthValue=1;$yearValue=2;break;}
    }
    return $resultDate;
  }//fdate


  // Formate une date
  function fdate ($date) {
    if (!$date || !ctype_digit($date)) return $date;
    return date ($_SESSION['dateFormat'], $date);
  }//fdate


  // Déformate une date
  function unfdate ($date) {
    if (!$date) return $date;
    $dateTab = explode ('-', $date);
    if (count ($dateTab) == 3) {
      switch ($_SESSION['dateFormat']) {
        case 'Y-m-d' : {$dayValue=2;$monthValue=1;$yearValue=0;break;}
        case 'm-d-Y' : {$dayValue=1;$monthValue=0;$yearValue=2;break;}
        case 'd-m-Y' : {$dayValue=0;$monthValue=1;$yearValue=2;break;}
        default :      {$dayValue=0;$monthValue=1;$yearValue=2;break;}
      }
      return mktime (0, 0, 0, $dateTab[$monthValue], $dateTab[$dayValue], $dateTab[$yearValue]);
    } else
      return false;
  }//unfdate


  // Vérification de la session et initialisation des données principales
  if (!isset ($_SESSION['login']) || !isset ($_SESSION['albumName']) || $_SESSION['login']=='' || $_SESSION['albumName']=='') {
    header ("Location:http://".WEB_PAGE);
    exit();
  }


  // Initialise la base de donn�es
  $dbQuery = new DatabaseQuery (BD_HOST, BD_USER, BD_PSW, BD_NAME);


  // Vérifie si l'utilisateur est administrateur
  $dbQuery->connect();
  $isSuperAdmin = $dbQuery->isSuperAdmin ($_SESSION['login']);
  $admin = $dbQuery->checkAdminAccess ($_SESSION['adminCode']);
  $dbQuery->disconnect();


  // Initialise les données initiales
  $albumName = $_SESSION['albumName'];                                           // Nom de l'album actuel


  // Initialise les données principales de l'album
  $albumFile = USERS_PATH.$albumName.'/xml/album';
  $albumManager = new AlbumManager ($albumFile, $albumName);


  // Initialisation les chemin d'accès
  $ftpTempPath=USERS_PATH.$albumName.'/ftptemp/';                                // Dossier contenant les fichiers temporaires r�ceptionn� par FTP
  $tempPath=USERS_PATH.$albumName.'/temp/';                                      // Dossier contenant les fichiers temporaires de l'utilisateur
  $attachedInboxPath=USERS_PATH.$albumName.'/attachedfiles/inbox/';              // Dossier contenant les pi�ces jointes des messages re�us
  $attachedOutboxPath=USERS_PATH.$albumName.'/attachedfiles/outbox/';            // Dossier contenant les pi�ces jointes des messages envoy�s
  $webImgPath=USERS_PATH.$albumName.'/images/library/';                          // Dossier contenant les images pour le site
  $newsImgPath=USERS_PATH.$albumName.'/images/news/';                            // Dossier contenant les images des news
  $forumImgPath = USERS_PATH.$albumName.'/images/forum/';                        // Dossier contenant les images du forum
  $notebookImgPath = $webImgPath;                                                // Dossier contenant les images du carnet
  $humourImgPath = $webImgPath;                                                  // Dossier contenant les images du b�tisier
  $soundPath=USERS_PATH.$albumName.'/sounds/';                                   // Dossier contenant les sons
  $videoPath=USERS_PATH.$albumName.'/videos/';                                   // Dossier contenant les videos
  $backgroundImgPath=USERS_PATH.$albumName.'/images/background/';                // Dossier contenant les images de fond d'�cran
  $mapImgPath=USERS_PATH.$albumName.'/images/maps/';                             // Dossier contenant les cartes
  $otherFilesPath = USERS_PATH.$albumName.'/otherfiles/';                        // Dossier contenant les autres fichiers
  $secureFilesPath = USERS_PATH.$albumName.'/securefiles/';                      // Dossier contenant les fichiers s�curis�s
  $quota_path = USERS_PATH.$albumName.'/';                                       // Chemin du quota utilisateur


  // Initialisation des variables secondaires
  $albumTitle = $albumManager->getTitle ();                                      // Titre de l'album actuel
  $property=$albumManager->getProperties ();                                     // R�cup�re toutes les propri�t� de l'album
  $links = $albumManager->getLinks();                                            // R�cup�re les liens
  $menuOption = $albumManager->getMenuOptions ();                                // R�cup�re les options du menu
  $dateFormat = $property->dateFormat;                                           // Initialise le format de la date
  if (isset ($_SESSION['languageChosen'])) $language = $_SESSION['languageChosen']; else $language = $property->language;  // Initialise le langue de l'album
  $msgHandler = new Language ($language);                                        // Initialise la gestion des messages
  $headerPath = USERS_PATH.$albumName.'/';                                       // Dossier contenant les CSS propre � l'album
  $sendNews = false;                                                             // Initialise l'option d'envoi des news
  $origImgPath = "";                                                             // Dossier contenant les images originales
  if ($property->saveOrigImg==1) $origImgPath=USERS_PATH.$albumName.'/images/originals/';
  $_SESSION['dateFormat'] = $dateFormat;                                         // Met en m�moire le format de la date
  $_SESSION['tabMonth'] = $msgHandler->getTable('month');                        // Met en m�moire le tableau des mois


  // Initialise les données principales de l'album
  $rssFile = '../rss/'.$albumName;
  $rssManager = new RSSManager ($rssFile, $albumManager->getTitle(), WEB_PAGE, $albumManager->getOwner(), $msgHandler);


  // Initialise les données principales de l'itinéraire
  $routeFile = USERS_PATH.$albumName.'/xml/route';
  $routeManager = new RouteManager ($routeFile, $albumName, $rssManager, 'RSSRoute');


  // Initialise les données principales du forum
  $forumFile = USERS_PATH.$albumName.'/xml/forum';
  $forumManager = new ForumManager ($forumFile, $albumName, $rssManager, 'RSSForum');


  // Initialise les données principales du forum
  $taskFile = USERS_PATH.$albumName.'/xml/agenda';
  $taskManager = new TaskManager ($taskFile, $albumName);


  // Initialise les données principales du bétisier
  $humourFile = USERS_PATH.$albumName.'/xml/humour';
  $humourManager = new MessageManager ($humourFile, $albumName, 'humour', $rssManager, 'RSSHumour');


  // Initialise les données principales du carnet
  $notebookFile = USERS_PATH.$albumName.'/xml/notebook';
  $notebookManager = new MessageManager ($notebookFile, $albumName, 'notebook', $rssManager, 'RSSNotebook');


  // Initialise les données principales des préparatifs
  $prepareFile = USERS_PATH.$albumName.'/xml/preparation';
  $prepareManager = new MessageManager ($prepareFile, $albumName, 'preparation');


  // Initialise les données principales des vidéos
  $videoFile = USERS_PATH.$albumName.'/xml/video';
  $videoManager = new MessageManager ($videoFile, $albumName, 'video', $rssManager, 'RSSVideo');


  // Initialise les données principales des news
  $newsFile = USERS_PATH.$albumName.'/xml/news';
  $newsManager = new MessageManager ($newsFile, $albumName, 'news', $rssManager, 'RSSNews');


  // Initialise les données principales de l'inscription aux news
  $registerNewsFile = USERS_PATH.$albumName.'/xml/newsregister';
  $registerNewsManager = new NewsManager ($registerNewsFile, $albumName);


  // Initialise les données principales des contacts
  $contactsFile = USERS_PATH.$albumName.'/xml/contacts';
  $contactsManager = new ContactsManager ($contactsFile, $albumName);


  // Initialise les données principales du budget
  $costsFile = USERS_PATH.$albumName.'/xml/budget';
  $costsManager = new CostsManager($costsFile, $albumName);


  // Initialise les données principales du classement
  $rankingFile = USERS_PATH.$albumName.'/xml/ranking';
  $rankManager = new RankManager($rankingFile, $albumName);


  // Initialise les données principales des textures
  $skinsFile = USERS_PATH.$albumName.'/skins/';
  $skinsManager = new SkinManager($skinsFile, 'skins.lst');
  $tableSkins = $skinsManager->getAllSkins();


  // Initialise les données principales de la gestion des emails
  $emailsFile = USERS_PATH.$albumName.'/xml/mails';
  if (!isset($_SESSION['mailbox'])) $_SESSION['mailbox'] = 'default';
  $emailsManager = new EmailManager ($emailsFile, $albumName, 'inbox', $_SESSION['mailbox']);


  // Initialise les données principales de la gestion des m�dias
  $mediaManager = new MediaManager ($msgHandler);


  // Initialise les l'envoi et la récupération des e-mails vers/depuis l'extérieur
  $mailSender = new MailSender ();


  // Initialise les données principales de la gestion des fichiers temporaires
  $tempExt='.tmp';                                                               // Extension des fichiers temporaires
  $fileManager = new FileManager ($dbQuery, $msgHandler, $mediaManager, $tempPath, $tempExt);


  // Gestion des message d'erreur
  $error = "";
  $alertToDisplay = "";
  if (isset ($_GET['error'])) {
    $errorsTab = explode ("|", $_GET['error']);
    if ($errorsTab && count ($errorsTab) > 0) {
      foreach ($errorsTab as $errTab) {
        if ($error<>'') $error.= ' ';
        if ($errTab[0]=='*')
          $error.= $msgHandler->getError(substr($errTab, 1));
        else
          $error.= $errTab;
      }
    }
  }


  // Gestion des message de confirmation
  $confirm = "";
  if (isset ($_GET['confirm'])) {
    $confirmsTab = explode ("|", $_GET['confirm']);
    if ($confirmsTab && count ($confirmsTab) > 0) {
      foreach ($confirmsTab as $conTab) {
        if ($confirm<>'') $confirm.= ' ';
        if ($conTab[0]=='*')
          $confirm.= $msgHandler->getConfirm(substr($conTab, 1));
        else
          $confirm.= $conTab;
      }
    }
  }


  // Récupère les tous les nouvelles news et les stockes
//  if (!isset($_SESSION['mails'])) {
    $newsToSave = null;
    $mailReceiver = new mailReceiver ($mediaManager, EMAIL_HOST, EMAIL_USER, EMAIL_PSW, $albumManager->getQuota(), $quota_path);
    $mailsReceived = $mailReceiver->getMails($albumManager->getEmails(), true, true, $newsImgPath, 60, null, true);
    if ($mailsReceived && count ($mailsReceived) > 0) {
      foreach ($mailsReceived as $mail) {
        $images = null;
        if (count ($mail->attachments) > 0) {
          foreach ($mail->attachments as $keyAttach => $attachment) {
            $images[$keyAttach] = new ImageObj ($attachment, '', '');
          }
        }
        $newsToSave = new MessageObj (genID(), $mail->date, 'public', $mail->subject, strip_tags($mail->body), $images);
        $newsManager->editMessage ($newsToSave);
      }
    }
    if ($newsToSave) {
      $registeredPersons = $registerNewsManager->getPersons();
      if ($registeredPersons) {
        $messageToSend = convertToNews ($smarty, $newsToSave, $language, $albumName, $newsImgPath, DNS_NAME, true);
        foreach ($registeredPersons as $person) {
          $newsSender = new MailSender ();
          if (isset ($person->id)) {
            $stringId = '?unregister='.$person->id.'&album=';
            $messageToSend[0]->text = strtr($messageToSend[0]->text, array('?unregister=******&album=' => $stringId));
          }
          $newsSender->model->addTo ($person->email, $person->name);
          $newsSender->model->addFROM (USER_EMAIL);
          $newsSender->model->addHTML ($messageToSend[0]->text);
          $newsSender->model->addSubject ($messageToSend[0]->subject);
          if (isset ($messageToSend[1])) $newsSender->addAttachments ($messageToSend[1], true);
          if (!$newsSender->sender->send()) $error = $msgHandler->getError ('newsSent');
        }
      }
//    }
    $_SESSION['mails'] = true;

    //Vérifie s'il y a des tâches en cours
    if ($admin) {
      $tasksAlert = $taskManager->getNbOfCurrentTasks();
      if ($tasksAlert && count ($tasksAlert) > 0) {
        foreach ($tasksAlert as $keyTaskAlert => $taskAlert) {
          $alertToDisplay .= $taskAlert.' '.$msgHandler->getTable('taskType', $keyTaskAlert).'<br />';
        }
      }
    }
  }


  // Récupère le nombre d'élément dans chaque options du menu Catégories
  if (!isset ($_SESSION['menuCount'])) {
    $menuCount = array();
    $menuCount['news'] = $newsManager->count();
    $countNotebook=0;
    if ($admin) {
      $countNotebook = $notebookManager->count();
    } else {
      $notebookMsg = ($notebookManager->getMessages(-1, -1, false));
      $countNotebook = count ($notebookMsg);
    }
    $menuCount['notebook'] = $countNotebook;
    $menuCount['forum'] = $forumManager->count();
    $countHumour=0;
    if ($admin) {
      $countHumour = $humourManager->count();
    } else {
      $humourMsg = ($humourManager->getMessages(-1, -1, false));
      $countHumour = count ($humourMsg);
    }
    $menuCount['humour'] = $countHumour;
    $prepareMsg = $prepareManager->getMessages();
    $countPrepare=0;
    if ($prepareMsg && count ($prepareMsg) > 0) {
      foreach ($prepareMsg as $msg) {
        if ($msg->text<>'') $countPrepare++;
      }
    }
    $menuCount['prep'] = $countPrepare;
    $menuCount['video'] = $videoManager->count();
    $_SESSION['menuCount'] = $menuCount;
  }


  // Récupère toutes les catégories de l'album
  $categories = $routeManager->getCategories ();
  if (count ($categories) > 0) $categories = sortObjBy ($categories, 'startingDate', SORT_ASC);


  // Récupère les derniers pays visités
  if (!isset ($_SESSION['countriesList'])) {
    $countriesList = getCountriesList($dbQuery, $categories, $routeManager, -1);
    $_SESSION['countriesList'] = $countriesList;
  }


  // Initialise les menus à afficher
  $header = new Header ($language, $smarty, $headerPath, $_SESSION['countriesList'], $links, $soundPath, $menuOption, $property, $_SESSION['menuCount'], $albumName, $tableSkins[$property->skin]);


  // Assignation de la fonction de formatage de date dans Smarty
  $smarty->registerPlugin('modifier', 'fdate', 'fdate');
  $smarty->registerPlugin('modifier', 'showdate', 'showdate');

?>
