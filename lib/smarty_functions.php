<?php
/*
* Smarty plugin
* -------------------------------------------------------------
* Fichier :  function.html_select.php
* Type :     function
* Nom :      html_select
* Rôle :     Affiche un menu déroulant d'après les paramètres en
*            entrée :
*            "tab" -> tableau simple
*            "obj" -> tableau d'objet
*            "name" -> nom attribué au <select>
*            "selected" -> tableau des valeurs sélectionnées
*            "value" -> champs du tableau d'objet à afficher (facultatif, prend la valeur de name si inexistant)
*            "id" -> identifiant du tableau d'objet, sert à comparer l'identifiant avec les valeurs sélectionnées
*                    (facultatif, prend la valeur de value si inexistant)
*            "url" -> redirection vers une url lors d'un onChange
* -------------------------------------------------------------
*/

function smarty_function_html_select($params) {

    // Récupère les valeurs obligatoires
    $obj = $params['obj'];
    $tab = $params['tab'];
    $name = $params['name'];
    if (isset($params['max'])) $max = $params['max']; else $max = 25;
    if (isset($params['size'])) $size = ' multiple="yes" size = "'.$params['size'].'"'; else $size = '';
    if (isset($params['style'])) $style = ' style = "'.$params['style'].'"'; else $style = '';
    if (isset($params['class'])) $class = ' class = "'.$params['class'].'"'; else $class = '';
    if (($tab && $obj) || (!$tab && !$obj) || !$name) return null;

    // Récupère les valeurs de sélections (facultatif)
    if (isset($params['selected']))
        if (is_array($params['selected']))
  	        $selected = $params['selected'];
        else
            $selected[] = $params['selected'];

    // Récupère les valeurs de gestion du tableau d'objet (facultatif)
    if (isset($params['value'])) $value = $params['value']; else $value = $name;
    if (isset($params['id'])) $id = $params['id']; else $id = -1;

    // Récupère les éventuelles actions javascript (facultatif)
    if ($params['url']<>'')
        $redirect = "onChange=\"document.forms[0].action='".$params['url']."';document.forms[0].submit();\"";

    if ($params['js']<>'')
        $js = " ".$params['js'];

    echo ('<select id="'.$name.'" name="'.$name.'" '.$redirect.$js.$size.$style.$class.'>');
    if ($params['default']<>'')
      echo ('<option value="0">'.$params['default'].'</option>');
    if ($obj) {
        foreach($obj as $keyObj => $itemObj) {
            $i=0;
            while ($selected[$i]=='' && $i<sizeof($selected)) {
                $i++;
            }
            $selectedValue = $selected[$i];
            if (isset($params['decode']) && $params['decode']=='utf8') $valueOpt = utf8_decode ($itemObj->$value); else $valueOpt = $itemObj->$value;
            if ($id==-1) {
              if ($selectedValue<>'' && $selectedValue==$keyObj)
                echo ('<option value="'.$keyObj.'" selected="selected">'.substr($valueOpt, 0, $max).'</option>');
              else
                echo ('<option value="'.$keyObj.'">'.substr($valueOpt, 0, $max).'</option>');
            } else {
              if ($selectedValue<>'' && $selectedValue==$itemObj->$id)
                echo ('<option value="'.$itemObj->$id.'" selected="selected">'.substr($valueOpt, 0, $max).'</option>');
              else
                echo ('<option value="'.$itemObj->$id.'">'.substr($valueOpt, 0, $max).'</option>');
            }
        }
    }
    else {
        foreach($tab as $keyTab => $itemTab) {
            $i=0;
            while ($selected[$i]=='' && $i<sizeof($selected)) {
                $i++;
            }
            $selectedValue = $selected[$i];
            if ($selectedValue<>'' && $selectedValue==$keyTab)
                echo ('<option value="'.$keyTab.'" selected="selected">'.substr($itemTab, 0, $max).'</option>');
            else
                echo ('<option value="'.$keyTab.'">'.substr($itemTab, 0, $max).'</option>');
        }
    }
    echo ('</select>');
}

  // Formate les heures soit en H soit HH d'après une date
  function smarty_getHour ($date, $show=false) {
    if ($date=='') $date = 0;
    $result = date ('H', $date);
    if (!$show) $result = $result+0;
    return $result;
  }//getHour


  // Retourne les heures format�es HH d'après une date
  function smarty_showHour ($date) {
    return getHour ($date, true);
  }//showHour


  // Formate les minutes soit en M soit MM d'après une date
  function smarty_getMinute ($date, $show=false) {
    if ($date=='') $date = 0;
    $result = date ('i', $date);
    if (!$show) $result = $result+0;
    return $result;
  }//getMinute

  // Formate les secondes soit en S soit SS d'après une date
  function smarty_getSecond ($date, $show=false) {
    if ($date=='') $date = 0;
    $result = date ('s', $date);
    if (!$show) $result = $result+0;
    return $result;
  }//getSecond

  // Retourne les secondes formatées SS d'après une date
  function smarty_showSecond ($date) {
    return getSecond ($date, true);
  }//showSecond

  // Retourne les minutes formatées MM d'après une date
  function smarty_showMinute ($date) {
    return getMinute ($date, true);
  }//showMinute

  // Formate un chiffre avec 2 décimale
  function smarty_fPrice($nombre) {
    return number_format($nombre, 2, '.', '');
  }//fPrice

  // Racourci une phrase
  function smarty_shorten ($text, $size=20) {
    $text = cutWords ($text);
    if (strlen ($text) > $size) {
      $returnText = substr($text, 0, $size);
      $returnText.='...';
    } else {
      $returnText = $text;
    }
    return $returnText;
  }//shorten

?>
