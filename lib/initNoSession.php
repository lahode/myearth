<?php
  include_once ('../lib/GlobalClasses.php');
  include_once ('../lib/Language.php');
  include_once ('../lib/functions.php');
  include_once ('../lib/declare.php');
  include_once ('../lib/DatabaseQuery.php');
  include_once ('../lib/MediaManager.php');
  include_once ('../lib/AlbumManager.php');
  include_once ('../lib/MessageManager.php');
  include_once ('../lib/NewsManager.php');
  include_once ('../lib/ContactsManager.php');
  include_once ('../lib/SkinManager.php');
  include_once ('../lib/EmailManager.php');
  include_once ('../lib/MailReceiver.php');
//  include_once ('../lib/CostsManager.php');
//  include_once ('../lib/ForumManager.php');
//  include_once ('../lib/FileManager.php');
  include_once ('Header.php');


  // Vérification de la session et initialisation des données principales
  if (!isset($_GET['album']) || $_GET['album']=='') {
    header ("Location:http://".WEB_PAGE);
    exit();
  }


  // Initialise la base de données
  $dbQuery = new DatabaseQuery (BD_HOST, BD_USER, BD_PSW, BD_NAME);


  // Initialise les données initiales
  $albumName = $_GET['album'];                                                   // Nom de l'album actuel
  $admin = false;


  // Initialise les données principales de l'album
  $albumFile = USERS_PATH.$albumName.'/xml/album';
  $albumManager = new AlbumManager ($albumFile, $albumName);


  // Initialise les données principales de l'inscription aux news
  $registerNewsFile = USERS_PATH.$albumName.'/xml/newsregister';
  $registerNewsManager = new NewsManager ($registerNewsFile, $albumName);


  // Initialise les données principales des contacts
  $contactsFile = USERS_PATH.$albumName.'/xml/contacts';
  $contactsManager = new ContactsManager ($contactsFile, $albumName);


  // Initialise les données principales des textures
  $skinsFile = USERS_PATH.$albumName.'/skins/';
  $skinsManager = new SkinManager($skinsFile, 'skins.lst');
  $tableSkins = $skinsManager->getAllSkins();


  // Initialise les données principales de la gestion des emails
  $emailsFile = USERS_PATH.$albumName.'/xml/mailbox';
  $emailsManager = new EmailManager ($emailsFile, $albumName, 'inbox', 'default');


  // Initialisation les chemin d'accès
  $ftpTempPath=USERS_PATH.$albumName.'/ftptemp/';                                // Dossier contenant les fichiers temporaires r�ceptionn� par FTP
  $tempPath=USERS_PATH.$albumName.'/temp/';                                      // Dossier contenant les fichiers temporaires de l'utilisateur
  $attachedInboxPath=USERS_PATH.$albumName.'/attachedfiles/inbox/';              // Dossier contenant les pi�ces jointes des messages re�us
  $attachedOutboxPath=USERS_PATH.$albumName.'/attachedfiles/outbox/';            // Dossier contenant les pi�ces jointes des messages envoy�s
  $webImgPath=USERS_PATH.$albumName.'/images/library/';                          // Dossier contenant les images pour le site
  $newsImgPath=USERS_PATH.$albumName.'/images/news/';                            // Dossier contenant les images des news
  $forumImgPath = USERS_PATH.$albumName.'/images/forum/';                        // Dossier contenant les images du forum
  $notebookImgPath = $webImgPath;                                                // Dossier contenant les images du carnet
  $humourImgPath = $webImgPath;                                                  // Dossier contenant les images du b�tisier
  $soundPath=USERS_PATH.$albumName.'/sounds/';                                   // Dossier contenant les sons
  $videoPath=USERS_PATH.$albumName.'/videos/';                                   // Dossier contenant les videos
  $backgroundImgPath=USERS_PATH.$albumName.'/images/background/';                // Dossier contenant les images de fond d'�cran
  $otherFilesPath = USERS_PATH.$albumName.'/otherfiles/';                        // Dossier contenant les autres fichiers
  $quota_path = USERS_PATH.$albumName.'/';                                       // Chemin du quota utilisateur


  // Initialisation des variables secondaires
  $albumTitle = $albumManager->getTitle ();                                      // Titre de l'album actuel
  $property=$albumManager->getProperties ();                                     // R�cup�re toutes les propri�t� de l'album
  $links = $albumManager->getLinks();                                            // R�cup�re les liens
  $menuOption = $albumManager->getMenuOptions ();                                // R�cup�re les options du menu
  $dateFormat = $property->dateFormat;                                           // Initialise le format de la date
  $language = $property->language;                                               // Initialise le langue de l'album
  $msgHandler = new Language ($language);                                        // Initialise la gestion des messages
  $headerPath = USERS_PATH.$albumName.'/';                                       // Dossier contenant les CSS propre � l'album


  // Initialise les données principales de la gestion des médias
  $mediaManager = new MediaManager ($msgHandler);


  // Gestion des message d'erreur
  $error = "";
  if (isset ($_GET['error'])) {
    $errorsTab = explode ("|", $_GET['error']);
    if ($errorsTab && count ($errorsTab) > 0) {
      foreach ($errorsTab as $errTab) {
        if ($error<>'') $error.= ' ';
        if ($errTab[0]=='*')
          $error.= $msgHandler->getError(substr($errTab, 1));
        else
          $error.= $errTab;
      }
    }
  }


  // Gestion des message de confirmation
  $confirm = "";
  if (isset ($_GET['confirm'])) {
    $confirmsTab = explode ("|", $_GET['confirm']);
    if ($confirmsTab && count ($confirmsTab) > 0) {
      foreach ($confirmsTab as $conTab) {
        if ($confirm<>'') $confirm.= ' ';
        if ($conTab[0]=='*')
          $confirm.= $msgHandler->getConfirm(substr($conTab, 1));
        else
          $confirm.= $conTab;
      }
    }
  }


  // Initialise les menus à afficher
  $header = new Header ($language, $smarty, $headerPath, null, null, '', null, $property, null, $albumName, $tableSkins[$property->skin]);

?>
