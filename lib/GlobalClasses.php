<?php

  // Classe contenant toutes les informations d'une pièce jointe
  class Attachment {
    var $id;
    var $name;
    var $content;

    // Constructeur de la classe Attachment
    function __construct ($id, $name, $content) {
      $this->id = $id;
      $this->name = $name;
      $this->content = $content;
    }
  }//class Attachment


  // Classe contenant toutes les informations d'une catégorie (pays)
  class Category {
    var $id;
    var $access;
    var $country;
    var $arrivalCity;
    var $departureCity;
    var $startingDate;
    var $budget_accommodation_planed;
    var $budget_food_planed;
    var $budget_transport_planed;
    var $budget_other_planed;
    var $budget_accommodation_spent;
    var $budget_food_spent;
    var $budget_transport_spent;
    var $budget_other_spent;
    var $rate;
    var $summary;
    var $sound;
    var $showmap;
    var $allowsametime;
    var $order;

    // Constructeur de la classe Category
    function __construct ($id, $access, $country, $arrivalCity, $departureCity, $startingDate, $budget_accommodation_planed,
                       $budget_food_planed, $budget_transport_planed, $budget_other_planed, $budget_accommodation_spent,
                       $budget_food_spent, $budget_transport_spent, $budget_other_spent, $rate, $summary, $sound, $map, 
                       $showmap, $allowsametime=0, $order=0) {
      $this->id = $id;
      $this->access = $access;
      $this->country = $country;
      $this->arrivalCity = $arrivalCity;
      $this->departureCity = $departureCity;
      $this->startingDate = $startingDate;
      $this->budget_accommodation_planed = $budget_accommodation_planed;
      $this->budget_food_planed = $budget_food_planed;
      $this->budget_transport_planed = $budget_transport_planed;
      $this->budget_other_planed = $budget_other_planed;
      $this->budget_accommodation_spent = $budget_accommodation_spent;
      $this->budget_food_spent = $budget_food_spent;
      $this->budget_transport_spent = $budget_transport_spent;
      $this->budget_other_spent = $budget_other_spent;
      $this->rate = $rate;
      $this->summary = $summary;
      $this->sound = $sound;
      $this->map = $map;
      $this->showmap = $showmap;
      $this->allowsametime = $allowsametime;
      $this->order = $order;
    }
  }//class Category


  // Classe contenant toutes les informations d'un contact
  class Contact {
    var $id;
    var $title;
    var $firstname;
    var $lastname;
    var $nickname;
    var $email;
    var $birthdate;
    var $address;
    var $zipcode;
    var $city;
    var $country;
    var $tel;
    var $fax;
    var $mobile;
    var $messenger;
    var $comments;
    var $date;

    // Constructeur de la classe Contact
    function __construct ($id, $title, $firstname, $lastname, $nickname, $email, $birthdate, $address, $zipcode, $city, $country, $tel, $fax, $mobile, $messenger, $comments, $date) {
      $this->id = $id;
      $this->title = $title;
      $this->firstname = $firstname;
      $this->lastname = $lastname;
      $this->nickname = $nickname;
      $this->email = $email;
      $this->birthdate = $birthdate;
      $this->address = $address;
      $this->zipcode = $zipcode;
      $this->city = $city;
      $this->country = $country;
      $this->tel = $tel;
      $this->fax = $fax;
      $this->mobile = $mobile;
      $this->messenger = $messenger;
      $this->comments = $comments;
      $this->date = $date;
    }
  }//class Contact


  // Classe contenant toutes les informations d'un coût
  class Costs {
    var $id;
    var $category;
    var $element;
    var $date;
    var $type;
    var $departureCity;
    var $arrivalCity;
    var $name;
    var $price;
    var $quality;
    var $address;
    var $description;
    var $duration;
    var $length;

    // Constructeur de la classe Costs
    function __construct ($id, $category, $element, $date, $type, $departureCity, $arrivalCity, $name, $price,
                       $quality, $address, $description, $duration, $length) {
      $this->id = $id;
      $this->category = $category;
      $this->element = $element;
      $this->date = $date;
      $this->type = $type;
      $this->departureCity = $departureCity;
      $this->arrivalCity = $arrivalCity;
      $this->name = $name;
      $this->price = $price;
      $this->quality = $quality;
      $this->address = $address;
      $this->description = $description;
      $this->duration = $duration;
      $this->length = $length;
    }
  }//class Costs


  // Classe contenant toutes les informations d'un pays (simplifié)
  class Country {
    var $cat;
    var $nbdest;
    var $name;
    var $sound;

    // Constructeur de la classe Country
    function __construct ($cat, $nbdest, $name, $sound) {
      $this->cat = $cat;
      $this->nbdest = $nbdest;
      $this->name = $name;
      $this->sound = $sound;
    }
  }//class Country


  // Classe contenant toutes les informations d'une date de destination
  class DateDestination {
    var $id;
    var $startingDate;
    var $nbDays;

    // Constructeur de la classe DateDestination
    function __construct ($identifiant, $date, $days) {
      $this->id = $identifiant;
      $this->startingDate = $date;
      $this->nbDays = $days;
    }
  }//class DateDestination


  // Classe contenant toutes les informations du point de départ de l'album
  class Departure {
    var $country;
    var $region;
    var $city;
    var $cityName;
    var $startingDate;
    var $endingDate;
    var $budget_accommodation_planed;
    var $budget_food_planed;
    var $budget_transport_planed;
    var $budget_other_planed;

    // Constructeur de la classe Departure
    function __construct ($country, $region, $city, $cityName, $startingDate, $endingDate, $budget_accommodation_planed,
                       $budget_food_planed, $budget_transport_planed, $budget_other_planed) {
      $this->country = $country;
      $this->region = $region;
      $this->city = $city;
      $this->cityName = $cityName;
      $this->startingDate = $startingDate;
      $this->endingDate = $endingDate;
      $this->budget_accommodation_planed = $budget_accommodation_planed;
      $this->budget_food_planed = $budget_food_planed;
      $this->budget_transport_planed = $budget_transport_planed;
      $this->budget_other_planed = $budget_other_planed;
    }
  }//class Departure


  // Objet contenant toutes les informations d'une destination
  class DestinationObj {
    var $id;
    var $category;
    var $region;
    var $city;
    var $cityName;
    var $startingDate;
    var $nbDays;
    var $arrivalBy;
    var $text;
    var $images;
    var $map;
    var $showmap;
    var $order;
    var $access;

    // Constructeur de la classe DestinationObj
    function __construct ($id, $category, $region, $city, $cityName, $startingDate, $nbDays,
                          $arrivalBy, $text, $map, $showmap, $images, $order=3) {
      $this->id = $id;
      $this->category = $category;
      $this->region = $region;
      $this->city = $city;
      $this->cityName = $cityName;
      $this->startingDate = $startingDate;
      $this->nbDays = $nbDays;
      $this->arrivalBy = $arrivalBy;
      $this->text = $text;
      $this->images = $images;
      $this->map = $map;
      $this->showmap = $showmap;
      $this->order = $order;
    }

    function setHide ($hide) {
      $this->access = $hide;
    }
  }//class DestinationObj


  // Objet contenant toutes les informations d'un fichier
  class FileObj {
    var $name;
    var $temp;

    // Constructeur de la classe File
    function __construct ($name, $temp='') {
      $this->name = $name;
      $this->temp = $temp;
    }
  }//class FileObj


  // Objet les propriétés de la police d'un élément de texte
  class Fontstyle {
    var $name;
    var $size;
    var $style;
    var $textUnderline;
    var $linkUnderline;
    var $textColor;
    var $linkColor;
    var $hoverColor;
    var $visitedColor;

    // Constructeur de la classe Fontstyle
    function __construct ($name, $size, $style, $textUnderline, $linkUnderline, $textColor, $linkColor, $hoverColor, $visitedColor) {
      $this->name = $name;
      $this->size = $size;
      $this->style = $style;
      $this->textUnderline = $textUnderline;
      $this->linkUnderline = $linkUnderline;
      $this->textColor = $textColor;
      $this->linkColor = $linkColor;
      $this->hoverColor = $hoverColor;
      $this->visitedColor = $visitedColor;
    }
  }//Fontstyle


  // Classe contenant toutes les informations de la page d'accueil
  class Home {
    var $message;
    var $image;

    // Constructeur de la classe Home
    function __construct ($message, $image) {
      $this->message = $message;
      $this->image = $image;
    }
  }//class Home


  // Classe contenant toutes les informations d'une image
  class ImageObj {
    var $name;
    var $description;
    var $temp;
    var $noCache;

    // Constructeur de la classe Image
    function __construct ($name, $description, $temp='', $noCache=false) {
      $this->name = $name;
      $this->description = $description;
      $this->temp = $temp;
      if ($noCache) {
        $this->noCache = '?noCache='.md5(uniqid(rand(), true));
      }
    }
  }//class ImageObj


  // Classe contenant toutes les informations d'une localisation
  class LocationDetails {
    var $name;
    var $lattitude;
    var $longitude;
    var $population;
    var $capitale;
    var $altitude;
    var $surface;
    var $continent;

    // Constructeur de la classe LocationDetails
    function __construct ($name, $lattitude, $longitude, $population, $capitale, $altitude, $surface, $continent) {
      $this->name = $name;
      $this->lattitude = $lattitude;
      $this->longitude = $longitude;
      $this->population = $population;
      $this->capitale = $capitale;
      $this->altitude = $altitude;
      $this->surface = $surface;
      $this->continent = $continent;
    }
  }//class LocationDetails


  // Classe contenant toutes les informations d'un lien
  class Link {
    var $name;
    var $link;
    var $description;

    // Constructeur de la classe Link
    function __construct ($name, $link, $description) {
      $this->name = $name;
      $this->link = $link;
      $this->description = $description;
    }
  }//class Link


  // Classe contenant toutes les informations d'un courrier
  class Mail {
    var $id;
    var $date;
    var $from;
    var $to;
    var $cc;
    var $bcc;
    var $host;
    var $subject;
    var $body;
    var $attachments;
    var $name;
    var $html;

    // Constructeur de la classe Email
    function __construct ($id, $date, $from, $to, $cc, $bcc, $host, $subject, $body, $attachments, $name="", $html=0, $deleted=0) {
      $this->id = $id;
      $this->date = $date;
      $this->from = $from;
      $this->to = $to;
      $this->cc = $cc;
      $this->bcc = $bcc;
      $this->host = $host;
      $this->subject = $subject;
      $this->body = $body;
      $this->attachments = $attachments;
      $this->name = $name;
      $this->html = $html;
      $this->deleted = $deleted;
    }

  }//class Mail


  // Classe contenant toutes les informations d'une boîte de message
  class Mailbox {
    var $name;
    var $fullname;
    var $host;
    var $email;
    var $username;
    var $password;
    var $date;

    // Constructeur de la classe Mailbox
    function __construct ($name, $host, $email, $username, $password, $date) {
      $this->name = $name;
      $this->host = $host;
      $this->email = $email;
      $this->username = $username;
      $this->password = $password;
      $this->date = $date;
    }
  }//class Mailbox


  // Classe contenant toutes les options des menus de l'album
  class MenuOptions {
    var $forum;
    var $notebook;
    var $humour;
    var $news;
    var $chat;
    var $video;
    var $preparation;
    var $statistics;
    var $ranking;

    // Constructeur de la classe MenuOptions
    function __construct ($forum, $notebook, $humour, $news, $chat, $video, $preparation, $statistics, $ranking) {
      $this->forum = $forum;
      $this->notebook = $notebook;
      $this->humour = $humour;
      $this->news = $news;
      $this->chat = $chat;
      $this->video = $video;
      $this->preparation = $preparation;
      $this->statistics = $statistics;
      $this->ranking = $ranking;
    }
  }//class MenuOptions


  // Classe contenant toutes les informations d'un message d'envoi
  class MessageSender {
    var $id;
    var $name;
    var $email;
    var $date;
    var $subject;
    var $text;
    var $images;

    // Constructeur de la classe MessageSender
    function __construct ($id, $name, $email, $date, $subject, $text, $images) {
      $this->id = $id;
      $this->name = $name;
      $this->email = $email;
      $this->date = $date;
      $this->subject = $subject;
      $this->text = $text;
      $this->images = $images;
    }
  }//class MessageSender


  // Classe contenant toutes les informations d'un message standard
  class MessageObj {
    var $id;
    var $date;
    var $access;
    var $subject;
    var $text;
    var $images;

    // Constructeur de la classe MessageObj
    function __construct ($id, $date, $access, $subject, $text, $images) {
      $this->id = $id;
      $this->date = $date;
      $this->access = $access;
      $this->subject = $subject;
      $this->text = $text;
      $this->images = $images;
    }
  }//class MessageObj


  // Classe contenant toutes les informations d'une personne
  class Person {
    var $id;
    var $name;
    var $email;
    var $date;

    // Constructeur de la classe Person
    function __construct ($id, $name, $email, $date) {
      $this->id = $id;
      $this->name = $name;
      $this->email = $email;
      $this->date = $date;
    }
  }//class Person


  // Classe contenant toutes les informations des propriétés de l'album
  class Property {
    var $font;
    var $bgColor;
    var $bgBodyColor;
    var $bgImg;
    var $imgBgPos;
    var $skin;
    var $saveOrigImg;
    var $showStats;
    var $showBudget;
    var $showMap;
    var $showDescription;
    var $showShortCut;
    var $deleteEmails;
    var $sendEmails;
    var $dateFormat;
    var $language;

    // Constructeur de la classe Property
    function __construct ($font, $bgColor, $bgBodyColor, $bgImg, $imgBgPos, $skin, $saveOrigImg, $showStats, $showBudget, $showMap, $showDescription, $showShortCut, $deleteEmails, $sendEmails, $dateFormat, $language) {
      $this->font = $font;
      $this->bgColor = $bgColor;
      $this->bgBodyColor = $bgBodyColor;
      $this->bgImg = $bgImg;
      $this->imgBgPos = $imgBgPos;
      $this->skin = $skin;
      $this->saveOrigImg = $saveOrigImg;
      $this->showStats = $showStats;
      $this->showBudget = $showBudget;
      $this->showMap = $showMap;
      $this->showDescription = $showDescription;
      $this->showShortCut = $showShortCut;
      $this->deleteEmails = $deleteEmails;
      $this->sendEmails = $sendEmails;
      $this->dateFormat = $dateFormat;
      $this->language = $language;
    }
  }//class Property


  // Classe contenant toutes les informations d'une texture
  class Skin {
    var $path;
    var $imgLogoff;
    var $imgAdmin;

    // Constructeur de la classe Skin
    function __construct ($path, $imgLogoff, $imgAdmin){
      $this->path = $path;
      $this->imgLogoff = $imgLogoff;
      $this->imgAdmin = $imgAdmin;
    }
  }//class Skin


  // Classe contenant toutes les informations d'un résumé d'un pays
  class Summary {
    var $general;
    var $population;
    var $politic;
    var $history;
    var $nature;
    var $infra;
    var $food;
    var $activity;
    var $budget;
    var $accommodation;
    var $transport;
    var $communication;
    var $other;

    // Constructeur de la classe Summary
    function __construct ($general, $population, $politic, $history, $nature, $infra, $food, $activity, $budget, $accommodation, $transport, $communication, $other) {
      $this->general = $general;
      $this->population = $population;
      $this->politic = $politic;
      $this->history = $history;
      $this->nature = $nature;
      $this->infra = $infra;
      $this->food = $food;
      $this->activity = $activity;
      $this->budget = $budget;
      $this->accommodation = $accommodation;
      $this->transport = $transport;
      $this->communication = $communication;
      $this->other = $other;
    }
  }//class Summary


  // Classe contenant toutes les informations d'une tâche
  class Task {
    var $id;
    var $type;
    var $subject;
    var $description;
    var $start;
    var $end;
    var $block;
    var $reminder;

    // Constructeur de la classe Task
    function __construct ($id, $type, $subject, $description, $start, $end, $block, $reminder) {
      $this->id = $id;
      $this->type = $type;
      $this->subject = $subject;
      $this->description = $description;
      $this->start = $start;
      $this->end = $end;
      $this->block = $block;
      $this->reminder = $reminder;
    }
  }//class Task

?>