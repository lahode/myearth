// months as they appear in the calendar's title
var ARR_MONTHS = ["Janvier", "F�vrier", "Mars", "Avril", "Mai", "Juin",
		"Juillet", "Ao�t", "Septembre", "Octobre", "Novembre", "D�cembre"];
// week day titles as they appear on the calendar
var ARR_WEEKDAYS = ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"];
// day week starts from (normally 0-Su or 1-Mo)
var NUM_WEEKSTART = 1;
// path to the directory where calendar images are stored. trailing slash req.
var STR_ICONPATH = '../lib/pics/calendar/';

var re_url = new RegExp('datetime=(\\-?\\d+)');
var dt_current = (re_url.exec(String(window.location)) ? new Date(new Number(RegExp.$1)) : new Date());
var ttt = 1120168800000;
var re_id = new RegExp('id=(\\d+)');
var num_id = (re_id.exec(String(window.location)) ? new Number(RegExp.$1) : 0);
var obj_caller = (window.opener ? window.opener.calendars[num_id] : null);

// get same date in the previous year
var dt_prev_year = new Date(dt_current);
dt_prev_year.setFullYear(dt_prev_year.getFullYear() - 1);
if (dt_prev_year.getDate() != dt_current.getDate()) dt_prev_year.setDate(0);


// get same date in the next year
var dt_next_year = new Date(dt_current);
dt_next_year.setFullYear(dt_next_year.getFullYear() + 1);
if (dt_next_year.getDate() != dt_current.getDate()) dt_next_year.setDate(0);


// get same date in the previous month
var dt_prev_month = new Date(dt_current);
dt_prev_month.setMonth(dt_prev_month.getMonth() - 1);
if (dt_prev_month.getDate() != dt_current.getDate())
	dt_prev_month.setDate(0);


// get same date in the next month
var dt_next_month = new Date(dt_current);
dt_next_month.setMonth(dt_next_month.getMonth() + 1);
if (dt_next_month.getDate() != dt_current.getDate())
	dt_next_month.setDate(0);


// get first day to display in the grid for current month
var dt_firstday = new Date(dt_current);
dt_firstday.setDate(1);
dt_firstday.setDate(1 - (7 + dt_firstday.getDay() - NUM_WEEKSTART) % 7);


// function passing selected date to calling window
function set_datetime(n_datetime, b_close) {
  if (!obj_caller) return;
  var dt_datetime = obj_caller.prs_time((document.cal ? document.cal.time.value : ''), new Date(n_datetime));
  if (!dt_datetime) return;
  if (b_close) {
    var copydt_datetime = new Date(dt_datetime.valueOf());
    obj_caller.target.value = (document.cal ? obj_caller.gen_tsmp(dt_datetime) : obj_caller.gen_date(dt_datetime));
    if (obj_caller.formulaire) obj_caller.formulaire.submit();
    window.close();
  }
  else obj_caller.popup(dt_datetime.valueOf());
}
