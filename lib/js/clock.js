// CREDITS:
// Analog Clock 3.01
// By Peter Gehrig and Urs Dudli
// Copyright (c) 2004 Peter Gehrig and Urs Dudli. All rights reserved.
// Permission given to use the script provided that this notice remains as is.
// Additional scripts can be found at http://www.24fun.com
// info@24fun.com
// 1/29/2004

// IMPORTANT:
// If you add this script to a script-library or script-archive
// you have to add a highly visible link to
// http://www.24fun.com on the webpage
// where this script will be featured

var clipTop=0
var clipBottom=clipTop+60
var clipLeft
var clipRight
var imgSeconds
var imgMinutes
var imgHours
var imgClock
var textDate
var textDay
var textLink
var clipHours=""
var clipMinutes=""
var clipSeconds=""
var startLeft=10
var startTop=20
var tunePosHour =-2
var decalage=0;

var browserinfos=navigator.userAgent
var ie=document.all&&!browserinfos.match(/Opera/)
var ns6=document.getElementById&&!document.all&&!browserinfos.match(/Opera/)
var opera=browserinfos.match(/Opera/)

function initClock(decal) {
        var nowDate = new Date();
	decal = decal + (nowDate.getTimezoneOffset() / 60);                                // Ajustement GMT
        var tempHours = nowDate.getHours()+Math.ceil(decal);
        if (tempHours >= 24) var nowDate = new Date(nowDate.getTime() + (1000 * 60 * 60 * 24));
        if (tempHours < 0) var nowDate = new Date(nowDate.getTime() - (1000 * 60 * 60 * 24));
       
        var dayofweek = nowDate.getDay()
       
        var datetoday = nowDate.getDate()
       
        switch (dayofweek) {
          case 1  : dayofweek = "lun";break;
          case 2  : dayofweek = "mar";break;
          case 3  : dayofweek = "mer";break;
          case 4  : dayofweek = "jeu";break;
          case 5  : dayofweek = "ven";break;
          case 6  : dayofweek = "sam";break;
          default : dayofweek = "lun";break;
        }


	if (ie) {
		document.all.roof.style.posLeft=DL_GetElementLeft(document.all.marker);
		document.all.roof.style.posTop=DL_GetElementTop(document.all.marker);
		imgSeconds=document.all.imgSeconds.style
		imgMinutes=document.all.imgMinutes.style
		imgHours=document.all.imgHours.style
		imgClock=document.all.imgClock.style
		textDate=document.all.textDate.style
		textDay=document.all.textDay.style
		imgHours.posTop=startTop
		imgMinutes.posTop=startTop
		imgSeconds.posTop=startTop
		textDate.posTop=43
		textDate.posLeft=55
		textDay.posTop=43
		textDay.posLeft=23
		document.all.textDate.innerHTML=datetoday
		document.all.textDay.innerHTML=dayofweek
	}
	if (ns6 || opera) {
		document.getElementById('roof').style.left=DL_GetElementLeft(document.getElementById('marker'));
		document.getElementById('roof').style.top=DL_GetElementTop(document.getElementById('marker'));
		imgSeconds=document.getElementById("imgSeconds").style
		imgMinutes=document.getElementById("imgMinutes").style
		imgHours=document.getElementById("imgHours").style
		imgClock=document.getElementById("imgClock").style
		textDate=document.getElementById("textDate").style
		textDay=document.getElementById("textDay").style	
		imgHours.top=startTop
		imgMinutes.top=startTop
		imgSeconds.top=startTop
		textDate.top=43
		textDate.left=55
		textDay.top=43
		textDay.left=23
		document.getElementById("textDate").innerHTML=datetoday
		document.getElementById("textDay").innerHTML=dayofweek
	}
	imgSeconds.visibility="visible"
	imgMinutes.visibility="visible"
	imgHours.visibility="visible"
	imgClock.visibility="visible"
	textDate.visibility="visible"
	textDay.visibility="visible"	
	decalage = decal;
        tick()
}

function setDecalage (decal) {
   decalage = decal;
}

function tick() {
	var now = new Date();
        var nowMinutes = 0;
	var nowHours = 0;

        nowHours = now.getHours()+Math.ceil(decalage);
        if (decalage >= 0) {
          nowMinutes = now.getMinutes()+(Math.round((decalage-Math.floor(decalage))*60));
        } else {
          nowMinutes = now.getMinutes()+(Math.round((decalage-Math.ceil(decalage))*60));
        }
        if (nowMinutes >= 60) nowMinutes = nowMinutes - 60;
        if (nowMinutes < 0) nowMinutes = nowMinutes + 60;
        if (nowHours >= 24) nowHours = nowHours - 24;
        if (nowHours < 0) nowHours = nowHours + 24;
        nowHours = nowHours % 12;
	var nowSeconds = now.getSeconds();
	if (ie) {
		imgHours.posLeft=-(nowHours*60*5+((Math.round(nowMinutes/12))*60)+startLeft+tunePosHour)
		clipLeft = -(imgHours.posLeft)+20
	}
	if (ns6 || opera) {
		imgHours.left=-(nowHours*60*5+((Math.round(nowMinutes/12))*60)+startLeft+tunePosHour)
		clipLeft = -(parseInt(imgHours.left))+20
	}
	clipRight= clipLeft+60
	clipHours ="rect("+clipTop+" "+clipRight+" "+clipBottom+" "+clipLeft+")"
	imgHours.clip=clipHours

	if (ie) {
		imgMinutes.posLeft=-((nowMinutes)*60+startLeft)
		clipLeft = -(imgMinutes.posLeft)+20
	}
	if (ns6 || opera) {
		imgMinutes.left=-((nowMinutes)*60+startLeft)
		clipLeft = -(parseInt(imgMinutes.left))+20
	}
	clipRight= clipLeft+60
	clipMinutes ="rect("+clipTop+" "+clipRight+" "+clipBottom+" "+clipLeft+")"
	imgMinutes.clip=clipMinutes

	if (ie) {
		imgSeconds.posLeft=-((nowSeconds)*60+startLeft)
		clipLeft = -(imgSeconds.posLeft)+20
	}
	if (ns6 || opera) {
		imgSeconds.left=-((nowSeconds)*60+startLeft)
		clipLeft = -(parseInt(imgSeconds.left))+20
	}
	clipRight= clipLeft+60
	clipSeconds ="rect("+clipTop+" "+clipRight+" "+clipBottom+" "+clipLeft+")"
	imgSeconds.clip=clipSeconds
	setTimeout("tick("+decalage+")", 400);
}

function DL_GetElementLeft(eElement) {
    var nLeftPos = eElement.offsetLeft;          
    var eParElement = eElement.offsetParent;     
    while (eParElement != null) {                                            
        nLeftPos += eParElement.offsetLeft;      
        eParElement = eParElement.offsetParent;  
    }
    return nLeftPos;                            
}

function DL_GetElementTop(eElement) {
    var nTopPos = eElement.offsetTop;            
    var eParElement = eElement.offsetParent;     
    while (eParElement != null) {                                            
        nTopPos += eParElement.offsetTop;        
        eParElement = eParElement.offsetParent;  
    }
    return nTopPos;
}

