  // Initialisation des propri�t�s
  var agrandissement=10;                   // Coefficient fixe d'agrandissement
  var vitesse=0.2;                         // Vitesse initiale de l'agrandissement
  var largImg=460;                         // Largeur initiale de l'image
  var hautImg=234;                         // Hauteur initiale de l'image
  var posX = 0;                            // Position du zoom en abscisse
  var posY = 0;                            // Position du zoom en ordonn�e
  var coeff=agrandissement;                // Coefficient variable d'agrandissement
  var zoomOutLoop=0;                       // Valeur de la boucle ZoomOut
  var zoomInLoop=0;                        // Valeur de la boucle ZoomIn
  var positionImageX='left';               // Fixe la position horizontale de l'image
  var positionImageY='down';               // Fixe la position vertical de l'image
  var tailleCanevaX=460;                   // Largeur du canevas
  var tailleCanevaY=234;                   // Hauteur du canevas
  var tailleDest=18;                       // Taille initiale de l'image des destinations par d�faut
  var posDestX = new Array();              // Position du zoom en abscisse des destinations
  var posDestY = new Array();              // Position du zoom en abscisse des destinations
  var destination = null;                  // Tableau des destinations


  // R�initialisation les propri�t�s
  function initWorldVariable (zoom, largImage, hautImage, posImgX, posImgY, sizeDest, arrayDest) {
    agrandissement=zoom;                      // Coefficient fixe d'agrandissement
    largImg=largImage;                        // Largeur initiale de l'image
    hautImg=hautImage;                        // Hauteur initiale de l'image
    positionImageX=posImgX;                   // Fixe la position horizontale de l'image
    positionImageY=posImgY;                   // Fixe la position vertical de l'image
    tailleDest=sizeDest;                      // Taille initiale de l'image des destinations
    destination = arrayDest;                  // Tableau des destinations
    coeff=agrandissement;                     // Coefficient variable d'agrandissement
  }


  // Initialisation de l'agrandissement du zoom (minimum)
  function initWorld (decalX, decalY, scrollActive) {
    document.image.width = Math.round(largImg/agrandissement);
    document.image.height = Math.round(hautImg/agrandissement);

    if (destination && destination['tabjs'])
    for (var i = 0;i<destination['tabjs'].length;i++) {
      posDestX[i] = destination['tabjs'][i]['x'];
      posDestY[i] = destination['tabjs'][i]['y'];
      taille = Math.round(tailleDest/coeff);

      if (document.getElementById) {
        if (destination['tabjs'][i]['print']=='true') {
          document.getElementById('destination['+i+']').width = Math.round(tailleDest/agrandissement);
          document.getElementById('destination['+i+']').height = Math.round(tailleDest/agrandissement);
          if (positionImageX=='left')
            document.getElementById('layerDest['+i+']').style.left = posDestX[i]+'px';
          else
            document.getElementById('layerDest['+i+']').style.left = posDestX[i]-(taille/2)+'px';
          if (positionImageY=='down')
            document.getElementById('layerDest['+i+']').style.top = posDestY[i]-taille+'px';
          else
            document.getElementById('layerDest['+i+']').style.top = posDestY[i]-(taille/2)+'px';
          document.getElementById('number['+i+']').style.width = taille;
          document.getElementById('number['+i+']').style.height = taille;
          document.getElementById('number['+i+']').style.fontSize = Math.round(tailleDest/coeff*0.8)+'px';
        }
      } else if (document.all) {
        if (destination['tabjs'][i]['print']=='true') {
          document.all['destination['+i+']'].width = Math.round(tailleDest/agrandissement);
          document.all['destination['+i+']'].height = Math.round(tailleDest/agrandissement);
          if (positionImageX=='left')
            document.all['layerDest['+i+']'].style.left = posDestX[i]+'px';
          else
            document.all['layerDest['+i+']'].style.left = posDestX[i]-(taille/2)+'px';
          if (positionImageY=='down')
            document.all['layerDest['+i+']'].style.top = posDestY[i]-taille+'px';
          else
            document.all['layerDest['+i+']'].style.top = posDestY[i]-(taille/2)+'px';
          document.all['number['+i+']'].style.width = Math.round(tailleDest/coeff);
          document.all['number['+i+']'].style.height = Math.round(tailleDest/coeff);
          document.all['number['+i+']'].style.fontSize = Math.round(tailleDest/coeff*0.8)+'px';
        }
      }
    }
    initScrollLayers('wn', 'lyr', 'imgTbl', 'scrollbar', decalX, decalY, scrollActive);
  }


  // Zoom sur la carte
  function zoomer() {
    if (document.image.width < largImg && document.image.height < hautImg) {

      // Gestion du zoom de la carte
      var lastLeft = posX;
      var lastTop = posY;
      lastcoeff = coeff;
      coeff = coeff-vitesse;
      lastLeft = (lastLeft-(largImg/agrandissement/2))*Math.abs(lastcoeff/coeff)+(largImg/agrandissement/2);
      lastTop = (lastTop-(hautImg/agrandissement/2))*Math.abs(lastcoeff/coeff)+(hautImg/agrandissement/2);
      if (lastLeft-(largImg/agrandissement) < (-largImg/coeff)) lastLeft = -largImg/coeff+(largImg/agrandissement/2);
      if (lastTop-(hautImg/agrandissement) < (-hautImg/coeff)) lastTop = -hautImg/coeff+(hautImg/agrandissement/2);
      posX = lastLeft;
      posY = lastTop;
      document.image.width = Math.round(largImg/coeff);
      document.image.height = Math.round(hautImg/coeff);
      if (document.getElementById) {
        document.getElementById('lyr').style.left = lastLeft+'px';
        document.getElementById('lyr').style.top = lastTop+'px';
      } else if (document.all) {
        document.all['lyr'].style.left = lastLeft+'px';
        document.all['lyr'].style.top = lastTop+'px';
      }

      // Gestion du zoom des destinations
      if (destination && destination['tabjs'])
      for (var i = 0;i<destination['tabjs'].length;i++) {
        var lastLeftDest = posDestX[i];
        var lastTopDest = posDestY[i];
        lastLeftDest = (lastLeftDest)*Math.abs(lastcoeff/coeff);
        lastTopDest = (lastTopDest)*Math.abs(lastcoeff/coeff);
        posDestX[i] = lastLeftDest;
        posDestY[i] = lastTopDest;
        taille = Math.round(tailleDest/coeff);
        if (document.getElementById) {
          if (destination['tabjs'][i]['print']=='true') {
            document.getElementById('destination['+i+']').width = taille;
            document.getElementById('destination['+i+']').height = taille;
            if (positionImageX=='left')
              document.getElementById('layerDest['+i+']').style.left = lastLeftDest+'px';
            else
              document.getElementById('layerDest['+i+']').style.left = lastLeftDest-(taille/2)+'px';
            if (positionImageY=='down')
              document.getElementById('layerDest['+i+']').style.top = lastTopDest-taille+'px';
            else
              document.getElementById('layerDest['+i+']').style.top = lastTopDest-(taille/2)+'px';
            document.getElementById('number['+i+']').style.width = taille;
            document.getElementById('number['+i+']').style.height = taille;
            document.getElementById('number['+i+']').style.fontSize = Math.round(tailleDest/coeff*0.8)+'px';
          }
        } else if (document.all) {
          if (destination['tabjs'][i]['print']=='true') {
            document.all['destination['+i+']'].width = taille;
            document.all['destination['+i+']'].height = taille;
            if (positionImageX=='left')
              document.all['layerDest['+i+']'].style.left = lastLeftDest+'px';
            else
              document.all['layerDest['+i+']'].style.left = lastLeftDest-(taille/2)+'px';
            if (positionImageY=='down')
              document.all['layerDest['+i+']'].style.top = lastTopDest-taille+'px';
            else
              document.all['layerDest['+i+']'].style.top = lastTopDest-(taille/2)+'px';
            document.all['number['+i+']'].style.width = taille;
            document.all['number['+i+']'].style.height = taille;
            document.all['number['+i+']'].style.fontSize = Math.round(tailleDest/coeff*0.8)+'px';
          }
        }
      }

      // Rafraichi l'affichage
      zoomInLoop=window.setTimeout('zoomer();',60);//vitesse de l'effet
      initScrollLayers('wn', 'lyr', 'imgTbl', 'scrollbar', lastLeft, lastTop, 1);
      }
    else {window.clearTimeout(zoomInLoop);}
  }


  // Dezoom sur la carte
  function dezoomer() {
    if (document.image.width > largImg/agrandissement && document.image.height > hautImg/agrandissement && coeff<agrandissement) {

      // Gestion du zoom de la carte
      var lastLeft = posX;
      var lastTop = posY;
      lastcoeff = coeff;
      if (coeff+vitesse > agrandissement)
        coeff = coeff+0.1;
      else
        coeff = coeff+vitesse;
      lastLeft = (lastLeft-(largImg/agrandissement/2))*Math.abs(lastcoeff/coeff)+(largImg/agrandissement/2);
      lastTop = (lastTop-(hautImg/agrandissement/2))*Math.abs(lastcoeff/coeff)+(hautImg/agrandissement/2);
      if (lastLeft > 0) lastLeft = 0;
      if (lastTop > 0) lastTop = 0;
      if (lastLeft-tailleCanevaX < (-largImg/coeff)) if (-largImg/coeff+tailleCanevaX < 0) lastLeft = -largImg/coeff+tailleCanevaX; else lastLeft = 0;
      if (lastTop-tailleCanevaY < (-hautImg/coeff)) if (-hautImg/coeff+tailleCanevaY < 0) lastTop = -hautImg/coeff+tailleCanevaY; else lastTop = 0;
      posX = lastLeft;
      posY = lastTop;
      document.image.width = Math.round(largImg/coeff);
      document.image.height = Math.round(hautImg/coeff);
      if (document.getElementById) {
        document.getElementById('lyr').style.left = lastLeft+'px';
        document.getElementById('lyr').style.top = lastTop+'px';
      } else if (document.all) {
        document.all['lyr'].style.left = lastLeft+'px';
        document.all['lyr'].style.top = lastTop+'px';
      }

      // Gestion du zoom des destinations
      if (destination && destination['tabjs'])
      for (var i = 0;i<destination['tabjs'].length;i++) {
        var lastLeftDest = posDestX[i];
        var lastTopDest = posDestY[i];
        lastLeftDest = (lastLeftDest)*Math.abs(lastcoeff/coeff);
        lastTopDest = (lastTopDest)*Math.abs(lastcoeff/coeff);
        posDestX[i] = lastLeftDest;
        posDestY[i] = lastTopDest;
        taille = Math.round(tailleDest/coeff);
        if (document.getElementById) {
          if (destination['tabjs'][i]['print']=='true') {
            document.getElementById('destination['+i+']').width = taille;
            document.getElementById('destination['+i+']').height = taille;
            if (positionImageX=='left')
              document.getElementById('layerDest['+i+']').style.left = lastLeftDest+'px';
            else
              document.getElementById('layerDest['+i+']').style.left = lastLeftDest-(taille/2)+'px';
            if (positionImageY=='down')
              document.getElementById('layerDest['+i+']').style.top = lastTopDest-taille+'px';
            else
              document.getElementById('layerDest['+i+']').style.top = lastTopDest-(taille/2)+'px';
            document.getElementById('number['+i+']').style.width = taille;
            document.getElementById('number['+i+']').style.height = taille;
            document.getElementById('number['+i+']').style.fontSize = Math.round(tailleDest/coeff*0.8)+'px';
          }
        } else if (document.all) {
          if (destination['tabjs'][i]['print']=='true') {
            document.all['destination['+i+']'].width = taille;
            document.all['destination['+i+']'].height = taille;
            if (positionImageX=='left')
              document.all['layerDest['+i+']'].style.left = lastLeftDest+'px';
            else
              document.all['layerDest['+i+']'].style.left = lastLeftDest-(taille/2)+'px';
            if (positionImageY=='down')
              document.all['layerDest['+i+']'].style.top = lastTopDest-(taille/2)+'px';
            else
              document.all['layerDest['+i+']'].style.top = lastTopDest-(taille/2)+'px';
            document.all['number['+i+']'].style.width = taille;
            document.all['number['+i+']'].style.height = taille;
            document.all['number['+i+']'].style.fontSize = Math.round(tailleDest/coeff*0.8)+'px';
          }
        }
      }

      // Rafraichi l'affichage
      zoomOutLoop=window.setTimeout('dezoomer();',60);//vitesse de l'effet
      initScrollLayers('wn', 'lyr', 'imgTbl', 'scrollbar', lastLeft, lastTop, 1);
    }
    else {window.clearTimeout(zoomOutLoop);}
  }


  // Arr�te le zoom sur la carte
  function stopzoom () {
    window.clearTimeout(zoomInLoop);
    window.clearTimeout(zoomOutLoop);
  }
