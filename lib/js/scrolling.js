//scroller's width
var swidth=0;
var sspeed=0;
var rspeed=0;
var spause=0;
var singletext=new Array();
singletext[0]='<nobr class=tan>&nbsp;</nobr>';
var ii=0;

function speedup() {
  if(sspeed<rspeed*16) {
    sspeed=sspeed*2;
  }
}

function speeddown() {
  if(sspeed>rspeed/16) {
    sspeed=sspeed/2;
  }
}

function startScroll(message, width, speed, pause) {
  singletext[0] = '<nobr class=tan>'+message+'</nobr>';
  swidth = width;
  sspeed = speed;
  rspeed = sspeed;
  spause = pause;

  if (document.getElementById) {
    ns6div=document.getElementById('iens6div');
    ns6div.style.left=swidth;
    ns6div.innerHTML=singletext[0];
    sizeup=ns6div.offsetWidth;ns6scroll();
  } else if (document.layers) {
    ns4layer=document.ns4div.document.ns4div1;
    ns4layer.left=swidth;
    ns4layer.document.write(singletext[0]);
    ns4layer.document.close();
    sizeup=ns4layer.document.width;ns4scroll();
  } else if (document.all) {
    iediv=iens6div;
    iediv.style.pixelLeft=swidth;
    iediv.innerHTML=singletext[0];
    sizeup=iediv.offsetWidth;iescroll();
  }
}

function iescroll() {
  if (iediv.style.pixelLeft>0&&iediv.style.pixelLeft<=sspeed) {
    iediv.style.pixelLeft=0;
    setTimeout("iescroll()", spause);
  } else if (iediv.style.pixelLeft>=sizeup*-1) {
    iediv.style.pixelLeft-=sspeed;
    setTimeout("iescroll()",100);
  } else {
    if(ii==singletext.length-1) ii=0; else ii++;
    iediv.style.pixelLeft=swidth;
    iediv.innerHTML=singletext[ii];
    sizeup=iediv.offsetWidth;iescroll();
  }
}

function ns4scroll() {
  if (ns4layer.left>0&&ns4layer.left<=sspeed) {
    ns4layer.left=0;
    setTimeout("ns4scroll()", spause);
  } else if(ns4layer.left>=sizeup*-1) {
    ns4layer.left-=sspeed;
    setTimeout("ns4scroll()",100);
  } else {
    if(ii==singletext.length-1) ii=0; else ii++;
    ns4layer.left=swidth;
    ns4layer.document.write(singletext[ii]);
    ns4layer.document.close();
    sizeup=ns4layer.document.width;
    ns4scroll();
  }
}

function ns6scroll() {
  if (parseInt(ns6div.style.left)>0&&parseInt(ns6div.style.left)<=sspeed) {
    ns6div.style.left=0;
    setTimeout("ns6scroll()", spause);
  } else if (parseInt(ns6div.style.left)>=sizeup*-1) {
    ns6div.style.left=parseInt(ns6div.style.left)-sspeed;
    setTimeout("ns6scroll()",100);
  } else {
    if(ii==singletext.length-1) ii=0; else ii++;
    ns6div.style.left=swidth;
    ns6div.innerHTML=singletext[ii];
    sizeup=ns6div.offsetWidth;
    ns6scroll();
  }
}
