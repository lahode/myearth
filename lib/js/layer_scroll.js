var clipTop = 0;
var clipWidth = 170;
var clipBottom = 50;
var topper = 300;
var lyrheight = 0;
var time,amount,theTime,theHeight,DHTML;

function layer_init(obj)
{
  var x = obj;
  if (document.getElementById || document.all) {
    lyrheight = x.obj.offsetHeight;
    var clipstring = 'rect('+clipTop+'px,'+clipWidth+'px,'+clipBottom+'px,0)';
    x.style.clip = clipstring;
  } else return;
}

function layer_scroll(obj,amt,tim)
{
  thelayer = obj;
  if (!thelayer) return;
  amount = amt;
  theTime = tim;
  layer_realscroll();
}

function layer_stopScroll()
{
  if (time) clearTimeout(time);
}

function layer_realscroll()
{
  clipTop += amount;
  clipBottom += amount;
  topper -= amount;
  if (clipTop < 0 || clipBottom > lyrheight)
  {
    clipTop -= amount;
    clipBottom -= amount;
    topper += amount;
    return;
  }
  if (document.getElementById || document.all)
  {
    clipstring = 'rect('+clipTop+'px,'+clipWidth+'px,'+clipBottom+'px,0)'
    thelayer.style.clip = clipstring;
    thelayer.style.top = topper + 'px';
  }
  else if (document.layers)
  {
    thelayer.style.clip.top = clipTop;
    thelayer.style.clip.bottom = clipBottom;
    thelayer.style.top = topper;
  }
  time = setTimeout('layer_realscroll()',theTime);
}
