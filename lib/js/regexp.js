    /* Fonctions de conversion de suites de caract�re : lien http, email, smilies... */
    function convertInnerHTML(chaine) {
      /* Ins�rez ici les fonctions re remplacement d'expressions r�guli�re */
      res = chaine;
      res = convertHTTP(res);
      res = convertEmail(res);
      res = convertSmilies(res, '../lib/pics/emoticon/');
      return res;
    }

    function convertHTTP(chaine)
    {
      var exp1 = new RegExp("((ht|f)tps?://[A-Za-z0-9-_]+\\.[A-Za-z0-9-_%&\?\/.=]*)","gi");
      res = chaine;
      res = res.replace(exp1, "<a href='$1' target='_blank'>$1</a>");
      return res ;
    }
    
    function convertEmail(chaine)
    {
      var exp2 = new RegExp("([a-zA-Z0-9\.\-_]+@[a-zA-Z0-9\-_]+\.[a-zA-Z0-9]{2,})","g");
      
      res = chaine ;
      res = res.replace(exp2,"<a class='linkChat' href='mailto:$1'>$1</a>");
      
      return res ;
    }
    

    function convertSmilies(chaine, path)
    {
      res = chaine ;
      
      res = res.replace(new RegExp("(\\:\\))","g")," <img src='"+path+"01_glad.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:D)","g")," <img src='"+path+"02_happy.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:-D)","g")," <img src='"+path+"02_happy.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:grin\\:)","g")," <img src='"+path+"02_happy.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:\\))","g")," <img src='"+path+"01_glad.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:-\\))","g")," <img src='"+path+"01_glad.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:smile\\:)","g")," <img src='"+path+"01_glad.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:\\()","g")," <img src='"+path+"06_sad.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:-\\()","g")," <img src='"+path+"06_sad.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:sad\\:)","g")," <img src='"+path+"06_sad.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:o)","g")," <img src='"+path+"07_estonish.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:-o)","g")," <img src='"+path+"07_estonish.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:eek\\:)","g")," <img src='"+path+"07_estonish.gif' alt='smiley' />")
      res = res.replace(new RegExp("(8O)","g")," <img src='"+path+"13_oups.gif' alt='smiley' />")
      res = res.replace(new RegExp("(8-O)","g")," <img src='"+path+"13_oups.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:shock\\:)","g")," <img src='"+path+"13_oups.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:\\?)","g")," <img src='"+path+"10_charmed.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:-\\?)","g")," <img src='"+path+"10_charmed.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:\\?\\?\\?\\:)","g")," <img src='"+path+"10_charmed.gif' alt='smiley' />")
      res = res.replace(new RegExp("(8\\))","g")," <img src='"+path+"09_sunglass.gif' alt='smiley' />")
      res = res.replace(new RegExp("(8-\\))","g")," <img src='"+path+"09_sunglass.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:cool\\:)","g")," <img src='"+path+"09_sunglass.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:lol\\:)","g")," <img src='"+path+"03_laugh.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:x)","g")," <img src='"+path+"08_angry.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:-x)","g")," <img src='"+path+"08_angry.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:mad\\:)","g")," <img src='"+path+"08_angry.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:P)","g")," <img src='"+path+"05_tongue.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:-P)","g")," <img src='"+path+"05_tongue.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:razz\\:)","g")," <img src='"+path+"05_tongue.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:oops\\:)","g")," <img src='"+path+"10_charmed.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:cry\\:)","g")," <img src='"+path+"06_sad.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:evil\\:)","g")," <img src='"+path+"18_devil.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:twisted\\:)","g")," <img src='"+path+"18_devil.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:roll\\:)","g")," <img src='"+path+"15_stoned.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:wink\\:)","g")," <img src='"+path+"04_yeah.gif' alt='smiley' />")
      res = res.replace(new RegExp("(;\\))","g")," <img src='"+path+"04_yeah.gif' alt='smiley' />")
      res = res.replace(new RegExp("(;-\\))","g")," <img src='"+path+"04_yeah.gif' alt='smiley' />")
      res = res.replace(new RegExp("(\\:mrgreen\\:)","g")," <img src='"+path+"21_alien.gif' alt='smiley' />")
      
      return res ;
    }
    
    function protegeChaine(chaine)
    {
      res = chaine ;
      
      res = res.replace(new RegExp("(\\()","g"),"\\(") ;
      res = res.replace(new RegExp("(\\))","g"),"\\)") ;
      res = res.replace(new RegExp("(\\{)","g"),"\\{") ;
      res = res.replace(new RegExp("(\\})","g"),"\\}") ;      
      res = res.replace(new RegExp("(\\[)","g"),"\\[") ;
      res = res.replace(new RegExp("(\\])","g"),"\\]") ;
      res = res.replace(new RegExp("(\\^)","g"),"\\^") ;
      res = res.replace(new RegExp("(\\.)","g"),"\\.") ;
      res = res.replace(new RegExp("(\\-)","g"),"\\-") ;
      res = res.replace(new RegExp("(\\*)","g"),"\\*") ;
      res = res.replace(new RegExp("(\\+)","g"),"\\+") ;
      res = res.replace(new RegExp("(\\?)","g"),"\\?") ;
      res = res.replace(new RegExp("(\\\\)","g"),"\\") ;
      res = res.replace(new RegExp("(\\,)","g"),"\\,") ;
      res = res.replace(new RegExp("(\\|)","g"),"\\|") ;
      
      return res ;
    }
