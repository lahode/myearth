/************************************

  Custom Alert Demonstration
  version 1.0
  last revision: 02.02.2005
  steve@slayeroffice.com

  Should you improve upon this source please
  let me know so that I can update the version
  hosted at slayeroffice.

  Please leave this notice in tact!

************************************/
if (!ERROR_TITLE) var ERROR_TITLE = "Error !";
if (!CONFIRM_TITLE) var CONFIRM_TITLE = "Confirmation";
if (!INFO_TITLE) var INFO_TITLE = "Information";
if (!WARNING_TITLE) var WARNING_TITLE = "Warning !";
if (!REQUEST_TITLE) var REQUEST_TITLE = "Confirmation";
if (!ALERT_BUTTON_TEXT) var ALERT_BUTTON_TEXT = "Ok";
if (!CONFIRM_BUTTON_TEXT1) var CONFIRM_BUTTON_TEXT1 = "Cancel";
if (!CONFIRM_BUTTON_TEXT2) var CONFIRM_BUTTON_TEXT2 = "Ok";

var agt = navigator.userAgent.toLowerCase();
var isMac = (agt.indexOf('mac') != -1);
var isOpera = (agt.indexOf("opera") != -1);
var IEver = parseInt(agt.substring(agt.indexOf('msie ') + 5));
var isIE = ((agt.indexOf('msie')!=-1 && !isOpera && (agt.indexOf('webtv')==-1)) && !isMac);
var isIE5win = (isIE && IEver == 5);

if(document.getElementById) {
        window.alert = function(txt, type) {
    createCustomAlert(txt, type);
  }
}

function createCustomConfirm(txt) {
  d = document;
  var ALERT_TITLE;

  if(d.getElementById("modalContainer")) return;

  mObj = d.getElementsByTagName("body")[0].appendChild(d.createElement("div"));
  mObj.id = "modalContainer";
  mObj.name = "modalContainer";
  mObj.style.height = d.documentElement.scrollHeight + "px";

  alertObj = mObj.appendChild(d.createElement("div"));
  alertObj.id = "alertBox";
  //if(d.all && !window.opera) alertObj.style.top = document.documentElement.scrollTop + "px";
  //alertObj.style.left = d.documentElement.scrollWidth  - alertObj.offsetWidth)/2 + "px";
  alertObj.style.top = "100px";
  if (typeof document.body.style.maxHeight != "undefined") {
    alertObj.style.left = (d.documentElement.scrollWidth  - alertObj.offsetWidth)/2+"px";
  } else {
    alertObj.style.left = "0px";
  }
  alertObj.style.visiblity="visible";

  h1 = alertObj.appendChild(d.createElement("h1"));
  h1.className = "requestConfirm";
  h1.appendChild(d.createTextNode(REQUEST_TITLE));

  msg = alertObj.appendChild(d.createElement("p"));
  splitedtxt = txt.split ("<br />");
  for (var i=0; i<splitedtxt.length; i++) {
    texte = splitedtxt[i].replace("'", "�")
    msg.appendChild(d.createTextNode(texte));
    if (i<txt.length) msg.appendChild(document.createElement('br'));
  }
  msg.style.top = "40px"

  btn1 = alertObj.appendChild(d.createElement("a"));
  btn1.id = "cancelBtn";
  btn1.className = "requestConfirm";
  btn1.appendChild(d.createTextNode(CONFIRM_BUTTON_TEXT1));
  btn1.href = "#";
  btn1.onclick = function() { removeCustomAlert();return false; }

  btn2 = alertObj.appendChild(d.createElement("a"));
  btn2.id = "confirmBtn";
  btn2.className = "requestConfirm";
  btn2.appendChild(d.createTextNode(CONFIRM_BUTTON_TEXT2));
  btn2.href = "#";
  btn2.onclick = function() { removeCustomAlert();return false; }

  SelectVisible("hidden",document.getElementsByTagName('select'));
  alertObj.style.display = "block";
}


function createCustomAlert(txt, type) {
  d = document;
  var ALERT_TITLE;

  if(d.getElementById("modalContainer")) return;

  mObj = d.getElementsByTagName("body")[0].appendChild(d.createElement("div"));
  mObj.id = "modalContainer";
  mObj.style.height = d.documentElement.scrollHeight + "px";

  alertObj = mObj.appendChild(d.createElement("div"));
  alertObj.id = "alertBox";
  //if(d.all && !window.opera) alertObj.style.top = document.documentElement.scrollTop + "px";
  //alertObj.style.left = d.documentElement.scrollWidth  - alertObj.offsetWidth)/2 + "px";
  alertObj.style.top = "150px";
  if (typeof document.body.style.maxHeight != "undefined") {
    alertObj.style.left = (d.documentElement.scrollWidth  - alertObj.offsetWidth)/2+"px";
  } else {
    alertObj.style.left = "0px";
  }
  alertObj.style.visiblity="visible";

  h1 = alertObj.appendChild(d.createElement("h1"));
  h1.className = type;
  if (type=='errorAlert') ALERT_TITLE = ERROR_TITLE;
  else if (type=='confirmAlert') ALERT_TITLE = CONFIRM_TITLE;
  else if (type=='infoAlert') ALERT_TITLE = INFO_TITLE;
  else ALERT_TITLE = WARNING_TITLE;
  h1.appendChild(d.createTextNode(ALERT_TITLE));

  msg = alertObj.appendChild(d.createElement("p"));
  splitedtxt = txt.split ("<br />");
  for (var i=0; i<splitedtxt.length; i++) {
    texte = splitedtxt[i].replace("'", "�")
    msg.appendChild(d.createTextNode(texte));
    if (i<txt.length) msg.appendChild(document.createElement('br'));
  }
  msg.style.top = "40px"

  btn = alertObj.appendChild(d.createElement("a"));
  btn.id = "closeBtn";
  btn.className = type;
  btn.appendChild(d.createTextNode(ALERT_BUTTON_TEXT));
  btn.href = "#";
  btn.focus();
  btn.onclick = function() { removeCustomAlert();return false; }

  SelectVisible("hidden",document.getElementsByTagName('select'));
  alertObj.style.display = "block";
}


function removeCustomAlert() {
  document.getElementsByTagName("body")[0].removeChild(document.getElementById("modalContainer"));
  SelectVisible("visible",document.getElementsByTagName('select'));
}

function SelectVisible(v,elem) {
  if (isIE||isIE5win)
    for (var i=0;i<elem.length;i++) elem[i].style.visibility=v;
}
