// if two digit year input dates after this year considered 20 century.
var NUM_CENTYEAR = 30;
// is time input control required by default
var BUL_TIMECOMPONENT = false;
// are year scrolling buttons required by default
var BUL_YEARSCROLL = true;

var calendars = [];
var RE_NUM = /^\-?\d+$/;

function calendar1(obj_target, num, fdate, form1) {

	// assigning methods
	this.formulaire = form1;
	this.calNum = num;
	this.formatDate = fdate;
	this.gen_date = cal_gen_date1;
	this.gen_time = cal_gen_time1;
	this.gen_tsmp = cal_gen_tsmp1;
	this.prs_date = cal_prs_date1;
	this.prs_time = cal_prs_time1;
	this.prs_tsmp = cal_prs_tsmp1;
	this.popup    = cal_popup1;

	// validate input parameters
	if (!obj_target)
		return cal_error("Error calling the calendar: no target control specified");
	if (obj_target.value == null)
		return cal_error("Error calling the calendar: parameter specified is not valid target control");
	this.target = obj_target;
	this.time_comp = BUL_TIMECOMPONENT;
	this.year_scroll = BUL_YEARSCROLL;

	// register in global collections
	this.id = calendars.length;
	calendars[this.id] = this;
}


function transformDate(fdate) {
	// assigning methods
	this.formatDate = fdate;
	this.gen_date = cal_gen_date1;
	this.gen_time = cal_gen_time1;
	this.gen_tsmp = cal_gen_tsmp1;
	this.prs_date = cal_prs_date1;
	this.prs_time = cal_prs_time1;
	this.prs_tsmp = cal_prs_tsmp1;
}


function cal_popup1 (str_datetime) {
	if (str_datetime) {
		this.dt_current = this.prs_tsmp(str_datetime);
	}
	else {
		this.dt_current = this.prs_tsmp(this.target.value);
		this.dt_selected = this.dt_current;
	}
	if (!this.dt_current) return;

	var obj_calwindow = window.open(
		'calendar.php?datetime=' + this.dt_current.valueOf()+ '&id=' + this.id+ '&calNum=' + this.calNum,
		'Calendar', 'width=200,height='+(this.time_comp ? 215 : 190)+
		',status=yes,resizable=no, top=200,left=200,dependent=yes,alwaysRaised=yes,location=no,menuBar=no'
	);
	obj_calwindow.opener = window;
	obj_calwindow.focus();
}

// timestamp generating function
function cal_gen_tsmp1 (dt_datetime) {
	return(this.gen_date(dt_datetime) + ' ' + this.gen_time(dt_datetime));
}

// date generating function
function cal_gen_date1 (dt_datetime) {
	dd = (dt_datetime.getDate() < 10 ? '0' : '') + dt_datetime.getDate();
	mm = (dt_datetime.getMonth() < 9 ? '0' : '') + (dt_datetime.getMonth() + 1);
	YY = dt_datetime.getFullYear();
	switch (this.formatDate) {
		case 'Y-m-d' : returnDate = YY+'-'+mm+'-'+dd;break;
		case 'm-d-Y' : returnDate = mm+'-'+dd+'-'+YY;break;
		case 'd-m-Y' : returnDate = dd+'-'+mm+'-'+YY;break;
		default :      returnDate = dd+'-'+mm+'-'+YY;break;
	}
	return (returnDate);
}

// time generating function
function cal_gen_time1 (dt_datetime) {
	return (
		(dt_datetime.getHours() < 10 ? '0' : '') + dt_datetime.getHours() + ":"
		+ (dt_datetime.getMinutes() < 10 ? '0' : '') + (dt_datetime.getMinutes()) + ":"
		+ (dt_datetime.getSeconds() < 10 ? '0' : '') + (dt_datetime.getSeconds())
	);
}

// timestamp parsing function
function cal_prs_tsmp1 (str_datetime) {
	// if no parameter specified return current timestamp
	if (!str_datetime)
		return (new Date());

	// if positive integer treat as milliseconds from epoch
	if (RE_NUM.exec(str_datetime))
		return new Date(str_datetime);

	// else treat as date in string format
	var arr_datetime = str_datetime.split(' ');
	return this.prs_time(arr_datetime[1], this.prs_date(arr_datetime[0]));
}

// date parsing function
function cal_prs_date1 (str_date) {

	switch (this.formatDate) {
		case 'Y-m-d' : dayValue=2;monthValue=1;yearValue=0;break;
		case 'm-d-Y' : dayValue=1;monthValue=0;yearValue=2;break;
		case 'd-m-Y' : dayValue=0;monthValue=1;yearValue=2;break;
		default :      dayValue=0;monthValue=1;yearValue=2;break;
	}

	var arr_date = str_date.split('-');

	if (arr_date.length != 3) return cal_error ("Invalid date format: '" + str_date + "'.\n");
	if (!arr_date[dayValue]) return cal_error ("Invalid date format: '" + str_date + "'.\n");
	if (!RE_NUM.exec(arr_date[dayValue])) return cal_error ("Invalid day of month value: '" + arr_date[dayValue] + "'.\nAllowed values are unsigned integers.");
	if (!arr_date[monthValue]) return cal_error ("Invalid date format: '" + str_date + "'.\n");
	if (!RE_NUM.exec(arr_date[monthValue])) return cal_error ("Invalid month value: '" + arr_date[monthValue] + "'.\nAllowed values are unsigned integers.");
	if (!arr_date[yearValue]) return cal_error ("Invalid date format: '" + str_date + "'.\n");
	if (!RE_NUM.exec(arr_date[yearValue])) return cal_error ("Invalid year value: '" + arr_date[yearValue] + "'.\nAllowed values are unsigned integers.");

	var dt_date = new Date();
	dt_date.setDate(1);

	if (arr_date[monthValue] < 1 || arr_date[monthValue] > 12) return cal_error ("Invalid month value: '" + arr_date[monthValue] + "'.\nAllowed range is 01-12.");
	dt_date.setMonth(arr_date[monthValue]-1);

	if (arr_date[yearValue] < 100) arr_date[yearValue] = Number(arr_date[yearValue]) + (arr_date[yearValue] < NUM_CENTYEAR ? 2000 : 1900);
	dt_date.setFullYear(arr_date[yearValue]);

	var dt_numdays = new Date(arr_date[yearValue], arr_date[monthValue], 0);
	dt_date.setDate(arr_date[dayValue]);
	if (dt_date.getMonth() != (arr_date[monthValue]-1)) return cal_error ("Invalid day of month value: '" + arr_date[dayValue] + "'.\nAllowed range is 01-"+dt_numdays.getDate()+".");

	return (dt_date)
}

// time parsing function
function cal_prs_time1 (str_time, dt_date) {

	if (!dt_date) return null;
	var arr_time = String(str_time ? str_time : '').split(':');

	if (!arr_time[0]) dt_date.setHours(0);
	else if (RE_NUM.exec(arr_time[0]))
		if (arr_time[0] < 24) dt_date.setHours(arr_time[0]);
		else return cal_error ("Invalid hours value: '" + arr_time[0] + "'.\nAllowed range is 00-23.");
	else return cal_error ("Invalid hours value: '" + arr_time[0] + "'.\nAllowed values are unsigned integers.");

	if (!arr_time[1]) dt_date.setMinutes(0);
	else if (RE_NUM.exec(arr_time[1]))
		if (arr_time[1] < 60) dt_date.setMinutes(arr_time[1]);
		else return cal_error ("Invalid minutes value: '" + arr_time[1] + "'.\nAllowed range is 00-59.");
	else return cal_error ("Invalid minutes value: '" + arr_time[1] + "'.\nAllowed values are unsigned integers.");

	if (!arr_time[2]) dt_date.setSeconds(0);
	else if (RE_NUM.exec(arr_time[2]))
		if (arr_time[2] < 60) dt_date.setSeconds(arr_time[2]);
		else return cal_error ("Invalid seconds value: '" + arr_time[2] + "'.\nAllowed range is 00-59.");
	else return cal_error ("Invalid seconds value: '" + arr_time[2] + "'.\nAllowed values are unsigned integers.");

	dt_date.setMilliseconds(0);
	return dt_date;
}

function cal_error (str_message) {
	alert (str_message);
	return null;
}

