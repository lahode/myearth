// Multiple file selector by Stickman -- http://www.the-stickman.com
// with thanks to: [for Safari fixes] Luis Torrefranca -- http://www.law.pitt.edu and Shawn Parker & John Pennypacker -- http://www.fuzzycoconut.com [for duplicate name bug] 'neal'
function MultiSelector( list_target, max ) {
  this.list_target = list_target;this.count = 0;
  this.id = 0;
  if( max ) {
    this.max = max;
  } else {
    this.max = -1;
  };

  this.addElement = function( element ) {
    if( element.tagName == 'INPUT' && element.type == 'file' ) {
      element.name = 'file_' + this.id++;
      element.multi_selector = this;element.onchange = function() {
        var new_element = document.createElement( 'input' );
        new_element.type = 'file';
        this.parentNode.insertBefore( new_element, this );
        this.multi_selector.addElement( new_element );
        this.multi_selector.addListRow( this );
        this.style.position = 'absolute';
        this.style.left = '-1000px';
      };
      if( this.max != -1 && this.count >= this.max ) {
        element.disabled = true;
      };
      this.count++;
      this.current_element = element;
    } else {
      alert( 'Error: not a file input element' );
    };
  };

  this.addListRow = function( element ) {
    var table = this.list_target;
    var new_row = table.insertRow(-1);
    new_row.element = element;
    new_row.setAttribute('class', 'sitetext');
    new_row.setAttribute('className', 'sitetext');
    var new_row_button = document.createElement( 'input' );
    new_row_button.type = 'image';
    new_row_button.src = '../lib/pics/admin/delete.gif';
    new_row_button.setAttribute('class', 'imgButton');
    new_row_button.setAttribute('className', 'imgButton');
    new_row_button.onclick= function(){
      this.parentNode.parentNode.element.parentNode.removeChild( this.parentNode.parentNode.element );
      this.parentNode.parentNode.parentNode.removeChild( this.parentNode.parentNode );
      this.parentNode.parentNode.element.multi_selector.count--;
      this.parentNode.parentNode.element.multi_selector.current_element.disabled = false;
      return false;
    };
    /*
    var new_row_secure = document.formulaire.createElement( 'input' );
    new_row_secure.type = 'checkbox';
    new_row_secure.name = 'chfdox';
    */
    var new_row_secure = document.createTextNode(' ');
    var cell1 = new_row.insertCell(-1);
    cell1.appendChild(document.createTextNode(element.value));
    cell1.style.textAlign = "left";
    var cell2 = new_row.insertCell(-1);
    cell2.appendChild(new_row_secure);
    cell2.style.textAlign = "center";
    var cell3 = new_row.insertCell(-1);
    cell3.appendChild(new_row_button);
    cell3.style.textAlign = "center";
  };
};