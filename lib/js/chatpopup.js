/*
 SCRIPT EDITE SUR L'EDITEUR JAVASCRIPT
 http://www.editeurjavascript.com
 */

 var slidepop_tempo;
 var slidepop_x = -260;
 var slidepop_ismoving = false;
 var slidepop_first = true;

 function write_popchat (texte, url) {
   texte = texte.replace("&#039;", "'")
   if (document.getElementById("popchat_slidepop_box")) {
     document.getElementsByTagName("body")[0].removeChild(document.getElementById("popchat_slidepop_box"));
   }
   slidepop_x = -260;
   slidepop_ismoving = false;
   slidepop_first = true;
   mainObj = document.getElementsByTagName("body")[0].appendChild(document.createElement("div"));
   mainObj.id = "popchat_slidepop_box";
   mainObj.name = "popchat_slidepop_box";
   alertObj = mainObj.appendChild(document.createElement("div"));
   alertObj.id = "popchat_slidepop_image";
   alertObj.name = "popchat_slidepop_image";
   alertObj.style.cursor = "pointer";
   alertObj.onclick = function() { document.location.href=url;return(false); }
   linkObj = alertObj.appendChild(document.createElement("span"));
   linkObj.appendChild(document.createTextNode(texte));
   cmdObj = mainObj.appendChild(document.createElement("div"));
   cmdObj.id = "popchat_slidepop_bouton";
   cmdObj.name = "popchat_slidepop_bouton";
   bt1Obj = cmdObj.appendChild(document.createElement("a"));
   bt1Obj.appendChild(document.createTextNode('>'));
   bt1Obj.appendChild(document.createElement('br'));
   bt1Obj.appendChild(document.createTextNode('<'));
   bt1Obj.href = "#";
   bt1Obj.onclick = function() { slidepop_start();this.blur();return(false); }
   bt1Obj.className = "popchat_slidepop_blanc";
   cmdObj.appendChild(document.createElement('br'));
   bt2Obj = cmdObj.appendChild(document.createElement("a"));
   bt2Obj.appendChild(document.createTextNode('x'));
   bt2Obj.href = "#";
   bt2Obj.onclick = function() { slidepop_close();return(false); }
   bt2Obj.className = "popchat_slidepop_blanc";
   slidepop_start();
 }


 function slidepop_start()
   {
   if(slidepop_x == -260 && !slidepop_ismoving)
     slidepop_deballe();
   else if(!slidepop_ismoving)
     slidepop_remballe();
   }

 function slidepop_deballe()
   {
   slidepop_ismoving = true;
   if(slidepop_x < 10)
     {
     slidepop_x += 5;
     slidepop_move();
     setTimeout("slidepop_deballe()", 10);
     }
   else
     {
     slidepop_ismoving = false;
     if(slidepop_first)
       slidepop_tempo = setTimeout("slidepop_start()", 5000);
     slidepop_first = false;
     }  
   }
 
 function slidepop_remballe()
   {
   clearTimeout(slidepop_tempo);
   slidepop_ismoving = true;
   if(slidepop_x > -260)
     {
     slidepop_x -= 5;
     slidepop_move();
     setTimeout("slidepop_remballe()", 10);
     }
   else
     slidepop_ismoving = false;
   }
 
 function slidepop_move()
   {
   if(document.getElementById)
     {
     document.getElementById("popchat_slidepop_box").style.left=slidepop_x+'px';
     }
   }
 
 function slidepop_close()
   {
   if(document.getElementById)
     {
     document.getElementById("popchat_slidepop_box").innerHTML = '';
     document.getElementById("popchat_slidepop_box").style.top = -100;
     document.getElementById("popchat_slidepop_box").style.left = -100;
     document.getElementById("popchat_slidepop_box").style.width = 1;
     document.getElementById("popchat_slidepop_box").style.height = 1;
     }
   }
