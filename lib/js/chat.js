  var refreshMessageTime = 1500;
  var refreshUserTime = 2500;
  var refreshCallTime = 3000;
  var requestMessage = null;
  var requestUser = null;
  var requestListen = null;
  var requestSendMessage = null;
  var requestCall = null;
  var requestQuitChat = null;
  var elementChatCall = null;
  var elementChatMessage = null;
  var elementChatUsers = null;
  var userChatName = null;
  var oldTempUsers = '';
  var oldTempMessages = '';
  var oldTempCalls = '';


  function getMessageChat() {
    if(requestMessage.readyState == 4) {
      if(requestMessage.status == 200) {
        try {
          response = requestMessage.responseXML;
          if (requestMessage.responseText == "<messages></messages>") {
            setTimeout("getMessageFromServer()", refreshMessageTime);
            return false;
          }
          var tempMsg = '';
          msgList = response.getElementsByTagName("message");
          if (msgList) {
            for (i=0;i<msgList.length;i++) {
              tempDate = new Date (parseInt(msgList[i].getAttribute("time")+'000'));
              if (tempDate.getHours()<10) tempHour = '0'+tempDate.getHours(); else tempHour = tempDate.getHours();
              if (tempDate.getMinutes()<10) tempMinutes = '0'+tempDate.getMinutes(); else tempMinutes = tempDate.getMinutes();
              dateFormated = tempHour+':'+tempMinutes;
              if (msgList[i].firstChild) resultMessage = msgList[i].firstChild.data; else resultMessage = '';
              if (msgList[i].getAttribute("user")=='#time#') {
                messageToDisplay = '<br /><span class="timeChat" style="font-weight:bold;">'+resultMessage+'</span>';
                userToDisplay = '';
              } else {
                messageToDisplay = '<span class="messageChat">'+resultMessage+'</span>';
                userToDisplay = '<span class="timeChat">'+dateFormated+'</span> <span class="userChat">['+msgList[i].getAttribute("user")+']</span>';
              }
              tempMsg += userToDisplay+messageToDisplay+"<br />";
            }
          }
          tempMsg = convertInnerHTML(tempMsg);
          if (tempMsg!=oldTempMessages && tempMsg!='') {
            elementChatMessage.innerHTML = tempMsg;
            elementChatMessage.scrollTop = elementChatMessage.scrollHeight;
            elementChatMessage.scrollTop = elementChatMessage.scrollHeight;
          }
          oldTempMessages = tempMsg;
        } catch(e) {
          setTimeout("getMessageFromServer()", refreshMessageTime);
          return false;
        }
      }
      setTimeout("getMessageFromServer()", refreshMessageTime);
    }
  }


  function getUsersChat() {
    if(requestUser.readyState == 4) {
      if(requestUser.status == 200) {
        try {
          response = requestUser.responseXML;
          if (requestUser.responseText == "<users></users>") {
            setTimeout("getUsersFromServer()", refreshUserTime);
            return false;
          }
          var tempUser = '';
          userList = response.getElementsByTagName("user");
          if (userList) {
            for (i=0;i<userList.length;i++) {
              userName = userList[i].getAttribute("name");
              if (userName == userChatName)
                tempUser += '<span class="userChat">'+userName+"</span><br />";
              else
                tempUser += '<span class="userChatSelected" onclick="call(\''+userName+'\')">'+userName+"</span><br />";
            }
          }
          if (tempUser!=oldTempUsers && tempUser!='') elementChatUsers.innerHTML = tempUser;
          oldTempUsers = tempUser;
        } catch(e) {
          setTimeout("getUsersFromServer()", refreshUserTime);
          return false;
        }
      }
      setTimeout("getUsersFromServer()", refreshUserTime);
    }
  }


  function getCallsChat() {
    if(requestListen.readyState == 4) {
      if(requestListen.status == 200) {
        try {
          responseReceived = requestListen.responseText;
          blinkText = false;
          if (responseReceived.charAt(0)=='#') {
            response = responseReceived.substr(1);
            responseHTML = '<span class="userChatSelected" id="blink" onclick="window.location.replace(\'chatclient.php\')">'+response+'</a>';
            blinkText = true;
          } else {
            response = responseReceived;
            responseHTML = response;
          }
          if (response!=oldTempCalls) {
            elementChatCall.innerHTML = responseHTML;
            if (blinkText) write_popchat (response, 'chatclient.php');
          }
          oldTempCalls = response;
        } catch(e) {
          setTimeout("getCallsFromServer()", refreshCallTime);
          return false;
        }
      }
      setTimeout("getCallsFromServer()", refreshCallTime);
    }
  }


  function getMessageFromServer(element) {
    if (element) elementChatMessage = element;
    requestMessage = httprequest();
    if (requestMessage) {
      requestMessage.onreadystatechange = getMessageChat;
      requestMessage.open("POST", "chatserver.php?action=getmessage", true);
      requestMessage.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      requestMessage.send(null);
    }
  }


  function getUsersFromServer(element, userChat) {
    if (element) elementChatUsers = element;
    if (userChat) userChatName = userChat;
    requestUser = httprequest();
    if (requestUser) {
      requestUser.onreadystatechange = getUsersChat;
      requestUser.open("POST", "chatserver.php?action=getusers", true);
      requestUser.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      requestUser.send(null);
    }
  }


  function getCallsFromServer(element) {
    if (element) elementChatCall = element;
    requestListen = httprequest();
    if (requestListen) {
      requestListen.onreadystatechange = getCallsChat;
      requestListen.open("POST", "chatserver.php?action=getcalls", true);
      requestListen.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      requestListen.send(null);
    }
  }


  function sendMessage(messagetoSend) {
    if (messagetoSend) {
      requestsendMessage = httprequest();
      if (requestsendMessage) {
        requestsendMessage.open("POST", "chatserver.php?action=sendmessage", true);
        requestsendMessage.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        requestsendMessage.send("message=" + encodeURIComponent(messagetoSend));
      }
    }
  }


  function call(userChat) {
    requestCall = httprequest();
    if (requestCall) {
      requestCall.open("POST", "chatserver.php?action=call", true);
      requestCall.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      requestCall.send("user=" + userChat);
    }
  }
