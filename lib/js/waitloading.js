  var progressbar1 = -50;
  var progressbar2 = -48;
  var progressbar3 = -44;
  var progressbar4 = -38;
  var progressbar5 = -30;
  var progressbar6 = -20;
  var progressbar7 = -12;
  var progressbar8 = -6;
  var progressbar9 = -2;
  var progress = true;
  var stopProgress = false;
  
  function displayLoading(message) {
    drawLoading(message);
    launchLoading();
  }

  function drawLoading(message) {
    mainObj = document.getElementsByTagName("body")[0].appendChild(document.createElement("div"));
    mainObj.id = "loading";
    mainObj.appendChild(document.createElement('br'));
    splitedtxt = message.split ("<br />");
    for (var i=0; i<splitedtxt.length; i++) {
      mainObj.appendChild(document.createTextNode(splitedtxt[i]));
      if (i<message.length) mainObj.appendChild(document.createElement('br'));
    }
    mainObj.appendChild(document.createElement('br'));
    mainObj.appendChild(document.createElement('br'));
    progressHObj = mainObj.appendChild(document.createElement("div"));
    progressHObj.id = "progressholder";
    progressWObj = progressHObj.appendChild(document.createElement("div"));
    progressWObj.id = "progresswn";
    progressB1Obj = progressWObj.appendChild(document.createElement("div"));
    progressB1Obj.id = "progressbar1";
    progressB1Obj.className = "progressbar";
    progressB1Obj.appendChild(document.createTextNode(' '));
    progressB2Obj = progressWObj.appendChild(document.createElement("div"));
    progressB2Obj.id = "progressbar2";
    progressB2Obj.className = "progressbar";
    progressB2Obj.appendChild(document.createTextNode(' '));
    progressB3Obj = progressWObj.appendChild(document.createElement("div"));
    progressB3Obj.id = "progressbar3";
    progressB3Obj.className = "progressbar";
    progressB3Obj.appendChild(document.createTextNode(' '));
    progressB4Obj = progressWObj.appendChild(document.createElement("div"));
    progressB4Obj.id = "progressbar4";
    progressB4Obj.className = "progressbar";
    progressB4Obj.appendChild(document.createTextNode(' '));
    progressB5Obj = progressWObj.appendChild(document.createElement("div"));
    progressB5Obj.id = "progressbar5";
    progressB5Obj.className = "progressbar";
    progressB5Obj.appendChild(document.createTextNode(' '));
    progressB6Obj = progressWObj.appendChild(document.createElement("div"));
    progressB6Obj.id = "progressbar6";
    progressB6Obj.className = "progressbar";
    progressB6Obj.appendChild(document.createTextNode(' '));
    progressB7Obj = progressWObj.appendChild(document.createElement("div"));
    progressB7Obj.id = "progressbar7";
    progressB7Obj.className = "progressbar";
    progressB7Obj.appendChild(document.createTextNode(' '));
    progressB8Obj = progressWObj.appendChild(document.createElement("div"));
    progressB8Obj.id = "progressbar8";
    progressB8Obj.className = "progressbar";
    progressB8Obj.appendChild(document.createTextNode(' '));
    progressB9Obj = progressWObj.appendChild(document.createElement("div"));
    progressB9Obj.id = "progressbar9";
    progressB9Obj.className = "progressbar";
    progressB9Obj.appendChild(document.createTextNode(' '));
    mainObj.appendChild(document.createElement('br'));
    mainObj.appendChild(document.createElement('br'));
    SelectVisible("hidden",document.getElementsByTagName('select'));
  }

  function launchLoading() {
    if (progressbar9 <= 300 && progress && !stopProgress) {
      document.getElementById("progressbar1").style.left=progressbar1+"px";
      document.getElementById("progressbar2").style.left=progressbar2+"px";
      document.getElementById("progressbar3").style.left=progressbar3+"px";
      document.getElementById("progressbar4").style.left=progressbar4+"px";
      document.getElementById("progressbar5").style.left=progressbar5+"px";
      document.getElementById("progressbar6").style.left=progressbar6+"px";
      document.getElementById("progressbar7").style.left=progressbar7+"px";
      document.getElementById("progressbar8").style.left=progressbar8+"px";
      document.getElementById("progressbar9").style.left=progressbar9+"px";
      var j=0;
      while (j<=100)
        j++;
        setTimeout("launchLoading();", 1);
        progressbar1=progressbar1+2;
        progressbar2=progressbar2+2;
        progressbar3=progressbar3+2;
        progressbar4=progressbar4+2;
        progressbar5=progressbar5+2;
        progressbar6=progressbar6+2;
        progressbar7=progressbar7+2;
        progressbar8=progressbar8+2;
        progressbar9=progressbar9+2;
    } else progress = false;
    if (progressbar9 >= -3 && !progress && !stopProgress) {
      document.getElementById("progressbar1").style.left=progressbar1+"px";
      document.getElementById("progressbar2").style.left=progressbar2+"px";
      document.getElementById("progressbar3").style.left=progressbar3+"px";
      document.getElementById("progressbar4").style.left=progressbar4+"px";
      document.getElementById("progressbar5").style.left=progressbar5+"px";
      document.getElementById("progressbar6").style.left=progressbar6+"px";
      document.getElementById("progressbar7").style.left=progressbar7+"px";
      document.getElementById("progressbar8").style.left=progressbar8+"px";
      document.getElementById("progressbar9").style.left=progressbar9+"px";
      var j=0;
      while (j<=100)
        j++;
        setTimeout("launchLoading();", 1);
        progressbar1=progressbar1-2;
        progressbar2=progressbar2-2;
        progressbar3=progressbar3-2;
        progressbar4=progressbar4-2;
        progressbar5=progressbar5-2;
        progressbar6=progressbar6-2;
        progressbar7=progressbar7-2;
        progressbar8=progressbar8-2;
        progressbar9=progressbar9-2;
    } else progress = true;
    if (!stopProgress && progress && progressbar9 < -2) launchLoading();
  }

  function SelectVisible(v,elem) {
	if (isIE||isIE5win)
		for (var i=0;i<elem.length;i++) elem[i].style.visibility=v;
  }