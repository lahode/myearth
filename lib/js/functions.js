    var IB=new Object;
    var posX=0;posY=0;
    var xOffset=10;yOffset=10;
    var newWin = null;
    var blinkItValue = 0;


    function blinkIt(name) {
      if (document.getElementById(name)) {
         s=document.getElementById(name);
         if (blinkItValue>2) {
           s.style.visibility='hidden';
           blinkItValue=0;
         } else {
           s.style.visibility='visible';
           blinkItValue++;
         }
      } else return;
    }


    function httprequest() {
      var xmlhttp=false;
      try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
      } catch (e) {
        try {
          xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {
         xmlhttp = false;
        }
      }
      if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
        try {
          xmlhttp = new XMLHttpRequest();
        } catch (e) {
          xmlhttp = false;
        }
      }
      return xmlhttp;
    }


    function utf8_encode (string) {
      string = string.replace(/\r\n/g,"\n");
      var utftext = "";
      for (var n = 0; n < string.length; n++) {
        var c = string.charCodeAt(n);
        if (c < 128) {
          utftext += String.fromCharCode(c);
        } else if((c > 127) && (c < 2048)) {
          utftext += String.fromCharCode((c >> 6) | 192);
          utftext += String.fromCharCode((c & 63) | 128);
        } else {
          utftext += String.fromCharCode((c >> 12) | 224);
          utftext += String.fromCharCode(((c >> 6) & 63) | 128);
          utftext += String.fromCharCode((c & 63) | 128);
        }
      }
      return utftext;
    }


    function utf8_decode (utftext) {
      var string = "";
      var i = 0;
      var c = c1 = c2 = 0;
      while ( i < utftext.length ) {
        c = utftext.charCodeAt(i);
        if (c < 128) {
          string += String.fromCharCode(c);
          i++;
        } else if((c > 191) && (c < 224)) {
          c2 = utftext.charCodeAt(i+1);
          string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
          i += 2;
        } else {
          c2 = utftext.charCodeAt(i+1);
          c3 = utftext.charCodeAt(i+2);
          string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
          i += 3;
        }
      }
      return string;
    }


    function closeWin(){
      if (newWin != null){
        if(!newWin.closed)
        newWin.close();
      }
    }//closeWin


    function openWin (strURL, width, height, left, top) {
      closeWin();
      var strOptions="";
      strOptions ="height="+height+", width="+width+", status=yes,resizable=no,top="+top+",left="+left+",dependent=yes,alwaysRaised=yes,location=no,menuBar=no";
      newWin = window.open(strURL, 'newWin', strOptions);
      newWin.focus();
    }//openWin


    function AffBulle(img) {
      contenu="<img src='"+img+"' onmouseout=\"HideBulle()\">";
      var finalPosX=posX-xOffset;
      if (finalPosX<0) finalPosX=0;
      if (document.layers) {
        document.layers["bulle"].document.write(contenu);
        document.layers["bulle"].document.close();
        document.layers["bulle"].top=posY-200+yOffset;
        document.layers["bulle"].left=finalPosX-200;
        document.layers["bulle"].visibility="show";
      }
      if (document.all) {
        bulle.innerHTML=contenu;
        document.all["bulle"].style.top=posY-200+yOffset;
        document.all["bulle"].style.left=finalPosX-200;
        document.all["bulle"].style.visibility="visible";
      }
      if (document.getElementById) {
        document.getElementById("bulle").innerHTML=contenu;
        document.getElementById("bulle").style.top=posY-200+yOffset;
        document.getElementById("bulle").style.left=finalPosX-200;
        document.getElementById("bulle").style.visibility="visible";
      }
    }//AffBulle


    function getMousePos(e) {
      if (document.all) {
        posX=event.x+document.body.scrollLeft;
        posY=event.y+document.body.scrollTop;
      }
      else {
        posX=e.pageX;
        posY=e.pageY;
      }
    }//getMousePos


    function HideBulle() {
      if (document.layers) {document.layers["bulle"].visibility="hide";}
      if (document.all) {document.all["bulle"].style.visibility="hidden";}
      if (document.getElementById){document.getElementById("bulle").style.visibility="hidden";}
    }//HideBulle


    function InitBulle(ColTexte,ColFond,ColContour,NbPixel) {
      IB.ColTexte=ColTexte;IB.ColFond=ColFond;IB.ColContour=ColContour;IB.NbPixel=NbPixel;
      if (document.layers) {
        window.captureEvents(Event.MOUSEMOVE);window.onMouseMove=getMousePos;
        document.write("<LAYER name='bulle' top=0 left=0 visibility='hide'></LAYER>");
      }
      if (document.all) {
        document.write("<DIV id='bulle' style='position:absolute;top:0;left:0;visibility:hidden'></DIV>");
        document.onmousemove=getMousePos;
      }
      else if (document.getElementById) {
        document.onmousemove=getMousePos;
        document.write("<DIV id='bulle' style='position:absolute;top:0;left:0;visibility:hidden'></DIV>");
      }
    }//InitBulle


    function nl2br(string) {
      return string.replace(/\n/g,'<br />');
    }//nl2br


    function findPosX(obj)
    {
      var curleft = 0;
      if (obj.offsetParent) {
        while (obj.offsetParent) {
          curleft += obj.offsetLeft
          obj = obj.offsetParent;
        }
      }
      else if (obj.x) {
        curleft += obj.x;
      }
      return curleft;
    }

    function findPosY(obj)
    {
      var curtop = 0;
      if (obj.offsetParent) {
        while (obj.offsetParent) {
          curtop += obj.offsetTop
          obj = obj.offsetParent;
        }
      }
      else if (obj.y) {
        curtop += obj.y;
      }
      return curtop;
    }

    function storeCaret (textEl) {
      if (textEl.createTextRange)
        textEl.caretPos = document.selection.createRange().duplicate();
    }

    function insertAtCaret (textEl, text) {
      if (textEl.createTextRange && textEl.caretPos) {
        var caretPos = textEl.caretPos;
        caretPos.text =
          caretPos.text.charAt(caretPos.text.length - 1) == ' ' ?
          text + ' ' : text;
      } else if (textEl.selectionStart || textEl.selectionStart == '0') {
        var startPos = textEl.selectionStart;
        var endPos = textEl.selectionEnd;
        textEl.value = textEl.value.substring(0, startPos) + text + textEl.value.substring(endPos, textEl.value.length);
      } else {
        textEl.value += text;
      }
    }

    function toggleContact(elements, name) {
      var i=0;
      while (elements[i]) {
        field=elements[i];
        if (field.name==name) {
          field.checked=!field.checked;
        }
        i++;
      }
    }

    function checkSelection (elements, name) {
      var i=0;
      while (elements[i]) {
        field=elements[i];
        if (field.name==name) {
          if (field.checked) {
            return true;
          }
        }
        i++;
      }
      return false;
    }
    
    function go(link) {
      document.location.href=link;
    }

    function drawLine(x1,y1,x2,y2,color,espacementPointille,divId)
    {
      if(espacementPointille<1) { espacementPointille=1; }

      //on calcule la longueur du segment
      var lg=Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));

      //on determine maintenant le nombre de points necessaires
      var nbPointCentraux=Math.ceil(lg/espacementPointille)-1;

      //stepX, stepY (distance entre deux points de pointill�s);
      var stepX=(x2-x1)/(nbPointCentraux+0);
      var stepY=(y2-y1)/(nbPointCentraux+0);

      //on recreer un point apres l'autre
      var strNewPoints='';
      for(var i=1 ; i<nbPointCentraux ; i++)
      {
        strNewPoints+='<div style=\"cursor:default;font-size:1px; z-index:2; width:2px; height:2px; background-color:'+color+'; position:absolute; top:'+Math.round(y1+i*stepY)+'px; left:'+Math.round(x1+i*stepX)+'px; \">&nbsp;</div>';
      }
      //pointe de depart
      strNewPoints+='<div style=\"cursor:default;font-size:1px; z-index:2; width:2px; height:2px; background-color:'+color+'; position:absolute; top:'+(y1-1)+'px; left:'+(x1-1)+'px; \">&nbsp;</div>';
      //point d'arrive
      strNewPoints+='<div style=\"cursor:default;font-size:1px; z-index:2; width:2px; height:2px; background-color:'+color+'; position:absolute; top:'+(y2-1)+'px; left:'+(x2-1)+'px; \">&nbsp;</div>';
      //on suprimme tous les points actuels et on mets les nouveaux div en place
      //obj container des points
      //var myContainer=document.getElementById(divId);

      //myContainer.innerHTML=strNewPoints;
      return (strNewPoints);
    }//drawLine


    function htmldecode(str) {
      str=str.replace(/\\&quot;/g, '"');
      str=str.replace(/\\&#039;/g, '\'');
      return str;
    }
    

    function getURLParam (strParamName) {
      var strReturn = "";
      var strHref = window.location.href;
      if ( strHref.indexOf("?") > -1 ) {
        var strQueryString = strHref.substr(strHref.indexOf("?")).toLowerCase();
        var aQueryString = strQueryString.split("&");
        for ( var iParam = 0; iParam < aQueryString.length; iParam++ ) {
          if (aQueryString[iParam].indexOf(strParamName + "=") > -1 ) {
            var aParam = aQueryString[iParam].split("=");
            strReturn = aParam[1];
            break;
          }
        }
      }
      return strReturn;
    }//getURLParam


    // D�place les �l�ments s�lectionn�es d'une liste � l'autre
    function moveSelectedOptions (from, to) {
      fromSelect = document.getElementsByName(from)[0];
      selOpt = getSelectedOptions (fromSelect);
      var selValues = new Array();
      if (selOpt.length > 0) {
        selValues = getSelectedValues (fromSelect);
        toSelect = document.getElementsByName (to)[0];
        for (i = 0;i < selOpt.length;i++) {
          option = selOpt[i];
          fromSelect.removeChild (option);
          toSelect.appendChild (option);
        }
      }
      return selValues;
    }//moveSelectedOptions


    // R�cup�re les valeurs s�lectionn�es dans une liste
    function getSelectedValues (select) {
      var selValues = new Array();
      for (j = 0;j < select.options.length;j++) {
        selValues[selValues.length] = select.options[j].value;
      }
      return selValues;
    }//getSelectedValues


    // R�cup�re les �l�ments s�lectionn�s dans une liste
    function getSelectedOptions (select) {
      var selOptions = new Array();
      for (m = 0;m < select.options.length;m++) {
        if (select.options[m].selected) {
          selOptions[selOptions.length] = select.options[m];
        }
      }
      return selOptions;
    }//getSelectedOptions


    // D�place un �l�ment au sein de la liste dans vers le sens voulu (sens = + pour monter, - pour descendre)
    function orderSelect(idNomListe, sens) {
      var objListe = document.getElementById(idNomListe);
      if (objListe.options.selectedIndex<0) return false;
      var objLigneAD�placer = new Option(objListe.options[objListe.options.selectedIndex].text, objListe.options[objListe.options.selectedIndex].value);
      var iPositionAvant = objListe.options.selectedIndex;
      var iPositionApres=(sens=="+")?iPositionAvant+1:iPositionAvant-1;
      if ((iPositionApres>=objListe.length)||(iPositionApres<0)) return false;
      var objLigneAChanger = new Option(objListe.options[iPositionApres].text, objListe.options[iPositionApres].value);
      objListe.options[iPositionAvant] = objLigneAChanger;
      objListe.options[iPositionApres] = objLigneAD�placer;
      objListe.options[iPositionApres].selected=true;
      objListe.focus();
    }//orderSelect


    // S�lectionne tous les �l�ments d'une liste
    function allOptionsSelect (nom_formulaire, id_liste) {
      // On compte le nombre d'item de la liste select
      obj=document.getElementById(id_liste);
      NbOption=obj.length;
      //NbOption = document.forms[liste_adresse].elements.liste.length;
      // On lance une boucle pour selectionner tous les items
      for(a=0; a < NbOption; a++){
        obj.options[a].selected = true;
        //document.forms[liste_adresse].elements.liste.options[a].selected = true;
      }
    }//allOptionsSelect
