<?php
  // http://www.phpcs.com/codes/EXEMPLE-IMAP-POUR-LECTURE-MAIL_13054.aspx
  // http://www.phpcs.com/codes/EXTRAIRE-PIECE-JOINTE-APRES-RECEPTION-EMAIL_36798.aspx
  // http://malida.org/wordpress/

  class MailReceiver {

    var $mediaManager;
    var $host;
    var $name;
    var $psw;
    var $attachmentFormat = array ('pdf' => 'application/pdf', 'ai' => 'application/postscript', 'eps' => 'application/postscript', 'ps' => 'application/postscript',
                                  'rtf' => 'application/rtf', 'dvi' => 'application/x-dvi', 'tar' => 'application/x-tar', 'man' => 'application/zip',
                                  'snd' => 'audio/basic', 'au' => 'audio/basic', 'aif' => 'audio/x-aiff', 'aiff' => 'audio/x-aiff',
                                  'aifc' => 'audio/x-aiff', 'wav' => 'audio/x-wav', 'jpeg' => 'image/jpeg', 'jpg' => 'image/jpeg',
                                  'jpe' => 'image/jpeg', 'tif' => 'image/tiff', 'tiff' => 'image/tiff', 'cmu' => 'image/x-cmu-raster',
                                  'pnm' => 'image/x-portable-anymap', 'pbm' => 'image/x-portable-bitmap', 'pgm' => 'image/x-portable-graymap', 'ppm' => 'image/x-portable-pixmap',
                                  'rgb' => 'image/x-rgb', 'xbm' => 'image/x-xbitmap', 'xpm' => 'image/x-xpixmap', 'zip' => 'multipart/x-zip',
                                  'gz' => 'multipart/x-gzip', 'gzip' => 'multipart/x-gzip', 'html' => 'text/html', 'htm' => 'text/html',
                                  'txt' => 'text/plain', 'rtx' => 'text/richtext', 'tsv' => 'text/tab-separated-value', 'etx' => 'text/x-setext',
                                  'mpeg' => 'video/mpeg', 'mpg' => 'video/mpeg', 'mpe' => 'video/mpeg', 'mov' => 'video/quicktime',
                                  'qt' => 'video/quicktime', 'avi' => 'video/msvideo', 'movie' => 'video/x-sgi-movie', '' => 'application/octet-stream');

    // Constructeur
    function __construct ($mediaManager, $host, $name, $psw, $quota, $quotaPath) {
      $this->mediaManager = $mediaManager;
      $this->host = $host;
      $this->name = $name;
      $this->psw = $psw;
      $this->quota = $quota;
      $this->quotaPath = $quotaPath;
    }
                                                      

    // Récupère la structure MIME
    function get_mime_type(&$structure) {
      $primary_mime_type = array("TEXT", "MULTIPART","MESSAGE", "APPLICATION", "AUDIO","IMAGE", "VIDEO", "OTHER");
      if ($structure->subtype) {
        return $primary_mime_type[(int) $structure->type] . '/' .$structure->subtype;
      }
      return "TEXT/PLAIN";
    }//get_mime_type


    // Récupère les différentes parties d'un corps du message
    function get_part($stream, $msg_number, $mime_type, $structure = false,$part_number = false) {
      if (!$structure) {
        $structure = imap_fetchstructure($stream, $msg_number);
      }
      if ($structure) {
        if ($mime_type == $this->get_mime_type($structure)) {
          if(!$part_number) {
            $part_number = "1";
          }
          $text = imap_fetchbody($stream, $msg_number, $part_number);
          if ($structure->encoding == 3) {
            return imap_base64($text);
          } else if ($structure->encoding == 4) {
            return imap_qprint($text);
          } else {
            return $text;
          }
        }
        if($structure->type == 1) {
          foreach($structure->parts as $index => $sub_structure) {
            $prefix = '';
            if ($part_number) {
              $prefix = $part_number . '.';
            }
            $data = $this->get_part($stream, $msg_number, $mime_type, $sub_structure, $prefix.($index + 1));
            if($data) {
              return $data;
            }
          }
        }
      }
      return false;
    }//get_part


    // Récupère les pièces jointes
    function getAttachments ($mail, $messageNumber, $attachToPics, $path) {
      $struct = imap_fetchstructure($mail, $messageNumber);
      $attachment = null;
      $nom_fichier = '';
      if ($struct->type > 0) {
        $nbrparts = !$struct->parts ? "1" : count($struct->parts);
        $piece = array();
        $j=0;
        for ($h=1;$h<=$nbrparts;$h++) {
          $part = $struct->parts[1];
          $piece = imap_fetchbody($mail, $messageNumber, $h+1);
          if ($part->encoding == "3") {
            $nbparam = count($part->parameters);
            $i=0;
            while ($i < $nbparam) {
              $i++;
              @$nom_fichier = utf8_decode(imap_utf8($struct->parts[$h]->dparameters[0]->value));
            }
            $piece = imap_base64($piece);
          }
          if($nom_fichier!=null) {
            $nom_fichier = checkFileName($nom_fichier);
            if (getFreeSpace($this->quota, $this->quotaPath)-$part->bytes > 0) {
              if ($attachToPics) {
                if (!$error=$this->mediaManager->saveImage ($piece, $path.$nom_fichier, '', TAILLE_IMG, 0, 300, 80, '')) {
                  $attachment[$j] = $nom_fichier;
                  $j++;
                }
              } else {
                $handle = fopen($path.$nom_fichier, "w");
                fwrite($handle, $piece);
                fclose($handle);
                @chmod ($path.$nom_fichier, 0777);
                $attachment[$j] = $nom_fichier;
                $j++;
              }
            } else {
              return "quotaExceeded";
            }
          }
        }
      }
      return $attachment;
    }//getAttachments


    // Récupère le corps du message
    function getBody ($mbox, $mid, $forceText=false) {
      if ($forceText) return array ($this->get_part($mbox, $mid, "TEXT/PLAIN"), false);
      $body = $this->get_part($mbox, $mid, "TEXT/HTML");
      if ($body <> "") return array ($body, true);
      $body = $this->get_part($mbox, $mid, "TEXT/PLAIN");
      if ($body <> "") return array ($body, false);
      return array ("", false);
    }//getBody


    // Retourne les mails présent sur le serveur avec leur sujet, date, nom et serveur d'emission
    function checkMails () {
      //$mbox = imap_open("{".gethostbyname($this->host).":110/pop3/notls}INBOX", $this->name, $this->psw);
      //return imap_num_msg($mbox);
    }

    // Retourne les mails présent sur le serveur avec leur sujet, date, nom et serveur d'emission
    function getMails ($users='', $delete=false, $attachToPics=false, $path, $timeMax=60, $emailsManager=null, $forceText=false) {
      //Tout d'abord, on ouvre une boite mail
      return;
	  $timeStart = time();
      $mbox = imap_open("{".gethostbyname($this->host).":110/pop3/notls}INBOX", $this->name, $this->psw);
      if ($mbox) {
        $headers = imap_headers ($mbox);
        // S'il existe des mails, on les récupère
        if ($headers) {
          $message = null;
          foreach($headers as $key => $val) {
            $header = imap_headerinfo($mbox, $key+1);
            if (!($header->subject=='' && $header->date=='' && $header->from[0]->mailbox=='' && $header->from[0]->host=='')) {
              $id = $header->message_id;
              if (!$emailsManager || ($emailsManager && !$emailsManager->getEmail ($id))) {
                $date = $header->udate;
                $subject = $header->subject;
                $subject = imap_utf8 ($subject);
                $subject = utf8_decode ($subject);
                $name = $header->from[0]->mailbox;
                $host = $header->from[0]->host;
                $from = $name.'@'.$host;
                if ($header->from[0]->personal<>'') $personal = utf8_decode(imap_utf8($header->from[0]->personal)); else $personal = $from;
                $to = array();
                $cc = array();
                $bcc = array();
                if ($header->to && count ($header->to) > 0)
                  foreach ($header->to as $toItem) {
                    $to[] = $toItem->mailbox.'@'.$toItem->host;
                  }
                if ($header->cc && count ($header->cc) > 0)
                  foreach ($header->cc as $ccItem) {
                    $cc[] = $ccItem->mailbox.'@'.$ccItem->host;
                  }
                if ($header->bcc && count ($header->bcc) > 0)
                  foreach ($header->bcc as $bccItem) {
                    $bcc[] = $bccItem->mailbox.'@'.$bccItem->host;
                  }
                if (!$users) {
                  $resultBody = $this->getBody ($mbox, $key+1, $forceText);
                  $body = $resultBody[0];
                  $isHtml = $resultBody[1];
                  $body = imap_utf8 ($body);
                  $split = explode ('________________________________________', $body);
                  $body = $split[0];
                  $attachments = $this->getAttachments ($mbox, $key+1, $attachToPics, $path);
                  if ($attachments && is_string ($attachments)) return $attachments;
                  $message[$key] = new Mail ($id, $date, $from, $to, $cc, $bcc, $host, $subject, $body, $attachments, $personal);
                  if ($emailsManager) {
                    $tempMessage = $message[$key];
                    if ($tempMessage->attachments && count ($tempMessage->attachments) > 0) {
                      foreach ($tempMessage->attachments as $keyAttachedFiles => $attachedFiles) {
                        $tempMessage->attachments[$keyAttachedFiles] = new Attachment (genID (), $attachedFiles, $attachedFiles);
                      }
                    }
                    $emailsManager->editEmail ($tempMessage, 'in', $isHtml);
                  }
                  if ($delete) imap_delete($mbox, $key);
                } else {
                  $t = 0;
                  do {
                    if ($users[$t] == $name.'@'.$host) {
                      $resultBody = $this->getBody ($mbox, $key+1, $forceText);
                      $body = $resultBody[0];
                      $isHtml = $resultBody[1];
                      $body = imap_utf8 ($body);
                      $split = explode ('________________________________________', $body);
                      $body = $split[0];
                      $attachments = $this->getAttachments ($mbox, $key+1, $attachToPics, $path);
                      if ($attachments && is_string ($attachments)) return $attachments;
                      $message[$key] = new Mail ($id, $date, $from, $to, $cc, $bcc, $host, $subject, $body, $attachments, $personal);
                      if ($emailsManager) {
                        $tempMessage = $message[$key];
                        if ($tempMessage->attachments && count ($tempMessage->attachments) > 0) {
                          foreach ($tempMessage->attachments as $keyAttachedFiles => $attachedFiles) {
                            $tempMessage->attachments[$keyAttachedFiles] = new Attachment (genID (), $attachedFiles, $attachedFiles);
                          }
                        }
                        $emailsManager->editEmail ($tempMessage, 'in', $isHtml);
                      }
                      if ($delete) imap_delete($mbox, $key+1);
                    }
                    $t++;
                  } while (($t<count($users)) && ($users[$t-1] != $name.'@'.$host));
                }
                if (time() >= $timeStart+$timeMax) {
                  imap_expunge($mbox);
                  imap_close($mbox);
                  return "timeExceed";
                }
              }
            }
          }
          $sort = array();
          if ($message) {
            foreach ($message as $keyMsg => $msg) {
              $sort[$keyMsg]  = addslashes($msg->date);
            }
            array_multisort($sort, SORT_DESC, $message);
          }
          imap_expunge($mbox);
          imap_close($mbox);
          return $message;
        } else {
          imap_close($mbox);
          return false;
        }
      } else {
        return false;
      }
    }//getMails


  }//class MailReceiver

?>
