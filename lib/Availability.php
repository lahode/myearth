<?php

  // Cette classe permet de gérer les disponibilité du calendrier pour l'insertion 
  // d'une nouvelle destination ou d'un nouveau pays (menu Destination)
  class Availability {

    var $routeManager;

    // Constructeur
    function __construct ($routeManager) {
      $this->routeManager = $routeManager;
    }

    // Récupère les dates libres pour un pays
    function getCategoryAvailability ($id) {
      // Initialisation des données
      $i = 0;
      $key = 0;
      $lastKeyCat = -1;
      $destinations = null;
      $dateDestination = array();
      $occDates = null;
      $current_dests = array();
      $lastDestination = null;

      // Récupère les destinations de la catégories présente
      $destinations = $this->routeManager->getDestinations ($id);

      // Récupère toutes les catégories
      $categories = $this->routeManager->getCategories ();
      $categories = sortObjBy ($categories, 'startingDate', SORT_ASC);

      // Initialise la date de début et de fin
      $firstDate = $this->routeManager->getDeparture ()->startingDate;
      $lastDate = $this->routeManager->getDeparture ()->endingDate - (3600*24);

      if (!$firstDate) {
        $firstDate = 1154383200;
      }
      if (!$lastDate) {
        $lastDate = 1924902000;
      }

      if ($categories) {
        if ($destinations && count ($destinations) > 0) {
          // Vérifie la position de la catégorie dernière
          while ($key < count($categories) && $id <> $categories[$key]->id) {
            $lastKeyCat = $key;
            $key++;
          }
          if ($lastKeyCat > -1) {
            $lastDestination = $this->routeManager->getFirstLastDestination($categories[$lastKeyCat]->id, true);
            if ($lastDestination) {
              $firstDate = ($lastDestination->startingDate+$lastDestination->nbDays*3600*24);
            } else {
              $firstDate = ($categories[$lastKeyCat]->startingDate+3600*24);
            }
          }
          $lastDate = $destinations[0]->startingDate+(3600*24);
        } else {
          $categories = sortObjBy ($categories, 'startingDate', SORT_ASC);
          foreach ($categories as $keyCat => $itemCat) {
            $occDates['first'][$i] = $itemCat->startingDate.'000';
            $lastDestination = $this->routeManager->getFirstLastDestination($itemCat->id, true);
            if ($lastDestination) {
              $occDates['last'][$i] = ($lastDestination->startingDate+$lastDestination->nbDays*3600*24).'000';
            } else {
              $occDates['last'][$i] = $occDates['first'][$i];
            }
            if ($firstDate >= substr($occDates['first'][$i], 0, -3) && $firstDate <= substr($occDates['last'][$i], 0, -3)) {
              $firstDate = substr($occDates['last'][$i], 0, -3)+3600*24;
            }
            $i++;
          }
        }
      }

      $availability['borderDates'] = $occDates;
      $availability['occupedDates'] = null;
      $availability['firstDate'] = $firstDate.'000';
      $availability['lastDate'] = $lastDate.'000';
      return $availability;
    }//getCategoryAvailability


    // Récupère les dates libres pour une destination
    function getDestinationAvailability ($catId, $id, $allowsametime=0) {
      // Initialisation des données
      $occDates = null;

      // Récupère la catégorie associée à la destination
      $cat = $this->routeManager->getCategoryBy ('id', $catId);

      // Récupère toutes les catégories
      $categories = $this->routeManager->getCategories ();
      $categories = sortObjBy ($categories, 'startingDate', SORT_ASC);

      // Initialise la date de début
      $firstDate = $cat->startingDate;

      // Vérifie la position de la catégorie
      $key = -1;
      foreach ($categories as $keyCat => $itemCat) {
        if ($catId == $itemCat->id) $key = $keyCat;
      }

      // Initialise la date de fin
      if ($key >=0 && isset($categories[$key+1])) {
        $lastDate = $categories[$key+1]->startingDate;
      } else {
        $lastDate = $this->routeManager->getDeparture ()->endingDate;
      }
      if (!$lastDate) {
        $lastDate = 1924902000;
      }

      // Récupère toutes les destinations et trie par catégorie en conservant uniquement la date de départ et le nb de jours
      $destinations = $this->routeManager->getDestinations ($catId);
      if (count ($destinations) > 0) {
        $destinations = sortObjBy ($destinations, 'categories', SORT_ASC);
        foreach ($destinations as $keyDate => $itemDest) {
          $dateDestination[$itemDest->category][$keyDate] = new DateDestination ($itemDest->id, $itemDest->startingDate, $itemDest->nbDays);
        }

        // Récupère les dates occupées
        $dest = sortObjBy ($dateDestination[$catId], 'startingDate', SORT_ASC);
        if (count ($dest) > 0 && $allowsametime==0) {
          $j=0;
          foreach ($dest as $keyDestination => $itemDestination) {
            for ($i=0;$i<$itemDestination->nbDays;$i++) {
              if ($itemDestination->id != $id) {
                $occDates[$j] = ($itemDestination->startingDate + ($i*3600*24)).'000';
                $tempoccDate = $itemDestination->startingDate + ($i*3600*24);
                // Vérifie que la date de départ ne soit pas déjà occupée
                if ($firstDate-3600 <= $tempoccDate && $firstDate+3600 >= $tempoccDate) {
                  $firstDate = $firstDate + (24*3600);
                }
                $j++;
              }
            }
          }
          if ($occDates) sort ($occDates);
        }
      }

      $availability['borderDates'] = null;
      $availability['occupedDates'] = $occDates;
      $availability['firstDate'] = $firstDate.'000';
      $lastCatDate = $lastDate;
      $availability['lastDate'] = $lastDate.'000';
      $availability['lastCatDate'] = $lastCatDate.'000';
      return $availability;
    }


    // Retourne le nombre de jours disponible à partir de la date choisie
    function getNbDays ($dateReceive, $availability) {
      $lastDate = substr($availability['lastDate'], 0, -3);
      $j=0;
      $nbDays=-1;
      $untilDate=-1;
      if (count ($availability['occupedDates']) > 0) {
        sort ($availability['occupedDates']);
        while ($j < count($availability['occupedDates']) && $untilDate < 0) {
          if (substr ($availability['occupedDates'][$j], 0, -3) > $dateReceive) {
            $untilDate = substr ($availability['occupedDates'][$j], 0, -3);
          }
          $j++;
        }
      }
      if ($untilDate <= 0) {
        if ($lastDate > 0) {
          $untilDate = $lastDate+(3600*24);
        } else {
          $j = 365;
        }
      }
      $j=0;
      while ($nbDays < 0 && $j < 365) {
        if ($dateReceive >= $untilDate) {
          $nbDays = $j;
        }
        $dateReceive = $dateReceive + (3600*24);
        $j++;
      }
      if ($j == 365) $nbDays = $j;
      return $nbDays;
    }//getNbDays

  }//class Availability

?>
