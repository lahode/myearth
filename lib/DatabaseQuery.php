<?php
  include_once ('../lib/interfacemysql.php');

  // Gestion des requête à la base de données
  class DatabaseQuery {

    var $bdName;
    var $mysql;

    // Constructeur
    function __construct ($bdHost, $bdUser, $bdPsw, $bdName) {
      $this->mysql = new InterfaceMysqli($bdHost, $bdUser, $bdPsw);
      $this->bdName = $bdName;
    }//DatabaseQuery


    // Genère un identifiant unique
    function genID () {
      return md5(uniqid(rand(), true));
    }//genID


    // Efface un fichier
    function deleteFile ($fichier) {
      $succeed = false;
      if (file_exists($fichier)) {
        $succeed = @unlink ($fichier);
      }
      return $succeed;
    }//deleteFile


    // Récupère le nom d'un pays
    function getCountryName ($country) {
      $name = "";
      if ($country<>'') {
        if ($this->mysql->TblExist('countries')) {
          $condition = "WHERE iso2='".$country."'";
          $query = $this->mysql->Query("SELECT name FROM `countries` ".$condition);
          if ($query) {
            $name = convertFromUTF8($query->fetch_object()->name);
          }
        }
      }
      return $name;
    }//getCountryName


    // Récupère la monnaie d'un pays
    function getCountryCurrency ($country) {
      $name = "";
      if ($country<>'') {
        if ($this->mysql->TblExist('countries')) {
          $condition = "WHERE iso2='".$country."'";
          $query = $this->mysql->Query("SELECT currency FROM `countries` ".$condition);
          if ($query) {
            $name = convertFromUTF8($query->fetch_object()->currency);
          }
        }
      }
      return $name;
    }//getCountryCurrency


    // Récupère le nom d'une r�gion
    function getRegionName ($country, $region) {
      $name = "";
      if ($country<>'' && $region<>'') {
        if ($this->mysql->TblExist($country)) {
          $condition = "WHERE (admin='".$region."' AND type='ADM1')";
          $query = $this->mysql->Query("SELECT name FROM `".$country."` ".$condition);
          if ($query) {
            $name = convertFromUTF8($query->fetch_object()->name);
          }
        }
      }
      return $name;
    }//getRegionName


    // Récupère le nom d'un ville
    function getCityName ($country, $city) {
      $name = "";
      if ($country<>'' && $city<>'') {
        if ($this->mysql->TblExist($country)) {
          $condition = "WHERE id='".$city."'";
          $query = $this->mysql->Query("SELECT name FROM `".$country."` ".$condition);
          if ($query) {
            $name = convertFromUTF8($query->fetch_object()->name);
          }
        }
      }
      return $name;
    }//getCityName


    // Récupère toutes les régions d'un pays
    function getAllCountries () {
      $tableCountry = null;
      if ($this->mysql->TblExist('countries')) {
        $condition = "ORDER BY name";
        $tableauCountries = $this->mysql->Query("SELECT * FROM `countries` ".$condition);
        if ($tableauCountries) {
          $i=0;
          while($entree = $tableauCountries->fetch_object()) {
            $tableCountry[$i] = $entree;
            $i++;
          }
        }
      }
      return $tableCountry;
    }//getAllCountries


    // Récupère toutes les régions d'un pays
    function getRegionsFromCountry ($country) {
      $tableRegion = null;
      if ($country<>'') {
        $condition = "WHERE type='ADM1' ORDER BY name";
        $tableauRegion = array();
        if ($this->mysql->TblExist($country)) {
          $tableauRegion = $this->mysql->Query("SELECT * FROM `".$country."` ".$condition);
        }
        if ($tableauRegion) {
          $i=0;
          while($entree = $tableauRegion->fetch_object()) {
            $tableRegion[$i] = $entree;
            $i++;
          }
        }
      }
      return $tableRegion;
    }//getRegionsFromCountry


    // Récupère toutes les villes d'une région d'un pays
    function getCitiesFromRegion ($country, $region) {
      $tableCity=null;
      if ($country<>'' && $region<>'') {
        $condition = "WHERE (admin=".$region." AND type<>'ADM1')ORDER BY name";
        $tableauCity = array();
        if ($this->mysql->TblExist($country)) {
          $tableauCity = $this->mysql->Query("SELECT * FROM `".$country."` ".$condition);
        }
        if ($tableauCity) {
          $i=0;
          while($entree = $tableauCity->fetch_object()) {
            $tableCity[$i] = $entree;
            $i++;
          }
        }
      }
      return $tableCity;
    }//getCitiesFromRegion


    // Récupère les villes principale d'un pays
    function getMainCitiesFromCountry ($country) {
      $tableCity=null;
      if ($country<>'') {
        $condition = "WHERE (type<>'ADM1' AND (type='PPLA' OR type='PPLB' OR type='PPLC')) ORDER BY name";
        $tableauCity = array();
        if ($this->mysql->TblExist($country)) {
          $tableauCity = $this->mysql->Query("SELECT * FROM `".$country."` ".$condition);
        }
        if ($tableauCity) {
          $i=0;
          while($entree = $tableauCity->fetch_object()) {
            $tableCity[$i] = $entree;
            $i++;
          }
        }
      }
      return $tableCity;
    }//getCitiesFromRegion


    // Recherche une ville dans un pays
    function searchCityFromCountry ($citySearch, $country) {
      $region = "";
      $city = "";
      if ($country<>'' && $citySearch<>'') {
        $condition = "WHERE name='".$citySearch."'";
        if ($this->mysql->TblExist($country)) {
          $cityFound = $this->mysql->Query("SELECT id, name, admin FROM `".$country."` ".$condition);
          if ($cityFound) {
            $result = $cityFound->fetch_object();
            $region = convertFromUTF8($result->admin);
            $city = convertFromUTF8($result->id);
          }
        }
      }
      return array ('region'=>$region, 'city'=>$city);
    }//searchCityFromCountry


    // Récupère le décalage horaire du pays
    function getCountryTime ($country) {
      $gmt = 0;
      if ($country<>'') {
        $conditionCountry = "WHERE (iso2='".$country."')";
        if ($this->mysql->TblExist('countries')) {
          $queryCountry = $this->mysql->Query("SELECT gmt FROM `countries` ".$conditionCountry);
          if ($queryCountry && $this->mysql->TblExist($country)) {
            $resultGmt = $queryCountry->fetch_object()->gmt;
            $gmt = $this->getCorrectGMT ($resultGmt);
          }
        }
      }
      return $gmt;
    }//getCountryTime


    // Récupère le décalage horaire de la r�gion
    function getRegionTime ($country, $region) {
      $gmt = 0;
      if ($country<>'' && $region<>'') {
        $conditionRegion = "WHERE (name='".convertToUTF8($region)."' AND type='ADM1')";
        if ($this->mysql->TblExist($country)) {
          $queryCountry = $this->mysql->Query("SELECT gmt FROM `".$country."` ".$conditionRegion);
          if ($queryCountry && $this->mysql->TblExist($country)) {
            $resultGmt = $queryCountry->fetch_object()->gmt;
            $gmt = $this->getCorrectGMT ($resultGmt);
          }
        }
      }
      return $gmt;
    }//getRegionTime


    // Converti les données reçue en heure de décalage correcte
    function getCorrectGMT ($gmt) {
      $result = 0;
      $tabGmt = explode("#", $gmt);
      if (count ($tabGmt) == 7) {
        $lastDayWinter = date ('d', mktime(0, 0, 0, $tabGmt[2]+1, 0));
        $lastDaySummer = date ('d', mktime(0, 0, 0, $tabGmt[5]+1, 0));
        $w = 0;
        for ($i=1;$i<$lastDayWinter;$i++) {
          $summerDate = mktime ($tabGmt[6], 0, 0, $tabGmt[2], $i, date ('Y'));
          if (date ('w', $summerDate) == 0) {
            if ($tabGmt[1] == 5) $dateofSummer = $summerDate;
            elseif ($w == $tabGmt[1]-1) $dateofSummer = $summerDate;
            $w++;
          }
        }
        $s = 0;
        for ($i=1;$i<$lastDaySummer;$i++) {
          $winterDate = mktime ($tabGmt[6], 0, 0, $tabGmt[5], $i, date ('Y'));
          if (date ('w', $winterDate) == 0) {
            if ($tabGmt[4] == 5) $dateofWinter = $winterDate;
            elseif ($s == $tabGmt[1]-1) $dateofWinter = $winterDate;
            $s++;
          }
        }
        if ($dateofSummer < $dateofWinter) {
          if (time() > $dateofSummer && time() < $dateofWinter) $result = $tabGmt[3];
          else $result = $tabGmt[0];
        } else {
          if (time() > $dateofWinter && time() < $dateofSummer) $result = $tabGmt[0];
          else $result = $tabGmt[3];
        }
      } elseif (!is_null($tabGmt) && count ($tabGmt) == 1 && $tabGmt[0] <> '') $result = $gmt;
      return $result;
    }//getCorrectGMT


    // Récupère les détails d'un pays
    function getCountryDetails ($country) {
      $result = null;
      $capitaleName = "";
      if ($country<>'') {
        $conditionCountry = "WHERE (iso2='".$country."')";
        if ($this->mysql->TblExist('countries')) {
          $queryCountry = $this->mysql->Query("SELECT name, surface, population, continent, gmt FROM `countries` ".$conditionCountry);
          if ($queryCountry && $this->mysql->TblExist($country)) {
            $resultCountry = $queryCountry->fetch_object();
            $conditionCapitale = "WHERE type='PPLC' ORDER BY name";
            $queryCapitale = $this->mysql->Query("SELECT name FROM `".$country."` ".$conditionCapitale);
            if ($queryCapitale) $capitaleName = $queryCapitale->fetch_object()->name;
            $result = new LocationDetails (convertFromUTF8 ($resultCountry->name), "", "",
                                           convertFromUTF8 ($resultCountry->population), convertFromUTF8($capitaleName),
                                           "", convertFromUTF8 ($resultCountry->surface), convertFromUTF8 ($resultCountry->continent));
          }
        }
      }
      return $result;
    }//getCountryDetails


    // Récupère les détails d'une région d'un pays
    function getRegionDetails ($country, $region) {
      $result = null;
      $capitaleName = "";
      if ($country<>'' && $region<>'') {
        $conditionRegion = "WHERE (admin='".$region."' AND type='ADM1')";
        if ($this->mysql->TblExist($country)) {
          $queryRegion = $this->mysql->Query("SELECT name, lattitude, longitude, admin, population FROM `".$country."` ".$conditionRegion);
          if ($queryRegion) {
            $resultRegion = $queryRegion->fetch_object();
            $administration = convertFromUTF8 ($resultRegion->admin);
            $conditionCapitale = "WHERE admin='".$administration."' AND (type='PPLA' || type='PPLC') ORDER BY name";
            $queryCapitale = $this->mysql->Query("SELECT name FROM `".$country."` ".$conditionCapitale);
            if ($queryCapitale) $capitaleName = $queryCapitale->fetch_object()->name;
            $result = new LocationDetails (convertFromUTF8 ($resultRegion->name), convertFromUTF8 ($resultRegion->lattitude),
                                           convertFromUTF8 ($resultRegion->longitude), convertFromUTF8 ($resultRegion->population),
                                           convertFromUTF8($capitaleName), "", "", "");
          }
        }
      }
      return $result;
    }//getRegionDetails


    // Récupère les détails d'une ville d'un pays
    function getCityDetails ($country, $city) {
      $result = null;
      if ($country<>'' && $city<>'') {
        $conditionCity = "WHERE (id='".$city."')";
        if ($this->mysql->TblExist($country)) {
          $queryCity = $this->mysql->Query("SELECT name, lattitude, longitude, population, altitude FROM `".$country."` ".$conditionCity);
          if ($queryCity) {
            $resultCity = $queryCity->fetch_object();
            $result = new LocationDetails (convertFromUTF8 ($resultCity->name), convertFromUTF8 ($resultCity->lattitude),
                                           convertFromUTF8 ($resultCity->longitude), convertFromUTF8 ($resultCity->population),
                                           "", convertFromUTF8 ($resultCity->altitude), "", "");
          }
        }
      }
      return $result;
    }//getCityDetails


    // Récupère la description d'une région d'un pays
    function getDescription ($country, $region, $language) {
      $name = "";
      if ($region=='') $region=0;
      if ($country<>'') {
        if ($this->mysql->TblExist('descriptions')) {
          $condition = "WHERE (country='".$country."' AND region='".$region."')";
          $query = $this->mysql->Query("SELECT ".$language." FROM `descriptions` ".$condition);
          if ($query) {
            $name = convertFromUTF8($query->fetch_object()->$language);
          }
        }
      }
      return $name;
    }//getDescription


    // Récupère les coordonnées extrême d'un pays
    function getExtremityCountry ($country) {
      if ($country<>'') {
        if ($this->mysql->TblExist($country)) {
          $queryMinLat = $this->mysql->Query("SELECT MIN(lattitude) as result FROM `".$country."`");
          $queryMaxLat = $this->mysql->Query("SELECT MAX(lattitude) as result FROM `".$country."`");
          $queryMinLong = $this->mysql->Query("SELECT MIN(longitude) as result FROM `".$country."`");
          $queryMaxLong = $this->mysql->Query("SELECT MAX(longitude) as result FROM `".$country."`");
          if ($queryMinLat && $queryMaxLat && $queryMinLong && $queryMaxLong)
            return array ('minlat' => $queryMinLat->fetch_object()->result, 'maxlat' => $queryMaxLat->fetch_object()->result,
                          'minlong' => $queryMinLong->fetch_object()->result, 'maxlong' => $queryMaxLong->fetch_object()->result);
        }
      }
    }//getExtremityCountry


    // Récupère le mot de passe de l'invité
    function getGuestPsw ($album) {
      $password = "";
      if ($album<>'') {
        $conditionUser = "WHERE (type<>'admin' AND album='".$album."')";
        if ($this->mysql->TblExist('users')) {
          $queryUser = $this->mysql->Query("SELECT psw FROM `users` ".$conditionUser);
          if ($queryUser) $password = $queryUser->fetch_object()->psw;
        }
      }
      return $password;
    }//getGuestPsw


    // Vérifie l'accès de l'administrateur
    function checkAdminPsw ($username, $psw, $album) {
      $access = false;
      if ($album<>'') {
        $conditionAdmin = "WHERE (login='".$username."' AND psw='".md5($psw)."' AND type='admin' AND album='".$album."')";
        if ($this->mysql->TblExist('users')) {
          $queryAdmin = $this->mysql->Query("SELECT album FROM `users` ".$conditionAdmin);
          if ($queryAdmin) $access=true;
        }
      }
      return $access;
    }//checkAdminPsw


    // Vérifie l'accès du super administrateur
    function checkSuperAdminPsw ($username, $psw) {
      $access = false;
      $conditionSuperAdmin = "WHERE (login='".$username."' AND psw='".md5($psw)."' AND type='spadm')";
      if ($this->mysql->TblExist('users')) {
        $querySuperAdmin = $this->mysql->Query("SELECT album FROM `users` ".$conditionSuperAdmin);
        if ($querySuperAdmin) $access=true;
      }
      return $access;
    }//checkSuperAdminPsw


    // Vérifie si le nom d'utilisateur correspond à un super administrateur
    function isSuperAdmin ($username) {
      $superadmin = "";
      $conditionSuperAdmin = "WHERE (login='".$username."' AND type='spadm')";
      if ($this->mysql->TblExist('users')) {
        $querySuperAdmin = $this->mysql->Query("SELECT login FROM `users` ".$conditionSuperAdmin);
        if ($querySuperAdmin) $superadmin=$querySuperAdmin->fetch_object()->login;
      }
      return $superadmin;
    }//isSuperAdmin


    // Crée un code d'accès pour l'administrateur
    function initAdminAccess () {
      if ($this->mysql->TblExist('access')) {
        do {
          $accessCode = $this->genID();
          $query = $this->mysql->Query("SELECT access FROM `access` WHERE (access='".$accessCode."')");
        } while ($query);
        $this->mysql->Query("INSERT INTO `access` (access, timestamp) VALUES ('".md5($accessCode)."', ".time().")");
      }
      return $accessCode;
    }//initAdminAccess


    // Met à jour le temps d'accès pour l'administrateur
    function updateAdminAccess ($accessCode) {
      if ($this->mysql->TblExist('access')) {
        $this->mysql->Query("UPDATE access SET timestamp=".time()." WHERE (access='".md5($accessCode)."')");
      }
      return $accessCode;
    }//updateAdminAccess


    // Nettoie les anciens codes d'accès administrateur
    function cleanAdminAccess () {
      if ($this->mysql->TblExist('access')) {
        $this->mysql->Query("DELETE FROM access WHERE (timestamp<".(time()-1200).")");
      }
    }//cleanAdminAccess


    // Vérifie le code d'accès de l'administrateur
    function checkAdminAccess ($adminCode) {
      $accessEntry = false;
      if ($adminCode<>'') {
        $conditionAdminAccess = "WHERE (access='".md5($adminCode)."')";
        if ($this->mysql->TblExist('access')) {
          $queryAdminAccess = $this->mysql->Query("SELECT * FROM `access` ".$conditionAdminAccess);
          if ($queryAdminAccess) {
            $accessEntry = true;
            $this->updateAdminAccess($adminCode);
          }
          $this->cleanAdminAccess();
        }
      }
      return $accessEntry;
    }//checkAdminAccess


    // Vérifie si le nom d'utilisateur correspond à un administrateur ou au super administrateur
    function forbiddenLogin ($album, $username) {
      $forbiddenEntry = false;
      if ($album<>'' && $username<>'') {
        $conditionAdmin = "WHERE (login='".$username."' AND ((album='".$album."' AND type='admin') OR type='spadm'))";
        $forbiddenEntry = $this->mysql->Query("SELECT album FROM `users` ".$conditionAdmin);
      }
      return $forbiddenEntry;
    }//forbiddenLogin


    // Modifie le mot de passe des invités
    function changeGuestPsw ($psw, $album) {
      $queryUser = null;
      if ($album<>'') {
        $conditionUser = "UPDATE users SET psw='".$psw."' WHERE (type='guest' AND album='".$album."')";
        if ($this->mysql->TblExist('users')) {
          $queryUser = $this->mysql->Query($conditionUser);
        }
      }
      return $queryUser;
    }//changeGuestPsw


    // Modifie le mot de passe de l'administrateur
    function changeAdminPsw ($psw, $album) {
      $queryUser = null;
      if ($psw<>'' && $album<>'') {
        $conditionUser = "UPDATE users SET psw='".md5($psw)."' WHERE (type='admin' AND album='".$album."')";
        if ($this->mysql->TblExist('users')) {
          $queryUser = $this->mysql->Query($conditionUser);
        }
      }
      return $queryUser;
    }//changeAdminPsw


    // Ajoute le fichier temporaire et retourne son identifiant
    function addTempFile ($fileName) {
      if ($this->mysql->TblExist('tempfiles')) {
        do {
          $id = $this->genID();
          $query = $this->mysql->Query("SELECT filename FROM `tempfiles` WHERE (id='".$id."')");
        } while ($query);
        $this->mysql->Query("INSERT INTO `tempfiles` (id, filename, timestamp) VALUES ('".$id."', '".$fileName."', ".time().")");
      }
      return $id;
    }//addTempFile


    // Recherche le nom d'un fichier temporaire d'après son identifiant
    function getTempFile ($id) {
      $filename = '';
      if ($id<>'') {
        $conditionID = "WHERE (id='".$id."')";
        if ($this->mysql->TblExist('tempfiles')) {
          $query = $this->mysql->Query("SELECT filename FROM `tempfiles` ".$conditionID);
          if ($query) {
            $filename = $query->fetch_object()->filename;
          }
        }
      }
      return $filename;
    }//getTempFile


    // Supprime un fichier temporaire
    function deleteTempFile ($tempPath, $id) {
      if ($id<>'') {
        $conditionID = "WHERE (id='".$id."')";
        if ($this->mysql->TblExist('tempfiles')) {
          $query = $this->mysql->Query("DELETE FROM tempfiles ".$conditionID);
        }
        $this->deleteFile ($tempPath.$id.'.tmp');
        $this->deleteFile ($tempPath.$id.'_o.tmp');
      }
    }//deleteTempFile


    // Déplace un fichier temporaire
    function moveTempFile ($tempPath, $id, $dest) {
      if ($id<>'') {
        @copy ($tempPath.$id.'.tmp', $dest);
        @chmod ($dest, 0777);
        $this->deleteTempFile ($tempPath, $id);
      }
    }//moveTempFile


    // Nettoie les anciens fichiers temporaires
    function cleanTempFiles ($tempPath) {
      if ($tempPath<>'') {
        $conditionTempPath = "WHERE (timestamp<".(time()-600).")";
        if ($this->mysql->TblExist('tempfiles')) {
          $queryTempPath = $this->mysql->Query("SELECT id FROM `tempfiles` ".$conditionTempPath);
        }
        if ($queryTempPath) {
          $i=0;
          while($entree = $queryTempPath->fetch_object()) {
            $this->deleteFile ($tempPath.$entree->id.'.tmp');
            $this->deleteFile ($tempPath.$entree->id.'_o.tmp');
          }
        }
        if ($this->mysql->TblExist('tempfiles')) {
          $this->mysql->Query("DELETE FROM tempfiles WHERE (timestamp<".(time()-600).")");
        }
      }
    }//cleanTempFiles


    // Génère un identifiant pour accéder aux fichiers sécurisés
    function genAccessFilesID($id, $albumName) {
      $secondID = md5($albumName);
      $combinedID = '';
      for ($i=0;$i<strlen($id);$i++) {
        if ($i%5==0) $combinedID.=$secondID[$i];
        else $combinedID.=$id[$i];
      }
      return $combinedID;
    }//genAccessFilesID


    // Crée un nouvel accès aux fichiers sécurisés
    function createNewAccessToSecuredFiles($albumName) {
      if ($albumName<>'') {
        $returnedID = $this->genID();
        $psw = $this->genID();
        $accessCode = $this->genAccessFilesID($returnedID, $albumName);
        $query = $this->mysql->Query("SELECT uniqid FROM `securedfiles` WHERE (album='".$albumName."')");
        if ($query) {
          $this->mysql->Query("UPDATE securedfiles SET uniqid='".$accessCode."', psw='".$psw."', timestamp=".time()." WHERE (uniqid='".$query->fetch_object()->uniqid."')");
          return $returnedID;
        }
      }
    }//createNewAccessToSecuredFiles


    // Se connecte à la base de donnée
    function connect () {
      $this->mysql->Connect($this->bdName);
    }//connect


    // Se déconnecte de la base de donn�e
    function disconnect () {
      $this->mysql->Disconnect();
    }//disconnect

  }//class DatabaseQuery

?>
