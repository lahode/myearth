<?php

  // Gestion des paramètres de l'album suivants:
  // Quota de l'espace disque, paramètre FTP, E-mail pour réception des news,
  // Titre et auteur du site, propriétés et format de la police du site (menu Configuration)
  // Liens vers sites favoris (menu Liens)
  class RankManager {

    var $file;
    var $name;

    // Constructeur
    function __construct ($file, $name) {
      $this->file = $file;
      $this->name = $name;
    }


    // Récupère tous les pays d'un classement
    function getCountries ($rankType) {
      $countries = array();
      // Ouvre le fichier XML
      $domRank = new DOMDocument();
      if (($domRank->load($this->file.'.xml'))) {//&& (@$domRank->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domRank);
        $resultVoyage = $xpath->query("/ranking[@name='".$this->name."']/rank[@name='".$rankType."']");
        if ($resultVoyage->item(0)) {
          $resultAllRanks = $xpath->query("country", $resultVoyage->item(0));
          if ($resultAllRanks && $resultAllRanks->length > 0) {
            foreach ($resultAllRanks as $keyRank => $rank) {
              $countries[] = $rank->nodeValue;
            }
          }
        }
      }
      return $countries;
    }//getCountries


    // Insert un liste de pays à un classement
    function edit ($rankType, $list) {
      if ($rankType<>'' && $list) {
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();
        if (file_exists($this->file.'.xml')) {
          $domRank = new DOMDocument();
          $rank_node = $domRank->createElement('rank');
          $rank_node->setAttribute ('name', $rankType);
          foreach ($list as $listItem) {
            $rank_node->appendChild($domRank->createElement('country', $listItem));
          }
        
          $domRank->appendChild($rank_node);
          @$domAlbum->load($this->file.'.xml');
          //if (!$domAlbum->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

          // Recherche les elements invoice de l'ancien fichier
          $xpath = new DOMXpath($domAlbum);
          $resultVoyage = $xpath->query("/ranking[@name='".$this->name."']");
          if ($resultVoyage->item(0)) {
            $nodelist = $xpath->query("rank[@name='".$rankType."']", $resultVoyage->item(0));
            if ($nodelist->item(0)) {
              // Remplace l'ancien noeud existant par le nouveau
              $oldnode = $nodelist->item(0);
              $newnode = $domAlbum->importNode($domRank->documentElement, true);
              $resultVoyage->item(0)->replaceChild($newnode, $oldnode);
            }
          }
          // Sauvegarde le nouveau fichier d'archive
          @$domAlbum->save($this->file.'.xml');
        }
      }
    }//edit

  }//class RankManager

?>
