<?php

  class TaskManager {

    var $file;
    var $name;

    // Constructeur
    function __construct ($file, $name) {
      $this->file = $file;
      $this->name = $name;
    }


    // Retourne un id pour la tâche
    function getNewID () {
      do {
        $id = genID();
      } while ($this->getTask($id));
      return $id;
    }//getNewID


    // Récupère une tâche d'après son identifiant
    function getTask ($id) {
      // Ouvre le fichier XML
      $result = null;
      $domAlbum = new DOMDocument();
      $resultAllTsk = array();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/agenda[@name='".$this->name."']/tasks");

        if ($resultVoyage->item(0)) {
          $resultTsk = $xpath->query("task[@id='".convertToUTF8($id)."']", $resultVoyage->item(0));
          if ($resultTsk->item(0)) {
            $task = $resultTsk->item(0);
            $result = new Task (
              convertFromUTF8($task->getAttribute('id')),
              convertFromUTF8($task->getAttribute('type')),
              convertFromUTF8($task->getAttribute('subject')),
              convertFromUTF8($task->getAttribute('description')),
              convertFromUTF8($task->getAttribute('start')),
              convertFromUTF8($task->getAttribute('end')),
              convertFromUTF8($task->getAttribute('block')),
              convertFromUTF8($task->getAttribute('reminder'))
            );
          }
        }
      }
      return $result;
    }//getTask


    // Récupère toutes les tâches
    function getTasks ($month="", $year="") {
      // Ouvre le fichier XML
      $tasks = array();
      $domAlbum = new DOMDocument();
      $resultAllTsk = array();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/agenda[@name='".$this->name."']/tasks");

        if ($resultVoyage->item(0)) {
          if ($month<>"" && $year<>"") {
            $firstDayCurrentMonth = mktime(0, 0, 0, $month, 1, $year);
            $firstDayNextMonth = mktime(0, 0, 0, $month+1, 1, $year);
            $resultAllTsk = $xpath->query("task[(@start<".$firstDayCurrentMonth." and @end>=".$firstDayNextMonth.") or (@start>=".$firstDayCurrentMonth." and @start<".$firstDayNextMonth.") or (@end>=".$firstDayCurrentMonth." and @end<".$firstDayNextMonth.")]", $resultVoyage->item(0));
          } else {
            $resultAllTsk = $xpath->query("task", $resultVoyage->item(0));
          }
          if ($resultAllTsk && $resultAllTsk->length > 0) {
            foreach ($resultAllTsk as $keyTsk => $task) {
              $tasks[$keyTsk] = new Task (
                convertFromUTF8($task->getAttribute('id')),
                convertFromUTF8($task->getAttribute('type')),
                convertFromUTF8($task->getAttribute('subject')),
                convertFromUTF8($task->getAttribute('description')),
                convertFromUTF8($task->getAttribute('start')),
                convertFromUTF8($task->getAttribute('end')),
                convertFromUTF8($task->getAttribute('block')),
                convertFromUTF8($task->getAttribute('reminder'))
              );
            }
          }
        }
      }
      return $tasks;
    }//getTasks


    // Récupère le nombre de toutes les tâches courantes
    function getNbOfCurrentTasks () {
      // Ouvre le fichier XML
      $tasks = array();
      $today = time();
      $domAlbum = new DOMDocument();
      $resultAllTsk = array();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/agenda[@name='".$this->name."']/tasks");

        if ($resultVoyage->item(0)) {
          $resultAllTsk = $xpath->query("task[@reminder>=0]", $resultVoyage->item(0));
          if ($resultAllTsk && $resultAllTsk->length > 0) {
            foreach ($resultAllTsk as $keyTsk => $task) {
              $start = convertFromUTF8($task->getAttribute('start'));
              $reminder = convertFromUTF8($task->getAttribute('reminder'));
              $end = convertFromUTF8($task->getAttribute('end'));
              $type = convertFromUTF8($task->getAttribute('type'));
              $reminderStart = $start-($reminder*3600*24);
              if ($reminderStart < $today && $end > $today) if (!isset ($tasks[$type])) $tasks[$type] = 1; else $tasks[$type]++;
            }
          }
        }
      }
      return $tasks;
    }//getNbOfCurrentTasks


    // Supprime toutes les tâches courantes
    function deleteCurrentTasks () {
      // Ouvre le fichier XML
      $today = time();
      $domAlbum = new DOMDocument();
      $resultAllTsk = array();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/agenda[@name='".$this->name."']/tasks");

        if ($resultVoyage->item(0)) {
          $resultAllTsk = $xpath->query("task[@reminder>=0]", $resultVoyage->item(0));
          if ($resultAllTsk && $resultAllTsk->length > 0) {
            foreach ($resultAllTsk as $keyTsk => $task) {
              $taskTemp = new Task (
                convertFromUTF8($task->getAttribute('id')),
                convertFromUTF8($task->getAttribute('type')),
                convertFromUTF8($task->getAttribute('subject')),
                convertFromUTF8($task->getAttribute('description')),
                convertFromUTF8($task->getAttribute('start')),
                convertFromUTF8($task->getAttribute('end')),
                convertFromUTF8($task->getAttribute('block')),
                convertFromUTF8($task->getAttribute('reminder'))
              );
              $reminderStart = $taskTemp->start-($taskTemp->reminder*3600*24);
              if ($reminderStart < $today && $taskTemp->end > $today) {$taskTemp->reminder=-1;$this->editTask ($taskTemp);}
            }
          }
        }
      }
    }//deleteCurrentTasks


    // Supprime une tâche
    function deleteTask ($id) {
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();

      // Si un ancien fichier d'archive n'existe pas au préalable, crée un nouveau fichier d'archive avec les en-têtes
      if (file_exists($this->file.'.xml')) {
        @$domAlbum->load($this->file.'.xml');
        // Recherche les elements categories du fichier
        $xpath = new DOMXpath($domAlbum);
        $nodelist = $xpath->query("/agenda[@name='".$this->name."']/tasks/task[@id='".convertToUTF8($id)."']");
        $node = $nodelist->item(0);
        if ($node) {
          $node->parentNode->removeChild($node);
        }
        // Sauvegarde le nouveau fichier
        @$domAlbum->save($this->file.'.xml');
      }
    }//deleteTask


    // Edite une tâche
    function editTask ($task) {
      if ($task) {
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();
        if (file_exists($this->file.'.xml')) {
          $domTask = new DOMDocument();
          $task_node = $domTask->createElement('task');
          $task_node->setAttribute ('id', convertToUTF8($task->id));
          $task_node->setAttribute ('type', convertToUTF8($task->type));
          $task_node->setAttribute ('subject', convertToUTF8($task->subject));
          $task_node->setAttribute ('description', convertToUTF8($task->description));
          $task_node->setAttribute ('start', convertToUTF8($task->start));
          $task_node->setAttribute ('end', convertToUTF8($task->end));
          $task_node->setAttribute ('block', convertToUTF8($task->block));
          $task_node->setAttribute ('reminder', convertToUTF8($task->reminder));
          $domTask->appendChild($task_node);
          @$domAlbum->load($this->file.'.xml');
          //if (!$domAlbum->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

          // Recherche les elements invoice de l'ancien fichier
          $xpath = new DOMXpath($domAlbum);
          $resultVoyage = $xpath->query("/agenda[@name='".$this->name."']/tasks");
          if ($resultVoyage->item(0)) {
            $nodelist = $xpath->query("task", $resultVoyage->item(0));
            if ($nodelist->item(0)) {
              // Remplace les anciens noeuds existant par les nouveau ou rajoute les nouveau si inexistant
              $nodeModified = false;
              foreach ($nodelist as $keyNode => $node) {
                if (convertFromUTF8 ($node->getAttribute('id')) == convertFromUTF8 ($task_node->getAttribute('id'))) {
                  $oldnode = $nodelist->item($keyNode);
                  $domTask->getElementsByTagName('task')->item(0)->setAttribute('date', $node->getAttribute('date'));
                  $newnode = $domAlbum->importNode($domTask->documentElement, true);
                  $oldnode->parentNode->replaceChild($newnode, $oldnode);
                  $nodeModified = true;
                }
              }
              if (!$nodeModified) {
                $oldnode = $nodelist->item(0);
                $newnode = $domAlbum->importNode($domTask->documentElement, true);
                $oldnode->parentNode->appendChild($newnode);
              }
            } else {
              $newnode = $domAlbum->importNode($domTask->documentElement, true);
              $resultVoyage->item(0)->appendChild($newnode);
            }
          }
          // Sauvegarde le nouveau fichier d'archive
          @$domAlbum->save($this->file.'.xml');
        }
      }
    }//editTask

  }//class TaskManager

?>
