<?php
//
// +----------------------------------------------------------------------+
// | PHP Version 5                                                        |
// +----------------------------------------------------------------------+
// | Créé le 4 oct. 2005                                                  |
// +----------------------------------------------------------------------+
// | Authors: Fredrik Lahode - COMEM 31 IT                                |
// +----------------------------------------------------------------------+
//

// {{{ class Error

/**
 * Commentaire...
 *
 * @author  Fredrik Lahode
 * @version $Revision: 1.0 $
 */

class Language {

    // {{{ propriétés de Language

    var $language;

    /**
     * Tableau de messages
     *
     * @access public
     * @var    array
     */

    var $tab = array ('fr' => array (
        "menuDestination" => "Destinations",
        "menuLink" => "Liens",
        "menuForum" => "Livre d'or",
        "menuHome" => "Page d'accueil",
        "menuConfiguration" => "Configuration",
        "menuSpending" => "Dépenses",
        "menuSecurity" => "Sécurité",
        "menuFileManager" => "Gestion des fichiers",
        "menuSendNews" => "Envoi des nouvelles",
        "menuNews" => "News",
        "menuChat" => "Chat",
        "menuNotebook" => "Journal",
        "menuHumour" => "Le bétisier",
        "menuInsert" => "Insertion",
        "menuPrepare" => "Préparatifs",
        "menuVideo" => "Vidéos",
        "menuCompose" => "Nouveau message",
        "menuContacts" => "Liste des contacts",
        "menuInbox" => "Boîte de réception",
        "menuOutbox" => "Messages envoyés",
        "menuContactDetails" => "Détail du contact",
        "menuEmailDetails" => "Détail de l'e-mail",
        "menuLastVisited" => "Dernière visite",
        "menuBudgetAccommodation" => "Budget de logements",
        "menuBudgetTransport" => "Budget de transports",
        "menuBudget" => "Budget",
        "download" => "Télécharger",
        "select" => "Sélectionner",
        "today" => "Aujourd'hui",
        "isConnected" => " est connecté(e)",
        "isDisconnected" => " est déconnecté(e)",
        "isDisconnected_timeout" => " est déconnecté(e) (timeout)",
        "isCalling" => " lance un appel",
        "hasConnected" => " s'est connecté",
        "isModerator" => " devient modérateur(trice)",
        "purgePosted" => "Purge des posts effectuée",
        "noInfo" => "Pas d'information",
        "populationOf" => "Population de",
        "altitudeOf" => "Altitude de",
        "surface" => "Surface ",
        "continent" => "Continent ",
        "capital" => "Capitale",
        "longitude" => "Longitude",
        "lattitude" => "Lattitude",
        "accommodation" => "Logement",
        "food" => "Nourriture",
        "transport" => "Transport",
        "other" => "Loisirs",
        "total" => "Budget global",
        "above" => "au-dessus",
        "below" => "au-dessous",
        "spared" => "d'économie",
        "recover" => "à récupérer !",
        "visible" => "Visible",
        "hidden" => "Caché",
        "connectedPeople" => " personnes connectées",
        "moreInfo" => "Pour plus d'information, tu peux consulter la rubrique ",
        "joinme" => "Pour en voir plus, rejoins-moi sur mon site",
        "unregisternews" => "Pour ne plus recevoir les news, ",
        "updateClientSubject" => "Mise à jour de tes coordonnées",
        "updateClientInfo" => "Pour mettre à jour tes données actuelles, ",
        "born" => "Né(e) le ",
        "tel" => "Tél. ",
        "fax" => "Fax ",
        "mobile" => "Mobile ",
        "messenger" => "Messenger: ",
        "comments" => "Commentaires: ",
        "clickhere" => "clique ici",
        "inhabitant" => "habitants",
        "country" => "Pays",
        "unclassified" => "Non classée",
        "background" => "Fond d'écran",
        "maps" => "Cartes",
        "senton" => "Envoyé le",
        "localCurrency" => "Monnaie locale",
        "defaultCurrency" => "Monnaie par défaut",
        "RSSNews" => "Des nouvelles toutes fraîches",
        "RSSForum" => "Message de ",
        "RSSHumour" => "De quoi rire un peu",
        "RSSNotebook" => "Une page du journal a été ajoutée",
        "RSSRoute" => "Une nouvelle destination a été ajouté",
        "RSSVideo" => "Une nouvelle vidéo sur le site",
        "theNewsOf" => "Les nouvelles de ",
        "infoselectpics" => "Sélection une images dans la bibliothèque",
        "sameDay" => "Le même jour",
        "dayBefore" => "jour avant",
        "daysBefore" => "jours avant",
        "noReminder" => "Ne pas me rappeler",
        "defaultMailbox" => "Boîte par défaut",
        "addToContact" => "Ajouter un nouveau contact"
        ), 'en' => array (
        "menuDestination" => "Destinations",
        "menuLink" => "Links",
        "menuForum" => "Forum",
        "menuHome" => "Home",
        "menuConfiguration" => "Configuration",
        "menuSpending" => "Spending",
        "menuSecurity" => "Security",
        "menuFileManager" => "File management",
        "menuSendNews" => "Send News",
        "menuNews" => "News",
        "menuChat" => "Chat",
        "menuNotebook" => "Notebook",
        "menuHumour" => "Humour",
        "menuInsert" => "Insert",
        "menuPrepare" => "Preparations",
        "menuVideo" => "Videos",
        "menuCompose" => "New message",
        "menuContacts" => "Contacts List",
        "menuInbox" => "Inbox",
        "menuOutbox" => "Sent Messages",
        "menuContactDetails" => "Contacts Details",
        "menuEmailDetails" => "Email Details",
        "menuLastVisited" => "Last visited",
        "menuBudgetAccommodation" => "Budget for accommodation",
        "menuBudgetTransport" => "Budget for transports",
        "menuBudget" => "Budget",
        "download" => "Upload",
        "select" => "Select",
        "today" => "Today",
        "isConnected" => " is connected",
        "isDisconnected" => " is disconnected",
        "isDisconnected_timeout" => " is disconnected (timeout)",
        "isCalling" => " sends a call ",
        "hasConnected" => " is now connected",
        "isModerator" => " become moderator",
        "purgePosted" => "Purge of posts",
        "noInfo" => "No information",
        "populationOf" => "Population of",
        "altitudeOf" => "Altitude of",
        "surface" => "Surface ",
        "continent" => "Continent ",
        "capital" => "Capital",
        "longitude" => "Longitude",
        "lattitude" => "Lattitude",
        "accommodation" => "Accommodation",
        "food" => "Food",
        "transport" => "Transport",
        "other" => "Leisures",
        "total" => "Total of budget",
        "above" => "above",
        "below" => "below",
        "spared" => "of spared money",
        "recover" => "to recover !",
        "visible" => "Visible",
        "hidden" => "Hidden",
        "connectedPeople" => " user connected",
        "moreInfo" => "For more information, please visit the section ",
        "joinme" => "To see more, join me on my website",
        "unregisternews" => "To unregister from the newsletter, ",
        "updateClientSubject" => "Update your personal information",
        "updateClientInfo" => "To update your present information, ",
        "born" => "Born on ",
        "tel" => "Tel. ",
        "fax" => "Fax ",
        "mobile" => "Mobile ",
        "messenger" => "Messenger: ",
        "comments" => "Comments: ",
        "clickhere" => "click here",
        "inhabitant" => "inhabitants",
        "country" => "Country",
        "unclassified" => "Unclassified",
        "background" => "Background images",
        "maps" => "Maps",
        "senton" => "Sent on",
        "localCurrency" => "Local Currency",
        "defaultCurrency" => "Default Currency",
        "RSSNews" => "Brand fresh news",
        "RSSForum" => "Message from ",
        "RSSHumour" => "Some reason to laugh",
        "RSSNotebook" => "A new page in the notebook has been added",
        "RSSRoute" => "A new destination has been added",
        "RSSVideo" => "A new video on the site",
        "theNewsOf" => "The News of ",
        "infoselectpics" => "Select a picture in the library",
        "sameDay" => "The same day",
        "dayBefore" => "day before",
        "daysBefore" => "days before",
        "noReminder" => "Don't remind me",
        "defaultMailbox" => "Default mailbox",
        "addToContact" => "Add a new contact"
    ));

    /**
     * Tableau d'erreur
     *
     * @access public
     * @var    array
     */

    var $tabError = array ('fr' => array (
        "invalidID" => "Identifiant incorrect",
        "invalidEmail" => "L'adresse e-mail est invalide",
        "invalidEmails" => "Une ou plusieurs adresses e-mail sont invalides",
        "invalidLink" => "Le lien que vous essayez d'entrer est invalide",
        "invalidQuota" => "Le quota que vous essayez d'entrer doit être numérique",
        "selectCountry" => "Un pays doit être sélectionné",
        "selectRegion" => "Une région doit être sélectionnée",
        "selectCity" => "Une ville doit être sélectionnée",
        "selectArrivalCity" => "La ville d'arrivée doit être sélectionnée",
        "selectDepartureCity" => "La ville de départ doit être sélectionnée",
        "budget_accommodation_planed" => "Le budget de logements planifié contient une erreur (facultatif)",
        "budget_food_planed" => "Le budget de nourriture planifié contient une erreur (facultatif)",
        "budget_transport_planed" => "Le budget de transports planifié contient une erreur (facultatif)",
        "budget_other_planed" => "Le budget de loisirs planifié contient une erreur (facultatif)",
        "budget_accommodation_spent" => "Le budget de logements et nourritures dépensé contient une erreur (facultatif)",
        "budget_sleep_spent" => "Le budget de nourriture et nourritures dépensé contient une erreur (facultatif)",
        "budget_transport_spent" => "Le budget de transports dépensé contient une erreur (facultatif)",
        "budget_other_spent" => "Le budget de loisirs dépensé contient une erreur (facultatif)",
        "formatPersoIncorrect" => "Le format de votre page personnelle doit être en HTML",
        "rate" => "Un taux supérieur à zéro doit être inséré",
        "uploadImpossible" => "Impossible de charger le fichier",
        "copyImpossible" => "Impossible de copier le fichier",
        "insertNameorLink" => "Un nom et un lien doivent être insérés",
        "insertNameorEmail" => "Un nom et un email doivent être insérés",
        "insertName" => "Un nom doit être inséré",
        "insertSubject" => "Un sujet doit être inséré",
        "insertContact" => "Au moins un contact doit être inséré",
        "insertPrice" => "Le prix inséré est invalide",
        "insertDuration" => "Une durée supérieur é zéro doit être insérée",
        "insertDistance" => "La distance insérée est invalide",
        "noDatesConfig" => "La date de départ et d'arrivée du voyage doivent être configur�s dans les paramêtres de configuration",
        "noDataFound" => "Aucune donnée n'a été trouvée",
        "noContactToDelete" => "Aucun contact n'a sélectionné pour être supprimé",
        "noMsgToDelete" => "Aucun message n'a sélectionné pour être supprimé",
        "noMsgToRestore" => "Aucun message n'a sélectionné pour être restauré",
        "noMsgToClean" => "Aucun message n'a sélectionné pour être vidé de la corbeille",
        "noBudgetBeforeDeparture" => "Impossible d'afficher le budget avant le départ",
        "passwordValidation" => "Votre mot de passe doit comporter au minimum 8 caractères, dont 1 chiffre",
        "passwordConfirmation" => "Le mot de passe de confirmation ne correspond pas",
        "passwordCheck" => "Erreur! Mot de passe incorrect",
        "passwordField" => "Vous devez compléter les trois champs pour modifier le mot de passe",
        "dbError" => "Connexion impossible avec la base de données",
        "userAlreadyConnected" => "Une personne du même nom est déjà connectée",
        "nicknameForbidden" => "Vous ne pouvez vous utiliser ce pseudo",
        "fieldError" => "Tous les champs n'ont pas été rempli correctement",
        "pictureExist" => "images du même nom existe déjà et n'ont pas été transférée",
        "pictureIncorrect" => "Le fichier n'est pas une image",
        "pictureFormat" => "Le format de l'image est incorrect",
        "pictureGetError" => "Impossible de récupérer l'image",
        "mp3FormatError" => "Le format du fichier n'est pas un mp3",
        "videoFormatError" => "Le format du fichier n'est pas une vidéo",
        "fileExist" => "Le nom du fichier existe déjà",
        "ftpError" => "Connexion impossible au serveur FTP",
        "quotaExceeded" => "Vous avez excédé votre quota, veuillez purger vos images orginales ou contactez le webmaster",
        "date" => "La date que vous avez entré est invalide",
        "existantName"  => "Le nom que vous avez entré existe déjà",
        "existantPerson"  => "Cette personne existe déjà",
        "inexistantPerson"  => "Erreur! La personne n'existe pas",
        "inexistantLink"  => "Erreur! Le lien n'existe pas",
        "noDestinationFound" => "Vous devez préalablement entrer au moins une destination",
        "noCategoryFound" => "Vous devez préalablement entrer au moins un pays",
        "noRegisteredPersons" => "Impossible d'envoyer votre message, car personne n'est enregistré",
        "maxsize100" => "La taille du fichier ne doit pas excéder 100 ko",
        "sizeToBig" => "La taille du fichier est trop grande",
        "saveOrigError" => "Impossible de sauvegarder le fichier original",
        "saveError" => "Le fichier n'a pas pu être enregistré",
        "nofileSelected" => "Aucun fichier n'a été sélectionné",
        "invalidFile" => "Le fichier est vide ou corrompu",
        "oneTypeByColumn" => "Vous ne pouvez définir qu'un type différent par colonne",
        "columnSelect" => "Vous devez sélectionner au moins une colonne",
        "importContact" => "Une erreur s'est produite durant l'importation des nouveaux contacts",
        "newDirFailed" => "Impossible de créer un nouveau dossier",
        "nameAlpha" => "Le champs nom n'accepte que des caractères alphanumérique",
        "dateStart" => "Une date de début doit être définie",
        "dateEnd" => "Une date de fin doit être définie",
        "dateStartInvalid" => "La date de début ne peut être après la date de fin",
        "reservedName" => "Erreur! ce nom est réservé, veuillez en sélectionner un autre",
        "timeExceed" => "Le temps limite est dépassé, veuillez relancer l'action pour effectuer le reste",
        "newsSent" => "Une erreur a été détectée lors de l'envoi des news aux destinataires",
        "emailSent" => "Une erreur a été détectée lors de l'envoi du message aux destinataires"
        ), 'en' => array (
        "invalidID" => "Incorrect identificator",
        "invalidEmail" => "The e-mail address is invalid",
        "invalidEmails" => "One or several e-mail address are invalid",
        "invalidLink" => "The link you are trying to type is invalid",
        "selectCountry" => "A country must be selected",
        "selectRegion" => "A region must be selected",
        "selectCity" => "A city must be selected",
        "selectArrivalCity" => "The city of arrival must be selected",
        "selectDepartureCity" => "The city of departure must be selected",
        "budget_accommodation_planed" => "The budget planed for accommodation contains an erreur (optional)",
        "budget_food_planed" => "The budget planed for food contains an erreur (optional)",
        "budget_transport_planed" => "The budget planed for transport contains an erreur (optional)",
        "budget_other_planed" => "The budget planed for leisures contains an erreur (optional)",
        "budget_accommodation_spent" => "The budget spent for accommodation contains an erreur (optional)",
        "budget_food_spent" => "The budget spent for food contains an erreur (optional)",
        "budget_transport_spent" => "The budget spent for transport contains an erreur (optional))",
        "budget_other_spent" => "The budget spent for leisures contains an erreur (optional)",
        "formatPersoIncorrect" => "The format of your personal page must be in HTML",
        "rate" => "A rate above zero must be defined",
        "uploadImpossible" => "Cannot uploader the file",
        "copyImpossible" => "Cannot copy the file",
        "insertName" => "A name must be defined",
        "insertSubject" => "A subject must be defined",
        "insertContact" => "At least one contact must be inserted",
        "insertPrice" => "The price entered is invalid",
        "insertDuration" => "A duration above zero must be defined",
        "insertDistance" => "The distance entered is invalid",
        "noDatesConfig" => "The date of departure and arrival must be configured in the configuration panel",
        "noDataFound" => "No data has been found",
        "noContactToDelete" => "No contact have been selected for deleting",
        "noMsgToDelete" => "No message have been selected to be deleted",
        "noMsgToRestore" => "No message have been selected to be restored",
        "noMsgToClean" => "No message have been selected to be emptied from the trash",
        "noBudgetBeforeDeparture" => "Impossible to display the budget before the departure",
        "passwordValidation" => "Your password must contain at least 8 characters with one number",
        "passwordConfirmation" => "The confirmation password does not match",
        "passwordCheck" => "Incorrect password",
        "passwordField" => "You must fill the three field to update your password",
        "dbError" => "Impossible to connect with the database",
        "userAlreadyConnected" => "A user with the same name is already connected",
        "nicknameForbidden" => "You cannot use this nickname",
        "fieldError" => "Some fields have not been correctly filled",
        "pictureExist" => "pictures having the same name already exist and couldn't be transfered",
        "pictureIncorrect" => "This file is not a picture",
        "pictureFormat" => "The format of the picture is incorrect",
        "pictureGetError" => "Impossible to recuperate the picture",
        "mp3FormatError" => "The file format is not a mp3",
        "videoFormatError" => "The file format is not a video",
        "fileExist" => "The file name already exists",
        "ftpError" => "Impossible to connect to the FTP server",
        "quotaExceeded" => "You have exceeded your quota, please purge your original pictures or contact the webmaster",
        "date" => "The date you have entered is invalid",
        "existantName"  => "The name you have entered already exists",
        "existantPerson"  => "This person already exists",
        "inexistantPerson"  => "Error! The person does not exists",
        "inexistantLink"  => "Error! The link does not exist",
        "noDestinationFound" => "You need first to enter at least one destination",
        "noCategoryFound" => "You need first to enter at least one country",
        "noRegisteredPersons" => "Impossible to send your message, because noone is registered",
        "maxsize100" => "The file size cannot exceed 100 kb",
        "sizeToBig" => "The file size is to large",
        "saveOrigError" => "Impossible to store the original picture",
        "saveError" => "The file could not be stored",
        "nofileSelected" => "No file has been selected",
        "invalidFile" => "The file is empty or corrupted",
        "oneTypeByColumn" => "Only one different type must be selected for each column",
        "columnSelect" => "You have to select at least one column",
        "importContact" => "An error occured while importing the new contacts",
        "newDirFailed" => "Impossible to create a new directory",
        "nameAlpha" => "The field name only accept alphanumeric characters",
        "dateStart" => "A starting date must be defined",
        "dateEnd" => "An ending date must be defined",
        "dateStartInvalid" => "The starting date cannot be after the ending date",
        "reservedName" => "Error! This name is reserved, please select another one",
        "timeExceed" => "The time limit is expired, please relaunch the action to execute the rest",
        "newsSent" => "An error has occured while sending news to addressees",
        "emailSent" => "An error has occured while sending message to addressees"
    ));

    /**
     * Tableau de confirmation
     *
     * @access public
     * @var    array
     */

    var $tabConfirm = array ('fr' => array (
        "uploadSuccess" => "Le fichier %%% a été envoyé avec succès",
        "registerNews" => "Vous avez été enregistré avec succès",
        "newsSent" => "Vos nouvelles ont été transmises aux destinataires",
        "guestPasswordChanged" => "Le mot de passe des invités a été modifié avec succès",
        "adminPasswordChanged" => "Votre mot de passe personnel a été modifié avec succès",
        "configChanged" => "Votre nouvelle configuration a été correctement enregistrée",
        "messageConfirm" => "Merci de m'avoir transmis votre message",
        "deletePerson" => "La personne sélectionnée a été supprimée des news correctement",
        "deletePersonFromNews" => "Vous avez été supprimé des news avec succès",
        "confirmPicsTransfered" => "nouvelles images ont été transférées",
        "noNewPics" => "Aucunes nouvelles images trouvées",
        "contactUpdated" => "Le contact a été modifié avec succès",
        "contactInserted" => "Le nouveau contact a été inséré avec succès",
        "selfUpdated" => "Vos données ont été mis à jour correctement",
        "sentMail" => "Votre message a été envoyé"
        ), 'en' => array (
        "uploadSuccess" => "The file %%% has been successfully uploaded",
        "registerNews" => "You have been successfully registered",
        "newsSent" => "Your news have been sent to your addressee",
        "guestPasswordChanged" => "The guest password has been correctly updated",
        "adminPasswordChanged" => "Your personal password has been correctly updated",
        "configChanged" => "Your new configuration has been correctly saved",
        "messageConfirm" => "Thank you for having sent this message",
        "deletePerson" => "The selected person has been correctly deleted",
        "deletePersonFromNews" => "You have been successfully deleted from the news",
        "confirmPicsTransfered" => "new pictures have been transfered",
        "noNewPics" => "No new pictures has been found",
        "contactUpdated" => "The contact has been successfully updated",
        "contactInserted" => "The new contact has been successfully inserted",
        "selfUpdated" => "Your information have been correctly updated",
        "sentMail" => "Your message has been sent"
    ));


    /**
     * Tableau des continents
     *
     * @access public
     * @var    array
     */

    var $tabContinent = array ('fr' => array (
        "AF" => "Afrique",
        "AS" => "Asie",
        "SA" => "Amérique du sud",
        "NA" => "Amérique du nord",
        "OC" => "Océanie",
        "EU" => "Europe"
        ), 'en' => array (
        "AF" => "Africa",
        "AS" => "Asia",
        "SA" => "South America",
        "NA" => "North America",
        "OC" => "Oceania",
        "EU" => "Europe"
    ));


    // }}}


    /**
     * Tableau des transports
     *
     * @access public
     * @var    array
     */

    var $tabArrivalBy = array ('fr' => array (
        0 => "auto",
        1 => "avion",
        2 => "bateau",
        3 => "bus",
        4 => "moto",
        5 => "train",
        6 => "trek",
        7 => "vélo",
        8 => "taxi",
        9 => "hélicoptère"
        ), 'en' => array (
        0 => "car",
        1 => "plane",
        2 => "boat",
        3 => "bus",
        4 => "motocycle",
        5 => "train",
        6 => "trek",
        7 => "bicycle",
        8 => "taxi",
        9 => "helicopter"
    ));


    // }}}


    /**
     * Tableau des nourritures
     *
     * @access public
     * @var    array
     */

    var $tabFood = array ('fr' => array (
        0 => "restaurant",
        1 => "café",
        2 => "supermarché",
        3 => "épicerie",
        4 => "stand",
        5 => "marché",
        6 => "livraison à domicile"
        ), 'en' => array (
        0 => "restaurant",
        1 => "cafe",
        2 => "supermarket",
        3 => "grocer's shop",
        4 => "stand",
        5 => "market",
        6 => "delivery"
    ));


    /**
     * Tableau des transports
     *
     * @access public
     * @var    array
     */

    var $tabAccommodation = array ('fr' => array (
        0 => "auberge",
        1 => "appartement",
        2 => "bungalow",
        3 => "chez l'habitant",
        4 => "dortoir",
        5 => "hôtel",
        6 => "camping"
        ), 'en' => array (
        0 => "guesthouse",
        1 => "appartement",
        2 => "bungalow",
        3 => "bed & breakfast",
        4 => "dormitories",
        5 => "hotel",
        6 => "camping"
    ));


    // }}}


    /**
     * Tableau des éléments des statistiques
     *
     * @access public
     * @var    array
     */

    var $tabStats = array ('fr' => array (
        "accommodation" => "Logement",
        "food" => "Nourriture",
        "transport" => "Transport",
        "other" => "Loisirs",
        "total" => "Total"
        ), 'en' => array (
        "accommodation" => "Accommodation",
        "food" => "Food",
        "transport" => "Transport",
        "other" => "Leisures",
        "total" => "Total"
    ));


    // }}}


    /**
     * Tableau des pr�parations
     *
     * @access public
     * @var    array
     */

    var $tabPreparation = array ('fr' => array (
        "beforeDeparture" => "Juste avant de partir",
        "tickets" => "Billet de transports",
        "health" => "Vaccins et Santé",
        "formalities" => "Visas, Passport et permis",
        "sponsors" => "Sponsors",
        "insurance" => "Assurances",
        "vacancy" => "Congés",
        "guide" => "Guide et Itinéraire",
        "budget" => "Budget",
        "material" => "Affaires de voyage",
        "scholarship" => "Scolarité"
        ), 'en' => array (
        "beforeDeparture" => "Just before departure",
        "tickets" => "Transport ticket",
        "health" => "Health and vaccination",
        "formalities" => "Visas, Passport et permits",
        "sponsors" => "Sponsors",
        "insurance" => "Insurances",
        "vacancy" => "Vacancy",
        "guide" => "Guide et Itinaries",
        "budget" => "Budget",
        "material" => "Travel materials",
        "scholarship" => "Scholarships"
    ));


    // }}}


    /**
     * Tableau des mois
     *
     * @access public
     * @var    array
     */

    var $tabMonth = array ('fr' => array (
        "01" => "jan.",
        "02" => "fév.",
        "03" => "mars",
        "04" => "avr.",
        "05" => "mai",
        "06" => "juin",
        "07" => "juil.",
        "08" => "août",
        "09" => "sep.",
        "10" => "oct.",
        "11" => "nov.",
        "12" => "déc."
        ), 'en' => array (
        "01" => "jan.",
        "02" => "feb.",
        "03" => "mar.",
        "04" => "apr.",
        "05" => "may",
        "06" => "june",
        "07" => "july.",
        "08" => "aug.",
        "09" => "sep.",
        "10" => "oct.",
        "11" => "nov.",
        "12" => "dec."
    ));


    // }}}


    /**
     * Tableau des style de police
     *
     * @access public
     * @var    array
     */

    var $tabFontstyle = array ('fr' => array (
        0 => "Normal",
        1 => "Gras",
        2 => "Italic",
        3 => "Gras / Italic"
        ), 'en' => array (
        0 => "Normal",
        1 => "Bold",
        2 => "Italic",
        3 => "Bold / Italic"
    ));


    // }}}


    /**
     * Tableau des style de police
     *
     * @access public
     * @var    array
     */

    var $tabTaskType = array ('fr' => array (
        "note" => "Note",
        "todo" => "A faire",
        "appointment" => "Rendez-vous",
        "anniversary" => "Anniversaire",
        "call" => "Appel"
        ), 'en' => array (
        "note" => "Note",
        "todo" => "To do",
        "appointment" => "Appointment",
        "anniversary" => "Anniversary",
        "call" => "Call"
    ));


    // }}}

    /**
     * Tableau des contacts
     *
     * @access public
     * @var    array
     */

    var $tabContact = array ('fr' => array (
        'none' => 'Ne pas afficher',
        'title' => 'Titre',
        'firstname' => 'Prénom',
        'lastname' => 'Nom',
        'nickname' => 'Surnom',
        'email' => 'E-mail',
        'birthdate' => 'Date de naissance',
        'address' => 'Adresse',
        'zipcode' => 'Code postal',
        'city' => 'Ville',
        'country' => 'Pays',
        'tel' => 'Téléphone',
        'fax' => 'Fax',
        'mobile' => 'Mobile',
        'messenger' => 'Messenger'
        ), 'en' => array (
        'none' => 'Do not display',
        'title' => 'Title',
        'firstname' => 'First name',
        'lastname' => 'Last name',
        'nickname' => 'Nickname',
        'email' => 'E-mail',
        'birthdate' => 'Birth date',
        'address' => 'Address',
        'zipcode' => 'Zip code',
        'city' => 'City',
        'country' => 'Country',
        'tel' => 'Telephone',
        'fax' => 'Fax',
        'mobile' => 'Mobile', 
        'messenger' => 'Messenger'
    ));


    // }}}

    /**
     * Tableau du classement
     *
     * @access public
     * @var    array
     */

    var $tabRanking = array ('fr' => array (
        'general' => 'Impression générale',
        'population' => 'Culture et peuple',
        'politic' => 'Politique et religion',
        'history' => 'Histoire et architecture',
        'nature' => 'Géographie et nature',
        'infra' => 'Infrastructure et hygiène',
        'food' => 'Art culinaire',
        'activity' => 'Activités',
        'budget' => 'Coût de la vie',
        'accommodation' => 'Hébergement',
        'transport' => 'Transport',
        'communication' => 'Communication'
        ), 'en' => array (
        'general' => 'General impression',
        'population' => 'Culture and Population',
        'politic' => 'Politics and Religion',
        'history' => 'History and Architecture',
        'nature' => 'Geographie and Nature',
        'infra' => 'Infrastructure and Hygiene',
        'food' => 'Food art',
        'activity' => 'Activities',
        'budget' => 'Life cost',
        'accommodation' => 'Accommodation',
        'transport' => 'Transport',
        'communication' => 'Communication'
    ));


    // }}}


    /**
     * Tableau du classement
     *
     * @access public
     * @var    array
     */


    var $tabRankingValues = array ('fr' => array (
        0 => 'Dernier au classement',
        1 => 'Premier au classement',
        2 => '2ème rang',
        3 => '3ème rang',
        4 => '4ème rang',
        5 => '5ème rang',
        6 => 'ème rang'
        ), 'en' => array (
        0 => 'Last in ranking',
        1 => 'First in ranking',
        2 => '2th in ranking',
        3 => '3rd in ranking',
        4 => '4th in ranking',
        5 => '5th in ranking',
        6 => 'th in ranking'
    ));

    // }}}


    // {{{ Constructor

    /**
     * .
     * Construteur d'erreur
     *
     * @return void
     */
    function __construct($lang) {
      $this->language = $lang;
    }

    // }}}
    // {{{ getError()
    /**
     * .
     * Retourne l'erreur correspondante
     *
     * @param  string $error        Nom de l'erreur
     * @return bool
     * @access public
     */
    function getError ($error)
    {
        return $this->tabError[$this->language][$error];
    }

    // }}}
    // {{{ getConfirm()
    /**
     * .
     * Retourne la confirmation correspondante
     *
     * @param  string $error        Nom de l'erreur
     * @return bool
     * @access public
     */
    function getConfirm ($confirm)
    {
        return $this->tabConfirm[$this->language][$confirm];
    }

    // }}}

    // {{{ get()
    /**
     * .
     * Retourne la confirmation correspondante
     *
     * @param  string $word         Mot
     * @return bool
     * @access public
     */
    function get ($word)
    {
        return $this->tab[$this->language][$word];
    }

    // }}}


    // {{{ getTable()
    /**
     * .
     * Retourne la valeur du tableau correspondant
     *
     * @param  string $table         Choix du tableau
     * @param  string $key           Cl� du tableau (facultatif)
     * @return bool
     * @access public
     */
    function getTable ($table, $key='')
    {
        $result = false;
        if ($key<>'') {
          switch ($table) {
            case "continent" : {$result = $this->tabContinent[$this->language][$key];break;}
            case "arrivalBy" : {$result = $this->tabArrivalBy[$this->language][$key];break;}
            case "accommodation" : {$result = $this->tabAccommodation[$this->language][$key];break;}
            case "food" : {$result = $this->tabFood[$this->language][$key];break;}
            case "stats" : {$result = $this->tabStats[$this->language][$key];break;}
            case "preparation" : {$result = $this->tabPreparation[$this->language];break;}
            case "month" : {$result = $this->tabMonth[$this->language][$key];break;}
            case "fontstyle" : {$result = $this->tabFontstyle[$this->language][$key];break;}
            case "taskType" : {$result = $this->tabTaskType[$this->language][$key];break;}
            case "contact" : {$result = $this->tabContact[$this->language][$key];break;}
            case "ranking" : {$result = $this->tabRanking[$this->language][$key];break;}
            case "rankingValues" : {$result = $this->tabRankingValues[$this->language][$key];break;}
          }
        } else {
          switch ($table) {
            case "continent" : {$result = $this->tabContinent[$this->language];break;}
            case "arrivalBy" : {$result = $this->tabArrivalBy[$this->language];break;}
            case "accommodation" : {$result = $this->tabAccommodation[$this->language];break;}
            case "food" : {$result = $this->tabFood[$this->language];break;}
            case "stats" : {$result = $this->tabStats[$this->language];break;}
            case "preparation" : {$result = $this->tabPreparation[$this->language];break;}
            case "month" : {$result = $this->tabMonth[$this->language];break;}
            case "fontstyle" : {$result = $this->tabFontstyle[$this->language];break;}
            case "taskType" : {$result = $this->tabTaskType[$this->language];break;}
            case "contact" : {$result = $this->tabContact[$this->language];break;}
            case "ranking" : {$result = $this->tabRanking[$this->language];break;}
            case "rankingValues" : {$result = $this->tabRankingValues[$this->language];break;}
          }
        }
        return $result;
    }

    // }}}
}

// }}}
?>