<?php

  // Gestion des news (menu News, send message et news register)
  class NewsManager {

    var $file;
    var $name;
    var $rssManager;
    var $rssMessage;

    // Constructeur
    function __construct ($file, $name, $rssManager=null, $rssMessage="") {
      $this->file = $file;
      $this->name = $name;
      $this->rssManager = $rssManager;
      $this->rssMessage = $rssMessage;
    }

    // Récupère toutes les personnes inscrites des news
    function getPersons () {
      $persons = array();
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      $resultAllMsg = array();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/news[@name='".$this->name."']/registered");

        if ($resultVoyage->item(0)) {
          $resultAllPrs = $xpath->query("register", $resultVoyage->item(0));
          if ($resultAllPrs->length > 0) {
            foreach ($resultAllPrs as $keyMsg => $person) {
              $persons[$keyMsg] = new Person (
                convertFromUTF8($person->getAttribute('id')),
                convertFromUTF8($person->getAttribute('name')),
                convertFromUTF8($person->getAttribute('email')),
                convertFromUTF8($person->getAttribute('date'))
              );
            }
          }
        }
      }
      return $persons;
    }//getPersons


    // Récupère une personne par son id des news
    function getPerson ($id) {
      if ($id <> '') {
        $person = null;
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();
        $resultAllMsg = array();
        if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
          $xpath = new DOMXpath($domAlbum);
          $resultVoyage = $xpath->query("/news[@name='".$this->name."']/registered");

          if ($resultVoyage->item(0)) {
            $resultPerson = $xpath->query("register[@id='".$id."']", $resultVoyage->item(0));
            if ($resultPerson->item(0)) {
              $person = new Person (
                convertFromUTF8($resultPerson->item(0)->getAttribute('id')),
                convertFromUTF8($resultPerson->item(0)->getAttribute('name')),
                convertFromUTF8($resultPerson->item(0)->getAttribute('email')),
                convertFromUTF8($resultPerson->item(0)->getAttribute('date'))
              );
            }
          }
        }
      }
      return $person;
    }//getPerson


    // Récupère une personne par son id des news
    function getPersonByEmail ($email) {
      if ($email <> '') {
        $person = null;
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();
        $resultAllMsg = array();
        if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
          $xpath = new DOMXpath($domAlbum);
          $resultVoyage = $xpath->query("/news[@name='".$this->name."']/registered");

          if ($resultVoyage->item(0)) {
            $resultPerson = $xpath->query("register[@email='".convertToUTF8($email)."']", $resultVoyage->item(0));
            if ($resultPerson->item(0)) {
              $person = new Person (
                convertFromUTF8($resultPerson->item(0)->getAttribute('id')),
                convertFromUTF8($resultPerson->item(0)->getAttribute('name')),
                convertFromUTF8($resultPerson->item(0)->getAttribute('email')),
                convertFromUTF8($resultPerson->item(0)->getAttribute('date'))
              );
            }
          }
        }
      }
      return $person;
    }//getPersonByEmail


    // Récupère le message des news
    function getMessage() {
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      $resultAllMsg = array();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/news[@name='".$this->name."']");

        if ($resultVoyage->item(0)) {
          $message = convertFromUTF8($resultVoyage->item(0)->getElementsByTagName('message')->item(0)->nodeValue);
        }
      }
      return $message;
    }//getMessage


    // Récupère le message des news
    function getSendMessage() {
      $message = null;
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      $resultAllMsg = array();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/news[@name='".$this->name."']");

        if ($resultVoyage->item(0)) {
          $message = new MessageSender (
            '',
            '',
            convertFromUTF8($resultVoyage->item(0)->getElementsByTagName('sendmessage')->item(0)->getAttribute('email')),
            0,
            convertFromUTF8($resultVoyage->item(0)->getElementsByTagName('sendmessage')->item(0)->getAttribute('subject')),
            convertFromUTF8($resultVoyage->item(0)->getElementsByTagName('sendmessage')->item(0)->nodeValue),
            null
          );
        }
      }
      return $message;
    }//getSendMessage


    // Insert ou modifie le message des news
    function editMessage ($message) {
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
        if (file_exists($this->file.'.xml')) {
          $domMessage = new DOMDocument();
          $message_node = $domMessage->createElement('message', convertToUTF8($message));
          $domMessage->appendChild($message_node);
          @$domAlbum->load($this->file.'.xml');
          //if (!$domAlbum->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

          // Recherche les elements invoice de l'ancien fichier
          $xpath = new DOMXpath($domAlbum);
          $resultVoyage = $xpath->query("/news[@name='".$this->name."']");
          if ($resultVoyage->item(0)) {
            $nodelist = $xpath->query("message", $resultVoyage->item(0));
            if ($nodelist->item(0)) {
              // Remplace l'ancien noeud existant par le nouveau
              $oldnode = $nodelist->item(0);
              $newnode = $domAlbum->importNode($domMessage->documentElement, true);
              $resultVoyage->item(0)->replaceChild($newnode, $oldnode);
            }
          }
          // Sauvegarde le nouveau fichier d'archive
          @$domAlbum->save($this->file.'.xml');
          if ($this->rssManager) $this->rssManager->insert (genID(), $this->rssMessage, '', $message->text);
        }
    }//editMessage


    // Insert ou modifie le message des news
    function editSendMessage ($message) {
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
        if (file_exists($this->file.'.xml')) {
          $domMessage = new DOMDocument();
          $message_node = $domMessage->createElement('sendmessage', convertToUTF8($message->text));
          $message_node->setAttribute ('email', convertToUTF8($message->email));
          $message_node->setAttribute ('subject', convertToUTF8($message->subject));
          $domMessage->appendChild($message_node);
          @$domAlbum->load($this->file.'.xml');
          //if (!$domAlbum->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

          // Recherche les elements invoice de l'ancien fichier
          $xpath = new DOMXpath($domAlbum);
          $resultVoyage = $xpath->query("/news[@name='".$this->name."']");
          if ($resultVoyage->item(0)) {
            $nodelist = $xpath->query("sendmessage", $resultVoyage->item(0));
            if ($nodelist->item(0)) {
              // Remplace l'ancien noeud existant par le nouveau
              $oldnode = $nodelist->item(0);
              $newnode = $domAlbum->importNode($domMessage->documentElement, true);
              $resultVoyage->item(0)->replaceChild($newnode, $oldnode);
            }
          }
          // Sauvegarde le nouveau fichier d'archive
          @$domAlbum->save($this->file.'.xml');
        }
    }//editSendMessage


    // Insert un message des news
    function insertPerson ($person) {
      if ($person) {
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();
        if (file_exists($this->file.'.xml')) {
          $domPerson = new DOMDocument();
          $person_node = $domPerson->createElement('register');
          $person_node->setAttribute ('id', convertToUTF8($person->id));
          $person_node->setAttribute ('name', convertToUTF8($person->name));
          $person_node->setAttribute ('email', convertToUTF8($person->email));
          $person_node->setAttribute ('date', convertToUTF8($person->date));
          $domPerson->appendChild($person_node);
          @$domAlbum->load($this->file.'.xml');
          //if (!$domAlbum->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

          // Recherche les elements invoice de l'ancien fichier
          $xpath = new DOMXpath($domAlbum);
          $resultVoyage = $xpath->query("/news[@name='".$this->name."']/registered");
          if ($resultVoyage->item(0)) {
            $newnode = $domAlbum->importNode($domPerson->documentElement, true);
            $resultVoyage->item(0)->appendChild($newnode);
          }
          // Sauvegarde le nouveau fichier d'archive
          @$domAlbum->save($this->file.'.xml');
        }
      }
    }//insertMessageForum


    // Supprime une personne des news
    function deletePerson ($id) {
      if ($id <> '') {
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();

        // Si un ancien fichier d'archive n'existe pas au pr�alable, cr�e un nouveau fichier d'archive avec les en-t�tes
        if (file_exists($this->file.'.xml')) {
          @$domAlbum->load($this->file.'.xml');
          // Recherche les elements categories du fichier
          $xpath = new DOMXpath($domAlbum);
          $nodelist = $xpath->query("/news[@name='".$this->name."']/registered/register[@id='".$id."']");
          $node = $nodelist->item(0);
          if ($node) {
            $node->parentNode->removeChild($node);
          }
          // Sauvegarde le nouveau fichier
          @$domAlbum->save($this->file.'.xml');
        }
      }
    }//deletePerson

  }//class NewsManager

?>
