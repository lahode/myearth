<?php

  // Gestion des contacts (menu Contact)
  class ContactsManager {

    var $file;
    var $name;
    
    // Constructeur
    function __construct ($file, $name) {
      $this->file = $file;
      $this->name = $name;
    }


    // Récupère tous les contacts
    function getContacts () {
      $contacts = array();
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      $resultAllContact = array();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/contacts[@name='".$this->name."']");
        if ($resultVoyage->item(0)) {
          $resultAllContact = $xpath->query("contact", $resultVoyage->item(0));
          if ($resultAllContact->length > 0) {
            foreach ($resultAllContact as $keyMsg => $contact) {
              $contacts[$keyMsg] = new Contact (
                utf8_decode($contact->getAttribute('id')),
                utf8_decode($contact->getAttribute('title')),
                utf8_decode($contact->getAttribute('firstname')),
                utf8_decode($contact->getAttribute('lastname')),
                utf8_decode($contact->getAttribute('nickname')),
                utf8_decode($contact->getAttribute('email')),
                utf8_decode($contact->getAttribute('birthdate')),
                utf8_decode($contact->getAttribute('address')),
                utf8_decode($contact->getAttribute('zipcode')),
                utf8_decode($contact->getAttribute('city')),
                utf8_decode($contact->getAttribute('country')),
                utf8_decode($contact->getAttribute('tel')),
                utf8_decode($contact->getAttribute('fax')),
                utf8_decode($contact->getAttribute('mobile')),
                utf8_decode($contact->getAttribute('messenger')),
                utf8_decode($contact->getAttribute('comments')),
                utf8_decode($contact->getAttribute('date'))
              );
            }
          }
        }
      }
      return $contacts;
    }//getContacts


    // Récupère un contact par son id
    function getContact ($id) {
      $contact = null;
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      $resultContact = array();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/contacts[@name='".$this->name."']");

        if ($resultVoyage->item(0)) {
          $resultContact = $xpath->query("contact[@id='".$id."']", $resultVoyage->item(0));
          if ($resultContact->item(0)) {
            $cnt = $resultContact->item(0);
            $contact = new Contact (
              utf8_decode($cnt->getAttribute('id')),
              utf8_decode($cnt->getAttribute('title')),
              utf8_decode($cnt->getAttribute('firstname')),
              utf8_decode($cnt->getAttribute('lastname')),
              utf8_decode($cnt->getAttribute('nickname')),
              utf8_decode($cnt->getAttribute('email')),
              utf8_decode($cnt->getAttribute('birthdate')),
              utf8_decode($cnt->getAttribute('address')),
              utf8_decode($cnt->getAttribute('zipcode')),
              utf8_decode($cnt->getAttribute('city')),
              utf8_decode($cnt->getAttribute('country')),
              utf8_decode($cnt->getAttribute('tel')),
              utf8_decode($cnt->getAttribute('fax')),
              utf8_decode($cnt->getAttribute('mobile')),
              utf8_decode($cnt->getAttribute('messenger')),
              utf8_decode($cnt->getAttribute('comments')),
              utf8_decode($cnt->getAttribute('date'))
            );
          }
        }
      }
      return $contact;
    }//getContact


    // Supprime un contact
    function deleteContact ($id) {
      if ($id) {
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();

        // Si un ancien fichier d'archive n'existe pas au préalable, crée un nouveau fichier d'archive avec les en-têtes
        if (file_exists($this->file.'.xml')) {
          @$domAlbum->load($this->file.'.xml');
          // Recherche les elements categories du fichier
          $xpath = new DOMXpath($domAlbum);
          $nodelist = $xpath->query("/contacts[@name='".$this->name."']/contact[@id='".$id."']");
          $node = $nodelist->item(0);
          if ($node) {
            $node->parentNode->removeChild($node);
          }
          // Sauvegarde le nouveau fichier
          @$domAlbum->save($this->file.'.xml');
        }
      }
    }//deleteContact


    // Insert ou modifie un contact
    function editContact ($contact) {
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
        if (file_exists($this->file.'.xml')) {
          $domContact = new DOMDocument();
          $contact_node = $domContact->createElement('contact');
          $contact_node->setAttribute ('id', convertToUTF8($contact->id));
          $contact_node->setAttribute ('title', convertToUTF8($contact->title));
          $contact_node->setAttribute ('firstname', convertToUTF8($contact->firstname));
          $contact_node->setAttribute ('lastname', convertToUTF8($contact->lastname));
          $contact_node->setAttribute ('nickname', convertToUTF8($contact->nickname));
          $contact_node->setAttribute ('email', convertToUTF8($contact->email));
          $contact_node->setAttribute ('birthdate', convertToUTF8($contact->birthdate));
          $contact_node->setAttribute ('address', convertToUTF8($contact->address));
          $contact_node->setAttribute ('zipcode', convertToUTF8($contact->zipcode));
          $contact_node->setAttribute ('city', convertToUTF8($contact->city));
          $contact_node->setAttribute ('country', convertToUTF8($contact->country));
          $contact_node->setAttribute ('tel', convertToUTF8($contact->tel));
          $contact_node->setAttribute ('fax', convertToUTF8($contact->fax));
          $contact_node->setAttribute ('mobile', convertToUTF8($contact->mobile));
          $contact_node->setAttribute ('messenger', convertToUTF8($contact->messenger));
          $contact_node->setAttribute ('comments', convertToUTF8($contact->comments));
          $contact_node->setAttribute ('date', convertToUTF8($contact->date));
          $domContact->appendChild($contact_node);
          @$domAlbum->load($this->file.'.xml');
          //if (!$domAlbum->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

          // Recherche les elements invoice de l'ancien fichier
          $xpath = new DOMXpath($domAlbum);
          $resultVoyage = $xpath->query("/contacts[@name='".$this->name."']");
          if ($resultVoyage->item(0)) {
            $nodelist = $xpath->query("contact", $resultVoyage->item(0));
            if ($nodelist->item(0)) {
              // Remplace les anciens noeuds existant par les nouveau ou rajoute les nouveau si inexistant
              $nodeModified = false;
              foreach ($nodelist as $keyNode => $node) {
                if (utf8_decode ($node->getAttribute('id')) == utf8_decode ($contact_node->getAttribute('id'))) {
                  $oldnode = $nodelist->item($keyNode);
                  $newnode = $domAlbum->importNode($domContact->documentElement, true);
                  $oldnode->parentNode->replaceChild($newnode, $oldnode);
                  $nodeModified = true;
                }
              }
              if (!$nodeModified) {
                $oldnode = $nodelist->item(0);
                $newnode = $domAlbum->importNode($domContact->documentElement, true);
                if (utf8_decode ($oldnode->getAttribute('id')) == 0)
                  $oldnode->parentNode->replaceChild($newnode, $oldnode);
                else
                  $oldnode->parentNode->appendChild($newnode);
              }
            } else {
              $newnode = $domAlbum->importNode($domContact->documentElement, true);
              $resultVoyage->item(0)->appendChild($newnode);
            }
          }
          // Sauvegarde le nouveau fichier d'archive
          @$domAlbum->save($this->file.'.xml');
        }
    }//editContact

  }//class ContactsManager

?>
