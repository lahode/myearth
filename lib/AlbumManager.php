<?php

  // Gestion des paramètres de l'album suivants:
  // Quota de l'espace disque, paramètre FTP, E-mail pour réception des news,
  // Titre et auteur du site, propriétés et format de la police du site (menu Configuration)
  // Liens vers sites favoris (menu Liens)
  class AlbumManager {

    var $file;
    var $name;

    // Constructeur
    function __construct ($file, $name) {
      $this->file = $file;
      $this->name = $name;
    }


    // Récupère toutes les liens d'un album
    function getQuota () {
      $quota = 0;
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/album[@name='".$this->name."']");

        if ($resultVoyage->item(0)) {
          $quota = convertFromUTF8($resultVoyage->item(0)->getAttribute('quota'));
        }
      }
      return $quota;
    }//getQuota


    // Récupère toutes les liens d'un album
    function getFTP () {
      $ftp = 0;
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/album[@name='".$this->name."']/ftp");

        if ($resultVoyage->item(0)) {
          $ftp = convertFromUTF8($resultVoyage->item(0)->getAttribute('address'));
        }
      }
      return $ftp;
    }//getFTP


    // Récupère tous les emails d'un album
    function getEmails () {
      $emails = array();
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/album[@name='".$this->name."']/emails");

        if ($resultVoyage->item(0)) {
          $resultAllEmails = $xpath->query("email", $resultVoyage->item(0));
          if ($resultAllEmails && $resultAllEmails->length > 0) {
            foreach ($resultAllEmails as $keyEmail => $email) {
              $emails[$keyEmail] = convertFromUTF8($email->getAttribute('name'));
            }
          }
        }
      }
      return $emails;
    }//getEmails


    // Récupère toutes les liens d'un album
    function getLinks () {
      $links = array();
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/album[@name='".$this->name."']/links");

        if ($resultVoyage->item(0)) {
          $resultAllLinks = $xpath->query("link", $resultVoyage->item(0));
          if ($resultAllLinks && $resultAllLinks->length > 0) {
            foreach ($resultAllLinks as $keyLink => $link) {
              $links[$keyLink] = new Link (
                convertFromUTF8($link->getAttribute('name')),
                convertFromUTF8($link->getAttribute('link')),
                convertFromUTF8($link->getAttribute('description'))
              );
            }
          }
        }
      }
      return $links;
    }//getLinks


    // Récupère un lien d'après son nom d'un album
    function getLink ($name) {
      $link = null;
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      $resultLink = array();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/album[@name='".$this->name."']/links");

        if ($resultVoyage->item(0)) {
          $resultLink = $xpath->query("link[@name='".convertToUTF8($name)."']", $resultVoyage->item(0));
          if ($resultLink->item(0)) {
            $lnk = $resultLink->item(0);
            $link = new Link (
              convertFromUTF8($lnk->getAttribute('name')),
              convertFromUTF8($lnk->getAttribute('link')),
              convertFromUTF8($lnk->getAttribute('description'))
            );
          }
        }
      }
      return $link;
    }//getLink


    // Récupère le titre de l'album
    function getTitle () {
      $title = "";
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/album[@name='".$this->name."']");

        if ($resultVoyage->item(0)) {
          $title = convertFromUTF8($resultVoyage->item(0)->getAttribute('title'));
        }
      }
      return $title;
    }//getTitle


    // Récupère l'auteur de l'album
    function getOwner () {
      $owner = "";
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/album[@name='".$this->name."']");

        if ($resultVoyage->item(0)) {
          $owner = convertFromUTF8($resultVoyage->item(0)->getAttribute('owner'));
        }
      }
      return $owner;
    }//getOwner


    // Récupère les information de la page d'accueil de l'album
    function getHome () {
      $home = null;
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/album[@name='".$this->name."']");

        if ($resultVoyage->item(0)) {
          $resultHome = $xpath->query("home", $resultVoyage->item(0));
          if ($resultHome->item(0)) {
            $home = new Home (
              convertFromUTF8($resultHome->item(0)->getAttribute('message')),
              convertFromUTF8($resultHome->item(0)->getAttribute('image'))
            );
          }
        }
      }
      return $home;
    }//getHome


    // Récupère les propriétés de tous les éléments de texte d'un album
    function getFontstyles () {
      $fontstyle = array();
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/album[@name='".$this->name."']/fontstyles");

        if ($resultVoyage->item(0)) {
          $resultAllStyles = $xpath->query("fontstyle", $resultVoyage->item(0));
          if ($resultAllStyles && $resultAllStyles->length > 0) {
            foreach ($resultAllStyles as $keyStyle => $style) {
              $fontstyle[$keyStyle] = new Fontstyle (
                convertFromUTF8 ($style->getAttribute('name')),
                convertFromUTF8 ($style->getAttribute('size')),
                convertFromUTF8 ($style->getAttribute('style')),
                convertFromUTF8 ($style->getAttribute('textUnderline')),
                convertFromUTF8 ($style->getAttribute('linkUnderline')),
                convertFromUTF8 ($style->getAttribute('textColor')),
                convertFromUTF8 ($style->getAttribute('linkColor')),
                convertFromUTF8 ($style->getAttribute('hoverColor')),
                convertFromUTF8 ($style->getAttribute('visitedColor'))
              );
            }
          }
        }
      }
      return $fontstyle;
    }//getFontstyles


    // Récupère les propriétés de tous les éléments de texte d'un album
    function getFontstyle ($font) {
      if ($font<>'') {
        $fontstyle = array();
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();
        if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
          $xpath = new DOMXpath($domAlbum);
          $resultVoyage = $xpath->query("/album[@name='".$this->name."']/fontstyles");

          if ($resultVoyage->item(0)) {
            $resultStyle = $xpath->query("fontstyle[@name = '".$font."']", $resultVoyage->item(0));
            if ($resultStyle->item(0)) {
              $fontstyle = new Fontstyle (
                convertFromUTF8 ($resultStyle->item(0)->getAttribute('name')),
                convertFromUTF8 ($resultStyle->item(0)->getAttribute('size')),
                convertFromUTF8 ($resultStyle->item(0)->getAttribute('style')),
                convertFromUTF8 ($resultStyle->item(0)->getAttribute('textUnderline')),
                convertFromUTF8 ($resultStyle->item(0)->getAttribute('linkUnderline')),
                convertFromUTF8 ($resultStyle->item(0)->getAttribute('textColor')),
                convertFromUTF8 ($resultStyle->item(0)->getAttribute('linkColor')),
                convertFromUTF8 ($resultStyle->item(0)->getAttribute('hoverColor')),
                convertFromUTF8 ($resultStyle->item(0)->getAttribute('visitedColor'))
              );
            }
          }
        }
        return $fontstyle;
      }
    }//getFontstyles


    // Récupère les propriétés d'un album
    function getProperties () {
      $property = null;
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/album[@name='".$this->name."']");
        if ($resultVoyage->item(0)) {
          $resultProperties = $xpath->query("properties", $resultVoyage->item(0));
          if ($resultProperties->item(0)) {
            $property = new Property (
              convertFromUTF8 ($resultProperties->item(0)->getAttribute('font')),
              convertFromUTF8 ($resultProperties->item(0)->getAttribute('bgColor')),
              convertFromUTF8 ($resultProperties->item(0)->getAttribute('bgBodyColor')),
              convertFromUTF8 ($resultProperties->item(0)->getAttribute('bgImg')),
              convertFromUTF8 ($resultProperties->item(0)->getAttribute('imgBgPos')),
              convertFromUTF8 ($resultProperties->item(0)->getAttribute('skin')),
              convertFromUTF8 ($resultProperties->item(0)->getAttribute('saveOrigImg')),
              convertFromUTF8 ($resultProperties->item(0)->getAttribute('showStats')),
              convertFromUTF8 ($resultProperties->item(0)->getAttribute('showBudget')),
              convertFromUTF8 ($resultProperties->item(0)->getAttribute('showMap')),
              convertFromUTF8 ($resultProperties->item(0)->getAttribute('showDescription')),
              convertFromUTF8 ($resultProperties->item(0)->getAttribute('showShortCut')),
              convertFromUTF8 ($resultProperties->item(0)->getAttribute('deleteEmails')),
              convertFromUTF8 ($resultProperties->item(0)->getAttribute('sendEmails')),
              convertFromUTF8 ($resultProperties->item(0)->getAttribute('dateFormat')),
              convertFromUTF8 ($resultProperties->item(0)->getAttribute('language'))
            );
          }
        }
      }
      return $property;
    }//getProperties


    // Récupère les options du menu d'un album
    function getMenuOptions () {
      $menuOptions = null;
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/album[@name='".$this->name."']");

        if ($resultVoyage->item(0)) {
          $resultMenuOptions = $xpath->query("menuOptions", $resultVoyage->item(0));
          if ($resultMenuOptions->item(0)) {
            $menuOptions = new MenuOptions (
              convertFromUTF8 ($resultMenuOptions->item(0)->getAttribute('forum')),
              convertFromUTF8 ($resultMenuOptions->item(0)->getAttribute('notebook')),
              convertFromUTF8 ($resultMenuOptions->item(0)->getAttribute('humour')),
              convertFromUTF8 ($resultMenuOptions->item(0)->getAttribute('news')),
              convertFromUTF8 ($resultMenuOptions->item(0)->getAttribute('chat')),
              convertFromUTF8 ($resultMenuOptions->item(0)->getAttribute('video')),
              convertFromUTF8 ($resultMenuOptions->item(0)->getAttribute('preparation')),
              convertFromUTF8 ($resultMenuOptions->item(0)->getAttribute('statistics')),
              convertFromUTF8 ($resultMenuOptions->item(0)->getAttribute('ranking'))
            );
          }
        }
      }
      return $menuOptions;
    }//getMenuOptions


    // Supprime un lien dans un album
    function deleteLink ($nameLink) {
      if ($nameLink) {
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();

        // Si un ancien fichier d'archive n'existe pas au préalable, crée un nouveau fichier d'archive avec les en-têtes
        if (file_exists($this->file.'.xml')) {
          @$domAlbum->load($this->file.'.xml');
          // Recherche les elements categories du fichier
          $xpath = new DOMXpath($domAlbum);
          $nodelist = $xpath->query("/album[@name='".$this->name."']/links/link[@name='".convertToUTF8($nameLink)."']");
          $node = $nodelist->item(0);
          if ($node) {
            $node->parentNode->removeChild($node);
          }
          // Sauvegarde le nouveau fichier
          @$domAlbum->save($this->file.'.xml');
        }
      }
    }//deleteLink


    // Insert ou modifie une propriété d'un élément de texte dans un album
    function editFontstyle ($fontstyle) {
      if ($fontstyle) {
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();
        if (file_exists($this->file.'.xml')) {
          $domFontstyle = new DOMDocument();
          $font_node = $domFontstyle->createElement('fontstyle');
          $font_node->setAttribute ('name', convertToUTF8($fontstyle->name));
          $font_node->setAttribute ('size', convertToUTF8($fontstyle->size));
          $font_node->setAttribute ('style', convertToUTF8($fontstyle->style));
          $font_node->setAttribute ('textUnderline', convertToUTF8($fontstyle->textUnderline));
          $font_node->setAttribute ('linkUnderline', convertToUTF8($fontstyle->linkUnderline));
          $font_node->setAttribute ('textColor', convertToUTF8($fontstyle->textColor));
          $font_node->setAttribute ('linkColor', convertToUTF8($fontstyle->linkColor));
          $font_node->setAttribute ('hoverColor', convertToUTF8($fontstyle->hoverColor));
          $font_node->setAttribute ('visitedColor', convertToUTF8($fontstyle->visitedColor));
          $domFontstyle->appendChild($font_node);
          @$domAlbum->load($this->file.'.xml');
          //if (!$domAlbum->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

          // Recherche les elements invoice de l'ancien fichier
          $xpath = new DOMXpath($domAlbum);
          $resultVoyage = $xpath->query("/album[@name='".$this->name."']/fontstyles");
          if ($resultVoyage->item(0)) {
            $nodelist = $xpath->query("fontstyle[@name='".$fontstyle->name."']", $resultVoyage->item(0));
            if ($nodelist->item(0)) {
              // Remplace l'ancien noeud existant par le nouveau
              $oldnode = $nodelist->item(0);
              $newnode = $domAlbum->importNode($domFontstyle->documentElement, true);
              $resultVoyage->item(0)->replaceChild($newnode, $oldnode);
            } else {
              $newnode = $domAlbum->importNode($domFontstyle->documentElement, true);
              $resultVoyage->item(0)->appendChild($newnode);
            }
          } else {
            $resultVoyage1 = $xpath->query("/album[@name='".$this->name."']");
            if ($resultVoyage1->item(0)) {
              $domFontstyle1 = new DOMDocument();
              $newnode = $domAlbum->importNode($domFontstyle->documentElement, true);
              $fonts_node = $domFontstyle1->createElement('fontstyles');
              $domFontstyle1->appendChild($fonts_node);
              $newnodes = $domAlbum->importNode($domFontstyle1->documentElement, true);
              $resultVoyage1->item(0)->appendChild($newnodes);
              $resultVoyage1->item(0)->getElementsByTagName('fontstyles')->item(0)->appendChild($newnode);
            }
          }
          // Sauvegarde le nouveau fichier d'archive
          @$domAlbum->save($this->file.'.xml');
        }
      }
    }//editFontstyle


    // Insert ou modifie une propriété dans un album
    function editProperties ($property) {
      if ($property) {
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();
        if (file_exists($this->file.'.xml')) {
          $domProperties = new DOMDocument();
          $prop_node = $domProperties->createElement('properties');
          $prop_node->setAttribute ('font', convertToUTF8($property->font));
          $prop_node->setAttribute ('bgColor', convertToUTF8($property->bgColor));
          $prop_node->setAttribute ('bgBodyColor', convertToUTF8($property->bgBodyColor));
          $prop_node->setAttribute ('bgImg', convertToUTF8($property->bgImg));
          $prop_node->setAttribute ('imgBgPos', convertToUTF8($property->imgBgPos));
          $prop_node->setAttribute ('skin', convertToUTF8($property->skin));
          $prop_node->setAttribute ('saveOrigImg', convertToUTF8($property->saveOrigImg));
          $prop_node->setAttribute ('showStats', convertToUTF8($property->showStats));
          $prop_node->setAttribute ('showBudget', convertToUTF8($property->showBudget));
          $prop_node->setAttribute ('showMap', convertToUTF8($property->showMap));
          $prop_node->setAttribute ('showDescription', convertToUTF8($property->showDescription));
          $prop_node->setAttribute ('showShortCut', convertToUTF8($property->showShortCut));
          $prop_node->setAttribute ('deleteEmails', convertToUTF8($property->deleteEmails));
          $prop_node->setAttribute ('sendEmails', convertToUTF8($property->sendEmails));
          $prop_node->setAttribute ('dateFormat', convertToUTF8($property->dateFormat));
          $prop_node->setAttribute ('language', convertToUTF8($property->language));
          $domProperties->appendChild($prop_node);
          @$domAlbum->load($this->file.'.xml');
          //if (!$domAlbum->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

          // Recherche les elements invoice de l'ancien fichier
          $xpath = new DOMXpath($domAlbum);
          $resultVoyage = $xpath->query("/album[@name='".$this->name."']");
          if ($resultVoyage->item(0)) {
            $nodelist = $xpath->query("properties", $resultVoyage->item(0));
            if ($nodelist->item(0)) {
              // Remplace l'ancien noeud existant par le nouveau
              $oldnode = $nodelist->item(0);
              $newnode = $domAlbum->importNode($domProperties->documentElement, true);
              $resultVoyage->item(0)->replaceChild($newnode, $oldnode);
            } else {
              $newnode = $domAlbum->importNode($domProperties->documentElement, true);
              $resultVoyage->item(0)->appendChild($newnode);
            }

          }
          // Sauvegarde le nouveau fichier d'archive
          @$domAlbum->save($this->file.'.xml');
        }
      }
    }//editProperties


    // Insert ou modifie une option du menu dans un album
    function editMenuOptions ($menuOptions) {
      if ($menuOptions) {
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();
        if (file_exists($this->file.'.xml')) {
          $domMenuOptions = new DOMDocument();
          $menuOpt_node = $domMenuOptions->createElement('menuOptions');
          $menuOpt_node->setAttribute ('forum', convertToUTF8($menuOptions->forum));
          $menuOpt_node->setAttribute ('notebook', convertToUTF8($menuOptions->notebook));
          $menuOpt_node->setAttribute ('humour', convertToUTF8($menuOptions->humour));
          $menuOpt_node->setAttribute ('news', convertToUTF8($menuOptions->news));
          $menuOpt_node->setAttribute ('chat', convertToUTF8($menuOptions->chat));
          $menuOpt_node->setAttribute ('video', convertToUTF8($menuOptions->video));
          $menuOpt_node->setAttribute ('preparation', convertToUTF8($menuOptions->preparation));
          $menuOpt_node->setAttribute ('statistics', convertToUTF8($menuOptions->statistics));
          $menuOpt_node->setAttribute ('ranking', convertToUTF8($menuOptions->ranking));
          $domMenuOptions->appendChild($menuOpt_node);
          @$domAlbum->load($this->file.'.xml');
          //if (!$domAlbum->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

          // Recherche les elements invoice de l'ancien fichier
          $xpath = new DOMXpath($domAlbum);
          $resultVoyage = $xpath->query("/album[@name='".$this->name."']");
          if ($resultVoyage->item(0)) {
            $nodelist = $xpath->query("menuOptions", $resultVoyage->item(0));
            if ($nodelist->item(0)) {
              // Remplace l'ancien noeud existant par le nouveau
              $oldnode = $nodelist->item(0);
              $newnode = $domAlbum->importNode($domMenuOptions->documentElement, true);
              $resultVoyage->item(0)->replaceChild($newnode, $oldnode);
            } else {
              $newnode = $domAlbum->importNode($domMenuOptions->documentElement, true);
              $resultVoyage->item(0)->appendChild($newnode);
            }
          }
          // Sauvegarde le nouveau fichier d'archive
          @$domAlbum->save($this->file.'.xml');
        }
      }
    }//editMenuOptions


    // Insert ou modifie le titre dans un album
    function editTitle ($title) {
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      if (file_exists($this->file.'.xml')) {
        @$domAlbum->load($this->file.'.xml');
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/album[@name='".$this->name."']");
        if ($resultVoyage->item(0)) {
          $resultVoyage->item(0)->setAttribute("title", convertToUTF8($title));
        }
        // Sauvegarde le nouveau fichier d'archive
        @$domAlbum->save($this->file.'.xml');
      }
    }//editTitle


    // Insert ou modifie l'auteur de l'album
    function editOwner ($owner) {
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      if (file_exists($this->file.'.xml')) {
        @$domAlbum->load($this->file.'.xml');
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/album[@name='".$this->name."']");
        if ($resultVoyage->item(0)) {
          $resultVoyage->item(0)->setAttribute("owner", convertToUTF8($owner));
        }
        // Sauvegarde le nouveau fichier d'archive
        @$domAlbum->save($this->file.'.xml');
      }
    }//editOwner


    // Insert ou modifie le quota dans un album
    function editQuota ($quota) {
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      if (file_exists($this->file.'.xml')) {
        @$domAlbum->load($this->file.'.xml');
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/album[@name='".$this->name."']");
        if ($resultVoyage->item(0)) {
          $resultVoyage->item(0)->setAttribute("quota", convertToUTF8($quota));
        }
        // Sauvegarde le nouveau fichier d'archive
        @$domAlbum->save($this->file.'.xml');
      }
    }//editQuota


    // Insert ou modifie l'adresse FTP dans un album
    function editFTP ($ftp) {
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      if (file_exists($this->file.'.xml')) {
        @$domAlbum->load($this->file.'.xml');
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/album[@name='".$this->name."']/ftp");
        if ($resultVoyage->item(0)) {
          $resultVoyage->item(0)->setAttribute("address", convertToUTF8($ftp));
        }
        // Sauvegarde le nouveau fichier d'archive
        @$domAlbum->save($this->file.'.xml');
      }
    }//editFTP


    // Insert ou modifie les information de la page d'accueil dans un album
    function editHome ($home) {
      if ($home) {
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();
        if (file_exists($this->file.'.xml')) {
          $domHome = new DOMDocument();
          $home_node = $domHome->createElement('home');
          $home_node->setAttribute ('message', convertToUTF8($home->message));
          $home_node->setAttribute ('image', convertToUTF8($home->image));
          $domHome->appendChild($home_node);
          @$domAlbum->load($this->file.'.xml');
          //if (!$domAlbum->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

          // Recherche les elements invoice de l'ancien fichier
          $xpath = new DOMXpath($domAlbum);
          $resultVoyage = $xpath->query("/album[@name='".$this->name."']");
          if ($resultVoyage->item(0)) {
            $nodelist = $xpath->query("home", $resultVoyage->item(0));
            if ($nodelist->item(0)) {
              // Remplace l'ancien noeud existant par le nouveau
              $oldnode = $nodelist->item(0);
              $newnode = $domAlbum->importNode($domHome->documentElement, true);
              $resultVoyage->item(0)->replaceChild($newnode, $oldnode);
            }
          }
          // Sauvegarde le nouveau fichier d'archive
          @$domAlbum->save($this->file.'.xml');
        }
      }
    }//editHome


    // Insert, modifie ou supprime les emails dans un album
    function editEmail ($emails) {
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      if (file_exists($this->file.'.xml')) {
        $domEmail = new DOMDocument();
        $emails_node = $domEmail->createElement('emails');
        if (count ($emails) > 0) {
          foreach ($emails as $email) {
            $email_node = $emails_node->appendChild($domEmail->createElement('email'));
            $email_node->setAttribute ('name', convertToUTF8($email));
          }
        }
        $domEmail->appendChild($emails_node);
        @$domAlbum->load($this->file.'.xml');
        //if (!$domAlbum->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

        // Recherche les elements invoice de l'ancien fichier
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/album[@name='".$this->name."']");
        if ($resultVoyage->item(0)) {
          $nodelist = $xpath->query("emails", $resultVoyage->item(0));
          if ($nodelist->item(0)) {
            // Remplace l'ancien noeud existant par le nouveau
            $oldnode = $nodelist->item(0);
            $newnode = $domAlbum->importNode($domEmail->documentElement, true);
            $resultVoyage->item(0)->replaceChild($newnode, $oldnode);
          }
        }
        // Sauvegarde le nouveau fichier d'archive
        @$domAlbum->save($this->file.'.xml');
      }
    }//editEmail


    // Insert un nouveau lien
    function editLink ($link, $nameToEdit='') {
      if ($link) {
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();
        if (file_exists($this->file.'.xml')) {
          $domLink = new DOMDocument();
          $link_node = $domLink->createElement('link');
          $link_node->setAttribute ('name', convertToUTF8($link->name));
          $link_node->setAttribute ('link', convertToUTF8($link->link));
          $link_node->setAttribute ('description', convertToUTF8($link->description));
        }
        $domLink->appendChild($link_node);
        @$domAlbum->load($this->file.'.xml');
        //if (!$domAlbum->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

          // Recherche les elements invoice de l'ancien fichier
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/album[@name='".$this->name."']/links");
          if ($resultVoyage->item(0)) {
            $nodelist = $xpath->query("link", $resultVoyage->item(0));
            if ($nodelist->item(0)) {
              // Remplace les anciens noeuds existant par les nouveau ou rajoute les nouveau si inexistant
              $nodeModified = false;
              foreach ($nodelist as $keyNode => $node) {
                if (convertFromUTF8 ($node->getAttribute('name')) == convertFromUTF8 ($link_node->getAttribute('name'))) {
                  $oldnode = $nodelist->item($keyNode);
                  $newnode = $domAlbum->importNode($domLink->documentElement, true);
                  $oldnode->parentNode->replaceChild($newnode, $oldnode);
                  $nodeModified = true;
                } elseif ($nameToEdit<>'' && convertFromUTF8 ($node->getAttribute('name')) == $nameToEdit) {
                  $oldnode = $nodelist->item($keyNode);
                  $newnode = $domAlbum->importNode($domLink->documentElement, true);
                  $oldnode->parentNode->replaceChild($newnode, $oldnode);
                  $nodeModified = true;
                }
              }
              if (!$nodeModified) {
                $oldnode = $nodelist->item(0);
                $newnode = $domAlbum->importNode($domLink->documentElement, true);
                $oldnode->parentNode->appendChild($newnode);
              }
            } else {
              $newnode = $domAlbum->importNode($domLink->documentElement, true);
              $resultVoyage->item(0)->appendChild($newnode);
            }
          }
          // Sauvegarde le nouveau fichier d'archive
          @$domAlbum->save($this->file.'.xml');
        }
    }//editLink


    // Récupère toutes les liens d'un album
    function switchLinks ($name1, $name2) {
      if ($name1<>'' && $name2) {
        $link1 = $this->getLink ($name1);
        $link2 = $this->getLink ($name2);
        if ($link1 && $link2) {
          $tempLink = clone($link1);
          $tempLink->name = "##Link_To_Be_Changed@@";
          $this->editLink ($tempLink, $link2->name);
          $this->editLink ($link2, $link1->name);
          $this->editLink ($link1, $tempLink->name);
        }
      }
    }//switchLinks

  }//class Availability

?>
