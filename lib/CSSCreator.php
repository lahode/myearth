<?php

  // Créé un fichier CSS avec les paramètres personnels de l'utilisateur
  function createCSSElement ($fileName, $fontstyle) {
    switch ($fontstyle->style) {
      case "0" : {$weight="normal";$style="normal";break;}
      case "1" : {$weight="bold";$style="normal";break;}
      case "2" : {$weight="normal";$style="italic";break;}
      case "3" : {$weight="bold";$style="italic";break;}
    }
    if ($fontstyle->textUnderline == 1) $textDecoration = 'underline'; else $textDecoration = 'none';
    if ($fontstyle->linkUnderline == 1) $linkDecoration = 'underline'; else $linkDecoration = 'none';
    $text = ".".$fontstyle->name." {
  font-size: ".($fontstyle->size+7)."px;
  font-style: ".$style.";
  font-weight: ".$weight.";
  text-decoration: ".$textDecoration.";
  color: ".$fontstyle->textColor.";
}

.".$fontstyle->name." A:link {
  text-decoration: ".$linkDecoration.";
  color: ".$fontstyle->linkColor.";
}

.".$fontstyle->name." A:visited {
  text-decoration: ".$linkDecoration.";
  color: ".$fontstyle->visitedColor.";
}

.".$fontstyle->name." A:active {
  text-decoration: ".$linkDecoration.";
  color: ".$fontstyle->linkColor.";
}

.".$fontstyle->name." A:hover {
  text-decoration: ".$linkDecoration.";
  color: ".$fontstyle->hoverColor.";
}

.cadre".$fontstyle->name." {
  color: ".$fontstyle->textColor.";
}
";
    if ($fileName) {
      $file = fopen($fileName, 'w');
      fwrite($file, $text);
      fclose($file);
      @chmod ($fileName, 0777);
    } else {
      return $text;
    }
  }//createCSS



  // Créé un fichier CSS avec les paramètres personnels de l'utilisateur
  function createCSSGlobal ($fileName, $property, $path, $roundCorner=false) {
    $text = "body {
  font-family: ".$property->font.";
  background-color: ".$property->bgBodyColor.";
";

  if (!$roundCorner && $property->bgImg<>'') {
    $text .= "
  background-image: url(".$path.$property->bgImg.");
";
    switch ($property->imgBgPos) {
      case 1 : {$text .= "  background-attachment: fixed;\r\n  background-repeat: no-repeat;\r\n  background-position: left top;";break;}
      case 2 : {$text .= "  background-attachment: fixed;\r\n  background-repeat: no-repeat;\r\n  background-position: center top;";break;}
      case 3 : {$text .= "  background-attachment: fixed;\r\n  background-repeat: no-repeat;\r\n  background-position: right top;";break;}
      case 4 : {$text .= "  background-attachment: fixed;\r\n  background-repeat: no-repeat;\r\n  background-position: left center;";break;}
      case 5 : {$text .= "  background-attachment: fixed;\r\n  background-repeat: no-repeat;\r\n  background-position: center center;";break;}
      case 6 : {$text .= "  background-attachment: fixed;\r\n  background-repeat: no-repeat;\r\n  background-position: right center;";break;}
      case 7 : {$text .= "  background-attachment: fixed;\r\n  background-repeat: no-repeat;\r\n  background-position: left bottom;";break;}
      case 8 : {$text .= "  background-attachment: fixed;\r\n  background-repeat: no-repeat;\r\n  background-position: center bottom;";break;}
      case 9 : {$text .= "  background-attachment: fixed;\r\n  background-repeat: no-repeat;\r\n  background-position: right bottom;";break;}
      default : {$text .= "  background-repeat: repeat;";break;}
    }
  }

  $text .= "
}

";

      $text .= ".tableBackground {
  color:".$property->bgColor.";
}

textarea {
  font-family:".$property->font.";
}

";

    if ($roundCorner) {
      $text .= ".rbroundboxMenu { background: url(pics/nt.gif) repeat; }
.rbtopMenu div { background: url(pics/tl.gif) no-repeat top left; }
.rbtopMenu { background: url(pics/tr.gif) no-repeat top right; }
.rbbotMenu div { background: url(pics/bl.gif) no-repeat bottom left; }
.rbbotMenu { background: url(pics/br.gif) no-repeat bottom right; }

.rbroundbox { background: url(pics/nt.gif) repeat; }
.rbtop div { background: url(pics/tl.gif) no-repeat top left; }
.rbtop { background: url(pics/tr.gif) no-repeat top right; }
.rbbot div { background: url(pics/bl.gif) no-repeat bottom left; }
.rbbot { background: url(pics/br.gif) no-repeat bottom right; }

";} elseif ($property->bgColor<>'') {
      $text .= ".tableBackground {
  background-color:".$property->bgColor.";
}

";
}
    if ($fileName) {
      $file = fopen($fileName, 'w');
      fwrite($file, $text);
      fclose($file);
      @chmod ($fileName, 0777);
    } else {
      return $text;
    }
  }//createCSS

?>
