<?php
  require '../vendor/autoload.php';

  include_once('../xajax/xajax.inc.php');
  include_once('../lib/smarty_functions.php');

  // Configuration xajax
  $xajax = new xajax();
  //$xajax->debugOn();
  $xajax->setCharEncoding('ISO-8859-1');
  $xajax->decodeUTF8InputOn();
//  $xajax->registerExternalFunction("showDestination","./app/showdest.php");
//  $xajax->registerExternalFunction("showCountry","./app/showcountry.php");
//  $xajax->setRequestURI("../app/showcountry.php");
//  $xajax->registerFunction("showCountry");
//  $xajax->registerExternalFunction("showDestination", "../app/showdest.php", XAJAX_POST);
//  $xajax->registerExternalFunction("showCountry", "../app/showcountry.php", XAJAX_POST);
  $xajax->setRequestURI ("../app/showtemp.php");
  $xajax->registerFunction ("showDestination");
  $xajax->registerFunction ("showCountry");
  $xajax->registerFunction ("accessSecure");

  // Initialisation des variables smarty
  $smarty = new Smarty();
  $smarty->template_dir = 'views/';
  $smarty->compile_dir = '../smarty/templates_c/';
  $smarty->config_dir = '../lib/configs/';
  $smarty->cache_dir = '../smarty/cache/';
  $smarty->registerPlugin('function', 'html_select', 'smarty_function_html_select');
  $smarty->registerPlugin('modifier', 'sslash', 'stripslashes');
  $smarty->registerPlugin('function', 'shorten', 'smarty_shorten');
  $smarty->registerPlugin('function', 'fPrice', 'smarty_fPrice');
  $smarty->registerPlugin('modifier', 'getHour', 'smarty_getHour');
  $smarty->registerPlugin('modifier', 'getMinute', 'smarty_getMinute');
  $smarty->registerPlugin('modifier', 'getSecond', 'smarty_getSecond');
  $smarty->registerPlugin('modifier', 'showHour', 'smarty_showHour');
  $smarty->registerPlugin('modifier', 'showMinute', 'smarty_showMinute');
  $smarty->registerPlugin('modifier', 'showSecond', 'smarty_showSecond');
  $smarty->registerPlugin('modifier', 'rawurldecode', 'rawurldecode');

  $domConfig = new DOMDocument();
  if (($domConfig->load('../tmp/config.xml'))) {
    $xpath = new DOMXpath($domConfig);
    $resultDefine = $xpath->query("/config/define");
    if ($resultDefine->length > 0) {
      foreach ($resultDefine as $keyDefine => $define) {
        define ($define->getAttribute('name'), $define->getAttribute('value'));
      }
    }
    $resultTimeZone = $xpath->query("/config/timezone");
    if ($resultTimeZone->length > 0) {
      foreach ($resultTimeZone as $keyTimeZone => $timezone) {
        date_default_timezone_set($timezone->getAttribute('set'));
      }
    }
    $resultFileMaxSize = $xpath->query("/config/filemaxsize");
    if ($resultFileMaxSize->length > 0) {
      foreach ($resultFileMaxSize as $keyFileMaxSize => $filemaxsize) {
        ini_set('upload_max_filesize', $filemaxsize->getAttribute('set'));
      }
    }
  }

  // Initialisation des variables principales
  $tableFont = array (
    'Georgia, sans-serif;' => 'Georgia',
    'Arial, Helvetica, sans-serif;' => 'Arial, Helvetica',
    'Times New Roman, Times, serif;' => 'Times New Roman',
    'Courier New, Courier, monospace;' => 'Courier New, Courier',
    'Tahoma, serif;' => 'Tahoma'
  );
  $tableSize = array (1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14,
                      15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22);
  $tableNbDays = array (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
  $tableQuality = array (0 => '', 1 => '*', 2 => '**', 3 => '***', 4 => '****', 5 => '*****');
  $tableLanguage = array ('fr' => 'Fran�ais', 'en' => 'English');
  $tableDateFormat = array ('d-m-Y' => '31-12-2005', 'm-d-Y' => '12-31-2005', 'Y-m-d' => '2005-01-31');
  $tableBgPosition = array ('mosaic', 'topLeft', 'topCenter', 'topRight', 'cntLeft', 'center', 'cntRight', 'bottomLft', 'bottomCnt', 'bottomRgt');
  $error = '';
?>
