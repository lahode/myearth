<?php

  // Gestion des trajets (pays et destinations) (menu Destinations, budget, send news)
  class RouteManager {

    var $file;
    var $name;
    var $rssManager;
    var $rssMessage;

    // Constructeur
    function __construct ($file, $name, $rssManager=null, $rssMessage="") {
      $this->file = $file;
      $this->name = $name;
      $this->rssManager = $rssManager;
      $this->rssMessage = $rssMessage;
    }


    // Récupère le nombre de destinations
    function countDest ($category) {
      // Ouvre le fichier XML
      $result = array();
      $domRoute = new DOMDocument();
      $resultAllMsg = array();
      if (($domRoute->load($this->file.'.xml'))) {//&& (@$domRoute->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domRoute);
        $result = $xpath->query("/route[@name='".$this->name."']/destinations/destination[@category='".$category."'][@access='public']");
        if ($result)
          return $result->length;
        else
          return 0;
      } else
        return 0;
    }//countDest


    // Récupère toutes les cat�gories d'un itin�raire
    function getCategories () {
      $category = array();
      // Ouvre le fichier XML
      $domRoute = new DOMDocument();
      if (($domRoute->load($this->file.'.xml'))) {//&& (@$domRoute->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domRoute);
        $resultVoyage = $xpath->query("/route[@name='".$this->name."']/categories");

        if ($resultVoyage->item(0)) {
          $resultAllCats = $xpath->query("category", $resultVoyage->item(0));
          if ($resultAllCats && $resultAllCats->length > 0) {
            foreach ($resultAllCats as $keyCat => $cat) {
              if ($cat->getElementsByTagName('summary')->item(0)) {
              $summary = new Summary (
                convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('general')),
                convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('population')),
                convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('politic')),
                convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('history')),
                convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('nature')),
                convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('infra')),
                convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('food')),
                convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('activity')),
                convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('budget')),
                convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('accommodation')),
                convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('transport')),
                convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('communication')),
                convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('other'))
              );
              }
              $category[$keyCat] = new Category (
                convertFromUTF8 ($cat->getAttribute('id')),
                convertFromUTF8 ($cat->getAttribute('access')),
                convertFromUTF8 ($cat->getAttribute('country')),
                convertFromUTF8 ($cat->getAttribute('arrivalCity')),
                convertFromUTF8 ($cat->getAttribute('departureCity')),
                convertFromUTF8 ($cat->getAttribute('startingDate')),
                convertFromUTF8 ($cat->getAttribute('budget_accommodation_planed')),
                convertFromUTF8 ($cat->getAttribute('budget_food_planed')),
                convertFromUTF8 ($cat->getAttribute('budget_transport_planed')),
                convertFromUTF8 ($cat->getAttribute('budget_other_planed')),
                convertFromUTF8 ($cat->getAttribute('budget_accommodation_spent')),
                convertFromUTF8 ($cat->getAttribute('budget_food_spent')),
                convertFromUTF8 ($cat->getAttribute('budget_transport_spent')),
                convertFromUTF8 ($cat->getAttribute('budget_other_spent')),
                convertFromUTF8 ($cat->getAttribute('rate')),
                $summary,
                convertFromUTF8 ($cat->getAttribute('sound')),
                convertFromUTF8 ($cat->getAttribute('map')),
                convertFromUTF8 ($cat->getAttribute('showmap')),
                convertFromUTF8 ($cat->getAttribute('allowsametime'))
              );
            }
          }
        }
      }
      return $category;
    }//getCategories


    // Récupère une catégorie de l'itinéraire selon le type en entrée
    function getCategoryBy ($getBy, $item) {
      $category = null;
      // Ouvre le fichier XML
      $domRoute = new DOMDocument();
      if (($domRoute->load($this->file.'.xml'))) {//&& (@$domRoute->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domRoute);
        $resultVoyage = $xpath->query("/route[@name='".$this->name."']/categories");

        if ($resultVoyage->item(0)) {
          $resultCat = $xpath->query("category[@".$getBy."='".$item."']", $resultVoyage->item(0));
          if ($resultCat->item(0)) {
            $cat = $resultCat->item(0);
            if ($cat->getElementsByTagName('summary')->item(0)) {
            $summary = new Summary (
              convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('general')),
              convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('population')),
              convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('politic')),
              convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('history')),
              convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('nature')),
              convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('infra')),
              convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('food')),
              convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('activity')),
              convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('budget')),
              convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('accommodation')),
              convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('transport')),
              convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('communication')),
              convertFromUTF8 ($cat->getElementsByTagName('summary')->item(0)->getAttribute('other'))
            );
            } else $summary = new Summary (convertFromUTF8 ($cat->getAttribute('text')), '', '', '', '', '', '', '', '', '', '', '', '');
            $category = new Category (
              convertFromUTF8 ($cat->getAttribute('id')),
              convertFromUTF8 ($cat->getAttribute('access')),
              convertFromUTF8 ($cat->getAttribute('country')),
              convertFromUTF8 ($cat->getAttribute('arrivalCity')),
              convertFromUTF8 ($cat->getAttribute('departureCity')),
              convertFromUTF8 ($cat->getAttribute('startingDate')),
              convertFromUTF8 ($cat->getAttribute('budget_accommodation_planed')),
              convertFromUTF8 ($cat->getAttribute('budget_food_planed')),
              convertFromUTF8 ($cat->getAttribute('budget_transport_planed')),
              convertFromUTF8 ($cat->getAttribute('budget_other_planed')),
              convertFromUTF8 ($cat->getAttribute('budget_accommodation_spent')),
              convertFromUTF8 ($cat->getAttribute('budget_food_spent')),
              convertFromUTF8 ($cat->getAttribute('budget_transport_spent')),
              convertFromUTF8 ($cat->getAttribute('budget_other_spent')),
              convertFromUTF8 ($cat->getAttribute('rate')),
              $summary,
              convertFromUTF8 ($cat->getAttribute('sound')),
              convertFromUTF8 ($cat->getAttribute('map')),
              convertFromUTF8 ($cat->getAttribute('showmap')),
              convertFromUTF8 ($cat->getAttribute('allowsametime'))
            );
          }
        }
      }
      return $category;
    }


    // Récupère toutes les destinations d'un itinéraire par catégorie
    function getDestinations ($category='', $hide=false) {
      $destination = array();
      // Ouvre le fichier XML
      $domRoute = new DOMDocument();
      if (($domRoute->load($this->file.'.xml'))) {//&& (@$domRoute->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domRoute);
        $resultVoyage = $xpath->query("/route[@name='".$this->name."']/destinations");

        if ($resultVoyage->item(0)) {
          if ($category == '') {
            $resultAllDests = $xpath->query("destination", $resultVoyage->item(0));
          } else {
            $resultAllDests = $xpath->query("destination[@category='".$category."']", $resultVoyage->item(0));
          }
          if ($resultAllDests && $resultAllDests->length > 0) {
            foreach ($resultAllDests as $keyDest => $dest) {
              $images=null;
              $resultImages = $xpath->query("images", $dest);
              if ($resultImages && $resultImages->length > 0) {
                foreach ($resultImages as $keyImage => $image) {
                  if (convertFromUTF8 ($image->getAttribute('name')) <> '') {
                    $images[$keyImage] = new ImageObj (convertFromUTF8($image->getAttribute('name')), convertFromUTF8($image->getAttribute('description')));
                  }
                }
              }
              if (!$hide || $dest->getAttribute('access')=='public') {
                $destination[$keyDest] = new DestinationObj (
                  convertFromUTF8 ($dest->getAttribute('id')),
                  convertFromUTF8 ($dest->getAttribute('category')),
                  convertFromUTF8 ($dest->getAttribute('region')),
                  convertFromUTF8 ($dest->getAttribute('city')),
                  convertFromUTF8 ($dest->getAttribute('cityName')),
                  convertFromUTF8 ($dest->getAttribute('startingDate')),
                  convertFromUTF8 ($dest->getAttribute('nbDays')),
                  convertFromUTF8 ($dest->getAttribute('arrivalBy')),
                  convertFromUTF8 ($dest->getAttribute('text')),
                  convertFromUTF8 ($dest->getAttribute('map')),
                  convertFromUTF8 ($dest->getAttribute('showmap')),
                  $images
                );
                $destination[$keyDest]->setHide ($dest->getAttribute('access'));
              }
            }
          }
        }
      }
      return $destination;
    }//getDestinations


    // Récupère toutes les destinations d'un itinéraire par catégorie
    function getFirstLastDestination ($category, $last) {
      $destination = null;
      $resultImg = null;
      // Ouvre le fichier XML
      $domRoute = new DOMDocument();
      if (($domRoute->load($this->file.'.xml'))) {//&& (@$domRoute->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domRoute);
        $resultVoyage = $xpath->query("/route[@name='".$this->name."']/destinations");
        if ($resultVoyage->item(0)) {
          if ($last)
            $resultAllDests = $xpath->query("destination[@category='".$category."'][@access='public'][not(@startingDate < (preceding-sibling::destination[@category='".$category."']/@startingDate | following-sibling::destination[@category='".$category."']/@startingDate))]", $resultVoyage->item(0));
          else
            $resultAllDests = $xpath->query("destination[@category='".$category."'][@access='public'][not(@startingDate > (preceding-sibling::destination[@category='".$category."']/@startingDate | following-sibling::destination[@category='".$category."']/@startingDate))]", $resultVoyage->item(0));
          if ($resultAllDests && $resultAllDests->length > 0) {
            $resultImages = $xpath->query("images", $resultAllDests->item(0));
            if ($resultImages->item(0)) $resultImg = new ImageObj (convertFromUTF8($resultImages->item(0)->getAttribute('name')), convertFromUTF8($resultImages->item(0)->getAttribute('description')));
            $destination = new DestinationObj (
              convertFromUTF8 ($resultAllDests->item(0)->getAttribute('id')),
              convertFromUTF8 ($resultAllDests->item(0)->getAttribute('category')),
              convertFromUTF8 ($resultAllDests->item(0)->getAttribute('region')),
              convertFromUTF8 ($resultAllDests->item(0)->getAttribute('city')),
              convertFromUTF8 ($resultAllDests->item(0)->getAttribute('cityName')),
              convertFromUTF8 ($resultAllDests->item(0)->getAttribute('startingDate')),
              convertFromUTF8 ($resultAllDests->item(0)->getAttribute('nbDays')),
              convertFromUTF8 ($resultAllDests->item(0)->getAttribute('arrivalBy')),
              convertFromUTF8 ($resultAllDests->item(0)->getAttribute('text')),
              convertFromUTF8 ($resultAllDests->item(0)->getAttribute('map')),
              convertFromUTF8 ($resultAllDests->item(0)->getAttribute('showmap')),
              $resultImg
            );
            $destination->setHide ($resultAllDests->item(0)->getAttribute('access'));
          }
        }
      }
      return $destination;
    }//getFirstLastDestination


    // Récupère toutes les destinations d'un itinéraire par catégorie
    function getLastXDestination ($number) {
      $destinations = null;
      $resultImg = null;
      $resultDest = [];
      $i=0;
      // Ouvre le fichier XML
      $domRoute = new DOMDocument();
      if (($domRoute->load($this->file.'.xml'))) {//&& (@$domRoute->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domRoute);
        $resultVoyage = $xpath->query("/route[@name='".$this->name."']/destinations");
        if ($resultVoyage->item(0)) {
          $resultAllDests = $xpath->query("destination", $resultVoyage->item(0));
          if ($resultAllDests && $resultAllDests->length > 0) {
            foreach ($resultAllDests as $keyDest => $dest) {
              $destinations[$keyDest] = new DestinationObj (
                convertFromUTF8 ($dest->getAttribute('id')),
                convertFromUTF8 ($dest->getAttribute('category')),
                convertFromUTF8 ($dest->getAttribute('region')),
                convertFromUTF8 ($dest->getAttribute('city')),
                convertFromUTF8 ($dest->getAttribute('cityName')),
                convertFromUTF8 ($dest->getAttribute('startingDate')),
                convertFromUTF8 ($dest->getAttribute('nbDays')),
                convertFromUTF8 ($dest->getAttribute('arrivalBy')),
                convertFromUTF8 ($dest->getAttribute('text')),
                convertFromUTF8 ($dest->getAttribute('map')),
                convertFromUTF8 ($dest->getAttribute('showmap')),
                null
              );
              $destinations[$keyDest]->setHide ($dest->getAttribute('access'));
            }
          }
          if ($destinations) {
            $destinations = sortObjBy($destinations, 'startingDate', SORT_DESC);
            while (count ($resultDest)<$number && isset($destinations[$i])) {
              if ($destinations[$i]->access=='public') {
                $resultDest[] = $destinations[$i];
              }
              $i++;
            }
            if ($resultDest) asort($resultDest);
          }
        }
      }
      return $resultDest;
    }//getLastXDestination


    // Récupère une destination de l'itinéraire selon le type en entrée
    function getDestinationBy ($getBy, $item) {
      $destination = null;
      // Ouvre le fichier XML
      $domRoute = new DOMDocument();
      if (($domRoute->load($this->file.'.xml'))) {//&& (@$domRoute->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domRoute);
        $resultVoyage = $xpath->query("/route[@name='".$this->name."']/destinations");

        if ($resultVoyage->item(0)) {
          $resultDest = $xpath->query("destination[@".$getBy."='".$item."']", $resultVoyage->item(0));
          if ($resultDest->item(0)) {
            $dest = $resultDest->item(0);
            $images=null;
            $resultImages = $xpath->query("images", $dest);
            if ($resultImages && $resultImages->length > 0) {
              foreach ($resultImages as $keyImage => $image) {
                if (convertFromUTF8 ($image->getAttribute('name')) <> '') {
                  $images[$keyImage] = new ImageObj (convertFromUTF8($image->getAttribute('name')), convertFromUTF8($image->getAttribute('description')));
                }
              }
            }
            $destination = new DestinationObj (
              convertFromUTF8 ($dest->getAttribute('id')),
              convertFromUTF8 ($dest->getAttribute('category')),
              convertFromUTF8 ($dest->getAttribute('region')),
              convertFromUTF8 ($dest->getAttribute('city')),
              convertFromUTF8 ($dest->getAttribute('cityName')),
              convertFromUTF8 ($dest->getAttribute('startingDate')),
              convertFromUTF8 ($dest->getAttribute('nbDays')),
              convertFromUTF8 ($dest->getAttribute('arrivalBy')),
              convertFromUTF8 ($dest->getAttribute('text')),
              convertFromUTF8 ($dest->getAttribute('map')),
              convertFromUTF8 ($dest->getAttribute('showmap')),
              $images
            );
            $destination->setHide ($dest->getAttribute('access'));
          }
        }
      }
      return $destination;
    }//getDestinationBy


    // Récupère le point de départ d'un itinéraire
    function getDeparture () {
      $departure = null;
      // Ouvre le fichier XML
      $domRoute = new DOMDocument();
      if (($domRoute->load($this->file.'.xml'))) {//&& (@$domRoute->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domRoute);
        $resultVoyage = $xpath->query("/route[@name='".$this->name."']");

        if ($resultVoyage->item(0)) {
          $resultDeparture = $xpath->query("departure", $resultVoyage->item(0));
            if ($resultDeparture->item(0)) {
            $departure = new Departure (
              convertFromUTF8 ($resultDeparture->item(0)->getAttribute('country')),
              convertFromUTF8 ($resultDeparture->item(0)->getAttribute('region')),
              convertFromUTF8 ($resultDeparture->item(0)->getAttribute('city')),
              convertFromUTF8 ($resultDeparture->item(0)->getAttribute('cityName')),
              convertFromUTF8 ($resultDeparture->item(0)->getAttribute('startingDate')),
              convertFromUTF8 ($resultDeparture->item(0)->getAttribute('endingDate')),
              convertFromUTF8 ($resultDeparture->item(0)->getAttribute('budget_accommodation_planed')),
              convertFromUTF8 ($resultDeparture->item(0)->getAttribute('budget_food_planed')),
              convertFromUTF8 ($resultDeparture->item(0)->getAttribute('budget_transport_planed')),
              convertFromUTF8 ($resultDeparture->item(0)->getAttribute('budget_other_planed'))
            );
          }
        }
      }
      return $departure;
    }//getDeparture


    // Récupère toutes les images
    function getImageByCountry () {
      $images = array();
      // Ouvre le fichier XML
      $domRoute = new DOMDocument();
      if (($domRoute->load($this->file.'.xml'))) {//&& (@$domRoute->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domRoute);
        $resultAllDests = $xpath->query("/route[@name='".$this->name."']/destinations/destination");
        if ($resultAllDests && $resultAllDests->length > 0) {
          $images=null;
          foreach ($resultAllDests as $keyDest => $dest) {
            $resultCat = $xpath->query("/route[@name='".$this->name."']/categories/category[@id='".$dest->getAttribute('category')."']");
            if ($resultCat->item(0)) {
              $country = $resultCat->item(0)->getAttribute('country');
              $resultImages = $xpath->query("images", $dest);
              if ($resultImages && $resultImages->length > 0) {
                foreach ($resultImages as $keyImage => $image) {
                  if (convertFromUTF8 ($image->getAttribute('name')) <> '') {
                    $images[$country][] = new ImageObj (convertFromUTF8($image->getAttribute('name')), convertFromUTF8($image->getAttribute('description')));
                  }
                }
              }
            }
          }
        }
      }
      return $images;
    }//getImageByCountry


    // Supprime une catégorie dans un itinéraire
    function deleteCategory ($category) {
      if ($category) {
        $destinations = $this->getDestinations ($category);
        if ($destinations) {
          foreach ($destinations as $destination) {
            $this->deleteDestinations ($category, $destination->id);
          }
        }
        // Ouvre le fichier XML
        $domRoute = new DOMDocument();

        // Si un ancien fichier d'archive n'existe pas au préalable, crée un nouveau fichier d'archive avec les en-têtes
        if (file_exists($this->file.'.xml')) {
          @$domRoute->load($this->file.'.xml');
          // Recherche les elements categories du fichier
          $xpath = new DOMXpath($domRoute);
          $nodelist = $xpath->query("/route[@name='".$this->name."']/categories/category[@id='".$category."']");
          $node = $nodelist->item(0);
          if ($node) {
            $node->parentNode->removeChild($node);
          }
          // Sauvegarde le nouveau fichier
          @$domRoute->save($this->file.'.xml');
        }
      }
    }//deleteCategory


    // Supprime une destination dans un itinéraire
    function deleteDestinations ($category, $destination) {
      if ($destination) {
        // Ouvre le fichier XML
        $domRoute = new DOMDocument();

        // Si un ancien fichier d'archive n'existe pas au préalable, crée un nouveau fichier d'archive avec les en-têtes
        if (file_exists($this->file.'.xml')) {
          @$domRoute->load($this->file.'.xml');
          // Recherche les elements categories du fichier
          $xpath = new DOMXpath($domRoute);
          if ($destination) {
            $nodelist = $xpath->query("/route[@name='".$this->name."']/destinations/destination[@id='".$destination."']");
            $node = $nodelist->item(0);
            if ($node) {
              $node->parentNode->removeChild($node);
              if ($this->rssManager) $this->rssManager->delete ($destination, $this->rssMessage);
            }
          } else {
            $nodelist = $xpath->query("/route[@name='".$this->name."']/destinations/destination[@category='".$category."']");
            if ($nodelist && $nodelist->length > 0) {
              foreach ($nodelist as $key_nodelist => $item_nodelist) {
                $node = $nodelist->item($key_nodelist);
                if ($node) {
                  $node->parentNode->removeChild($node);
                  $dest = getDestinationBy ('category', $category);
                  if ($dest && $this->rssManager) $this->rssManager->delete ($dest->id, $this->rssMessage);
                }
              }
            }
          }
          // Sauvegarde le nouveau fichier
          @$domRoute->save($this->file.'.xml');
        }
      }
    }//deleteDestinations


    // Insert ou modifie une categorie dans un itinéraire
    function editCategory ($category) {
      if ($category) {
        // Ouvre le fichier XML
        $domRoute = new DOMDocument();

        if (file_exists($this->file.'.xml')) {
          $domCategory = new DOMDocument();
          $cat_node = $domCategory->createElement('category');
          $cat_node->setAttribute ('id', convertToUTF8($category->id));
          $cat_node->setAttribute ('country', convertToUTF8($category->country));
          $cat_node->setAttribute ('access', convertToUTF8($category->access));
          $cat_node->setAttribute ('arrivalCity', convertToUTF8($category->arrivalCity));
          $cat_node->setAttribute ('departureCity', convertToUTF8($category->departureCity));
          $cat_node->setAttribute ('startingDate', convertToUTF8($category->startingDate));
          $cat_node->setAttribute ('budget_accommodation_planed', convertToUTF8($category->budget_accommodation_planed));
          $cat_node->setAttribute ('budget_food_planed', convertToUTF8($category->budget_food_planed));
          $cat_node->setAttribute ('budget_transport_planed', convertToUTF8($category->budget_transport_planed));
          $cat_node->setAttribute ('budget_other_planed', convertToUTF8($category->budget_other_planed));
          $cat_node->setAttribute ('budget_accommodation_spent', convertToUTF8($category->budget_accommodation_spent));
          $cat_node->setAttribute ('budget_food_spent', convertToUTF8($category->budget_food_spent));
          $cat_node->setAttribute ('budget_transport_spent', convertToUTF8($category->budget_transport_spent));
          $cat_node->setAttribute ('budget_other_spent', convertToUTF8($category->budget_other_spent));
          $cat_node->setAttribute ('rate', convertToUTF8($category->rate));
          $cat_node->setAttribute ('sound', convertToUTF8($category->sound));
          $cat_node->setAttribute ('map', convertToUTF8($category->map));
          $cat_node->setAttribute ('showmap', convertToUTF8($category->showmap));
          $cat_node->setAttribute ('allowsametime', convertToUTF8($category->allowsametime));
          $summary_node = $cat_node->appendChild($domCategory->createElement('summary'));
          $summary_node->setAttribute ('general', convertToUTF8($category->summary->general));
          $summary_node->setAttribute ('population', convertToUTF8($category->summary->population));
          $summary_node->setAttribute ('politic', convertToUTF8($category->summary->politic));
          $summary_node->setAttribute ('history', convertToUTF8($category->summary->history));
          $summary_node->setAttribute ('nature', convertToUTF8($category->summary->nature));
          $summary_node->setAttribute ('infra', convertToUTF8($category->summary->infra));
          $summary_node->setAttribute ('food', convertToUTF8($category->summary->food));
          $summary_node->setAttribute ('activity', convertToUTF8($category->summary->activity));
          $summary_node->setAttribute ('budget', convertToUTF8($category->summary->budget));
          $summary_node->setAttribute ('accommodation', convertToUTF8($category->summary->accommodation));
          $summary_node->setAttribute ('transport', convertToUTF8($category->summary->transport));
          $summary_node->setAttribute ('communication', convertToUTF8($category->summary->communication));
          $summary_node->setAttribute ('other', convertToUTF8($category->summary->other));
          $domCategory->appendChild($cat_node);

          @$domRoute->load($this->file.'.xml');
          //if (!$domRoute->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

          // Recherche les elements invoice de l'ancien fichier
          $xpath = new DOMXpath($domRoute);
          $resultVoyage = $xpath->query("/route[@name='".$this->name."']/categories");
          if ($resultVoyage->item(0)) {
            $nodelist = $xpath->query("category", $resultVoyage->item(0));
            if ($nodelist->item(0)) {
              // Remplace les anciens noeuds existant par les nouveau ou rajoute les nouveau si inexistant
              $nodeModified = false;
              foreach ($nodelist as $keyNode => $node) {
                  if (convertFromUTF8 ($node->getAttribute('id')) == convertFromUTF8 ($cat_node->getAttribute('id'))) {
                  $oldnode = $nodelist->item($keyNode);
                  $newnode = $domRoute->importNode($domCategory->documentElement, true);
                  $oldnode->parentNode->replaceChild($newnode, $oldnode);
                  $nodeModified = true;
                }
              }
              if (!$nodeModified) {
                $oldnode = $nodelist->item(0);
                $newnode = $domRoute->importNode($domCategory->documentElement, true);
                if (convertFromUTF8 ($oldnode->getAttribute('id')) == 0)
                  $oldnode->parentNode->replaceChild($newnode, $oldnode);
                else
                  $oldnode->parentNode->appendChild($newnode);
              }
            } else {
              $newnode = $domRoute->importNode($domCategory->documentElement, true);
              $resultVoyage->item(0)->appendChild($newnode);
            }
          }
          // Sauvegarde le nouveau fichier d'archive
          @$domRoute->save($this->file.'.xml');
        }
      }
    }//editCategory


    // Insert ou modifie une destination dans un itinéraire
    function editDestination ($destination) {
      if ($destination) {
        // Ouvre le fichier XML
        $domRoute = new DOMDocument();
        if (file_exists($this->file.'.xml')) {
          $domDestination = new DOMDocument();
          $dest_node = $domDestination->createElement('destination');
          $dest_node->setAttribute ('id', convertToUTF8($destination->id));
          $dest_node->setAttribute ('category', convertToUTF8($destination->category));
          $dest_node->setAttribute ('region', convertToUTF8($destination->region));
          $dest_node->setAttribute ('city', convertToUTF8($destination->city));
          $dest_node->setAttribute ('cityName', convertToUTF8($destination->cityName));
          $dest_node->setAttribute ('startingDate', convertToUTF8($destination->startingDate));
          $dest_node->setAttribute ('nbDays', convertToUTF8($destination->nbDays));
          $dest_node->setAttribute ('arrivalBy', convertToUTF8($destination->arrivalBy));
          $dest_node->setAttribute ('text', convertToUTF8($destination->text));
          $dest_node->setAttribute ('map', convertToUTF8($destination->map));
          $dest_node->setAttribute ('showmap', convertToUTF8($destination->showmap));
          $dest_node->setAttribute ('access', convertToUTF8($destination->access));
          if (count ($destination->images) > 0) {
            foreach ($destination->images as $image) {
              $image_node = $dest_node->appendChild($domDestination->createElement('images'));
              $image_node->setAttribute ('name', convertToUTF8($image->name));
              $image_node->setAttribute ('description', convertToUTF8($image->description));
            }
          }
          $domDestination->appendChild($dest_node);
          @$domRoute->load($this->file.'.xml');
          //if (!$domRoute->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

          // Recherche les elements invoice de l'ancien fichier
          $xpath = new DOMXpath($domRoute);
          $resultVoyage = $xpath->query("/route[@name='".$this->name."']/destinations");
          if ($resultVoyage->item(0)) {
            $nodelist = $xpath->query("destination", $resultVoyage->item(0));
            if ($nodelist->item(0)) {
              // Remplace les anciens noeuds existant par les nouveau ou rajoute les nouveau si inexistant
              $nodeModified = false;
              foreach ($nodelist as $keyNode => $node) {
                if (convertFromUTF8 ($node->getAttribute('id')) == convertFromUTF8 ($dest_node->getAttribute('id'))) {
                  $oldnode = $nodelist->item($keyNode);
                  $newnode = $domRoute->importNode($domDestination->documentElement, true);
                  $oldnode->parentNode->replaceChild($newnode, $oldnode);
                  $nodeModified = true;
                  if ($this->rssManager) {
                    $this->rssManager->delete ($destination->id, $this->rssMessage);
                    $this->rssManager->insert ($destination->id, $this->rssMessage, '', $destination->text);
                  }
                }
              }
              if (!$nodeModified) {
                $oldnode = $nodelist->item(0);
                $newnode = $domRoute->importNode($domDestination->documentElement, true);
                if (convertFromUTF8 ($oldnode->getAttribute('id')) == 0)
                  $oldnode->parentNode->replaceChild($newnode, $oldnode);
                else
                  $oldnode->parentNode->appendChild($newnode);
                if ($this->rssManager) {
                  $this->rssManager->delete ($destination->id, $this->rssMessage);
                  $this->rssManager->insert ($destination->id, $this->rssMessage, '', $destination->text);
                }
              }
            } else {
              $newnode = $domRoute->importNode($domDestination->documentElement, true);
              $resultVoyage->item(0)->appendChild($newnode);
              if ($this->rssManager) $this->rssManager->insert ($destination->id, $this->rssMessage, '', $destination->text);
            }
          }
          // Sauvegarde le nouveau fichier d'archive
          @$domRoute->save($this->file.'.xml');
        }
      }
    }//editDestination


    // Insert ou modifie un point de départ dans un itinéraire
    function editDeparture ($departure) {
      if ($departure) {
        // Ouvre le fichier XML
        $domRoute = new DOMDocument();
        if (file_exists($this->file.'.xml')) {
          $domDeparture = new DOMDocument();
          $depart_node = $domDeparture->createElement('departure');
          $depart_node->setAttribute ('country', convertToUTF8($departure->country));
          $depart_node->setAttribute ('region', convertToUTF8($departure->region));
          $depart_node->setAttribute ('city', convertToUTF8($departure->city));
          $depart_node->setAttribute ('cityName', convertToUTF8($departure->cityName));
          $depart_node->setAttribute ('startingDate', convertToUTF8($departure->startingDate));
          $depart_node->setAttribute ('endingDate', convertToUTF8($departure->endingDate));
          $depart_node->setAttribute ('budget_accommodation_planed', convertToUTF8($departure->budget_accommodation_planed));
          $depart_node->setAttribute ('budget_food_planed', convertToUTF8($departure->budget_food_planed));
          $depart_node->setAttribute ('budget_transport_planed', convertToUTF8($departure->budget_transport_planed));
          $depart_node->setAttribute ('budget_other_planed', convertToUTF8($departure->budget_other_planed));
          $domDeparture->appendChild($depart_node);
          @$domRoute->load($this->file.'.xml');
          //if (!$domRoute->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

          // Recherche les elements invoice de l'ancien fichier
          $xpath = new DOMXpath($domRoute);
          $resultVoyage = $xpath->query("/route[@name='".$this->name."']");
          if ($resultVoyage->item(0)) {
            $nodelist = $xpath->query("departure", $resultVoyage->item(0));
            if ($nodelist->item(0)) {
              // Remplace l'ancien noeud existant par le nouveau
              $oldnode = $nodelist->item(0);
              $newnode = $domRoute->importNode($domDeparture->documentElement, true);
              $resultVoyage->item(0)->replaceChild($newnode, $oldnode);
            }
          }
          // Sauvegarde le nouveau fichier d'archive
          @$domRoute->save($this->file.'.xml');
        }
      }
    }//editDeparture


  }//class RouteManager

?>
