<?php

  // Gestion du livre d'or (menu Livre d'or)
  class ForumManager {

    var $file;
    var $name;
    var $rssManager;
    var $rssMessage;

    // Constructeur
    function __construct ($file, $name, $rssManager=null, $rssMessage="") {
      $this->file = $file;
      $this->name = $name;
      $this->rssManager = $rssManager;
      $this->rssMessage = $rssMessage;
    }


    // Retourne un id pour le message
    function getNewID () {
      do {
        $id = genID();
      } while ($this->getMessage($id));
      return $id;
    }//getNewID


    // Récupère le nombre de message
    function count () {
      // Ouvre le fichier XML
      $result = array();
      $domAlbum = new DOMDocument();
      $resultAllMsg = array();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $result = $xpath->query("/forum[@name='".$this->name."']/messages/message");
        if ($result)
          return $result->length;
        else
          return 0;
      } else
        return 0;
    }//count


    // Récupère toutes les messages du forum
    function getMessages ($first=-1, $length=-1) {
      // Ouvre le fichier XML
      $result = array();
      $domAlbum = new DOMDocument();
      $resultAllMsg = array();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/forum[@name='".$this->name."']/messages");

        if ($resultVoyage->item(0)) {
          $resultAllMsg = $xpath->query("message", $resultVoyage->item(0));
          if ($resultAllMsg && $resultAllMsg->length > 0) {
            if ($first<>-1 && $length<>-1 && $this->getNbMessages($this->file, $this->name) < $first) return false;
            $sort = array();
            foreach ($resultAllMsg as $keyMsg => $message) {
              $images=null;
              $resultImages = $xpath->query("image", $message);
              if ($resultImages && $resultImages->length > 0) {
                foreach ($resultImages as $keyImage => $image) {
                  if ($image->getAttribute('name') <> '') {
                    $images[$keyImage] = new ImageObj (convertFromUTF8($image->getAttribute('name')), convertFromUTF8($image->getAttribute('description')));
                  }
                }
              }
              $messages[$keyMsg] = new MessageSender (
                convertFromUTF8($message->getAttribute('id')),
                convertFromUTF8($message->getAttribute('name')),
                convertFromUTF8($message->getAttribute('email')),
                convertFromUTF8($message->getAttribute('date')),
                convertFromUTF8($message->getAttribute('subject')),
                convertFromUTF8($message->getElementsByTagName('text')->item(0)->nodeValue),
                $images
              );
              $sort[$keyMsg]  = $message->getAttribute('date');
            }
            // Séléctionne et trie les messages à afficher
            if (count ($messages) > 0) {
              array_multisort($sort, SORT_DESC, $messages);
              $i=0;
              foreach ($messages as $key => $item) {
                if (($first==-1 && $length==-1) || ($i >= $first &&  $i < $first+$length)) {
                  $result[$i] = $item;
                }
                $i++;
              }
            }
          }
        }
      }
      return $result;
    }//getForumMessages


    // Récupère un message d'après son identifiant
    function getMessage ($id) {
      // Ouvre le fichier XML
      $result = null;
      $domAlbum = new DOMDocument();
      $resultAllMsg = array();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/forum[@name='".$this->name."']/messages");

        if ($resultVoyage->item(0)) {
          $resultMsg = $xpath->query("message[@id='".$id."']", $resultVoyage->item(0));
          if ($resultMsg->item(0)) {
            $message = $resultMsg->item(0);
            $images=null;
            $resultImages = $xpath->query("image", $message);
            if ($resultImages && $resultImages->length > 0) {
              foreach ($resultImages as $keyImage => $image) {
                if ($image->getAttribute('name') <> '') {
                  $images[$keyImage] = new ImageObj (convertFromUTF8($image->getAttribute('name')), convertFromUTF8($image->getAttribute('description')));
                }
              }
            }
            $result = new MessageSender (
              convertFromUTF8($message->getAttribute('id')),
              convertFromUTF8($message->getAttribute('name')),
              convertFromUTF8($message->getAttribute('email')),
              convertFromUTF8($message->getAttribute('date')),
              convertFromUTF8($message->getAttribute('subject')),
              convertFromUTF8($message->getElementsByTagName('text')->item(0)->nodeValue),
              $images
            );
          }
        }
      }
      return $result;
    }//getMessage


    // Récupère toutes les messages du forum
    function getNbMessages () {
      $i=0;
      $resultAllMsg = array();
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/forum[@name='".$this->name."']/messages");

        if ($resultVoyage->item(0)) {
          $resultAllMsg = $xpath->query("message", $resultVoyage->item(0));
          if ($resultAllMsg && $resultAllMsg->length > 0) {
            foreach ($resultAllMsg as $keyMsg => $message) {
              $i++;
            }
          }
        }
      }
      return $i;
    }//getNbForumMessages


    // Supprime un message dans le forum
    function deleteMessage ($id) {
      if ($id<>'') {
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();

        // Si un ancien fichier d'archive n'existe pas au préalable, crée un nouveau fichier d'archive avec les en-têtes
        if (file_exists($this->file.'.xml')) {
          @$domAlbum->load($this->file.'.xml');
          // Recherche les elements categories du fichier
          $xpath = new DOMXpath($domAlbum);
          $nodelist = $xpath->query("/forum[@name='".$this->name."']/messages/message[@id='".$id."']");
          $node = $nodelist->item(0);
          if ($node) {
            $node->parentNode->removeChild($node);
          }
          // Sauvegarde le nouveau fichier
          @$domAlbum->save($this->file.'.xml');
          if ($this->rssManager) $this->rssManager->delete ($id, $this->rssMessage);
        }
      }
    }//deleteMessage


    // Insert un message dans le forum
    function insertMessage ($message) {
      if ($message) {
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();
        if (file_exists($this->file.'.xml')) {
          $domMessage = new DOMDocument();
          $message_node = $domMessage->createElement('message');
          $message_node->setAttribute ('id', convertToUTF8($message->id));
          $message_node->setAttribute ('name', convertToUTF8($message->name));
          $message_node->setAttribute ('email', convertToUTF8($message->email));
          $message_node->setAttribute ('date', convertToUTF8($message->date));
          $message_node->setAttribute ('subject', convertToUTF8($message->subject));
          $message_node->appendChild($domMessage->createElement('text', convertToUTF8($message->text)));
          if (count ($message->images) > 0) {
            foreach ($message->images as $image) {
              $image_node = $message_node->appendChild($domMessage->createElement('image'));
              $image_node->setAttribute ('name', convertToUTF8($image->name));
              $image_node->setAttribute ('description', convertToUTF8($image->description));
            }
          }
          $domMessage->appendChild($message_node);
          @$domAlbum->load($this->file.'.xml');
          //if (!$domAlbum->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

          // Recherche les elements invoice de l'ancien fichier
          $xpath = new DOMXpath($domAlbum);
          $resultVoyage = $xpath->query("/forum[@name='".$this->name."']/messages");
          if ($resultVoyage->item(0)) {
            $newnode = $domAlbum->importNode($domMessage->documentElement, true);
            $resultVoyage->item(0)->appendChild($newnode);
          }
          // Sauvegarde le nouveau fichier d'archive
          @$domAlbum->save($this->file.'.xml');
          if ($this->rssManager) $this->rssManager->insert ($message->id, $this->rssMessage, $message->name, $message->text);
        }
      }
    }//insertMessageForum

  }//class ForumManager
  
?>
