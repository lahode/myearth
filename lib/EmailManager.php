<?php

  // Gestion des e-mails et des boîtes de messages (menu Message)
  class EmailManager {

    var $file;
    var $name;
    var $type;
    var $boxname;

    // Constructeur
    function __construct ($file, $name, $type, $boxname) {
      $this->file = $file;
      $this->name = $name;
      $this->type = $type;
      $this->boxname = $boxname;
    }

    // Récupère un email d'après son identifiant
    function getEmail ($id, $box='in') {
      // Ouvre le fichier XML
      $this->type = $box.'box';
      $result = null;
      $domAlbum = new DOMDocument();
      $resultAllEmail = array();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/mails[@name='".$this->name."']/mailboxes/mailbox[@name='".convertFromUTF8($this->boxname)."']/".$this->type."/emails");

        if ($resultVoyage->item(0)) {
          $resultEmail = $xpath->query("email[@id='".convertToUTF8($id)."']", $resultVoyage->item(0));
          if ($resultEmail->item(0)) {
            $email = $resultEmail->item(0);
            $sentTo=null;
            $resultsentTo = $email->getElementsByTagName('to');
            if ($resultsentTo && $resultsentTo->length > 0) {
              foreach ($resultsentTo as $keySentTo => $to) {
                $sentTo[$keySentTo] = convertFromUTF8($to->nodeValue);
              }
            }
            $sentCc=null;
            $resultsentCc = $email->getElementsByTagName('cc');
            if ($resultsentCc && $resultsentCc->length > 0) {
              foreach ($resultsentCc as $keySentCc => $cc) {
                $sentCc[$keySentCc] = convertFromUTF8($cc->nodeValue);
              }
            }
            $sentBcc=null;
            $resultsentBcc = $email->getElementsByTagName('bcc');
            if ($resultsentBcc && $resultsentBcc->length > 0) {
              foreach ($resultsentBcc as $keySentBcc => $bcc) {
                $sentBcc[$keySentBcc] = convertFromUTF8($bcc->nodeValue);
              }
            }
            $attachments=null;
            $resultAttachments = $xpath->query("attachment", $email);
            if ($resultAttachments && $resultAttachments->length > 0) {
              foreach ($resultAttachments as $keyAttachment => $attachment) {
                if ($attachment->getAttribute('name') <> '') {
                  $attachments[$keyAttachment] = new Attachment (convertFromUTF8($attachment->getAttribute('id')), convertFromUTF8($attachment->getAttribute('name')), convertFromUTF8($attachment->getAttribute('file')));
                }
              }
            }
            $result = new Mail (
              convertFromUTF8($email->getAttribute('id')),
              convertFromUTF8($email->getAttribute('date')),
              convertFromUTF8($email->getAttribute('from')),
              $sentTo,
              $sentCc,
              $sentBcc,
              '',
              convertFromUTF8($email->getAttribute('subject')),
              convertFromUTF8($email->getElementsByTagName('body')->item(0)->nodeValue),
              $attachments,
              convertFromUTF8($email->getAttribute('name')),
              convertFromUTF8($email->getAttribute('html')),
              convertFromUTF8($email->getAttribute('deleted'))
            );
          }
        }
      }
      return $result;
    }//getEmail


    // Récupère toutes les emails
    function getEmails ($box='in', $first=-1, $length=-1, $sortBy=false, $deleted=0) {
      // Ouvre le fichier XML
      $this->type = $box.'box';
      $emails = array();
      $result = array();
      $domAlbum = new DOMDocument();
      $resultAllEmail = array();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/mails[@name='".$this->name."']/mailboxes/mailbox[@name='".convertFromUTF8($this->boxname)."']/".$this->type."/emails");

        if ($resultVoyage->item(0)) {
          $resultAllEmail = $xpath->query("email", $resultVoyage->item(0));
          if ($resultAllEmail && $resultAllEmail->length > 0) {
            if ($first<>-1 && $length<>-1 && $resultAllEmail->length < $first) return false;
            $sort = array();
            foreach ($resultAllEmail as $keyEmail => $email) {
              $attachments=null;
              $sentTo=null;
              $resultsentTo = $email->getElementsByTagName('to');
              if ($resultsentTo && $resultsentTo->length > 0) {
                foreach ($resultsentTo as $keySentTo => $to) {
                  $sentTo[$keySentTo] = convertFromUTF8($to->nodeValue);
                }
              }
              $sentCc=null;
              $resultsentCc = $email->getElementsByTagName('cc');
              if ($resultsentCc && $resultsentCc->length > 0) {
                foreach ($resultsentCc as $keySentCc => $cc) {
                  $sentCc[$keySentCc] = convertFromUTF8($cc->nodeValue);
                }
              }
              $sentBcc=null;
              $resultsentBcc = $email->getElementsByTagName('bcc');
              if ($resultsentBcc && $resultsentBcc->length > 0) {
                foreach ($resultsentBcc as $keySentBcc => $bcc) {
                  $sentBcc[$keySentBcc] = convertFromUTF8($bcc->nodeValue);
                }
              }
              $resultAttachments = $xpath->query("attachment", $email);
              if ($resultAttachments && $resultAttachments->length > 0) {
                foreach ($resultAttachments as $keyAttachment => $attachment) {
                  if ($attachment->getAttribute('name') <> '') {
                    $attachments[$keyAttachment] = new Attachment (convertFromUTF8($attachment->getAttribute('id')), convertFromUTF8($attachment->getAttribute('name')), convertFromUTF8($attachment->getAttribute('file')));
                  }
                }
              }
              if ($email->getAttribute('deleted')==$deleted) {
                $emails[$keyEmail] = new Mail (
                  convertFromUTF8($email->getAttribute('id')),
                  convertFromUTF8($email->getAttribute('date')),
                  convertFromUTF8($email->getAttribute('from')),
                  $sentTo,
                  $sentCc,
                  $sentBcc,
                  '',
                  convertFromUTF8($email->getAttribute('subject')),
                  convertFromUTF8($email->getElementsByTagName('body')->item(0)->nodeValue),
                  $attachments,
                  convertFromUTF8($email->getAttribute('name')),
                  convertFromUTF8($email->getAttribute('html')),
                  convertFromUTF8($email->getAttribute('deleted'))
                );
              }
              if ($sortBy) $sort[$keyEmail] = $email->getAttribute($sortBy);
            }
            // Séléctionne et trie les emails à afficher
            if (count ($emails) > 0) {
              if ($sortBy) array_multisort($sort, SORT_DESC, $emails);
              $i=0;
              foreach ($emails as $key => $item) {
                if (($first==-1 && $length==-1) || ($i >= $first &&  $i < $first+$length)) {
                  $result[$i] = $item;
                }
                $i++;
              }
            }
          }
        }
      }
      return $result;
    }//getEmails


    // Récupère une boîte de message d'après son nom
    function getMailbox ($name) {
      // Ouvre le fichier XML
      $result = null;
      $domAlbum = new DOMDocument();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/mails[@name='".$this->name."']/mailboxes");

        if ($resultVoyage->item(0)) {
          $resultMailbox = $xpath->query("mailbox[@name='".convertFromUTF8($name)."']", $resultVoyage->item(0));
          if ($resultMailbox->item(0)) {
            $mailbox = $resultMailbox->item(0);
            $result = new Mailbox (
              convertFromUTF8($mailbox->getAttribute('name')),
              convertFromUTF8($mailbox->getElementsByTagName('parameter')->item(0)->getAttribute('host')),
              convertFromUTF8($mailbox->getElementsByTagName('parameter')->item(0)->getAttribute('email')),
              convertFromUTF8($mailbox->getElementsByTagName('parameter')->item(0)->getAttribute('username')),
              convertFromUTF8($mailbox->getElementsByTagName('parameter')->item(0)->getAttribute('password')),
              convertFromUTF8($mailbox->getElementsByTagName('parameter')->item(0)->getAttribute('date'))
            );
          }
        }
      }
      return $result;
    }//getMailboxes


    // Récupère toutes les boîtes de message
    function getMailboxes () {
      // Ouvre le fichier XML
      $mailboxes = array();
      $domAlbum = new DOMDocument();
      $resultAllMailboxes = array();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/mails[@name='".$this->name."']/mailboxes");

        if ($resultVoyage->item(0)) {
          $resultAllMailboxes = $xpath->query("mailbox", $resultVoyage->item(0));
          if ($resultAllMailboxes && $resultAllMailboxes->length > 0) {
            foreach ($resultAllMailboxes as $keyMailbox => $mailbox) {
              $mailboxes[$keyMailbox] = new Mailbox (
                convertFromUTF8($mailbox->getAttribute('name')),
                convertFromUTF8($mailbox->getElementsByTagName('parameter')->item(0)->getAttribute('host')),
                convertFromUTF8($mailbox->getElementsByTagName('parameter')->item(0)->getAttribute('email')),
                convertFromUTF8($mailbox->getElementsByTagName('parameter')->item(0)->getAttribute('username')),
                convertFromUTF8($mailbox->getElementsByTagName('parameter')->item(0)->getAttribute('password')),
                convertFromUTF8($mailbox->getElementsByTagName('parameter')->item(0)->getAttribute('date'))
              );
            }
          }
        }
      }
      return $mailboxes;
    }//getMailboxes


    // Supprime un email
    function deleteEmail ($id, $box='in') {
      $mailToDelete = $this->getEmail ($id);
      $mailToDelete->deleted = 1;
      $this->editEmail ($mailToDelete, $box, false, true);
    }//deleteEmail


    // Supprime un email
    function restoreEmail ($id, $box='in') {
      $mailToRestore = $this->getEmail ($id);
      $mailToRestore->deleted = 0;
      $this->editEmail ($mailToRestore, $box, false, true);
    }//Restore Email


    // Vide un email de son contenu
    function cleanEmail ($id, $box='in') {
      $mailToClean = new Mail ($id, '', '', null, null, null, '', '', '', null, '', '', -1);
      $this->editEmail ($mailToClean, $box, false, true);
    }//cleanEmail Email


    // Supprime une pièce jointe d'un email
    function deleteAttachment ($id, $fileName, $box='in') {
      // Ouvre le fichier XML
      $this->type = $box.'box';
      $domAlbum = new DOMDocument();

      // Si un ancien fichier d'archive n'existe pas au préalable, crée un nouveau fichier d'archive avec les en-têtes
      if (file_exists($this->file.'.xml')) {
        @$domAlbum->load($this->file.'.xml');
        // Recherche les elements categories du fichier
        $xpath = new DOMXpath($domAlbum);
        $nodelist = $xpath->query("/mails[@name='".$this->name."']/mailboxes/mailbox[@name='".convertFromUTF8($this->boxname)."']/".$this->type."/emails/email[@id='".convertToUTF8($id)."']/attachment[@file='".convertToUTF8($fileName)."']");
        $node = $nodelist->item(0);
        if ($node) {
          $node->parentNode->removeChild($node);
        }
        // Sauvegarde le nouveau fichier
        @$domAlbum->save($this->file.'.xml');
      }
    }//deleteEmail


    // Edite un email
    function editEmail ($email, $box, $html=0, $forceChange=false) {
      $this->type = $box.'box';
      if ($email) {
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();
        if (file_exists($this->file.'.xml')) {
          $domEmail = new DOMDocument();
          $email_node = $domEmail->createElement('email');
          $email_node->setAttribute ('id', convertToUTF8($email->id));
          $email_node->setAttribute ('date', convertToUTF8($email->date));
          $email_node->setAttribute ('from', convertToUTF8($email->from));
          $email_node->setAttribute ('subject', convertToUTF8($email->subject));
          $email_node->setAttribute ('name', convertToUTF8($email->name));
          $email_node->setAttribute ('html', convertToUTF8($html));
          $email_node->setAttribute ('deleted', convertToUTF8($email->deleted));
          $email_node->appendChild($domEmail->createElement('body', convertToUTF8($email->body)));
          if (is_array ($email->to) && count ($email->to) > 0) {
            foreach ($email->to as $to) {
              $email_node->appendChild($domEmail->createElement('to', convertToUTF8($to)));
            }
          }
          if (is_array ($email->cc) && count ($email->cc) > 0) {
            foreach ($email->cc as $cc) {
              $email_node->appendChild($domEmail->createElement('cc', convertToUTF8($cc)));
            }
          }
          if (is_array ($email->bcc) && count ($email->bcc) > 0) {
            foreach ($email->bcc as $bcc) {
              $email_node->appendChild($domEmail->createElement('bcc', convertToUTF8($bcc)));
            }
          }
          if (count ($email->attachments) > 0) {
            foreach ($email->attachments as $attachment) {
              $attachment_node = $email_node->appendChild($domEmail->createElement('attachment'));
              $attachment_node->setAttribute ('id', convertToUTF8($attachment->id));
              $attachment_node->setAttribute ('name', convertToUTF8($attachment->name));
              $attachment_node->setAttribute ('file', convertToUTF8($attachment->content));
            }
          }
          $domEmail->appendChild($email_node);
          @$domAlbum->load($this->file.'.xml');
          //if (!$domAlbum->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

          // Recherche les elements invoice de l'ancien fichier
          $xpath = new DOMXpath($domAlbum);
          $resultVoyage = $xpath->query("/mails[@name='".$this->name."']/mailboxes/mailbox[@name='".convertFromUTF8($this->boxname)."']/".$this->type."/emails");
          if ($resultVoyage->item(0)) {
            $nodelist = $xpath->query("email", $resultVoyage->item(0));
            if ($nodelist->item(0)) {
              // Remplace les anciens noeuds existant par les nouveau ou rajoute les nouveau si inexistant
              $nodeModified = false;
              foreach ($nodelist as $keyNode => $node) {
                if (convertFromUTF8 ($node->getAttribute('id')) == convertFromUTF8 ($email_node->getAttribute('id'))) {
                  $oldnode = $nodelist->item($keyNode);
                  $newnode = $domAlbum->importNode($domEmail->documentElement, true);
                  if ($forceChange) $oldnode->parentNode->replaceChild($newnode, $oldnode);
                  $nodeModified = true;
                }
              }
              if (!$nodeModified) {
                $oldnode = $nodelist->item(0);
                $newnode = $domAlbum->importNode($domEmail->documentElement, true);
                $oldnode->parentNode->appendChild($newnode);
              }
            } else {
              $newnode = $domAlbum->importNode($domEmail->documentElement, true);
              $resultVoyage->item(0)->appendChild($newnode);
            }
          }
          // Sauvegarde le nouveau fichier d'archive
          @$domAlbum->save($this->file.'.xml');
        }
      }
    }//editEmail


    // Edite un boîte de message
    function editMailbox ($mailbox) {
      if ($mailbox) {
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();
        if (file_exists($this->file.'.xml')) {
          $domMailbox = new DOMDocument();
          $mailbox_node = $domMailbox->createElement('mailbox');
          $mailbox_node->setAttribute ('name', convertToUTF8($mailbox->name));
          $param_node = $mailbox_node->appendChild($domMailbox->createElement('parameter'));
          $param_node->setAttribute ('host', convertToUTF8($mailbox->host));
          $param_node->setAttribute ('email', convertToUTF8($mailbox->email));
          $param_node->setAttribute ('username', convertToUTF8($mailbox->username));
          $param_node->setAttribute ('password', convertToUTF8($mailbox->password));
          $param_node->setAttribute ('date', convertToUTF8($mailbox->date));
          $domMailbox->appendChild($mailbox_node);
          @$domAlbum->load($this->file.'.xml');
          //if (!$domAlbum->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

          // Recherche les elements invoice de l'ancien fichier
          $xpath = new DOMXpath($domAlbum);
          $resultVoyage = $xpath->query("/mails[@name='".$this->name."']/mailboxes");
          if ($resultVoyage->item(0)) {
            $nodelist = $xpath->query("mailbox", $resultVoyage->item(0));
            if ($nodelist->item(0)) {
              // Remplace les anciens noeuds existant par les nouveau ou rajoute les nouveau si inexistant
              $nodeModified = false;
              foreach ($nodelist as $keyNode => $node) {
                if (convertFromUTF8 ($node->getAttribute('name')) == convertFromUTF8 ($mailbox_node->getAttribute('name'))) {
                  $resultParam = $xpath->query("parameter", $nodelist->item($keyNode));
                  $oldnode = $resultParam->item(0);
                  $newnode = $domAlbum->importNode($param_node, true);
                  $oldnode->parentNode->replaceChild($newnode, $oldnode);
                  $nodeModified = true;
                }
              }
              if (!$nodeModified) {
                $oldnode = $nodelist->item(0);
                $inbox_node = $mailbox_node->appendChild($domMailbox->createElement('inbox'));
                $inbox_node->appendChild($domMailbox->createElement('emails'));
                $outbox_node = $mailbox_node->appendChild($domMailbox->createElement('outbox'));
                $outbox_node->appendChild($domMailbox->createElement('emails'));
                $newnode = $domAlbum->importNode($domMailbox->documentElement, true);
                $oldnode->parentNode->appendChild($newnode);
              }
            } else {
              $inbox_node = $mailbox_node->appendChild($domMailbox->createElement('inbox'));
              $inbox_node->appendChild($domMailbox->createElement('emails'));
              $outbox_node = $mailbox_node->appendChild($domMailbox->createElement('outbox'));
              $outbox_node->appendChild($domMailbox->createElement('emails'));
              $newnode = $domAlbum->importNode($domMailbox->documentElement, true);
              $resultVoyage->item(0)->appendChild($newnode);
            }
          }
          // Sauvegarde le nouveau fichier d'archive
          @$domAlbum->save($this->file.'.xml');
        }
      }
    }//editMailbox


    // Converti un flux HTML en e-mail, en retirant ne gardant que le corps du message et en parsant les éventuels style CSS
    function convertFromHTMLEmail ($string) {
      $start_body = preg_split('/<body.*?>/', $string);
      if (isset ($start_body[1])) {
        $end_body = preg_split('/<\/body>/', $start_body[1]);
        //$end_body[0] = parseStyleHeaderOnBody ($start_body[0], $end_body[0]);
      } else $end_body[0] = $string;
      return $end_body[0];
    }//convertFromHTMLEmail


    // Permet récupérer les styles CSS de l'en-tête et les parser dans le corps du message (méthode interne)
    function parseStyleHeaderOnBody ($header, $body) {
      $start_style = preg_split('/<style.*?>/', $header);
      if (isset ($start_style[1])) {
        $end_style = preg_split('/<\/style>/', $start_style[1]);
        $style = $end_style[0];
        $catch = 0;
        $j = 0;
        $name = "";
        $old=false;
        $type = ".";
        $class = array();
        $id = array();
        for ($i=0;$i<strlen ($style);$i++) {
          if ($catch == 1 && !preg_match("/^[[:alpha:]]+$/", $style[$i])) $catch = 2;
          if ($catch == 1 && preg_match("/^[[:alpha:]]+$/", $style[$i])) $name.=$style[$i];
          if (($catch == 3 || $catch == 4) && $style[$i] == '}') {$catch = 0;$j++;}
          if ($type == '.') {
            if ($catch == 3) {if (!isset($class[$name])) {$class[$name] = $style[$i];$old=false;} elseif (!$old) {$class[$name].=$style[$i];$old=false;}}
            if ($catch == 4 && preg_match("/^[[:alpha:]]+$/", $style[$i])) {$catch=3;$class[$name].=$style[$i];}
          } else {
            if ($catch == 3) {if (!isset($id[$name])) {$id[$name] = $style[$i];$old=false;} elseif (!$old) {$id[$name].=$style[$i];$old=false;}}
            if ($catch == 4 && preg_match("/^[[:alpha:]]+$/", $style[$i])) {$catch=3;$id[$name].=$style[$i];}
          }
          if ($catch == 3 && $style[$i] == ';') $catch = 4;
          if ($catch == 2 && $style[$i] == '{') {$catch = 3;$old=true;}
          if ($catch == 0 && $style[$i] == '#' || $style[$i] == '.') {$catch = 1;$type = $style[$i];$name = "";}
        }
        return ($this->remplace_tags ($body, $class, $id));
      }
      return $body;
    }//parseStyleHeaderOnBody


    // Permet de parser les class et id CSS dans le corps de texte HTML (méthode interne)
    function remplace_tags ($string, $class, $id) {
      $array = preg_split ('/<(.*)>/U', $string, -1, PREG_SPLIT_DELIM_CAPTURE);
      $total = "";
      foreach ($array as $i=>$e) {
        if($i%2==1) {
          $attribute = '';
          $a2=explode(' ',$e);
          $tag=strtolower(array_shift($a2));
          $attr=array();
          foreach($a2 as $kv => $v)
            if(preg_match('^([^=]*)=["\']?([^"\']*)["\']?$',$v,$a3))
              $attr[strtolower($a3[1])]=$a3[2];
          krsort ($attr);
          $style = '';
          foreach ($attr as $katt=> $att) {
            if ($katt == 'class') {if (isset ($class[$att])) $style.= $class[$att];else $style.='';if ($style<>'' && $style[strlen($style)-1]<>';') $style.= ';';}
            elseif ($katt == 'id') {if (isset ($id[$att])) $style.= $id[$att];else $style.='';if ($style<>'' && $style[strlen($style)-1]<>';') $style.= ';';}
            elseif ($katt == 'style') {$style.= $attr[$katt];if ($attr[$katt][strlen($attr[$katt])-1]<>';') $style.=';';}
            else $attribute = ' '.$katt.'="'.$attr[$katt].'"';
          }
          if ($style<>'') $style=' style="'.$style.'"';
          $total.= '<'.$tag.$attribute.$style.'>';
        } else {
          $total.= $e;
        }
      }
      return $total;
    }//remplace_tags


    // Converti les balises pour la conversion des noms des émetteurs et récepteurs (méthode interne)
    function convertReceivers ($receivers, $contacts, $print) {
      $idContact = 0;
      $receiverTemp = '';
      $nameDisplayTemp = '';
      $nameTemp = '';
      if ($receivers && count ($receivers) > 0) {
        foreach ($receivers as $receiver) {
          if ($receiverTemp <> '') $receiverTemp .= ', ';
          if ($nameDisplayTemp <> '') $nameDisplayTemp .= ', ';
          if ($contacts) {
            foreach ($contacts as $contact) {
              if ($receiver<>"" && $receiver==$contact->email) {
                if ($contact->nickname=='') $nameTemp = $contact->firstname.' '.$contact->lastname;
                else $nameTemp = $contact->nickname;
                $idContact=$contact->id;
              }
            }
          }
          if ($nameTemp<>'' && $idContact) {
            if ($print==2) {
              $receiverTemp .= $nameTemp.' ['.$receiver.']';
            } elseif ($print==1) {
              $receiverTemp .= '<a href="'.PAGE_CONTACTDETAILS.'?id='.$idContact.'&actions=view" style="cursor:pointer" title="'.$receiver.'">'.$nameTemp.'</a>';
              $nameDisplayTemp .= $nameTemp;
            } else {
              $receiverTemp .= '<span style="cursor:pointer" title="'.$receiver.'">'.$nameTemp.'</span>';
              $nameDisplayTemp .= $nameTemp;
            }
          } else {
            $receiverTemp .= $receiver;
            $nameDisplayTemp .= $receiver;
          }
          $nameTemp = '';
        }
      }
      return array ($receiverTemp, $nameDisplayTemp);
    }//convertReceivers


    // Converti les noms des émetteurs ou récepteurs d'un email
    function convertNamesofEmail ($email, $contacts, $box, $msgHandler=null, $print=0) {
      $idContact = 0;
      if ($box=='out') {
        $toConverted = $this->convertReceivers ($email->to, $contacts, $print);
        $ccConverted = $this->convertReceivers ($email->cc, $contacts, $print);
        $bccConverted = $this->convertReceivers ($email->bcc, $contacts, $print);
        $email->from = '';
        $email->to = $toConverted[0];
        $email->cc = $ccConverted[0];
        $email->bcc = $bccConverted[0];
        if ($bccConverted[1] <> '') $email->name = $bccConverted[1];
        if ($ccConverted[1] <> '') $email->name = $ccConverted[1];
        if ($toConverted[1] <> '') $email->name = $toConverted[1];
      } else {
        $toConverted = $this->convertReceivers ($email->to, $contacts, $print);
        $ccConverted = $this->convertReceivers ($email->cc, $contacts, $print);
        $bccConverted = $this->convertReceivers ($email->bcc, $contacts, $print);
        $email->to = $toConverted[0];
        $email->cc = $ccConverted[0];
        $email->bcc = $bccConverted[0];
        if ($contacts) {
          foreach ($contacts as $contact) {
            if ($email->from<>"" && $email->from==$contact->email) {
              if ($contact->nickname=='') $email->name = $contact->firstname.' '.$contact->lastname;
              else $email->name = $contact->nickname;
              $idContact=$contact->id;
            }
          }
        }
        if ($email->name<>'') {
          if ($print==2) {
            $email->from = $email->name.' ['.$email->from.']';
          } else {
            if ($idContact) {
              if ($print==1) {
                $email->from = '<a href="'.PAGE_CONTACTDETAILS.'?id='.$idContact.'&actions=view" style="cursor:pointer" title="'.$email->from.'">'.$email->name.'</a>';
              } else {
                $email->from = '<span style="cursor:pointer" title="'.$email->from.'">'.$email->name.'</span>';
              }
            } else {
              if ($print==1 && $msgHandler) {
                $email->from = '<span style="cursor:pointer" title="'.$email->from.'">'.$email->name.'</span>&nbsp;<a href="'.PAGE_CONTACTDETAILS.'?id=-1&actions=edit&email='.$email->from.'"><img title="'.$msgHandler->get ('addToContact').'" src="'.PICS_PATH.'admin/addressbook.gif" /></a>';
              } else {
                $email->from = '<span style="cursor:pointer" title="'.$email->from.'">'.$email->name.'</span>';
              }
            }
          }
        }
      }
      return $email;
    }//convertNameofEmail


  }//class EmailManager

?>