<?php

  // Gestion des actions sur les m�dias
  class MediaManager {

    var $msgHandler;

    // Constructeur
    function __construct ($msgHandler) {
      $this->msgHandler = $msgHandler;
    }


    // Effectue une rotation d'image
    function rotatePicture ($image, $angle, $quality=80) {
      // Check if the file is a picture
      if(!list($width_orig,$height_orig,$ext) = getimagesize($image)) {
        $error = $this->msgHandler->getError ('pictureIncorrect');
        return $error;
      }
      else {
        switch ($ext) {
          case 1: {
            $im=imagecreatefromgif($image);
            break;
          }
          case 2: {
            $im=imagecreatefromjpeg($image);
            break;
          }
          case 3: {
            $im=imagecreatefrompng($image);
            break;
          }
          default: {
            $error = $this->msgHandler->getError ('pictureFormat');
            return $error;
          }
        }
      }
      $coul_blanc= imagecolorallocate($im, 0, 0, 0);
      $rotation = imagerotate($im, $angle, $coul_blanc);
      $fileSaved = imageJpeg($rotation, $image, $quality);
      @chmod ($image, 0777);
      imagedestroy($rotation);
    }//rotatePic


    // Modifie et sauve une image
    function saveImage($source, $destination, $saveOrig='', $filesize=2000000, $width=0, $height=0, $quality = 80, $checkOtherDest='', $type='jpeg') {
      // Si l'image est un tableau, récupère le fichier uploadé et le transforme en image
      if (is_array ($source)) {
        if (isset ($source['tmp_name'])) {
          // Vérifie si le fichier a correctement été upload�e
          if( !is_uploaded_file($source['tmp_name'])) {
            $error = $this->msgHandler->getError ('uploadImpossible');
            return $error;
          }

          // Vérifie que la taille de l'image
          if ($source['size'] > $filesize) {
            $error = $this->msgHandler->getError ('sizeToBig');
            return $error;
          }

          $srcName = $source['tmp_name'];
        } else {
          $srcName = $source['filename'];
        }
        // Vérifie que le fichier est une image
        if(!list($width_orig, $height_orig, $ext) = getimagesize($srcName)) {
          $error = $this->msgHandler->getError ('pictureIncorrect');
          return $error;
        } else {

          // Transforme l'image dans le bon format
          switch ($ext) {
            case 1: {
              $im=imagecreatefromgif($srcName);
              break;
            }
            case 2: {
              $im=imagecreatefromjpeg($srcName);
              break;
            }
            case 3: {
              $im=imagecreatefrompng($srcName);
              break;
            }
            default: {
              $error = $this->msgHandler->getError ('pictureFormat');
              return $error;
            }
          }

          // Sauvegarde l'originale de l'image
          if ($saveOrig<>'') {
            if (!@copy($srcName, $saveOrig)) {
              $error = $this->msgHandler->getError ('saveOrigError');
              return $error;
            } else {
              @chmod ($saveOrig, 0777);
            }
          }
        }

      // Si l'image est en sous forme d'une chaîne de caractère, transforme la chaîne en image
      } elseif (is_string ($source)) {
        $im = @imagecreatefromstring($source);
        if ($im === FALSE) {
          $error = $this->msgHandler->getError ('pictureGetError');
          return $error;
        }

        // Sauvegarde l'originale de l'image
        if ($saveOrig<>'') {
          if (($newfichier = fopen($saveOrig, "w+")) === FALSE) {
            $error = $this->msgHandler->getError ('saveOrigError');
            return $error;
          } else {
            fwrite ($newfichier, $source);
            fclose ($newfichier);
            @chmod ($saveOrig, 0777);
          }
        }
      }

      if (!isset ($im) || !$im) {
        $error = $this->msgHandler->getError ('pictureGetError');
        return $error;
      } else {
        $sizeX = (imagesx($im));
        $sizeY = (imagesy($im));
      }

      // Redimensionne l'image avec les nouvelles valeurs de largeur et hauteur
      if (($width > 0) && ($height > 0)) {
        $src = imagecreatetruecolor($width,$height);
        $black= imagecolorallocate($src, 0, 0, 0);
        imagecopyresampled($src, $im, 0, 0, 0, 0, $width, $height, $sizeX, $sizeY);
      } elseif (($width < 1) && ($height > 0)) {
        $ratio = ($sizeX/$sizeY)*$height;
        $src = imagecreatetruecolor($ratio,$height);
        $black= imagecolorallocate($src, 0, 0, 0);
        imagecopyresampled($src, $im, 0, 0, 0, 0, $ratio, $height, $sizeX, $sizeY);
      } elseif (($width > 0) && ($height < 1)) {
        $ratio = ($sizeY/$sizeX)*$width;
        $src = imagecreatetruecolor($width,$ratio);
        $black= imagecolorallocate($src, 0, 0, 0);
        imagecopyresampled($src, $im, 0, 0, 0, 0, $width, $ratio, $sizeX, $sizeY);
      } else {
        $src = $im;
      }

      // Enregistre l'image
      if ($checkOtherDest=='') $checkOtherDest=$destination;
      if (!file_exists($checkOtherDest)) {
        switch ($type) {
          case 'png' :   {$fileSaved = imagepng ($src, $destination, $quality);break;}
          case 'gif' :   {$fileSaved = imagegif ($src, $destination, $quality);break;}
          default :      {$fileSaved = imagejpeg($src, $destination, $quality);break;}
        }
        @chmod ($destination, 0777);
        imagedestroy($src);
      } else {
        $error = $this->msgHandler->getError ('fileExist');
        return $error;
      }

      // V�rifie que l'image a bien été enregistr�e
      if (!$fileSaved) {
        $error = $this->msgHandler->getError ('saveError');
        return $error;
      } else {
        return false;
      }
    }//saveImage


    // Sauvegarde une musique mp3
    function saveSound($source, $destination, $filesize=2000000) {
      // Check if the has been correctly uploaded
      if( !is_uploaded_file($source['tmp_name'])) {
        $error = $this->msgHandler->getError ('uploadImpossible');
        return $error;
      }
      // Check if the size of the file
      if ($source['size'] > $filesize) {
        $error = $this->msgHandler->getError ('sizeToBig');
        return $error;
      }

      // Check if the file is a mp3 file
      if ($source['type'] <> 'audio/mpeg') {
        $error = $this->msgHandler->getError ('mp3FormatError');
        return $error;
      }

      // Vérifie que le son a bien été enregistré
      if (!move_uploaded_file($source['tmp_name'], $destination)) {
        $error = $this->msgHandler->getError ('saveError');
        return $error;
      } else {
        @chmod ($destination, 0777);
        return false;
      }
    }//saveSound


    // Sauvegarde une video
    function saveVideo($source, $destination, $filesize=2000000) {
      // Check if the has been correctly uploaded
      if( !is_uploaded_file($source['tmp_name'])) {
        $error = $this->msgHandler->getError ('uploadImpossible');
        return $error;
      }
      // Check if the size of the file
      if ($source['size'] > $filesize) {
        $error = $this->msgHandler->getError ('sizeToBig');
        return $error;
      }

      // Check if the size of the file
      if (strtoupper(substr ($source['name'], -3)) <> 'AVI' && strtoupper(substr ($source['name'], -3)) <> 'MPG'  && strtoupper(substr ($source['name'], -3)) <> 'MPE' && strtoupper(substr ($source['name'], -3)) <> 'MOV' && strtoupper(substr ($source['name'], -3)) <> 'WMV' && strtoupper(substr ($source['name'], -3)) <> 'RM') {
        $error = $this->msgHandler->getError ('videoFormatError');
        return $error;
      }

      // Vérifie que la vidéo a bien été enregistrée
      if (!move_uploaded_file($source['tmp_name'], $destination)) {
        $error = $this->msgHandler->getError ('saveError');
        return $error;
      } else {
        @chmod ($destination, 0777);
        return false;
      }
    }//saveVideo


    // Crée un lien pour avoir accès au lecteur de MP3
    function createSoundLink($destination, $autoplay=0) {
      return '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" width="300" height="70" id="player" align="middle">'.
             '<param name="allowScriptAccess" value="sameDomain" />'.
             '<param name="movie" value="'.MP3PLAYER.'?son='.$destination.'&autoplay='.$autoplay.'" />'.
             '<param name="quality" value="high" />'.
             '<embed src="'.MP3PLAYER.'?son='.$destination.'&autoplay='.$autoplay.'" quality="high" width="250" height="30" name="player" align="middle" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />'.
             '</object>';
    }//createSoundLink

  }//MediaManager

?>