<?php

  // Gestion des fichiers lors de leur envoi ou réception en les stockant dans le dossier temporaire
  class FileManager {

    var $db;
    var $msgHandler;
    var $mediaManager;
    var $tempPath;
    var $tempExt;
    var $original;

    // Constructeur
    function __construct ($db, $msgHandler, $mediaManager, $tempPath, $tempExt) {
      $this->db = $db;
      $this->msgHandler = $msgHandler;
      $this->mediaManager = $mediaManager;
      $this->tempPath = $tempPath;
      $this->tempExt = $tempExt;
      $this->original = '';
    }


    // Modifie la propriété "original" pour les images
    function setOriginal ($original) {
      $this->original = $original;
    }//setOriginal


    // Initialise la session des fichiers temporaires et nettoie les précédents
    function init () {
      if (isset ($_SESSION['tempFiles']) && count ($_SESSION['tempFiles']) > 0) {
        foreach ($_SESSION['tempFiles'] as $files) {
          if (count ($files) > 0)
          foreach ($files as $keyFile => $file) {
            $this->db->connect();
            $this->db->deleteTempFile ($this->tempPath, $keyFile);
            $this->db->disconnect();
          }
        }
      }
      $_SESSION['tempFiles'] = array();
    }//init


    // Détruit la session des fichiers temporaires et nettoie les précédents
    function kill () {
      if (isset ($_SESSION['tempFiles']) && count ($_SESSION['tempFiles']) > 0) {
        foreach ($_SESSION['tempFiles'] as $files) {
          if (count ($files) > 0)
          foreach ($files as $keyFile => $file) {
            $this->db->connect();
            $this->db->deleteTempFile ($this->tempPath, $keyFile);
            $this->db->disconnect();
          }
        }
      }
      unset ($_SESSION['tempFiles']);
    }//kill


    // Retourne la session des fichiers temporaires
    function getTempFiles ($name, $id='') {
      $return = null;
      if (isset ($_SESSION['tempFiles']) && isset ($_SESSION['tempFiles'][$name])) {
        if (isset ($_SESSION['tempFiles'][$name][$id])) $return = $_SESSION['tempFiles'][$name][$id];
        else $return = $_SESSION['tempFiles'][$name];
      }
      return $return;
    }//getTempFiles


    // Modifie le contenu de tous les fichiers dans le tableau
    function setTempFiles ($name, $files) {
      if (isset ($_SESSION['tempFiles']) && isset ($_SESSION['tempFiles'][$name])) unset ($_SESSION['tempFiles'][$name]);
      $_SESSION['tempFiles'][$name] = $files;
      return $files;
    }//setTempFiles


    // Modifie le contenu d'un fichier dans le tableau
    function setTempFile ($name, $id, $new) {
      $return = null;
      if (isset ($_SESSION['tempFiles']) && isset ($_SESSION['tempFiles'][$name])) {
        if (isset ($_SESSION['tempFiles'][$name][$id])) $_SESSION['tempFiles'][$name][$id] = $new;
        $return = $_SESSION['tempFiles'][$name];
      }
      return $return;
    }//setTempFile


    // Ajoute une image de la bibliothèque dans la session des fichiers temporaires
    function addImage ($name, $path, $fileName, $description) {
      do {
        $idAt = substr(genID(), 0, 4);
      } while (isset ($_SESSION['tempFiles'][$name][$idAt]));
      $_SESSION['tempFiles'][$name][$idAt] = new ImageObj ($fileName, rawurldecode($description), $path, true);
      return $_SESSION['tempFiles'][$name];
    }//addImage


    // Ajoute un fichier de la bibliothèque dans la session des fichiers temporaires
    function addFile ($name, $path, $fileName) {
      do {
        $idAt = substr(genID(), 0, 4);
      } while (isset ($_SESSION['tempFiles'][$name][$idAt]));
      $_SESSION['tempFiles'][$name][$idAt] = new FileObj ($fileName, $path);
      return $_SESSION['tempFiles'][$name];
    }//addFile


    // Sauve les fichiers temporaires
    function saveTempFiles ($name, $path) {
      $files = $this->getTempFiles($name);
      if (count ($files) > 0) {
        $this->db->connect();
        foreach ($files as $keyFile => $file) {
          $tabType = explode ('.', $file->name);
          $idAt = '';
          if (strlen ($keyFile) <> 4) {
            while (file_exists ($path.$tabType[0].$idAt.'.'.$tabType[1])) {
              $idAt = substr(genID(), 0, 4);
            }
            if ($this->original<>'') {
              @rename ($this->tempPath.$keyFile.'_o'.$this->tempExt, $this->original.$tabType[0].$idAt.'.'.$tabType[1]);
              @chmod ($this->original.$tabType[0].$idAt.'.'.$tabType[1], 0777);
            }
            $this->db->moveTempFile ($this->tempPath, $keyFile, $path.$tabType[0].$idAt.'.'.$tabType[1]);
          }
          if (isset ($files[$keyFile]->content)) {
            $files[$keyFile]->id = $idAt;
            $files[$keyFile]->content = $tabType[0].$idAt.'.'.$tabType[1];
            $files[$keyFile]->name = $file->name;
          } else {
            $files[$keyFile]->name = $tabType[0].$idAt.'.'.$tabType[1];
          }
        }
        $this->db->disconnect();
        unset ($_SESSION['tempFiles']['name']);
      }
      return $files;
    }//saveTempFiles


    // Sauve un fichier temporaire
    function saveTempFile ($name, $path, $fileID, $delete=true) {
      $file = $this->getTempFiles($name, $fileID);
      if ($file) {
        $this->db->connect();
        $tabType = explode ('.', $file->name);
        $idAt = '';
        while (file_exists ($path.$tabType[0].$idAt.'.'.$tabType[1])) {
          $idAt = substr(genID(), 0, 4);
        }
        if ($this->original<>'') {
          @rename ($this->tempPath.$fileID.'_o'.$this->tempExt, $this->original.$tabType[0].$idAt.'.'.$tabType[1]);
          @chmod ($path.$tabType[0].$idAt.'.'.$tabType[1], 0777);
        }
        if ($delete) {
          $this->db->moveTempFile ($this->tempPath, $fileID, $path.$tabType[0].$idAt.'.'.$tabType[1]);
        } else {
          @copy ($this->tempPath.$fileID.$this->tempExt, $path.$tabType[0].$idAt.'.'.$tabType[1]);
          @chmod ($path.$tabType[0].$idAt.'.'.$tabType[1], 0777);
        }
        if (isset ($files[$fileID]->content)) {
          $file->id = $idAt;
          $file->content = $tabType[0].$idAt.'.'.$tabType[1];
          $file->name = $file->name;
        } else {
          $file->name = $tabType[0].$idAt.'.'.$tabType[1];
        }
        $this->db->disconnect();
        if ($delete) unset ($_SESSION['tempFiles'][$name]['$fileID']);
      }
      return $file;
    }//saveTempFile


    // Copie des pièces jointes aux fichiers temporaires
    function copyAttachmentToTemp ($name, $path, $attachments) {
      if (count ($attachments) > 0) {
        $this->db->connect();
        foreach ($attachments as $att) {
          $tempFileName = $this->db->addTempFile ($att->name);
          @copy($path.$att->content, $this->tempPath.$tempFileName.$this->tempExt);
          @chmod ($this->tempPath.$tempFileName.$this->tempExt, 0777);
          $_SESSION['tempFiles'][$name][$tempFileName] = new Attachment ('', $att->name, $this->tempPath.$tempFileName.$this->tempExt);
        }
        $this->db->disconnect();
      }
    }//copyAttachmentToTemp


    // Converti des pièces jointes des fichiers temporaires en mode
    function convertTempAttachmentToMail ($name) {
      $attachments = $this->getTempFiles($name);
      if (count ($attachments) > 0) {
        foreach ($attachments as $keyAttachement => $attachment) {
          $attachments[$keyAttachement]->id  = genID().$_SERVER['SERVER_NAME'];
          $attachments[$keyAttachement]->content  = $this->tempPath.$keyAttachement.$this->tempExt;
        }
      }
      return $attachments;
    }//convertTempAttachmentToMail


    // Ajoute une images aux fichiers temporaires
    function addTempImage ($name, $file, $description, $returnID=false, $tailleX=0, $tailleY=300, $quality = 80) {
      $error = "";
      $saveOriginal = "";
      $images = $this->getTempFiles($name);
      $fileName = checkFileName($file['name']);
      $checkExist = false;
      if (count ($images) > 0) {
        foreach ($images as $keyFile => $itemFile) {
          if ($itemFile->name == $fileName) $checkExist = true;
        }
      }
      if (!$checkExist) {
        $this->db->connect();
        $tempFileName = $this->db->addTempFile ($fileName);
        $this->db->disconnect();
        if ($this->original<>'') $saveOriginal = $this->tempPath.$tempFileName.'_o'.$this->tempExt;
        if (!$error=$this->mediaManager->saveImage ($file, $this->tempPath.$tempFileName.$this->tempExt, $saveOriginal, TAILLE_IMG, $tailleX, $tailleY, $quality)) {
          $images[$tempFileName] = new ImageObj ($fileName, rawurldecode($description), $this->tempPath, true);
          $_SESSION['tempFiles'][$name] = $images;
        }
      } else {
        $error = $this->msgHandler->getError ('fileExist');
      }
      if ($error) return $error;
      elseif ($returnID) return array ('id' => $tempFileName, 'obj' => $images);
      else return $images;
    }//addTempImage


    // Ajoute un fichier aux fichiers temporaires
    function addTempFile ($name, $file, $returnID=false) {
      $error = "";
      $files = $this->getTempFiles($name);
      $fileName = checkFileName($file['name']);
      $checkExist = false;
      if (count ($files) > 0) {
        foreach ($files as $keyFile => $itemFile) {
          if ($itemFile->name == $fileName) $checkExist = true;
        }
      }
      if (!$checkExist) {
        $this->db->connect();
        $tempFileName = $this->db->addTempFile ($fileName);
        $this->db->disconnect();
        if (move_uploaded_file($file['tmp_name'], $this->tempPath.$tempFileName.$this->tempExt)) {
          @chmod ($this->tempPath.$tempFileName.$this->tempExt, 0777);
          $files[$tempFileName] = new FileObj ($fileName, $this->tempPath.$tempFileName.$this->tempExt);
          $_SESSION['tempFiles'][$name] = $files;
        } else {
          $error = $this->msgHandler->getError ('uploadImpossible');
        }
      } else {
        $error = $this->msgHandler->getError ('fileExist');
      }
      if ($error) return $error;
      elseif ($returnID) return array ('id' => $tempFileName, 'obj' => $files);
      else return $files;
    }//addTempFile


    // Ajoute un fichier aux fichiers temporaires
    function addTempSound ($name, $file, $returnID=false) {
      $error = "";
      $files = $this->getTempFiles($name);
      $fileName = checkFileName($file['name']);
      $checkExist = false;
      if (count ($files) > 0) {
        foreach ($files as $keyFile => $itemFile) {
          if ($itemFile->name == $fileName) $checkExist = true;
        }
      }
      if (!$checkExist) {
        $this->db->connect();
        $tempFileName = $this->db->addTempFile ($fileName);
        $this->db->disconnect();
        if (!$error=$this->mediaManager->saveSound ($file, $this->tempPath.$tempFileName.$this->tempExt, TAILLE_SND)) {
          $files[$tempFileName] = new FileObj ($fileName, $this->tempPath.$tempFileName.$this->tempExt);
          $_SESSION['tempFiles'][$name] = $files;
        }
      } else {
        $error = $this->msgHandler->getError ('fileExist');
      }
      if ($error) return $error;
      elseif ($returnID) return array ('id' => $tempFileName, 'obj' => $files);
      else return $files;
    }//addTempFile


    // Ajoute une pièce jointe aux fichiers temporaires
    function addTempAttachment ($name, $file, $returnID=false) {
      $error = "";
      $attachment = $this->getTempFiles($name);
      $fileName = checkFileName($file['name']);
      $checkExist = false;
      if (count ($attachment) > 0) {
        foreach ($attachment as $keyFile => $itemFile) {
          if ($itemFile->name == $fileName) $checkExist = true;
        }
      }
      if (!$checkExist) {
        $this->db->connect();
        $tempFileName = $this->db->addTempFile ($fileName);
        $this->db->disconnect();
        if (move_uploaded_file($file['tmp_name'], $this->tempPath.$tempFileName.$this->tempExt)) {
          @chmod ($this->tempPath.$tempFileName.$this->tempExt, 0777);
          $attachment[$tempFileName] = new Attachment ('', $fileName, $this->tempPath.$tempFileName.$this->tempExt);
          $_SESSION['tempFiles'][$name] = $attachment;
        } else {
          $error = $this->msgHandler->getError ('uploadImpossible');
        }
      } else {
        $error = $this->msgHandler->getError ('fileExist');
      }
      if ($error) return $error;
      elseif ($returnID) return array ('id' => $tempFileName, 'obj' => $attachment);
      else return $attachment;
    }//addTempAttachment


    // Supprime un fichier temporaire
    function deleteTempFile ($name, $file) {
      if (isset ($_SESSION['tempFiles'][$name][$file])) {
        $this->db->connect();
        $this->db->deleteTempFile ($this->tempPath, $file);
        $this->db->disconnect();
        unset ($_SESSION['tempFiles'][$name][$file]);
      }
      return $_SESSION['tempFiles'][$name];
    }//deleteTempFile


    // Supprime les fichiers temporaires
    function deleteTempFiles ($name) {
      if (isset ($_SESSION['tempFiles'][$name])) {
        foreach ($_SESSION['tempFiles'] as $keyFile => $file) {
          $this->db->connect();
          $this->db->deleteTempFile ($this->tempPath, $file);
          $this->db->disconnect();
        }
        unset ($_SESSION['tempFiles'][$name]);
      }
    }//deleteTempFile


    // Déplace l'élément d'un cran vers l'avant
    function moveRight ($name, $key) {
      $return = null;
      if (isset ($_SESSION['tempFiles'][$name])) {
        $_SESSION['tempFiles'][$name] = array_move_element($_SESSION['tempFiles'][$name], $key, 'right');
        $return = $_SESSION['tempFiles'][$name];
      }
      return $return;
    }//moveRight


    // Déplace l'élément d'un cran vers l'arrière
    function moveLeft ($name, $key) {
      $return = null;
      if (isset ($_SESSION['tempFiles'][$name])) {
        $_SESSION['tempFiles'][$name] = array_move_element($_SESSION['tempFiles'][$name], $key, 'left');
        $return = $_SESSION['tempFiles'][$name];
      }
      return $return;
    }//moveLeft


  }//class FileManager

?>
