<?php
//**********************************************************
// CLASSE D'INTERFACAGE OBJET POUR UNE BD MYSQL(V4.1 ou sup)
//**********************************************************
//
//  METHODES(résumé et paramètres):
//
//      __construct($host="localhost", $user="root", $pwd="")
//          constructeur de la classe
//      __destruct()
//          destructeur de la classe   
//      Connect($bdName="")
//          connexion a un serveur BD mysql 4.1 ou sup
//      Disconnect($mode=self::cstModeError)
//          déconnexion a un serveur BD mysql 4.1 ou sup
//      SelectBd($bdName)
//          Selectione une des bases de données du serveur
//      Query($query)
//          Execute une requete dans la base en cours du serveur et retourne le résultat (si il y en a un).
//          Retourne FALSE si la requete échoue.
//      TblExist($tblName)
//          Retounre true/false selon que la table existe/n'existe pas
//      EscapeStr($str)
//          formatte une chaine de caract�e au format Mysql
//
//  UTILISATION:
//
//  - CREE L'OBJET D'INTERFACAGE A UN SERVEUR MYSQL 4.1 OU SUP
//    REMARQUE: SI LES PARAMETRES SONT OMIS "localhost", "root" et "" SERONT MIS PAR DEFAUT
//      $mysql = new InterfaceMysqli("hostname","username","pwd");
//
//  - POUR SE CONNECTER A MYSQL
//      $mysql->Connect();
//    OU POUR SE CONNECTER DIRECTEMENT A UNE DES BD
//      $mysql->Connect("nomDeLaBase");
//
//  - POUR SE DECONNECTER
//    REMARQUE: LA DECONNEXION FINALE (EN FIN DE SCRIPT) EST AUTOMATIQUE
//      $mysql->Disconnect();
//
//  - POUR SELECTIONNER UNE BD DU SERVEUR MYSQL
//      $mysql->SelectBd("nomDeLaBase");
//
//  - POUR FAIRE UNE REQUETE SIMPLE
//      $mysql->Query("CREATE TABLE `test` (`test` VARCHAR( 12 ) NOT NULL)");
//
//  - POUR FAIRE UNE REQUETE RETOURNANT DES RESULTATS
//      $result = $mysql->Query("SELECT * FROM `test`");
//    LA METHODE RETOURNE FALSE SI AUCUNE DONNEES N'EST RETOUNREE
//      if (!$mysql->Query("SELECT * FROM `tblUser` WHERE username='Nicolas'")) {echo ("ce username n'existe pas");}
//
//  - POUR SAVOIR SI UNE TABLE EXISTE
//      If ($mysql->TblExist("nomDeLaTable")) { ... }
//
//  - POUR ENLEVER LES CARACTERES QUE LA BD NE SUPPORTE PAS
//      $valueToInsert = mysql->EscapeStr("c'est une valeur avec des ' que mysql n'aime pas"); 
//      
//
//  REMARQUE:
//		- Le module Mysqli doit être activé pour php 5 ou +
//*********************************************************
// DECLARATION DE LA CLASSE
//*********************************************************
Class InterfaceMysqli{
  protected $hostname, $username, $pwd, $bdLink, $bdName;
  const cstErrConnect = "Impossible de se connecter � la base de donn�es";
  const cstErrSelectBD = "Impossible de selectionner la base de donn�es";
  const cstErrReconnect = "Connexion � la BD impossible, une autre conexion est d�j� en cours";
  const cstErrConnInactive = "Impossible car la connexion � la BD est inactive";
  const cstErrQuery = "La requete � echou�e";
  const cstModeNoError = 0;
  const cstModeError = 1;

  function __construct($host="localhost", $user="root", $pwd="") {
    $this->hostname = $host;
    $this->username = $user;
    $this->pwd = $pwd;
    unset($this->bdLink);
  }

  function __destruct(){self::Disconnect(self::cstModeNoError);}

  function Connect($bdName=""){
    self::GestionErreur(isset($this->bdLink), "Connect - ".self::cstErrReconnect);
    $this->bdLink = @mysqli_connect($this->hostname, $this->username, $this->pwd);
    self::GestionErreur(!$this->bdLink, 'Connect - '.self::cstErrConnect.' '.$this->hostname);
    if ($bdName!="") {self::SelectBd($bdName);}
  }

  function Disconnect($mode=self::cstModeError){
    if ($mode==self::cstModeError) {self::GestionErreur(!isset($this->bdLink), "Disconnect - ".self::cstErrConnInactive);}
    @mysqli_close($this->bdLink);
    unset($this->bdLink);
  }

  function SelectBd($bdName){
    self::GestionErreur(!isset($this->bdLink), "SelectBd - ".self::cstErrConnInactive);
    $this->bdName = $bdName;
    self::GestionErreur(!@mysqli_select_db($this->bdLink, $bdName),"SelectBd - ".self::cstErrSelectBD.' '.$this->bdName);
  }

  function Query($query){
  	self::GestionErreur(!isset($this->bdLink), "Query - ".self::cstErrConnInactive);
  	self::GestionErreur(!@mysqli_real_query($this->bdLink,  $query),"Query - ".self::cstErrQuery.' '.$query);  	
  	//si c'est une requête qui n'est pas censé ramener qqchose on stop
  	if (@mysqli_field_count($this->bdLink) == 0) {return true;}
  	$result = @mysqli_store_result($this->bdLink);  	
  	if (@mysqli_num_rows($result)>0) {  	    	  
  	  return $result;
  	} else {
  	  return false; 
  	}
  }
  
  function TblExist($tblName){
    self::GestionErreur(!isset($this->bdLink), "TblExist - ".self::cstErrConnInactive);
    return self::Query("SHOW TABLES FROM ".$this->bdName." LIKE '".$tblName."'");
  }
  
  function EscapeStr($str){
   return mysqli_real_escape_string ($this->bdLink, $str);
  }
  
  protected function GestionErreur($testErreur, $msgErreur){
  	if (isset ($this->bdLink) && $this->bdLink) {
  		$phpError = mysqli_error($this->bdLink);
  		$phpErrorNum = mysqli_errno($this->bdLink);
  	}else{
  		$phpError = mysqli_connect_error();
  		$phpErrorNum = mysqli_connect_errno();
  	}
  	if ($phpErrorNum!=0) $msgPhpError = 'Error n�'.$phpErrorNum.': '.$phpError; else $msgPhpError = '';
    if ($testErreur) {die($msgErreur.'<br/>'.$msgPhpError);}
  }
}

?>
