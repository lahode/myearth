<?php

  // Gestion des coûts du budget (menu Budget)
  class CostsManager {

    var $file;
    var $name;

    // Constructeur
    function __construct ($file, $name) {
      $this->file = $file;
      $this->name = $name;
    }


    // Récupère toutes les coûts d'un album
    function getCosts ($type=null, $category=null) {
      $costs = array();
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/budget[@name='".$this->name."']/costs");

        if ($resultVoyage->item(0)) {
          if (is_numeric ($type)) {
            if (is_numeric ($category)) {
              $resultAllCosts = $xpath->query("cost[@element='".$type."' and @category='".$category."']", $resultVoyage->item(0));
            } else {
              $resultAllCosts = $xpath->query("cost[@element='".$type."']", $resultVoyage->item(0));
            }
          } else {
            $resultAllCosts = $xpath->query("cost", $resultVoyage->item(0));
          }
          if ($resultAllCosts && $resultAllCosts->length > 0) {
            foreach ($resultAllCosts as $keyCost => $cost) {
              $costs[$keyCost] = new Costs (
                convertFromUTF8 ($cost->getAttribute('id')),
                convertFromUTF8 ($cost->getAttribute('category')),
                convertFromUTF8 ($cost->getAttribute('element')),
                convertFromUTF8 ($cost->getAttribute('date')),
                convertFromUTF8 ($cost->getAttribute('type')),
                convertFromUTF8 ($cost->getAttribute('departureCity')),
                convertFromUTF8 ($cost->getAttribute('arrivalCity')),
                convertFromUTF8 ($cost->getAttribute('name')),
                convertFromUTF8 ($cost->getAttribute('price')),
                convertFromUTF8 ($cost->getAttribute('quality')),
                convertFromUTF8 ($cost->getAttribute('address')),
                convertFromUTF8 ($cost->getAttribute('description')),
                convertFromUTF8 ($cost->getAttribute('duration')),
                convertFromUTF8 ($cost->getAttribute('length'))
              );
            }
          }
        }
      }
      return $costs;
    }//getCosts


    // Récupère un coût de l'album selon le type en entrée
    function getCostBy ($getBy, $item) {
      $cost = null;
      // Ouvre le fichier XML
      $domAlbum = new DOMDocument();
      if (($domAlbum->load($this->file.'.xml'))) {//&& (@$domAlbum->schemaValidate($this->file.'XSD'))) {
        $xpath = new DOMXpath($domAlbum);
        $resultVoyage = $xpath->query("/budget[@name='".$this->name."']/costs");

        if ($resultVoyage->item(0)) {
          $resultCost = $xpath->query("cost[@".$getBy."='".$item."']", $resultVoyage->item(0));
          if ($resultCost->item(0)) {
            $costItem = $resultCost->item(0);
            $cost = new Costs (
              convertFromUTF8 ($costItem->getAttribute('id')),
              convertFromUTF8 ($costItem->getAttribute('category')),
              convertFromUTF8 ($costItem->getAttribute('element')),
              convertFromUTF8 ($costItem->getAttribute('date')),
              convertFromUTF8 ($costItem->getAttribute('type')),
              convertFromUTF8 ($costItem->getAttribute('departureCity')),
              convertFromUTF8 ($costItem->getAttribute('arrivalCity')),
              convertFromUTF8 ($costItem->getAttribute('name')),
              convertFromUTF8 ($costItem->getAttribute('price')),
              convertFromUTF8 ($costItem->getAttribute('quality')),
              convertFromUTF8 ($costItem->getAttribute('address')),
              convertFromUTF8 ($costItem->getAttribute('description')),
              convertFromUTF8 ($costItem->getAttribute('duration')),
              convertFromUTF8 ($costItem->getAttribute('length'))
            );
          }
        }
      }
      return $cost;
    }//getCostBy


    // Supprime un coût dans un album
    function deleteCost ($id) {
      if ($id) {
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();

        // Si un ancien fichier d'archive n'existe pas au préalable, crée un nouveau fichier d'archive avec les en-têtes
        if (file_exists($this->file.'.xml')) {
          @$domAlbum->load($this->file.'.xml');
          // Recherche les elements co�ts du fichier
          $xpath = new DOMXpath($domAlbum);
          $nodelist = $xpath->query("/budget[@name='".$this->name."']/costs/cost[@id='".convertToUTF8($id)."']");
          $node = $nodelist->item(0);
          if ($node) {
            $node->parentNode->removeChild($node);
          }
          // Sauvegarde le nouveau fichier
          @$domAlbum->save($this->file.'.xml');
        }
      }
    }//deleteCost


    // Insert ou modifie un coût dans un album
    function editCost ($cost) {
      if ($cost) {
        // Ouvre le fichier XML
        $domAlbum = new DOMDocument();

        if (file_exists($this->file.'.xml')) {
          $domCost = new DOMDocument();
          $cost_node = $domCost->createElement('cost');
          $cost_node->setAttribute ('id', utf8_encode($cost->id));
          $cost_node->setAttribute ('category', utf8_encode($cost->category));
          $cost_node->setAttribute ('element', utf8_encode($cost->element));
          $cost_node->setAttribute ('date', utf8_encode($cost->date));
          $cost_node->setAttribute ('type', utf8_encode($cost->type));
          $cost_node->setAttribute ('departureCity', utf8_encode($cost->departureCity));
          $cost_node->setAttribute ('arrivalCity', utf8_encode($cost->arrivalCity));
          $cost_node->setAttribute ('name', utf8_encode($cost->name));
          $cost_node->setAttribute ('price', utf8_encode($cost->price));
          $cost_node->setAttribute ('quality', utf8_encode($cost->quality));
          $cost_node->setAttribute ('address', utf8_encode($cost->address));
          $cost_node->setAttribute ('description', utf8_encode($cost->description));
          $cost_node->setAttribute ('duration', utf8_encode($cost->duration));
          $cost_node->setAttribute ('length', utf8_encode($cost->length));
          $domCost->appendChild($cost_node);

          @$domAlbum->load($this->file.'.xml');
          //if (!$domAlbum->schemaValidate(ARCHIVE_XSD)) return $msgHandler->getError('balanceSheetProblem', $language).$msgHandler->getError('noXML', $language);

          // Recherche les elements invoice de l'ancien fichier
          $xpath = new DOMXpath($domAlbum);
          $resultVoyage = $xpath->query("/budget[@name='".$this->name."']/costs");
          if ($resultVoyage->item(0)) {
            $nodelist = $xpath->query("cost", $resultVoyage->item(0));
            if ($nodelist->item(0)) {
              // Remplace les anciens noeuds existant par les nouveau ou rajoute les nouveau si inexistant
              $nodeModified = false;
              foreach ($nodelist as $keyNode => $node) {
                  if (convertFromUTF8 ($node->getAttribute('id')) == convertFromUTF8 ($cost_node->getAttribute('id'))) {
                  $oldnode = $nodelist->item($keyNode);
                  $newnode = $domAlbum->importNode($domCost->documentElement, true);
                  $oldnode->parentNode->replaceChild($newnode, $oldnode);
                  $nodeModified = true;
                }
              }
              if (!$nodeModified) {
                $oldnode = $nodelist->item(0);
                $newnode = $domAlbum->importNode($domCost->documentElement, true);
                if (convertFromUTF8 ($oldnode->getAttribute('id')) == 0)
                  $oldnode->parentNode->replaceChild($newnode, $oldnode);
                else
                  $oldnode->parentNode->appendChild($newnode);
              }
            } else {
              $newnode = $domAlbum->importNode($domCost->documentElement, true);
              $resultVoyage->item(0)->appendChild($newnode);
            }
          }
          // Sauvegarde le nouveau fichier d'archive
          @$domAlbum->save($this->file.'.xml');
        }
      }
    }//editCost

  }//class CostsManager

?>
