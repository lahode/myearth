<?php

  function printt ($table) {
    print ('<pre>');
    var_dump ($table);
    print ('</pre>');
  }


  // Vérifie dans une connexion ftp si $folder en entrée est un dossier
  function ftp_is_dir($conn_id, $folder) {
    if (@ftp_chdir($conn_id, $folder)) {
      ftp_chdir($conn_id, '..');
      return true;
    } else {
      return false;
    }
  }//ftp_is_dir


  // Retourne dans une connexion ftp, la liste de tous les fichiers et dossiers présent à un endroit
  function ftpList($conn_id, $path) {
    $contents = ftp_nlist($conn_id, $path);
    $list = array ();
    $i = 0;
    foreach($contents as $file) {
      if ($file!='.'&&$file!='..') {
        if (ftp_is_dir($conn_id, $file)) {
          $list[$i]['type'] = 'folder';
        } else {
          $list[$i]['type'] = 'file';
        }
        $list[$i]['name'] = $file;
        $i++;
      }
    }
    return $list;
  }//ftpList


  // Formate les heures soit en H soit HH d'après une date
  function getHour ($date, $show=false) {
    if ($date=='') $date = 0;
    $result = date ('H', $date);
    if (!$show) $result = $result+0;
    return $result;
  }//getHour


  // Retourne les heures formatées HH d'après une date
  function showHour ($date) {
    return getHour ($date, true);
  }//showHour


  // Formate les minutes soit en M soit MM d'après une date
  function getMinute ($date, $show=false) {
    if ($date=='') $date = 0;
    $result = date ('i', $date);
    if (!$show) $result = $result+0;
    return $result;
  }//getMinute


  // Retourne les minutes formatées MM d'après une date
  function showMinute ($date) {
    return getMinute ($date, true);
  }//showMinute


  // Formate les secondes soit en S soit SS d'après une date
  function getSecond ($date, $show=false) {
    if ($date=='') $date = 0;
    $result = date ('s', $date);
    if (!$show) $result = $result+0;
    return $result;
  }//getSecond


  // Retourne les secondes formatés SS d'après une date
  function showSecond ($date) {
    return getSecond ($date, true);
  }//showSecond


  // Encode une chaîne de caractères en format UTF-8
  function convertToUTF8 ($string) {
    return utf8_encode(htmlspecialchars ($string, ENT_QUOTES));
  }//convertToUTF8


  // Décode une chaîne de caractères du format UTF-8
  function convertFromUTF8 ($string) {
    return htmlspecialchars_decode (utf8_decode($string), ENT_QUOTES);
  }//convertFromUTF8


  // Formate un nombre
  function nformat ($number) {
    return number_format($number, 0, ',', '\'');
  }//nformat


  // Insertion d'un élément dans une liste
  function array_insert (&$array, $position, $insert_array) {
    if (count ($array) > 0 && count ($insert_array) > 0) {
      $first_array = array_splice ($array, 0, $position);
      $array = array_merge ($first_array, $insert_array, $array);
    }
  }//array_insert


  // Converti un message en news
  function convertToNews ($smarty, $message, $language, $albumName, $imgPath, $dnsUrl='', $email=false, $nl2br=true) {
    $msgHandler = new Language ($language);
    $attachments = null;
    if (isset ($message->body)) {
      if ($nl2br) {
        $message->body = htmlentities($message->body, ENT_QUOTES);
        $message->body = htmlspecialchars_decode($message->body, ENT_NOQUOTES);
        $message->body = nl2br($message->body);
      }
      $message->body = stripslashes($message->body);
    } else {
      if ($nl2br) {
        $message->text = htmlentities($message->text, ENT_QUOTES);
        $message->text = nl2br($message->text);
      }
      $message->text = stripslashes($message->text);
    }
    $message->subject = stripslashes($message->subject);
    $smarty->assign ('userLanguage', $language);
    $smarty->assign ('picPath', PICS_PATH);
    $smarty->assign ('imgPath', $imgPath);
    $smarty->assign ('titleNews', htmlentities($message->subject, ENT_QUOTES));
    $smarty->assign ('msgNews', $message);

    // Création d'une news à envoyer par e-mail
    if ($email) {
      // Récupération des images à attacher
      $imagesToAttach = null;
      if (isset ($message->images)) {
        if ($message->images && count ($message->images) > 0 && $message->images[0]) {
          foreach ($message->images as $keyImg => $img) {
            $imagesToAttach[$keyImg]['path'] = $imgPath;
            $imagesToAttach[$keyImg]['name'] = $img->name;
          }
        }
      }
      $lastCount = count ($imagesToAttach);
      $imagesToAttach[$lastCount]['path'] = PICS_PATH;
      $imagesToAttach[$lastCount]['name'] = 'news.gif';
      // Création des attachments
      foreach ($imagesToAttach as $keyFile => $img) {
        $attachments[$keyFile] = new Attachment (genID().$_SERVER['SERVER_NAME'], $img['name'], $img['path'].$img['name']);
      }
      // Récupère les CSS
      $fileCSS1 = USERS_PATH.$albumName.'/css/perso.css';
      $fileCSS2 = CSS_PATH.'style.css';
      $fileCSS3 = USERS_PATH.$albumName.'/css/sitetitle.css';
      $fileCSS4 = USERS_PATH.$albumName.'/css/persotext.css';
      $css_file = fopen($fileCSS1, 'r');
      $css = fread ($css_file, filesize ($fileCSS1));
      fclose($css_file);
      unset ($css_file);
      $css_file = fopen($fileCSS2, 'r');
      $css .= "\r\n".fread ($css_file, filesize ($fileCSS2));
      fclose($css_file);
      unset ($css_file);
      $css_file = fopen($fileCSS3, 'r');
      $css .= "\r\n".fread ($css_file, filesize ($fileCSS3));
      fclose($css_file);
      unset ($css_file);
      $css_file = fopen($fileCSS4, 'r');
      $css .= "\r\n".fread ($css_file, filesize ($fileCSS4));
      fclose($css_file);
      unset ($css_file);
      // Création de l'en-tête et pied de page
      $headerNews = "<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\r\n<style type=\"text/css\" media=\"screen\">\r\n".$css."</style>\r\n</head>\r\n<body>\r\n";
      if ($dnsUrl) {
        $footerNews = "\r\n<p class=\"normalT\" align=\"center\"><a href=\"http://".$dnsUrl."/app/login.php?album=".$albumName."\">".$msgHandler->get('joinme')."</a></p><br />";
        $footerNews .= "\r\n<p class=\"normalT\" align=\"center\">".$msgHandler->get('unregisternews')." <a href=\"http://".$dnsUrl."/app/external.php?unregister=******&album=".$albumName."\">".$msgHandler->get('clickhere')."</a></p><br />\r\n</body>\r\n</html>";
      } else {
        $footerNews = "\r\n</body>\r\n</html>";
      }
      $smarty->assign ('headerNews', $headerNews);
      $smarty->assign ('footerNews', $footerNews);
      $smarty->assign ('lastAttach', count ($attachments)-1);
      $smarty->assign ('attachments', $attachments);
    }
    if (isset ($message->body)) {
      $message->body = $smarty->fetch ('newsDisplay.tpl');
    } else {
      $message->text = $smarty->fetch ('newsDisplay.tpl');
    }
    return array($message, $attachments);
  }//convertToNews


  // Objet permettant d'afficher les résultats par groupe
  class SplitDisplay {
    var $first;
    var $length;
    var $maxNum;
    var $start;
    var $loop;

    // Constructeur de l'objet SplitDisplay
    function __construct ($len, $max) {
      $this->first=0;
      $this->start=0;
      $this->length=$len;
      $this->maxNum=$max;
      $this->loop=0;
      $this->total=0;
    }
  }//SplitDislay


  // Objet de gestion des emoticons
  class Emoticon {
    var $file;
    var $name;
    var $shortcut;

    // Constructeur de l'objet Emoticon
    function __construct ($file, $name, $shortcut){
      $this->file = $file;
      $this->name = $name;
      $this->shortcut = $shortcut;
    }
    
    // M�thode statique consistant à récupérer tous les émoticons
    static function getEmotes ($path) {
      $i=0;
      $dir = opendir($path);
      while ($f = readdir($dir)) {
        if(is_file($path.$f)) {
          $tab1 = explode("_", $f);
          $tabF = explode(".gif", $tab1[1]);
          $list[$i] = new Emoticon ($f, $tabF[0], '[:'.$tabF[0].':]');
          $i++;
        }
      }
      $list = sortObjBy ($list, 'file', SORT_ASC);
      closedir($dir);
      return $list;
    }

    // Méthode statique consistant à récupérer tous les émoticons
    static function setEmotes ($path, $text) {
      $emotes = Emoticon::getEmotes ($path);
      foreach ($emotes as $emote) {
        $text = preg_replace('/\[:'.$emote->name.':\]/', '<img src="'.PICS_PATH.'/emoticon/'.$emote->file.'" />', $text);
      }
      return $text;
    }
  }//class Emoticon


  // Calcule la position sur la carte d'un point à partir des coordonnées longitudes et lattitudes
  function calculPosition ($lat, $long, $IMAGE_X, $IMAGE_Y) {
    //Constantes
    $MAX_LAT = 46;
    $ZERO_LAT = $MAX_LAT / 2;
    $DIFF_LAT = 7.4;
    $MAX_LONG = 23.4;
    $ZERO_LONG = $MAX_LONG / 2;
    $DIFF_LONG = 1.95;
    $A = 50;
    // Gestion de la lattitude
    if ($lat >= 0) {
      $lat_signe = 'N';
    } else {
      $lat = abs($lat);
      $lat_signe = 'S';
    }
    // Gestion de la longitude
    if ($long >= 0) {
      $long_signe = 'E';
    } else {
      $long = abs($long);
      $long_signe = 'W';
    }
    // Calcul
    $center_long = $MAX_LONG / 2;
    $lat_num = (($lat - floor($lat)) / 0.6) + floor($lat);
    $long_num = (($long - floor($long)) / 0.6) + floor($long);
    $Y = ($DIFF_LAT / 30) * $lat_num;
    $B = ((($DIFF_LONG / 30) * $long_num) + $ZERO_LONG) - $center_long;
    if ($A == 0) $A = $Y;
    $X = sqrt((1 - (($Y*$Y) / ($A*$A))) * ($B*$B));
    if ($lat_signe=='N')
      $lat_result = $ZERO_LAT - $Y;
    else
      $lat_result = $ZERO_LAT + $Y;
    if ($long_signe=='W')
      $long_result = $center_long - $X;
    else
      $long_result = $center_long + $X;
    if ($IMAGE_X == 0) {
      $lat_X = round ($long_result / $MAX_LONG);
    } else {
      $lat_X = round ($long_result / ($MAX_LONG / $IMAGE_X));
    }
    if ($IMAGE_Y == 0) {
      $lat_Y = round ($lat_result / $MAX_LAT);
    } else {
      $lat_Y = round ($lat_result / ($MAX_LAT / $IMAGE_Y));
    }
    return array ('x' => $lat_X, 'y' => $lat_Y);
  }//calculPosition


  // Calcule la distance entre 2 points cardinaux
  function calculDistance ($p1, $p2) {
    $sourceLat = M_PI * $p1['x'] / 180;
    $sourceLong = M_PI * $p1['y'] / 180;
    $destLat = M_PI * $p2['x'] / 180;
    $destLong = M_PI * $p2['y'] / 180;
    $r = 6378;
    return ($r * (M_PI/2 - asin(sin($destLat) * sin($sourceLat) + cos($destLong - $sourceLong) * cos($destLat) * cos($sourceLat))));
  }//calculDistance


  // Renomme un fichier
  function renameFile ($source, $destination) {
    $succeed = false;
    if (file_exists($source)) {
      $succeed = @rename ($source, $destination);
      @chmod ($destination, 0777);
    }
    return $succeed;
  }//renameFile


  // Efface un fichier
  function deleteFile ($fichier) {
    $succeed = false;
    if (file_exists($fichier)) {
      $succeed = @unlink ($fichier);
    }
    return $succeed;
  }//deleteFile


  // Efface tous les fichiers d'un dossier
  function deleteAllFiles ($path) {
    if ($path<>'' && $handle = opendir($path)) {
      $file = '';
      while (false !== ($file = readdir($handle))) {
        deleteFile ($path.$file);
      }
      closedir($handle);
    }
  }//deleteAllFiles


  // Efface tous le dossier et son contenu
  function deleteDir ($dir) {
    if (!is_writable($dir)) {
      if (!@chmod($dir, 0777)) {
        return FALSE;
      }
    }
    $d = dir ($dir);
    while (FALSE !== ($entry = $d->read())) {
      if ( $entry == '.' || $entry == '..' ) {
        continue;
      }
      $entry = $dir . '/' . $entry;
      if (is_dir($entry)) {
        if (!deleteDir($entry)) {
          return FALSE;
        }
        continue;
      }
      if (!@unlink($entry)) {
        $d->close();
        return FALSE;
      }
    }
    $d->close();
    rmdir($dir);
    return TRUE;
  }//deletedir


  // Copie tous le dossier et son contenu
  function copyDir ($source, $target) {
    if (is_dir($source)) {
      @mkdir( $target );
      $d = dir ($source);
      while (FALSE !== ($entry = $d->read())) {
        if ( $entry == '.' || $entry == '..' ) {
          continue;
        }
        $Entry = $source . '/' . $entry;
        if (is_dir($Entry)) {
          copyDir ($Entry, $target.'/'.$entry);
          continue;
        }
        @copy( $Entry, $target . '/' . $entry );
      }
      $d->close();
    } else {
      @copy( $source, $target );
    }
  }//copyDir


  // Déplace tous le dossier et son contenu
  function moveDir ($source, $target) {
    if (is_dir($source)) {
      @mkdir( $target );
      $d = dir ($source);
      while (FALSE !== ($entry = $d->read())) {
        if ( $entry == '.' || $entry == '..' ) {
          continue;
        }
        $Entry = $source . '/' . $entry;
        if (is_dir($Entry)) {
          moveDir ($Entry, $target.'/'.$entry);
          continue;
        }
        @rename( $Entry, $target . '/' . $entry );
      }
      $d->close();
      @rmdir( $source );
    } else {
      @rename( $source, $target );
    }
  }//moveDir


  // Remplace les accents et le espaces dans le nom d'un fichier
  function checkFileName($chaine) {
    $string= strtr($chaine,
      "����������������������������������������������������� %#';\></?!\"�~^)(�|��*+-}{��@�:,$`",
      "aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn__________________________________");
    return $string;
  }//checkFileName


  // Trie un tableau d'objet
  function sortObjBy ($table, $sortBy, $sortType) {
    $sort = null;
    if (count ($table) > 0) {
      foreach ($table as $key => $item) {
      if (isset($item->$sortBy)) {
          $sort[$key]  = addslashes($item->$sortBy);
        }
      }
      if (count ($sort) > 0) array_multisort($sort, $sortType, $table);
    }
    return ($table);
  }//sortObjBy


  // Vérifie si $email correspond à un e-mail
  function is_email($email) {
    $regexp="/^[a-z0-9]+([_\\.-][a-z0-9]+)*@([a-z0-9]+([\.-][a-z0-9]+)*)+\\.[a-z]{2,}$/i";
    if ( !preg_match($regexp, $email) ) {
      return false;
    }
    return true;
  }//is_email


  // Vérifie si $url correspond à une URL
  function is_url ($url) {
    $validUrl = false;
    $regexp1 = "/^(http(s?):\\/\\/|ftp:\\/\\/{1})((\w+\.)+)\w{2,}(\/?)$/i";
    $regexp2 = "/^((http|https|ftp):\/\/)(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?\//i";
    if (preg_match($regexp1, $url) || preg_match($regexp2, $url)) {
      $validUrl = $url;
    } else {
      $url = 'http://'.$url;
      if (preg_match($regexp1, $url) || preg_match($regexp2, $url)) {
        $validUrl = $url;
      }
    }
    return $validUrl;
  }//is_url


  // Vérifie si $password correspond au règle d'un mot de passe
  function checkPassword ($password) {
    $validated = false;
    if(ctype_alnum($password) // numbers & digits only
       && strlen($password)>7 // at least 8 chars
       && strlen($password)<15 // at most 14 chars
       && preg_match('`[a-z]`',$password) // at least one lower case
       && preg_match('`[0-9]`',$password) // at least one digit
       ) $validated = true;
    return $validated;
  }//checkPassword


  // Incrémente le compteur
  function hitcount($file, $add) {
    if (!file_exists($file)){
      touch ($file);
      $handle = fopen ($file, 'r+');
      $count = 0;
    } else{
      $handle = fopen ($file, 'r+');
      $count = fread ($handle, filesize ($file));
      settype ($count,"integer");
    }
    rewind ($handle);
    if ($add) {
      fwrite ($handle, ++$count);
    } else {
      fwrite ($handle, $count);
    }
    fclose ($handle);
    return $count;
  }//hitcount


  // Inverse la luminosité d'une couleur
  function inverseColor ($col) {
    if ($col=='') return false;
    $r = (string2dec ($col[1].$col[2], 16));
    $g = (string2dec ($col[3].$col[4], 16));
    $b = (string2dec ($col[5].$col[6], 16));
    $result = pow($r*$g*$b, 1/3);
    if ($result > 128) {
      $colresult = darken ($col, 50);//100 - (($result-128)/1.28));
    } else {
      $colresult = lighten ($col, 50);//100 - ($result/1.28));
    }
    return $colresult;
  }
  //inverseColor


  // Assombri une couleur
  function darken ($color, $percent) {
    if ($color=='') return false;
    $r = (string2dec ($color[1].$color[2], 16));
    $g = (string2dec ($color[3].$color[4], 16));
    $b = (string2dec ($color[5].$color[6], 16));
    $r = round($r*$percent/100);
    $g = round($g*$percent/100);
    $b = round($b*$percent/100);
    if (strlen(dec2string ($r, 16))==1) {
      $r = '0'.dec2string ($r, 16);
    } else {
      $r = dec2string ($r, 16);
    }
    if (strlen(dec2string ($g, 16))==1) {
      $g = '0'.dec2string ($g, 16);
    } else {
      $g = dec2string ($g, 16);
    }
    if (strlen(dec2string ($b, 16))==1) {
      $b = '0'.dec2string ($b, 16);
    } else {
      $b = dec2string ($b, 16);
    }
    return ('#'.$r.$g.$b);
  }//darken


  // Eclairci une couleur
  function lighten ($color, $percent) {
    if ($color=='') return false;
    $r = (string2dec ($color[1].$color[2], 16));
    $g = (string2dec ($color[3].$color[4], 16));
    $b = (string2dec ($color[5].$color[6], 16));
    $r = round($r*$percent/100) + round(254 - $percent/100*254);
    $g = round($g*$percent/100) + round(254 - $percent/100*254);
    $b = round($b*$percent/100) + round(254 - $percent/100*254);
    if (strlen(dec2string ($r, 16))==1) {
      $r = '0'.dec2string ($r, 16);
    } else {
      $r = dec2string ($r, 16);
    }
    if (strlen(dec2string ($g, 16))==1) {
      $g = '0'.dec2string ($g, 16);
    } else {
      $g = dec2string ($g, 16);
    }
    if (strlen(dec2string ($b, 16))==1) {
      $b = '0'.dec2string ($b, 16);
    } else {
      $b = dec2string ($b, 16);
    }
    return ('#'.$r.$g.$b);
  }//lighten


  // Convert un nombre décimal en base de $base
  function dec2string ($decimal, $base) {
    global $error;
    $string = null;
    $base = (int)$base;
    if ($base < 2 | $base > 36 | $base == 10) {
      echo 'BASE must be in the range 2-9 or 11-36';
      exit;
    }
    $charset = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charset = substr($charset, 0, $base);
    if (!preg_match('(^[0-9]{1,16}$)', trim($decimal))) {
      $error['dec_input'] = 'Value must be a positive integer';
      return false;
    }
    do {
      $remainder = $decimal % $base;
      $char      = substr($charset, $remainder, 1);
      $string    = "$char$string";
      $decimal   = ($decimal - $remainder) / $base;
    } while ($decimal > 0);
    return $string;
  }//dec2string


  // Convert un nombre de base $base en décimal
  function string2dec ($string, $base) {
    global $error;
    $decimal = 0;
    $base = (int)$base;
    if ($base < 2 | $base > 36 | $base == 10) {
      echo 'BASE must be in the range 2-9 or 11-36';
      exit;
    }
    $charset = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charset = substr($charset, 0, $base);
    $string = trim($string);
    if (empty($string)) {
      $error[] = 'Input string is empty';
      return false;
    }
    do {
      $char   = substr($string, 0, 1);
      $string = substr($string, 1);
      $pos = strpos($charset, $char);
      if ($pos === false) {
        $error[] = "Illegal character ($char) in INPUT string";
        return false;
      }
      $decimal = ($decimal * $base) + $pos;
    } while($string <> null);
    return $decimal;
  }//string2dec


  // Transforme une couleur hexadecimale et tableau rgb
  function hex2rgb($hex) {
    $hex = substr ($hex, 1);
    for($i=0; $i<3; $i++)
    {
      $temp = substr($hex,2*$i,2);
      $rgb[$i] = 16 * hexdec(substr($temp,0,1)) + hexdec(substr($temp,1,1));
    }
    return $rgb;
  }//hex2rgb


  // Crée un arc de cercle en l'enregistre
  function createArc ($cn, $width, $height, $bkcolor, $color, $destination) {
    $bkcolorArc = hex2rgb($bkcolor);
    $colorArc = hex2rgb($color);
    $drawArc = true;
    switch ($cn) {
      case "tl" : {$center_x = $width;$center_y = $height;$arc_start = 180;$arc_end = 270;break;}
      case "tr" : {$center_x = -1;$center_y = $height;$arc_start = 270;$arc_end = 360;break;}
      case "bl" : {$center_x = $width;$center_y = -1;$arc_start = 90;$arc_end = 0;break;}
      case "br" : {$center_x = -1;$center_y = -1;$arc_start = 0;$arc_end = 90;break;}
      default : {$drawArc = false;}
    }
    $image = imagecreatetruecolor($width, $height);
    $imgColor  = imagecolorallocate($image, $colorArc[0], $colorArc[1],$colorArc[2]);
    $imgBkColor  = imagecolorallocate($image, $bkcolorArc[0], $bkcolorArc[1],$bkcolorArc[2]);
    if ($drawArc) {
      imagefill($image, 0, 0, $imgBkColor);
      imagefilledarc($image, $center_x, $center_y, ($width*2), ($height*2), $arc_start, $arc_end, $imgColor, IMG_ARC_PIE);
    } else {
      imagefill($image, 0, 0, $imgColor);
    }
    imagegif($image, $destination);
    @chmod ($destination, 0777);
    imagedestroy($image);
  }//createArc


  // Crée un camember équivalent au quota introduit
  function createQuotaImg ($ratio, $width, $height, $bkcolor, $color, $destination) {
    if ($ratio >= 0 && $ratio <= 100) $arc = $ratio * 3.6; else $arc = 0;
    if ($width > 10 && $height > 10) {
      $bkcolorShadow = darken ($bkcolor, 20);
      $colorShadow = darken ($color, 20);
      $bkcolorArc = hex2rgb($bkcolor);
      $colorArc = hex2rgb($color);
      $image = imagecreatetruecolor($width, $height+11);
      $trans = imagecolorallocate($image, 1, 0, 0);
      imagecolortransparent($image, $trans);
      $imgColor  = imagecolorallocate($image, $colorArc[0], $colorArc[1],$colorArc[2]);
      $imgBkColor  = imagecolorallocate($image, $bkcolorArc[0], $bkcolorArc[1],$bkcolorArc[2]);
      $shadowColor  = imagecolorallocate($image, $colorArc[0], $colorArc[1],$colorArc[2]);
      $shadowBkColor  = imagecolorallocate($image, $bkcolorArc[0], $bkcolorArc[1],$bkcolorArc[2]);
      for ($i = ($height/2)+10; $i > $height/2; $i--) {
       imagefilledarc($image, $width/2, $i, $width, $height, 0, $arc, $shadowColor, IMG_ARC_PIE);
       imagefilledarc($image, $width/2, $i, $width, $height, $arc, 360 , $shadowBkColor, IMG_ARC_PIE);
      }
      imagefilledarc($image, $width/2,$height/2, $width, $height, 0, $arc, $imgColor, IMG_ARC_PIE);
      imagefilledarc($image, $width/2, $height/2, $width, $height, $arc, 360 , $imgBkColor, IMG_ARC_PIE);
      imagegif($image, $destination);
      @chmod ($destination, 0777);
      imagedestroy($image);
    }
  }//createQuotaDisplay


  // Calcul l'espace disque courant d'un dossier
  function folderspace($dir) {
    $s = stat($dir);
    $space = $s["size"];
    if (is_dir($dir)) {
      $dh = opendir($dir);
      while (($file = readdir($dh)) !== false)
        if ($file != "." and $file != "..")
          $space += folderspace($dir."/".$file);
      closedir($dh);
    }
    return $space;
  }//folderspace


  // Ajoute au besoin des anti-slash
  function addslash ($text) {
    return addslashes($text);
  }//addslash


  // Retourne l'espace disque disponible pour l'utilisateur
  function getFreeSpace($maxSpace, $quota_path) {
    $occupedSpace = folderSpace($quota_path);
    $freeSpace = 0;
    if ($occupedSpace <= $maxSpace) $freeSpace = $maxSpace - $occupedSpace;
    return $freeSpace;
  }//getFreeSpace


  // Formate un chiffre avec 2 décimale
  function fPrice($nombre) {
    return number_format($nombre, 2, '.', '');
  }//fPrice


  // Ferme la fenêtre courante
  function closeWindow () {
    ?>
    <script language="JavaScript" type="text/javascript">
    <!--
      window.opener.location.href = window.opener.location.href;
      if (window.opener.progressWindow) {
        window.opener.progressWindow.close();
      }
      window.close();
    -->
    </script>
    <?php
  }//closeWindow


  // Découpe un mot d'après sa longueur max
  function cutWords ($txt , $limit=40, $html_nl=0) {
    $str_nl = $html_nl == 1 ? "<br />" : ($html_nl == 2 ? "<br />" : "\n");
    $pseudo_words = explode(' ', $txt);
    $txt = '';
    foreach ($pseudo_words as $v) {
      if (($tmp_len = strlen($v)) > $limit) {
        $final_nl = is_int($tmp_len / $limit);
        $txt .= chunk_split($v, $limit, $str_nl);
        if(!$final_nl)
          $txt = substr($txt, 0, - strlen($str_nl));
        $txt .= ' ';
      } else {
        $txt .= $v . ' ';
      }
    }
    return substr( $txt, 0 , -1 );
  }//cutWords


  // Racourci une phrase
  function shorten ($text, $size=20) {
    $text = cutWords ($text);
    if (strlen ($text) > $size) {
      $returnText = substr($text, 0, $size);
      $returnText.='...';
    } else {
      $returnText = $text;
    }
    return $returnText;
  }//shorten


  // Initialise une nouvelle session
  function initSession($name) {
    if (!isset ($_COOKIE[$name]) || !isset ($_COOKIE[$name]['id']) || $_COOKIE[$name]['id'] == "") {
      $id = genID();
      setcookie($name.'[id]', $id, (time() + 3600 * 24));
    } else {
      $id = $_COOKIE[$name]['id'];
    }
    session_name($id);
    session_cache_limiter("nocache");
    session_start();
  }//initSession


  // Crée des lignes d'après un tableau de coordonnées sur la carte
  function createLines ($array, $destination=null) {
    $img = imagecreatefromjpeg ('../lib/pics/world/world.jpg');
    foreach ($array as $item) {
      $color = hex2rgb($item['color']);
      $colImg = imagecolorallocate($img, $color[0], $color[1], $color[2]);
      imagelinethick ($img, $item['x1'], $item['y1'], $item['x2'], $item['y2'], $colImg, 4);
    }
    imagejpeg($img, $destination, 20);
    @chmod ($destination, 0777);
    imagedestroy($img);
  }//createLines


  // Dessine une ligne d'une certaine épaisseur
  function imagelinethick($image, $x1, $y1, $x2, $y2, $color, $thick = 1) {
    if ($thick == 1) {
      return imageline($image, $x1, $y1, $x2, $y2, $color);
    }
    $t = $thick / 2 - 0.5;
    if ($x1 == $x2 || $y1 == $y2) {
      return imagefilledrectangle($image, round(min($x1, $x2) - $t), round(min($y1, $y2) - $t), round(max($x1, $x2) + $t), round(max($y1, $y2) + $t), $color);
    }
    $k = ($y2 - $y1) / ($x2 - $x1); //y = kx + q
    $a = $t / sqrt(1 + pow($k, 2));
    $points = array(
        round($x1 - (1+$k)*$a), round($y1 + (1-$k)*$a),
        round($x1 - (1-$k)*$a), round($y1 - (1+$k)*$a),
        round($x2 + (1+$k)*$a), round($y2 - (1-$k)*$a),
        round($x2 + (1-$k)*$a), round($y2 + (1+$k)*$a),
    );
    imagefilledpolygon($image, $points, 4, $color);
    return imagepolygon($image, $points, 4, $color);
  }//imagelinethick


  // Supprime un dossier et tout ce qui s'y trouve à l'intérieur
  function rmdirtree($dirname) {
    if (is_dir($dirname)) {
        $result=array();
        if (substr($dirname,-1)!='/') {$dirname.='/';}
        $handle = opendir($dirname);
        while (false !== ($file = readdir($handle))) {
            if ($file!='.' && $file!= '..') {
                $path = $dirname.$file;
                if (is_dir($path)) {
                    $result=array_merge($result,rmdirtree($path));
                }else{
                    unlink($path);
                    $result[].=$path;
                }
            }
        }
        closedir($handle);
        rmdir($dirname);
        $result[].=$dirname;
        return $result;
    } else {
      return false;
    }
  }//rmdirtree


  // Genère un identifiant unique
  function genID () {
    return md5(uniqid(rand(), true));
  }//genID


  // Génère le CSS du Header d'une texture personnelle
  function array_move_element($array, $value, $direction = 'left') {
    $temp = array();
    end($array);
    if(key($array) == $value && $direction == 'right') {
      return $array;
    }
    reset($array);
    if(key($array) == $value && $direction == 'left') {
      return $array;
    }
    while ($array_value = current($array)) {
      $this_key = key($array);
      if ($this_key == $value) {
        if($direction == 'right') {
          $next_value = next($array);
          $temp[key($array)] = $next_value;
          $temp[$this_key] = $array_value;
        } else {
          $prev_value = prev($array);
          $prev_key = key($array);
          unset($temp[$prev_key]);
          $temp[$this_key] = $array_value;
          $temp[$prev_key] = $prev_value;
          next($array);
          next($array);
        }
        continue;
      } else {
        $temp[$this_key] = $array_value;
      }
      next($array);
    }
    return $temp;
  }//array_move_element


  // Génère le CSS du Header d'une texture personnelle
  function generateHeaderCSS () {
    return "#headertopout {
  height:190px;
  margin-left:1px;
  width:806px;
  background-image: url('header.jpg');
}

#headertopin {
  height:1px;
  font-size:1px;
}

#headerbottomout {
  height:45px;
  margin-left:1px;
  width:806px;
  background-image: url('footer.jpg');
  font-size:5px;
}

#headerbottomin {
  height:5px;
  font-size:3px;
}

#headerleftout {
  width:1px;
  font-size:1px;
}

#headerleftin {
  width:189px;
}

#headerrightout {
  width:1px;
  font-size:1px;
}

#headerrightin {
  width:617px;
}
";
  }//generateHeaderCSS


?>
