<?php
function ListDevices(&$hostname, &$community){
echo "
<html>
<head>
  <title>Storage Information</title>
<head>
<body>
<center>
<table cellpadding=0 cellspacing=0 border=1 width=100%>
<tr bgcolor=\"#e5e5e5\">
  <td width=33% align=center>hrStorageIndex</td>
  <td width=33% align=center>hrStorageType</td>
  <td width=33% align=center>hrStorageDescr</td>
</tr>";

$hrStorageIndex = snmpwalk($hostname, $community, ".1.3.6.1.2.1.25.2.3.1.1");
$hrStorageType  = snmpwalk($hostname, $community, ".1.3.6.1.2.1.25.2.3.1.2");
$hrStorageDescr = snmpwalk($hostname, $community, ".1.3.6.1.2.1.25.2.3.1.3");

for($i = 0; $i < sizeof($hrStorageIndex); $i++){
  echo "
        <tr>
            <td width=33% align=center><a href=\"".$_SERVER["PHP_SELF"]."?action=2&storageindex=".$hrStorageIndex[$i]."\">".$hrStorageIndex[$i]."</a></td>
          <td width=33% align=center>";
  
          $type = substr($hrStorageType[$i], strlen($hrStorageType[$i]) - 1);
          switch($type){
              case 0:
                echo "Network Disk";
                break;
              case 1:
                echo "Other";
                break;
              case 2:
                echo "Ram";
                break;
              case 3:
                echo "Vrtual Memory";
                break;
              case 4:
                echo "Fixed Disk";
                break;
              case 5:
                echo "Removable Disk";
                break;
              case 6:
                echo "Floppy Disk";
                break;
              case 7:
                echo "Compact Disk";
                break;
              case 8:
                echo "Ram Disk";
                break;
              case 9:
                echo "Flash Memory";
                break;
              default:
                echo "Unknown";
          }
                    
  echo "  </td>
          <td width=33% align=center>".$hrStorageDescr[$i]."</td>
        </tr>";
  }

echo "
</table>
</center>
</body>
</html>";
}

function DetailedInfo(&$hostname, &$community, $storageIndex){
echo "
<html>
<head>
  <title>Disk Usage</title>
</head>
<body>";

$hrStorageDescr = snmpget($hostname, $community, ".1.3.6.1.2.1.25.2.3.1.3.".$storageIndex);
$hrStorageSize = snmpget($hostname, $community, ".1.3.6.1.2.1.25.2.3.1.5.".$storageIndex);
$hrStorageUsed = snmpget($hostname, $community, ".1.3.6.1.2.1.25.2.3.1.6.".$storageIndex);
$hrStorageAllocationUnits = snmpget($hostname, $community, ".1.3.6.1.2.1.25.2.3.1.4.".$storageIndex);

$usedBytes = $hrStorageUsed * $hrStorageAllocationUnits;
$freeBytes = ($hrStorageSize - $hrStorageUsed) * $hrStorageAllocationUnits;
$capacity = $hrStorageSize * $hrStorageAllocationUnits;
$usedBytesPer = round(($usedBytes / $capacity) * 100, 2);
$freeBytesPer = round(($freeBytes / $capacity) * 100, 2);
$percentToImage = round(($usedBytes / $capacity) * 100);

echo "
<font face=\"Courier\" size=4 color=\"#000000\">
<center>
<table cellpadding=0 cellspacing=0 width=75% border=0>
  <tr><td width=100% align=center colspan=4><b>".$hrStorageDescr."</b><br> </td></tr>";

if($capacity < 1073741824){
  $usedMBytes = round($usedBytes / pow(2,20), 2);
  $freeMBytes = round($freeBytes / pow(2,20), 2);
  $capacityMBytes = round($capacity / pow(2,20), 2);
  echo "
  <tr>
    <td width=20% align=right>Used Space:</td>
    <td width=50% align=center>".number_format($usedBytes)." Bytes</td>
    <td width=15% align=center>".$usedMBytes." MB</td>
    <td width=15% align=center>".$usedBytesPer."%</td>
  </tr>
  <tr>
    <td width=20% align=right>Free Space:</td>
    <td width=50% align=center>".number_format($freeBytes)." Bytes</td>
    <td width=15% align=center>".$freeMBytes." MB</td>
    <td width=15% align=center>".$freeBytesPer."%</td>
  </tr>
  <tr>
    <td width=20% align=right>Capacity:</td>
    <td width=50% align=center>".number_format($capacity)." Bytes</td>
    <td width=15% align=center>".$capacityMBytes." MB</td>
    <td width=15% align=center> </td>
  </tr>";
} else {
  $usedGBytes = round($usedBytes / pow(2,30), 2);
  $freeGBytes = round($freeBytes / pow(2,30), 2);
  $capacityGBytes = round($capacity / pow(2,30), 2);
  echo "
  <tr>
    <td width=20% align=right>Used Space:</td>
    <td width=50% align=center>".number_format($usedBytes)." Bytes</td>
    <td width=15% align=center>".$usedGBytes." GB</td>
    <td width=15% align=center>".$usedBytesPer."%</td>
  </tr>
  <tr>
    <td width=20% align=right>Free Space:</td>
    <td width=50% align=center>".number_format($freeBytes)." Bytes</td>
    <td width=15% align=center>".$freeGBytes." GB</td>
    <td width=15% align=center>".$freeBytesPer."%</td>
  </tr>
  <tr>
    <td width=20% align=right>Capacity:</td>
    <td width=50% align=center>".number_format($capacity)." Bytes</td>
    <td width=15% align=center>".$capacityGBytes." GB</td>
    <td width=15% align=center> </td>
  </tr>";
}

echo "
  <tr>
    <td width=100% align=center colspan=4> <p><img src=\"".$_SERVER["PHP_SELF"]."?action=3&percent=".$percentToImage."\"></td>
  </tr>
</table>
</center>
</font>
</body>
</html>";
}

function GD2DPie($perUsed){
$perFree = 100 - $perUsed;
$endUsed_startFree = ceil((360 / 100) * $perUsed);
$img = imagecreate(300, 300);
$bg = imagecolorallocate($img, 255, 255, 255);
$usedColor = imagecolorallocate($img, 0, 0, 200);
$freeColor = imagecolorallocate($img, 225, 0, 200);
imagearc($img, 150, 150, 250, 250, 0, $endUsed_startFree, $usedColor);
imageline($img, 150, 150, 275, 150, $usedColor);
imageline($img, 150 + (cos(deg2rad($endUsed_startFree))*(250/2)), 150 + (sin(deg2rad($endUsed_startFree))*(250/2)), 150, 150, $usedColor);
imagefilltoborder($img, 150 + (cos(deg2rad(($endUsed_startFree)/2))*(250/4)), 150 + (sin(deg2rad(($endUsed_startFree)/2))*(250/4)), $usedColor, $usedColor);
imagearc($img, 150, 150, 250, 250, $endUsed_startFree, 360, $freeColor);
imageline($img, 150 + (cos(deg2rad($endUsed_startFree))*(250/2)), 150 + (sin(deg2rad($endUsed_startFree))*(250/2)), 150, 150, $freeColor);
imageline($img, 150, 150, 275, 150, $freeColor);
imagefilltoborder($img, 150 + (cos(deg2rad(($endUsed_startFree+360)/2))*(250/4)), 150 + (sin(deg2rad(($endUsed_startFree+360)/2))*(250/4)), $freeColor, $freeColor);
header("Content-type = image/png");
imagepng($img);
imagedestroy($img);
}

$hostname  = "127.0.0.1";  // change this accordingly
$community = "public";     // public is default but most likely not "public"

if (empty($_GET["action"])){
  $action = 1;
} else {
  $action = $_GET["action"];
}

switch($action){
  case 1:
    ListDevices($hostname, $community);
    break;
  case 2:
    DetailedInfo($hostname, $community, $_GET['storageindex']);
    break;
  case 3:
    GD2DPie($_GET['percent']);
    break;
  default:
    die("Unable to process request!");
}

?>