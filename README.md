# Installation

Create "cache" and "templates_c" subfolders in "smarty" folder and allow full write access

Create a folder named "tmp", move the "test" subfolder and "config.xml" file from existing in "demo" into the new "tmp" folder and allow full write access to "tmp" folder and all it's subfiles/subfolders.

Dump the database with install.sql and configure "tmp/config.xml" file
