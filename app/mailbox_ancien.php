<?php
  include_once('../lib/init.php');


  // Si l'administrateur de l'album ne s'est pas authentifié, renvoie l'utilisateur à la page d'accueil
  if (!$admin) {header("Location:".PAGE_ACCUEIL);exit();}


  // Initialisation des données
  $msgSelected = null;
  $mailbox = null;
  $pageName = "mailbox";
  $actions = "";


  // Récupération des données
  if (isset ($_GET['box']) && $_GET['box']=='out') {
    $box = $_GET['box'];
    $attachmentPath = $attachedOutboxPath;
  } else {
    $box = 'in';
    $attachmentPath = $attachedInboxPath;
  }
  if (!isset($_POST['init_'.$pageName]) && isset ($_SESSION['msg']) && !isset ($_GET['saveOrder'])) unset ($_SESSION['msg']);
  if (isset ($_POST['mailbox']) && $_POST['mailbox'] <> $_SESSION['mailbox']) {
    $_SESSION['mailbox'] = $_POST['mailbox'];
    $emailsManager = new EmailManager ($emailsFile, $albumName, 'inbox', $_SESSION['mailbox']);
  }
  if (isset ($_POST['actions'])) $actions = $_POST['actions']; else $actions = "";
  if (isset ($_POST['showTrash'])) {
    if ($_POST['showTrash'] == 'true') $deletedItems = 1; else $deletedItems = 0;
    $_SESSION['msg']['trash'] = $deletedItems;
  } else $deletedItems = 0;
  if (isset($_SESSION['msg']['trash']) && $_SESSION['msg']['trash']==1) $deletedItems = 1;
  printt ($deletedItems);
  if ($actions == 'edit') $mailbox = $emailsManager->getMailbox ($_SESSION['mailbox']);


/** Actions sur la mailbox **********************************************************************************/

  // Réception de tous les nouveaux message externe dans la boîte de message
  if (isset ($_POST['synch']) && $_POST['synch']=='true') {
    if ($property->deleteEmails) $deleteOrigMail = true; else $deleteOrigMail = false;
    $mailboxTemp = $emailsManager->getMailbox ($_SESSION['mailbox']);
    $mailboxManager = new MailReceiver ($mediaManager, $mailboxTemp->host, $mailboxTemp->username, $mailboxTemp->password, $albumManager->getQuota(), $quota_path);
    $mailsReceived = $mailboxManager->getMails('', $deleteOrigMail, false, $attachmentPath, 60, $emailsManager);
    if (is_string ($mailsReceived)) $error = $msgHandler->getError ($mailsReceived);
  }


  // Enregistre la nouvelle boîte de message ou modifie une existante
  if (isset ($_POST['save']) && $_POST['save']<>'') {
    if ($_POST['name']=='default') $error = $msgHandler->getError ('reservedName');
    if (!is_email($_POST['email'])) $error = $msgHandler->getError ('invalidEmail');
    if ($_POST['name']=='' || $_POST['host']=='' || $_POST['email']=='' || $_POST['username']=='' || $_POST['password']=='') $error = $msgHandler->getError ('fieldError');
    $mailbox = new Mailbox ($_POST['name'], $_POST['host'], $_POST['email'], $_POST['username'], $_POST['password'], time());
    if ($error=="") {
      $emailsManager->editMailbox ($mailbox);
    } else {
      $actions = $_POST['save'];
    }
  }

/** Actions sur la liste de messages *****************************************************************************/

  // Récupération des messages sélectionnés
  if (isset ($_POST['msgshidden'])) {
    $hiddenMsgs = array_flip($_POST['msgshidden']);
    if (isset ($_POST['msgs'])) {
      $postedMsgs = array_flip($_POST['msgs']);
    } else {
      $postedMsgs = null;
    }
    if (count ($hiddenMsgs > 0)) {
      foreach ($hiddenMsgs as $kMsg => $iMsg) {
        if (isset ($postedMsgs[$kMsg])) {
          if (isset ($_SESSION['msg']) && isset ($_SESSION['msg']['msgs']) && is_array ($_SESSION['msg']['msgs']))
          $_SESSION['msg']['msgs']=$_SESSION['msg']['msgs'] + $postedMsgs;
          else $_SESSION['msg']['msgs']=$postedMsgs;
        } else {
          if (isset ($_SESSION['msg']) && isset ($_SESSION['msg']['msgs']) && isset ($_SESSION['msg']['msgs'][$kMsg]))
            unset ($_SESSION['msg']['msgs'][$kMsg]);
        }
      }
    }
  } else {
    $postedMsgs = null;
  }


  // Si le bouton "Delete Msg" a été pressé, supprime le message
  if (isset ($_POST['deleteMsg']) && $_POST['deleteMsg']<>'') {
    $tempEmail = $emailsManager->getEmail ($_POST['deleteMsg'], $box);
    if ($tempEmail) {
      $attachmentsToDelete = $tempEmail->attachments;
      $emailsManager->deleteEmail ($_POST['deleteMsg'], $box);
      if (count ($attachmentsToDelete) > 0) {
        foreach ($attachmentsToDelete as $att) {
          deleteFile ($attachmentPath.$att->content);
        }
      }
    }
  }


  // Si le bouton "Delete Selected Msgs" a été pressé, supprime les msgs sélectionnés
  if (isset ($_POST['deleteMsgs']) && $_POST['deleteMsgs']=='true') {
    if (isset ($_SESSION['msg']) && isset ($_SESSION['msg']['msgs']) && count($_SESSION['msg']['msgs']) > 0) {
      foreach ($_SESSION['msg']['msgs'] as $keyMsg => $itemMsg) {
        $tempEmail = $emailsManager->getEmail ($keyMsg, $box);
        if ($tempEmail) {
          $attachmentsToDelete = $tempEmail->attachments;
          $emailsManager->deleteEmail ($keyMsg, $box);
          if (count ($attachmentsToDelete) > 0) {
            foreach ($attachmentsToDelete as $att) {
              deleteFile ($attachmentPath.$att->content);
            }
          }
        }
        unset ($_SESSION['msg']['msgs'][$keyMsg]);
      }
    } else {
      $error = $msgHandler->getError ('noMsgToDelete');
    }
  }


  // Récupération de tous les messages
  $msgs = $emailsManager->getEmails($box, -1, -1, false, $deletedItems);
  $contacts = $contactsManager->getContacts();
  if (count ($msgs) > 0) {
    foreach ($msgs as $keyMsg => $msg) {
      $msg = $emailsManager->convertNamesofEmail ($msg, $contacts, $box);
      if ($box == 'out') $msg->from = $msg->to;
      $msg->subject = shorten ($msg->subject, 30);
    }
  }


  // Si le bouton "Select All" a été pressé, sélectionne tous les messages
  if (isset ($_POST['selectAll']) && $_POST['selectAll']=='true') {
    if (isset ($_SESSION['msg']) && isset ($_SESSION['msg']['msgs']) && count($_SESSION['msg']['msgs']) > 0) {
      unset ($_SESSION['msg']['msgs']);
    } else {
      if (count ($msgs) > 0) {
        foreach ($msgs as $keyMsg => $msg) {
          $_SESSION['msg']['msgs'][$msg->id] = 1;
          $msgSelected[$keyMsg] = 1;
        }
      }
    }
  }


  // Si le bouton "View" a été pressé, affiche les détails du message
  if (isset ($_POST['view']) && $_POST['view']<>'' && isset ($_POST['box']) && $_POST['box']<>'') {
    header ("Location:".PAGE_EMAILDETAILS."?id=".$_POST['view']."&box=".$_POST['box']);
  }

/*****************************************************************************************************************/
  // Gestion de l'affichage des résultats
  $splitDisplay = new SplitDisplay (25, 10);
  if (!isset ($_SESSION['msg']['page']) || !isset ($_SESSION['msg']['next'])) {
    $_SESSION['msg']['page'] = 0;
    $_SESSION['msg']['next'] = 0;
  }
  if (isset($_GET['next'])) {
    $_SESSION['msg']['next'] = $_GET['next'];
    if (isset($_GET['page'])) {
      $_SESSION['msg']['page'] = $_GET['page'];
    } else {
      $_SESSION['msg']['page'] = $_GET['next']*$splitDisplay->length;
    }
  }
  $splitDisplay->start = $_SESSION['msg']['next'];
  $splitDisplay->first = $_SESSION['msg']['page'];


  // Gestion du tri
  $display = array();
  if (!isset ($_SESSION['msg']['sortBy']) || !isset ($_SESSION['msg']['sortOrder'])) {
    $_SESSION['msg']['sortBy'] = 'date';
    $_SESSION['msg']['sortOrder'] = 'desc';
  }
  if ((isset ($_GET['msg_sort_by']) && isset ($_GET['msg_sort_order'])) || (isset ($_SESSION['msg']['sortBy']) && isset ($_SESSION['msg']['sortOrder']))) {
    if (isset ($_GET['msg_sort_by'])) {$_SESSION['msg']['sortBy'] = $_GET['msg_sort_by'];$splitDisplay->start = 0;$splitDisplay->first = 0;}
    if (isset ($_GET['msg_sort_order'])) {$_SESSION['msg']['sortOrder'] = $_GET['msg_sort_order'];$splitDisplay->start = 0;$splitDisplay->first = 0;}
    if ($_SESSION['msg']['sortOrder'] == 'asc') {
      $msgs = sortObjBy ($msgs, $_SESSION['msg']['sortBy'], SORT_ASC);
    } else {
      $msgs = sortObjBy ($msgs, $_SESSION['msg']['sortBy'], SORT_DESC);
    }
  }
  $display['sortBy'] = $_SESSION['msg']['sortBy'];
  $display['sortOrder'] = $_SESSION['msg']['sortOrder'];


  // Gestion de l'affichage des résultats
  $splitDisplay->total = count ($msgs);
  $result = array();
  if ($splitDisplay->total > 0) {
    $i=0;
    if ($splitDisplay->first >= $splitDisplay->total) $splitDisplay->first=$splitDisplay->first-$splitDisplay->length;
    if ($splitDisplay->first < 0) $splitDisplay->first = 0;
    foreach ($msgs as $key => $item) {
      if (isset ($_SESSION['msg']) && isset ($_SESSION['msg']['msgs']) && isset ($_SESSION['msg']['msgs'][$item->id])) $msgSelected[$i] = 1;
      if (($splitDisplay->first==-1 && $splitDisplay->length==-1) || ($i >= $splitDisplay->first &&  $i < $splitDisplay->first+$splitDisplay->length)) {
        $result[$i] = $item;
      }
      $i++;
    }
    $msgs = $result;
  }
  if ($splitDisplay->length > 0) {
    $splitDisplay->loop = ceil(($splitDisplay->total)/$splitDisplay->length);
    $splitDisplay->first = ceil(($splitDisplay->first)/$splitDisplay->length);
  }


  // Récupère toutes les boîtes de message
  $mailboxes = $emailsManager->getMailboxes();
  if ($mailboxes) {
    foreach ($mailboxes as $itemMailbox) {
      if ($itemMailbox->name == 'default') $itemMailbox->fullname = $msgHandler->get('defaultMailbox'); else $itemMailbox->fullname = $itemMailbox->name;
    }
  }


  // Assignation de Smarty
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  if ($box=='out') {
    $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuOutbox'), $admin, 4));
    $smarty->assign ('boxName', '<a href="'.PAGE_MAILBOX.'?box=in">'.$msgHandler->get ('menuInbox').'</a>');
  } else {
    $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuInbox'), $admin, 4));
    $smarty->assign ('boxName', '<a href="'.PAGE_MAILBOX.'?box=out">'.$msgHandler->get ('menuOutbox').'</a>');
  }
  $smarty->assign ('showTrash', $deletedItems);
  $smarty->assign ('box', $box);
  $smarty->assign ('display', $display);
  $smarty->assign ('msgSelected', $msgSelected);
  $smarty->assign ('mailboxes', $mailboxes);
  $smarty->assign ('mailbox', $mailbox);
  $smarty->assign ('defaultMailbox', $_SESSION['mailbox']);
  $smarty->assign ('action', $actions);
  $smarty->assign ('msgs', $msgs);
  $smarty->assign ('splitDisplay', $splitDisplay);
  $smarty->assign ('pageName', $pageName);
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->display($pageName.'.tpl');

?>

