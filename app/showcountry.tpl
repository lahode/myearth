<div class="sectionP">
  <p class="sitetitle">{#regionVisited#}</p>
</div>
<div class="sectionP">
<table width="100%">
  {foreach from=$destinations key=keyDest item=dest}
  {if $keyDest % 2 == 0}
  <tr>
  {/if}
    <td align="left">
       <div id="ff{$dest->id}" class="cadresitetitle" onclick="xajax_showDestination({$dest->id});" style="cursor: pointer;height: 170px;width: 250px">
       <p class="sitetitle">{$dest->region} - {$dest->city}</p>
       <p class="persotext">{$dest->text|sslash}</p>
       <table width="80%">
         <tr>
         {foreach from=$dest->images item=image}
           <td align="center">
             <img src="{$webImgPath}{$image->name}" width="50" />
           </td>
         {/foreach}
         </tr>
       </table>
       </div>
    </td>
  {if ($keyDest % 2 == 1 || $keyDest==($destinations|@count)-1)}
  </tr>
  {/if}
  {/foreach}
</table>
</div>

<div class="sectionP">
  <span class="sitetitle">{#tips#} :</span><span class="persotext"> {$cat->text|sslash|nl2br}</span>
</div>

<hr />

<div class="sectionP">
  {if $stats}
  <span class="sitetitle">{#budget#} :</span><span class="sitetext">&nbsp;{$stats}</span>
  {else}
  <div style="float:left">
  <span class="sitetitle">{#budget#} :</span>
  </div>
  <div style="float:left">
    <table cellpadding="0" cellspacing="0" width="100%" style="margin-top:5px">
      <tr>
        <td class="sitetext">
          &nbsp;&nbsp;&nbsp;{#foodsleep#}
        </td>
        <td class="sitetext">
          &nbsp;&nbsp;&nbsp;{#planed#}: {$cat->budget_foodsleep_planed+0} {$countrycurrency}
        </td>
        <td class="sitetext">
          &nbsp;&nbsp;&nbsp;{#spent#}: {$cat->budget_foodsleep_spent+0} {$countrycurrency}
        </td>
      </tr>
      <tr>
        <td class="sitetext">
          &nbsp;&nbsp;&nbsp;{#transport#}
        </td>
        <td class="sitetext">
          &nbsp;&nbsp;&nbsp;{#planed#}: {$cat->budget_transport_planed+0} {$countrycurrency}
        </td>
        <td class="sitetext">
          &nbsp;&nbsp;&nbsp;{#spent#}: {$cat->budget_transport_spent+0} {$countrycurrency}
        </td>
      </tr>
      <tr>
        <td class="sitetext">
          &nbsp;&nbsp;&nbsp;{#other#}</span>
        </td>
        <td class="sitetext">
          &nbsp;&nbsp;&nbsp;{#planed#}: {$cat->budget_other_planed+0} {$countrycurrency}
        </td>
        <td class="sitetext">
          &nbsp;&nbsp;&nbsp;{#spent#}: {$cat->budget_other_spent+0} {$countrycurrency}
        </td>
      </tr>
      <tr>
        <td class="sitetext">
          <b>&nbsp;&nbsp;&nbsp;{#total#}</b>
        </td>
        <td class="sitetext">
          <b>&nbsp;&nbsp;&nbsp;{#planed#}: {$total.planed+0} {$countrycurrency}</b>
        </td>
        <td class="sitetext">
          <b>&nbsp;&nbsp;&nbsp;{#spent#}: {$total.spent+0} {$countrycurrency}</b>
        </td>
      </tr>
    </table>
  </div>
  <div style="clear:both"></div>
  {if ($budget && $costAccommodation)}
  <br />
  <p class="sitetitle">{#accommodation#} :</p>
  <table cellpadding="0" cellspacing="0" width="100%" align="center" border="0">
    <tbody>
      <tr>
        <td class="sitetitle" style="BORDER-BOTTOM: #b3c0d0 1px solid" align=middle width="18" height="18" style="font-size:11px">
          {#date#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
          {#type#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
          {#name#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
          {#address#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
          {#city#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
          {#price#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
          {#duration#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
          {#quality#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
          {#info#}&nbsp;
        </td>
      </tr>
      {foreach from=$costAccommodation item=itemCost}
      <tr valign="top">
        <td align="left">
          <p class="sitetext" style="font-size:11px">{$itemCost->date|fdate}</p>
        </td>
        <td align="left">
          <p class="sitetext" style="font-size:11px">{$itemCost->type}</p>
        </td>
        <td align="left">
          <p class="sitetext" style="font-size:11px">{$itemCost->name|sslash}</p>
        </td>
        <td align="left">
          {if $itemCost->address}
          <img title="{#address#}" src="{$picPath}info.gif" style="cursor:pointer" onclick="alert ('{$itemCost->address|sslash}', 'infoAlert');" />
          {else}
          &nbsp;
          {/if}
        </td>
        <td align="left">
          <p class="sitetext" style="font-size:11px">{$itemCost->arrivalCity|sslash}</p>
        </td>
        <td align="left">
          <p class="sitetext" style="font-size:11px">{$itemCost->price+0} {$countriescurrency.$keyCat}</p>
        </td>
        <td align="left">
          <p class="sitetext" style="font-size:11px">{$itemCost->duration}</p>
        </td>
        <td align="left">
          <p class="sitetext" style="font-size:11px">{$itemCost->quality}</p>
        </td>
        <td align="left">
          {if $itemCost->description}
          <img title="{#moreinfo#}" src="{$picPath}info.gif" style="cursor:pointer" onclick="alert ('{$itemCost->description|sslash}', 'infoAlert');" />
          {else}
          &nbsp;
          {/if}
        </td>
      </tr>
      {/foreach}
    </tbody>
  </table>
  {/if}
  {if ($budget && $costTransport)}
  <br />
  <p class="sitetitle">{#transports#} :</p>
  <table cellpadding="0" cellspacing="0" width="100%" align="center" border="0">
    <tbody>
      <tr>
        <td class="sitetitle" style="BORDER-BOTTOM: #b3c0d0 1px solid" align=middle width="18" height="18" style="font-size:11px">
          {#date#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
          {#type#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
          {#name#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
          {#departureCity#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
          {#arrivalCity#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
         {#length#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
          {#duration#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
          {#price#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
          {#quality#}&nbsp;
        </td>
      </tr>
      {foreach from=$costTransport item=itemCost}
      <tr valign="top">
        <td align="left">
          <p class="sitetext" style="font-size:11px">{$itemCost->date|fdate}</p>
        </td>
        <td align="left">
          <p class="sitetext" style="font-size:11px">{$itemCost->type}</p>
        </td>
        <td align="left">
          <p class="sitetext" style="font-size:11px">{$itemCost->name|sslash}</p>
        </td>
      <td align="left">
          <p class="sitetext" style="font-size:11px">{$itemCost->departureCity|sslash}</p>
        </td>
        <td align="left">
          <p class="sitetext" style="font-size:11px">{$itemCost->arrivalCity|sslash}</p>
        </td>
        <td align="left">
          <p class="sitetext" style="font-size:11px">{$itemCost->length} {#km#}</p>
        </td>
        <td align="left">
          <p class="sitetext" style="font-size:11px">{$itemCost->duration} {#hour#}</p>
        </td>
        <td align="left">
          <p class="sitetext" style="font-size:11px">{$itemCost->price+0} {$countrycurrency}</p>
        </td>
        <td align="left">
          <p class="sitetext" style="font-size:11px">{$itemCost->quality}</p>
        </td>
      </tr>
      {/foreach}
    </tbody>
  </table>
  {/if}
  {if ($budget && $costFood)}
  <br />
  <p class="sitetitle">{#food#} :</p>
  <table cellpadding="0" cellspacing="0" width="100%" align="center" border="0">
    <tbody>
      <tr>
        <td class="sitetitle" style="BORDER-BOTTOM: #b3c0d0 1px solid" align=middle width="18" height="18" style="font-size:11px">
          {#date#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
          {#type#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
          {#name#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
          {#address#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
          {#city#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
          {#price#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
          {#quality#}&nbsp;
        </td>
        <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
          {#info#}&nbsp;
        </td>
      </tr>
      {foreach from=$costFood item=itemCost}
      <tr valign="top">
        <td align="left">
          <p class="sitetext" style="font-size:11px">{$itemCost->date|fdate}</p>
        </td>
        <td align="left">
          <p class="sitetext" style="font-size:11px">{$itemCost->type}</p>
        </td>
        <td align="left">
          <p class="sitetext" style="font-size:11px">{$itemCost->name|sslash}</p>
        </td>
        <td align="left">
          {if $itemCost->address}
          <img title="{#address#}" src="{$picPath}info.gif" style="cursor:pointer" onclick="alert ('{$itemCost->address|sslash}', 'infoAlert');" />
          {else}
          &nbsp;
          {/if}
        </td>
        <td align="left">
          <p class="sitetext" style="font-size:11px">{$itemCost->arrivalCity|sslash}</p>
        </td>
        <td align="left">
          <p class="sitetext" style="font-size:11px">{$itemCost->price+0} {$countriescurrency.$keyCat}</p>
        </td>
        <td align="left">
          <p class="sitetext" style="font-size:11px">{$itemCost->quality}</p>
        </td>
        <td align="left">
          {if $itemCost->description}
          <img title="{#moreinfo#}" src="{$picPath}info.gif" style="cursor:pointer" onclick="alert ('{$itemCost->description|sslash}', 'infoAlert');" />
          {else}
          &nbsp;
          {/if}
        </td>
      </tr>
      {/foreach}
    </tbody>
  </table>
  {/if}
  {/if}
</div>
