<?php
  include_once('../lib/init.php');


  // Si le option du bétisier n'est pas activé, renvoie à la page d'accueil
  if (!$menuOption->humour) {header ("Location:".PAGE_ACCUEIL);exit();}


  // Initialisation des données
  $pageName = "humour";
  $splitDisplay = new SplitDisplay (4, 9);
  $access="";
  $subject="";
  $text="";
  $images=null;
  $imgHumour = new Media ('humour', $msgHandler, $fileManager, $albumManager, $quota_path, $humourImgPath, true, 4);
  $fileManager->setOriginal ($origImgPath);
  if (isset ($_POST['actions']) && $_POST['actions']<>'') $action = $_POST['actions']; else $action = 'new';


  // Gestion de l'affichage des résultats
  if (isset($_GET['next'])) {
    $splitDisplay->start=$_GET['next'];
    if (isset($_GET['page'])) {
      $splitDisplay->first=$_GET['page'];
    } else {
      $splitDisplay->first=$_GET['next']*$splitDisplay->length;
    }
  }


  // Initialisation des données renvoyées par le formulaire
  if (!isset($_POST['init_'.$pageName]) || (isset ($_POST['cancel']) && $_POST['cancel']=='true') || !$admin) {
    $fileManager->init();
    $action = 'new';
    $id = $humourManager->getNewID();
  } else {
    $singleMessage = $humourManager->getMessage ($_POST['id']);
    $action='edit';
    $access = 'public';
    $id = $_POST['id'];
    $subject = addslash($_POST['subject']);
    $text = addslash($_POST['text']);
    $images = $fileManager->getTempFiles('humour');
  }


  // Si le bouton "Changer d'accès" a été pressé, modifie l'accès du message
  if (isset ($_POST['changeAccess']) && $_POST['changeAccess'] <> '') {
    $singleMessage = $humourManager->getMessage ($_POST['changeAccess']);
    if ($singleMessage->access == 'public') {
      $access = 'private';
    } else {
      $access = 'public';
    }
    $messageToSave = new MessageObj ($singleMessage->id, $singleMessage->date, $access, $singleMessage->subject, $singleMessage->text, $singleMessage->images);
    $humourManager->editMessage ($messageToSave);
    $id = $humourManager->getNewID();
    header ("Location:".PAGE_REDIRECTION."?redirection=humour");
    exit();
  }


  // Si le bouton "Edition du message" a été pressé, modifie l'accès du message
  if (isset ($_POST['editMessage']) && $_POST['editMessage'] <> '') {
    $singleMessage = $humourManager->getMessage ($_POST['editMessage']);
    $id = $singleMessage->id;
    $subject = $singleMessage->subject;
    $text = $singleMessage->text;
    $fileManager->init();
    if ($singleMessage->images && count ($singleMessage->images) > 0) {
      foreach ($singleMessage->images as $itemImg) {
        $fileManager->addImage ('humour', $humourImgPath, $itemImg->name, $itemImg->description);
      }
      $images = $fileManager->getTempFiles('humour');
    }
  }


  // Si le bouton "Suppression du message" a été pressé, modifie l'accès du message
  if (isset ($_POST['deleteMessage']) && $_POST['deleteMessage'] <> '') {
    $humourManager->deleteMessage ($_POST['deleteMessage']);
    $id = $humourManager->getNewID();
    if (isset ($_SESSION['menuCount'])) unset ($_SESSION['menuCount']);
    header ("Location:".PAGE_REDIRECTION."?redirection=humour");
    exit();
  }


  // Si le bouton "Save" a été pressé, sauvegarde les informations de la destination éditée
  if (isset ($_POST['save']) && $_POST['save']=='true') {
    if ($subject=='') $error = $msgHandler->getError ('insertSubject');
    if (isset($_POST['email']) && $_POST['email']<>'' && !is_email ($_POST['email'])) $error = $msgHandler->getError ('invalidEmail');
    if (!$error) {
      $images = $fileManager->saveTempFiles ('humour', $humourImgPath);
      $messageToSave = new MessageObj ($id, time(), $access, $subject, $text, $images);
      $humourManager->editMessage ($messageToSave);
      $id = $humourManager->getNewID();
      if (isset ($_SESSION['menuCount'])) unset ($_SESSION['menuCount']);
      header ("Location:".PAGE_REDIRECTION."?redirection=humour");
      exit();
    }
  }


  // Si le bouton "Add Image" a été pressé, ajoute l'image à la liste
  if (isset ($_POST['addImage']) && $_POST['addImage']=='save') {
    $resultAdd = $imgHumour->addImage($_POST['imageDescription']);
    if ($resultAdd[0] && count ($resultAdd[0]) > 0) $images = $resultAdd[0];
    if ($resultAdd[1]<>'') $error = $resultAdd[1];
  }


  // Si le bouton "Delete Image" a été pressé, efface l'image
  if (isset ($_POST['deleteImage']) && $_POST['deleteImage']<>'') {
    $images = $fileManager->deleteTempFile ('humour', $_POST['deleteImage']);
  }


  // Gestion de l'affichage des résultats
  $newMessage = new MessageObj ($id, fdate(time()), $access, $subject, $text, $images);
  $splitDisplay->total = $humourManager->getNbMessages ($admin);
  $message = $humourManager->getMessages ($splitDisplay->first, $splitDisplay->length, $admin, 'date');
  if ($splitDisplay->length > 0) {
    $splitDisplay->loop = ceil(($splitDisplay->total)/$splitDisplay->length);
    $splitDisplay->first = ceil(($splitDisplay->first)/$splitDisplay->length);
  }


  // Gestion des emoticons
  $emotes = Emoticon::getEmotes (PICS_PATH.'/emoticon/');
  if ($message) {
    foreach ($message as $itemmessage) {
      $itemmessage->text = Emoticon::setEmotes (PICS_PATH.'/emoticon/', $itemmessage->text);
    }
  }
  $nbEmote = count ($emotes);


  // Assignation de Smarty
  $smarty->assign ('imgPath', $humourImgPath);
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuHumour'), $admin, 3));
  $smarty->assign ('imgHumour', $imgHumour->output('web'));
  $smarty->assign ('emotes', $emotes);
  $smarty->assign ('nbEmote', $nbEmote);
  $smarty->assign ('textarea', 'document.form.text');
  $smarty->assign ('showEmote', $smarty->fetch ('emoticon.tpl'));
  $smarty->assign ('splitDisplay', $splitDisplay);
  $smarty->assign ('message', $message);
  $smarty->assign ('action', $action);
  $smarty->assign ('newMessage', $newMessage);
  $smarty->assign ('pageName', $pageName);
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->assign ('albumTitle', $albumTitle);
  $smarty->assign ('admin', $admin);
  $smarty->display($pageName.'.tpl');

?>

