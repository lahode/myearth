<?php
  include_once('../lib/init.php');
  include_once('../lib/NewsManager.php');


  // Efface l'utilisateur de la news à distance
  if (isset ($_GET['delete']) && $_GET['delete'] <> '') {
    if ($registerNewsManager->getPerson($_GET['delete'])) {
      $registerNewsManager->deletePerson($_GET['delete']);
      header ("Location:".PAGE_ACCUEIL."?confirm=*deletePersonFromNews");
      exit();
    } else {
      $error = $msgHandler->getError ('inexistantPerson');
    }
  }


  // Si le option des news n'est pas activée, renvoie à la page d'accueil
  if (!$menuOption->news) {header ("Location:".PAGE_ACCUEIL);exit();}


  // Efface l'utilisateur de la news à distance
  if (isset ($_POST['deletePerson']) && $_POST['deletePerson'] <> '') {
    if ($registerNewsManager->getPerson($_POST['deletePerson'])) {
      $registerNewsManager->deletePerson($_POST['deletePerson']);
      header ("Location:".PAGE_REDIRECTION."?redirection=registernews&confirm=*deletePerson");
      exit();
    } else {
      $error = $msgHandler->getError ('inexistantPerson');
    }
  }


  // Initialisation des données
  $pageName = "registernews";
  $message = $registerNewsManager->getMessage();
  $registeredPersons = $registerNewsManager->getPersons();


  // Si le bouton "Save" a été pressé, sauvegarde les informations de la personne
  if (isset ($_POST['save']) && $_POST['save']=='true') {
    if (isset ($_POST['email']) && isset ($_POST['name']) && $_POST['email']<>'' && $_POST['name']<>'') {
      if ($email = is_email($_POST['email'])) {
        if (!$registerNewsManager->getPersonByEmail(addslash($_POST['email']))) {
          do {
            $id = genID();
          } while ($registerNewsManager->getPerson($id));
          $registerNewsManager->insertPerson (new Person ($id, addslash($_POST['name']), addslash($_POST['email']), time()));
          header ("Location:".PAGE_REDIRECTION."?redirection=news&confirm=*registerNews");
          exit();
        } else {
          $error = $msgHandler->getError ('existantPerson');
        }
      } else {
        $error = $msgHandler->getError ('invalidEmail');
      }
    } else {
      $error = $msgHandler->getError ('insertNameorEmail');
    }
  }


  // Si le bouton "Save Msg" a été pressé, sauvegarde le message
  if (isset ($_POST['saveMsg']) && $_POST['saveMsg']=='true') {
    if (isset ($_POST['message'])) $message = $_POST['message'];
    $registerNewsManager->editMessage ($message);
  }


  // Assignation de Smarty
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuNews'), $admin, 3));
  $smarty->assign ('message', stripslashes($message));
  $smarty->assign ('registeredPersons', $registeredPersons);
  $smarty->display($pageName.'.tpl');

?>

