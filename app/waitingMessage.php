<style type="text/css">
<!--
  DIV#loading {
    width: 400px;
    background-color: #c0c0c0;
    position: absolute;
    left: 50%;
    top: 50%;
    margin-top: -50px;
    margin-left: -100px;
    text-align: center;
    font-family: Georgia, "Courier New", "New Roman", Verdana, Serif.;
    font-weight: bold;
    font-size: 18px;
  }
  DIV#progressholder {
    Z-INDEX: 101;
    LEFT: 0px;
    OVERFLOW: hidden;
    WIDTH: 254px;
    POSITION: relative;
    TOP: 0px;
    HEIGHT: 16px;
    background-color:#000000;
  }

  DIV#progresswn {
    Z-INDEX: 1;
    LEFT: 2px;
    OVERFLOW: hidden;
    WIDTH: 252px;
    POSITION: absolute;
    TOP: 2px;
    HEIGHT: 14px
  }

  DIV#progresscontent {
    position:absolute;
    background-color: #EEEEEE;
    border: 1px solid #000000;
	width: 250px;
    height: 12px;
        left: 0px;
        top: 0px;
  }

  .progressbar {
    font-size:1px;
    position:absolute;
    height: 10px;
  }

  DIV#progressbar1 {
    left:-50px;
    width: 2px;
    background-color: #BBBBBB;
  }

  DIV#progressbar2 {
    left:-48px;
    width: 4px;
    background-color: #999999;
  }

  DIV#progressbar3 {
    left:-44px;
    width: 6px;
    background-color: #666666;
  }

  DIV#progressbar4 {
    left:-38px;
    width: 8px;
    background-color: #333333;
  }

  DIV#progressbar5 {
    left:-30px;
    width: 10px;
    background-color: #000000;
  }

  DIV#progressbar6 {
    left:-20px;
    width: 8px;
    background-color: #333333;
  }

  DIV#progressbar7 {
    left:-12px;
    width: 6px;
    background-color: #666666;
  }

  DIV#progressbar8 {
    left:-6px;
    width: 4px;
    background-color: #999999;
  }

  DIV#progressbar9 {
    left:-2px;
    width: 2px;
    background-color: #BBBBBB;
  }
-->
</style>

<script type="text/javascript">
  var progressbar1 = -50;
  var progressbar2 = -48;
  var progressbar3 = -44;
  var progressbar4 = -38;
  var progressbar5 = -30;
  var progressbar6 = -20;
  var progressbar7 = -12;
  var progressbar8 = -6;
  var progressbar9 = -2;
  var progress = true;
  var stopProgress = false;
  function timedProg() {
    if (progressbar9 <= 300 && progress && !stopProgress) {
      document.getElementById("progressbar1").style.left=progressbar1+"px";
      document.getElementById("progressbar2").style.left=progressbar2+"px";
      document.getElementById("progressbar3").style.left=progressbar3+"px";
      document.getElementById("progressbar4").style.left=progressbar4+"px";
      document.getElementById("progressbar5").style.left=progressbar5+"px";
      document.getElementById("progressbar6").style.left=progressbar6+"px";
      document.getElementById("progressbar7").style.left=progressbar7+"px";
      document.getElementById("progressbar8").style.left=progressbar8+"px";
      document.getElementById("progressbar9").style.left=progressbar9+"px";
      var j=0;
      while (j<=100)
        j++;
        setTimeout("timedProg();", 1);
        progressbar1=progressbar1+2;
        progressbar2=progressbar2+2;
        progressbar3=progressbar3+2;
        progressbar4=progressbar4+2;
        progressbar5=progressbar5+2;
        progressbar6=progressbar6+2;
        progressbar7=progressbar7+2;
        progressbar8=progressbar8+2;
        progressbar9=progressbar9+2;
    } else progress = false;
    if (progressbar9 >= -3 && !progress && !stopProgress) {
      document.getElementById("progressbar1").style.left=progressbar1+"px";
      document.getElementById("progressbar2").style.left=progressbar2+"px";
      document.getElementById("progressbar3").style.left=progressbar3+"px";
      document.getElementById("progressbar4").style.left=progressbar4+"px";
      document.getElementById("progressbar5").style.left=progressbar5+"px";
      document.getElementById("progressbar6").style.left=progressbar6+"px";
      document.getElementById("progressbar7").style.left=progressbar7+"px";
      document.getElementById("progressbar8").style.left=progressbar8+"px";
      document.getElementById("progressbar9").style.left=progressbar9+"px";
      var j=0;
      while (j<=100)
        j++;
        setTimeout("timedProg();", 1);
        progressbar1=progressbar1-2;
        progressbar2=progressbar2-2;
        progressbar3=progressbar3-2;
        progressbar4=progressbar4-2;
        progressbar5=progressbar5-2;
        progressbar6=progressbar6-2;
        progressbar7=progressbar7-2;
        progressbar8=progressbar8-2;
        progressbar9=progressbar9-2;
    } else progress = true;
  if (!stopProgress && progress && progressbar9 < -2) timedProg();
  }

  function stopProg() {
    stopProgress = true;
  }

</script>

<body>
<div id=progressholder>
  <div id=progresswn>
    <div id="progresscontent" align="center">
      <div id="progressbar1" class="progressbar">&nbsp;</div>
      <div id="progressbar2" class="progressbar">&nbsp;</div>
      <div id="progressbar3" class="progressbar">&nbsp;</div>
      <div id="progressbar4" class="progressbar">&nbsp;</div>
      <div id="progressbar5" class="progressbar">&nbsp;</div>
      <div id="progressbar6" class="progressbar">&nbsp;</div>
      <div id="progressbar7" class="progressbar">&nbsp;</div>
      <div id="progressbar8" class="progressbar">&nbsp;</div>
      <div id="progressbar9" class="progressbar">&nbsp;</div>
    </div>
  </div>
</div>
<input type="button" onclick="stopProg()" />
</body>
<script type="text/javascript">
  timedProg();
</script>

