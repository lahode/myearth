<?php
  include_once('../lib/init.php');


  // Affiche un pays
  function showCountry ($catInput) {
    // Initialisation des variables globales
    global $routeManager;
    global $costsManager;
    global $dbQuery;
    global $property;
    global $msgHandler;
    global $language;
    global $smarty;
    global $webImgPath;

    // Vérification des variables en entrée
    $response = new xajaxResponse('ISO-8859-1');
    $category = $routeManager->getCategoryBy ('id', $catInput);
    $allDest =  $routeManager->getDestinations ($catInput);
    if (!$category || !$allDest) {$response->addRedirect(PAGE_DESTINATION);return $response->getXML();}
    $allDest = sortObjBy ($allDest, 'startingDate', SORT_ASC);

    // Initialisation des données
    $pageNameHeader = "showheader";
    $pageNameDescription = "showdescription";
    $pageNameContent = "showcountry";
    $country = $category->country;
    $costTransport = $costsManager->getCosts(0, $category->id);
    $costAccommodation = $costsManager->getCosts(1, $category->id);
    $leavingDate = "";
    $description = "";
    $decalTime = 0;
    $details = null;
    $dbQuery->connect();
    $country_name = $dbQuery->getCountryName ($country);
    $countryDetails = $dbQuery->getCountryDetails ($country);
    $dbQuery->disconnect();

    // Récupère la date de départ
    if ($allDest[sizeof($allDest)-1]->startingDate > 0 && $allDest[sizeof($allDest)-1]->nbDays > 0) {
      $leavingDate = $allDest[sizeof($allDest)-1]->startingDate + (3600*24*$allDest[sizeof($allDest)-1]->nbDays);
    } else {
      $leavingDate = '('.$msgHandler->get('noInfo').')';
    }

    // Gestion du budget
    $total = array();
    if (($category->budget_accommodation_planed > 0 || $category->budget_food_planed > 0 || $category->budget_transport_planed > 0 &&
         $category->budget_other_planed > 0) && ($category->budget_accommodation_spent > 0 || $category->budget_food_spent > 0 ||
         $category->budget_transport_spent > 0 && $category->budget_other_spent > 0) && $property->showStats) {
      $stats = false;
      $total['planed'] = $category->budget_accommodation_planed+$category->budget_food_planed+$category->budget_transport_planed+$category->budget_other_planed;
      $total['spent'] = $category->budget_accommodation_spent+$category->budget_food_spent+$category->budget_transport_spent+$category->budget_other_spent;
    } else {
      $stats = $msgHandler->get('noInfo');
    }

    // Gestion des conseils
    if ($category->text == '') $category->text = $msgHandler->get('noInfo');

    // Récupère le nom des régions et villes
    $dbQuery->connect();
    foreach ($allDest as $keyDest => $itemDest) {
      $itemDest->region = $dbQuery->getRegionName($country, $itemDest->region);
      $itemDest->city = $dbQuery->getCityName($country, $itemDest->city);
      if ($itemDest->cityName <> '') $itemDest->city = $itemDest->cityName;
      if ($itemDest->images) {
        $itemDest->text = shorten ($itemDest->text, 50);
        if (count ($itemDest->images) > 3) $itemDest->images = array_slice ($itemDest->images, 0, 3);
      } else {
        $itemDest->text = shorten ($itemDest->text, 150);
      }
    }

    // Récupère la monnaie courante du pays
    $countrycurrency = $dbQuery->getCountryCurrency ($country);

    // Création des détails
    if ($property->showDescription && $countryDetails) {
      $decalTime = $dbQuery->getCountryTime ($country);
      $description = $dbQuery->getDescription ($country, 0, $language);
      $details[0][0] = $msgHandler->get('populationOf').' '.$country_name;
      if ($countryDetails->population == 0)
        $details[0][1] = $msgHandler->get('noInfo');
      else
        $details[0][1] = nformat(utf8_decode ($countryDetails->population)).' '.$msgHandler->get('inhabitant');
      $details[1][0] = $msgHandler->get('capital');
      if ($countryDetails->capitale=='')
        $details[1][1] = $msgHandler->get('noInfo');
      else
        $details[1][1] = $countryDetails->capitale;
      $details[2][0] = $msgHandler->get('surface');
      if ($countryDetails->capitale=='')
        $details[2][1] = $msgHandler->get('noInfo');
      else
        $details[2][1] = nformat($countryDetails->surface).' km2';
      $details[3][0] = $msgHandler->get('continent');
      if ($countryDetails->capitale=='')
        $details[3][1] = $msgHandler->get('noInfo');
      else
        $details[3][1] = $msgHandler->getTable('continent', $countryDetails->continent);
    }
    $dbQuery->disconnect();

    // Remplace les valeurs des Costs pour affichages
    if (count ($costTransport) > 0) {
      foreach ($costTransport as $keyCost => $costItem) {
        $costTransport[$keyCost]->type = $msgHandler->getTable ('arrivalBy', $costItem->type);
        $costTransport[$keyCost]->quality = $tableQuality[$costItem->quality];
      }
    }
    if (count ($costAccommodation) > 0) {
      foreach ($costAccommodation as $keyCost => $costItem) {
        $costAccommodation[$keyCost]->type = $msgHandler->getTable ('accommodation', $costItem->type);
        $costAccommodation[$keyCost]->quality = $tableQuality[$costItem->quality];
      }
    }

    // Assignation de Smarty
    $smarty->assign ('picPath', PICS_PATH);
    $smarty->assign ('leavingDate', $leavingDate);
    $smarty->assign ('destinations', $allDest);
    $smarty->assign ('details', $details);
    $smarty->assign ('description', $description);
    $smarty->assign ('stats', $stats);
    $smarty->assign ('budget', $property->showBudget);
    $smarty->assign ('total', $total);
    $smarty->assign ('webImgPath', $webImgPath);
    $smarty->assign ('cat', $category);
    $smarty->assign ('countrycurrency', $countrycurrency);
    $smarty->assign ('countryID', $country);
    $smarty->assign ('costTransport', $costTransport);
    $smarty->assign ('costAccommodation', $costAccommodation);
    $smarty->assign ('userLanguage', $language);
    $displayHeader = $smarty->fetch ($pageNameHeader.'.tpl');
    $displayContent = $smarty->fetch ($pageNameContent.'.tpl');
    $displayDescription = $smarty->fetch ($pageNameDescription.'.tpl');

    // Renvoi le contenu vers xajax pour affichage
    $response->addAssign ("headerShow", "innerHTML", $displayHeader);
    $response->addAssign ("contentShow", "innerHTML", $displayContent);
    $response->addAssign ("descriptionShow", "innerHTML", $displayDescription);
    $response->addScriptCall ("clockajust", $decalTime);
    return $response->getXML();
  }//showCountry


  $xajax->processRequests();

?>
