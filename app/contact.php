<?php

  // Récupère les emails d'un fichier
  function getEmailsFromFile ($filename, $separator) {
    if (file_exists ($filename) && filesize ($filename) > 0) {
      $handle = fopen($filename, "r");
      $content = fread ($handle, filesize ($filename));
      fclose($handle);
      $result = explode ($separator, $content);
      if (count ($result) > 0) return $result;
    }
    return array();
  }


  // Sauvegarde les emails dans un fichier
  function saveEmailsToFile ($filename, $separator, $emails) {
    $result = '';
    if (count ($emails) > 0) {
      foreach ($emails as $email) {
        if ($result <> '') $result .= $separator;
        $result .= $email;
      }
      $handle = fopen($filename, "w");
      $content = fwrite ($handle, $result);
      fclose($handle);
      @chmod ($filename, 0777);
    } else {
      deleteFile ($filename);
    }
  }


  // Vérifie si la page est appelée depuis une session
  if (isset ($_GET['session']) && $_GET['session']=='active') {
    include_once ('../lib/init.php');
    $showMenu = 1;
    $showJSError = true;
    $sessionStatus = 'active';
    $tabEmails = $albumManager->getEmails();
  } else {
    include_once ('../lib/initNoSession.php');
    $showMenu = -1;
    $showJSError = false;
    $sessionStatus = '';
    $tabEmails = null;
  }


  // Initialisation des données
  $pageName = "contact";
  $fileContact = USERS_PATH.$albumName.'/contacts.lst';
  $separatorContact = ';';
  $personToSend = null;
  if (isset ($_POST['emails'])) $emails = $_POST['emails']; else $emails = array();
  if (!isset($_POST['init_'.$pageName])) $emails = getEmailsFromFile ($fileContact, $separatorContact);
  if (isset ($_POST['nom'])) $nom = addslash ($_POST['nom']); else $nom = "";
  if (isset ($_POST['subject'])) $subject = addslash ($_POST['subject']); else $subject = "";
  if (isset ($_POST['email'])) $email = addslash ($_POST['email']); else $email = "";
  if (isset ($_POST['message'])) $message = addslash ($_POST['message']); else $message = "";
  if (isset ($_GET['webmaster']) && $_GET['webmaster']=='true') {
    $personToSend = array (new Person (genID(), WEBMASTER, WEBMASTER, time()));
    $contactWebmaster = true;
  } else {
    if (count ($emails > 0)) {
      foreach ($emails as $to) {
        $personToSend[] = new Person (null, $to, $to, time());
      }
    }
    $contactWebmaster = false;
  }
  $emailExist = false;


  // Gestion de l'e-mail
  if (isset ($_POST['sentEmail']) && $_POST['sentEmail']=='true') {
    if ($nom <> '' && $email <> '' && $message <> '' && $subject <> '') {
      if (is_email($email)) {
        $messageToSend = new MessageObj (genID(), time(), 'public', $subject, nl2br($message), null);
        if ($personToSend) {
          $mailSender->addTo($personToSend);
          $mailSender->model->addFROM (USER_EMAIL);
          $mailSender->model->addHTML ($messageToSend->text);
          $mailSender->model->addSubject ($messageToSend->subject);
          if (!$mailSender->sender->send()) $error = $msgHandler->getError ('emailSent');
        }
        if (!$contactWebmaster) {
          do {
            $id = genID();
          } while ($emailsManager->getEmail($id));
          $emailsManager->editEmail (new Mail ($id, time(), $email, null, null, null, '', $messageToSend->subject, html_entity_decode($messageToSend->text, ENT_NOQUOTES, 'ISO-8859-1'), null, $nom), null);
        }
        if ($sessionStatus == 'active')
          header ("Location:".PAGE_REDIRECTION."?redirection=home&confirm=*messageConfirm");
        else
          header ("Location:".PAGE_REDIRECTION."?redirection=login&confirm=*messageConfirm&album=".$albumName);
        exit();
      } else {
        $email = "";
        $error = $msgHandler->getError ('invalidEmail');
      }
    } else {
      $error = $msgHandler->getError ('fieldError');
    }
  }


  // Si le bouton "Add New Email" a été pressé, ajoute un email entr� par l'utilisateur � la liste
  if (isset ($_POST['addnewEmail']) && $_POST['addnewEmail']=='save') {
    if (isset ($_POST['newemail']) && is_email ($_POST['newemail'])) {
      foreach ($emails as $itemEmail) {
        if ($itemEmail == $_POST['newemail']) $emailExist = true;
      }
      if (!$emailExist) {
        $emails[] = $_POST['newemail'];
      }
    } else {
      $error = $msgHandler->getError ('invalidEmail');
    }
  }


  // Si le bouton "Add Existing Email" a été pressé, ajoute un email existant � la liste
  if (isset ($_POST['addexistingEmail']) && $_POST['addexistingEmail']=='save') {
    foreach ($emails as $itemEmail) {
      if ($itemEmail == $tabEmails[$_POST['existingemail']]) $emailExist = true;
    }
    if (!$emailExist) {
      $emails[] = $tabEmails[$_POST['existingemail']];
    }
  }


  // Si le bouton "Delete Email" a été pressé, efface l'email de la liste
  if (isset ($_POST['deleteEmail']) && $_POST['deleteEmail']<>'') {
    unset ($emails[$_POST['deleteEmail']]);
  }


  // Si le bouton "Save" a été pressé, enregistre l'e-mails dans un fichier
  if (isset ($_POST['save']) && $_POST['save']=='true') {
    saveEmailsToFile ($fileContact, $separatorContact, $emails);
  }


  // Assignation de Smarty
  $smarty->assign ('pageName', $pageName);
  $smarty->assign ('nom', $nom);
  $smarty->assign ('email', $email);
  $smarty->assign ('tabEmails', $tabEmails);
  $smarty->assign ('emails', $emails);
  $smarty->assign ('admin', $admin);
  $smarty->assign ('message', $message);
  $smarty->assign ('contactWebmaster', $contactWebmaster);
  $smarty->assign ('sessionStatus', $sessionStatus);
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, '', $showJSError, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, '', $admin, $showMenu));
  $smarty->assign ('error', $error);
  $smarty->display($pageName.'.tpl');
?>

