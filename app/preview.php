<?php
  include_once('../lib/init.php');
  include_once('../lib/CSSCreator.php');


  // Transformation des styles de la page de prévisulation
  $pageName = "preview";
  $cssTemp = "<style type=\"text/css\">\r\n";
  if (isset ($_SESSION['properties']['properties'])) $property = $_SESSION['properties']['properties'];
  if (isset ($_SESSION['properties']['menuOptions'])) $menuOption = $_SESSION['properties']['menuOptions'];
  if (isset ($_SESSION['fontstyles'])) {
    foreach ($_SESSION['fontstyles']['obj'] as $keyStyle => $itemStyle) {
      $cssTemp.= createCSSElement ('', $itemStyle);
    }
    $cssTemp.= "\r\n";
  }
  if ($property->skin == 'Design') $roundCorner = true; else $roundCorner = false;
  $tempImgToGet = $fileManager->getTempFiles('bg');
  if (isset ($tempImgToGet)) {
    $property->bgImg = key ($tempImgToGet).$tempExt;
    $pathToShow=$tempPath;
  } else {
    $pathToShow=$backgroundImgPath;
  }
  $cssTemp.= createCSSGlobal ('', $property, $pathToShow, $roundCorner);
  $cssTemp.= "</style>\r\n";
  $header1 = new Header ($language, $smarty, $headerPath, $_SESSION['countriesList'], $links, $soundPath, $menuOption, $property, $_SESSION['menuCount'], $albumName, $tableSkins[$property->skin]);


  // Gestion de l'affichage des résultats
  $splitDisplay = new SplitDisplay (4, 9);
  $splitDisplay->total = 30;
  if ($splitDisplay->length > 0) {
    $splitDisplay->loop = ceil(($splitDisplay->total)/$splitDisplay->length);
    $splitDisplay->first = ceil(($splitDisplay->first)/$splitDisplay->length);
  }


  // Assignation de Smarty
  $smarty->assign ('showFooter', $header1->showFooter ('', '', '', true, 0));
  $smarty->assign ('showTopHeader', $header1->showTopHeader ($cssTemp));
  $smarty->assign ('showHeader', $header1->showHeader ($albumTitle, '', true, 'preview'));
  $smarty->assign ('splitDisplay', $splitDisplay);
  $smarty->display($pageName.'.tpl');

?>

