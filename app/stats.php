<?php
  include_once('../lib/init.php');


  // Si le option des statistiques n'est pas activée, renvoie à la page d'accueil
  if (!$menuOption->statistics && ($admin || $property->showStats)) {header ("Location:".PAGE_ACCUEIL);exit();}


  if (isset ($_POST['selectGraph']) && $_POST['selectGraph']<>'') {
    $selectGraph = $_POST['selectGraph'];
  } else {
    $selectGraph = 'total';
  }


  // Récupère toutes les information de l'album
  $departure = $routeManager->getDeparture ();
  $firstDay = $departure->startingDate;
  if ((time()+(480*3600)) < $departure->endingDate) {
    $lastDay = time()+(240*3600);
  } else {
    $lastDay = $departure->endingDate;
  }

  // Initialisation des variables
  $pageName = "stats";
  $calc1 = array();
  $calc2 = array();
  $endDate = array();
  $resultCat = array();
  $globalTotal = array();
  $total = array();
  $countriescurrency = array();
  $stateofBudget = "";
  $sparedRecover = "";
  $grandTotal = 0;


  // Substitution des données des catégories
  if (isset ($_POST['currency']) && $_POST['currency']=='1') $selectedCurrency = 1; else $selectedCurrency = 0;
  if ($departure) {$dbQuery->connect();$maincurrency = $dbQuery->getCountryCurrency ($departure->country);$dbQuery->disconnect();} else $maincurrency = '';
  if (count ($categories) > 0 && $firstDay<>'' && $lastDay<>'') {
    $categories = sortObjBy ($categories, 'startingDate', SORT_ASC);
    $i = 0;
    $globalTotal = array();
    $spareGlobal = array();
    $globalTotal['total']['planed'] = 0;
    $globalTotal['total']['spent'] = 0;
    $spareGlobal['planed'] = 0;
    $spareGlobal['spent'] = 0;
    $globalTotal['accommodation']['planed'] = 0;
    $globalTotal['accommodation']['spent'] = 0;
    $globalTotal['food']['planed'] = 0;
    $globalTotal['food']['spent'] = 0;
    $globalTotal['transport']['planed'] = 0;
    $globalTotal['transport']['spent'] = 0;
    $globalTotal['other']['planed'] = 0;
    $globalTotal['other']['spent'] = 0;
    $dbQuery->connect();
    foreach ($categories as $keyCat => $category) {
      if ($category->rate > 0) $convertRate = 1 / $category->rate; else $convertRate = 1;
      $countriescurrency[$keyCat]  = $dbQuery->getCountryCurrency ($category->country);
      $category->country = $dbQuery->getCountryName ($category->country);
      $lastDestination = $routeManager->getFirstLastDestination($category->id, true);
      if ($lastDestination) {
        $endDate[$i] = $lastDestination->startingDate+($lastDestination->nbDays-1)*3600*24;
      } else {
        $endDate[$i] = $category->startingDate;
      }
      $total[$i]['planed'] = $category->budget_accommodation_planed+$category->budget_food_planed+$category->budget_transport_planed+$category->budget_other_planed;
      $total[$i]['spent'] = $category->budget_accommodation_spent+$category->budget_food_spent+$category->budget_transport_spent+$category->budget_other_spent;
      $calc1['accommodation'][$categories[$keyCat]->startingDate] = ($category->budget_accommodation_planed+0)*$convertRate;
      $calc2['accommodation'][$categories[$keyCat]->startingDate] = ($category->budget_accommodation_spent+0)*$convertRate;
      $calc1['food'][$categories[$keyCat]->startingDate]          = ($category->budget_food_planed+0)*$convertRate;
      $calc2['food'][$categories[$keyCat]->startingDate]          = ($category->budget_food_spent+0)*$convertRate;
      $calc1['transport'][$categories[$keyCat]->startingDate]     = ($category->budget_transport_planed+0)*$convertRate;
      $calc2['transport'][$categories[$keyCat]->startingDate]     = ($category->budget_transport_spent+0)*$convertRate;
      $calc1['other'][$categories[$keyCat]->startingDate]         = ($category->budget_other_planed+0)*$convertRate;
      $calc2['other'][$categories[$keyCat]->startingDate]         = ($category->budget_other_spent+0)*$convertRate;
      $calc1['total'][$categories[$keyCat]->startingDate]         = ($total[$i]['planed']+0)*$convertRate;
      $calc2['total'][$categories[$keyCat]->startingDate]         = ($total[$i]['spent']+0)*$convertRate;
      $globalTotal['accommodation']['planed'] += $calc1['accommodation'][$categories[$keyCat]->startingDate];
      $globalTotal['accommodation']['spent'] += $calc2['accommodation'][$categories[$keyCat]->startingDate];
      $globalTotal['food']['planed'] += $calc1['food'][$categories[$keyCat]->startingDate];
      $globalTotal['food']['spent'] += $calc2['food'][$categories[$keyCat]->startingDate];
      $globalTotal['transport']['planed'] += $calc1['transport'][$categories[$keyCat]->startingDate];
      $globalTotal['transport']['spent'] += $calc2['transport'][$categories[$keyCat]->startingDate];
      $globalTotal['other']['planed'] += $calc1['other'][$categories[$keyCat]->startingDate];
      $globalTotal['other']['spent'] += $calc2['other'][$categories[$keyCat]->startingDate];
      $globalTotal['total']['planed'] += $calc1['total'][$categories[$keyCat]->startingDate];
      $globalTotal['total']['spent'] += $calc2['total'][$categories[$keyCat]->startingDate];
      if ($calc2['total'][$categories[$keyCat]->startingDate] > 0) { 
        $spareGlobal['planed']+= $calc1['total'][$categories[$keyCat]->startingDate];
        $spareGlobal['spent']+= $calc2['total'][$categories[$keyCat]->startingDate];
      }
      $startVal[$i] = $categories[$keyCat]->startingDate;
      if ($selectedCurrency == 1) {
        $category->budget_accommodation_planed = $category->budget_accommodation_planed*$convertRate;
        $category->budget_food_planed          = $category->budget_food_planed*$convertRate;
        $category->budget_transport_planed     = $category->budget_transport_planed*$convertRate;
        $category->budget_other_planed         = $category->budget_other_planed*$convertRate;
        $category->budget_accommodation_spent  = $category->budget_accommodation_spent*$convertRate;
        $category->budget_food_spent           = $category->budget_food_spent*$convertRate;
        $category->budget_transport_spent      = $category->budget_transport_spent*$convertRate;
        $category->budget_other_spent          = $category->budget_other_spent*$convertRate;
        $total[$i]['planed']                   = $total[$i]['planed']*$convertRate;
        $total[$i]['spent']                    = $total[$i]['spent']*$convertRate;
      }
      $i++;
    }
    $dbQuery->disconnect();

  
    // Si le voyage a commencé, effectue les calculs statistiques
    if ($lastDay-$firstDay>0) {

      // Détermine la période d'analyse
      $per = $firstDay;
      $periode[] = $per;
      while ($per < $lastDay) {
        $per = time(0, 0, 0, date("m", $per), date("d", $per)+7,  date("Y", $per));
        $periode[] = $per;
        $periodeLabel[] = fdate($per);
      }

      // Détermine les 2 tableaux à comparer
      if (isset ($calc1[$selectGraph])) $tab1 = $calc1[$selectGraph];
      if (isset ($calc2[$selectGraph])) $tab2 = $calc2[$selectGraph];
      $val1 = array();
      $val2 = array();

      // Détermine les valeurs par période
      $per = $firstDay;
      $sum1=0;$sum2=0;
      $indexPer=0;$indexVal=0;
      while ($per < $lastDay) {
        if ($per >= $periode[$indexPer]) {
          $val1[$indexPer]=$sum1;$sum1=0;
          $val2[$indexPer]=$sum2;$sum2=0;
          $indexPer++;
        }
        $duree = round(($endDate[$indexVal] - $startVal[$indexVal]) / (3600 * 24));
        if ($duree > 0) {
          $sum1 = $sum1+$tab1[$startVal[$indexVal]]/$duree;
          $sum2 = $sum2+$tab2[$startVal[$indexVal]]/$duree;
        }
        $per = time(0, 0, 0, date("m", $per), date("d", $per)+1,  date("Y", $per));
        if ($endDate[$indexVal] <= $per) {
          if ($indexVal+1<count ($endDate)) {
            $indexVal++;
          } else {
            $sum1 = 0;
            $sum2 = 0;
          }
        }
      }

      // Détermine la valeur maximale
      if (max($val1) > max($val2)) $max = max($val1); else $max = max($val2);
      foreach ($periode as $itemPer) {
        if ($itemPer < time(0, 0, 0, date("m")  , date("d"), date("Y"))) $setToday[] = 0;
        else $setToday[] = $max;
      }


      // Evaluation du budget total
      if ($spareGlobal)
        $grandTotal = $spareGlobal['planed']-$spareGlobal['spent']; else $grandTotal = 0;

      if ($grandTotal > 0) {
        $stateofBudget = $msgHandler->get('below');
        $sparedRecover = $msgHandler->get('spared');
      } else {
        $stateofBudget = $msgHandler->get('above');
        $sparedRecover = $msgHandler->get('recover');
      }


      // Envoie les variables en session pour qu'elles soient récupérée par le graphique
      $_SESSION['val1'] = $val1;
      $_SESSION['val2'] = $val2;
      $_SESSION['setToday'] = $setToday;
      $_SESSION['periodeLabel'] = $periodeLabel;
    }
  } elseif ($firstDay=='' || $lastDay=='') {
    header("Location:".PAGE_ACCUEIL."?error=*noDatesConfig");
    exit();
  } else {
    header("Location:".PAGE_ACCUEIL."?error=*noCategoryFound");
    exit();
  }


  // Assignation de Smarty
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuSpending'), $admin, 1));
  $smarty->assign ('categories', $categories);
  $smarty->assign ('countriescurrency', $countriescurrency);
  $smarty->assign ('maincurrency', $maincurrency);
  $smarty->assign ('total', $total);
  $smarty->assign ('globalTotal', $globalTotal);
  $smarty->assign ('grandTotal', $grandTotal);
  $smarty->assign ('stateofBudget', $stateofBudget);
  $smarty->assign ('sparedRecover', $sparedRecover);
  $smarty->assign ('endDate', $endDate);
  $smarty->assign ('selectTab', $msgHandler->getTable ('stats'));
  $smarty->assign ('selectGraph', $selectGraph);
  $smarty->assign ('tabCurrency', array ($msgHandler->get ('localCurrency'), $msgHandler->get ('defaultCurrency').' ('.$maincurrency.')'));
  $smarty->assign ('selectedCurrency', $selectedCurrency);
  $smarty->assign ('titleGraph', $msgHandler->get($selectGraph));
  $smarty->assign ('admin', $admin);
  $smarty->display($pageName.'.tpl');

?>

