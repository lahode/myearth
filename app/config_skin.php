<?php
  include_once('../lib/CSSCreator.php');
  include_once('../lib/init.php');


  // Function qui recharge les CSS d'une texture
  function loadSkinCSS ($tempFileName) {
    $content = '';
    if (file_exists($tempFileName) && filesize($tempFileName)) {
      $handle = fopen($tempFileName, "r");
      $content = fread($handle, filesize($tempFileName));
      fclose($handle);
    }
    return $content;
  }//loadSkinCSS


  // Si l'administrateur de l'album ne s'est pas authentifié, renvoie l'utilisateur à la page d'accueil
  if (!$admin) {header("Location:".PAGE_ACCUEIL);exit();}


  // Si le bouton "Annuler" a été pressé, renvoie à la page d'accueil
  if (isset ($_POST['cancelSkin']) && $_POST['cancelSkin'] == 'true') {
    header("Location:".PAGE_REDIRECTION."?redirection=config&getFromSession=1");
    exit();
  }


  // Initialisation des données
  $pageName = "config_skin";
  $pathCSS = str_replace (USERS_PATH.$albumName, '..', $backgroundImgPath);
  $pathName = '';
  $selectedSkin = '';
  $menuToChange = '';
  $formToChange = '';
  $generalToChange = '';
  $tempFileGeneral = '../lib/skins/space/general.css';
  $tempFileMenu = '../lib/skins/space/menu.css';
  $tempFileForm = '../lib/skins/space/form.css';


  // Initialisation des données renvoyées par le formulaire
  if (isset ($_POST['init_'.$pageName])) {
    if (isset ($_POST['generalCSS'])) $generalToChange = $_POST['generalCSS'];
    if (isset ($_POST['menuCSS'])) $menuToChange = $_POST['menuCSS'];
    if (isset ($_POST['formCSS'])) $formToChange = $_POST['formCSS'];
    if (isset ($_POST['skin']) && $_POST['skin']<>'') {
      $pathName = $skinsFile.$skinsManager->getSkin ($_POST['skin']).'/';
      $selectedSkin = $_POST['skin'];
    }
    if (isset ($_POST['changeSkin']) && $_POST['changeSkin']=='true') {
      if ($pathName<>'') $tempFileMenu = $pathName.'menu.css';
      $menuToChange = loadSkinCSS ($tempFileMenu);
      if ($pathName<>'') $tempFileGeneral = $pathName.'general.css';
      $generalToChange = loadSkinCSS ($tempFileGeneral);
      if ($pathName<>'') $tempFileForm = $pathName.'form.css';
      $formToChange = loadSkinCSS ($tempFileForm);
    }
  } else {
    $menuToChange = loadSkinCSS ($tempFileMenu);
    $generalToChange = loadSkinCSS ($tempFileGeneral);
    $formToChange = loadSkinCSS ($tempFileForm);
  }


  // Sauve la texture
  if (isset ($_POST['saveSkin']) && $_POST['saveSkin']=='true') {
    if (!preg_match('/^[a-zA-Z0-9]*$/', $_POST['name'])) $error = $msgHandler->getError ('nameAlpha');
    if ($_POST['name']=='') $error = $msgHandler->getError ('insertName');
    if (!$error) {
      if (!$pathName) {
        $pathName = $skinsManager->addSkin ($_POST['name']);
      }
      if ($pathName) {
        if (isset ($_POST['saveCurrent'])) {
          $contentXML = "<?xml version=\"1.0\"?>\r\n<album name=\"".$albumName."\" />";
          $tempFileName = $pathName.'config.xml';
          $handle = fopen($tempFileName, "w");
          $contents = fwrite($handle, $contentXML);
          fclose($handle);
          @chmod ($tempFileName, 0777);
          $skinConfig = new AlbumManager ($pathName.'config', $albumName);
          if (isset ($_SESSION['properties']['properties'])) {
            $properties = $_SESSION['properties']['properties'];
            if (isset ($_SESSION['properties']['tempImg'])) {
              $tempImg = $_SESSION['properties']['tempImg'];
              if (isset ($tempImg['bg']) && $tempImg['bg']<>'') {
                $tempImgMsg = $fileManager->saveTempFile ($backgroundImgPath, $tempImg['bg'], false);
                if ($tempImgMsg) $properties->bgImg = $tempImgMsg->name;
              }
            }
            $skinConfig->editProperties ($properties);
          }
          if (isset ($_SESSION['properties']['menuOptions'])) $skinConfig->editMenuOptions ($_SESSION['properties']['menuOptions']);
          if (isset ($_SESSION['fontstyles']['obj'])) {
            foreach ($_SESSION['fontstyles']['obj'] as $keyStyle => $itemStyle) {
              $skinConfig->editFontstyle ($itemStyle);
            }
          }
        }
        if ($_FILES['imgHeader']['name'] <> '') $mediaManager->saveImage ($_FILES['imgHeader'], $pathName.'header.jpg', '', TAILLE_IMG, 808, 0, 80, '###');
        if ($_FILES['imgFooter']['name'] <> '') $mediaManager->saveImage ($_FILES['imgFooter'], $pathName.'footer.jpg', '', TAILLE_IMG, 808, 0, 80, '###');
        $tempFileName = $pathName.'general.css';
        $handle = fopen($tempFileName, "w");
        $contents = fwrite($handle, $_POST['generalCSS']);
        fclose($handle);
        @chmod ($tempFileName, 0777);
        $tempFileName = $pathName.'form.css';
        $handle = fopen($tempFileName, "w");
        $contents = fwrite($handle, $_POST['formCSS']);
        fclose($handle);
        @chmod ($tempFileName, 0777);
        $tempFileName = $pathName.'menu.css';
        $handle = fopen($tempFileName, "w");
        $contents = fwrite($handle, $_POST['menuCSS']);
        fclose($handle);
        @chmod ($tempFileName, 0777);
        $tempFileName = $pathName.'header.css';
        $handle = fopen($tempFileName, "w");
        $contents = fwrite($handle, generateHeaderCSS());
        fclose($handle);
        @chmod ($tempFileName, 0777);
        header("Location:".PAGE_REDIRECTION."?redirection=config&getFromSession=1");
      } else {
        $error = $msgHandler->getError ('newDirFailed');
      }
    }
  }

  // Supprime la texture sélectionnées
  if (isset ($_POST['deleteSkin']) && ($_POST['deleteSkin']<>'')) {
    $skinsManager->deleteSkin ($_POST['deleteSkin']);
    $pathName = '';
    $selectedSkin = '';
    $menuToChange = loadSkinCSS ($tempFileMenu);
    $generalToChange = loadSkinCSS ($tempFileGeneral);
    $formToChange = loadSkinCSS ($tempFileForm);
  }


  // Récupère toutes les textures personnelles
  $skins = $skinsManager->getSkins ();


  // Assignation de Smarty
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get('menuConfiguration'), true, 1));
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->assign ('error', $error);
  $smarty->assign ('pageName', $pageName);
  $smarty->assign ('currentStyle', $property->skin);
  $smarty->assign ('tableSkins', $skins);
  $smarty->assign ('formToChange', $formToChange);
  $smarty->assign ('menuToChange', $menuToChange);
  $smarty->assign ('generalToChange', $generalToChange);
  $smarty->assign ('pathName', $pathName);
  $smarty->assign ('selectedSkin', $selectedSkin);
  $smarty->display($pageName.'.tpl');

?>