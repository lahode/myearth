<?php
  include_once('../lib/functions.php');
  include_once('../lib/declare.php');
  include_once "../lib/artichow/LinePlot.class.php";


  // Initialisation des données
  initSession(SESSION_NAME);
  if (isset ($_GET['mainLegend'])) $mainLegend = $_GET['mainLegend']; else $mainLegend = '';
  if (isset ($_GET['legend1'])) $legend1 = $_GET['legend1']; else $legend1 = '';
  if (isset ($_GET['legend2'])) $legend2 = $_GET['legend2']; else $legend2 = '';


  // Initialisation du graphique
  $graph = new Graph(550, 280);
  $graph->setAntiAliasing(TRUE);
  $graph->title->set($mainLegend);

  $group = new PlotGroup;

  $group->setBackgroundColor(new Color(197, 180, 210, 80));
  $group->setPadding(40, NULL, 50, NULL);

  $group->axis->left->setLabelNumber(8);
  $group->axis->left->setLabelPrecision(1);
  $group->axis->left->setTickStyle(Tick::OUT);

  $group->axis->bottom->setTickStyle(Tick::OUT);
  $group->axis->bottom->setLabelText($_SESSION['periodeLabel']);
  $group->axis->bottom->setLabelInterval(round(count ($_SESSION['periodeLabel'])/10));
  $group->axis->bottom->label->setAngle(25);


  // Gestion des deux valeurs envoyées
  $hideLine = false;
  for($n = 0; $n < 3; $n++) {
    $x = array();
    if ($n==0) {
      $x = $_SESSION['val1'];
      $lineColor = new Color(255, 0, 0, 10);
      $bkColor = new Color(255, 0, 0, 80);
      $legend = $legend1;
    } elseif ($n==1) {
      $x = $_SESSION['val2'];
      $lineColor = new Color (0, 255, 0, 10);
      $bkColor = new Color(0, 255, 0, 80);
      $legend = $legend2;
    } else {
      $x = $_SESSION['setToday'];
      $hideLine = true;
      $lineColor = new Color (0, 0, 0, 80);
      $bkColor = new Color(0, 0, 0, 80);
    }

    $plot = new LinePlot($x);
    $plot->hideLine($hideLine);
    $plot->yAxis->setLabelPrecision(1);
    $plot->setColor($lineColor);
    $plot->setFillColor($bkColor);

    $group->add($plot);
    if (!$hideLine) $group->legend->add($plot, $legend, Legend::LINE);
  }


  // Affichage du graphique
  $group->legend->setSpace(12);
  $group->legend->setBackgroundColor(new Color(255, 255, 255));
  $group->setPadding(NULL, 120, NULL, 40);

  $graph->add($group);
  $graph->draw();


  // Suppression des variables de sessions
  unset ($_SESSION['val1']);
  unset ($_SESSION['val2']);
  unset ($_SESSION['setToday']);
  unset ($_SESSION['periodeLabel']);
?>

