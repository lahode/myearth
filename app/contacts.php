<?php
  include_once('../lib/init.php');


  // Si l'administrateur de l'album ne s'est pas authentifié, renvoie l'utilisateur à la page d'accueil
  if (!$admin) {header("Location:".PAGE_ACCUEIL);exit();}


  // Initialisation des données
  $pageName = "contacts";
  $abcList = array ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
  $exportCSV = true;
  if (!isset($_POST['init_'.$pageName]) && isset ($_SESSION['contact'])) unset ($_SESSION['contact']);
  if (isset ($_POST['contactshidden'])) {
    $hiddenContacts = array_flip($_POST['contactshidden']);
    if (isset ($_POST['contacts'])) {
      $postedContacts = array_flip($_POST['contacts']);
    } else {
      $postedContacts = null;
    }
    if (count ($hiddenContacts > 0)) {
      foreach ($hiddenContacts as $kContact => $iContact) {
        if (isset ($postedContacts[$kContact])) {
          if (isset ($_SESSION['contact']) && isset ($_SESSION['contact']['contacts']) && is_array ($_SESSION['contact']['contacts']))
          $_SESSION['contact']['contacts']=$_SESSION['contact']['contacts'] + $postedContacts;
          else $_SESSION['contact']['contacts']=$postedContacts;
        } else {
          if (isset ($_SESSION['contact']) && isset ($_SESSION['contact']['contacts']) && isset ($_SESSION['contact']['contacts'][$kContact]))
            unset ($_SESSION['contact']['contacts'][$kContact]);
        }
      }
    }
  } else {
    $postedContacts = null;
  }
  $contactSelected = null;


  // Si le bouton "Delete Contact" a été pressé, supprime le contact
  if (isset ($_POST['deleteContact']) && $_POST['deleteContact']<>'') {
    $contactsManager->deleteContact ($_POST['deleteContact']);
    $exportCSV = false;
  }


  // Si le bouton "Delete Selected Contacts" a été pressé, supprime les contacts sélectionnés
  if (isset ($_POST['deleteContacts']) && $_POST['deleteContacts']=='true') {
    if (isset ($_SESSION['contact']) && isset ($_SESSION['contact']['contacts']) && count($_SESSION['contact']['contacts']) > 0) {
      foreach ($_SESSION['contact']['contacts'] as $keyContact => $itemContact) {
        $contactsManager->deleteContact ($keyContact);
        unset ($_SESSION['contact']['contacts'][$keyContact]);
      }
    } else {
      $error = $msgHandler->getError ('noContactToDelete');
    }
    $exportCSV = false;
  }


  // Si le bouton "addNews" a été pressé, ajoute le contact à l'enregistrement de la newsletter
  if (isset ($_POST['addNews']) && $_POST['addNews']<>'') {
    $contact = $contactsManager->getContact($_POST['addNews']);
    if ($contact) {
      if ($contact->nickname=="") $contact->nickname = $contact->firstname.' '.$contact->lastname;
      if (!$registerNewsManager->getPersonByEmail(addslash($contact->email))) {
        do {
          $id = genID();
        } while ($registerNewsManager->getPerson($id));
        $registerNewsManager->insertPerson (new Person ($id, addslash($contact->nickname), addslash($contact->email), time()));
        header ("Location:".PAGE_REDIRECTION."?redirection=registernews&confirm=*registerNews");
        exit();
      } else {
        $error = $msgHandler->getError ('existantPerson');
      }
    }
    $exportCSV = false;
  }


  // Récupération de tous les contacts
  $contacts = $contactsManager->getContacts();
  if (count ($contacts) > 0) {
    foreach ($contacts as $contact) {
      if ($contact->nickname=="") $contact->nickname = $contact->firstname.' '.$contact->lastname;
    }
  }


  // Si le bouton "Select All" a été pressé, sélectionne tous les contacts
  if (isset ($_POST['selectAll']) && $_POST['selectAll']=='true') {
    if (isset ($_SESSION['contact']) && isset ($_SESSION['contact']['contacts']) && count($_SESSION['contact']['contacts']) > 0) {
      unset ($_SESSION['contact']['contacts']);
    } else {
      if (count ($contacts) > 0) {
        foreach ($contacts as $keyContact => $contact) {
          $_SESSION['contact']['contacts'][$contact->id] = 1;
          $contactSelected[$keyContact] = 1;
        }
      }
    }
    $exportCSV = false;
  }


  // Si le bouton "Print Contacts" a été pressé, désactive l'option d'export
  if (isset ($_POST['printContacts']) && $_POST['printContacts']=='true') {
    $exportCSV = false;
  }
  

  // Gestion de l'affichage des résultats
  $splitDisplay = new SplitDisplay (20, 5);
  if (isset($_GET['next'])) {
    $splitDisplay->start=$_GET['next'];
    if (isset($_GET['page'])) {
      $splitDisplay->first=$_GET['page'];
    } else {
      $splitDisplay->first=$_GET['next']*$splitDisplay->length;
    }
    $exportCSV = false;
  }


  // Gestion du tri
  $display = array();
  if (!isset ($_SESSION['contact']['sortBy']) || !isset ($_SESSION['contact']['sortOrder'])) {
    $_SESSION['contact']['sortBy'] = 'nickname';
    $_SESSION['contact']['sortOrder'] = 'asc';
    $exportCSV = false;
  }
  if ((isset ($_GET['contact_sort_by']) && isset ($_GET['contact_sort_order'])) || (isset ($_SESSION['contact']['sortBy']) && isset ($_SESSION['contact']['sortOrder']))) {
    if (isset ($_GET['contact_sort_by'])) $_SESSION['contact']['sortBy'] = $_GET['contact_sort_by'];
    if (isset ($_GET['contact_sort_order'])) $_SESSION['contact']['sortOrder'] = $_GET['contact_sort_order'];
    if ($_SESSION['contact']['sortOrder'] == 'asc') {
      $contacts = sortObjBy ($contacts, $_SESSION['contact']['sortBy'], SORT_ASC);
    } else {
      $contacts = sortObjBy ($contacts, $_SESSION['contact']['sortBy'], SORT_DESC);
    }
  }
  $display['sortBy'] = $_SESSION['contact']['sortBy'];
  $display['sortOrder'] = $_SESSION['contact']['sortOrder'];


  // Gestion du filtre
  $display['filter'] = "";
  if (isset ($_GET['filter']) || isset ($_SESSION['contact']['filter'])) {
    if (isset ($_GET['filter']) && $_GET['filter']<>'no') $_SESSION['contact']['filter'] = $_GET['filter'];
    if (isset ($_GET['filter']) && $_GET['filter']=='no') {
      if (isset ($_SESSION['contact']['filter'])) unset ($_SESSION['contact']['filter']);
    } else {
      foreach ($contacts as $keyContact => $contact) {
        if ($_SESSION['contact']['filter']=='num') {
          $erase = true;
          for ($i=0;$i<10;$i++) {
            if ($contact->nickname[0]==''.$i) $erase = false;
          }
          if ($erase) unset ($contacts[$keyContact]);
        }
        elseif (strtoupper($contact->nickname[0])<>$_SESSION['contact']['filter']) unset ($contacts[$keyContact]);
      }
      $display['filter'] = $_SESSION['contact']['filter'];
    }
    $exportCSV = false;
  }


  // Gestion de l'affichage des résultats
  $splitDisplay->total = count ($contacts);
  $result = array();
  if ($splitDisplay->total > 0) {
    $i=0;
    if ($splitDisplay->first >= $splitDisplay->total) $splitDisplay->first=$splitDisplay->first-$splitDisplay->length;
    if ($splitDisplay->first < 0) $splitDisplay->first = 0;
    foreach ($contacts as $key => $item) {
      if (isset ($_SESSION['contact']) && isset ($_SESSION['contact']['contacts']) && isset ($_SESSION['contact']['contacts'][$item->id])) $contactSelected[$i] = 1;
      if (($splitDisplay->first==-1 && $splitDisplay->length==-1) || ($i >= $splitDisplay->first &&  $i < $splitDisplay->first+$splitDisplay->length)) {
        $result[$i] = $item;
      }
      $i++;
    }
    $contacts = $result;
  }
  if ($splitDisplay->length > 0) {
    $splitDisplay->loop = ceil(($splitDisplay->total)/$splitDisplay->length);
    $splitDisplay->first = ceil(($splitDisplay->first)/$splitDisplay->length);
  }


  // Si le bouton "export Contacts" a été pressé, export les contacts sélectionnés en fichier CVS
  if (isset ($_POST['exportContacts']) && $_POST['exportContacts']=='true' && isset ($_GET['exportContacts']) && $_GET['exportContacts']=='true' && $exportCSV) {
    if (isset ($_SESSION['contact']) && isset ($_SESSION['contact']['contacts']) && count($_SESSION['contact']['contacts']) > 0) {
      $xls_output = "";
      foreach ($_SESSION['contact']['contacts'] as $keyContact => $itemContact) {
        $contact = $contactsManager->getContact($keyContact);
        if ($contact) {
          $xls_output .= $contact->title.";".$contact->firstname.";".$contact->lastname.";".$contact->nickname.";".$contact->email.";".$contact->birthdate.";".$contact->address.";".$contact->zipcode.";".$contact->city.";".$contact->country.";".$contact->tel.";".$contact->mobile.";".$contact->fax.";".$contact->messenger.";".$contact->comments.";"."\n";
        }
      }

      header('Content-Type: application/x-msexcel');
      header('Content-Disposition: attachment; filename="address_' . date("Ymd") . '.csv"');
      header('Expires: 0');
      header('Pragma: no-cache');

      print $xls_output;
      exit;
    }
  }


  // Assignation de Smarty
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuContacts'), $admin, 4));
  $smarty->assign ('abcList', $abcList);
  $smarty->assign ('display', $display);
  $smarty->assign ('contactSelected', $contactSelected);
  $smarty->assign ('contacts', $contacts);
  $smarty->assign ('splitDisplay', $splitDisplay);
  $smarty->assign ('pageName', $pageName);
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->display($pageName.'.tpl');

?>

