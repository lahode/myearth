<?php
  include_once('../lib/init.php');
  include_once('../lib/Availability.php');


  // Initialisation des données
  $pageName = "insert_cat";
  $selectedCurrency = 0;
  $convertRate = 1;
  $access = "";
  $country = "";
  $startingDate = "";
  $country_name = "";
  $cityArr = "";
  $cityDep = "";
  $budget_accommodation_planed = "";
  $budget_food_planed = "";
  $budget_transport_planed = "";
  $budget_other_planed = "";
  $budget_accommodation_spent = "";
  $budget_food_spent = "";
  $budget_transport_spent = "";
  $budget_other_spent = "";
  $rate = "";
  $modifyCat = "";
  $sound = null;
  $map = null;
  $summary = null;
  $categor = null;
  $availability = null;
  $tableCountry = array();
  $tableCity = array();
  $showmap = $property->showMap;
  $allowsametime = 0;
  $mapCat = new Media ('map', $msgHandler, $fileManager, $albumManager, $quota_path, $mapImgPath, true);
  $soundCat = new Media ('sound', $msgHandler, $fileManager, $albumManager, $quota_path, $soundPath, true);
  $availability_obj = new Availability ($routeManager);
  $uploadedSound = false;
  $uploadedMap = false;


  // Si l'administrateur de l'album ne s'est pas authentifié, renvoie l'utilisateur à la page d'accueil
  if (!$admin || !isset ($_GET['idCat']) || $_GET['idCat'] == '') {header("Location:".PAGE_DESTINATION);exit();}


  // Si le bouton "Annuler" a été pressé, renvoie à la page d'accueil
  if (isset ($_POST['cancel']) && $_POST['cancel'] == 'true') {header("Location:".PAGE_DESTINATION);exit();}


  // Initialisation des données renvoyées par le formulaire
  if (!isset($_POST['init_'.$pageName])) {
    $fileManager->init();
    if ($categories) {
      $cat = $routeManager->getCategoryBy ('id', $_GET['idCat']);
      if ($cat) {
        $id = $cat->id;
        $access = $cat->access;
        $showmap = $cat->showmap;
        $allowsametime = $cat->allowsametime;
        $country = $cat->country;
        $cityArr = $cat->arrivalCity;
        $cityDep = $cat->departureCity;
        $startingDate = $cat->startingDate;
        $budget_accommodation_planed = $cat->budget_accommodation_planed;
        $budget_food_planed = $cat->budget_food_planed;
        $budget_transport_planed = $cat->budget_transport_planed;
        $budget_other_planed = $cat->budget_other_planed;
        $budget_accommodation_spent = $cat->budget_accommodation_spent;
        $budget_food_spent = $cat->budget_food_spent;
        $budget_transport_spent = $cat->budget_transport_spent;
        $budget_other_spent = $cat->budget_other_spent;
        $summary = $cat->summary;
        if ($cat->rate == 0) {
          $rate = 1;
          foreach ($categories as $catItem) {
            if ($catItem->country == $country && $catItem->rate<>0) $rate = $catItem->rate;
          }
        } else {
          $rate = $cat->rate;
        }
        if ($cat->sound<>'') $sound = $fileManager->addFile ('sound', $soundPath, $cat->sound);
        if ($cat->map<>'') $map = $fileManager->addImage ('map', $mapImgPath, $cat->map, '');
        $modifyCat = $country;
      } else {
        $id = $_GET['idCat'];
      }
    } else {
      $id = $_GET['idCat'];
    }
  } else {
    $id = $_POST['id'];
    $access = 'public';
    if (isset ($_POST['showmap'])) $showmap = 1; else $showmap = 0;
    if (isset ($_POST['allowsametime'])) $allowsametime = 1; else $allowsametime = 0;
    $country = $_POST['country'];
    $modifyCat = $_POST['modifyCat'];
    if (isset ($_POST['cityArr'])) $cityArr = $_POST['cityArr'];
    if (isset ($_POST['cityDep'])) $cityDep = $_POST['cityDep'];
    $startingDate = unfdate($_POST['startingDate']);
    $rate = addslash($_POST['rate']);
    if (!is_numeric ($rate) || $rate == '0') $rate = 1;
    $selectedCurrency = $_POST['currency'];
    if ($_POST['lastCurrency'] == 0 && $selectedCurrency == 1) $convertRate = 1 / $rate;
    elseif ($_POST['lastCurrency'] == 1 && $selectedCurrency == 0) $convertRate = $rate;
    $budget_accommodation_planed = addslash($_POST['budget_accommodation_planed']) * $convertRate;
    $budget_food_planed = addslash($_POST['budget_food_planed']) * $convertRate;
    $budget_transport_planed = addslash($_POST['budget_transport_planed']) * $convertRate;
    $budget_other_planed = addslash($_POST['budget_other_planed']) * $convertRate;
    $budget_accommodation_spent = addslash($_POST['budget_accommodation_spent']) * $convertRate;
    $budget_food_spent = addslash($_POST['budget_food_spent']) * $convertRate;
    $budget_transport_spent = addslash($_POST['budget_transport_spent']) * $convertRate;
    $budget_other_spent = addslash($_POST['budget_other_spent']) * $convertRate;
    $summary = new Summary (addslash($_POST['general']), addslash($_POST['population']), addslash($_POST['politic']), addslash($_POST['history']),
                            addslash($_POST['nature']), addslash($_POST['infra']), addslash($_POST['food']), addslash($_POST['activity']),
                            addslash($_POST['budget']), addslash($_POST['accommodation']), addslash($_POST['transport']), addslash($_POST['communication']),
                            addslash($_POST['other']));
    $result_map = $mapCat->addMap('', true);
    if ($result_map[0]) $map = $result_map[0];
    if ($result_map[1]<>'') $error = $result_map[1];
    $result_sound = $soundCat->addSound(true);
    if ($result_sound[0]) $sound = $result_sound[0];
    if ($result_sound[1]<>'') $error = $result_sound[1];
  }


  // Si le bouton "Save" a été pressé, sauvegarde les informations de la destination éditée
  if (isset ($_POST['save']) && $_POST['save']=='true') {
    if ($id < 1) $error = $msgHandler->getError ('invalidID');
    if ($country=='0') $error = $msgHandler->getError ('selectCountry');
    if ($cityArr=='0' || $cityArr=='') $error = $msgHandler->getError ('selectArrivalCity');
    if ($cityDep=='0' || $cityDep=='') $error = $msgHandler->getError ('selectDepartureCity');
    if (!is_numeric ($budget_accommodation_planed) && $budget_accommodation_planed <> '') $error = $msgHandler->getError ('budget_accommodation_planed');
    if (!is_numeric ($budget_food_planed) && $budget_food_planed <> '') $error = $msgHandler->getError ('budget_food_planed');
    if (!is_numeric ($budget_transport_planed) && $budget_transport_planed <> '') $error = $msgHandler->getError ('budget_transport_planed');
    if (!is_numeric ($budget_other_planed) && $budget_other_planed <> '') $error = $msgHandler->getError ('budget_other_planed');
    if (!is_numeric ($budget_accommodation_spent) && $budget_accommodation_spent <> '') $error = $msgHandler->getError ('budget_accommodation_spent');
    if (!is_numeric ($budget_food_spent) && $budget_food_spent <> '') $error = $msgHandler->getError ('budget_food_spent');
    if (!is_numeric ($budget_transport_spent) && $budget_transport_spent <> '') $error = $msgHandler->getError ('budget_transport_spent');
    if (!is_numeric ($budget_other_spent) && $budget_other_spent <> '') $error = $msgHandler->getError ('budget_other_spent');
    if (!is_numeric ($rate) || $rate == '0') $error = $msgHandler->getError ('rate');
    if (!$error) {
      if ($selectedCurrency == 1) $tempConvert = $rate; else $tempConvert = 1;
      $sound = $fileManager->saveTempFiles ('sound', $soundPath);
      $map = $fileManager->saveTempFiles ('map', $mapImgPath);
      if ($map && isset (current($map)->name)) $mapName = current($map)->name; else $mapName = "";
      if ($sound && isset (current($sound)->name)) $soundName = current($sound)->name; else $soundName = "";
      $aCategory =  new Category (
         $id, $access, $country, $cityArr, $cityDep, $startingDate, round($budget_accommodation_planed*$tempConvert, 2),
         round($budget_food_planed*$tempConvert, 2), round($budget_transport_planed*$tempConvert, 2), round($budget_other_planed*$tempConvert, 2),
         round($budget_accommodation_spent*$tempConvert, 2), round($budget_food_spent*$tempConvert, 2), round($budget_transport_spent*$tempConvert, 2),
         round($budget_other_spent*$tempConvert, 2), $rate, $summary, $soundName, $mapName, $showmap, $allowsametime);
      $routeManager->editCategory ($aCategory);
      if (isset ($_SESSION['countriesList'])) unset ($_SESSION['countriesList']);
      header("Location:".PAGE_DESTINATION);
      exit();
    }
  }


  // Si le bouton "Delete Map" a été pressé, efface la carte
  if (isset ($_POST['deleteMap']) && $_POST['deleteMap']<>'') {
    $map = $fileManager->deleteTempFile ('map', $_POST['deleteMap']);
  }


  // Si le bouton "Delete Spund" a été pressé, efface le son
  if (isset ($_POST['deleteSound']) && $_POST['deleteSound']<>'') {
    $sound = $fileManager->deleteTempFile ('sound', $_POST['deleteSound']);
  }


// ************** Initialisation des tableaux ******************************************

  $dbQuery->connect();

  // Sélection du pays
  $country_name = $dbQuery->getCountryName ($country);

  // Sélection des tableaux pour la categorie
  if ($modifyCat=='') $tableCountry = $dbQuery->getAllCountries ();
  $tableCity = $dbQuery->getMainCitiesFromCountry ($country);

  // Récupère la monnaie par défaut et celle du pays en cours
  $departure = $routeManager->getDeparture ();
  if ($departure) $maincurrency = $dbQuery->getCountryCurrency ($departure->country); else $maincurrency = '';
  $countrycurrency = $dbQuery->getCountryCurrency ($country);

  $dbQuery->Disconnect();

// **************************************************************************************

  // Insère en session toutes les dates occupées pour le calendrier
  if (!isset($_POST['init_'.$pageName])) {
    $availability = $availability_obj->getCategoryAvailability($id);
    $_SESSION['datesBooked'] = $availability;
    if ($startingDate=='' && $availability['firstDate']<$availability['lastDate']) $startingDate = substr($availability['firstDate'], 0, -3);
  }


  // Réassigne les valeurs transformée à l'objet pour affichage
  if ($startingDate) {
    $categor = new Category ($id, $access, $country, $cityArr, $cityDep, $startingDate, $budget_accommodation_planed, $budget_food_planed,
                             $budget_transport_planed, $budget_other_planed, $budget_accommodation_spent, $budget_food_spent,
                             $budget_transport_spent, $budget_other_spent, $rate, $summary, '', '', $showmap, $allowsametime);
  } else {
    header("Location:".PAGE_DESTINATION);exit();
  }


  // Récupère les petites images à afficher
  if ($sound && isset (current($sound)->name)) $soundName = current($sound)->name; else $soundName = "";
  if ($soundName=='') $displaySound = ''; elseif (file_exists ($soundPath.$soundName)) $displaySound = $soundName; else {$displaySound = '';$uploadedSound=true;}
  if ($map &&isset (current($map)->name)) $mapName = current($map)->name; else $mapName = "";
  if ($mapName=='') $displayMap = ''; elseif (file_exists ($mapImgPath.$mapName)) $displayMap = $mapName; else {$displayMap = '';$uploadedMap=true;}


  // Assignation de Smarty
  if ($sound) {
    $smarty->assign ('sound', current ($sound));
    $smarty->assign ('soundKey', key($sound));
  }
  if ($map) {
    $smarty->assign ('map', current ($map));
    $smarty->assign ('mapKey', key($map));
  }
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuInsert'), true, 2));
  $smarty->assign ('displaySound', $soundCat->output('sound', $displaySound, $uploadedSound));
  $smarty->assign ('displayMap', $mapCat->output('map', $displayMap, $uploadedMap));
  $smarty->assign ('cat', $categor);
  $smarty->assign ('country_name', $country_name);
  $smarty->assign ('country', $country);

  $smarty->assign ('modifyCat', $modifyCat);
  $smarty->assign ('countrycurrency', $countrycurrency);
  $smarty->assign ('maincurrency', $maincurrency);
  $smarty->assign ('tabCurrency', array ($msgHandler->get ('localCurrency').' ('.$countrycurrency.')', $msgHandler->get ('defaultCurrency').' ('.$maincurrency.')'));
  $smarty->assign ('selectedCurrency', $selectedCurrency);

  $smarty->assign ('tableCountry', $tableCountry);
  $smarty->assign ('tableCity', $tableCity);
  $smarty->assign ('pageName', $pageName);
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->assign ('dateFormat', $dateFormat);

  $smarty->display($pageName.'.tpl');

?>

