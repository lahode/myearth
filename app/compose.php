<?php
  include_once('../lib/init.php');


  // Si l'administrateur de l'album ne s'est pas authentifié, renvoie l'utilisateur à la page d'accueil
  if (!$admin) {header ("Location:".PAGE_ACCUEIL);exit();}


  // Si le bouton "Annuler" a été pressé, supprime la session et les pièces jointes et renvoie à la page des contacts
  if (isset ($_POST['back']) && $_POST['back'] == 'true') {
    $fileManager->kill();
    if (isset ($_POST['backTo'])) {header("Location:".PAGE_MAILBOX."?box=".$_POST['backTo']);exit();}
    header("Location:".PAGE_CONTACTS);
    exit();
  }


  // Initialisation des données
  $pageName = "compose";
  if (isset ($_SESSION['mailbox']) && $_SESSION['mailbox'] <> 'default') {
    $mailbox = $emailsManager->getMailbox ($_SESSION['mailbox']);
    $emails = array ($mailbox->email);
  } else {
    $emails = $albumManager->getEmails();
  }
  $subjectDisplay = '';
  $messageDisplay = '';
  $to = '';
  $cc = '';
  $bcc = '';
  $box = 'contact';
  if (!isset($_POST['init_'.$pageName])) $fileManager->init();
  if (isset ($_GET['to']) && !isset($_POST['init_'.$pageName])) $to .= $_GET['to'];
  if (isset ($_POST['to'])) $to .= $_POST['to'];
  if (isset ($_POST['cc'])) $cc .= $_POST['cc'];
  if (isset ($_POST['bcc'])) $bcc .= $_POST['bcc'];
  if (isset ($_POST['subject'])) $subjectDisplay .= $_POST['subject'];
  if (isset ($_POST['message'])) $messageDisplay .= $_POST['message'];
  if (isset ($_GET['box'])) $box = $_GET['box'];


  // Récupération des information d'un utilisateur si une demande de mise à jour a été lancée
  if (isset ($_GET['request']) && !isset($_POST['init_'.$pageName])) {
    $contact = $contactsManager->getContact($_GET['request']);
    if ($contact) {
      $to = $contact->email;
      $monthList = $msgHandler->getTable ('month');
      $dayList = array ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31');
      if ($contact->lastname<>'') $nicknameContact = ' ('.$contact->nickname.')'; else $nicknameContact = '';
      if ($contact->country<>'') $countryContact = '<br />'.$contact->country; else $countryContact = '';
      if ($contact->tel<>'') $telContact = '<br />'.$msgHandler->get('tel').$contact->tel; else $telContact = '';
      if ($contact->fax<>'') $faxContact = '<br />'.$msgHandler->get('fax').$contact->fax; else $faxContact = '';
      if ($contact->mobile<>'') $mobileContact = '<br />'.$msgHandler->get('mobile').$contact->mobile; else $mobileContact = '';
      if ($contact->messenger<>'') $messengerContact = '<br />'.$msgHandler->get('messenger').$contact->messenger; else $messengerContact = '';
      if ($contact->comments<>'') $commentsContact = '<br />'.$msgHandler->get('comments').$contact->comments; else $commentsContact = '';
      $birthdate = explode ('-', $contact->birthdate);
      $birthday = '01';
      $birthmonth = '01';
      $birthyear = '1900';
      if (count ($birthdate)==3) {
        $birthday = $birthdate[0]-1;
        $birthmonth = $birthdate[1];
        $birthyear = $birthdate[2];
      }
      $contactInfo = $contact->title.' '.$contact->firstname.' '.$contact->lastname.$nicknameContact.'<br />'.$contact->address.'<br />'.$contact->zipcode.' '.$contact->city.$countryContact.
                     '<br />'.$contact->email.'<br />'.$msgHandler->get('born').$dayList[$birthday].' '.$monthList[$birthmonth].' '.$birthyear.'<br />'.$telContact.$faxContact.$mobileContact.$messengerContact.$commentsContact.'<br /><br />';
      $messageDisplay = $contactInfo.$msgHandler->get('updateClientInfo').'<a href="http://'.WEB_PAGE.'/app/external.php?updateuser='.$_GET['request'].'&album='.$albumName.'">'.$msgHandler->get('clickhere').'</a>';
      $subjectDisplay = $msgHandler->get('updateClientSubject');
    }
  }


  // Gère les reply ou les forward d'e-mail
  if (isset ($_GET['actions']) && isset ($_GET['id']) && $_GET['id']<>'' && !isset($_POST['init_'.$pageName]) && $box<>'contact') {
    $msg = $emailsManager->getEmail($_GET['id'], $box);
    if ($box=='out') $attachmentPath = $attachedOutboxPath; else $attachmentPath = $attachedInboxPath;
    if ($msg) {
      switch ($_GET['actions']) {
        case 'reply' : {$to = $msg->from;$subjectDisplay = 'Re: '.$msg->subject;$messageDisplay = $msg->body;break;}
        case 'fwd' :   {$subjectDisplay = 'Fw: '.$msg->subject;$messageDisplay = $msg->body;
                        $fileManager->copyAttachmentToTemp ('attachments', $attachmentPath, $msg->attachments);break;}
      }
    }
  }


  // Si le bouton "Send" a été pressé, envoie le message aux personnes concernées
  if (isset ($_POST['send']) && $_POST['send'] == 'true') {
    $to_array = array();
    $cc_array = array();
    $bcc_array = array();
    $headerMail = "";
    if ($subjectDisplay=='') $error = $msgHandler->getError ('insertSubject');
    if ($to=='' && $cc=='' && $bcc=='') $error = $msgHandler->getError ('insertContact');
    if ($emails) $email = $emails[$_POST['email']]; else $email = $_POST['email'];
    if ($to<>'') $to_array = preg_split("/[\s,;]+/", $to);
    if ($cc<>'') $cc_array = preg_split("/[\s,;]+/", $cc);
    if ($bcc<>'') $bcc_array = preg_split("/[\s,;]+/", $bcc);
    $usersMail = array_merge ($to_array, $cc_array, $bcc_array);
    if (count ($usersMail) > 0) {
      foreach ($usersMail as $userCheck) {
        if (!is_email ($userCheck)) $error = $msgHandler->getError ('invalidEmails');
      }
    }
    $messageMail = new MessageObj ('', 0, 'public', stripslashes($subjectDisplay), nl2br($messageDisplay), null);
    if (!$error) {
      $attachmentsToSend = $fileManager->convertTempAttachmentToMail('attachments');
      $mailSender->addTo($to_array);
      $mailSender->addCc($cc_array);
      $mailSender->addBcc($bcc_array);
      $mailSender->model->addFROM ($email);
      $mailSender->model->addHTML ($messageMail->text);
      $mailSender->model->addSubject ($messageMail->subject);
      $mailSender->addAttachments ($fileManager->convertTempAttachmentToMail('attachments'), false);
      if (!$mailSender->sender->send()) $error = $msgHandler->getError ('emailSent');
      if (!$error) {
        if (isset ($_POST['saveMail'])) {
          do {
            $id = genID();
          } while ($emailsManager->getEmail($id, 'out'));
          $attachments = $fileManager->saveTempFiles ('attachments', $attachedOutboxPath);
          $emailsManager->editEmail (new Mail ($id, time(), '', $to_array, $cc_array, $bcc_array, '', $messageMail->subject, html_entity_decode($messageMail->text, ENT_NOQUOTES, 'ISO-8859-1'), $attachments), 'out');
        }
        if (isset ($_POST['backTo'])) {header("Location:".PAGE_MAILBOX."?box=".$_POST['backTo']."&confirm=*sentMail");exit();}
        header("Location:".PAGE_CONTACTS."?confirm=*sentMail");
        exit();
      }
    }
  }


  // Si le bouton "addFile" a été pressé, attache un nouveau fichier au mail
  if (isset ($_POST['addFile']) && $_POST['addFile'] == 'true') {
    if (isset ($_FILES['attachedfile']) && isset ($_FILES['attachedfile']['tmp_name']) && $_FILES['attachedfile']['tmp_name']<>'') {
      $resultAdd = $fileManager->addTempAttachment ('attachments', $_FILES['attachedfile']);
      if (is_string ($resultAdd)) $error = $resultAdd; else $images = $resultAdd;
    } else {
      $error = $msgHandler->getError ('nofileSelected');
    }
  }


  // Si le bouton "deleteFiles" a été pressé, supprime des fichiers attachés au mail
  if (isset ($_POST['deleteFiles']) && $_POST['deleteFiles'] == 'true') {
    $attachmentsToDelete = $fileManager->getTempFiles('attachments');
    if (isset ($_POST['attachedFiles']) && count ($_POST['attachedFiles']) > 0 && count ($attachmentsToDelete) > 0) {
      foreach ($_POST['attachedFiles'] as $fileToDelete) {
        foreach ($attachmentsToDelete as $keyAttachmentToDelete => $attachmentToDelete) {
          if ($attachmentToDelete->name == $fileToDelete) $fileManager->deleteTempFile ('attachments', $keyAttachmentToDelete);
        }
      }
    }
  }


  // Assignation de Smarty
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuCompose'), $admin, 4));
  $smarty->assign ('to', $to);
  $smarty->assign ('cc', $cc);
  $smarty->assign ('bcc', $bcc);
  $smarty->assign ('emails', $emails);
  $smarty->assign ('back', $box);
  $smarty->assign ('pageName', $pageName);
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->assign ('attachedFiles', $fileManager->getTempFiles('attachments'));
  $smarty->assign ('messageDisplay', $messageDisplay);
  $smarty->assign ('subjectDisplay', $subjectDisplay);
  $smarty->assign ('freespace', getFreeSpace($albumManager->getQuota(), $quota_path)-TAILLE_IMG);
  $smarty->display($pageName.'.tpl');

?>

