<?php
  include_once ('../lib/initNoSession.php');


  // Efface l'utilisateur de la news à distance
  if (isset ($_GET['unregister']) && $_GET['unregister'] <> '') {
    if ($registerNewsManager && $registerNewsManager->getPerson($_GET['unregister'])) {
      $registerNewsManager->deletePerson($_GET['unregister']);
      header ("Location:http://".PAGE_LOGIN."?album=".$_GET['album']."&confirm=*deletePersonFromNews");
      exit();
    }
    header ("Location:http://".WEB_PAGE);
    exit();
  }


  // Met à jour les données d'un contact à distance
  elseif (isset ($_GET['updateuser']) && $_GET['updateuser']) {
    // Initialisation des données
    $dayList = array ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31');
    $contact = $contactsManager->getContact($_GET['updateuser']);

    // Si le bouton "Annuler" a été pressé, renvoie à la page d'accueil
    if (isset ($_POST['back']) && $_POST['back'] == 'true') {header("Location:".WEB_PAGE);exit();}

    // Si le bouton "Save" a été pressé, enregistre les information et renvoie à la page d'accueil
    if (isset ($_POST['save']) && $_POST['save']=='true') {
      if (($_POST['birthday'] > 29 && ($_POST['birthmonth']=='04' || $_POST['birthmonth']=='06' || $_POST['birthmonth']=='09' || $_POST['birthmonth']=='11')) ||
          ($_POST['birthday'] > 28 && $_POST['birthmonth']=='02') ||
          ($_POST['birthday'] > 27 && $_POST['birthmonth']=='02' && ($_POST['birthyear']%4<>0 || $_POST['birthyear']%100==0))) $error = $msgHandler->getError ('date');
      if (!is_email ($_POST['email'])) $error = $msgHandler->getError ('invalidEmail');
      if ($_POST['firstname']=='' && $_POST['lastname']=='' && $_POST['nickname']=='') $error = $msgHandler->getError ('insertName');
      if (!$error) {
        $contact = new Contact ($contact->id, $_POST['title'], $_POST['firstname'],
                                $_POST['lastname'], $_POST['nickname'], $_POST['email'],
                               ($_POST['birthday']+1).'-'.$_POST['birthmonth'].'-'.$_POST['birthyear'],
                                $_POST['address'], $_POST['zipcode'], $_POST['city'], $_POST['country'],
                                $_POST['tel'], $_POST['fax'], $_POST['mobile'], $_POST['messenger'], $_POST['comments'], time());
        $contactsManager->editContact ($contact);
        header ("Location:http://".PAGE_LOGIN."?album=".$_GET['album']."&confirm=*selfUpdated");
        exit();
      }
    }


    // Gestion des données
    if (!$contact) {header ("Location:http://".WEB_PAGE);exit();}
    $birthdate = explode ('-', $contact->birthdate);
    $birthday = '01';
    $birthmonth = '01';
    $birthyear = '1900';
    if (count ($birthdate)==3) {
      $birthday = $birthdate[0]-1;
      $birthmonth = $birthdate[1];
      $birthyear = $birthdate[2];
    }


    // Assignation de Smarty
    $smarty->assign ('showFooter', $header->showFooter ($error, '', '', true, false));
    $smarty->assign ('showTopHeader', $header->showTopHeader ());
    $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuContactDetails'), false, -1));
    $smarty->assign ('contact', $contact);
    $smarty->assign ('birthday', $birthday);
    $smarty->assign ('birthmonth', $birthmonth);
    $smarty->assign ('birthyear', $birthyear);
    $smarty->assign ('action', 'edit');
    $smarty->assign ('dayList', $dayList);
    $smarty->assign ('monthList', $msgHandler->getTable ('month'));
    $smarty->display('editcontact.tpl');
  }
  
  
  else {
    header ("Location:http://".PAGE_LOGIN."?album=".$albumName);
    exit();
  }


?>
