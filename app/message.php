<?php
  include_once('../lib/init.php');


  // Si le option de gestion des message n'est pas activée, renvoie à la page d'accueil
  if (!$menuOption->message) {header ("Location:".PAGE_ACCUEIL);exit();}


  // Récupère toutes les information de l'album
  $pageName = "message";
  $message = array();
  if (isset ($_SESSION['mails'])) {
    $message = unserialize($_SESSION['mails']);
  }

  // Assignation de Smarty
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuMessage'), $admin, 3));
  $smarty->assign ('message', $message);
  $smarty->assign ('albumTitle', $albumTitle);
  $smarty->assign ('admin', $admin);
  $smarty->display($pageName.'.tpl');

?>

