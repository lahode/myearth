<?php
  include_once('../lib/init.php');


  // Récupère toutes les images d'un lien FTP
  function getImagesByFTP ($ftpAddress, $userName, $password, $freeSpace) {
    global $mediaManager;
    global $msgHandler;
    global $ftpTempPath;
    global $webImgPath;
    global $originalImg;
    $error = "";
    $confirm = "";
    $warning = "";
    $conn_id = @ftp_connect($ftpAddress);
    $login_result = @ftp_login ($conn_id, $userName, $password);
    $nbFiles = 0;
    $i = 0;
    $j = 0;

    // Tentative de connexion au serveur FTP
    if (($conn_id) && ($login_result)) {
      ftp_pasv($conn_id, true);
      $contents = ftpList($conn_id, ".");
      foreach ($contents as $content) {
        $file = $content['name'];
        if (!is_file($webImgPath.$file) && $content['type'] == 'file' && $error=='') {
          $handle = fopen($ftpTempPath.$file, 'w');
          $fileSize = @ftp_size($conn_id, $file);
          if ($fileSize < 0 || $freeSpace-$fileSize > 0) {
            if ($ret = ftp_nb_fget($conn_id, $handle, $file, FTP_BINARY)) {
              while ($ret == FTP_MOREDATA) {
                $ret = ftp_nb_continue ($conn_id);
              }
              if ($ret == FTP_FINISHED) {
                if (getimagesize($ftpTempPath.$file)) {
                  $originalImg = '';
                  if ($origImgPath<>'') $originalImg = $origImgPath.$file;
                  $error=$mediaManager->saveImage (array('filename' => $ftpTempPath.$file), $webImgPath.$file, $originalImg, TAILLE_IMG, 0, 300);
                  if (!$error) {
                    @ftp_delete($conn_id, $file);
                  }
                  $nbFiles++;
                }
                deleteFile ($ftpTempPath.$file);
              } else {
                $error = $msgHandler->getError('uploadImpossible');
              }
            } else {
              $error = $msgHandler->getError('uploadImpossible');
            }
          } else {
            $error = $msgHandler->getError('quotaExceeded');
          }
        } elseif ($content['type'] == 'file' && $error=='') {
          $j++;
          $warning = "|".$j." ".$msgHandler->getError('pictureExist');
        }
      }
      ftp_close($conn_id);
    } else {
      $error = $msgHandler->getError('ftpError');;
    }
    if (!$error) {
      if ($nbFiles > 0) $confirm = $nbFiles.' '.$msgHandler->getConfirm('confirmPicsTransfered').$warning;
      else $confirm = $msgHandler->getConfirm('noNewPics').$warning;
    }
    return array ($confirm, $error);
  }//getImagesByFTP


  // Si l'administrateur de l'album ne s'est pas authentifié, renvoie l'utilisateur à la page d'accueil
  if (!$admin) {header("Location:".PAGE_ACCUEIL);exit();}


  // Initialisation des données
  $pageName = "filemanager";
  $ftpAddress = $albumManager->getFTP();
  $confirmFiles = "";
  $warning = "";
  $displayWaitingMessage = true;


  // Création du quota
  $maxSpace = $albumManager->getQuota();
  $quota_path = USERS_PATH.$albumName.'/';
  $occupedSpace = folderSpace($quota_path);
  if ($occupedSpace <= $maxSpace) {
    $freeSpace = $maxSpace - $occupedSpace;
    $ratioSpace = ($occupedSpace/$maxSpace)*100;
  } else {
    $freeSpace = 0;
    $ratioSpace = 100;
  }
  createQuotaImg ($ratioSpace, 120, 60, '#00FF00', '#FF0000', $quota_path.'quota.gif');


  // Si des fichiers ont été envoyés, sauvegarde ceux-ci dans le bon dossier
  if ($_FILES && isset ($_POST['save']) && $_POST['save']=='true') {
    foreach($_FILES as $file) {
      if ($file['name']<>'' && $error=='') {
        if (is_uploaded_file($file['tmp_name'])) {
          if ($freeSpace-$file['size'] > 0) {
            if (@getimagesize($file['tmp_name'])) {
              $fileName = checkFileName($file['name']);
              $originalImg = '';
              if ($origImgPath<>'') $originalImg = $origImgPath.$fileName;
              $error=$mediaManager->saveImage ($file, $webImgPath.$fileName, $originalImg, TAILLE_IMG, 0, 300);
            } elseif (strtoupper(substr ($file['name'], -3)) == 'MP3') {
              $fileName = checkFileName($file['name']);
              $error=$mediaManager->saveSound ($file, $soundPath.$fileName, TAILLE_SND);
            } elseif (strtoupper(substr ($file['name'], -3)) == 'AVI' || strtoupper(substr ($file['name'], -3)) == 'MPG' || strtoupper(substr ($file['name'], -3)) == 'MOV' || strtoupper(substr ($file['name'], -3)) == 'WMV' || strtoupper(substr ($file['name'], -3)) == 'RM') {
              $fileName = checkFileName($file['name']);
              $error=$mediaManager->saveVideo ($file, $videoPath.$fileName, TAILLE_VID);
            } else {
              $fileName = checkFileName($file['name']);
              if (!move_uploaded_file($file['tmp_name'], $otherFilesPath.$fileName)) {
                $error = $msgHandler->getError('uploadImpossible').' '.$file['name'];
              }
            }
            if ($error == '') {
              if ($confirmFiles<>'') $confirmFiles .= ', ';
              $confirmFiles .= $file['name'];
            }
          } else {
            $error = $msgHandler->getError('quotaExceeded');
          }
        } else {
          $error = $msgHandler->getError('uploadImpossible').' '.$file['name'];
        }
      }
    }
  }


  // Si les fichiers ont été envoyés avec succès, initialise le message de confirmation
  if ($confirmFiles) {
    $confirm = str_replace('%%%', $confirmFiles, $msgHandler->getConfirm('uploadSuccess'));
  }


  // Synchronize les nouvelles images dans le dossier des images originales
  if (isset ($_POST['synchroPics']) && $_POST['synchroPics']=='true' && $ftpAddress<>'') {
    if (isset ($_POST['password']) && $_POST['password']<>'') {
      $returnedMsg = getImagesByFTP ($ftpAddress, $albumName, $_POST['password'], $freeSpace);
      $confirm = $returnedMsg[0];
      $error = $returnedMsg[1];
    } else {
      $error = $msgHandler->getError('fieldError');;
    }
  }


  // Purge toutes les images originales
  if (isset ($_POST['purgeImg']) && $_POST['purgeImg']=='true') {
    if ($origImgPath<>'' && $handle = opendir($origImgPath)) {
      $file = '';
      while (false !== ($file = readdir($handle))) {
        if (strtoupper(substr ($file, -3)) == 'JPG' || strtoupper(substr ($file, -3)) == 'GIF' || strtoupper(substr ($file, -3)) == 'PNG') {
          deleteFile ($origImgPath.$file);
        }
      }
      closedir($handle);
    }
  }


  // Assignation de Smarty
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get('menuFileManager'), $admin, 1));
  $smarty->assign ('freeSpace', round ($freeSpace / 1048576, 2));
  $smarty->assign ('ratioSpace', round ($ratioSpace, 2));
  $smarty->assign ('isFTPactive', $ftpAddress);
  $smarty->assign ('quota_img', $quota_path.'quota.gif?noCache='.genID());
  $smarty->assign ('admin', $admin);
  $smarty->assign ('origImgPath', $origImgPath);
  $smarty->assign ('xajax_javascript', $xajax->getJavascript('../xajax/'));
  $smarty->display($pageName.'.tpl');

?>

