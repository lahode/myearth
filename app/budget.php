<?php
  include_once('../lib/init.php');
  include_once('../lib/Availability.php');


  // Met à jour le budget
  function updateBudget ($routeManager, $costsManager, $cost, $action='add') {
    if (!$cost) return;
    $lastCost = $costsManager->getCosts($cost->element, $cost->category);
    $tempCat = $routeManager->getCategoryBy ('id', $cost->category);
    $lastTotalCost = 0;
    foreach ($lastCost as $lastCostItem) {
      $lastTotalCost+= $lastCostItem->price;
    }
    if ($cost->element == 0 && ($tempCat->budget_transport_spent == 0 || $tempCat->budget_transport_spent == $lastTotalCost)) {
      if ($action=='delete') $tempCat->budget_transport_spent = $lastTotalCost-$cost->price;
      if ($action=='add') $tempCat->budget_transport_spent = $lastTotalCost+$cost->price;
      $routeManager->editCategory ($tempCat);
    }
    if ($cost->element == 1 && ($tempCat->budget_accommodation_spent == 0 || $tempCat->budget_accommodation_spent == $lastTotalCost)) {
      if ($action=='delete') $tempCat->budget_accommodation_spent = $lastTotalCost-$cost->price;
      if ($action=='add') $tempCat->budget_accommodation_spent = $lastTotalCost+$cost->price;
      $routeManager->editCategory ($tempCat);
    }
    if ($cost->element == 2 && ($tempCat->budget_food_spent == 0 || $tempCat->budget_food_spent == $lastTotalCost)) {
      if ($action=='delete') $tempCat->budget_food_spent = $lastTotalCost-$cost->price;
      if ($action=='add') $tempCat->budget_food_spent = $lastTotalCost+$cost->price;
      $routeManager->editCategory ($tempCat);
    }
  }


  // Si le option des budgets n'est pas activée, renvoie à la page d'accueil
  if (!$menuOption->statistics && ($admin || $property->showBudget)) {header ("Location:".PAGE_ACCUEIL);exit();}


  // Sélection du type de budget
  if (isset ($_GET['costType']) && $_GET['costType']==1) $costType = 1;
  elseif (isset ($_GET['costType']) && $_GET['costType']==2) $costType = 2;
  else $costType = 0;


  // Déclaration des variables
  $pageName = "budget";
  $costByCountry = array();
  $cat = null;
  $cost = null;
  $action = "";
  $countryToDisplay = "";
  $countriescurrency = array();
  $countrycurrency = "";


  // Récupération de l'envoie du formulaire
  if (isset ($_POST['element']) && $_POST['element'] == 0 && isset ($_POST['id']) && isset ($_POST['category']) && isset ($_POST['date']) && isset ($_POST['type']) &&
      isset ($_POST['departureCity']) && isset ($_POST['arrivalCity']) && isset ($_POST['name']) && isset ($_POST['price']) &&
      isset ($_POST['quality']) && isset ($_POST['length']) && isset ($_POST['duration'])) {
      $cost = new Costs ($_POST['id'], $_POST['category'], 0, unfdate($_POST['date']), $_POST['type'], addslash($_POST['departureCity']),
                         addslash($_POST['arrivalCity']), addslash($_POST['name']), $_POST['price'], $_POST['quality'],
                         '', '', $_POST['duration'], $_POST['length']);
  } elseif (isset ($_POST['element']) && $_POST['element'] == 1 && isset ($_POST['id']) && isset ($_POST['category']) && isset ($_POST['date']) && isset ($_POST['type']) &&
            isset ($_POST['arrivalCity']) && isset ($_POST['name']) && isset ($_POST['price']) && isset ($_POST['quality']) && isset ($_POST['address']) && isset ($_POST['description']) && isset ($_POST['duration'])) {
            $cost = new Costs ($_POST['id'], $_POST['category'], 1, unfdate($_POST['date']), $_POST['type'], '',
                               addslash($_POST['arrivalCity']), addslash($_POST['name']), $_POST['price'], $_POST['quality'],
                               $_POST['address'], $_POST['description'], $_POST['duration'], '');
  } elseif (isset ($_POST['element']) && $_POST['element'] == 2 && isset ($_POST['id']) && isset ($_POST['category']) && isset ($_POST['date']) && isset ($_POST['type']) &&
            isset ($_POST['arrivalCity']) && isset ($_POST['name']) && isset ($_POST['price']) && isset ($_POST['quality']) && isset ($_POST['address']) && isset ($_POST['description'])) {
            $cost = new Costs ($_POST['id'], $_POST['category'], 2, unfdate($_POST['date']), $_POST['type'], '',
                               addslash($_POST['arrivalCity']), addslash($_POST['name']), $_POST['price'], $_POST['quality'],
                               $_POST['address'], $_POST['description'], '', '');
  }

  // Si le bouton "Save" a été pressé, sauvegarde le nouveau budget
  if (isset ($_POST['save']) && $_POST['save']=='true' && $cost) {
    $action = $_POST['actionValue'];
    if ($cost->id=='') {
      do {
        $id = genID();
      } while ($costsManager->getCostBy('id', $id));
    } else {
      $id = $cost->id;
    }
    if ($cost->date<>'' && $cost->arrivalCity<>'' && (($cost->departureCity<>'' && $cost->element==0) || $cost->element==1  || $cost->element==2)) {
      if ($cost->element==1 && !is_numeric ($cost->length) && $cost->length <> '') $error = $msgHandler->getError ('insertDistance');
      if ($cost->element<>2 && !is_numeric ($cost->duration)) $error = $msgHandler->getError ('insertDuration');
      if (!is_numeric ($cost->price) && $cost->price <> '') $error = $msgHandler->getError ('insertPrice');
      if (!$error) {
        $cost->id = $id;
        updateBudget ($routeManager, $costsManager, $cost, 'add');
        $costsManager->editCost ($cost);
        header ("Location:".PAGE_REDIRECTION."?redirection=budget&costType=".$cost->element);
        exit();
      }
    } else {
      $error = $msgHandler->getError ('insertNameorLink');
    }
  }


  // Si le bouton "Add second" a été pressé, ajoute une seconde au coût existant
  if (isset ($_POST['addSecond']) && $_POST['addSecond']<>'') {
    $costSecond = $costsManager->getCostBy('id', $_POST['addSecond']);
    $costSecond->date++; 
    $costsManager->editCost ($costSecond);
    header ("Location:".PAGE_REDIRECTION."?redirection=budget&costType=".$costSecond->element);
    exit();
  }


  // Si le bouton "Edit" a été pressé, edite un coût existant
  if (isset ($_POST['editCost']) && $_POST['editCost']<>'') {
    $cost = $costsManager->getCostBy('id', $_POST['editCost']);
    $action = "edit";
  }


  // Ajustement du calendrier
  $departure = $routeManager->getDeparture ();
  $availability_obj = new Availability ($routeManager);
  if (!$departure) {header("Location:".PAGE_ACCUEIL."?error=*noDatesConfig");exit();}
  if ($departure->startingDate > time() && !$admin) {header("Location:".PAGE_ACCUEIL."?error=*noBudgetBeforeDeparture");exit();}
  $firstday = $departure->startingDate+(3600*24);
  if (isset ($_POST['editCost']) && $_POST['editCost']<>'' && $cost) {
    $availability = $availability_obj->getDestinationAvailability($cost->category, 0, 1);
  } elseif (isset ($_POST['insertCost']) && $_POST['insertCost']<>'') {
    $availability = $availability_obj->getDestinationAvailability($_POST['insertCost'], 0, 1);
    $firstday = substr ($availability['firstDate'], 0, -3);
  } else {
    $availability['lastDate'] = $departure->endingDate.'000';
    $availability['firstDate'] = $departure->startingDate.'000';
  }
  if ($cost && ($cost->element == 0 || $cost->element == 2)) {
    $availability['lastDate']=(substr ($availability['lastDate'], 0, -3)+3600*24).'000';
  }
  $availability['borderDates'] = null;
  $availability['occupedDates'] = null;
  $_SESSION['datesBooked'] = $availability;


  // Si le bouton "Insert" a été pressé, edite le nouveau coût
  if (isset ($_POST['insertCost']) && $_POST['insertCost']<>'') {
    $cost = new Costs ("", $_POST['insertCost'], '', $firstday, '', '', '', '', 0, 0, '', '', 0, 0);
    $action = "insert";
  }


  // Si le bouton "Delete" a été pressé, supprime le coût existant
  if (isset ($_POST['deleteCost']) && $_POST['deleteCost']<>'') {
    $tempCost = $costsManager->getCostBy('id', $_POST['deleteCost']);
    updateBudget ($routeManager, $costsManager, $tempCost, 'delete');
    $costsManager->deleteCost ($_POST['deleteCost']);
  }


  // Remplace les identifiants de l'album par les noms de la base de données
  if (count ($categories) > 0) {
    $dbQuery->connect();
    if ($cost) {
      $tempCat = $routeManager->getCategoryBy ('id', $cost->category);
      $countryToDisplay = $dbQuery->getCountryName($tempCat->country);
      $countrycurrency = $dbQuery->getCountryCurrency ($tempCat->country);
    }
    $incDest=1;
    $cat = null;
    foreach ($categories as $keyCat => $category) {
      $categoryChanged = clone $category;
      $categoryChanged->text = "";
      $categoryChanged->arrivalCity = $dbQuery->getCountryName($category->country);
      if ($categoryChanged->arrivalCity) {
        $country[$category->id] = $category->country;
      }
      $category->order = $keyCat;
      $cat[$category->id] = $categoryChanged;
      $countriescurrency[$category->id] = $dbQuery->getCountryCurrency ($category->country);
    }
//    if (count($cat) > 0) unset ($cat[count($cat)-1]);
    $dbQuery->disconnect();

    // Remplace les valeurs des Costs pour affichages
    $costs = $costsManager->getCosts($costType);
    if (count ($costs) > 0) {
      foreach ($costs as $keyCost => $costItem) {
        $costByCountry[$costItem->category][$keyCost] = $costItem;
        if ($costByCountry[$costItem->category][$keyCost]->element==1) {
          $costByCountry[$costItem->category][$keyCost]->type = $msgHandler->getTable ('accommodation', $costItem->type);
        } elseif ($costByCountry[$costItem->category][$keyCost]->element==2) {
          $costByCountry[$costItem->category][$keyCost]->type = $msgHandler->getTable ('food', $costItem->type);
        } else {
          $costByCountry[$costItem->category][$keyCost]->type = $msgHandler->getTable ('arrivalBy', $costItem->type);
        }
        $costByCountry[$costItem->category][$keyCost]->quality = $tableQuality[$costItem->quality];
        $costByCountry[$costItem->category][$keyCost]->description = str_replace("\r\n", "<br />", $costByCountry[$costItem->category][$keyCost]->description);
        $costByCountry[$costItem->category][$keyCost]->description = str_replace("\n", "<br />", $costByCountry[$costItem->category][$keyCost]->description);
        $costByCountry[$costItem->category][$keyCost]->description = str_replace("\r", "<br />", $costByCountry[$costItem->category][$keyCost]->description);
        $costByCountry[$costItem->category][$keyCost]->description = str_replace("'", " ", $costByCountry[$costItem->category][$keyCost]->description);
        $costByCountry[$costItem->category][$keyCost]->address = str_replace("\r\n", "<br />", $costByCountry[$costItem->category][$keyCost]->address);
        $costByCountry[$costItem->category][$keyCost]->address = str_replace("\n", "<br />", $costByCountry[$costItem->category][$keyCost]->address);
        $costByCountry[$costItem->category][$keyCost]->address = str_replace("\r", "<br />", $costByCountry[$costItem->category][$keyCost]->address);
        $costByCountry[$costItem->category][$keyCost]->address = str_replace("'", "¶", $costByCountry[$costItem->category][$keyCost]->address);
      }
      if (count ($costByCountry) > 0) {
        foreach ($costByCountry as $keyCostByCountry => $costByCountryItem) {
          $costByCountry[$keyCostByCountry] = sortObjBy ($costByCountryItem, 'date', SORT_ASC);
        }
      }
    }
  } else {
    header("Location:".PAGE_ACCUEIL."?error=*noCategoryFound");
    exit();
  }


  // Assignation de Smarty
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('tableArrivalBy', $msgHandler->getTable ('arrivalBy'));
  $smarty->assign ('tableLogging', $msgHandler->getTable ('accommodation'));
  $smarty->assign ('tableFood', $msgHandler->getTable ('food'));
  $smarty->assign ('tableQuality', $tableQuality);
  $smarty->assign ('cost', $cost);
  $smarty->assign ('action', $action);
  $smarty->assign ('costs', $costByCountry);
  $smarty->assign ('categories', $cat);
  $smarty->assign ('countriescurrency', $countriescurrency);
  $smarty->assign ('countrycurrency', $countrycurrency);
  $smarty->assign ('dateFormat', $dateFormat);
  $smarty->assign ('countryToDisplay', $countryToDisplay);
  $smarty->assign ('pageName', $pageName);
  $smarty->assign ('picPath', PICS_PATH);
  if ($costType==0) {
    $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuBudgetTransport'), $admin, 4));
    $smarty->display($pageName.'_transport.tpl');
  } elseif ($costType==1) {
    $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuBudgetAccommodation'), $admin, 4));
    $smarty->display($pageName.'_accommodation.tpl');
  } else {
    $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuBudgetFood'), $admin, 4));
    $smarty->display($pageName.'_food.tpl');
  }

?>

