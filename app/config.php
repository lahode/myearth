<?php
  include_once('../lib/CSSCreator.php');
  include_once('../lib/init.php');


  // Si l'administrateur de l'album ne s'est pas authentifié, renvoie l'utilisateur à la page d'accueil
  if (!$admin) {header("Location:".PAGE_ACCUEIL);exit();}


  // Si le bouton "Annuler" a été pressé, renvoie à la page d'accueil
  if (isset ($_POST['cancelConfig']) && $_POST['cancelConfig'] == 'true') {
    $fileManager->kill();
    if (isset($_SESSION['properties'])) unset ($_SESSION['properties']);
    if (isset($_SESSION['fontstyles'])) unset ($_SESSION['fontstyles']);
    header("Location:".PAGE_ACCUEIL);
    exit();
  }


  // Initialisation des données
  $pageName = "config";
  $pathCSS = str_replace (USERS_PATH.$albumName, '..', $backgroundImgPath);
  $tableRegion = array();
  $tableCity = array();
  $tableCountry = array();
  $availability = array();
  $imgBg = new Media ('bg', $msgHandler, $fileManager, $albumManager, $quota_path, $backgroundImgPath, true);
  $imgHome = new Media ('home', $msgHandler, $fileManager, $albumManager, $quota_path, $webImgPath, true);
  $uploadedBg = false;
  $uploadedHome = false;
  $availability = array();


  // Initialisation des données renvoyées par le formulaire
  if (isset ($_POST['init_'.$pageName])) {
    if (isset ($_POST['country']))            $postedCountry = $_POST['country']; else $postedCountry = "";
    if (isset ($_POST['region']))             $postedRegion = $_POST['region'];   else $postedRegion = "";
    if (isset ($_POST['city']))               $postedCity = $_POST['city'];       else $postedCity = "";
    if (isset ($_POST['prop_saveOrigImg']))   $postedSaveOrigImg = 1;             else $postedSaveOrigImg = 0;
    if (isset ($_POST['prop_showStats']))     $postedShowStats = 1;               else $postedShowStats = 0;
    if (isset ($_POST['prop_showBudget']))    $postedShowBudget = 1;              else $postedShowBudget = 0;
    if (isset ($_POST['prop_showMap']))       $postedShowMap = 1;                 else $postedShowMap = 0;
    if (isset ($_POST['prop_showDesc']))      $postedShowDescription = 1;         else $postedShowDescription = 0;
    if (isset ($_POST['prop_showShortCut']))  $postedShowShortCut = 1;            else $postedShowShortCut = 0;
    if (isset ($_POST['prop_deleteEmails']))  $postedDeleteEmails = 1;            else $postedDeleteEmails = 0;
    if (isset ($_POST['prop_sendEmails']))    $postedSendEmails = 1;              else $postedSendEmails = 0;
    if (isset ($_POST['option_forum']))       $postedOption_forum = 1;            else $postedOption_forum = 0;
    if (isset ($_POST['option_notebook']))    $postedOption_notebook = 1;         else $postedOption_notebook = 0;
    if (isset ($_POST['option_humour']))      $postedOption_humour = 1;           else $postedOption_humour = 0;
    if (isset ($_POST['option_news']))        $postedOption_news = 1;             else $postedOption_news = 0;
    if (isset ($_POST['option_chat']))        $postedOption_chat = 1;             else $postedOption_chat = 0;
    if (isset ($_POST['option_video']))       $postedOption_video = 1;            else $postedOption_video = 0;
    if (isset ($_POST['option_preparation'])) $postedOption_preparation = 1;      else $postedOption_preparation = 0;
    if (isset ($_POST['option_statistics']))  $postedOption_statistics = 1;       else $postedOption_statistics = 0;
    if (isset ($_POST['option_ranking']))     $postedOption_ranking = 1;          else $postedOption_ranking = 0;

    // Récupère les images
    $result_bgImg = $imgBg->addImage('', true, 0, 0);
    $result_homeImg = $imgHome->addImage('', true);
    if ($result_bgImg[0]) {$currentbgImg = current ($result_bgImg[0]);$posted_bgImg = $currentbgImg->name;} else $posted_bgImg = '';
    if ($result_homeImg[0]) {$currenthomeImg = current ($result_homeImg[0]);$posted_homeImg = $currenthomeImg->name;} else $posted_homeImg = '';
    if ($result_bgImg[1]<>'') $error = $result_bgImg[1];
    if ($result_homeImg[1]<>'') $error = $result_homeImg[1];

    $departure =   new Departure ($postedCountry, $postedRegion, $postedCity, addslash($_POST['cityName']), unfdate($_POST['startingDate']),
                                  unfdate($_POST['endingDate']), addslash($_POST['budget_accommodation_planed']), addslash($_POST['budget_food_planed']),
                                  addslash($_POST['budget_transport_planed']), addslash($_POST['budget_other_planed']));
    $properties =  new Property ($_POST['prop_font'], $_POST['prop_bgColor'], $_POST['prop_bgBodyColor'], $posted_bgImg, $_POST['prop_imgBgPos'],
                                 $_POST['prop_skin'], $postedSaveOrigImg, $postedShowStats, $postedShowBudget, $postedShowMap, $postedShowDescription,
                                 $postedShowShortCut, $postedDeleteEmails, $postedSendEmails, $_POST['dateFormat'], $_POST['language']);
    $menuOptions = new MenuOptions ($postedOption_forum, $postedOption_notebook, $postedOption_humour, $postedOption_news,
                                    $postedOption_chat, $postedOption_video, $postedOption_preparation, $postedOption_statistics, $postedOption_ranking);
    $home =        new Home (addslash($_POST['homeMsg']), $posted_homeImg);
    $albumTitle =  addslash($_POST['albumTitle']);
    $albumOwner =  addslash($_POST['albumOwner']);
    if (isset ($_POST['email'])) $emails = $_POST['email']; else $emails = array();
    if (isset ($_POST['quota'])) $quota = $_POST['quota']; else $quota = "";
    if (isset ($_POST['ftpaddress'])) $ftp = $_POST['ftpaddress']; else $ftp = "";
    if (isset ($_SESSION['fontstyles'])) $fontstyles = $_SESSION['fontstyles']['obj']; else $fontstyles = $albumManager->getFontStyles ();
    if (isset ($_POST['changeSkin']) && $_POST['changeSkin']=='true') {
      $pathName = $skinsFile.$skinsManager->getSkin ($_POST['prop_skin']).'/';
      if (file_exists ($pathName.'config.xml')) {
        $skinConfig = new AlbumManager ($pathName.'config', $albumName);
        if ($skinConfig->getProperties ()) {
          $tempSkin = $properties->skin;
          $properties = $skinConfig->getProperties ();
          $properties->skin = $tempSkin;
        }
        if ($skinConfig->getMenuOptions ()) $menuOptions = $skinConfig->getMenuOptions ();
        if ($skinConfig->getFontStyles ()) $fontstyles = $skinConfig->getFontStyles ();
      }
    }
    $_SESSION['properties']['departure'] = $departure;
    $_SESSION['properties']['properties'] = $properties;
    $_SESSION['properties']['menuOptions'] = $menuOptions;
    $_SESSION['properties']['home'] = $home;
    $_SESSION['properties']['title'] = $albumTitle;
    $_SESSION['properties']['owner'] = $albumOwner;
    $_SESSION['properties']['emails'] = $emails;
    $_SESSION['properties']['quota'] = $quota;
    $_SESSION['properties']['ftp'] = $ftp;
  } elseif (isset ($_GET['getFromSession']) && $_GET['getFromSession']=1 && isset ($_SESSION['properties'])) {
    $departure = $_SESSION['properties']['departure'];
    $properties = $_SESSION['properties']['properties'];
    $menuOptions = $_SESSION['properties']['menuOptions'];
    $home = $_SESSION['properties']['home'];
    $albumTitle = $_SESSION['properties']['title'];
    $albumOwner = $_SESSION['properties']['owner'];
    $emails = $_SESSION['properties']['emails'];
    $quota = $_SESSION['properties']['quota'];
    $ftp = $_SESSION['properties']['ftp'];
    if (isset ($_SESSION['fontstyles'])) $fontstyles = $_SESSION['fontstyles']['obj']; else $fontstyles = $albumManager->getFontStyles ();
 } else {
    $fileManager->init();
    $fontstyles = $albumManager->getFontStyles ();
    $departure = $routeManager->getDeparture ();
    $properties = $albumManager->getProperties ();
    $menuOptions = $albumManager->getMenuOptions ();
    $home = $albumManager->getHome ();
    $albumTitle = $albumManager->getTitle ();
    $albumOwner = $albumManager->getOwner ();
    $emails = $albumManager->getEmails ();
    $quota = round ($albumManager->getQuota ()/1048576);
    $ftp = $albumManager->getFTP ();
    $fileManager->addImage ('home', $webImgPath, $home->image, '');
    $fileManager->addImage ('bg', $backgroundImgPath, $properties->bgImg, '');
    $_SESSION['properties']['departure'] = $departure;
    $_SESSION['properties']['properties'] = $properties;
    $_SESSION['properties']['menuOptions'] = $menuOptions;
    $_SESSION['properties']['home'] = $home;
    $_SESSION['properties']['title'] = $albumTitle;
    $_SESSION['properties']['owner'] = $albumOwner;
    $_SESSION['properties']['emails'] = $emails;
    $_SESSION['properties']['quota'] = $quota;
    $_SESSION['properties']['ftp'] = $ftp;
    $_SESSION['fontstyles']['obj'] = $fontstyles;
  }

// *********************************************************************************************

  // Initialise les données pour les textures
  if (isset ($_POST['editSkin']) && $_POST['editSkin']<>'') {
    header("Location:config_skin.php");exit();
  }


  // Si le bouton d'édition d'un élément de texte a été pressé, charge l'élément et met le reste en mémoire
  if (isset ($_POST['editFont']) && $_POST['editFont']<>'') {
    header("Location:config_font.php?font=".$_POST['editFont']);exit();
  }


  // Si le bouton "Add Email" a été pressé, ajoute l'email à la liste
  if (isset ($_POST['addEmail']) && $_POST['addEmail']=='save') {
    if (isset ($_POST['newemail']) && is_email ($_POST['newemail'])) {
      $emails[count($emails)] = $_POST['newemail'];
    } else {
      $error = $msgHandler->getError ('invalidEmail');
    }
  }


  // Si le bouton "Delete Email" a été pressé, ajoute une page personnelle
  if (isset ($_FILES['perso']) && $_FILES['perso']['name']<>'') {
    if (is_uploaded_file($_FILES['perso']['tmp_name']) && (strtoupper(substr ($_FILES['perso']['name'], -3)) == 'TPL' || strtoupper(substr ($_FILES['perso']['name'], -3)) == 'HTM' || strtoupper(substr ($_FILES['perso']['name'], -4)) == 'HTML')) {
      if (!move_uploaded_file($_FILES['perso']['tmp_name'], USERS_PATH.$albumName.'/perso.tpl')) {
        $error = $msgHandler->getError('uploadImpossible').' '.$_FILES['perso']['name'];
      }
    } else {
      $error = $msgHandler->getError('formatPersoIncorrect');
    }
  }


  // Si le bouton "Delete Perso page" a été pressé, efface la page personnelle de la liste
  if (isset ($_POST['deletePerso']) && $_POST['deletePerso']<>'') {
    deleteFile (USERS_PATH.$albumName.'/perso.tpl');
  }


  // Si le bouton "Delete Email" a été pressé, efface l'email de la liste
  if (isset ($_POST['deleteEmail']) && $_POST['deleteEmail']<>'') {
    unset ($emails[$_POST['deleteEmail']]);
  }


  // Si le bouton "Delete Home's picture" a été pressé, l'image de la page d'accueil
  if (isset ($_POST['deleteImgHome']) && $_POST['deleteImgHome']=='true') {
    $home->image = '';
    $fileManager->deleteTempFiles ('home');
  }


  // Si le bouton "Delete Propertie's background picture" a été pressé, l'image en arrière-plan
  if (isset ($_POST['deleteImgBg']) && $_POST['deleteImgBg']=='true') {
    $properties->bgImg = '';
    $fileManager->deleteTempFiles ('bg');
  }

  // Si le bouton "Add Email" a été pressé, efface l'email de la liste
  if (isset ($_POST['deleteEmail']) && $_POST['deleteEmail']<>'') {
    unset ($emails[$_POST['deleteEmail']]);
  }


  // Si le bouton "Save" a été pressé, sauvegarde les informations de la configuration générale
  if (isset ($_POST['save']) && $_POST['save']=='true') {
    if ($departure->country=='') $error = $msgHandler->getError ('selectCountry');
    if ($departure->region=='') $error = $msgHandler->getError ('selectRegion');
    if ($departure->city=='') $error = $msgHandler->getError ('selectCity');
    if (!is_numeric ($departure->budget_accommodation_planed) && $departure->budget_accommodation_planed <> '') $error = $msgHandler->getError ('budget_accommodation_planed');
    if (!is_numeric ($departure->budget_food_planed) && $departure->budget_food_planed <> '') $error = $msgHandler->getError ('budget_food_planed');
    if (!is_numeric ($departure->budget_transport_planed) && $departure->budget_transport_planed <> '') $error = $msgHandler->getError ('budget_transport_planed');
    if (!is_numeric ($departure->budget_other_planed) && $departure->budget_other_planed <> '') $error = $msgHandler->getError ('budget_other_planed');
    if (!is_numeric ($quota) && $quota <> '') $error = $msgHandler->getError ('invalidQuota');
    if (!is_url ($ftp) && $ftp <> '') $error = $msgHandler->getError ('invalidLink');

    // Si aucune erreur n'a été détectée, sauvegarde les paramètres, modifie les styles et renvoie l'utilisateur à la page d'accueil
    if (!$error) {
      $fileManager->saveTempFiles ('bg', $backgroundImgPath);
      $fileManager->saveTempFiles ('home', $webImgPath);
      $albumManager->editTitle ($albumTitle);
      $albumManager->editOwner ($albumOwner);
      $albumManager->editEmail ($emails);
      if ($isSuperAdmin) $albumManager->editQuota ($quota*1048576);
      $albumManager->editFTP ($ftp);
      $albumManager->editHome ($home);
      $routeManager->editDeparture ($departure);
      $albumManager->editProperties ($properties);
      $albumManager->editMenuOptions ($menuOptions);
      if (isset ($_SESSION['fontstyles'])) {
        foreach ($_SESSION['fontstyles']['obj'] as $keyStyle => $itemStyle) {
          if (isset ($_SESSION['fontstyles']['changed'][$keyStyle]) && $_SESSION['fontstyles']['changed'][$keyStyle] == 'true') {
            $albumManager->editFontstyle ($itemStyle);
            createCSSElement ($headerPath."css/".$itemStyle->name.".css", $itemStyle);
          }
        }
      }
      
      // Création des bordures si le skin choisi est Design
      if ($properties->skin == 'Design') {
        $roundCorner = true;
        createArc ('tl', 10, 10, $properties->bgBodyColor, $properties->bgColor, $headerPath."css/pics/tl.gif");
        createArc ('tr', 10, 10, $properties->bgBodyColor, $properties->bgColor, $headerPath."css/pics/tr.gif");
        createArc ('bl', 10, 10, $properties->bgBodyColor, $properties->bgColor, $headerPath."css/pics/bl.gif");
        createArc ('br', 10, 10, $properties->bgBodyColor, $properties->bgColor, $headerPath."css/pics/br.gif");
        createArc ('', 10, 10, $properties->bgBodyColor, $properties->bgColor, $headerPath."css/pics/nt.gif");
      } else $roundCorner = false;
      createCSSGlobal ($headerPath."css/perso.css", $properties, $pathCSS, $roundCorner);
      unset ($home);
      unset ($departure);
      unset ($properties);
      unset ($menuOptions);
      if (isset($_SESSION['properties'])) unset ($_SESSION['properties']);
      if (isset($_SESSION['fontstyles'])) unset ($_SESSION['properties']);
      header("Location:".PAGE_ACCUEIL."?confirm=*configChanged");
      exit();
    }
  }


  // Initialisation les tableaux de la Base de données
  $dbQuery->connect();
  $tableCountry = $dbQuery->getAllCountries();
  $tableRegion = $dbQuery->getRegionsFromCountry($departure->country);
  $tableCity = $dbQuery->getCitiesFromRegion($departure->country, $departure->region);
  $dbQuery->disconnect();


  // Insère en session toutes les dates occupées pour le calendrier
  $availability1['lastDate'] = $departure->endingDate.'000';
  $availability2['firstDate'] = $departure->startingDate+(3600*24).'000';
  if ($categories) {
    $categories = sortObjBy ($categories, 'startingDate', SORT_ASC);
    $lastDate = $categories[0]->startingDate;
    $destinations = $routeManager->getDestinations ($categories[count($categories)-1]->id);
    if ($destinations) {
      $destinations = sortObjBy ($destinations, 'startingDate', SORT_ASC);
      $firstDate = $destinations[count($destinations)-1]->startingDate+(($destinations[count($destinations)-1]->nbDays+1)*3600*24);
    } else {
      $firstDate = $categories[count($categories)-1]->startingDate+(3600*24);
    }
    if ($lastDate <> '') {
      $availability1['lastDate'] = $lastDate.'000';
    }
    if ($firstDate <> '') {
      $availability2['firstDate'] = $firstDate.'000';
    }
  }
  $_SESSION['datesBooked']['startingDate'] = $availability1;
  $_SESSION['datesBooked']['endingDate'] = $availability2;


  // Récupère les petites images à afficher
  if ($properties && $properties->bgImg=='') $showBg = ''; elseif (file_exists ($backgroundImgPath.$properties->bgImg)) $showBg = $properties->bgImg; else {$showBg = '';$uploadedBg=true;}
  if ($home && $home->image=='') $showHome = ''; elseif (file_exists ($webImgPath.$home->image)) $showHome = $home->image; else {$showHome = '';$uploadedHome=true;}


  // Assignation de Smarty
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get('menuConfiguration'), true, 1));
  if (file_exists (USERS_PATH.$albumName.'/perso.tpl')) $smarty->assign ('persoFile', USERS_PATH.$albumName.'/perso.tpl');
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->assign ('error', $error);
  $smarty->assign ('imgBg', $imgBg->output('bg', $showBg, $uploadedBg));
  $smarty->assign ('imgHome', $imgHome->output('web', $showHome, $uploadedBg));
  $smarty->assign ('pageName', $pageName);
  $smarty->assign ('superadmin', $isSuperAdmin);
  $smarty->assign ('tableFont', $tableFont);
  $smarty->assign ('tableSkins', $tableSkins);
  $smarty->assign ('tableCountry', $tableCountry);
  $smarty->assign ('tableRegion', $tableRegion);
  $smarty->assign ('tableCity', $tableCity);
  $smarty->assign ('tableDateFormat', $tableDateFormat);
  $smarty->assign ('tableLanguage', $tableLanguage);
  $smarty->assign ('tableBgPosition', $tableBgPosition);
  $smarty->assign ('availability', $availability);
  $smarty->assign ('departure', $departure);
  $smarty->assign ('property', $properties);
  $smarty->assign ('menuOptions', $menuOptions);
  $smarty->assign ('home', $home);
  $smarty->assign ('albumTitle', $albumTitle);
  $smarty->assign ('albumOwner', $albumOwner);
  $smarty->assign ('emails', $emails);
  $smarty->assign ('ftpaddress', $ftp);
  $smarty->assign ('quota', $quota);
  $smarty->assign ('webImgPath', $webImgPath);
  $smarty->assign ('backgroundImgPath', $backgroundImgPath);
  $smarty->display($pageName.'.tpl');

?>

