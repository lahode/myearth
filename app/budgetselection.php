<?php
  include_once('../lib/init.php');
  include_once('../lib/CostsManager.php');


  // Déclaration des variables
  $pageName = "budget";


  // Si l'administrateur de l'album ne s'est pas authentifié, renvoie l'utilisateur à la page d'accueil
  if (!$admin) {header("Location:".PAGE_ACCUEIL);exit();}
  

  // Assignation de Smarty
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuBudget'), $admin, 4));
  $smarty->assign ('pageName', $pageName);
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->display($pageName.'.tpl');

?>

