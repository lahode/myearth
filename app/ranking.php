<?php
  include_once('../lib/init.php');

  // Récupère toutes les information de l'album
  $pageName = "ranking";
  $editRank = "";
  $rank = "";
  $countriesList = array();
  $allcountries = array();
  $countriesSelected = array();
  $countriesToDisplay = array();
  $countriesToAdd = array();
  $displayCountries = array();

  // Si Rank existe, affiche le classement sélectionné
  if (isset ($_GET['rank']) && $_GET['rank']<>'') {
    $rank = $_GET['rank'];

    // Enregistre les modifications du classement
    if (isset ($_POST['save']) && $_POST['save']=='true' && isset ($_POST['countryselected'])) {
      $rankManager->edit($rank, $_POST['countryselected']);
    }

    // Récupère les pays du classement
    $countriesList = $rankManager->getCountries($rank);
    if (count ($countriesList) > 0) {
      $dbQuery->connect();
      $i=0;
      foreach ($countriesList as $itemCountry) {
        $i++;
        $countriesSelected[$itemCountry] = $dbQuery->getCountryName($itemCountry);
        $countriesToAdd[$itemCountry] = $i;
      }
      $dbQuery->disconnect();
    }

    // Récupère les pays hors du classement
    if (count ($categories) > 0) {
      $dbQuery->connect();
      foreach ($categories as $keyCat => $category) {
        if (!isset ($countriesToAdd[$category->country])) {
          $allcountries[$category->country] = $dbQuery->getCountryName($category->country);
        } else {
          $displayCountries[$countriesToAdd[$category->country]] = array (
            $dbQuery->getCountryName($category->country),
            $category->summary->$rank
          );
        }
      }
      $dbQuery->disconnect();
      if ($allcountries) asort ($allcountries);
      if ($displayCountries) ksort ($displayCountries);
    }

    // Si Edit Rank a été pressé, affiche les listes déroulantes de modification
    if (isset ($_POST['editRank']) && $_POST['editRank']<>'') $editRank = $_POST['editRank'];

  // Sinon affiche tous les classements
  } else {
    foreach ($msgHandler->getTable('ranking') as $keyRankType => $rankType) {
      $countriesListTemp = $rankManager->getCountries($keyRankType);
      if (count ($countriesListTemp) > 0) {
        $dbQuery->connect();
        foreach ($countriesListTemp as $itemCountry) {
          $countriesToDisplay[$keyRankType][$itemCountry] = $dbQuery->getCountryName($itemCountry);
        }
        $dbQuery->disconnect();
      }
    }
  }

  // Assignation de Smarty
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->assign ('userLanguage', $language);
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $lastNewsDate, $admin, 1, $scrollMessage));
  $smarty->assign ('allcountries', $allcountries);
  $smarty->assign ('countriesSelected', $countriesSelected);
  $smarty->assign ('countriesToDisplay', $countriesToDisplay);
  $smarty->assign ('displayCountries', $displayCountries);
  $smarty->assign ('elements', $msgHandler->getTable('ranking'));
  $smarty->assign ('tabRanking', $msgHandler->getTable('rankingValues'));
  $smarty->assign ('rank', $rank);
  $smarty->assign ('editRank', $editRank);
  $smarty->display($pageName.'.tpl');

?>

