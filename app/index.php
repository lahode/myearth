<?php
  include_once ('Worldmap.php');
  include_once ('../lib/declare.php');
  include_once ('../lib/GlobalClasses.php');
  include_once ('../lib/DatabaseQuery.php');
  include_once ('../lib/RouteManager.php');
  include_once ('../lib/AlbumManager.php');
  include_once ('../lib/functions.php');
  include_once ('../lib/Language.php');

  class User {
    var $name;
    var $link;

    function __construct($name, $link) {
      $this->name = $name;
      $this->link = $link;
    }
  }

  // Formate une date
  function showdate ($date) {
    $msgHandler = new Language ('fr');
    $tabMonth = $msgHandler->getTable('month');
    if (!$date || !ctype_digit($date)) return $date;
    $resultDate = date ('d', $date).' '.$tabMonth[date ('m', $date)].' '.date ('Y', $date);
    return $resultDate;
  }//fdate


  // Liste des utilisateurs
  $dir = '../tmp';
  $names = array();
  $destinations = null;
  $countries = null;
  $i = 0;
  $dh = opendir($dir);
  while (($file = readdir($dh)) !== false)
    if ($file != "." and $file != ".." and $file != "demo") {
      $albumManager = new AlbumManager ($dir.'/'.$file.'/xml/album', $file);
      $routeManager = new RouteManager ($dir.'/'.$file.'/xml/route', $file);
      $categories = $routeManager->getCategories ();
      $departure = $routeManager->getDeparture ();
      if (count ($categories) > 0 && $departure->startingDate <= time()) {
        if ($departure->endingDate < time()) {
          $destinations[$i] = new DestinationObj (0, -2, $departure->region, $departure->city, $departure->cityName, $departure->endingDate, 0, '', '', null, 0, null);
          $countries[$i] = $departure->country;
        } else {
          foreach ($categories as $keyCat => $itemCat) {
            $lastDestination = $routeManager->getFirstLastDestination($itemCat->id, true);
            if ($lastDestination) {
              if (!isset ($destinations[$i]) || (isset ($destinations[$i]) && $destinations[$i]->startingDate < $lastDestination->startingDate)) {
                $countries[$i] = $itemCat->country;
                $destinations[$i] = $routeManager->getFirstLastDestination($itemCat->id, true);
              }
            }
          }
        }
      }
      if (!isset ($destinations[$i])) {
        if ($departure->startingDate > time()) {
          $destinations[$i] = new DestinationObj (0, -1, $departure->region, $departure->city, $departure->cityName, $departure->startingDate, 0, '', '', null, 0, null);
          $countries[$i] = $departure->country;
        }
      }
      $names[$i] = new User (htmlentities($albumManager->getOwner()), 'http://'.WEB_PAGE.'/app/login.php?album='.$file);
      //if (isset ($destinations[$i])) 
        $i++;
    }
    closedir($dh);

  // Initialisation des données
  $worldmap = new Worldmap ($smarty, null);
  $showWorld = $worldmap->showAllUsers ($destinations, $countries, $names, true);
  asort($names);
  $names[] = new User ('Demo', 'http://'.WEB_PAGE.'/app/login.php?album=test');


  // Assignation de Smarty
  $smarty->registerPlugin('modifier', 'showdate', 'showdate');
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->assign ('headerWorldmap', $showWorld[0]);
  $smarty->assign ('worldmap', $showWorld[1]);
  $smarty->assign ('linkTest', 'http://'.WEB_PAGE.'/app/login.php?album=test');
  $smarty->assign ('contact', 'http://'.WEB_PAGE.'/app/contact.php?album=test');
  $smarty->assign ('names', $names);
  $smarty->display('index.tpl');

?>

