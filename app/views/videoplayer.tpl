{if ($player=="wmp")}
<object id="{$videoID}" width="{$width}" height="{$height}" classid="CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95"
codebase="http://activex.microsoft.com/activex/controls/ mplayer/en/nsmp2inf.cab#Version=6,4,5,715" standby="Chargement..." type="application/x-oleobject">
<param name="FileName" value="{$file}">
<param name="AutoStart" value="{$autostart}">
<embed type="application/x-mplayer2" pluginspage = "http://www.microsoft.com/Windows/MediaPlayer/"
src="{$file}" name="{$videoID}" width="{$width}" height="{$height}" AutoStart="{$autostart}">
</embed>
</object>

{elseif ($player=="quicktime")}
<object id="{$videoID}" width="{$width}" height="{$height}" classid="CLSID:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B"
codebase="http://www.apple.com/qtactivex/qtplugin.cab">
<param name="src" value="{$file}">
<param name="autoplay" value="{$autostart}">
<param name="controller" value="true">
<embed TYPE="video/quicktime"
src="{$file}" name="{$videoID}" width="{$width}" height="{$height}" autoplay="{$autostart}" controller="true">
</embed>
</object>

{elseif ($player=="real")}
<object id="{$videoID}" width="{$width}" height="{$height}" classid="CLSID:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA">
<param name="src" value="{$file}">
<param name="console" value="video">
<param name="controls" value="controlpanel">
<param name="autostart" value="{$autostart}">
<embed type="audio/x-pn-realaudio-plugin"
src="{$file}" name="{$videoID}" width="{$width}" height="{$height}" autostart="{$autostart}" controls="controlpanel" console="video">
</embed>
</object>
{/if}