{config_load file=$userLanguage|cat:"/viewcontact_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<form name="form" method="post">
<input type="hidden" name="save" value="false" />
<input type="hidden" name="cancel" value="false" />
<input type="hidden" name="id" value="{$contact->id}" />
<input type="hidden" name="actions" value="{$action}" />

<fieldset>
  <br />
  <label style="width:100px">{#name#}</label>
  <label style="width:450px">
  <input type="text" name="title" value="{$contact->title}" size="5" />
  <input type="text" name="firstname" value="{$contact->firstname}" size="25" />
  <input type="text" name="lastname" value="{$contact->lastname}" size="25" />
  </label>
  <br /><br />
  <label style="width:100px">{#nickname#}</label>
  <label style="width:450px">
  <input type="text" name="nickname" value="{$contact->nickname}" size="30" />
  </label>
  <br /><br />
  <label style="width:100px">{#email#}</label>
  <label style="width:450px">
  <input type="text" name="email" value="{$contact->email}" size="30" />
  </label>
  <br /><br />
  <label style="width:100px">{#birthdate#}</label>
  <label style="width:450px">
  <table cellpadding="0" cellspacing="0" >
    <tr>
      <td>
        {html_select tab=$dayList name='birthday' selected=$birthday}
      </td>
      <td>
        {html_select tab=$monthList name='birthmonth' selected=$birthmonth}
      </td>
      <td>
        <select name="birthyear">
          {section name=foo start=1900 loop=2008 step=1}
          {if $smarty.section.foo.index==$birthyear}
          <option value="{$smarty.section.foo.index}" selected="selected">{$smarty.section.foo.index}</option>
          {else}
          <option value="{$smarty.section.foo.index}">{$smarty.section.foo.index}</option>
          {/if}
          {/section}
        </select>
      </td>
    </tr>
  </table>
  </label>
  <br /><br />
  <label style="width:100px">{#address#}</label>
  <label style="width:450px">
  <textarea name="address" cols="55" rows="5">{$contact->address}</textarea>
  </label>
  <br /><br /><br /><br /><br /><br />
  <label style="width:100px">{#zipcode#}</label>
  <label style="width:80px"><input type="text" name="zipcode" value="{$contact->zipcode}" size="5" /></label>
  <label style="width:50px">{#city#}</label>
  <input type="text" name="city" value="{$contact->city}" size="25" />
  <br /><br />
  <label style="width:100px">{#country#}</label>
  <label style="width:450px">
  <input type="text" name="country" value="{$contact->country}" size="50" />
  </label>
  <br /><br />
  <label style="width:100px">{#tel#}</label>
  <label style="width:180px"><input type="text" name="tel" value="{$contact->tel}" size="20" /></label>
  <label style="width:80px">{#fax#}</label>
  <input type="text" name="fax" value="{$contact->fax}" size="20" />
  <br /><br />
  <label style="width:100px">{#mobile#}</label>
  <label style="width:180px"><input type="text" name="mobile" value="{$contact->mobile}" size="20" /></label>
  <label style="width:80px">{#messenger#}</label>
  <input type="text" name="messenger" value="{$contact->messenger}" size="20" />
  <br /><br />
  <label style="width:100px">{#comments#}</label>
  <label style="width:450px">
  <textarea name="comments" cols="55" rows="5">{$contact->comments}</textarea>
  </label>
</fieldset>

<br /><br />
<table width="100%">
  <tr>
    <td align="center">
      <input type="submit" value="{#cancel#}" onclick="form.cancel.value='true'" />
    </td>
    <td align="center">
      <input type="submit" value="{#save#}" onclick="form.save.value='true'" />
    </td>
  </tr>
</table>

</form>

{$showFooter}
