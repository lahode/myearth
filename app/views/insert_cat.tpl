{config_load file=$userLanguage|cat:"/insert_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

<script language="javascript" src="../lib/js/functions.js"></script>
<script language="javascript" src="../lib/js/calendar.js"></script>
<script language="javascript" src="../lib/js/phpTab2js.js"></script>

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<form name="form" method="post" enctype="multipart/form-data">
<input type="hidden" name="cancel" value="false" />
<input type="hidden" name="save" value="false" />
<input type="hidden" name="country" value="{$country}" />
<input type="hidden" name="deleteSound" value="" />
<input type="hidden" name="deleteMap" value="" />
<input type="hidden" name="addSound" value="false" />
<input type="hidden" name="id" value="{$cat->id}" />
<input type="hidden" name="lastCurrency" value="{$selectedCurrency}" />
<input type="hidden" name="modifyCat" value="{$modifyCat}">
<input type="hidden" name="init_{$pageName}" value="true">

<p class="formTitle">{#infoCountry#}</p>

<fieldset>
  <br />
  {if $tableCountry}
  <label style="width:300px">{#country#}</label>
  {html_select obj=$tableCountry name='country' url='#' decode='utf8' id='iso2' value='name' default='Veuillez sélectionner un pays' selected=$cat->country}
  <input type="image" src="{$picPath}admin/refresh.gif" title="{#inforefreshlist#}" class="imgButton" class="middle" />
  {else}
  <label style="width:300px">{#country#}</label>
  {$country_name}
  {/if}
  <br /><br />

  <label style="width:300px">{#cityArr#}</label>
  {html_select obj=$tableCity name='cityArr' id='id' value='name' default='Veuillez sélectionner une ville' selected=$cat->arrivalCity}
  <br /><br />

  <label style="width:300px">{#cityDep#}</label>
  {html_select obj=$tableCity name='cityDep' id='id' value='name' default='Veuillez sélectionner une ville' selected=$cat->departureCity}
  <br /><br />

  <label style="width:300px">{#arrivalDate#}</label>
  <input type="text" name="startingDate" value="{$cat->startingDate|fdate}" readonly="readonly" size="20" maxlength="20" />
  <a href="javascript:cal1.popup();"><img src="{$picPath}calendar/cal.gif" border="0" title="{#infocalendar#}" width="16" height="16" class="middle" /></a>
  <br /><br />

  <label style="width:300px">{#selectCurrency#}</label>
  {html_select tab=$tabCurrency name='currency' url='#' id='admin' value='name' selected=$selectedCurrency}
  <input type="image" src="{$picPath}admin/refresh.gif" title="{#inforefreshlist#}" class="imgButton" class="middle" />
  <br /><br />

  <label style="width:300px">{#budget_accommodation_planed#}</label>
  <input type="text" name="budget_accommodation_planed" value="{$cat->budget_accommodation_planed}" size="10" maxlength="20" /> {if $selectedCurrency==0}{$countrycurrency}{else}{$maincurrency}{/if}
  <br /><br />

  <label style="width:300px">{#budget_food_planed#}</label>
  <input type="text" name="budget_food_planed" value="{$cat->budget_food_planed}" size="10" maxlength="20" /> {if $selectedCurrency==0}{$countrycurrency}{else}{$maincurrency}{/if}
  <br /><br />

  <label style="width:300px">{#budget_transport_planed#}</label>
  <input type="text" name="budget_transport_planed" value="{$cat->budget_transport_planed}" size="10" maxlength="20" /> {if $selectedCurrency==0}{$countrycurrency}{else}{$maincurrency}{/if}
  <br /><br />

  <label style="width:300px">{#budget_other_planed#}</label>
  <input type="text" name="budget_other_planed" value="{$cat->budget_other_planed}" size="10" maxlength="20" /> {if $selectedCurrency==0}{$countrycurrency}{else}{$maincurrency}{/if}
  <br /><br />

  <label style="width:300px">{#budget_accommodation_spent#}</label>
  <input type="text" name="budget_accommodation_spent" value="{$cat->budget_accommodation_spent}" size="10" maxlength="20" /> {if $selectedCurrency==0}{$countrycurrency}{else}{$maincurrency}{/if}
  <br /><br />

  <label style="width:300px">{#budget_food_spent#}</label>
  <input type="text" name="budget_food_spent" value="{$cat->budget_food_spent}" size="10" maxlength="20" /> {if $selectedCurrency==0}{$countrycurrency}{else}{$maincurrency}{/if}
  <br /><br />

  <label style="width:300px">{#budget_transport_spent#}</label>
  <input type="text" name="budget_transport_spent" value="{$cat->budget_transport_spent}" size="10" maxlength="20" /> {if $selectedCurrency==0}{$countrycurrency}{else}{$maincurrency}{/if}
  <br /><br />

  <label style="width:300px">{#budget_other_spent#}</label>
  <input type="text" name="budget_other_spent" value="{$cat->budget_other_spent}" size="10" maxlength="20" /> {if $selectedCurrency==0}{$countrycurrency}{else}{$maincurrency}{/if}
  <br /><br />

  {if $maincurrency == $countrycurrency}
  <input type="hidden" name="rate" value="1" />
  {else}
  <label style="width:300px">{#rate#}</label>
  (1 {$maincurrency} = <input type="text" name="rate" value="{$cat->rate}" size="5" maxlength="20" /> {$countrycurrency})
  <span style="cursor:pointer" title="{#infoselectsnd#}" onclick="window.open('calc.html', null, 'height=250,width=280,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes')"><img src="{$picPath}admin/calculator.gif" border="0" width="24" height="24" class="middle" /></span>
  <br /><br />
  {/if}

  <label style="width:150px">{#general#}</label>
  <textarea name="general" id="general" cols="60" rows="3">{$cat->summary->general|sslash}</textarea>
  <br /><br />

  <label style="width:150px">{#population#}</label>
  <textarea name="population" id="population" cols="60" rows="3">{$cat->summary->population|sslash}</textarea>
  <br /><br />

  <label style="width:150px">{#politic#}</label>
  <textarea name="politic" id="politic" cols="60" rows="3">{$cat->summary->politic|sslash}</textarea>
  <br /><br />

  <label style="width:150px">{#history#}</label>
  <textarea name="history" id="history" cols="60" rows="3">{$cat->summary->history|sslash}</textarea>
  <br /><br />

  <label style="width:150px">{#nature#}</label>
  <textarea name="nature" id="nature" cols="60" rows="3">{$cat->summary->nature|sslash}</textarea>
  <br /><br />

  <label style="width:150px">{#infra#}</label>
  <textarea name="infra" id="infra" cols="60" rows="3">{$cat->summary->infra|sslash}</textarea>
  <br /><br />

  <label style="width:150px">{#food#}</label>
  <textarea name="food" id="food" cols="60" rows="3">{$cat->summary->food|sslash}</textarea>
  <br /><br />

  <label style="width:150px">{#activity#}</label>
  <textarea name="activity" id="activity" cols="60" rows="3">{$cat->summary->activity|sslash}</textarea>
  <br /><br />

  <label style="width:150px">{#budget#}</label>
  <textarea name="budget" id="budget" cols="60" rows="3">{$cat->summary->budget|sslash}</textarea>
  <br /><br />

  <label style="width:150px">{#accommodation#}</label>
  <textarea name="accommodation" id="accommodation" cols="60" rows="3">{$cat->summary->accommodation|sslash}</textarea>
  <br /><br />

  <label style="width:150px">{#transport#}</label>
  <textarea name="transport" id="transport" cols="60" rows="3">{$cat->summary->transport|sslash}</textarea>
  <br /><br />

  <label style="width:150px">{#communication#}</label>
  <textarea name="communication" id="communication" cols="60" rows="3">{$cat->summary->communication|sslash}</textarea>
  <br /><br />

  <label style="width:150px">{#other#}</label>
  <textarea name="other" id="other" cols="60" rows="3">{$cat->summary->other|sslash}</textarea>
  <br /><br />

  <label style="width:150px">{#map#}</label>
  {$displayMap}
  {if $mapKey}
  &nbsp;<img src="{$picPath}admin/delete.gif" style="cursor:pointer" class="middle" onclick="createCustomConfirm('{#areyousure#} {$map->name} ?');
  document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteMap.value='{$mapKey}';form.submit();{literal}}{/literal}" />
  {/if}
  <br /><br />

  <label style="width:300px">{#showmap#}</label>
  <input type="checkbox" name="showmap" id="showmap" {if $cat->showmap}checked="checked"{/if} />
  <br /><br />

  <label style="width:300px">{#allowsametime#}</label>
  <input type="checkbox" name="allowsametime" id="allowsametime" {if $cat->allowsametime}checked="checked"{/if} />
  <br /><br />

  <label style="width:150px">{#sound#}</label>
  {$displaySound}
  {if $soundKey}
  &nbsp;<img src="{$picPath}admin/delete.gif" style="cursor:pointer" class="middle" onclick="createCustomConfirm('{#areyousure#} {$sound->name} ?');
  document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteSound.value='{$soundKey}';form.submit();{literal}}{/literal}" />
  {/if}
  </label>
  <br /><br />

</fieldset>

<br /><br />

<table width="100%" border="0" align="center">
  <tr>
    <td align="center">
      <input type="submit" value="{#cancel#}" onclick="form.cancel.value='true'" />
    </td>
    <td align="center">
      <input type="submit" value="{#save#}" onclick="form.save.value='true'" />
    </td>
  </tr>
</table>

</form>

<script language="JavaScript">
<!-- // create calendar object(s) just after form tag closed
  var cal1 = new calendar1(document.forms['form'].elements['startingDate'], '', '{$dateFormat}', document.forms['form']);
  cal1.year_scroll = true;
  cal1.time_comp = false;
//-->
</script>

<a name="downPage"></a>

{$showFooter}
