{config_load file=$userLanguage|cat:"/insert_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

<script language="javascript" src="../lib/js/functions.js"></script>
<script language="javascript" src="../lib/js/calendar.js"></script>
<script language="javascript" src="../lib/js/phpTab2js.js"></script>

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<form name="form" method="post" enctype="multipart/form-data">
<input type="hidden" name="cancel" value="false" />
<input type="hidden" name="save" value="false" />
<input type="hidden" name="country" value="{$country}" />
<input type="hidden" name="search" value="" />
<input type="hidden" name="category" value="{$category}" />
<input type="hidden" name="id" value="{$dest->id}" />
<input type="hidden" name="init_{$pageName}" value="true">

<p class="formTitle">{#infoDest#}</p>

<fieldset>
  <br />
  <label style="width:200px">{#country#}</label>
  {$country_name}
  <br /><br />

  <label style="width:200px">{#searchCity#}</label>
  <input type="text" name="searchCity" />
  <input type="submit" value="{#search#}" onclick="form.search.value='true'" size="30" maxlength="30" />
  <br /><br />

  {if $tableRegion}
  <label style="width:200px">{#region#}</label>
  {html_select obj=$tableRegion name='region' url='#' id='admin' value='name' default='Veuillez sélectionner une région' selected=$dest->region}
  <input type="image" src="{$picPath}admin/refresh.gif" title="{#inforefreshlist#}" class="imgButton" class="middle" />
  <br /><br />
  {/if}

  {if $tableCity}
  <label style="width:200px">{#city#}</label>
  {html_select obj=$tableCity name='city' id='id' value='name' default='Veuillez sélectionner une ville' selected=$dest->city}
  <br /><br />
  {/if}

  <label style="width:200px">{#cityifnotfound#}</label>
  <input type="text" name="cityName" value="{$dest->cityName|sslash}" />
  <br /><br />

  <label style="width:200px">{#arrivalDate#}</label>
  <input type="text" name="startingDate" value="{$dest->startingDate|fdate}" readonly="readonly" size="20" maxlength="20" />
  <a href="javascript:cal1.popup();"><img src="{$picPath}calendar/cal.gif" border="0" title="{#infocalendar#}" width="16" height="16" class="middle" /></a>
  <br /><br />

  <label style="width:200px">{#arrivalBy#}</label>
  {html_select tab=$tableArrivalBy name='arrivalBy' selected=$dest->arrivalBy}
  <br /><br />
</fieldset>

<br /><br />

<table width="100%">
  <tr>
    <td align="center">
      <input type="submit" value="{#cancel#}" onclick="form.cancel.value='true'" />
    </td>
    <td align="center">
     <input type="submit" value="{#save#}" onclick="form.save.value='true';" />
    </td>
  </tr>
</table>

</form>

<script language="JavaScript">
<!-- // create calendar object(s) just after form tag closed
  var cal1 = new calendar1(document.forms['form'].elements['startingDate'], '', '{$dateFormat}', document.forms['form']);
  cal1.year_scroll = true;
  cal1.time_comp = false;
//-->
</script>

{$showFooter}
