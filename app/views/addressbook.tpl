{config_load file=$userLanguage|cat:"/fileviewer_"|cat:$userLanguage|cat:".conf"}
{config_load file=$userLanguage|cat:"/compose_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

</head>

<body id="mainbody" style="background-image:none;" onload="window.opener.form.action='{#PAGE_COMPOSE#}'">
<form name="ab" method="POST" onsubmit="return checkForm(document.ab);">
<input type="hidden" name="init_{$pageName}" value="true">
<input type="hidden" name="selected" value="false">
<input type="hidden" name="actions" value="init">
<div>
  <br />
  <table width="575" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td rowspan="3" valign="top" style="padding-right:15px;">
        {html_select obj=$contacts name='contacts[]' id='id' value='email' style='width:241px;height:312px;font-size:11px' max='50' size='10'}&nbsp;
      </td>
      <td valign="top">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td valign="top">
              <table border="0" width="72" cellpadding="0" cellspacing="0" align="center">
                <tr><td align="center"><input type="button" value=">>" onclick="document.ab.actions.value='to+'; document.ab.submit();" /></td></tr>
                <tr><td class="sitetitle" style="text-align:center">{#to#}</td></tr>
                <tr><td align="center"><input type="button" value="<<" onclick="document.ab.actions.value='to-'; document.ab.submit();" /></td></tr>
              </table>
            </td>
            <td align="right" width="241">
              {if ($addressbook.to_address)}
              {html_select tab=$addressbook.to_address name='to_address[]' style='width:241px;height:92px;font-size:11px' size='10'}&nbsp;
              {else}
              <select name="to_address[]" multiple  size="10" style="width:241px;height:92px;font-size:11px">
              </select>
              {/if}
            </td>
          </tr>
	</table>
      </td>
    </tr>
    <tr>
      <td valign="top" style="padding-top:15px;">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td valign="top">
              <table border="0" width="72" cellpadding="0" cellspacing="0" align="center">
                <tr><td align="center"><input type="button" value=">>" onclick="document.ab.actions.value='cc+'; document.ab.submit();" /></td></tr>
                <tr><td class="sitetitle" style="text-align:center">{#cc#}</td></tr>
                <tr><td align="center"><input type="button" value="<<" onclick="document.ab.actions.value='cc-'; document.ab.submit();" /></td></tr>
              </table>
            </td>
            <td align="right" width="241">
              {if ($addressbook.cc_address)}
              {html_select tab=$addressbook.cc_address name='cc_address[]' style='width:241px;height:92px;font-size:11px' size='10'}&nbsp;
              {else}
              <select name="cc_address[]" multiple  size="10" style="width:241px;height:92px;font-size:11px">
              </select>
              {/if}
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td valign="top" style="padding-top:15px;">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td valign="top">
              <table border="0" width="72" cellpadding="0" cellspacing="0" align="center">
                <tr><td align="center"><input type="button" value=">>" onclick="document.ab.actions.value='bcc+'; document.ab.submit();" /></td></tr>
                <tr><td class="sitetitle" style="text-align:center">{#bcc#}</td></tr>
                <tr><td align="center"><input type="button" value="<<" onclick="document.ab.actions.value='bcc-'; document.ab.submit();" /></td></tr>
              </table>
            </td>
            <td align="right" width="241">
              {if ($addressbook.bcc_address)}
              {html_select tab=$addressbook.bcc_address name='bcc_address[]' style='width:241px;height:92px;font-size:11px' size='10'}&nbsp;
              {else}
              <select name="bcc_address[]" multiple  size="10" style="width:241px;height:92px;font-size:11px">
              </select>
              {/if}
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td align="center" colspan="2">
        <br />
        <input type="button" name="" value="{#save#}" onclick="document.ab.selected.value='true'; document.ab.submit();">
      </td>
    </tr>
  </table>
</div>
</form>
</body>
</html>
