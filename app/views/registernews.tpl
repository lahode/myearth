{config_load file=$userLanguage|cat:"/news_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<form name="form" method="post">
<input type="hidden" name="save" value="false" />
<input type="hidden" name="saveMsg" value="false" />
<input type="hidden" name="deletePerson" value="false" />


<br />
<p class="sitetext">{$message|nl2br}</p>
<br />

<br />
<fieldset>
  <br />
  <label style="width:80px">{#name#}</label>
  <label style="width:250px"><input type="text" name="name" size="50" /></label>
  <br />
  <br />
  <label style="width:80px">{#email#}</label>
  <label style="width:200px"><input type="text" name="email" size="50" /></label>
  <br />
  <br />
  <p align="center"><input type="submit" value="{#register#}" onclick="form.save.value='true'" /></p>
</fieldset>

<br />
{if $admin && $registeredPersons}
<br />
<table cellpadding="0" cellspacing="0" width="60%" align="center" border="0">
  <tbody>
    <tr>
      <td class="sitetitle" style="BORDER-BOTTOM: #b3c0d0 1px solid" align=middle width="380" height="18">
        {#registeredPersons#}&nbsp;
      </td>
      <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18">
        {#action#}&nbsp;
      </td>
    </tr>
    {foreach from=$registeredPersons item=person}
    <tr valign="top">
      <td align="left">
        <p class="sitetext"><a href="mailto:{$person->email}">{$person->name}</a> ({$person->date|fdate})</p>
      </td>
      <td align="center">
        <img style="cursor:pointer" title="{#infodeletePerson#}" src="{$picPath}admin/delete.gif" onclick="createCustomConfirm('{#areyousure#} {$itemLink->name} ?');
        document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deletePerson.value='{$person->id}';form.submit();{literal}}{/literal}" />
      </td>
    </tr>
    {/foreach}
  </tbody>
</table>
<br />
{/if}



{if $admin}
<br />
<fieldset>
  <br />
  <label style="width:80px">{#message#}</label>
  <textarea name="message" cols="80" rows="5">{$message}</textarea>
  <br />
  <br />
  <p align="center"><input type="submit" value="{#save#}" onclick="form.saveMsg.value='true'" /></p>
</fieldset>
{/if}

</form>

{$showFooter}
