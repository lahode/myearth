<a name="top"></a>

<table cellspacing="0" cellpadding="0" width="808" align="center" border="0">
  <tbody>
  <tr>
    <td colspan="4">
      <div id="headertopout">
        <div class="rbroundbox"><div class="rbtop"><div></div></div><div class="rbcontent">
          <div id="maintitle" class="maintitle">{$title|sslash}</div>
          {if $showMenu && $showMenu<>'preview'}
          <div id="logout"><a href="{#PAGE_REDIRECTION#}?redirection=login&logout=true">{if $imgLogoff}<img alt="{#logoff#}" src="{$skinPath}{$imgLogoff}" border="0">{else}{#logoff#}{/if}</a></div>
          {if !$admin}
          <div id="admin"><a href="{#PAGE_LOGIN#}?album={$albumName}&admin=true">{if $imgAdmin}<img alt="{#adminaccess#}" src="{$skinPath}{$imgAdmin}" border="0">{else}{#adminaccess#}{/if}</a></div>
          {else}
          <div id="admin">&nbsp;</div>
          {/if}
          {else}
          <div id="logout">&nbsp;</div>
          <div id="admin">&nbsp;</div>
          {/if}
        </div><div class="rbbot"><div></div></div></div>
      </div>
    </td>
  </tr>
  <tr>
    <td id="headerleftout">&nbsp;</td>
    <td colspan="2" id="headertopin" class="tableBackground">&nbsp;</td>
    <td id="headerrightout">&nbsp;</td>
  </tr>
  <tr valign="top">
    <td id="headerleftout">&nbsp;</td>
    <td id="headerleftin" class="tableBackground" style="text-align:center">
      {$menu}
      <br /><br />
    </td>
    <td id="headerrightin" class="tableBackground">
      <div class="rbroundbox"><div class="rbtop"><div></div></div><div class="rbcontent">
      <table cellspacing="0" cellpadding="2" width="100%">
        <tbody>
        <tr>
          <td align="middle">
            <div id="blocCalls" style="float:right"></div>
            <div id="pagetitle" class="pagetitle" style="float:left">{if $pagetitle}{$pagetitle|sslash}{/if}</div>
            {if $othertitle}<div style="clear:both;margin-left:10px;">{$othertitle|sslash}{/if}</div>
            <p>&nbsp;</p>
            <div id="mainbody" style="clear:both">

