{config_load file=$userLanguage|cat:"/insert_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

<script language="javascript" src="../lib/js/functions.js"></script>
<script language="javascript" src="../lib/js/calendar.js"></script>
<script language="javascript" src="../lib/js/phpTab2js.js"></script>

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<form name="form" method="post" enctype="multipart/form-data">
<input type="hidden" name="cancel" value="false" />
<input type="hidden" name="save" value="false" />
<input type="hidden" name="country" value="{$country}" />
<input type="hidden" name="addImage" value="false" />
<input type="hidden" name="deleteImage" value="" />
<input type="hidden" name="deleteMap" value="" />
<input type="hidden" name="editImage" value="" />
<input type="hidden" name="rotateImage" value="" />
<input type="hidden" name="moveLeftImage" value="" />
<input type="hidden" name="moveRightImage" value="" />
<input type="hidden" name="search" value="" />
<input type="hidden" name="category" value="{$category}" />
<input type="hidden" name="id" value="{$dest->id}" />
<input type="hidden" name="init_{$pageName}" value="true">

<p class="formTitle">{#infoDest#}</p>

<fieldset>
  <br />
  <label style="width:200px">{#country#}</label>
  {$country_name}
  <br /><br />

  <label style="width:200px">{#searchCity#}</label>
  <input type="text" name="searchCity" />
  <input type="submit" value="{#search#}" onclick="form.search.value='true'" size="30" maxlength="30" />
  <br /><br />

  {if $tableRegion}
  <label style="width:200px">{#region#}</label>
  {html_select obj=$tableRegion name='region' url='#' id='admin' value='name' default='Veuillez sélectionner une région' selected=$dest->region}
  <input type="image" src="{$picPath}admin/refresh.gif" title="{#inforefreshlist#}" class="imgButton" class="middle" />
  <br /><br />
  {/if}

  {if $tableCity}
  <label style="width:200px">{#city#}</label>
  {html_select obj=$tableCity name='city' id='id' value='name' default='Veuillez sélectionner une ville' selected=$dest->city}
  <br /><br />
  {/if}

  <label style="width:200px">{#cityifnotfound#}</label>
  <input type="text" name="cityName" value="{$dest->cityName|sslash}" />
  <br /><br />

  <label style="width:200px">{#arrivalDate#}</label>
  <input type="text" name="startingDate" value="{$dest->startingDate|fdate}" readonly="readonly" size="20" maxlength="20" />
  <a href="javascript:cal1.popup();"><img src="{$picPath}calendar/cal.gif" border="0" title="{#infocalendar#}" width="16" height="16" class="middle" /></a>
  <br /><br />

  <label style="width:200px">{#nbDays#}</label>
  {$tableNbDays}
  <br /><br />

  <label style="width:200px">{#arrivalBy#}</label>
  {html_select tab=$tableArrivalBy name='arrivalBy' selected=$dest->arrivalBy}
  <br /><br />

  <label style="width:200px">{#map#}</label>
  {$displayMap}
  {if $mapKey}
  &nbsp;<img src="{$picPath}admin/delete.gif" style="cursor:pointer" class="middle" onclick="createCustomConfirm('{#areyousure#} {$map->name} ?');
  document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteMap.value='{$mapKey}';form.submit();{literal}}{/literal}" />
  {/if}
  <br /><br />

  <label style="width:200px">{#showmap#}</label>
  <input type="checkbox" name="showmap" id="showmap" {if $dest->showmap}checked="checked"{/if} />
  <br /><br />

  <label style="width:200px">{#description#}</label>
  <textarea name="text" id="text" cols="50" rows="6">{$dest->text|sslash}</textarea>
  <br /><br />
</fieldset>

<br /><br />
<a name="middlePage"></a>

{if ($dest->images)}
<table border="0" width="100%">
  {foreach key=keyImage item=image from=$dest->images name=foo}
    {if ($smarty.foreach.foo.index % 3 == 0)}
    <tr>
      {/if}
      <td align="center">
        <table border="0" cellpadding="0" cellspacing="0" >
          <tr>
            <td align="center">
              <img src="{$image->temp}{if ($keyImage|count_characters)==4}{$image->name}{$image->noCache}{else}{$keyImage}.tmp{$image->noCache}{/if}" height="60" />
            </td>
          </tr>
          <tr>
            <td align="center">
              <table border="0" cellpadding="0" cellspacing="0" width="100%" bgColor="#DDDDDD">
                <tr>
                  <td align="left">
                    <input type="image" src="{$picPath}admin/left.gif" title="{#infomovebackpics#}" class="imgButton" onclick="form.moveLeftImage.value='{$keyImage}';form.action='#middlePage'" class="middle" />
                  </td>
                  <td align="center">
                    <input type="image" src="{$picPath}admin/rotate.gif" title="{#inforotatepics#}" class="imgButton" onclick="form.rotateImage.value='{$keyImage}';form.action='#middlePage'" class="middle" />
                  </td>
                  <td align="center">
                    <input type="image" src="{$picPath}admin/update.gif" title="{#infoeditpics#}" class="imgButton" onclick="form.editImage.value='{$keyImage}';form.action='#middlePage'" class="middle" />
                  </td>
                  <td align="center">
                    <img src="{$picPath}admin/delete.gif" title="{#infodeletepics#}" class="middle" style="cursor:pointer" onclick="createCustomConfirm('{#areyousure#} {$image->name} ?');
                    document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteImage.value='{$keyImage}';form.action='#middlePage';form.submit();{literal}}{/literal}" />
                  </td>
                  <td align="right">
                    <input type="image" src="{$picPath}admin/right.gif" title="{#infomovefwdpics#}" class="imgButton" onclick="form.moveRightImage.value='{$keyImage}';form.action='#middlePage'" />
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          {if $image->description<>''}
          <tr>
            <td align="center">
              {$image->description|sslash|shorten}
            </td>
          </tr>
          {/if}
        </table>
        <br />
      </td>
    {if ($smarty.foreach.foo.index % 3 == 2 || $smarty.foreach.foo.index==($dest->images|@count)-1)}
    </tr>
    {/if}
  {/foreach}
</table>
{/if}

<input type="submit" value="{#insertPicture#}" onclick="form.addImage.value='true';form.action='#downPage'" />

{if ($addImage=='true')}
<br /><br />
<fieldset>
  <br />
  <label style="width:200px">{#pictureName#}</label>
  {$imgDest}
  <br /><br />

  <label style="width:200px">{#description#}</label>
  <textarea name="imageDescription" id="text" cols="50" rows="2"></textarea>
  <br /><br /><br />
  <p align="center"><input type="submit" value="{#addPicture#}" onclick="form.addImage.value='save';form.action='#middlePage'" /></p>
  <br />
</fieldset>

{elseif ($addImage=='modify')}
<br /><br />
<fieldset>
  <br />
  <label style="width:200px">{#pictureName#}</label>
  <input type="hidden" name="imageFromAlbum" id="imageFromAlbum" value="{$picUpdate->temp}" />
  {$picUpdate->name}
  <br /><br />

  <label style="width:200px">{#description#}</label>
  <textarea name="imageDescription" id="text" cols="50" rows="2">{$picUpdate->description|sslash}</textarea>
  <br />
  <p align="center"><input type="submit" value="{#updatePicture#}" onclick="form.addImage.value='update';form.action='#middlePage'" /></p>
  <br />

</fieldset>
{/if}

<br /><br />

<table width="100%">
  <tr>
    <td align="center">
      <input type="submit" value="{#cancel#}" onclick="form.cancel.value='true'" />
    </td>
    <td align="center">
     <input type="submit" value="{#save#}" onclick="form.save.value='true';form.action='#downPage'" />
    </td>
  </tr>
</table>

</form>

<script language="JavaScript">
<!-- // create calendar object(s) just after form tag closed
  var cal1 = new calendar1(document.forms['form'].elements['startingDate'], '', '{$dateFormat}', document.forms['form']);
  cal1.year_scroll = true;
  cal1.time_comp = false;
//-->
</script>

<a name="downPage"></a>

{$showFooter}
