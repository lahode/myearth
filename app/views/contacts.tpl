{config_load file=$userLanguage|cat:"/contacts_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<form name="form" method="post">
<input type="hidden" name="deleteContact" />
<input type="hidden" name="selectAll" value="false" />
<input type="hidden" name="exportContacts" value="false" />
<input type="hidden" name="deleteContacts" value="false" />
<input type="hidden" name="printContacts" value="false" />
<input type="hidden" name="addNews" />
<input type="hidden" name="init_{$pageName}" value="true">

<table cellspacing="0" cellpadding="0" width="100%" border="0">
  <tr>
    <td>
      <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tbody>
          <tr>
            <td width="28" class="sitetext">{if $display.filter<>'no' && $display.filter<>''}<a href="#" onclick="form.action='?filter=no';form.submit();">{#all#}</a>{else}{#all#}{/if}</td>
            <td>&nbsp;</td>
            <td noWrap width="28" class="sitetext">{if $display.filter<>'num'}<a href="#" onclick="form.action='?filter=num';form.submit();">0-9</a>{else}0-9{/if}</td>
            {foreach from=$abcList item=letter}
            <td>&nbsp;</td>
            <td width="28" class="sitetext">{if $display.filter<>$letter}<a href="#" onclick="form.action='?filter={$letter}';form.submit();">{$letter}</a>{else}{$letter}{/if}</td>
            {/foreach}
          </tr>
        </tbody>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tbody>
          <tr>
            <td class="sitetitle" style="BORDER-BOTTOM: #b3c0d0 1px solid" align=middle width="18" height="18">
              <a href="javascript:toggleContact(document.form.elements, 'contacts[]')"><img src="{$picPath}admin/checkall.gif" border="0" /></a>
            </td>
            <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18">
              {#name#}&nbsp;{if $display.sortBy<>'nickname' || $display.sortOrder<>'asc'}<img src="{$picPath}admin/sortasc.gif" onclick="form.action='?contact_sort_by=nickname&contact_sort_order=asc';form.submit();" style="cursor:pointer" />{else}<img src="{$picPath}admin/sortasc.gif" />{/if}
                                {if $display.sortBy<>'nickname' || $display.sortOrder<>'desc'}<img src="{$picPath}admin/sortdesc.gif" onclick="form.action='?contact_sort_by=nickname&contact_sort_order=desc';form.submit();" style="cursor:pointer" />{else}<img src="{$picPath}admin/sortdesc.gif" />{/if}
            </td>
            <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18">
              {#email#}&nbsp;{if $display.sortBy<>'email' || $display.sortOrder<>'asc'}<img src="{$picPath}admin/sortasc.gif" onclick="form.action='?contact_sort_by=email&contact_sort_order=asc';form.submit();" style="cursor:pointer" /></a>{else}<img src="{$picPath}admin/sortasc.gif" />{/if}
                         {if $display.sortBy<>'email' || $display.sortOrder<>'desc'}<img src="{$picPath}admin/sortdesc.gif" onclick="form.action='?contact_sort_by=email&contact_sort_order=asc';form.submit();" style="cursor:pointer" /></a>{else}<img src="{$picPath}admin/sortdesc.gif" />{/if}
            </td>
            <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18">
              {#action#}&nbsp;
            </td>
          </tr>
          {foreach from=$contacts key=keyContact item=itemContact}
          <tr onmouseover="this.bgColor='#B3C0D0'" onmouseout="this.bgColor=''">
            <td valign="top" height="21">
              <input type="hidden" value="{$itemContact->id}" name="contactshidden[]">
              <input type="checkbox" value="{$itemContact->id}" {if ($contactSelected.$keyContact==1)}checked="checked" {/if}name="contacts[]">
            </td>
            <td class="sitetext" style="cursor: pointer" onclick="go('{#PAGE_CONTACTDETAILS#}?id={$itemContact->id}&actions=view')" valign="top" height="21">
              {$itemContact->nickname}
            </td>
            <td class="sitetext" style="cursor: pointer" onclick="go('{#PAGE_CONTACTDETAILS#}?id={$itemContact->id}&actions=view')" valign="top" height="21">
               {$itemContact->email}
            </td>
            <td class="sitetext" height="21">
              {if $itemContact->email}<a href="{#PAGE_COMPOSE#}?to={$itemContact->email}"><img title="{#infosendmsg#}" src="{$picPath}admin/newmail.gif" width="16" /></a>{else}<img src="{$picPath}transparent.gif" width="16" />{/if}
              <a href="{#PAGE_CONTACTDETAILS#}?id={$itemContact->id}&actions=view"><img title="{#infoviewcontact#}" src="{$picPath}admin/viewcontact.gif" /></a>
              <a href="{#PAGE_CONTACTDETAILS#}?id={$itemContact->id}&actions=edit"><img title="{#infoeditcontact#}" src="{$picPath}admin/update.gif" /></a>
              <img title="{#infodeletecontact#}" style="cursor:pointer" src="{$picPath}admin/delete.gif" onclick="createCustomConfirm('{#areyousure#} {$itemContact->nickname} ?');
              document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteContact.value='{$itemContact->id}';form.submit();{literal}}{/literal}" />
              {if $itemContact->email}
              <a href="{#PAGE_COMPOSE#}?request={$itemContact->id}"><img title="{#inforefreshcontact#}" src="{$picPath}admin/refresh.gif"></a>
              <img title="{#infosendcontacttonews#}" style="cursor:pointer" src="{$picPath}news.gif" onclick="createCustomConfirm('{#adding#} {$itemContact->nickname} {#tonewsletter#}?');
              document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.addNews.value='{$itemContact->id}';form.submit();{literal}}{/literal}" />
              {/if}
            </td>
          </tr>
          {/foreach}
        </tbody>
      </table>
    </td>
  </tr>
</table>

{if $splitDisplay->total > $splitDisplay->length}
  <div class="navigation">
  {if $splitDisplay->start > 0}
    <img height="12" src="{$picPath}admin/leftarrow.gif" width="12" align="absmiddle" onclick="form.action='?next={$splitDisplay->start-$splitDisplay->maxNum}';form.submit();" style="cursor:pointer" />&nbsp;
  {/if}
  {section name=foo start=$splitDisplay->start loop=$splitDisplay->loop max=$splitDisplay->maxNum}
    {if ($smarty.section.foo.index==$splitDisplay->first)}
      {$smarty.section.foo.index+1}
    {else}
      <a href="#" onclick="form.action='?page={$smarty.section.foo.index*$splitDisplay->length}&next={$splitDisplay->start}';form.submit();">{$smarty.section.foo.index+1}<a>
    {/if}
  {/section}
  {if $splitDisplay->start+$splitDisplay->maxNum < $splitDisplay->loop}
    &nbsp;<img height="12" src="{$picPath}admin/rightarrow.gif" width="12" align="absmiddle"onclick=" form.action='?next={$splitDisplay->start+$splitDisplay->maxNum}';form.submit();" style="cursor:pointer" />
  {/if}
  </div>
{/if}

<br />
<table width="100%" align="left">
  <tr>
    <td align="center">
      <img src="{$picPath}admin/selectall.gif" title="{#infoselectallcontacts#}" border="0" style="cursor:pointer" onclick="form.selectAll.value='true';form.submit();" /></a>
    </td>
    <td align="center">
      <img title="{#infodeleteselected#}" style="cursor:pointer" src="{$picPath}admin/delete.gif" onclick="if (checkSelection(document.form.elements, 'contacts[]')) {literal}{{/literal}createCustomConfirm('{#areyousure#} {#selectedContacts#} ?');
             document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteContacts.value='true';form.submit();{literal}}}{/literal} else alert ('{#errorselectedcontacts#}', 'errorAlert');" />
    </td>
    <td align="center">
      <img title="{#infoprintselected#}" src="{$picPath}admin/print.gif" style="cursor:pointer" onclick="if (checkSelection(document.form.elements, 'contacts[]')) {literal}{{/literal}form.printContacts.value='true';form.submit();window.open('{#PAGE_PRINT#}?print=selectedcontacts', null, 'height=480,width=640,status=yes,toolbar=no,menubar=yes,location=no,scrollbars=yes');{literal}}{/literal}else alert ('{#errorselectedcontacts#}', 'errorAlert');" />
    </td>
    <td align="right" width="70%">
      &nbsp;&nbsp;&nbsp;&nbsp;
    </td>
    <td align="left">
      <input type="button" value="{#newContact#}" onclick="go('{#PAGE_CONTACTDETAILS#}?id=-1&actions=edit')" />
    </td>
    <td align="left">
      <input type="button" value="{#newEmail#}" onclick="go('{#PAGE_COMPOSE#}')" />
    </td>
  </tr>
  <tr>
    <td colspan="3">
      &nbsp;
    </td>
    <td align="right" width="70%">
      &nbsp;&nbsp;&nbsp;&nbsp;
    </td>
    <td align="left">
      <input type="button" value="{#importContact#}" onclick="window.open('{#PAGE_IMPORT#}?print=selectedcontacts', null, 'height=768,width=1024,status=yes,toolbar=no,menubar=yes,location=no,scrollbars=yes');" />
    </td>
    <td align="left">
      <input type="button" value="{#exportContact#}" onclick="if (checkSelection(document.form.elements, 'contacts[]')) {literal}{{/literal}form.exportContacts.value='true';form.action='?exportContacts=true';form.submit();{literal}}{/literal}else alert ('{#errorselectedcontacts#}', 'errorAlert');" />
    </td>
  </tr>
</table>
<br />
</form>

{$showFooter}
