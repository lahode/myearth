{config_load file=$userLanguage|cat:"/notebook_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<form name="form" method="post" ENCTYPE=multipart/form-data>
<input type="hidden" name="actions" value="{$action}">
<input type="hidden" name="editMessage">
<input type="hidden" name="deleteMessage">
<input type="hidden" name="changeAccess">
<input type="hidden" name="newdate" value="{$newMessage->date}">

<input type="hidden" name="deleteImage" value="" />
<input type="hidden" name="addImage" value="false" />
<input type="hidden" name="cancel" value="false" />
<input type="hidden" name="save" value="false" />
<input type="hidden" name="init_{$pageName}" value="true">

<div style="position:relative;width: 550px; height: 724px;background-image: url({$picPath}carnet.gif);">
<br />
<br />

{* Split display for titles *}
<div style="position:absolute;top:10px;left:80px;width: 420px;margin-left:0px;text-align:left">
  <table width="100%">
    <tr>
      <td align="left">
        <a href="?"><img src="{$picPath}home.gif" /><a>
        {if $splitDisplay->total > $splitDisplay->length}
        &nbsp;{if $splitDisplay->start > 0}<a href="?next={$splitDisplay->start-$splitDisplay->maxNum}"><img src="{$picPath}lastpage.gif" /><a>&nbsp;{/if}
        {/if}
      </td>
      {if $splitPage->total > $splitPage->length}
      <td align="left">
        {if $splitPage->start > 0}<a href="?nextpage={$splitPage->start-$splitPage->maxNum}{$next}"><img src="{$picPath}admin/left.gif" /><a>&nbsp;{/if}
        {if $splitPage->start+$splitPage->maxNum < $splitPage->loop}&nbsp;<a href="?nextpage={$splitPage->start+$splitPage->maxNum}{$next}"><img src="{$picPath}admin/right.gif" /><a>{/if}
      </td>
      {/if}
      {if $splitDisplay->total > $splitDisplay->length}
      <td align="right">
        {if $splitDisplay->start+$splitDisplay->maxNum < $splitDisplay->loop}&nbsp;<a href="?next={$splitDisplay->start+$splitDisplay->maxNum}"><img src="{$picPath}nextpage.gif" /><a>{/if}
      </td>
      {/if}
    </tr>
  </table>
</div>

{if $titles}
<div style="position:absolute;width: 450px;left:80px;right:20px;text-align:left">
<p class="sitetitle" style="font-size:18px;text-align:center">Table des mati�res</p>
<table width="100%">
{foreach from=$titles key=keytitle item=itemTitle}
  <tr>
    <td class="sitetitle" colspan="2">
      {$itemTitle->date|showdate}
    </td>
  </tr>
  <tr>
    <td class="persotext" style="cursor:pointer" onclick="go('?next={$keytitle*$splitDisplay->length}')">
      {$itemTitle->subject|sslash}
    </td>
    <td class="sitetitle" style="text-align:right">
      <a href="?next={$keytitle*$splitDisplay->length}">{$keytitle+1}<a>
    </td>
  </tr>
{/foreach}
</table>
</div>
{/if}

{if $message}
<div style="position:absolute;width: 450px;left:80px;right:20px;text-align:left">
  <p class="sitetitle">
    {if ($admin)}
    {if ($message->access=='public')}
    &nbsp;<input type="image" src="{$picPath}admin/enable.gif" class="imgButton" onclick="form.changeAccess.value='{$message->id}'" />
    {else}
    &nbsp;<input type="image" src="{$picPath}admin/disable.gif" class="imgButton" onclick="form.changeAccess.value='{$message->id}'" />
    {/if}
    {/if}
    {if ($admin)}
    &nbsp;<input type="image" src="{$picPath}admin/update.gif" class="imgButton" onclick="form.editMessage.value='{$message->id}'" />
    &nbsp;<img src="{$picPath}admin/delete.gif" style="cursor:pointer" class="middle" onclick="createCustomConfirm('{#areyousure#} {$message->subject} ?');
    document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteMessage.value='{$message->id}';
    form.submit();{literal}}{/literal}" />
    {/if}
  </p>
  <span class="sitetitle">{$message->date|showdate} : </span><span class="persotext">{$message->subject|sslash}</span>
  <p class="persotext">{$message->text|sslash|nl2br}</p>
  {if $message->images}
  <table cellpadding="0" cellspacing="0" align="center">
  {foreach from=$message->images key=keyImage item=image}
    {if ($keyImage % 2 == 0)}
    <tr>
    {/if}
      <td align="center">
        <table border="0">
          <tr valign="middle">
            <td align="center">
              <img src="{$imgPath}{$image->name}" height="160" />
            </td>
          </tr>
          <tr>
            <td align="center" style="font-size:11px">
              {$image->description|sslash|nl2br}
              <br />
            </td>
          </tr>
        </table>
      </td>
    {if ($keyImage % 2 == 2 || $keyImage==($message->images|@count)-1)}
    </tr>
    {/if}
  {/foreach}
  </table>
  {/if}
</div>
{/if}

{* Split display for titles *}
<div style="position:absolute;top:670px;left:80px;width: 420px;margin-left:0px;text-align:left">
  <table width="100%">
    <tr>
      <td align="left">
        <a href="?"><img src="{$picPath}home.gif" /><a>
        {if $splitDisplay->total > $splitDisplay->length}
        &nbsp;{if $splitDisplay->start > 0}<a href="?next={$splitDisplay->start-$splitDisplay->maxNum}"><img src="{$picPath}lastpage.gif" /><a>&nbsp;{/if}
        {/if}
      </td>
      {if $splitPage->total > $splitPage->length}
      <td align="left">
        {if $splitPage->start > 0}<a href="?nextpage={$splitPage->start-$splitPage->maxNum}{$next}"><img src="{$picPath}admin/left.gif" /><a>&nbsp;{/if}
        {if $splitPage->start+$splitPage->maxNum < $splitPage->loop}&nbsp;<a href="?nextpage={$splitPage->start+$splitPage->maxNum}{$next}"><img src="{$picPath}admin/right.gif" /><a>{/if}
      </td>
      {/if}
      {if $splitDisplay->total > $splitDisplay->length}
      <td align="right">
        {if $splitDisplay->start+$splitDisplay->maxNum < $splitDisplay->loop}&nbsp;<a href="?next={$splitDisplay->start+$splitDisplay->maxNum}"><img src="{$picPath}nextpage.gif" /><a>{/if}
      </td>
      {/if}
    </tr>
  </table>
</div>

</div>

{if $admin}
<a name="textentry"></a>

<p class="formTitle">{if $action=='new'}{#newMessage#}{else}{#editMessage#}{/if}</p>

<fieldset>
  <br />
  <input type="hidden" name="id" value="{$newMessage->id}" />
  <label style="width:100px">{#subject#}</label>
  <input type="text" name="subject" value="{$newMessage->subject|sslash}" />
  <br /><br />

  <label style="width:100px">{#message#}</label>
  <textarea name="text" id="text" cols="70" rows="7"
    onselect="storeCaret(this);"
    onclick="storeCaret(this);"
    onkeyup="storeCaret(this);">{$newMessage->text|sslash}</textarea>
  <br /><br />

  {$showEmote}
  <br /><br />
</fieldset>

<fieldset>
  <label style="width:200px">{#pictureName#}<br />{#max4img#}</label>
  {$imgNotebook}
  <br /><br />

  <label style="width:200px">{#descriptionImg#}</label>
  <textarea name="imageDescription" id="imageDescription" cols="50" rows="2"></textarea>
  <br /><br /><br />
  <p align="center"><input type="submit" value="{#addPicture#}" onclick="form.addImage.value='save';form.action='#textentry';" /></p>
</fieldset>

{if ($newMessage->images)}
<table width="100%">
  <tr>
  {foreach key=keyImage item=image from=$newMessage->images}
    <td align="center">
      <table border="0">
        <tr valign="middle">
          <td>
            <img src="{$image->temp}{if ($keyImage|count_characters)==4}{$image->name}{else}{$keyImage}.tmp{/if}" height="60" />
          </td>
          <td>
            &nbsp;<img src="{$picPath}admin/delete.gif" style="cursor:pointer" class="middle" onclick="createCustomConfirm('{#areyousure#} {$image->name} ?');
            document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteImage.value='{$keyImage}';form.submit();{literal}}{/literal}" />
          </td>
        </tr>
        <tr>
          <td align="center" colspan="2" style="font-size:9px">
            {$image->description|sslash}
            <br />
          </td>
        </tr>
      </table>
    </td>
  {/foreach}
  </tr>
</table>
{/if}
<br />
<table width="100%">
  <tr>
    <td align="center">
      <input type="submit" value="{#cancel#}" onclick="form.cancel.value='true'" />
    </td>
    <td align="center">
      <input type="submit" value="{#save#}" onclick="form.save.value='true'" />
    </td>
  </tr>
</table>
{/if}

</form>

{$showFooter}
