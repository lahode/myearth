{config_load file=$userLanguage|cat:"/mailbox_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<form name="form" method="post">
<input type="hidden" name="deleteMsg" />
<input type="hidden" name="restoreMsg" />
<input type="hidden" name="cleanMsg" />
<input type="hidden" name="selectAll" value="false" />
<input type="hidden" name="deleteMsgs" value="false" />
<input type="hidden" name="restoreMsgs" value="false" />
<input type="hidden" name="cleanMsgs" value="false" />
<input type="hidden" name="printMsgs" value="false" />
<input type="hidden" name="showTrash" value="{$showTrash}" />
<input type="hidden" name="addNews" />
<input type="hidden" name="actions" />
<input type="hidden" name="synch" />
<input type="hidden" name="view" />
<input type="hidden" name="box" />
<input type="hidden" name="save" />
<input type="hidden" name="cancel" />
<input type="hidden" name="init_{$pageName}" value="true">

<table cellspacing="0" cellpadding="0" width="100%" border="0">
  <tr>
    <td>
      <span class="sitetitle">{#switchTo#}</span> <span class="sitetext">{$boxName}</span>
    </td>
    <td align="right">
      <span class="sitetitle">{#mailbox#}</span> <span class="sitetext">{html_select obj=$mailboxes name='mailbox' url="#" value='fullname' id='name' selected=$defaultMailbox}&nbsp;</span>
      {if ($defaultMailbox<>'default') && $box=='in'}
      <img style="cursor:pointer" title="{#synchMailbox#}" src="{$picPath}mailbox.gif" class="middle" onclick="displayLoading('{#waitloading#}');form.synch.value='true';form.submit();" />
      {/if}
    </td>
  </tr>
</table>
<br />
<table cellspacing="0" cellpadding="0" width="100%" border="0">
  <tr>
    <td>
      <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tbody>
          <tr>
            <td class="sitetitle" style="BORDER-BOTTOM: #b3c0d0 1px solid" align=middle width="18" height="18">
              <a href="javascript:toggleContact(document.form.elements, 'msgs[]')"><img src="{$picPath}admin/checkall.gif" border="0" /></a>
            </td>
            <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18">
              {if $box=='out'}{#to#}{else}{#from#}{/if}&nbsp;{if $display.sortBy<>'name' || $display.sortOrder<>'asc'}<img src="{$picPath}admin/sortasc.gif" onclick="form.action='?msg_sort_by=name&msg_sort_order=asc&box={$box}';form.submit();" style="cursor:pointer" />{else}<img src="{$picPath}admin/sortasc.gif" />{/if}
                                {if $display.sortBy<>'name' || $display.sortOrder<>'desc'}<img src="{$picPath}admin/sortdesc.gif" onclick="form.action='?msg_sort_by=name&msg_sort_order=desc&box={$box}';form.submit();" style="cursor:pointer" />{else}<img src="{$picPath}admin/sortdesc.gif" />{/if}
            </td>
            <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18">
              {#subject#}&nbsp;{if $display.sortBy<>'subject' || $display.sortOrder<>'asc'}<img src="{$picPath}admin/sortasc.gif" onclick="form.action='?msg_sort_by=subject&msg_sort_order=asc&box={$box}';form.submit();" style="cursor:pointer" /></a>{else}<img src="{$picPath}admin/sortasc.gif" />{/if}
                         {if $display.sortBy<>'subject' || $display.sortOrder<>'desc'}<img src="{$picPath}admin/sortdesc.gif" onclick="form.action='?msg_sort_by=subject&msg_sort_order=desc&box={$box}';form.submit();" style="cursor:pointer" /></a>{else}<img src="{$picPath}admin/sortdesc.gif" />{/if}
            </td>
            <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18">
              {#date#}&nbsp;{if $display.sortBy<>'date' || $display.sortOrder<>'asc'}<img src="{$picPath}admin/sortasc.gif" onclick="form.action='?msg_sort_by=date&msg_sort_order=asc&box={$box}';form.submit();" style="cursor:pointer" /></a>{else}<img src="{$picPath}admin/sortasc.gif" />{/if}
                         {if $display.sortBy<>'date' || $display.sortOrder<>'desc'}<img src="{$picPath}admin/sortdesc.gif" onclick="form.action='?msg_sort_by=date&msg_sort_order=desc&box={$box}';form.submit();" style="cursor:pointer" /></a>{else}<img src="{$picPath}admin/sortdesc.gif" />{/if}
            </td>
            <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18">
              {#action#}&nbsp;
            </td>
          </tr>
          {foreach from=$msgs key=keyMsg item=itemMsg}
          <tr onmouseover="this.bgColor='#B3C0D0'" onmouseout="this.bgColor=''">
            <td valign="top" height="21">
              <input type="hidden" value="{$itemMsg->id}" name="msgshidden[]">
              <input type="checkbox" value="{$itemMsg->id}" {if ($msgSelected.$keyMsg==1)}checked="checked" {/if}name="msgs[]">
            </td>
            <td class="sitetext" style="cursor: pointer" onclick="form.box.value='{$box}';form.view.value='{$itemMsg->id}';form.submit();" valign="top" height="21">
              {if $itemMsg->attachments}<img src="{$picPath}admin/attachment.gif" />{/if}
              {$itemMsg->from}
            </td>
            <td class="sitetext" style="cursor: pointer" onclick="form.box.value='{$box}';form.view.value='{$itemMsg->id}';form.submit();" valign="top" height="21">
               {$itemMsg->subject}
            </td>
            <td class="sitetext" style="cursor: pointer" onclick="form.box.value='{$box}';form.view.value='{$itemMsg->id}';form.submit();" valign="top" height="21">
               {$itemMsg->date|fdate}
            </td>
            <td class="sitetext" height="21">
              {if !$showTrash}
              {if $box=='in'}<a href="{#PAGE_COMPOSE#}?actions=reply&box={$box}&id={$itemMsg->id}"><img title="{#inforeplymsg#}" src="{$picPath}admin/reply.gif" width="16" /></a>{/if}
              <a href="{#PAGE_COMPOSE#}?actions=fwd&box={$box}&id={$itemMsg->id}"><img title="{#infoforwardmsg#}" src="{$picPath}admin/forward.gif" width="16" /></a>
              <img title="{#infodeletemsg#}" style="cursor:pointer" src="{$picPath}admin/delete.gif" onclick="createCustomConfirm('{#areyousure#} {#msgof#} {$itemMsg->name} ?');
              document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteMsg.value='{$itemMsg->id}';form.submit();{literal}}{/literal}" />
              {else}
              <img title="{#inforestoremsg#}" style="cursor:pointer" src="{$picPath}admin/restore.gif" onclick="createCustomConfirm('{#areyousurerestore#} {#msgof#} {$itemMsg->name} ?');
              document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.restoreMsg.value='{$itemMsg->id}';form.submit();{literal}}{/literal}" />
              <img title="{#infocleanmsg#}" style="cursor:pointer" src="{$picPath}admin/empty.gif" onclick="createCustomConfirm('{#areyousureclean#} {#msgof#} {$itemMsg->name} ?');
              document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.cleanMsg.value='{$itemMsg->id}';form.submit();{literal}}{/literal}" />
              {/if}
            </td>
          </tr>
          {/foreach}
        </tbody>
      </table>
    </td>
  </tr>
</table>

{if $splitDisplay->total > $splitDisplay->length}
  <div class="navigation">
  {if $splitDisplay->start > 0}
    <img height="12" src="{$picPath}admin/leftarrow.gif" width="12" align="absmiddle" onclick="form.action='?next={$splitDisplay->start-$splitDisplay->maxNum}&box={$box}';form.submit();" style="cursor:pointer" />&nbsp;
  {/if}
  {section name=foo start=$splitDisplay->start loop=$splitDisplay->loop max=$splitDisplay->maxNum}
    {if ($smarty.section.foo.index==$splitDisplay->first)}
      {$smarty.section.foo.index+1}
    {else}
      <a href="#" onclick="form.action='?page={$smarty.section.foo.index*$splitDisplay->length}&next={$splitDisplay->start}&box={$box}';form.submit();">{$smarty.section.foo.index+1}<a>
    {/if}
  {/section}
  {if $splitDisplay->start+$splitDisplay->maxNum < $splitDisplay->loop}
    &nbsp;<img height="12" src="{$picPath}admin/rightarrow.gif" width="12" align="absmiddle"onclick=" form.action='?next={$splitDisplay->start+$splitDisplay->maxNum}&box={$box}';form.submit();" style="cursor:pointer" />
  {/if}
  </div>
{/if}

<br />
<table width="100%">
  <tr>
    <td align="center">
      <img src="{$picPath}admin/selectall.gif" title="{#infoselectallmsg#}" border="0" style="cursor:pointer" onclick="form.selectAll.value='true';form.submit();" /></a>
    </td>
    {if !$showTrash}
    <td align="center">
      <img title="{#infodeleteselected#}" style="cursor:pointer" src="{$picPath}admin/delete.gif" onclick="if (checkSelection(document.form.elements, 'msgs[]')) {literal}{{/literal}createCustomConfirm('{#areyousure#} {#selectedmsg#} ?');
             document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteMsgs.value='true';form.submit();{literal}}{/literal}{literal}}{/literal} else alert ('{#errorselectedmsg#}', 'errorAlert');" />
    </td>
    <td align="center">
      <img title="{#infoprintselected#}" src="{$picPath}admin/print.gif" style="cursor:pointer" onclick="if (checkSelection(document.form.elements, 'msgs[]')) {literal}{{/literal}form.printMsgs.value='true';form.submit();window.open('{#PAGE_PRINT#}?print=selectedemails&box={$box}', null, 'height=480,width=640,status=yes,toolbar=no,menubar=yes,location=no,scrollbars=yes');{literal}}{/literal}else alert ('{#errorselectedmsg#}', 'errorAlert');" />
    </td>
    <td align="center">
      <img title="{#infotrash#}" src="{$picPath}admin/trash.gif" style="cursor:pointer" onclick="form.showTrash.value=1;form.submit();" />
    </td>
    <td align="right" width="70%">
      &nbsp;&nbsp;&nbsp;&nbsp;
    </td>
    <td align="right">
      <input type="button" value="{#newEmail#}" onclick="go('{#PAGE_COMPOSE#}?box={$box}')" />
    </td>
    {else}
    <td align="center">
      <img title="{#inforestoreselected#}" style="cursor:pointer" src="{$picPath}admin/restore.gif" onclick="if (checkSelection(document.form.elements, 'msgs[]')) {literal}{{/literal}createCustomConfirm('{#areyousurerestore#} {#selectedmsg#} ?');
             document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.restoreMsgs.value='true';form.submit();{literal}}{/literal}{literal}}{/literal} else alert ('{#errorselectedmsg#}', 'errorAlert');" />
    </td>
    <td align="center">
      <img title="{#infocleanselected#}" style="cursor:pointer" src="{$picPath}admin/empty.gif" onclick="if (checkSelection(document.form.elements, 'msgs[]')) {literal}{{/literal}createCustomConfirm('{#areyousureclean#} {#selectedmsg#} ?');
             document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.cleanMsgs.value='true';form.submit();{literal}}{/literal}{literal}}{/literal} else alert ('{#errorselectedmsg#}', 'errorAlert');" />
    </td>
    <td align="center">
      <img title="{#infotrash#}" src="{$picPath}admin/notrash.gif" style="cursor:pointer" onclick="form.showTrash.value=0;form.submit();" />
    </td>
    <td width="95%">
      &nbsp;
    </td>
    {/if}
  </tr>
  {if !$showTrash}
  <tr>
    <td colspan="5" align="right">
      {if $defaultMailbox<>'default'}
      <input type="button" value="{#editMailbox#}" onclick="form.actions.value='edit';form.submit();" />
      {/if}
      &nbsp;&nbsp;
    </td>
    <td align="left">
      <input type="button" value="{#newMailbox#}" onclick="form.actions.value='add';form.submit();" />
    </td>
  </tr>
  {/if}
</table>
<br />
<br />
{if $action}
<p class="formTitle">{if $action=="edit"}{#editMailbox#}{else}{#addMailbox#}{/if}</p>
<fieldset>
  <br />
  <label style="width:200px">{#name#}</label>
  <label style="width:250px">{if $action=="edit"}{$mailbox->name}<input type="hidden" name="name" value="{$mailbox->name}" />{else}<input type="text" size="30" maxlength="30" name="name" value="{$mailbox->name}" />{/if}</label>
  <br /><br />
  <label style="width:200px">{#email#}</label>
  <label style="width:250px"><input type="text" size="30" name="email" value="{$mailbox->email}" /></label>
  <br /><br />
  <label style="width:200px">{#host#}</label>
  <label style="width:250px"><input type="text" size="30" name="host" value="{$mailbox->host}" /></label>
  <br /><br />
  <label style="width:200px">{#username#}</label>
  <label style="width:250px"><input type="text" size="30" name="username" value="{$mailbox->username}" /></label>
  <br /><br />
  <label style="width:200px">{#password#}</label>
  <label style="width:250px"><input type="password" size="30" name="password" /></label>
  <br /><br />
</fieldset>
<br />
<table width="100%">
  <tr>
    <td align="center">
      <input type="submit" value="{#cancel#}" onclick="form.cancel.value='true'" />
    </td>
    <td align="center">
      <input type="submit" value="{if $action=="edit"}{#edit#}{else}{#insert#}{/if}" onclick="form.save.value='{$action}'" />
    </td>
  </tr>
</table>
{/if}
</form>

{$showFooter}
