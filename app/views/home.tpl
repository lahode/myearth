{config_load file=$userLanguage|cat:"/home_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

<script language="javascript" src="../lib/js/scrolling.js"></script>

<script type="text/javascript" language="JavaScript">
<!--
//scroller's width
var swidth=530;

//scroller's height
var sheight=20;

//scroller's speed
var sspeed=2;
var rspeed=sspeed;

//scroller's background
sbcolor="";
-->
</script>

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" {if $scrollActive}onload="startScroll('<i>{$scrollMessage}</i>', swidth, sspeed, 0);"{/if}>

{$showHeader}
<form method="post" name="form">
<input type="hidden" name="deleteCurrentTask" value="false" />
<br />
<table cellpadding="0" cellspacing="0" width="80%" align="center">
  <tr>
    <td colspan="2" align="center">
      <p class="txt_medium">{$home->message|sslash}</p>
      <br />
    </td>
  </tr>
  {if ($home->image<>'')}
  <tr>
    <td colspan="2" align="center">
      <img src="{$webImgPath}{$home->image}" border="0" />
    </td>
  </tr>
  {/if}
</table>
</form>

<br /><br />
<p class="txt_medium" align="center">{#youare#} {$counter}{#visitor#}</p>

<div class="sectionP">
{if $elements}
<br />
<table width="100%">
  {foreach from=$elements key=keyElement item=element}
  {if $keyElement % 2 == 0}
  <tr>
  {/if}
    <td align="left">
       <div class="cadresitetitle" onclick="location.href='{$element.link}';" style="cursor: pointer;height: 240px;width: 250px">
       <p class="pagetitle">{$element.title|sslash}</p>
       <p class="sitetitle">{$element.subject|sslash}</p>
       <p class="persotext">{$element.text|sslash}</p>
       {if $element.picture}<p class="persotext" align="center"><img src="{$element.picture}" height="110" /></p>{/if}
       </div>
    </td>
  {if ($keyElement % 2 == 1 || $keyElement==($elements|@count)-1)}
  </tr>
  {/if}
  {/foreach}
</table>
{/if}
</div>

{$showFooter}

{if $alertToDisplay}
<script type="text/javascript">
<!--
  createCustomConfirm('{#alertBefore#}<br />{$alertToDisplay}{#alertAfter#} ?'); document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteCurrentTask.value='true';form.submit();{literal}}{/literal};
-->
</script>
{/if}

