{config_load file=$userLanguage|cat:"/link_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<form name="form" method="post">
<input type="hidden" name="save" value="false" />
<input type="hidden" name="deleteLink" />
<input type="hidden" name="editLink" />
<input type="hidden" name="moveup" />
<input type="hidden" name="movedown" />
<input type="hidden" name="actions" value="{$action}" />
<input type="hidden" name="init_{$pageName}" value="true">

<table cellpadding="0" cellspacing="0" width="80%" align="center" border="0">
  <tbody>
    <tr>
      <td class="sitetitle" style="BORDER-BOTTOM: #b3c0d0 1px solid" align=middle width="18" height="18">
        {#link#}&nbsp;
      </td>
      <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18">
        {#description#}&nbsp;
      </td>
      <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18">
        {#action#}&nbsp;
      </td>
    </tr>
    {foreach from=$links item=itemLink}
    <tr valign="top">
      <td align="left">
        <p class="sitetext"><a href="{$itemLink->link}" target="_blank">{$itemLink->name|sslash}</a></p>
      </td>
      <td align="left">
        <p class="sitetext">{$itemLink->description|shorten|sslash}</p>
      </td>
      <td align="center">
        <a href="#" onclick="form.editLink.value='{$itemLink->name}';form.submit();"><img title="{#infoeditlink#}" src="{$picPath}admin/update.gif"></a>
        <img title="{#infodeletelink#}" src="{$picPath}admin/delete.gif" style="cursor:pointer" class="medium" onclick="createCustomConfirm('{#areyousure#} {$itemLink->name} ?');
        document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteLink.value='{$itemLink->name}';form.submit();{literal}}{/literal}" />
        <a href="#" onclick="form.moveup.value='{$itemLink->name}';form.submit();"><img title="{#infomoveup#}" src="{$picPath}admin/sortasc.gif"></a>
        <a href="#" onclick="form.movedown.value='{$itemLink->name}';form.submit();"><img title="{#infomovedown#}" src="{$picPath}admin/sortdesc.gif"></a>
      </td>
    </tr>
    {/foreach}
  </tbody>
</table>

<br />
<p class="formTitle">{if $action=="edit"}{#editLink#}{else}{#addLink#}{/if}</p>
<fieldset>
  <br />
  <label style="width:80px">{#name#}</label>
  <label style="width:250px">{if $action=="edit"}<input type="hidden" name="name" value="{$link->name}"/>{$link->name}{else}<input type="text" name="name" value="{$link->name}" />{/if}</label>
  <br /><br />
  <label style="width:80px">{#link#}</label>
  <label style="width:250px"><input type="text" size="60" name="link" value="{$link->link}" /></label>
  <br /><br />
  <label style="width:80px">{#description#}</label>
  <label style="width:250px"><input type="text" name="description" size="60" value="{$link->description|sslash}" /></label>
  <br /><br />

  <p align="center"><input type="submit" value="{if $action=="edit"}{#edit#}{else}{#insert#}{/if}" onclick="form.save.value='true'" /></p>
</fieldset>

</form>

{$showFooter}
