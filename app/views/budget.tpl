{config_load file=$userLanguage|cat:"/budget_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<table cellpadding="0" cellspacing="0" width="100%" align="center" border="0">
  <tr>
    <td>
      <p class="sitetitle">{#budAccommodation#}</p>
    </td>
  </tr>
  <tr>
    <td align="center">
      <a href="{#PAGE_BUDGET_ACCOMODATION#}"><img src="{$picPath}accommodation.gif" /></a>
      <p>&nbsp</p>
    </td>
  </tr>
  <tr>
    <td>
      <p class="sitetitle">{#budFood#}</p>
    </td>
  </tr>
  <tr>
    <td align="center">
      <a href="{#PAGE_BUDGET_FOOD#}"><img src="{$picPath}food.gif" /></a>
      <p>&nbsp</p>
    </td>
  </tr>
  <tr>
    <td>
      <p class="sitetitle">{#budTransport#}</p>
    </td>
  </tr>
  <tr>
    <td align="center">
      <a href="{#PAGE_BUDGET_TRANSPORT#}"><img src="{$picPath}transport.gif" /></a>
      <p>&nbsp</p>
    </td>
  </tr>
  <tr>
    <td>
      <p class="sitetitle">{#spending#}</p>
    </td>
  </tr>
  <tr>
    <td align="center">
      <a href="{#PAGE_STATISTICS#}"><img src="{$picPath}chart.gif" /></a>
      <p>&nbsp</p>
    </td>
  </tr>
</table>

{$showFooter}
