{config_load file=$userLanguage|cat:"/stats_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}


<form name="form" method=post>
<p class="sitetitle">{#selectCurrency#} {html_select tab=$tabCurrency name='currency' url='#' id='admin' value='name' selected=$selectedCurrency}<input type="image" src="{$picPath}admin/refresh.gif" title="{#inforefreshlist#}" class="imgButton" class="middle" /></p>
<br />
<table cellpadding="0" cellspacing="0" width="100%">
{foreach from=$categories item=category key=keyCat}
  <tr>
    <td colspan="3" class="sitetitle">
      <b>{$category->country}</b>&nbsp;&nbsp;<span style="font-size:13px">(<i>{if ($endDate.$keyCat > $category->startingDate)}{#fromDate#}{$category->startingDate|fdate} {#toDate#}{$endDate.$keyCat|fdate}{else}{$category->startingDate|fdate}{/if}</i>)<br /></span>
    </td>
  </tr>
  <tr>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#accommodation#}
    </td>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#planed#}: {$category->budget_accommodation_planed+0|fPrice} {if $selectedCurrency==0}{$countriescurrency.$keyCat}{else}{$maincurrency}{/if}
    </td>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#spent#}: {$category->budget_accommodation_spent+0|fPrice} {if $selectedCurrency==0}{$countriescurrency.$keyCat}{else}{$maincurrency}{/if}
    </td>
  </tr>
  <tr>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#food#}
    </td>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#planed#}: {$category->budget_food_planed+0|fPrice} {if $selectedCurrency==0}{$countriescurrency.$keyCat}{else}{$maincurrency}{/if}
    </td>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#spent#}: {$category->budget_food_spent+0|fPrice} {if $selectedCurrency==0}{$countriescurrency.$keyCat}{else}{$maincurrency}{/if}
    </td>
  </tr>
  <tr>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#transport#}
    </td>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#planed#}: {$category->budget_transport_planed+0|fPrice} {if $selectedCurrency==0}{$countriescurrency.$keyCat}{else}{$maincurrency}{/if}
    </td>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#spent#}: {$category->budget_transport_spent+0|fPrice} {if $selectedCurrency==0}{$countriescurrency.$keyCat}{else}{$maincurrency}{/if}
    </td>
  </tr>
  <tr>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#other#}</span>
    </td>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#planed#}: {$category->budget_other_planed+0|fPrice} {if $selectedCurrency==0}{$countriescurrency.$keyCat}{else}{$maincurrency}{/if}
    </td>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#spent#}: {$category->budget_other_spent+0|fPrice} {if $selectedCurrency==0}{$countriescurrency.$keyCat}{else}{$maincurrency}{/if}
    </td>
  </tr>
  <tr>
    <td class="sitetext">
      <b>&nbsp;&nbsp;&nbsp;{#total#}</b>
    </td>
    <td class="sitetext">
      <b>&nbsp;&nbsp;&nbsp;{#planed#}: {$total.$keyCat.planed+0|fPrice} {if $selectedCurrency==0}{$countriescurrency.$keyCat}{else}{$maincurrency}{/if}</b>
    </td>
    <td class="sitetext">
      <b>&nbsp;&nbsp;&nbsp;{#spent#}: {$total.$keyCat.spent+0|fPrice} {if $selectedCurrency==0}{$countriescurrency.$keyCat}{else}{$maincurrency}{/if}</b>
    </td>
  </tr>
  <tr>
    <td colspan="3">
      <b>&nbsp;<br />
    </td>
  </tr>
{/foreach}
</table>
<hr />
<table cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td colspan="3" class="sitetitle">
      <b>{#total#}</b>
    </td>
  </tr>
  <tr>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#accommodation#}
    </td>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#planed#}: {$globalTotal.accommodation.planed+0|fPrice} {$maincurrency}
    </td>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#spent#}: {$globalTotal.accommodation.spent+0|fPrice} {$maincurrency}
    </td>
  </tr>
  <tr>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#food#}
    </td>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#planed#}: {$globalTotal.food.planed+0|fPrice} {$maincurrency}
    </td>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#spent#}: {$globalTotal.food.spent+0|fPrice} {$maincurrency}
    </td>
  </tr>
  <tr>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#transport#}
    </td>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#planed#}: {$globalTotal.transport.planed+0|fPrice} {$maincurrency}
    </td>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#spent#}: {$globalTotal.transport.spent+0|fPrice} {$maincurrency}
    </td>
  </tr>
  <tr>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#other#}
    </td>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#planed#}: {$globalTotal.other.planed+0|fPrice} {$maincurrency}
    </td>
    <td class="sitetext">
      &nbsp;&nbsp;&nbsp;{#spent#}: {$globalTotal.other.spent+0|fPrice} {$maincurrency}
    </td>
  </tr>
  <tr>
    <td class="sitetext">
      <b>&nbsp;&nbsp;&nbsp;{#total#}</b>
    </td>
    <td class="sitetext">
      <b>&nbsp;&nbsp;&nbsp;{#planed#}: {$globalTotal.total.planed+0|fPrice} {$maincurrency}</b>
    </td>
    <td class="sitetext">
      <b>&nbsp;&nbsp;&nbsp;{#spent#}: {$globalTotal.total.spent+0|fPrice} {$maincurrency}</b>
    </td>
  </tr>
  <tr>
    <td colspan="3">
      <b>&nbsp;<br />
    </td>
  </tr>
</table>
{if !$error && $stateofBudget}
<p class="sitetitle">{#selectGraph#} {html_select tab=$selectTab url='#graph' name='selectGraph' selected=$selectGraph}<input type="image" src="{$picPath}admin/refresh.gif" title="{#inforefreshlist#}" class="imgButton" class="middle" onclick="form.action='#graph'" /></p>
</form>

<br />


{$titleGraph}
<a name="#graph"></a>

<table>
  <tr>
    <td>
      <img src="{#PAGE_GRAPH#}?mainLegend={$titleGraph}&legend1={#planed#}&legend2={#spent#}" />
    </td>
  </tr>
</table>

<br />

<span style="font-size:13px;margin-left:30px"><i>{#budgetGlobal1#} {$stateofBudget} {#budgetGlobal2#} "{$titleGraph}" {#budgetGlobal3#} <b>{$grandTotal|fPrice} {$maincurrency}</b> {$sparedRecover}</i></span>
{else}
</form>
{/if}

<br /><br />

{$showFooter}
