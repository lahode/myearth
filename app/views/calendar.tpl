{$headercal}
<table class="clsOTable" cellspacing="0" border="0" width="100%">
  <tr>
    <td bgcolor="#4682B4">
      <table cellspacing="1" cellpadding="3" border="0" width="100%">
        <tr>
          <td colspan="7">
            <table cellspacing="0" cellpadding="0" border="0" width="100%">
              <tr>
                <script language="JavaScript">

/*******************************************************************************************/
                  // Défini les limites du calendrier par rapport au date occupées
                  var previousYear;
                  var previousMonth = dt_prev_month.valueOf();
                  var nextMonth = dt_next_month.valueOf();
                  var nextYear = dt_next_year.valueOf();
                  {if $datesBooked} var datesBooked = new PhpArray2Js('{$datesBooked}');
                  {else} var datesBooked = null;{/if}
                  {literal}
                  if (datesBooked && datesBooked['tabjs']['firstDate'] != null) {
                    var firstDate = parseInt(datesBooked['tabjs']['firstDate']);
                    var firstdayofThisMonth = new Date(firstDate);
                    firstdayofThisMonth.setDate(1);
                    if (firstdayofThisMonth <= dt_prev_year.valueOf()) {
                      previousYear = dt_prev_year.valueOf();
                    } else {
                      previousYear = firstDate;
                    }
                    if (firstDate <= dt_prev_month.valueOf()) {
                      previousMonth = dt_prev_month.valueOf();
                    } else {
                      previousMonth = firstDate;
                    }
                  } else {
                    previousYear = dt_prev_year.valueOf();
                    previousMonth = dt_prev_month.valueOf();
                  }
                  if (datesBooked && datesBooked['tabjs']['lastDate'] != null) {
                    var lastDate = parseInt(datesBooked['tabjs']['lastDate']);
                    var firstdayofNextMonth = new Date(lastDate);
                    firstdayofNextMonth.setDate(1);
                    firstdayofNextMonth.setMonth(firstdayofNextMonth.getMonth() + 1);
                    if (firstdayofNextMonth > dt_next_year.valueOf()) {
                      nextYear = dt_next_year.valueOf();
                    } else {
                      nextYear = lastDate;
                    }
                    if (lastDate > dt_next_month.valueOf()) {
                      nextMonth = dt_next_month.valueOf();
                    } else {
                      nextMonth = lastDate;
                    }
                  } else {
                    nextYear = dt_next_year.valueOf();
                    nextMonth = dt_next_month.valueOf();
                  }
                  {/literal}

/*******************************************************************************************/

                  document.write(
                    '<td>'+(obj_caller&&obj_caller.year_scroll?'<a href="javascript:set_datetime('+previousYear+
                    ')"><img src="'+STR_ICONPATH+'prev_year.gif" width="16" height="16" border="0" alt="Ann�e pr�c�dente"></a> ':'')+
                    '<a href="javascript:set_datetime('+previousMonth+')"><img src="'+
                    STR_ICONPATH+'prev.gif" width="16" height="16" border="0" alt="Mois pr�c�dent"></a></td>'+
                    '<td align="center" width="100%"><font color="#ffffff">'+
                    ARR_MONTHS[dt_current.getMonth()]+' '+dt_current.getFullYear() + '</font></td>'+
                    '<td><a href="javascript:set_datetime('+nextMonth+')"><img src="'+
                    STR_ICONPATH+'next.gif" width="16" height="16" border="0" alt="Mois prochain"></a>'+
                    (obj_caller && obj_caller.year_scroll?' <a href="javascript:set_datetime('+
                    nextYear+')"><img src="'+
                    STR_ICONPATH+'next_year.gif" width="16" height="16" border="0" alt="Ann�e prochaine"></a>':'')+'</td>'
                  );
                </script>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <script language="JavaScript">
          {literal}

          // print weekdays titles
          for (var n=0; n<7; n++)
            document.write('<td bgcolor="#87cefa" align="center"><font color="#ffffff">'+ARR_WEEKDAYS[(NUM_WEEKSTART+n)%7]+'</font></td>');
          document.write('</tr>');

          // print calendar table
          var dt_current_day = new Date(dt_firstday);
          while (dt_current_day.getMonth() == dt_current.getMonth() || dt_current_day.getMonth() == dt_firstday.getMonth()) {
            // print row heder
            document.write('<tr>');
            for (var n_current_wday=0; n_current_wday<7; n_current_wday++) {

/*******************************************************************************/
              occupied = 0;

              if (firstDate!=null && firstDate > dt_current_day.valueOf()) {
                occupied = 2;
              } else if (lastDate!=null && lastDate <= dt_current_day.valueOf()) {
                occupied = 2;
              } else if (datesBooked && datesBooked['tabjs']['lastCatDate']!=null && parseInt(datesBooked['tabjs']['lastCatDate']) <= dt_current_day.valueOf()) {
                occupied = 1;
              } else if (datesBooked && datesBooked['tabjs']['occupedDates']!=null) {
                for (var i=0;i<datesBooked['tabjs']['occupedDates'].length;i++) {
                  dateOccupied = parseInt (datesBooked['tabjs']['occupedDates'][i]);
                  if (dt_current_day.valueOf() == dateOccupied) {
                    occupied = 2;
                  }
                }
              } else if (datesBooked && datesBooked['tabjs']['borderDates']!=null) {
                for (var i=0;i<datesBooked['tabjs']['borderDates']['first'].length;i++) {
                  dateFirst = parseInt (datesBooked['tabjs']['borderDates']['first'][i]);
                  dateLast = parseInt (datesBooked['tabjs']['borderDates']['last'][i]);
                  if ((dt_current_day.valueOf() >= dateFirst) && (dt_current_day.valueOf() <= dateLast)) {
                    occupied = 2;
                  }
                }
              }

/*******************************************************************************/

              if (occupied<2) {
                if (occupied==1)
                  // print current date
                  document.write('<td bgcolor="#ffcc99" align="center" width="14%">');
                else if (dt_current_day.getDate() == dt_current.getDate() &&
                  dt_current_day.getMonth() == dt_current.getMonth())
                  // print current date
                  document.write('<td bgcolor="#ffb6c1" align="center" width="14%">');
                else if (dt_current_day.getDay() == 0 || dt_current_day.getDay() == 6)
                  // weekend days
                  document.write('<td bgcolor="#dbeaf5" align="center" width="14%">');
                else
                  // print working days of current month
                  document.write('<td bgcolor="#ffffff" align="center" width="14%">');

                document.write('<a href="javascript:set_datetime('+dt_current_day.valueOf() +', true);">');

                if (dt_current_day.getMonth() == this.dt_current.getMonth())
                  // print days of current month
                  document.write('<font color="#000000">');
                else
                  // print days of other months
                  document.write('<font color="#606060">');

                document.write(dt_current_day.getDate()+'</font></a></td>');
            } else {
              document.write('<td bgcolor="#aaaaaa" align="center" width="14%">');
              document.write('<font color="#666666">');
              document.write(dt_current_day.getDate()+'</font></td>');
            }
            dt_current_day.setDate(dt_current_day.getDate()+1);
	  }
	  // print row footer
	  document.write('</tr>');
        }
        if (obj_caller && obj_caller.time_comp)
          document.write('<form onsubmit="javascript:set_datetime('+dt_current.valueOf()+', true)" name="cal"><tr><td colspan="7" bgcolor="#87CEFA"><font color="White" face="tahoma, verdana" size="2">Heure : <input type="text" name="time" value="'+obj_caller.gen_time(this.dt_current)+'" size="8" maxlength="8"></font></td></tr></form>');
        {/literal}
        </script>
      </table>
    </tr>
  </td>
</table>
{$footercal}
