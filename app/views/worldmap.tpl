{config_load file="file.conf"}
<div id=holder>
  <div id=wn>
    <div class="content" id="lyr">
      <table id=imgTbl cellSpacing=0 cellPadding=0 border=0>
        <tbody>
          <tr>
            <td><img src="{$path}{$imgName}?id={$idCache}" border="0" name="image" /></td>
          </tr>
        </tbody>
      </table>
      {if $destinations}
      {foreach from=$destinations key=keyDest item=dest}
      {if $dest.print == 'true'}
      <div id="layerDest[{$keyDest}]" style="cursor:pointer;z-index:3;position:absolute;left:{$dest.x}px;top:{$dest.y}px;color:#66FF44;">
        {if ($dest.link)}
        <div title="{$dest.name}" id="number[{$keyDest}]" class="destNumber" style="z-index:-1;position:absolute;"></div>
        <div style="position:absolute;"><a href="{$dest.link}"><img title="{$dest.name}" src="{$pics}fanion.gif" border="0" id="destination[{$keyDest}]" /></a></div>
        {else}
        {if ($keyDest==0)}
        <div style="position:absolute;"><img src="{$pics}point.gif" border="0" id="destination[{$keyDest}]" /></div>
        <div title="{$dest.name}" id="number[{$keyDest}]" class="destNumber" style="z-index:4;position:absolute;">D</div>
        {else}
        <div style="position:absolute;"><a href="{#PAGE_SHOW#}?dest={$number++}"><img src="{$pics}point.gif" border="0" id="destination[{$keyDest}]" /></a></div>
        <div title="{$dest.name}" id="number[{$keyDest}]" class="destNumber" style="z-index:4;position:absolute;"><a href="{#PAGE_SHOW#}?dest={$number}">{$number}</a></div>
        {/if}
        {/if}
      </div>
      {/if}
      {/foreach}
      {elseif $position}
      <div id="Layer1" style="position:absolute; left:{$position.x}px; top:{$position.y}px;width:24px; height:24px; z-index:2">
        <img src="{$pics}cross.gif" border="0" />
      </div>
      {/if}
    </div>
  </div>

  {if $displayScrollBar}
  <div id=scrollbar>
    <div id=right>
      <a onmouseup="dw_scrollObj.resetSpeed('wn')"
         onmousedown="dw_scrollObj.doubleSpeed('wn')"
         onmouseover="dw_scrollObj.initScroll('wn','right')" onclick="return false"
         onmouseout="dw_scrollObj.stopScroll('wn')" href="javascript:;">
         <img height=11 alt="" src="{$pics}btn-rt.gif" width=11>
      </a>
    </div>
    <div id=up_right>
      <a onmouseup="dw_scrollObj.resetSpeed('wn')"
         onmousedown="dw_scrollObj.doubleSpeed('wn')"
         onmouseover="dw_scrollObj.initScroll('wn','up_right')" onclick="return false"
         onmouseout="dw_scrollObj.stopScroll('wn')" href="javascript:;">
         <img height=11 alt="" src="{$pics}btn-dn.gif" width=11>
      </a>
    </div>
    <div id=up>
      <a onmouseup="dw_scrollObj.resetSpeed('wn')"
         onmousedown="dw_scrollObj.doubleSpeed('wn')"
         onmouseover="dw_scrollObj.initScroll('wn','up')" onclick="return false"
         onmouseout="dw_scrollObj.stopScroll('wn')" href="javascript:;">
         <img height=11 alt="" src="{$pics}btn-up.gif" width=11>
      </a>
    </div>
    <div id=up_left>
      <a onmouseup="dw_scrollObj.resetSpeed('wn')"
         onmousedown="dw_scrollObj.doubleSpeed('wn')"
         onmouseover="dw_scrollObj.initScroll('wn','up_left')" onclick="return false"
         onmouseout="dw_scrollObj.stopScroll('wn')" href="javascript:;">
         <img height=11 alt="" src="{$pics}btn-dn.gif" width=11>
      </a>
    </div>
    <div id=left>
      <a onmouseup="dw_scrollObj.resetSpeed('wn')"
         onmousedown="dw_scrollObj.doubleSpeed('wn')"
         onmouseover="dw_scrollObj.initScroll('wn','left')" onclick="return false"
         onmouseout="dw_scrollObj.stopScroll('wn')" href="javascript:;">
         <img height=11 alt="" src="{$pics}btn-lft.gif" width=11>
      </a>
    </div>
    <div id=down_left>
      <a onmouseup="dw_scrollObj.resetSpeed('wn')"
         onmousedown="dw_scrollObj.doubleSpeed('wn')"
         onmouseover="dw_scrollObj.initScroll('wn','down_left')" onclick="return false"
         onmouseout="dw_scrollObj.stopScroll('wn')" href="javascript:;">
         <img height=11 alt="" src="{$pics}btn-up.gif" width=11>
      </a>
    </div>
    <div id=down>
      <a onmouseup="dw_scrollObj.resetSpeed('wn')"
         onmousedown="dw_scrollObj.doubleSpeed('wn')"
         onmouseover="dw_scrollObj.initScroll('wn','down')" onclick="return false"
         onmouseout="dw_scrollObj.stopScroll('wn')" href="javascript:;">
         <img height=11 alt="" src="{$pics}btn-dn.gif" width=11>
      </a>
    </div>
    <div id=down_right>
      <a onmouseup="dw_scrollObj.resetSpeed('wn')"
         onmousedown="dw_scrollObj.doubleSpeed('wn')"
         onmouseover="dw_scrollObj.initScroll('wn','down_right')" onclick="return false"
         onmouseout="dw_scrollObj.stopScroll('wn')" href="javascript:;">
         <img height=11 alt="" src="{$pics}btn-up.gif" width=11>
      </a>
    </div>
  </div>
  <div id=scrollbar2>
    <div id=zoom>
      <a onmousedown="vitesse=0.4"
         onmouseup="vitesse=0.2"
         onmouseover="zoomer()"
         onmouseout="stopzoom()" style="cursor:pointer">
         <img height=20 width=30 alt="" src="{$pics}zoomin.gif" width=11>
      </a>
    </div>
    <div id=dezoom>
      <a onmousedown="vitesse=0.4"
         onmouseup="vitesse=0.2"
         onmouseover="dezoomer()"
         onmouseout="stopzoom()" style="cursor:pointer">
         <img height=20 width=30 alt="" src="{$pics}zoomout.gif" width=11>
      </a>
    </div>
  </div>
  {/if}
</div>
