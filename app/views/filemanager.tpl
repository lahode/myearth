{config_load file=$userLanguage|cat:"/filemanager_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

<script type="text/javascript" src="../lib/js/multifile_compressed.js"></script>

{$xajax_javascript}

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<p class="sitetitle">{#currentStatus#}</p>
<br /><br />
<div style="float:left;margin-right:20px">
<img src="{$quota_img}" />
</div>
<br />
<p class="sitetitle">{$ratioSpace} % {#occupied#}</p>
<p class="sitetitle">{$freeSpace} {#freeSpace#}</p>
<br /><br />
<hr />

<p class="sitetitle">{#downloadFiles#}</p>
<br />
<p align="center">
  <p style="cursor:pointer" title="{#infoviewpics#}" onclick="window.open('{#PAGE_FILEVIEWER#}?actions=download', null, 'height=480,width=640,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes')"><img src="{$picPath}admin/download.png" border="0" width="24" height="24" /></p>
</p>
<br />
<hr />

<p class="sitetitle">{#sendFiles#}</p>
<br /><br />
<p align="center">
  <form name="form" method="post" enctype="multipart/form-data">
  <input type="hidden" name="save" value="false" />
  <input id="my_file_element" type="file" name="file_1" >
  <table id="files_table" width="80%" class="cadresitetitle">
    <tr class="sitetitle">
      <td style="text-align:left">
        {#files#}
      </td>
      <td style="text-align:center">
        &nbsp;
      </td>
      <td style="text-align:center">
        &nbsp;
      </td>
    </tr>
  </table>
  <div id="files_list"></div>
  <input type="submit" value="{#save#}" onclick="form.save.value='true';" />
  </form>
</p>
<p class="sitetext" align="center" style="cursor:pointer" onclick="xajax_accessSecure()"><img height="15" src="{$picPath}lock.gif" /> {#secureFiles#}</p>
<br /><br />

{if $isFTPactive}
<hr />
<p class="sitetitle">{#uploadFTP#}</p>
<br /><br />
<form name="form2" method="post">
<p class="sitetext" align="center">{#password#} : <input type="password" name="password" value="" onKeyPress = "if (window.event.keyCode==13) return false;" /></p>
<br />
<input type="hidden" name="synchroPics" value="false" />
<p class="sitetext" style="cursor:pointer" align="center" onclick="createCustomConfirm('{#areyousureSynchro#} ?');
  document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();displayLoading('{#waitftpcharging#}');form2.synchroPics.value='true';form2.submit();{literal}}{/literal}">{#synchroPics#}</p>
</form>
<br /><br />
{/if}

{if $origImgPath}
<hr />
<p class="sitetitle">{#deleteImg#}</p>
<br />
<form name="form3" method="post">
<input type="hidden" name="purgeImg" value="false" />
<p class="sitetext" style="cursor:pointer" align="center" onclick="createCustomConfirm('{#areyousurePurge#} ?');
  document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form3.purgeImg.value='true';form3.submit();{literal}}{/literal}">{#purgeImg#}</p>
</p>
</form>
<br />
{/if}

{$showFooter}

<script>
  <!-- Create an instance of the multiSelector class, pass it the output target and the max number of files -->
  var multi_selector = new MultiSelector( document.getElementById( 'files_table' ), 3 );
  <!-- Pass in the file element -->
  multi_selector.addElement( document.getElementById( 'my_file_element' ));
</script>

