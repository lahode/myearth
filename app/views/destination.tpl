{config_load file=$userLanguage|cat:"/destination_"|cat:$userLanguage|cat:".conf"}
{config_load file="file.conf"}

{$showTopHeader}

{$headerWorldmap}

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" onload="initWorld(0, 0, 1);">

{$showHeader}

{if $worldmap}
<br />
<div style="text-align: center;">
  <div id="subWorldBorder">
    {$worldmap}
  </div>
  <div id="worldLegend">
    <div style="float:left;margin-left:20px;margin-top:5px;color:#FF2222">
          ----&nbsp;&nbsp;{#plane#}
    </div>
    <div style="float:left;margin-left:90px;margin-top:5px;color:#FFFF55">
          ----&nbsp;&nbsp;{#train#}
    </div>
    <div style="margin-left:290px;margin-top:5px;color:#6677FF">
          ----&nbsp;&nbsp;{#boat#}
    </div>
    <div style="float:left;margin-left:20px;margin-top:5px;color:#55FF55">
          ----&nbsp;&nbsp;{#others#}
    </div>
    <div style="float:left;margin-left:90px;margin-top:5px;color:#BBBBBB">
          ----&nbsp;&nbsp;{#planedRoute#}
    </div>
    <div>
      &nbsp;
    </div>
    <p style="clean:both;text-align:center;color:#000000">{#distance#} {$distance} {#km#}</p>
  </div>
</div>
{/if}
<table cellpadding="20" cellspacing="20" width="100%" border="0">
  <tr>
    <td >
      <form name="form" method="post">
      <input type="hidden" name="deleteDest" />
      <input type="hidden" name="deleteCat" />
      <input type="hidden" name="changeAccessCat" />
      <input type="hidden" name="changeAccess" />
      {if ($categories)}
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
        {foreach key=keyCat item=category from=$categories}
          {if $category->access=='public' || $admin}
          <tr>
            <td>
              <p class="sitetitle">
              <img src="{$picPath}flags/{$category->country}.gif" height="24" class="middle" />
              {if ($destinations.$keyCat)}
              <a href="{#PAGE_SHOW#}?cat={$category->id}">
                {$category->arrivalCity}({$category->startingDate|showdate})
              </a>
              {else}
                {$category->arrivalCity}({$category->startingDate|showdate})
              {/if}
              {if $category->sound}<img src="{$picPath}music.jpg" class="middle" />{/if}
              {if ($admin)}
                &nbsp;&nbsp;<img src="{$picPath}admin/update.gif" title="{#infoeditcountry#}" style="cursor: pointer" onclick="go('{#PAGE_INSERT_CAT#}?idCat={$category->id}')" class="middle" />
                &nbsp;<img src="{$picPath}admin/delete.gif" style="cursor:pointer" title="{#infodeletecountry#}" class="middle" onclick="createCustomConfirm('{#areyousure#} {$category->arrivalCity} ?');
                document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteCat.value={$category->id};form.submit();{literal}}{/literal}" />
              {/if}
              </p>
              {if $countryImage.$keyCat || $category->summary->general}
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  {if $countryImage.$keyCat}
                  <td>
                    <a href="{#PAGE_SHOW#}?cat={$category->id}"><img src="{$webImgPath}{$countryImage.$keyCat.0}" height="75" /></a>&nbsp;&nbsp;
                  </td>
                  {/if}
                  {if !$category->summary->general}
                  {if $countryImage.$keyCat.1}
                  <td>
                    <a href="{#PAGE_SHOW#}?cat={$category->id}"><img src="{$webImgPath}{$countryImage.$keyCat.1}" height="75" /></a>&nbsp;&nbsp;
                  </td>
                  {/if}
                  {if $countryImage.$keyCat.2}
                  <td>
                    <a href="{#PAGE_SHOW#}?cat={$category->id}"><img src="{$webImgPath}{$countryImage.$keyCat.2}" height="75" /></a>&nbsp;&nbsp;
                  </td>
                  {/if}
                  {/if}
                  {if $category->summary->general}
                  <td>
                    {$category->summary->general|sslash}
                  </td>
                  {/if}
                </tr>
              </table>
              {/if}
              <br />
            </td>
          </tr>
          {if ($destinations.$keyCat)}
          <tr>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                {foreach key=keyDest item=destination from=$destinations.$keyCat}
                {if ($admin || $destination->access=='public')}
                <tr>
                  <td class="sitetext">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="{#PAGE_SHOW#}?dest={$destination->id}">
                      {$destination->order}.
                      {$destination->region} - {$destination->city}
                    </a>
                    ({$destination->startingDate|showdate})
                  </td>
                  {if ($admin)}
                  {if ($destination->nbDays==-1)}
                  <td>
                    &nbsp;
                  </td>
                  {else}
                  <td>
                    &nbsp;<img src="{$picPath}admin/update.gif" title="{#infoeditdestination#}" style="cursor: pointer" onclick="go('{#PAGE_INSERT_DEST#}?idDest={$destination->id}&idCat={$category->id}')" class="middle" />
                  </td>
                  {/if}
                  <td>
                    &nbsp;
                    <img src="{$picPath}admin/delete.gif" style="cursor:pointer" title="{#infodeletedestination#}" class="middle" onclick="createCustomConfirm('{#areyousure#} {$destination->city} ?');
                    document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteDest.value={$destination->id};form.submit();{literal}}{/literal}" />
                  </td>
                  {if ($destination->nbDays==-1)}
                  <td>
                    &nbsp;
                  </td>
                  {else}
                  <td>
                    {if ($destination->access=='public')}
                    &nbsp;<input type="image" src="{$picPath}admin/enable.gif" class="imgButton" onclick="form.changeAccessCat.value='{$keyCat}';form.changeAccess.value='{$keyDest}'" />
                    {else}
                    &nbsp;<input type="image" src="{$picPath}admin/disable.gif" class="imgButton" onclick="form.changeAccessCat.value='{$keyCat}';form.changeAccess.value='{$keyDest}'" />
                    {/if}
                  </td>
                  {/if}
                  {/if}
                </tr>
                {/if}
                {/foreach}
              </table>
            {if ($admin)}<br />&nbsp;<a href="{#PAGE_INSERT_DEST#}?idDest={$lastDestID}&idCat={$category->id}" class="sitetitle">{#insertDestination#}</a><br /><br />{/if}
            {if ($admin)}&nbsp;<a href="{#PAGE_INSERT_TEMPDEST#}?idDest={$lastDestID}&idCat={$category->id}" class="sitetitle">{#insertTempDestination#}</a><br /><br />{/if}
            </td>
          </tr>
          {elseif ($admin)}
          <tr>
            <td>
              <br /><a href="{#PAGE_INSERT_DEST#}?idDest={$lastDestID}&idCat={$category->id}" class="sitetitle">{#insertDestination#}</a><br /><br />
            </td>
          </tr>
          {/if}
          <tr>
            <td>
              <br /><hr />
            </td>
          </tr>
          {/if}
        {/foreach}
        </table>
      {/if}
      {if ($admin)}<br /><a href="{#PAGE_INSERT_CAT#}?idCat={$lastCatID}" class="sitetitle">{#insertCountry#}</a>{/if}
      </form>
    </td>
  </tr>
</table>

{$showFooter}
