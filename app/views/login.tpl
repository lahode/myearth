{config_load file=$userLanguage|cat:"/login_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

<script language="javascript" src="../lib/js/detectFlash.js"></script>
<script language="javascript" src="../lib/js/sniffer.js"></script>

{if $refreshScreen}
<script type="text/javascript" language="javascript">           
{literal}
if (self.name != '_refreshed_')
{
    self.name = '_refreshed_';
    self.location.reload(true);
}
else self.name = '';
{/literal}
</script>
{/if}

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<form id="loginForm" name="loginForm" method="post">
<input type="hidden" name="init_{$pageName}" value="true">
<br /><br /><br />
<p align="center">
  <table align="center" width="60%" border="0" class="login">
    <tr>
      <td colspan="4" class="sitetitle" style="text-align:center">
        {#identification#}
        <br /><br />
      </td>
    </tr>
    <tr>
      <td width="15%">
        &nbsp;
      </td>
      <td class="sitetext">
        {#username#}
      </td>
      <td align="left">
        <input type="text" name="login" value="{if ($admin<>'true')}{$username}{/if}" size="14" maxlength="12" />
      </td>
      <td width="15%">
        &nbsp;
      </td>
    </tr>
    {if !$hidePassword}
    <tr>
      <td width="15%">
        &nbsp;
      </td>
      <td class="sitetext">
        {#password#}
      </td>
      <td align="left">
        <input type="password" name="psw" size="14" maxlength="12" />
      </td>
      <td width="15%">
        &nbsp;
      </td>
    </tr>
    {/if}
    <tr>
      <td width="15%">
        &nbsp;
      </td>
      <td class="sitetext">
        {#language#}
      </td>
      <td align="left">
        {html_select tab=$tableLanguage name='language' default="Auto" selected=$languageChosen}
      </td>
      <td width="15%">
        &nbsp;
      </td>
    </tr>
    {if $displayChat}
    <tr>
      <td width="15%">
        &nbsp;
      </td>
      <td colspan="2" class="sitetext">
        <br />
        <input type="checkbox" checked="checked" name="connectToChat" /> {#chatConnect#}
      </td>
      <td width="15%">
        &nbsp;
      </td>
    </tr>
    {/if}
    <tr>
      <td colspan="4" align="center">
        <script language="JavaScript" type="text/javascript">
        <!--
          document.write ('<br /><input type="submit" value="{#validate#}" /><br />');
        // -->
        </script>
        <br />
      </td>
    </tr>
  <table>
</p>
</form>

<br /><br />
<table align="center" width="60%" border="0">
<tr>
<td>
{if ($confirm)}
<p class="confirm">
  &nbsp;
    {if ($admin<>'true')}{$confirm}{/if}
    <noscript>
      <br /><br />{#jsOff#}
    </noscript>
</p>
{else}
<p class="error">
  &nbsp;
    {if ($admin<>'true')}{$error}{/if}
    <noscript>
      <br /><br />{#jsOff#}
    </noscript>
</p>
{/if}
<script language="JavaScript" type="text/javascript">
<!--
{literal}
  // Globals
  // Major version of Flash required
  var requiredMajorVersion = 8;
  // Minor version of Flash required
  var requiredMinorVersion = 0;
  // Minor version of Flash required
  var requiredRevision = 0;

  // Version check based upon the values entered above in "Globals"
  var hasReqestedVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);

  // Check to see if the version meets the requirements for playback
  if (!hasReqestedVersion) {
    var alternateContent = "<p class=\"error\">{/literal}{#noflash#}{literal}<br />"
      + "{/literal}{#restrictedAccess#}{literal}<br />"
      + "<a href=\"http://www.adobe.com/go/getflash/\" target=\"_blank\"><img src=\"../lib/pics/flash_install.gif\" /></a></p>";
    document.write(alternateContent);  // insert non-flash content
  }
{/literal}
// -->
</script>

{if $album=='test'}
<br />
<p class="sitetext" style="text-align:center">{#testInfo#}</p>
{/if}

<br />
{if ($admin<>'true')}
<p class="sitetext" style="text-align:center">
{#moreInfo#} <a href="{#PAGE_CONTACT#}?first=true&album={$album}">{#contact#}</a>
</p>
{/if}
</td>
</tr>
</table>

{$showFooter}

<script language="JavaScript" type="text/javascript">
<!--
{literal}
  if ((is_nav && !is_nav7up) || (is_ie && !is_ie5_5up) || (is_opera && !is_opera7up))
    alert ('{#oldNavigator#}');

  if (is_js < 1.2)
    alert ('{#oldJS#}');

  if (!is_all || !is_anchors || !is_cookie || !is_forms ||
      !is_getElementById || !is_getElementsByTagName ||
      !is_documentElement || !is_images || !is_links)
    alert ('{#objectJSError#}');
{/literal}

{if $waitingMsg==1}
  displayLoading('{#waitloading#}');
  go('{#PAGE_ACCUEIL#}');
{/if}
// -->
</script>


