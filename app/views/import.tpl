{config_load file=$userLanguage|cat:"/import_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

<style type="text/css">
{literal}
select {
  font-size:10px;
}
{/literal}
</style>

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

<p class="formTitle">{#importCVS#}</p>

<br />

{if $tab}
<form name="form" method=post>
<input type="hidden" name="init_{$pageName}" value="true">
<table cellspacing="0" cellpadding="0" width="100%" border="0">
  <tbody>
    <tr>
      {section name=fo1 start=0 loop=$nbCol}
      <td>
        <select name=col[{$smarty.section.fo1.index}]>
        {foreach from=$tabContact key=keyElement item=element}
          <option value="{$keyElement}">{$element}</option>
        {/foreach}
        </select>
      </td>
      {/section}
    </td>
    {foreach from=$tab key=keyTab item=itemTab}
    <tr>
      {section name=fo2 start=0 loop=$nbCol}
      <td style="font-size:11px">
        {$itemTab[$smarty.section.fo2.index]}
        <input type="hidden" name="element[{$keyTab}][{$smarty.section.fo2.index}]" value="{$itemTab[$smarty.section.fo2.index]}" />
      </td>
      {/section}
    </tr>
    {/foreach}
  <tbody>
</table>

<p align="center">
  <input type="submit" value="{#save#}">
</p>
</form>
{else}
<form name="form" method=post ENCTYPE=multipart/form-data>

<p align="center">
  <input type="file" name="importfile" id="imgMsg" onchange="document.getElementById('showImageMsg').src='{$picPath}blank.gif'" />
</p>

<p align="center">
  {#separator#} : <input type="text" size="2" name="separator" maxlength="2" />
</p>

<br />

<p align="center">
  <input type="submit" value="{#import#}">
</p>

</form>
{/if}
<br /><br />
{if ($error<>'')}
<p class="error">{$error|sslash}</p>
{elseif ($confirm<>'')}
<p class="error">{$confirm|sslash}</p>
{/if}

</body>

</html>