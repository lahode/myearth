{config_load file=$userLanguage|cat:"/compose_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

<script language="javascript" type="text/javascript" src="../lib/js/tiny_mce/tiny_mce.js"></script>

<script language="javascript" type="text/javascript">
{literal}
  function openAB() {
{/literal}
    window.open('{#PAGE_ADDRESSBOOK#}', 'address_book', 'width=700,height=475,toolbar=no,scrollbars=yes,status=no');
{literal}
  }
{/literal}

{literal}
  tinyMCE.init({
    theme : "advanced",
    convert_urls : false,
    relative_urls : false,
    mode : "exact",
    elements : "message",
    extended_valid_elements : "a[href|target|name]",
    plugins : "table, emoticons",

    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,sub,sup,separator,charmap,separator,forecolor,backcolor",
    theme_advanced_buttons2 : "bullist,numlist,separator,outdent,indent,separator,undo,redo,separator,link,unlink",
    theme_advanced_buttons3 : "",

    plugins : "paste",
    theme_advanced_buttons2_add : "selectall,pastetext",

    debug : false
  });

{/literal}
<!-- /tinyMCE -->
</script>

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<form name="form" method="post" enctype="multipart/form-data">

<input type="hidden" name="addFile" value="false" />
<input type="hidden" name="deleteFiles" value="false" />
<input type="hidden" name="back" value="false" />
<input type="hidden" name="send" value="false" />
<input type="hidden" name="init_{$pageName}" value="true">
<input type="hidden" name="backTo" value="{$back}" />
<table cellSpacing=0 cellPadding=0 border=0>
  <tbody>
    <tr>
      <td class="sitetext" align=right height=25>{#from#}</td>
      <td>
        {if ($emails)}
        {html_select tab=$emails name='email' selected=$selectedEmail}
        {else}
        <input type="text" name="email" value="{$email}" size="50" />
        {/if}
      </td>
    </tr>
    <tr>
      <td class="sitetext" align=right height=25>
        <span style="cursor:pointer" onclick=openAB();><u>{#to#}</u></span>
      </td>
      <td>
        <input onkeydown="onFieldKeyDown(this, event);"
               onblur=fieldLostFocus(this.layerId);
               onkeyup="onFieldKeyUpAb(this, event);" 
               style="WIDTH: 358px" size=60 name=to 
               AUTOCOMPLETE="off" value="{$to}">&nbsp;
        <img style="cursor:pointer" onclick=openAB() src="{$picPath}admin/addressbook.gif" align=bottom><br />
        <div class=autocomplete id=autocomplete_to onmouseover=this.dontHide=true onmouseout=this.dontHide=false></div>
      </td>
    </tr>
    <tr>
      <td class="sitetext" align=right height=25>
        <span style="cursor:pointer" onclick=openAB();><u>{#cc#}</u></span>
      </td>
      <td>
        <input onkeydown="onFieldKeyDown(this, event);"
               onkeyup="onFieldKeyUpAb(this, event);"
               style="WIDTH: 358px" size=60 name=cc
               AUTOCOMPLETE="off" value="{$cc}">&nbsp;
        <img style="cursor:pointer" onclick=openAB() src="{$picPath}admin/addressbook.gif" align=bottom><br />
        <div class=autocomplete id=autocomplete_to onmouseover=this.dontHide=true onmouseout=this.dontHide=false></div>
      </td>
    </tr>
    <tr>
      <td class="sitetext" align=right height=25>
        <span style="cursor:pointer" onclick=openAB();><u>{#bcc#}</u></span>
      </td>
      <td>
        <input onkeydown="onFieldKeyDown(this, event);"
               onkeyup="onFieldKeyUpAb(this, event);"
               style="WIDTH: 358px" size=60 name=bcc 
               AUTOCOMPLETE="off" value="{$bcc}">&nbsp;
        <img style="cursor:pointer" onclick=openAB() src="{$picPath}admin/addressbook.gif" align=bottom border=0><br />
        <div class=autocomplete id=autocomplete_to onmouseover=this.dontHide=true onmouseout=this.dontHide=false></div>
      </td>
    </tr>
    <tr>
      <td class="sitetext" align=right height=27>{#subject#}</td>
      <td>
        <input style="WIDTH: 500px" size="60" name="subject" value="{$subjectDisplay|sslash}">
      </td>
    </tr>
    <tr>
      <td class="sitetext" align=right height=27>{#message#}</td>
      <td>
        <textarea style="WIDTH: 500px" rows="10" name="message">{$messageDisplay|sslash}</textarea>
      </td>
    </tr>
    <tr valign="top">
      <td class="sitetext" align=right height=27>{#attachments#}</td>
      <td>
        {if $freespace > 0}
        <table>
          <tr>
            <td>
              <input type="file" name="attachedfile" id="attachedfile" />
            </td>
            <td>
              <input type="submit" value="{#insertFile#}" onclick="form.addFile.value='true';" />
            </td>
          </tr>
          {if $attachedFiles}
          <tr>
            <td>
              {html_select obj=$attachedFiles name='attachedFiles[]' id='name' size='3' value='name'}<img src="{$picPath}admin/delete.gif" style="cursor:pointer" title="{#infodeletefile#}" class="middle" onclick="form.deleteFiles.value='true';form.submit();" />
            </td>
            <td>
              &nbsp;
            </td>
          </tr>
          {/if}
        </table>
        {else}&nbsp;<span class="sitetext" style="color:#FF0000">{#quotaMaxReached#}</span>{/if}
      </td>
    </tr>
    <tr>
      <td class="sitetext" align=left height=27 colspan="2">{#saveMail#} <input type="checkbox" name="saveMail" checked="checked" /></td>
    </tr>
  </tbody>
</table>


<br /><br />
<table width="100%">
  <tr>
    <td align="center">
      <input type="submit" value="{if $back=='in'}{#backtoinbox#}{elseif $back=='out'}{#backtooutbox#}{else}{#backtocontact#}{/if}" onclick="form.back.value='true'" />
    </td>
    <td align="center">
      <input type="submit" value="{#send#}" disable onclick="form.send.value='true'" />
    </td>
  </tr>
</table>


</form>
{$showFooter}
