{config_load file=$userLanguage|cat:"/config_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

<link rel="stylesheet" type="text/css" href="../lib/css/ColorPicker.css" />

<script type="text/javascript" src="../lib/js/CP_Class.js"></script>

{literal}
<script type="text/javascript">
  window.onload = function() {
    fctLoad();
  }                                  
  window.onscroll = function() {
    fctShow();
  }
  window.onresize = function() {
    fctShow();
  }
</script>
{/literal}

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<form name="form" method=post>
<input type="hidden" name="cancelFont" value="false" />
<input type="hidden" name="saveFont" value="false" />
<input type="hidden" name="fontName" value="{$fontstyle->name}" />
<input type="hidden" name="init_{$pageName}" value="true">

<p class="formTitle">{#editFont#}: {$fontstyle->name}</p>

<fieldset>
  <br />
  <label style="width:250px;clear:both">{#fontSize#}</label>
  <label style="width:210px">
    {html_select tab=$tableSize name=font_size selected=$fontstyle->size}&nbsp;
  </label>
  <br /><br />

  <label style="width:250px;clear:both">{#fontStyle#}</label>
  <label style="width:210px">
    {html_select tab=$tableStyle name=font_style selected=$fontstyle->style}&nbsp;
  </label>
  <br /><br />

  <label style="width:250px;clear:both">{#textUnderline#}</label>
  <label style="width:210px">
    <input type="checkbox" {if $fontstyle->textUnderline==1}checked="checked"{/if} id="font_textUnderline" name="font_textUnderline" />
  </label>
  <br /><br />

  <label style="width:250px;clear:both">{#linkUnderline#}</label>
  <label style="width:210px">
    <input type="checkbox" {if $fontstyle->linkUnderline==1}checked="checked"{/if} id="font_linkUnderline" name="font_linkUnderline" />
  </label>
  <br /><br />

  <label style="width:250px;clear:both">{#textColor#}</label>
  <label style="width:210px">
    <input type="text" size="10" name="font_textColor" value="{$fontstyle->textColor}" style="color:{$fontstyle->textColor};background-color:{$fontstyle->textColor}" maxlength="7" style="font-family:Tahoma;font-size:x-small;" />
    <img src="{$picPath}admin/color.gif" width="21" height="20" border="0" onClick="fctShow(document.form.font_textColor);" style="cursor:pointer;" class="middle" />
  </label>
  <br /><br />

  <label style="width:250px;clear:both">{#linkColor#}</label>
  <label style="width:210px">
    <input type="text" size="10" name="font_linkColor" value="{$fontstyle->linkColor}" style="color:{$fontstyle->linkColor};background-color:{$fontstyle->linkColor}" maxlength="7" style="font-family:Tahoma;font-size:x-small;" />
    <img src="{$picPath}admin/color.gif" width="21" height="20" border="0" onClick="fctShow(document.form.font_linkColor);" style="cursor:pointer;" class="middle" />
  </label>
  <br /><br />

  <label style="width:250px;clear:both">{#visitedColor#}</label>
  <label style="width:210px">
    <input type="text" size="10" name="font_visitedColor" value="{$fontstyle->visitedColor}" style="color:{$fontstyle->visitedColor};background-color:{$fontstyle->visitedColor}" maxlength="7" style="font-family:Tahoma;font-size:x-small;" />
    <img src="{$picPath}admin/color.gif" width="21" height="20" border="0" onClick="fctShow(document.form.font_visitedColor);" style="cursor:pointer;" class="middle" />
  </label>
  <br /><br />

  <label style="width:250px;clear:both">{#hoverColor#}</label>
  <label style="width:210px">
    <input type="text" size="10" name="font_hoverColor" value="{$fontstyle->hoverColor}" style="color:{$fontstyle->hoverColor};background-color:{$fontstyle->hoverColor}" maxlength="7" style="font-family:Tahoma;font-size:x-small;" />
    <img src="{$picPath}admin/color.gif" width="21" height="20" border="0" onClick="fctShow(document.form.font_hoverColor);" style="cursor:pointer;" class="middle" />
  </label>
  <br /><br />
</fieldset>

<br /><br />

<table width="100%">
  <tr>
    <td align="center">
      <input type="submit" value="{#cancel#}" onclick="form.cancelFont.value='true'" />
    </td>
    <td align="center">
      <input type="submit" value="{#validate#}" onclick="form.saveFont.value='true'" />
    </td>
  </tr>
</table>

</form>

{$showFooter}

