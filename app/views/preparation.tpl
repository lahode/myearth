{config_load file=$userLanguage|cat:"/preparation_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<form name="form" method=post ENCTYPE=multipart/form-data>
<p class="sitetitle">{#selectCategory#} : {html_select tab=$tablePreparation name='prepareTitle'}
<input type="submit" value="{#search#}" onclick="form.action='#'+form.prepareTitle.value" /></p>
<br /><br />

<input type="hidden" name="editMessage">
<input type="hidden" name="changeAccess">

<input type="hidden" name="deleteImage" value="" />
<input type="hidden" name="addImage" value="false" />
<input type="hidden" name="cancel" value="false" />
<input type="hidden" name="save" value="false" />
<input type="hidden" name="init_{$pageName}" value="true">

{foreach from=$message key=keymsg item=itemMessage}
<a name="{$itemMessage->id}"></a>
<div class="cadresitetitle">
  <p class="sitetitle">{$itemMessage->subject}
  {if ($admin)}
  &nbsp;<input type="image" src="{$picPath}admin/update.gif" title="{#infoedittext#}" class="imgButton" onclick="form.editMessage.value='{$itemMessage->id}';form.action='#edit'" />
  {/if}
  </p>
  <p class="persotext">{$itemMessage->text|sslash|nl2br}</p>
  {if $itemMessage->images}
  <table cellpadding="0" cellspacing="0" align="center">
  {foreach from=$itemMessage->images key=keyImage item=image}
    {if ($keyImage % 2 == 0)}
    <tr>
    {/if}
      <td align="center">
        <table border="0">
          <tr valign="middle">
            <td>
              <img src="{$imgPath}{$image->name}" height="180" />
            </td>
          </tr>
          <tr>
            <td align="center" style="font-size:11px">
              {$image->description|sslash|nl2br}
              <br />
            </td>
          </tr>
        </table>
      </td>
    {if ($keyImage % 2 == 2 || $keyImage==($itemMessage->images|@count)-1)}
    </tr>
    {/if}
  {/foreach}
  </table>
  {/if}
  <p align="right" style="margin-bottom:0px"><a href="#top"><img src="{$picPath}top.gif" style="border:0px" /></a></p>
</div>
{/foreach}


{if $admin && $showEdit}
<a name="edit"></a>
<p class="formTitle">{#newMessage#}</p>

<fieldset>
  <br />
  <input type="hidden" name="id" value="{$newMessage->id}" />
  <label style="width:100px">{#subject#}</label>
  <span class="sitetext">{$newMessage->subject}</span>
  <br /><br />

  <label style="width:100px">{#message#}</label>
  <textarea name="text" id="text" cols="70" rows="7">{$newMessage->text|sslash}</textarea>
  <br /><br />

</fieldset>

<fieldset>
  <label style="width:200px">{#pictureName#}<br />{#max4img#}</label>
  {$imgPrep}
  <br /><br />

  <label style="width:200px">{#descriptionImg#}</label>
  <textarea name="imageDescription" id="imageDescription" cols="50" rows="2"></textarea>
  <br /><br /><br />
  <p align="center"><input type="submit" value="{#addPicture#}" onclick="form.addImage.value='save';form.action='#edit';" /></p>
</fieldset>

{if ($newMessage->images)}
<table width="100%">
  <tr>
  {foreach key=keyImage item=image from=$newMessage->images}
    <td align="center">
      <table border="0">
        <tr valign="middle">
          <td>
            <img src="{$image->temp}{if ($keyImage|count_characters)==4}{$image->name}{else}{$keyImage}.tmp{/if}" height="60" />
          </td>
          <td>
            &nbsp;<img src="{$picPath}admin/delete.gif" style="cursor:pointer" class="middle" onclick="createCustomConfirm('{#areyousure#} {$image->name} ?');
            document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteImage.value='{$keyImage}';form.submit();{literal}}{/literal}" />
          </td>
        </tr>
        <tr>
          <td align="center" colspan="2" style="font-size:9px">
            {$image->description|sslash}
            <br />
          </td>
        </tr>
      </table>
    </td>
  {/foreach}
  </tr>
</table>
{/if}
<br />
<table width="100%">
  <tr>
    <td align="center">
      <input type="submit" value="{#cancel#}" onclick="form.cancel.value='true'" />
    </td>
    <td align="center">
      <input type="submit" value="{#save#}" onclick="form.save.value='true'" />
    </td>
  </tr>
</table>
{/if}

</form>

{$showFooter}
