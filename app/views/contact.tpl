{config_load file=$userLanguage|cat:"/contact_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

<script language="javascript" src="../lib/js/detectFlash.js"></script>
<script language="javascript" src="../lib/js/sniffer2.js"></script>
<script language="javascript" type="text/javascript" src="../lib/js/tiny_mce/tiny_mce.js"></script>

<script language="javascript" type="text/javascript">
{literal}
  tinyMCE.init({
    theme : "advanced",
    convert_urls : false,
    relative_urls : false,
    mode : "exact",
    elements : "message",
    extended_valid_elements : "a[href|target|name]",
    plugins : "table",

    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,sub,sup,separator,charmap,separator,forecolor,backcolor",
    theme_advanced_buttons2 : "bullist,numlist,separator,outdent,indent,separator,undo,redo,separator,link,unlink",
    theme_advanced_buttons3 : "",

    plugins : "paste",
    theme_advanced_buttons2_add : "selectall,pastetext",

    debug : false
  });
{/literal}
</script>

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<form id="loginForm" name="loginForm" method="post">
<input type="hidden" name="sentEmail" value="false">
<input type="hidden" name="addexistingEmail" value="false">
<input type="hidden" name="addnewEmail" value="false">
<input type="hidden" name="deleteEmail" value="false">
<input type="hidden" name="save" value="false">
<input type="hidden" name="init_{$pageName}" value="true">
{if $admin}
<p class="formTitle">{#manageEmail#}</p>
<fieldset>
  {if $tabEmails}
  <label style="width:240px">{#existingEmail#}</label>
  {html_select tab=$tabEmails name='existingemail' selected=$selectedEmail} <input type="submit" value="{#add#}" onclick="loginForm.addexistingEmail.value='save'" />
  <br /><br />
  {/if}
  <label style="width:240px">{#newEmail#}</label>
  <input type="text" name="newemail" size="30" maxlength="30" /> <input type="submit" value="{#add#}" onclick="loginForm.addnewEmail.value='save'" />
  <br /><br />
  {foreach from=$emails key=keyEmail item=itemEmail}
  <label style="width:240px">&nbsp;</label>
  <span style="font-size:13px;">
    {$itemEmail}<input type="hidden" name="emails[{$keyEmail}]" value='{$itemEmail}' />
    &nbsp;<img src="{$picPath}admin/delete.gif" style="cursor:pointer" title="{#infodeleteemail#}" class="middle" onclick="createCustomConfirm('{#areyousure#} {$itemEmail} ?');
    document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();loginForm.deleteEmail.value={$keyEmail};loginForm.submit();{literal}}{/literal}" />
  </span>
  <br />
  {/foreach}
  <br />
  <table width="100%">
    <tr>
      <td align="center">
        <input type="submit" value="{#save#}" onclick="loginForm.save.value='true'" />
      </td>
    </tr>
  </table>
</fieldset>
{/if}
<br /><br />
<p>Bonjour, ce service qui �tait jusqu'� pr�sent indisponible � �t� r�activ� avec succ�s, vous pouvez � pr�sent l'utiliser � nouveau, merci</p>
<br />
  <table align="center" width="60%" border="0" class="login">
    <tr>
      <td colspan="4">
        <p class="sitetitle" style="text-align:center">{if $contactWebmaster}{#contactwebmaster#}{else}{#contactme#}{/if}</p>
        <br />
      </td>
    </tr>
    <tr>
      <td width="15%">
        &nbsp;
      </td>
      <td class="sitetext">
        {#name#}
      </td>
      <td>
        <input type="text" name="nom" value="{$nom}"  />
      </td>
      <td width="15%">
        &nbsp;
      </td>
    </tr>
    <tr>
      <td width="15%">
        &nbsp;
      </td>
      <td class="sitetext">
        {#email#}
      </td>
      <td align="left">
        <input type="text" name="email" value="{$email}" />
      </td>
      <td width="15%">
        &nbsp;
      </td>
    </tr>
    <tr>
      <td width="15%">
        &nbsp;
      </td>
      <td class="sitetext">
        {#subject#}
      </td>
      <td align="left">
        <input type="text" name="subject" value="{$subject}" size="60" maxlength="100" />
      </td>
      <td width="15%">
        &nbsp;
      </td>
    </tr>
    <tr>
      <td width="15%">
        &nbsp;
      </td>
      <td class="sitetext" width="25%">
        {#message#}
      </td>
      <td align="left">
        <textarea style="WIDTH: 500px" rows="10" name="message">{$message|sslash}</textarea>
      </td>
      <td width="15%">
        &nbsp;
      </td>
    </tr>
<!--
    <tr valign="top">
      <td width="15%">
        &nbsp;
      </td>
      <td class="sitetext" align=right height=27>{#attachments#}</td>
      <td>
        <table>
          <tr>
            <td>
              <input type="file" name="attachedfile" id="imgMsg" onchange="document.getElementById('showImageMsg').src='{$picPath}blank.gif'" />
            </td>
            <td>
              <input type="submit" value="{#insertFile#}" onclick="loginForm.addFile.value='true';" />
            </td>
          </tr>
          {if $attachedFiles}
          <tr>
            <td>
              {html_select tab=$attachedFiles name='attachedFiles[]' size='3' value='name'}<img src="{$picPath}admin/delete.gif" style="cursor:pointer" title="{#infodeletefile#}" class="middle" onclick="loginForm.deleteFiles.value='true';loginForm.submit();" />
            </td>
            <td>
              &nbsp;
            </td>
          </tr>
          {/if}
        </table>
      </td>
      <td width="15%">
        &nbsp;
      </td>
    </tr>
-->
    <tr>
      <td colspan="4" align="center">
        <br /><input type="submit" value="{#send#}" onclick="loginForm.sentEmail.value='true'" /><br />
        <br />
      </td>
    </tr>
  <table>
</form>

<br /><br />

<table align="center" width="60%" border="0">
  <tr>
    <td>
      &nbsp;
      {if ($sessionStatus<>'active')}
      <p align="left" class="error">
        &nbsp;
        {$error}
      </p>
      <br /><br />
      <p class="sitetext" style="text-align:center">
        <a href="{#PAGE_ACCUEIL#}">Retour</a>
      </p>
      {/if}
    </td>
  </tr>
</table>

{$showFooter}
