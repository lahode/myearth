{config_load file="file.conf"}
{config_load file=$userLanguage|cat:"/showcountry_"|cat:$userLanguage|cat:".conf"}
{config_load file=$userLanguage|cat:"/showdest_"|cat:$userLanguage|cat:".conf"}

{if $dest}<p class="sitetitle"><img src="{$picPath}destination.gif" class="middle" /> {$dest->region} - {$dest->city} ::: <img src="{$picPath}flags/{$countryID}.gif" height="24" class="middle" /> ::: {#from#} {$dest->startingDate|showdate} &nbsp;{#to#} {$leavingDate|showdate}</p>{/if}
{if $cat}<p class="sitetitle"><img src="{$picPath}flags/{$countryID}.gif" height="24" class="middle" /> ::: {#from#} {$cat->startingDate|showdate} &nbsp;{#to#} {$leavingDate|showdate}</p>{/if}

{if $dest}
<div class="sectionP">
<span class="sitetitle">{#arrival#}: </span><span class="sitetext">{$dest->startingDate|fdate} &nbsp;{#by#} {$dest->arrivalBy}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="sitetitle">{#lengthStay#}: </span><span class="sitetext">{$dest->nbDays} {#days#}</span>
</div>
{/if}
