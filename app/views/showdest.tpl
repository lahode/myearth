{config_load file="file.conf"}

{if ($dest->text<>'')}
<div class="sectionP">
  <p class="persotext">{$dest->text|sslash|nl2br}</p>
</div>
<hr />
{/if}

{if ($dest->images)}
<p align="center">
<p style="cursor:pointer" onclick="window.open('{#PAGE_FILEVIEWER#}?actions=showDestination&id={$dest->id}', null, 'height=480,width=640,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes')"><img alt="{#showAllPictures#}" src="../lib/pics/album.gif" style="border-style:1px;border-color:silver" width="120" /></p>
<br />
<p align="center">
  {include file="showalbum.tpl"}
  <br /><br />
</p>
{/if}

<div class="sectionP">
  <p class="sitetitle">{#otherVisitedRegion#}</p>
</div>

<div class="sectionP">
<table width="100%">
  {foreach from=$allDest key=keyDest item=itemDest}
  {if $keyDest % 2 == 0}
  <tr>
  {/if}
    <td align="left" class="sitetext">
      {if $itemDest->id==$dest->id}
      {$itemDest->region} - {$itemDest->city}
      {else}
      <span style="cursor:pointer" onclick="xajax_showDestination({$itemDest->id})">{$itemDest->region} - {$itemDest->city} </span>
      {/if}
    </td>
  {if ($keyDest % 2 == 1 || $keyDest==($allDest|@count)-1)}
  </tr>
  {/if}
  {/foreach}
</table>
</div>
