{config_load file=$userLanguage|cat:"/home_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<form method="post" name="form" id="form">
<input type="hidden" name="save" value="false" />
<div class="sectionP">
{if $rank}
<input type="hidden" name="editRank" value="" />
<br />
<div class="cadresitetitle" style="width: 100%">
  <p class="pagetitle">{$elements.$rank}</p>
  {foreach name=displayboucle from=$displayCountries key=keyDisplay item=display}
  <p class="sitetitle">
{if ($smarty.foreach.displayboucle.last)}
{$tabRanking.0}
{elseif ($keyDisplay>5)}
{$keyDisplay}
{$tabRanking.6}
{else}
{$tabRanking.$keyDisplay}
{/if}
 : {$display.0|sslash}</p>
  <p class="sitetext">{$display.1|sslash}</p>
  {/foreach}
</div>
{if !$editRank && $admin}
<p style="cursor:pointer" onclick="form.editRank.value='{$rank}';form.submit();return true;" />Modifier le classement</p>
{/if}

<p style="cursor:pointer" onclick="location.href='{#PAGE_RANKING#}';" />Retour</p>

{else if $elements}
<br />
<table width="100%">
  {foreach name=elementboucle from=$elements key=keyElement item=element}
  {if $smarty.foreach.foreachboucle.index % 3 == 0}
  <tr>
  {/if}
    <td align="left">
       <div class="cadresitetitle" onclick="location.href='?rank={$keyElement}';" style="cursor: pointer;width: 150px">
       <p class="pagetitle">{$element}</p>
       {foreach name=countryboucle from=$countriesToDisplay.$keyElement item=country}
       <p class="sitetitle">{$smarty.foreach.countryboucle.index+1}) {$country|sslash}</p>
       {/foreach}
       </div>
    </td>
  {if ($smarty.foreach.elementboucle.index % 3 == 2 || $smarty.foreach.elementboucle.index==($elements|@count)-1)}
  </tr>
  {/if}
  {/foreach}
</table>
{/if}
</div>

{if $editRank && $admin}
<br />
<table width="575" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" style="padding-right:15px;">
      <select name="countrynotselect[]" id="countrynotselect[]" multiple  size="10" style="width:241px;height:92px;font-size:11px">
        {foreach from=$allcountries key=countryKey item=countryItem}
        <option value="{$countryKey}">{$countryItem}</option>
        {/foreach}
      </select>
    </td>
    <td valign="top">
      <table border="0" width="72" cellpadding="0" cellspacing="0" align="center">
        <tr><td align="center"><input type="button" value=">>" onclick="moveSelectedOptions ('countrynotselect[]', 'countryselected[]');" /></td></tr>
        <tr><td class="sitetitle" style="text-align:center">{#to#}</td></tr>
        <tr><td align="center"><input type="button" value="<<" onclick="moveSelectedOptions ('countryselected[]', 'countrynotselect[]');" /></td></tr>
      </table>
    </td>
    <td valign="top" style="padding-right:15px;">
      <select name="countryselected[]" id="countryselected[]" multiple  size="10" style="width:241px;height:92px;font-size:11px">
        {foreach from=$countriesSelected key=countryselKey item=countryselItem}
        <option value="{$countryselKey}">{$countryselItem}</option>
        {/foreach}
      </select>
      <input type="button" value="Monter" onClick="orderSelect('countryselected[]', '-')">
      &nbsp;&nbsp;&nbsp;
      <input type="button" value="Descendre" onClick="orderSelect('countryselected[]', '+')">
    </td>
  </tr>
</table>
<input type="submit" value="Annuler" />
<input type="button" value="Enregistrer" onclick="document.form.save.value='true';allOptionsSelect ('form', 'countryselected[]');form.submit();" />
{/if}
</form>

{$showFooter}

{if $alertToDisplay}
<script type="text/javascript">
<!--
  createCustomConfirm('{#alertBefore#}<br />{$alertToDisplay}{#alertAfter#} ?'); document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteCurrentTask.value='true';form.submit();{literal}}{/literal};
-->
</script>
{/if}
