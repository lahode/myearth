{config_load file=$userLanguage|cat:"/configskin_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

<style type="text/css">
{literal}
select {
  font-size:10px;
}
{/literal}
</style>

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<p class="formTitle">{#persoskin#}</p>

<br />

<form name="form" method=post ENCTYPE=multipart/form-data>
<input type="hidden" name="init_{$pageName}" value="true">
<input type="hidden" name="changeSkin" value="false" />
<input type="hidden" name="deleteSkin" value="" />
<input type="hidden" name="saveSkin" value="false" />
<input type="hidden" name="cancelSkin" value="false" />

<fieldset>
  <br />
  <label style="width:200px">{#selectskin#}</label>
  <label style="width:200px">
  <select name="skin" onchange="form.changeSkin.value='true';form.submit();">
    <option value="">{#newSkin#}</option>
    {if $tableSkins}
    {foreach from=$tableSkins key=skin item=itemSkin}
    {if ($selectedSkin == $skin)}
    <option value="{$skin}" selected=selected>{$skin}</option>
    {else}
    <option value="{$skin}">{$skin}</option>
    {/if}
    {/foreach}
    {/if}
  </select>
  <input type="image" src="{$picPath}admin/refresh.gif" title="{#inforefreshlist#}" onclick="form.changeSkin.value='true';" class="imgButton" class="middle" />
  {if $pathName && $currentStyle<>$selectedSkin}
  <img src="{$picPath}admin/delete.gif" style="cursor:pointer" title="{#infodeleteskin#}" class="middle" onclick="createCustomConfirm('{#areyousure#} {$selectedSkin} ?');
  document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteSkin.value='{$selectedSkin}';form.submit();{literal}}{/literal}" />
  {/if}
  </label>
  <br /><br /><br />

  <label style="width:200px">{#name#}</label>
  <label style="width:200px">
  {if $pathName}
  <input type="hidden" name="name" value="{$selectedSkin}" size="30" maxlength="30" />
  {$selectedSkin}
  {else}
  <input type="text" name="name" value="{$selectedSkin}" size="30" maxlength="30" />
  {/if}
  </label>
  <br /><br /><br />

  <label style="width:200px">{#saveCurrent#}</label>
  <label style="width:200px">
  <input type="checkbox" name="saveCurrent" />
  </label>
  <br /><br /><br />

  <label style="width:200px">{#pictures#}</label>
  <br /><br />
  <label style="width:20px">&nbsp;</label>
  <label style="width:180px">{#header#} (808x190)</label>
  <label style="width:350px">
  <table>
    <tr>
      <td>
        <img id="showImageHeader" name="showImageHeader" src="{if $pathName}{$pathName}header.jpg{else}{$picPath}blank.gif{/if}" height="22" width="70" border="0" class="middle" />
      </td>
      <td>
        <input type="file" id="imgHeader" name="imgHeader" />
      </td>
    </tr>
  </table>
  </label>
  <br /><br />
  <label style="width:20px">&nbsp;</label>
  <label style="width:180px">{#footer#} (808x45)</label>
  <label style="width:350px">
  <table>
    <tr>
      <td>
        <img id="showImageFooter" name="showImageFooter" src="{if $pathName}{$pathName}footer.jpg{else}{$picPath}blank.gif{/if}" height="22" width="70" border="0" class="middle" />
      </td>
      <td>
        <input type="file" id="imgFooter" name="imgFooter" />
      </td>
    </tr>
  </table>
  </label>
  <br /><br /><br />

  <label style="width:240px">{#styles#}</label>
  <br /><br />
  <label style="width:20px">&nbsp;</label>
  <label style="width:180px">{#general#}</label>
    <textarea name="generalCSS" cols="50" rows="12">{$generalToChange}</textarea>
  <br /><br />
  <label style="width:20px">&nbsp;</label>
  <label style="width:180px">{#menu#}</label>
    <textarea name="menuCSS"  cols="50" rows="12">{$menuToChange}</textarea>
  <br /><br />
  <label style="width:20px">&nbsp;</label>
  <label style="width:180px">{#form#}</label>
    <textarea name="formCSS"  cols="50" rows="12">{$formToChange}</textarea>
  <br /><br />
</fieldset>

<br />

<table width="100%">
  <tr>
    <td align="center">
      <input type="submit" value="{#cancel#}" onclick="form.cancelSkin.value='true'" />
    </td>
    <td align="center">
      <input type="submit" value="{#save#}" onclick="form.saveSkin.value='true'" />
    </td>
  </tr>
</table>

</form>

<br /><br />

{if ($error<>'')}
<p class="error">{$error|sslash}</p>
{elseif ($confirm<>'')}
<p class="error">{$confirm|sslash}</p>
{/if}

{$showFooter}
