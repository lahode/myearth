{config_load file=$userLanguage|cat:"/viewcontact_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<form name="form" method="post">
<input type="hidden" name="save" value="false" />
<input type="hidden" name="cancel" value="false" />
<input type="hidden" name="id" value="{$contact->id}" />
<input type="hidden" name="action" value="{$action}" />

<fieldset class="sitetext">
  <br />
  <label style="width:140px">{#name#}</label>
  <label style="width:400px">
  {$contact->title}
  {$contact->firstname}
  {$contact->lastname}
  </label>
  <br /><br />
  <label style="width:140px">{#nickname#}</label>
  <label style="width:400px">
  {$contact->nickname}
  </label>
  <br /><br />
  <label style="width:140px">{#email#}</label>
  <label style="width:400px">
  {$contact->email}
  </label>
  <br /><br />
  <label style="width:140px">{#birthdate#}</label>
  <label style="width:400px">
  {$dayList.$birthday}
  {$monthList.$birthmonth}
  {$birthyear}
  </label>
  <br /><br />
  <label style="width:140px">{#address#}</label>
  <label style="width:400px">
  {$contact->address|nl2br}<br />&nbsp;
  </label>
  <br />
  <label style="width:140px">{#zipcode#}</label>
  <label style="width:400px">
  {$contact->zipcode}&nbsp;{$contact->city}
  </label>
  <br /><br />
  <label style="width:140px">{#country#}</label>
  <label style="width:400px">
  {$contact->country}
  </label>
  <br /><br />
  <label style="width:140px">{#tel#}</label>
  <label style="width:400px"><a href="callto://{$contact->tel}/">{$contact->tel}</a></label>
  <br /><br />
  <label style="width:140px">{#fax#}</label>
  <label style="width:400px">{$contact->fax}</label>
  <br /><br />
  <label style="width:140px">{#mobile#}</label>
  <label style="width:400px"><a href="callto://{$contact->mobile}/">{$contact->mobile}</a></label>
  <br /><br />
  <label style="width:140px">{#messenger#}</label>
  <label style="width:400px">{$contact->messenger}</label>
  <br /><br />
  <label style="width:140px">{#comments#}</label>
  <label style="width:400px">
  {$contact->comments|nl2br}
  </label>
</fieldset>

<br /><br />
<table width="100%">
  <tr>
    <td align="center">
      <input type="submit" value="{#back#}" onclick="form.cancel.value='true'" />
    </td>
  </tr>
</table>

</form>

{$showFooter}
