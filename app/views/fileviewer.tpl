{config_load file=$userLanguage|cat:"/fileviewer_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}
{$xajax_javascript}
</head>
<body id="mainbody" style="background-image:none;">
<form name="form" method="post">
<input type="hidden" name="deletefiles" />
<input type="hidden" name="createdir" />
<input type="hidden" name="renamedir" />
<input type="hidden" name="cutfiles" />
<input type="hidden" name="copyfiles" />
<input type="hidden" name="pastefiles" />
<input type="hidden" name="olddirname" />
<input type="hidden" name="selectdir" />
<input type="hidden" name="init_{$pageName}" value="true">
  <table border="0" width="100%">
    <tr>
      <td colspan="2">
        <table border="0" width="100%">

{******************    Gestion des images *******************************}

          {if ($images)}
          {if ($action<>"showDestination")}
          <tr>
            <td>
              <p class="sitetitle" align="center">{#pictures#}</h4>
            </td>
          </tr>
          {/if}
          <tr><td><table border="0" width="100%">
          {if $selected <> ''}
          {if $viewSinglePicture <> ''}
             <tr>
               <td align="center">
                 <img src="{$viewSinglePicture->temp}{$viewSinglePicture->name}" />
               </td>
             </tr>
             <tr>
               <td class="sitetext" align="center">
                 {$viewSinglePicture->description}
                 {if ($downloadOrigImage && $admin)}<br /><a href="{$downloadOrigImage}" target="_blank" />{#downloadOrigPic#}</a>{/if}
               </td>
             </tr>
          {else}
          {foreach key=keyImage item=image from=$images.$selected name=foo}
            {if ($smarty.foreach.foo.index % $maxPerRow == 0)}
            <tr>
            {/if}
              <td align="center">
                <table border="0">
                  <tr>
                    <td align="center">
                      <a href="?actions={$action}&id={$id}&show={$showImage}&clean={$clean}&selected={$selected}&viewSingle={$image->name}&type={$type}"><img src="{$image->temp}{$image->name}" height="120" border="0" /></a>
                    </td>
                  </tr>
                  <tr>
                    <td class="sitetext" align="center">
                      {if $action=="selectPicture"}
                      <a href="#" onclick="window.opener.document.getElementById('{$id}').value='{$image->name}';
                        {if $showImage<>''}window.opener.document.getElementById('{$showImage}').src='{$image->temp}{$image->name}';{/if}
                        {if $clean<>''}window.opener.document.getElementById('{$clean}').value='';{/if}
                        window.close()">
                          {$action_name}
                      </a>
                      {else}
                      {$image->description|sslash|shorten}
                      {/if}
                    </td>
                  </tr>
                </table>
              </td>
            {if ($smarty.foreach.foo.index % $maxPerRow == $maxPerRow-1 || $smarty.foreach.foo.index==$nbImage.$selected)}
            </tr>
            {/if}
          {/foreach}
          {/if}
          {else}
          {foreach key=keyImage item=image from=$images}
             <tr>
               <td class="sitetext" align="center">
                 <a href="?actions={$action}&id={$id}&show={$showImage}&clean={$clean}&selected={$keyImage}&type={$type}">{$tabImgCategory.$keyImage} ({$nbImage.$keyImage} {#pictures#})</a>
               </td>
             </tr>
          {/foreach}
          {/if}
          </table></td></tr>
          {/if}

{******************    Gestion du son    ********************************}

          {if ($sounds && $viewSinglePicture == '')}
          <tr>
            <td>
              <br />
              <p class="sitetitle" align="center">{#sound#}</p>
            </td>
          </tr>
          {foreach key=keySound item=sound from=$sounds}
            <tr>
              <td align="left">
                <table border="0">
                  <tr>
                    <td align="left">
                      <img src="{$picPath}music.jpg" height="30" />
                    </td>
                    <td class="sitetext">
                      &nbsp;{$sound}
                    </td>
                    <td class="sitetext">
                      {if $action=="download"}
                      <a href="{$soundPath}{$sound}">{$action_name}</a>
                      {elseif $action=="selectSound"}
                      <a href="#" onclick="window.opener.document.getElementById('{$id}').value='{$sound}';
                        {if $showImage<>''}window.opener.document.getElementById('{$showImage}').src='{$picPath}music.jpg';{/if}
                        {if $clean<>''}window.opener.document.getElementById('{$clean}').value='';{/if}
                        {if $title<>''}window.opener.document.getElementById('{$title}').title='{$sound}';{/if}
                        window.close()">
                          {$action_name}
                      </a>
                      {/if}
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          {/foreach}
          {/if}

{******************    Gestion de la vidéo    *****************************}

          {if ($videos && $viewSinglePicture == '')}
          <tr>
            <td>
              <br />
              <p class="sitetitle" align="center">{#video#}</p>
            </td>
          </tr>
          {foreach key=keyVideo item=video from=$videos}
            <tr>
              <td align="left">
                <table border="0">
                  <tr>
                    <td align="left">
                      <img src="{$picPath}video.gif" height="30" />
                    </td>
                    <td class="sitetext">
                      &nbsp;{$video}
                    </td>
                    <td class="sitetext">
                      {if $action=="download"}
                      <a href="{$videoPath}{$video}">{$action_name}</a>
                      {elseif $action=="selectVideo"}
                      <a href="#" onclick="window.opener.document.getElementById('{$id}').value='{$video}';
                        {if $showImage<>''}window.opener.document.getElementById('{$showImage}').src='{$picPath}video.gif';{/if}
                        {if $clean<>''}window.opener.document.getElementById('{$clean}').value='';{/if}
                        window.close()">
                          {$action_name}
                      </a>
                      {/if}
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          {/foreach}
          {/if}

{******************    Gestion des fichiers    **************************}

          {if (($files || $dir)&& $viewSinglePicture == '')}
          <tr>
            <td>
              <br />
              <p class="sitetitle" align="center">{#files#}</p>
            </td>
          </tr>
          <tr>
            <td>
              <img src="{$picPath}admin/checkall.gif" border="0" style="cursor:pointer" class="middle" onclick="toggleContact(document.form.elements, 'directories[]');toggleContact(document.form.elements, 'files[]')" />
              &nbsp;&nbsp;&nbsp;
              <img src="{$picPath}admin/delete.gif" style="cursor:pointer" title="{#infodeletefiles#}" class="middle" onclick="if (checkSelection(document.form.elements, 'directories[]') || checkSelection(document.form.elements, 'files[]')) {literal}{{/literal}createCustomConfirm('{#areyousure#} {#theselectedfiles#} ?');
              document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deletefiles.value='true';form.action='#?actions={$action}&id={$id}&show={$showImage}&clean={$clean}&selected={$selected}&viewSingle={$image->name}&type={$type}';form.submit();{literal}}}{/literal} else alert ('{#errorselected#}', 'errorAlert');" />
              <img src="{$picPath}admin/cut.gif" title="{#infocut#}" style="cursor:pointer" class="middle" onclick = "if (checkSelection(document.form.elements, 'directories[]') || checkSelection(document.form.elements, 'files[]')) {literal}{{/literal}form.cutfiles.value='true';form.action='#?actions={$action}&id={$id}&show={$showImage}&clean={$clean}&selected={$selected}&viewSingle={$image->name}&type={$type}';form.submit();{literal}}{/literal} else alert ('{#errorselected#}', 'errorAlert');" />
              <img src="{$picPath}admin/copy.gif" title="{#infocopy#}" style="cursor:pointer" class="middle" onclick = "if (checkSelection(document.form.elements, 'directories[]') || checkSelection(document.form.elements, 'files[]')) {literal}{{/literal}form.copyfiles.value='true';form.action='#?actions={$action}&id={$id}&show={$showImage}&clean={$clean}&selected={$selected}&viewSingle={$image->name}&type={$type}';form.submit();{literal}}{/literal} else alert ('{#errorselected#}', 'errorAlert');" />
              {if $filestopaste}
              <img src="{$picPath}admin/paste.gif" title="{#infopaste#}" style="cursor:pointer" class="middle" onclick = "form.pastefiles.value='true';form.action='#?actions={$action}&id={$id}&show={$showImage}&clean={$clean}&selected={$selected}&viewSingle={$image->name}&type={$type}';form.submit();" />
              {else}
              <img src="{$picPath}admin/pastenotavailable.gif" class="middle" />
              {/if}
              <img src="{$picPath}admin/dir_create.gif" title="{#infocreatedir#}" style="cursor:pointer" class="middle" onclick = "form.createdir.value='true';form.action='#?actions={$action}&id={$id}&show={$showImage}&clean={$clean}&selected={$selected}&viewSingle={$image->name}&type={$type}';form.submit();" />
            </td>
          </tr>
          <tr>
            <td class="sitetext">
            <div style="margin-top:3px;margin-bottom:15px;"><span class="sitetitle">{#path#} :</span> {$currentDir}</div>
            </td>
          </tr>
            <tr>
              <td align="left">
                <table border="0" cellpadding="0" cellspacing="0">
                {if ($createDir == 'new')}
                  <tr>
                    <td align="left">
                      &nbsp;
                      &nbsp;
                    </td>
                    <td align="left">
                      <img src="{$picPath}dir.gif" height="30" />
                    </td>
                    <td class="sitetext">
                      <input type="text" name="newdirname" value="{$directory}" size="25" maxlength="50" />
                      <img src="{$picPath}admin/confirm.gif" style="cursor:pointer" class="middle" onclick = "form.olddirname.value='{$directory}';form.action='#?actions={$action}&id={$id}&show={$showImage}&clean={$clean}&selected={$selected}&viewSingle={$image->name}&type={$type}';form.submit();" />
                    </td>
                    <td class="sitetext">
                      &nbsp;&nbsp;&nbsp;
                    </td>
                    <td class="sitetext">
                      &nbsp;
                    </td>
                  </tr>
                {/if}
                {foreach key=keyDir item=directory from=$dir}
                  <tr>
                    <td align="left" width="35">
                      {if ($directory<>'..' && $renameDir <> $directory)}
                      <input type="checkbox" value="{$directory}" name="directories[]" />
                      {else}
                      &nbsp;
                      {/if}
                     </td>
                    <td align="left">
                      <img src="{$picPath}dir.gif" height="30" />
                    </td>
                    <td class="sitetext">
                      &nbsp;
                      {if ($renameDir == $directory)}
                      <input type="text" name="newdirname" value="{$directory}" size="25" maxlength="50" />
                      <img src="{$picPath}admin/confirm.gif" style="cursor:pointer" class="middle" onclick = "form.olddirname.value='{$directory}';form.action='#?actions={$action}&id={$id}&show={$showImage}&clean={$clean}&selected={$selected}&viewSingle={$image->name}&type={$type}';form.submit();" />
                      {else}
                      <a href="#" onclick = "form.selectdir.value='{$directory}';form.action='#?actions={$action}&id={$id}&show={$showImage}&clean={$clean}&selected={$selected}&viewSingle={$image->name}&type={$type}';form.submit();">{$directory}</a>
                      {/if}
                    </td>
                    <td class="sitetext">
                      &nbsp;&nbsp;&nbsp;
                    </td>
                    <td class="sitetext">
                      {if ($directory<>'..' && $renameDir <> $directory)}
                      <img src="{$picPath}admin/dir_rename.gif" style="cursor:pointer" title="{#inforenamedir#}" class="middle" onclick = "form.renamedir.value='{$directory}';form.action='#?actions={$action}&id={$id}&show={$showImage}&clean={$clean}&selected={$selected}&viewSingle={$image->name}&type={$type}';form.submit();" />
                      {else}
                      &nbsp;
                      {/if}
                    </td>
                  </tr>
                {/foreach}
                </table>
              </td>
            </tr>
            <tr>
              <td align="left">
                <table border="0" cellpadding="0" cellspacing="0">
                {foreach key=keyFile item=file from=$files}
                  <tr>
                    <td align="left" width="35">
                      <input type="checkbox" value="{$file}" name="files[]" />
                    </td>
                    <td align="left">
                      <img src="{$picPath}file.gif" height="30" />
                    </td>
                    <td class="sitetext">
                      &nbsp;{$file}
                    </td>
                    <td class="sitetext">
                      &nbsp;
                      <a href="{$otherFilesPath}{$file}">{$action_name}</a>
                    </td>
                  </tr>
                {/foreach}
                </table>
              </td>
            </tr>
          {/if}

{************************************************************************}

        </table>
      </td>
    </tr>
    <tr>
      <td class="sitetext" align="center">
      <br />
      {if $selected == '' && $action=="selectPicture"}
      <p style="cursor:pointer" onclick="createCustomConfirm('{#areyousure#} {$category->arrivalCity} ?');document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();go('?actions={$action}&id={$id}&show={$showImage}&clean={$clean}&type={$type}&del=all'){literal}}{/literal}" class="sitetext" style="text-align:center">Supprimer toutes les images non classees</p>
      <br />
      <br />
      {/if}
      {if ($showReturn)}
      <a href="?actions={$action}&id={$id}&show={$showImage}&clean={$clean}&type={$type}" class="sitetitle">{#back#}</a>
      {else}
      <a href="#" class="sitetitle" onclick="window.close()">{#close#}</a>
      {/if}
      <br /><br />
      {if $admin}
      <p class="sitetext" align="center" style="cursor:pointer" onclick="xajax_accessSecure()" /><img height="15" src="{$picPath}lock.gif" /> {#viewSecureFiles#}</p>
      <br /><br />
      {/if}
      </td>
    </tr>
  <table>
</form>
</body>
</html>
