{config_load file=$userLanguage|cat:"/budget_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

<script language="javascript" src="../lib/js/calendar.js"></script>

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<form name="form" method="post">
<input type="hidden" name="save" value="false" />
<input type="hidden" name="deleteCost" />
<input type="hidden" name="editCost" />
<input type="hidden" name="addSecond" />
<input type="hidden" name="insertCost" />
<input type="hidden" name="element" value="2" />
<input type="hidden" name="actionValue" value="{$action}" />
<input type="hidden" name="id" value="{$cost->id}"/>
<input type="hidden" name="category" value="{$cost->category}"/>
<input type="hidden" name="init_{$pageName}" value="true">

<table cellpadding="0" cellspacing="0" width="100%" align="center" border="0">
  <tbody>
    <tr>
      <td class="sitetitle" style="BORDER-BOTTOM: #b3c0d0 1px solid" align=middle width="18" height="18" style="font-size:11px">
        {#date#}&nbsp;
      </td>
      <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
        {#type#}&nbsp;
      </td>
      <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
        {#name#}&nbsp;
      </td>
      <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
        {#city#}&nbsp;
      </td>
      <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
        {#price#}&nbsp;
      </td>
      <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
        {#quality#}&nbsp;
      </td>
      <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
        {#info#}&nbsp;
      </td>
      {if $admin}
      <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18" style="font-size:11px">
        &nbsp;
      </td>
      {/if}
    </tr>
    {foreach key=keyCat item=category from=$categories}
    <tr>
      <td colspan="9" class="sitetitle">
        {$category->arrivalCity}({$category->startingDate|showdate})
      </td>
    </td>
    {if isset($costs.$keyCat)}
    {foreach from=$costs.$keyCat item=itemCost}
    <tr valign="top">
      <td align="left">
        <p class="sitetext" style="font-size:11px">{$itemCost->date|fdate}</p>
      </td>
      <td align="left">
        <p class="sitetext" style="font-size:11px">{$itemCost->type}</p>
      </td>
      <td align="left">
        <p class="sitetext" style="font-size:11px">{$itemCost->name|sslash}</p>
      </td>
      <td align="left">
        {if $itemCost->address}
        <img title="{#address#}" src="{$picPath}info.gif" style="cursor:pointer" onclick="alert ('{$itemCost->address|sslash}', 'infoAlert');" />
        {else}
        &nbsp;
        {/if}
      </td>
      <td align="left">
        <p class="sitetext" style="font-size:11px">{$itemCost->arrivalCity|sslash}</p>
      </td>
      <td align="left">
        <p class="sitetext" style="font-size:11px">{$itemCost->price+0} {$countriescurrency.$keyCat}</p>
      </td>
      <td align="left">
        <p class="sitetext" style="font-size:11px">{$itemCost->quality}</p>
      </td>
      <td align="left">
        {if $itemCost->description}
        <img title="{#moreinfo#}" src="{$picPath}info.gif" style="cursor:pointer" onclick="alert ('{$itemCost->description|sslash}', 'infoAlert');" />
        {else}
        &nbsp;
        {/if}
      </td>
      {if $admin}
      <td align="center">
        <img title="{#infoeditCost#}" src="{$picPath}admin/update.gif" style="cursor:pointer" onclick="form.editCost.value='{$itemCost->id}';form.action='#addCost';form.submit();return true;" />
        <img title="{#infodeleteCost#}" src="{$picPath}admin/delete.gif" style="cursor:pointer" class="medium" onclick="createCustomConfirm('{#areyousure#} {$itemCost->name} ?');
        document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteCost.value='{$itemCost->id}';form.submit();{literal}}{/literal}" />
        {*if !$nextKey || $nextKey == $itemCost->date-$itemCost->date%3600*}
        <img title="{#addSecond#}" src="{$picPath}admin/sortdesc.gif" style="cursor:pointer" onclick="form.addSecond.value='{$itemCost->id}';form.submit();return true;" />
        {*/if*}
        {assign var="nextKey" value=$itemCost->date-$itemCost->date%3600}
      </td>
      {/if}
    </tr>
    {/foreach}
    {/if}
    <tr>
      <td colspan="9">
        <br />
        {if $admin}
        <input type="submit" value="{#insertResto#}" onclick="form.insertCost.value={$category->id};form.action='#addCost'" /><br /><br />
        {else}
        <br />
        {/if}
      </td>
    </tr>
    {/foreach}
  </tbody>
</table>

{if $action=='edit' || $action=='insert'}
<a name="addCost"></a>
<br />
<p class="formTitle">{#addCost#}</p>
<fieldset>
  <br />
  <label style="width:100px">{#country#}</label>
  <label style="width:250px">{$countryToDisplay}</label>
  <br /><br />
  <label style="width:100px">{#date#}</label>
  <label style="width:250px">
  <input type="text" size="30" name="date" value="{$cost->date|fdate}" readonly="readonly" size="20" maxlength="20" />
  <a href="javascript:cal1.popup();">
    <img src="{$picPath}calendar/cal.gif" border="0" title="{#infocalendar#}" width="16" height="16" class="middle" />
  </a>
  </label>
  <br /><br />
  <label style="width:100px">{#type#}</label>
  <label style="width:250px">{html_select tab=$tableFood name='type' selected=$cost->type}</label>
  <br /><br />
  <label style="width:100px">{#name#}</label>
  <label style="width:250px"><input type="text" size="30" name="name" value="{$cost->name|sslash}"/></label>
  <br /><br />
  <label style="width:100px" size="60">{#address#}</label>
  <label style="width:250px"><textarea name="address">{$cost->address|sslash}</textarea></label>
  <br /><br /><br />
  <label style="width:100px">{#city#}</label>
  <label style="width:250px"><input type="text" name="arrivalCity" size="30" value="{$cost->arrivalCity|sslash}" /></label>
  <br /><br />
  <label style="width:100px">{#price#}</label>
  <label style="width:250px"><input type="text" name="price" size="7" value="{$cost->price}" /> {$countrycurrency}</label>
  <br /><br />
  <label style="width:100px">{#quality#}</label>
  <label style="width:250px">{html_select tab=$tableQuality name='quality' selected=$cost->quality}</label>
  <br /><br />
  <label style="width:100px">{#description#}</label>
  <label style="width:250px"><input type="text" name="description" size="30" value="{$cost->description|sslash}" /></label>
  <br /><br />

  <p align="center"><input type="submit" value="{if $action=="edit"}{#edit#}{else}{#insert#}{/if}" onclick="form.save.value='true'" /></p>
</fieldset>

<script language="JavaScript">
<!-- // create calendar object(s) just after form tag closed
  var cal1 = new calendar1(document.forms['form'].elements['date'], 'date', '{$dateFormat}');
  cal1.year_scroll = true;
  cal1.time_comp = false;
//-->
</script>
{/if}

</form>

{$showFooter}
