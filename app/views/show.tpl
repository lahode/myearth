{$showTopHeader}

<link rel="stylesheet" type="text/css" href="../lib/css/worldmap.css">

<script language="javascript" src="../lib/js/phpTab2js.js"></script>
<script language="javascript" src="../lib/js/world.js" type=text/javascript></script>
<script language="javascript" src="../lib/js/clock.js" type=text/javascript></script>
<script language="javascript" src="../lib/js/dw_scrollObj.js" type=text/javascript></script>
<script language="javascript" src="../lib/js/dw_hoverscroll.js" type=text/javascript></script>
<script TYPE="text/javascript" SRC="../lib/js/slideshow.js" type=text/javascript></script>

{$xajax_javascript}

<script type="text/javascript">
{literal}
var fadein_opacity = 0.04;
var fadeout_opacity = 1;
var fadein_img = null;
var SLIDES = null;

function clockajust(ajust) {
//  setDecalage(ajust);
}

function addImageToSlide (imagesList, descriptionList) {
  SLIDES = new slideshow("SLIDES");
  if (imagesList) {
    for (i=0;i<imagesList.length;i++) {
      s = new slide();
      s.src = '{/literal}{$webImgPath}{literal}'+imagesList[i];
      ttt = descriptionList[i].replace("&#039;", "'");
      s.text = htmldecode(descriptionList[i]);
      SLIDES.add_slide(s);
    }
    SLIDES.update();
  }
  if (document.images) {
    SLIDES.set_image(document.images.SLIDESIMG);
    SLIDES.set_textid("SLIDESTEXT"); // optional
    SLIDES.update();
  }
  fadein_img = SLIDES.image;
  SLIDES.post_update_hook = function() { fadeout(0.04); }
  SLIDES.post_update_hook = function() { fadein(0.04); }
}

function fadein(opacity) {
  if (typeof opacity != 'undefined') fadein_opacity = opacity;
  if (fadein_opacity < 0.99 && fadein_img && fadein_img.style &&
      typeof fadein_img.style.MozOpacity != 'undefined') {
    fadein_opacity += .05;
    fadein_img.style.MozOpacity = fadein_opacity;
    setTimeout("fadein()", 30);
  }
}

function fadeout(opacity) {
  if (typeof opacity != 'undefined') fadein_opacity = opacity;
  if (fadeout_opacity > 0.05 && fadein_img && fadein_img.style &&
      typeof fadein_img.style.MozOpacity != 'undefined') {
    fadeout_opacity -= .05;
    fadein_img.style.MozOpacity = fadeout_opacity;
    setTimeout("fadeout()", 30);
  }
}
{/literal}
</script>

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0"
onload = "{if $details}initClock({$decalTime});{/if};{if ($dest)}xajax_showDestination({$dest});{elseif ($cat)}xajax_showCountry({$cat});{/if}">

{$showHeader}

<div id="listenSound">{$player}</div>
<div id="headerShow"></div>

{if $details}
<div style="float:right;margin-right:20px;margin-top:50px">
<div id="marker" style="position:relative;overflow:hidden;width:97px;height:110px"></div>
<div id="roof" style="position:absolute;overflow:hidden;width:97px;height:110px">
<div id="imgClock" style="position:relative;visibility:hidden;"><img width=97 height=97 src="{$picPath}clock/clock.gif"></div>
<div id="textDate" style="position:absolute;visibility:hidden;width:20px;font-family:Arial;font-size:7pt;color:#EEEEEE;"> </div>
<div id="textDay" style="position:absolute;visibility:hidden;width:20px;font-family:Arial;font-size:7pt;color:#EEEEEE;"> </div>
<div id="imgHours" style="position:absolute;visibility:hidden;overflow:hidden;"><img src="{$picPath}clock/hours.gif"></div>
<div id="imgMinutes" style="position:absolute;visibility:hidden;overflow:hidden;"><img src="{$picPath}clock/minutes.gif"></div>
<div id="imgSeconds" style="position:absolute;visibility:hidden;overflow:hidden;"><img src="{$picPath}clock/seconds.gif"></div>
</div>
</div>
{/if}

<div id="descriptionShow"></div>

<div id="mapShow" style="text-align:left"></div>
<hr />

<div id="contentShow"></div>

{$showFooter}
