{config_load file=$userLanguage|cat:"/"|cat:$pageName|cat:"_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

<style type="text/css">
<!--
{literal}
.cadreAgenda {
{/literal}
	background-repeat:no-repeat;
	background-position:bottom right;
	border-style:solid;
	border-width:1px;
	border-top:hidden;
        border-color:{$borderColor};
{literal}
}

.reserve {
{/literal}
	background-color:{$occupedColor};
{literal}
}

.aujourdhui {
{/literal}
	background-color:{$borderColor};
{literal}
}
{/literal}
-->
</style>

<script type="text/javascript" src="../lib/js/calendar.js"></script>

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}
<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0" class="cadreAgenda">
  <tr>
    <td height="51" colspan="7">
      <table width="381" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="290" class="sitetitle" style="font-size:24px;font-weight:bold;font-style:italic;">{$fullDate|showDate}</td>
          <td width="50">
            <a href="?month={$lastMonth}&year={$lastYear}&day={$day}">
            <div align="left"><img src="{$picPath}previous.gif" /></div></a>
          </td>
          <td width="41">
            <a href="?month={$nextMonth}&year={$nextYear}&day={$day}">
            <div><img src="{$picPath}next.gif" /></div>
            </a>          
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr align="center" class="sitetitle" style="text-align:center">
    <td width="60">D</td>
    <td width="60">L</td>
    <td width="60">M</td>
    <td width="60">M</td>
    <td width="60">J</td>
    <td width="60">V</td>
    <td width="60">S</td>
  </tr>
</table>

<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0" class="cadreAgenda">
  <tr align="center">
  {section name=foo start=0 loop=7 step=1}
    {if $smarty.section.foo.index < $firstDay}<td width="60">&nbsp;</td>
    {else}
    {math equation="x + 1 - y" x=$smarty.section.foo.index y=$firstDay assign=thisDay}
    {if $dayTab.$thisDay} <td title="{$dayTab.$thisDay} {#tasks#}" width="60" {if $thisDay==$day}class="aujourdhui" {else}class="reserve" {/if}style="cursor:pointer" onclick="go('?month={$month}&year={$year}&day={$thisDay}')">
    {else}
    <td width="60" style="cursor:pointer" {if $thisDay==$day}class="aujourdhui" {/if}onclick="go('?month={$month}&year={$year}&day={$thisDay}')">
    {/if}
    <span class="sitetext">{$thisDay}</span></td>
    {/if}
  {/section}
  {math equation="x + 1 - y" x=$smarty.section.foo.index y=$firstDay assign=nextDay}
  {section name=foo2 start=0 loop=5 step=1}
  </tr>
  <tr align="center" class="numero">
    {section name=foo3 start=0 loop=7 step=1}
    {if $nextDay > $lastDay}
    <td width="60">&nbsp;</td>
    {else}
    {if $dayTab.$nextDay} <td title="{$dayTab.$nextDay} {#tasks#}" width="60" {if $nextDay==$day}class="aujourdhui" {else}class="reserve" {/if} style="cursor:pointer" onclick="go('?month={$month}&year={$year}&day={$nextDay}')">
    {else}
    <td width="60" style="cursor:pointer" {if $nextDay==$day}class="aujourdhui" {/if}onclick="go('?month={$month}&year={$year}&day={$nextDay}')">
    {/if}
    <span class="sitetext">{$nextDay++}</span></td>
    {/if}
    {/section}
  {/section}
  </tr>
</table>

<br />

<form name="form" method="post">
<input type="hidden" name="cancel" value="false" />
<input type="hidden" name="save" value="false" />
<input type="hidden" name="editTask" value="" />
<input type="hidden" name="deleteTask" value="" />
<input type="hidden" name="init_{$pageName}" value="true">
<input type="hidden" name="id" value="{$task->id}">

{if $tasksToShow}
<br />
<table cellspacing="0" cellpadding="0" width="100%" border="0">
  <tr>
    <td>
      <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tbody>
          <tr>
            <td class="sitetitle" style="BORDER-BOTTOM: #b3c0d0 1px solid" align=middle width="18" height="18">&nbsp;</td>
            <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18">{#task#}</td>
            <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18">{#description#}</td>
            <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18">{#start#}</td>
            <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18">{#end#}</td>
            <td class="sitetitle" style="PADDING-LEFT: 3px; BORDER-LEFT: #b3c0d0 1px solid; BORDER-BOTTOM: #b3c0d0 1px solid" height="18">{#actions#}</td>
          </tr>
          {foreach from=$tasksToShow item=itemTask}
          <tr>
            <td class="sitetext"><img src="{$picPath}agenda/{$itemTask->type}.gif" />&nbsp;</td>
            <td class="sitetext">{$itemTask->subject|sslash}</td>
            <td class="sitetext">{$itemTask->description|sslash}</td>
            <td class="sitetext">{$itemTask->start|showDate} {#at#} {$itemTask->start|showHour}:{$itemTask->start|showMinute}</td>
            <td class="sitetext">{$itemTask->end|showDate} {#at#} {$itemTask->end|showHour}:{$itemTask->end|showMinute}</td>
            <td class="sitetext" height="21">
              <img title="{#infoedittask#}" style="cursor:pointer" src="{$picPath}admin/update.gif" onclick="form.editTask.value='{$itemTask->id}';form.submit();" />
              <img title="{#infodeletetask#}" style="cursor:pointer" src="{$picPath}admin/delete.gif" onclick="createCustomConfirm('{#areyousure#} {#msgof#} {$itemTask->subject} ?');
              document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteTask.value='{$itemTask->id}';form.submit();{literal}}{/literal}" />
            </td>
          </tr>
          {/foreach}
        </tbody>
      </table>
    </td>
  </tr>
</table>
<br />
<br />
{/if}

<p class="formTitle">{if $action=="edit"}{#editTask#}{else}{#addTask#}{/if}</p>
<fieldset>
  <br />
  <label style="width:120px">{#type#}</label>
  <label style="width:350px">{html_select tab=$tabType name=type selected=$task->type}</label>
  <br /><br />
  <label style="width:120px">{#subject#}</label>
  <label style="width:350px"><input type="text" size="30" maxlength="30" name="subject" value="{$task->subject}" /></label>
  <br /><br />
  <label style="width:120px">{#description#}</label>
  <label style="width:350px"><input type="text" name="description" size="60" value="{$task->description|sslash}" /></label>
  <br /><br />
  <label style="width:120px">{#start#}</label>
  <label style="width:350px">
  <table cellpadding="0" cellspacing="0" >
    <tr>
      <td>
        <input type="text" name="start" value="{$task->start|fdate}" size="15" readonly="readonly">
      </td>
      <td>
        <a href="javascript:cal1.popup();"><img src="{$picPath}calendar/cal.gif" border="0" title="{#infocalendar#}" width="16" height="16" class="middle" /></a>
      </td>
      <td>
        &nbsp;{html_select tab=$tabHour name=hourStart selected=$task->start|getHour} : {html_select tab=$tabMinute name=minuteStart selected=$task->start|getMinute}
      </td>
    </tr>
  </table>
  </label>
  <br /><br />
  <label style="width:120px">{#end#}</label>
  <label style="width:350px">
  <table cellpadding="0" cellspacing="0" >
    <tr>
      <td>
        <input type="text" name="end" value="{$task->end|fdate}" size="15" readonly="readonly">
      </td>
      <td>
        <a href="javascript:cal2.popup();"><img src="{$picPath}calendar/cal.gif" border="0" title="{#infocalendar#}" width="16" height="16" class="middle" /></a>
      </td>
      <td>
        &nbsp;{html_select tab=$tabHour name=hourEnd selected=$task->end|getHour} : {html_select tab=$tabMinute name=minuteEnd selected=$task->end|getMinute}
      </td>
    </tr>
  </table>
  </label>
  <br /><br />
  <label style="width:120px">{#reminder#}</label>
  <label style="width:350px">{html_select tab=$tabReminder name=reminder selected=$task->reminder}</label>
  <br />
  <!--
  <br />
  <label style="width:400px">{#block#}</label>
  <label style="width:100px"><input type="checkbox" {if $task->block==1}checked="checked"{/if} id="block" name="block" /></label>
  <br />
  -->
</fieldset>
<br />
<table width="100%">
  <tr>
    <td align="center">
      <input type="submit" value="{#cancel#}" onclick="form.cancel.value='true'" />
    </td>
    <td align="center">
      <input type="submit" value="{if $action=="edit"}{#edit#}{else}{#insert#}{/if}" onclick="form.save.value='true'" />
    </td>
  </tr>
</table>

<br />
<hr />
<br />

<input type="hidden" name="synchroBirthday" value="false" />
<p class="sitetext" style="cursor:pointer" align="center" onclick="createCustomConfirm('{#areyousureCreate#} ?');
  document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.synchroBirthday.value='true';form.submit();{literal}}{/literal}">{#synchroBirthday#}</p>
</form>

<script language="JavaScript">
<!-- // create calendar object(s) just after form tag closed
  var cal1 = new calendar1(document.forms['form'].elements['start'], 'start', '{$property->dateFormat}');
  cal1.year_scroll = true;
  cal1.time_comp = false;
  var cal2 = new calendar1(document.forms['form'].elements['end'], 'end', '{$property->dateFormat}');
  cal2.year_scroll = true;
  cal2.time_comp = false;
//-->
</script>

{$showFooter}
