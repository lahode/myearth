{config_load file=$userLanguage|cat:"/security_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<form name="form" method=post ENCTYPE=multipart/form-data>
<input type="hidden" name="cancel" value="false" />
<input type="hidden" name="save" value="false" />

<p class="formTitle">{#editPassword#}</p>

<fieldset>
  <br />
  <label style="width:250px">{#presentPassword#}</label>
  <input type="password" name="presentPassword" size="14" maxlength="12" />
  <br /><br />

  <label style="width:250px">{#newPassword#}</label>
  <input type="password" name="newPassword" size="14" maxlength="12" />
  <br /><br />

  <label style="width:250px">{#confirmPassword#}</label>
  <input type="password" name="confirmPassword" size="14" maxlength="12" />
  <br /><br />
</fieldset>

<p class="formTitle">{#secureSite#}</p>

<fieldset>
  <br />
  <label style="width:250px">{#guestPassword#}</label>
  <input type="text" name="guestPassword" value="{$guestPassword}" size="14" maxlength="12" />
  <br /><br />
</fieldset>

<br /><br />

<table width="100%">
  <tr>
    <td align="center">
      <input type="submit" value="{#cancel#}" onclick="form.cancel.value='true'" />
    </td>
    <td align="center">
      <input type="submit" value="{#save#}" onclick="form.save.value='true'" />
    </td>
  </tr>
</table>

</form>

{$showFooter}
