{config_load file="file.conf"}
{config_load file=$userLanguage|cat:"/main_"|cat:$userLanguage|cat:".conf"}

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<title>My Earth</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" type="text/css" href="{$skinPath}header.css">
<link rel="stylesheet" type="text/css" href="{$skinPath}general.css">
<link rel="stylesheet" type="text/css" href="{$skinPath}form.css">
<link rel="stylesheet" type="text/css" href="../lib/css/style.css">
<link rel="stylesheet" type="text/css" href="{$skinPath}menu.css">
<link rel="stylesheet" type="text/css" href="../lib/css/alert.css">
<link rel="stylesheet" type="text/css" href="../lib/css/chatpopup.css">
<link rel="stylesheet" type="text/css" href="../lib/css/chat.css" />
<link rel="stylesheet" type="text/css" href="../lib/css/waitloading.css" />

{if $path && !$cssOutput}
<link rel="stylesheet" type="text/css" href="{$path}css/perso.css">
<link rel="stylesheet" type="text/css" href="{$path}css/maintitle.css">
<link rel="stylesheet" type="text/css" href="{$path}css/pagetitle.css">
<link rel="stylesheet" type="text/css" href="{$path}css/menutitle.css">
<link rel="stylesheet" type="text/css" href="{$path}css/menutext.css">
<link rel="stylesheet" type="text/css" href="{$path}css/sitetitle.css">
<link rel="stylesheet" type="text/css" href="{$path}css/sitetext.css">
<link rel="stylesheet" type="text/css" href="{$path}css/persotext.css">
<link rel="stylesheet" type="text/css" href="{$path}css/navigation.css">
{elseif $cssOutput}
{$cssOutput}
{else}
<link rel="stylesheet" type="text/css" href="../lib/css/default.css">
{/if}

<!--[if gte IE 5.5]>
<![if lt IE 7]>
<link rel="stylesheet" type="text/css" href="../lib/css/fixposition.css">
<![endif]>
<![endif]-->

<script type="text/javascript">
<!--
  var timeout = null;
  var ERROR_TITLE = "{#errorAlert#}"
  var CONFIRM_TITLE = "{#confirmAlert#}"
  var REQUEST_TITLE = "{#requestAlert#}"
  var CONFIRM_BUTTON_TEXT1 = "{#no#}"
  var CONFIRM_BUTTON_TEXT2 = "{#yes#}"
//-->
</script>

<script language="javascript" src="../lib/js/functions.js" type=text/javascript></script>
<script language="javascript" src="../lib/js/alert.js" type=text/javascript></script>
<script language="javascript" src="../lib/js/chatpopup.js" type=text/javascript></script>
<script language="javascript" src="../lib/js/chat.js"></script>
<script language="javascript" src="../lib/js/regexp.js"></script>
<script language="javascript" src="../lib/js/waitloading.js"></script>

<!-- End TopHeader -->
