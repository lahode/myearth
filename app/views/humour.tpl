{config_load file=$userLanguage|cat:"/humour_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<form name="form" method="post" ENCTYPE=multipart/form-data>
<input type="hidden" name="actions" value="{$action}">
<input type="hidden" name="editMessage">
<input type="hidden" name="deleteMessage">
<input type="hidden" name="changeAccess">

<input type="hidden" name="deleteImage" value="" />
<input type="hidden" name="addImage" value="false" />
<input type="hidden" name="cancel" value="false" />
<input type="hidden" name="save" value="false" />
<input type="hidden" name="init_{$pageName}" value="true">

{* Split display for messages *}
{if $splitDisplay->total > $splitDisplay->length}
  <div class="navigation">
  {if $splitDisplay->start > 0}
    <a href="?next={$splitDisplay->start-$splitDisplay->maxNum}"><<<a>&nbsp;
  {/if}
  {section name=foo start=$splitDisplay->start loop=$splitDisplay->loop max=$splitDisplay->maxNum}
    {if ($smarty.section.foo.index==$splitDisplay->first)}
      {$smarty.section.foo.index+1}
    {else}
      <a href="?page={$smarty.section.foo.index*$splitDisplay->length}&next={$splitDisplay->start}">{$smarty.section.foo.index+1}<a>
    {/if}
  {/section}
  {if $splitDisplay->start+$splitDisplay->maxNum < $splitDisplay->loop}
    &nbsp;<a href="?next={$splitDisplay->start+$splitDisplay->maxNum}">>><a>
  {/if}
  </div>
{/if}

{foreach from=$message key=keymsg item=itemMessage}
<div class="cadresitetitle">
  <p class="sitetitle">
    {if ($admin)}
    {if ($itemMessage->access=='public')}
    &nbsp;<input type="image" src="{$picPath}admin/enable.gif" class="imgButton" onclick="form.changeAccess.value='{$itemMessage->id}'" />
    {else}
    &nbsp;<input type="image" src="{$picPath}admin/disable.gif" class="imgButton" onclick="form.changeAccess.value='{$itemMessage->id}'" />
    {/if}
    {/if}
    <img src="{$picPath}smile.gif" width="22" height="21" class="middle" /> {#postedOn#} {$itemMessage->date|showdate}
    {if ($admin)}
    &nbsp;<input type="image" src="{$picPath}admin/update.gif" class="imgButton" onclick="form.editMessage.value='{$itemMessage->id}'" />
    &nbsp;<img src="{$picPath}admin/delete.gif" style="cursor:pointer" class="middle" onclick="createCustomConfirm('{#areyousure#} {$itemMessage->subject} ?');
    document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteMessage.value='{$itemMessage->id}';form.submit();{literal}}{/literal}" />
    {/if}
  </p>
  <span class="sitetitle">{#subject#} : </span><span class="persotext">{$itemMessage->subject|sslash}</span>
  <p class="persotext">{$itemMessage->text|sslash|nl2br}</p>
  {if $itemMessage->images}
  <table cellpadding="0" cellspacing="0" align="center">
  {foreach from=$itemMessage->images key=keyImage item=image}
    {if ($keyImage % 2 == 0)}
    <tr>
    {/if}
      <td align="center">
        <table border="0">
          <tr valign="middle">
            <td align="center">
              <img src="{$imgPath}{$image->name}" height="180" />
            </td>
          </tr>
          <tr>
            <td align="center" style="font-size:11px">
              {$image->description|sslash|nl2br}
              <br />
            </td>
          </tr>
        </table>
      </td>
    {if ($keyImage % 2 == 2 || $keyImage==($itemMessage->images|@count)-1)}
    </tr>
    {/if}
  {/foreach}
  </table>
  {/if}
</div>
{/foreach}

{* Split display for messages *}
{if $splitDisplay->total > $splitDisplay->length}
  <div class="navigation">
  {if $splitDisplay->start > 0}
    <a href="?next={$splitDisplay->start-$splitDisplay->maxNum}"><<<a>&nbsp;
  {/if}
  {section name=foo start=$splitDisplay->start loop=$splitDisplay->loop max=$splitDisplay->maxNum}
    {if ($smarty.section.foo.index==$splitDisplay->first)}
      {$smarty.section.foo.index+1}
    {else}
      <a href="?page={$smarty.section.foo.index*$splitDisplay->length}&next={$splitDisplay->start}">{$smarty.section.foo.index+1}<a>
    {/if}
  {/section}
  {if $splitDisplay->start+$splitDisplay->maxNum < $splitDisplay->loop}
    &nbsp;<a href="?next={$splitDisplay->start+$splitDisplay->maxNum}">>><a>
  {/if}
  </div>
{/if}

{if $admin}
<a name="textentry"></a>

<p class="formTitle">{if $action=='new'}{#newMessage#}{else}{#editMessage#}{/if}</p>

<fieldset>
  <br />
  <input type="hidden" name="id" value="{$newMessage->id}" />
  <label style="width:100px">{#subject#}</label>
  <input type="text" name="subject" value="{$newMessage->subject|sslash}" />
  <br /><br />

  <label style="width:100px">{#message#}</label>
  <textarea name="text" id="text" cols="70" rows="7"
    onselect="storeCaret(this);"
    onclick="storeCaret(this);"
    onkeyup="storeCaret(this);">{$newMessage->text|sslash}</textarea>
  <br /><br />

  {$showEmote}
  <br /><br />
</fieldset>

<fieldset>
  <label style="width:200px">{#pictureName#}<br />{#max4img#}</label>
  {$imgHumour}
  <br /><br />

  <label style="width:200px">{#descriptionImg#}</label>
  <textarea name="imageDescription" id="imageDescription" cols="50" rows="2"></textarea>
  <br /><br /><br />
  <p align="center"><input type="submit" value="{#addPicture#}" onclick="form.addImage.value='save';form.action='#textentry'" /></p>
</fieldset>

{if ($newMessage->images)}
<table width="100%">
  <tr>
  {foreach key=keyImage item=image from=$newMessage->images}
    <td align="center">
      <table border="0">
        <tr valign="middle">
          <td>
            <img src="{$image->temp}{if ($keyImage|count_characters)==4}{$image->name}{else}{$keyImage}.tmp{/if}" height="60" />
          </td>
          <td>
            &nbsp;<img src="{$picPath}admin/delete.gif" style="cursor:pointer" class="middle" onclick="createCustomConfirm('{#areyousure#} {$image->name} ?');
            document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteImage.value='{$keyImage}';form.submit();{literal}}{/literal}" />
          </td>
        </tr>
        <tr>
          <td align="center" colspan="2" style="font-size:9px">
            {$image->description|sslash}
            <br />
          </td>
        </tr>
      </table>
    </td>
  {/foreach}
  </tr>
</table>
{/if}
<br />
<table width="100%">
  <tr>
    <td align="center">
      <input type="submit" value="{#cancel#}" onclick="form.cancel.value='true'" />
    </td>
    <td align="center">
      <input type="submit" value="{#save#}" onclick="form.save.value='true'" />
    </td>
  </tr>
</table>
{/if}

</form>

{$showFooter}
