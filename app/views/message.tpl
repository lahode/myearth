{config_load file=$userLanguage|cat:"/message_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<table cellpadding="0" cellspacing="0" width="100%">
  {foreach from=$message item=itemMessage}
  <tr>
    <td><b>{#receivedOn#} {$itemMessage->date} {#at#}  {$itemMessage->time}</b></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td style="text-indent:30px">{$itemMessage->subject}</td>
  </tr>
  <tr>
    <td><br /><hr /><br /></td>
  </tr>
  {/foreach}
</table>

{$showFooter}
