{config_load file=$userLanguage|cat:"/config_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

<link rel="stylesheet" type="text/css" href="../lib/css/ColorPicker.css" />

<script type="text/javascript" src="../lib/js/calendar.js"></script>
<script type="text/javascript" src="../lib/js/CP_Class.js"></script>

{literal}
<script type="text/javascript">
  window.onload = function() {
    fctLoad();
  }                                  
  window.onscroll = function() {
    fctShow();
  }
  window.onresize = function() {
    fctShow();
  }
</script>
{/literal}

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<form name="form" method=post ENCTYPE=multipart/form-data>
<input type="hidden" name="cancelConfig" value="false" />
<input type="hidden" name="save" value="false" />
<input type="hidden" name="addEmail" value="false" />
<input type="hidden" name="editFont" value="" />
<input type="hidden" name="editSkin" value="" />
<input type="hidden" name="changeSkin" value="false" />
<input type="hidden" name="deleteEmail" />
<input type="hidden" name="init_{$pageName}" value="true">
<input type="hidden" name="deleteImgHome" />
<input type="hidden" name="deleteImgBg" />
<input type="hidden" name="deletePerso" />

<a name="generalConf">
<p class="formTitle">{#generalConf#}</p>

<fieldset>
  <br />
  <label style="width:240px">{#title#}</label>
  <input type="text" name="albumTitle" value="{$albumTitle|sslash}" size="30" maxlength="30" />
  <br /><br />

  <label style="width:240px">{#owner#}</label>
  <input type="text" name="albumOwner" value="{$albumOwner|sslash}" size="30" maxlength="50" />
  <br /><br />

  {if $superadmin}
  <label style="width:240px">{#quota#}</label>
  <input type="text" name="quota" value="{$quota}" size="4" maxlength="4" /> {#mb#}
  <br /><br />
  {/if}

  <label style="width:240px">{#emails#}</label>
  <input type="text" name="newemail" size="30" maxlength="30" /> <input type="submit" value="{#add#}" onclick="form.addEmail.value='save'" />
  <br /><br />
  {foreach from=$emails key=keyEmail item=itemEmail}
  <label style="width:240px">&nbsp;</label>
  <span style="font-size:13px;">
    {$itemEmail}<input type="hidden" name="email[{$keyEmail}]" value='{$itemEmail}' />
    &nbsp;<img src="{$picPath}admin/delete.gif" style="cursor:pointer" title="{#infodeleteemail#}" class="middle" onclick="createCustomConfirm('{#areyousure#} {$itemEmail} ?');
    document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteEmail.value={$keyEmail};form.submit();{literal}}{/literal}" />
  </span>
  <br />
  {/foreach}
  <br />

  <label style="width:240px">{#ftpaddress#}</label>
  <label style="width:40px">ftp://</label><input type="text" name="ftpaddress" value="{if $ftpaddress}{$ftpaddress}{/if}" size="30" maxlength="30" />
  <br /><br />

  <label style="width:240px">{#homeMsg#}</label>
  <textarea name="homeMsg" id="homeMsg" cols="35" rows="5">{$home->message|sslash}</textarea>
  <br /><br />

  <label style="width:240px">{#imgMsg#}</label>
  {$imgHome}
  <img src="{$picPath}admin/delete.gif" style="cursor:pointer" title="{#infodeletepics#}" class="middle" onclick="createCustomConfirm('{#areyousure#} {#image#} ?');document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteImgHome.value='true';form.submit();{literal}}{/literal}" />

  <br /><br />
  <label style="width:240px">{#persopage#}</label>
  {if ($persoFile)}<a href="{$persoFile}" title="{#downloadfile#}"><img src="{$picPath}file.gif" title="{#downloadfile#}" class="middle" /></a>{/if}
  <input type="file" name="perso" value="{$persoPage}" />
  <img src="{$picPath}admin/delete.gif" style="cursor:pointer" title="{#infodeleteperso#}" class="middle" onclick="createCustomConfirm('{#areyousure#} {#yourpersopage#} ?');document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deletePerso.value='true';form.submit();{literal}}{/literal}" />

  <br /><br />
</fieldset>


<a name="departure">
<p class="formTitle">{#departure#}</p>


<fieldset>
  <br />
  <label style="width:240px">{#departureCountry#}</label>
  {if $tableCountry}
  {html_select obj=$tableCountry name='country' id='iso2' url="#" value='name' default='Veuillez sélectionner un pays' selected=$departure->country}&nbsp;
  <input type="image" src="{$picPath}admin/refresh.gif" title="{#inforefreshlist#}" class="imgButton" class="middle" />
  {/if}
  <br /><br />

  {if $tableRegion}
  <label style="width:240px">{#departureCity#}</label>
  {html_select obj=$tableRegion name='region' id='admin' value='name' default='Veuillez sélectionner une région' selected=$departure->region}&nbsp;
  <br /><br />
  {/if}

  {if $tableCity}
  <label style="width:240px">{#departureRegion#}</label>
  {html_select obj=$tableCity name='city' id='id' value='name' default='Veuillez sélectionner une ville' selected=$departure->city}&nbsp;
  <br /><br />
  {/if}

  <label style="width:240px">{#cityifnotfound#}</label>
  <input type="text" name="cityName" value="{$departure->cityName|sslash}" />
  <br /><br />

  <label style="width:240px">{#departureDate#}</label>
  <input type="text" name="startingDate" value="{$departure->startingDate|fdate}" size="30" readonly="readonly">
  <a href="javascript:cal1.popup();"><img src="{$picPath}calendar/cal.gif" border="0" title="{#infocalendar#}" width="16" height="16" class="middle" /></a>
  <br /><br />

  <label style="width:240px">{#arrivalDate#}</label>
  <input type="text" name="endingDate" value="{$departure->endingDate|fdate}" size="30" readonly="readonly">
  <a href="javascript:cal2.popup();"><img src="{$picPath}calendar/cal.gif" border="0" title="{#infocalendar#}" width="16" height="16" class="middle" /></a>
  <br /><br />

  <label style="width:240px">{#budget_accommodation_planed#}</label>
  <input type="text" name="budget_accommodation_planed" value="{$departure->budget_accommodation_planed}" size="30" maxlength="30" />
  <br /><br />

  <label style="width:240px">{#budget_food_planed#}</label>
  <input type="text" name="budget_food_planed" value="{$departure->budget_food_planed}" size="30" maxlength="30" />
  <br /><br />

  <label style="width:240px">{#budget_transport_planed#}</label>
  <input type="text" name="budget_transport_planed" value="{$departure->budget_transport_planed}" size="30" maxlength="30" />
  <br /><br />

  <label style="width:240px">{#budget_other_planed#}</label>
  <input type="text" name="budget_other_planed" value="{$departure->budget_other_planed}" size="30" maxlength="30" />
  <br /><br />
</fieldset>


<a name="properties">
<p class="formTitle">{#properties#}</p>


<fieldset>
  <br />
  <label style="width:150px;clear:both">{#fontName#}</label>
  <label style="width:250px">
    {html_select tab=$tableFont name=prop_font selected=$property->font}&nbsp;
  </label>
  <br /><br />

  <label style="width:150px;clear:both">{#maintitle#}</label>
  <label style="width:150px">
    <input type="image" src="{$picPath}admin/update.gif" title="{#infomaintitle#}" class="imgButton" onclick="form.editFont.value='maintitle';" class="middle" />
  </label>
  <label style="width:150px">{#pagetitle#}</label>
    <input type="image" src="{$picPath}admin/update.gif" title="{#infopagetitle#}" class="imgButton" onclick="form.editFont.value='pagetitle';" class="middle" />
  <br /><br />

  <label style="width:150px;clear:both">{#menutitle#}</label>
  <label style="width:150px">
    <input type="image" src="{$picPath}admin/update.gif" title="{#infomenutitle#}" class="imgButton" onclick="form.editFont.value='menutitle';" class="middle" />
  </label>
  <label style="width:150px">{#menutext#}</label>
    <input type="image" src="{$picPath}admin/update.gif" title="{#infomenutext#}" class="imgButton" onclick="form.editFont.value='menutext';" class="middle" />
  <br /><br />

  <label style="width:150px;clear:both">{#sitetitle#}</label>
  <label style="width:150px">
    <input type="image" src="{$picPath}admin/update.gif" title="{#infositetitle#}" class="imgButton" onclick="form.editFont.value='sitetitle';" class="middle" />
  </label>
  <label style="width:150px">{#sitetext#}</label>
    <input type="image" src="{$picPath}admin/update.gif" title="{#infositetext#}" class="imgButton" onclick="form.editFont.value='sitetext';" class="middle" />
  <br /><br />

  <label style="width:150px;clear:both">{#persotext#}</label>
  <label style="width:150px">
    <input type="image" src="{$picPath}admin/update.gif" title="{#infopersotext#}" class="imgButton" onclick="form.editFont.value='persotext';" class="middle" />
  </label>
  <label style="width:150px">{#navigation#}</label>
    <input type="image" src="{$picPath}admin/update.gif" title="{#infonavigation#}" class="imgButton" onclick="form.editFont.value='navigation';" class="middle" />
  <br /><br />

  <label style="width:150px;clear:both">{#bgColor#}</label>
  <label style="width:150px">
    <input type="text" size="5" name="prop_bgColor" value="{$property->bgColor}" style="color:{$property->bgColor};background-color:{$property->bgColor}" maxlength="7" readonly="readonly" style="font-family:Tahoma;font-size:x-small;" />
    <img src="{$picPath}admin/color.gif" title="{#infocolor#}" width="21" height="20" border="0" onClick="fctShow(document.form.prop_bgColor);" style="cursor:pointer;" class="middle" />
  </label>
  <label style="width:150px">{#bgBodyColor#}</label>
  <input type="text" size="5" name="prop_bgBodyColor" value="{$property->bgBodyColor}" style="color:{$property->bgBodyColor};background-color:{$property->bgBodyColor}" readonly="readonly" maxlength="7" style="font-family:Tahoma;font-size:x-small;" />
  <img src="{$picPath}admin/color.gif" title="{#infocolor#}" width="21" height="20" border="0" onClick="fctShow(document.form.prop_bgBodyColor);" style="cursor:pointer;" class="middle" />
  <br /><br />

  <label style="width:150px;clear:both">{#language#}</label>
  <label style="width:150px">
    {html_select tab=$tableLanguage name=language selected=$property->language}&nbsp;
  </label>
  <label style="width:150px">{#dateFormat#}</label>
  {html_select tab=$tableDateFormat name=dateFormat selected=$property->dateFormat}&nbsp;
  <br /><br />

  <label style="width:150px;clear:both">{#skin#}</label>
  <label style="width:150px">
  {if $tableSkins}
  <select name="prop_skin" onchange="form.changeSkin.value='true';form.submit();">
  {foreach from=$tableSkins key=skin item=itemSkin}
    {if ($property->skin == $skin)}
    <option value="{$skin}" selected=selected>{$skin}</option>
    {else}
    <option value="{$skin}">{$skin}</option>
    {/if}
  {/foreach}
  </select>
  <input type="image" src="{$picPath}admin/refresh.gif" title="{#inforefreshlist#}" onclick="form.changeSkin.value='true';" class="imgButton" class="middle" />
  {else}
  &nbsp;
  {/if}
  </label>
  <label style="width:150px"><input type="submit" value="{#persoSkin#}" onclick="form.editSkin.value='true';" /></label>
  <br /><br />

  <label style="width:150px;clear:both">{#bgImg#}</label>
  {$imgBg}
  <img src="{$picPath}admin/delete.gif" style="cursor:pointer" title="{#infodeletepics#}" class="middle" onclick="createCustomConfirm('{#areyousure#} {#image#} ?');document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteImgBg.value='true';form.submit();{literal}}{/literal}" />
  {html_select tab=$tableBgPosition name=prop_imgBgPos selected=$property->imgBgPos}
  <br /><br />

  <label style="width:230px;clear:both">{#showStats#}</label>
  <label style="width:70px">
    <input type="checkbox" {if $property->showStats==1}checked="checked"{/if} id="prop_showStats" name="prop_showStats" />
  </label>
  <label style="width:210px;">{#showBudget#}</label>
  <input type="checkbox" {if $property->showBudget==1}checked="checked"{/if} id="prop_showBudget" name="prop_showBudget" />
  <br /><br />

  <label style="width:230px;clear:both">{#showMap#}</label>
  <label style="width:70px">
    <input type="checkbox" {if $property->showMap==1}checked="checked"{/if} id="prop_showMap" name="prop_showMap" />
  </label>
  <label style="width:210px;">{#showDescription#}</label>
  <input type="checkbox" {if $property->showDescription==1}checked="checked"{/if} id="prop_showDesc" name="prop_showDesc" />
  <br /><br />

  <label style="width:230px;clear:both">{#showShortCut#}</label>
  <label style="width:70px">
    <input type="checkbox" {if $property->showShortCut==1}checked="checked"{/if} id="prop_showShortCut" name="prop_showShortCut" />
  </label>
  <label style="width:210px;">{#saveOrigImg#}</label>
  <input type="checkbox" {if $property->saveOrigImg==1}checked="checked"{/if} id="prop_saveOrigImg" name="prop_saveOrigImg" />
  <br /><br />

  <label style="width:230px;clear:both">{#deleteEmails#}</label>
  <label style="width:70px">
    <input type="checkbox" {if $property->deleteEmails==1}checked="checked"{/if} id="prop_deleteEmails" name="prop_deleteEmails" />
  </label>
  <label style="width:210px;">{#sendEmails#}</label>
  <input type="checkbox" {if $property->sendEmails==1}checked="checked"{/if} id="prop_sendEmails" name="prop_sendEmails" />
  <br /><br />
</fieldset>


<a name="menuDisplay">
<p class="formTitle">{#menuDisplay#}</p>


<fieldset>
  <br />
  <label style="width:110px;clear:both">{#menuForum#}</label>
  <label style="width:210px">
    <input type="checkbox" {if $menuOptions->forum==1}checked="checked"{/if} id="option_forum" name="option_forum" />
  </label>
  <label style="width:110px;">{#menuNotebook#}</label>
  <input type="checkbox" {if $menuOptions->notebook==1}checked="checked"{/if} id="option_notebook" name="option_notebook" />
  <br />

  <label style="width:110px;clear:both">{#menuHumour#}</label>
  <label style="width:210px">
    <input type="checkbox" {if $menuOptions->humour==1}checked="checked"{/if} id="option_humour" name="option_humour" />
  </label>
  <label style="width:110px;">{#menuNews#}</label>
  <input type="checkbox" {if $menuOptions->news==1}checked="checked"{/if} id="option_news" name="option_news" />
  <br />

  <label style="width:110px;clear:both">{#menuVideo#}</label>
  <label style="width:210px">
    <input type="checkbox" {if $menuOptions->video==1}checked="checked"{/if} id="option_video" name="option_video" />
  </label>
  <label style="width:110px;">{#menuChat#}</label>
  <input type="checkbox" {if $menuOptions->chat==1}checked="checked"{/if} id="option_chat" name="option_chat" />
  <br />

  <label style="width:110px;">{#menuBudget#}</label>
  <label style="width:210px">
    <input type="checkbox" {if $menuOptions->statistics==1}checked="checked"{/if} id="option_statistics" name="option_statistics" />
  </label>
  <label style="width:110px;">{#menuPrep#}</label>
  <input type="checkbox" {if $menuOptions->preparation==1}checked="checked"{/if} id="option_preparation" name="option_preparation" />
  <br />

  <label style="width:110px;">{#menuRank#}</label>
  <label style="width:210px">
    <input type="checkbox" {if $menuOptions->ranking==1}checked="checked"{/if} id="option_ranking" name="option_ranking" />
  </label>
  <br /><br />
</fieldset>

<br /><br />

<table width="100%">
  <tr>
    <td align="center">
      <input type="submit" value="{#cancel#}" onclick="form.cancelConfig.value='true'" />
    </td>
    <td align="center">
      <input type="button" value="{#preview#}" onclick="form.submit();window.open('{#PAGE_PREVIEW#}', null, 'height=600,width=800,status=yes,toolbar=no,menubar=yes,scrollbars=yes,resizable=yes')" />
    </td>
    <td align="center">
      <input type="submit" value="{#save#}" onclick="form.save.value='true'" />
    </td>
  </tr>
</table>

</form>

<script language="JavaScript">
<!-- // create calendar object(s) just after form tag closed
  var cal1 = new calendar1(document.forms['form'].elements['startingDate'], 'startingDate', '{$property->dateFormat}');
  cal1.year_scroll = true;
  cal1.time_comp = false;
  var cal2 = new calendar1(document.forms['form'].elements['endingDate'], 'endingDate', '{$property->dateFormat}');
  cal2.year_scroll = true;
  cal2.time_comp = false;
//-->
</script>

{$showFooter}
