{$showTopHeader}

<script language="JavaScript" type="text/javascript">
<!--
{literal}
  function sendMessageToChat() {
    sendMessage(document.form.message.value);
    document.form.message.value='';
  }
{/literal}
// -->
</script>

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" {if $isConnected}onload="document.form.message.focus();"{/if}>

{$showHeader}
<br />
<form name="form" method="POST" action="" onsubmit="sendMessageToChat();return false;">
<input type="hidden" name="init_{$pageName}" value="true">
  <table width="600px">
    <tr>
      <td>
        <div id="blocMessage"></div>
      </td>
      {if $isConnected}
      <td>
        <div id="blocUsers"></div>
      </td>
      {/if}
    </tr>
    {if $isConnected}
    <tr>
      <td colspan="2">
        <input type="text" name="message" id="message" class="messageChat" size="50" value="" onKeyDown="if (window.event.keyCode==13) sendMessageToChat();" >
        <input type="button" value="Envoyer" name="sendMessageButton" id="sendMessageButton" class="messageChat" onclick="sendMessageToChat()">
      </td>
    </tr>
    {/if}
  </table>
</form>

<script language="JavaScript" type="text/javascript">
<!--
  getMessageFromServer(document.getElementById('blocMessage'));
  {if $isConnected}
  getUsersFromServer(document.getElementById('blocUsers'), '{$user}');
  {/if}
// -->
</script>

{$showFooter}

