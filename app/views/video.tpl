{config_load file=$userLanguage|cat:"/video_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<form name="form" method=post ENCTYPE=multipart/form-data>
<input type="hidden" name="actions" value="{$action}">
<input type="hidden" name="editVideo">
<input type="hidden" name="deleteVideo">
<input type="hidden" name="changeAccess">
<input type="hidden" name="cancel" value="false" />
<input type="hidden" name="save" value="false" />
<input type="hidden" name="init_{$pageName}" value="true">

{* Split display for videos *}
{if $splitDisplay->total > $splitDisplay->length}
  <div class="navigation">
  {if $splitDisplay->start > 0}
    <a href="?next={$splitDisplay->start-$splitDisplay->maxNum}"><<<a>&nbsp;
  {/if}
  {section name=foo start=$splitDisplay->start loop=$splitDisplay->loop max=$splitDisplay->maxNum}
    {if ($smarty.section.foo.index==$splitDisplay->first)}
      {$smarty.section.foo.index+1}
    {else}
      <a href="?page={$smarty.section.foo.index*$splitDisplay->length}&next={$splitDisplay->start}">{$smarty.section.foo.index+1}<a>
    {/if}
  {/section}
  {if $splitDisplay->start+$splitDisplay->maxNum < $splitDisplay->loop}
    &nbsp;<a href="?next={$splitDisplay->start+$splitDisplay->maxNum}">>><a>
  {/if}
  </div>
{/if}

{foreach from=$video key=keyVideo item=itemVideo}
<div class="cadresitetitle">
  <p class="sitetitle">
    {if ($admin)}
    {if ($itemVideo->access=='public')}
    &nbsp;<input type="image" src="{$picPath}admin/enable.gif" class="imgButton" onclick="form.changeAccess.value='{$itemVideo->id}'" />
    {else}
    &nbsp;<input type="image" src="{$picPath}admin/disable.gif" class="imgButton" onclick="form.changeAccess.value='{$itemVideo->id}'" />
    {/if}
    {/if}
    <a href="?play={$itemVideo->id}"><img src="{$picPath}video.gif" width="28" height="30" class="middle" alt="{#watchVideo#}" /></a> {#postedOn#} {$itemVideo->date|showdate}
    {if ($admin)}
    &nbsp;<input type="image" src="{$picPath}admin/update.gif" class="imgButton" onclick="form.editVideo.value='{$itemVideo->id}'" />
    &nbsp;<a href="#" onclick="createCustomConfirm('{#areyousure#} {$itemVideo->subject} ?');
    document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();form.deleteVideo.value='{$itemVideo->id}';
    form.submit();{literal}}{/literal}"><img src="{$picPath}admin/delete.gif" class="middle" /></a>
    {/if}
  </p>
  <span class="sitetitle">{#subject#} : </span><span class="persotext">{$itemVideo->subject|sslash}</span>
  <p class="persotext">{$itemVideo->text|sslash}</p>
</div>
{/foreach}


{* Split display for videos *}
{if $splitDisplay->total > $splitDisplay->length}
  <div class="navigation">
  {if $splitDisplay->start > 0}
    <a href="?next={$splitDisplay->start-$splitDisplay->maxNum}"><<<a>&nbsp;
  {/if}
  {section name=foo start=$splitDisplay->start loop=$splitDisplay->loop max=$splitDisplay->maxNum}
    {if ($smarty.section.foo.index==$splitDisplay->first)}
      {$smarty.section.foo.index+1}
    {else}
      <a href="?page={$smarty.section.foo.index*$splitDisplay->length}&next={$splitDisplay->start}">{$smarty.section.foo.index+1}<a>
    {/if}
  {/section}
  {if $splitDisplay->start+$splitDisplay->maxNum < $splitDisplay->loop}
    &nbsp;<a href="?next={$splitDisplay->start+$splitDisplay->maxNum}">>><a>
  {/if}
  </div>
{/if}

{if $player}
{$player}
{/if}

{if $admin}
<a name="textentry"></a>

<p class="formTitle">{if $action=='new'}{#newVideo#}{else}{#editVideo#}{/if}</p>

<fieldset>
  <br />
  <input type="hidden" name="id" value="{$newVideo->id}" />
  <input type="hidden" name="videoToEdit" value="{$newVideo->images->name}" />
  <label style="width:100px">{#name#}</label>
  <input type="text" name="name" value="{$newVideo->subject|sslash}" />
  <br /><br />

  <label style="width:100px">{#description#}</label>
  <textarea name="description" id="description" cols="70" rows="7">{$newVideo->text|sslash}</textarea>
  <br /><br />

  {if $action=='new'}
  <label style="width:200px">{#video#}</label>
  <img src="{$picPath}blank.gif" name="showVideo" id="showVideo" height="22" border="0" />
  <input type="hidden" name="videoFromAlbum" id="videoFromAlbum" />
  <input type="file" name="videoName" id="videoName" onchange="document.getElementById('showVideo').src='{$picPath}blank.gif'"/>
  <a href="#" onclick="window.open('{#PAGE_FILEVIEWER#}?actions=selectVideo&id=videoFromAlbum&show=showVideo&clean=videoName', null, 'height=480,width=640,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes')"><img src="{$picPath}admin/download.png" border="0" width="24" height="24" /></a>
  <br /><br />
  {/if}
</fieldset>

<br />
<table width="100%">
  <tr>
    <td align="center">
      <input type="submit" value="{#cancel#}" onclick="form.cancel.value='true'" />
    </td>
    <td align="center">
      <input type="submit" value="{#save#}" onclick="form.save.value='true'" />
    </td>
  </tr>
</table>
{/if}

</form>

{$showFooter}
