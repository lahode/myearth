{config_load file=$userLanguage|cat:"/mailbox_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

{if !$showTrash}
<form name="xbb12form" method="post">
<input type="hidden" name="xbb12deleteEmail" value="" />
<input type="hidden" name="xbb12backTo" value="{$box}" />

<p class="sitetitle">
{#actiononmail#}
{if $box=='in'}<a href="{#PAGE_COMPOSE#}?actions=reply&box={$box}&id={$idemail}"><img title="{#inforeplymsg#}" src="{$picPath}admin/reply.gif" width="16" /></a>{/if}
<a href="{#PAGE_COMPOSE#}?actions=fwd&box={$box}&id={$idemail}"><img title="{#infoforwardmsg#}" src="{$picPath}admin/forward.gif" width="16" /></a>
<img title="{#infodeletemsg#}" style="cursor:pointer" src="{$picPath}admin/delete.gif" onclick="createCustomConfirm('{#areyousure#} {#thisemail#} ?');
document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();xbb12form.xbb12deleteEmail.value='{$idemail}';xbb12form.submit();{literal}}{/literal}" />
</p>
</form>
{/if}

{$email}

<form name="xbb13form" method="post">
<input type="hidden" name="xbb12transfer" value="" />
<input type="hidden" name="xbb12transferName" value="" />
<input type="hidden" name="xbb12back" value="false" />
<input type="hidden" name="xbb12deleteAttachment" value="" />
{if $attachments}
<p class="sitetitle">{#attachments#}</p>
  {foreach from=$attachments item=attachment}
  <p class="sitetext" align="left">
    <img src="{$picPath}admin/attachment.gif" />
    <a href="{$attachmentPath}{$attachment->content}" target="_blank">{$attachment->name}</a>
    &nbsp;&nbsp;<img title="{#infodeleteattachment#}" style="cursor:pointer" src="{$picPath}admin/delete.gif" onclick="createCustomConfirm('{#areyousure#} {#attachment#} {$attachment->name} ?');
    document.getElementById('confirmBtn').onclick = function() {literal}{{/literal}removeCustomAlert();xbb13form.xbb12deleteAttachment.value='{$attachment->content}';xbb13form.submit();{literal}}{/literal}" />
    &nbsp;&nbsp;<img src="{$picPath}admin/transfer.gif" title="{#infotransfer#}" style="cursor:pointer" onclick="xbb13form.xbb12transfer.value='{$attachment->content}';xbb13form.xbb12transferName.value='{$attachment->name}';xbb13form.submit();" />
  </p>
  {/foreach}
{/if}
<br /><br />
<table width="100%">
  <tr>
    <td align="center">
      <input type="submit" value="{#back#}" onclick="xbb13form.xbb12back.value='true'" />
    </td>
  </tr>
</table>

</form>

{$showFooter}
