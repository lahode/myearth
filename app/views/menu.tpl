{config_load file=$userLanguage|cat:"/menu_"|cat:$userLanguage|cat:".conf"}
{config_load file="file.conf"}

<div class="rbroundboxMenu"><div class="rbtopMenu"><div></div></div><div class="rbcontentMenu">
<div class="cadremenutitle">
  <table cellspacing="0" cellpadding="0" class="tablemenu">
  <tr>
    <td width="120">
      <span class="menutitle"><a href="{#PAGE_ACCUEIL#}">{#menuHome#}</a></span>
    </td>
    {if $showRSS}
    <td align="right">
      <span class="menutitle"><a href="{$showRSS}" target="_blank"><img src="{$picPath}feedicon.gif" /></a></span>
    </td>
    {/if}
  </tr>
  </table>
</div>
</div><div class="rbbotMenu"><div></div></div></div>

{if $admin}
<div class="rbroundboxMenu"><div class="rbtopMenu"><div></div></div><div class="rbcontentMenu">
<div class="cadremenutitle">
  <p class="menutitle">{#menuAdministration#}&nbsp;<img src="{$picPath}menu/hammer.gif" width="24" height="20" class="middle"></p>
  <table cellspacing="0" cellpadding="0" class="tablemenu">
  <tr>
    <td width="130">
      <span class="menutext"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if $showMenu<>'preview'}<a href="{#PAGE_FILEMANAGER#}">{#menuFileManager#}</a>{else}{#menuFileManager#}{/if}</span>
    </td>
  </tr>
  {if $menuOptions->statistics}
  <tr>
    <td width="135">
      <span class="menutext"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if $showMenu<>'preview'}<a href="{#PAGE_BUDGET#}">{#menuBudget#}</a>{else}{#menuBudget#}{/if}</span>
    </td>
  </tr>
  {/if}
  <tr>
    <td width="135">
      <span class="menutext"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if $showMenu<>'preview'}<a href="{#PAGE_CONFIG#}">{#menuConfiguration#}</a>{else}{#menuConfiguration#}{/if}</span>
    </td>
  </tr>
  <tr>
    <td width="135">
      <span class="menutext"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if $showMenu<>'preview'}<a href="{#PAGE_SECURITY#}">{#menuSecurity#}</a>{else}{#menuSecurity#}{/if}</span>
    </td>
  </tr>
  <tr>
    <td width="135">
      <span class="menutext"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if $showMenu<>'preview'}<a href="{#PAGE_CONTACTS#}">{#menuContacts#}</a>{else}{#menuContacts#}{/if}</span>
    </td>
  </tr>
  <tr>
    <td width="135">
      <span class="menutext"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if $showMenu<>'preview'}<a href="{#PAGE_MAILBOX#}">{#menuMessages#}</a>{else}{#menuMessages#}{/if}</span>
    </td>
  </tr>
  <tr>
    <td width="135">
      <span class="menutext"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if $showMenu<>'preview'}<a href="{#PAGE_TASKS#}">{#menuTasks#}</a>{else}{#menuTasks#}{/if}</span>
    </td>
  </tr>
  <tr>
    <td width="135">
      <span class="menutext"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if $showMenu<>'preview'}<a href="{#PAGE_SENDNEWS#}">{#menuSendNews#}</a>{else}{#menuSendNews#}{/if}</span>
    </td>
  </tr>
  <tr>
    <td width="135">
      <span class="menutext"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if $showMenu<>'preview'}<a href="../help/usermanual.pdf" target="_blank">{#menuUserManual#}</a>{else}{#menuUserManual#}{/if}</span>
    </td>
  </tr>
  </table>
</div>
</div><div class="rbbotMenu"><div></div></div></div>
{/if}

{if $showPerso=='true'}
<div class="rbroundboxMenu"><div class="rbtopMenu"><div></div></div><div class="rbcontentMenu">
<div class="cadremenutitle">
  <table cellspacing="0" cellpadding="0" class="tablemenu">
  <tr>
    <td width="120" colspan="2">
      <span class="menutitle">{if $showMenu<>'preview'}<a href="{#PAGE_PERSO#}">{#menuPerso#}</a>{else}{#menuPerso#}{/if}</span>
    </td>
  </tr>
  </table>
</div>
</div><div class="rbbotMenu"><div></div></div></div>
{/if}

<div class="rbroundboxMenu"><div class="rbtopMenu"><div></div></div><div class="rbcontentMenu">
<div class="cadremenutitle">
  <p class="menutitle">{if $showMenu<>'preview'}<a href="{#PAGE_DESTINATION#}">{#menuDestination#}</a>{else}{#menuDestination#}{/if}{if ($admin)}&nbsp;<img src="{$picPath}menu/hammer.gif" width="24" height="20" class="middle">{/if}</p>
  <p class="menutitle">{#titleAlbum#}</p>
  {if ($countries)}
  <table cellspacing="0" cellpadding="0" class="tablemenu">
  <tr>
    <td width="120">
      <span class="menutext" style="font-weight:bold;"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if $showMenu<>'preview'}<a href="{#PAGE_DESTINATION#}">{#all#}&nbsp;</a>{else}{#all#}{/if}</span>
    </td>
    <td>
      <span class="menutext">{if $lastDestination->nbdest}({$lastDestination->nbdest}){/if}</span>
    </td>
  </tr>
  {foreach from=$countries key=keyCountry item=country}
  {if ($country->nbdest >= 0)}
  <tr>
    <td width="120">
      <span class="menutext"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if ($country->nbdest > 0 && showMenu<>5)}<a href="{#PAGE_SHOW#}?cat={$country->cat}">{/if}{$country->name}{if ($country->nbdest > 0 && showMenu<>5)}</a>{/if}</span>
    </td>
    <td>
      <span class="menutext">{if $country->nbdest}({$country->nbdest}){/if}</span>
    </td>
  </tr>
  {else}
  {if ($country->nbdest == -1)}
  <br />
  <tr>
    <td colspan="2">
      &nbsp;
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <span class="menutitle" style="margin-bottom:5px">{#menuLastVisited#}</span>
    </td>
  </tr>
  {/if}
  <tr>
    <td colspan="2">
      <span class="menutext"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if $showMenu<>'preview'}<a href="{#PAGE_SHOW#}?dest={$country->cat}">{$country->name}</a>{else}{$country->name}{/if}</span>
    </td>
  </tr>
  {/if}
  {/foreach}
  </table>
  {/if}
</div>
</div><div class="rbbotMenu"><div></div></div></div>


{if $menuOptions->forum}
<div class="rbroundboxMenu"><div class="rbtopMenu"><div></div></div><div class="rbcontentMenu">
<div class="cadremenutitle">
  <table cellspacing="0" cellpadding="0" class="tablemenu">
  <tr>
    <td width="120">
      <span class="menutitle">{if $showMenu<>'preview'}<a href="{#PAGE_FORUM#}">{#menuForum#}</a>{else}{#menuForum#}{/if}</span>
    </td>
    <td>
      <span class="menutext">({$count.forum})</span>
    </td>
  </tr>
  </table>
</div>
</div><div class="rbbotMenu"><div></div></div></div>
{/if}


<div class="rbroundboxMenu"><div class="rbtopMenu"><div></div></div><div class="rbcontentMenu">
<div class="cadremenutitle">
  <p class="menutitle">{#menuCategories#}{if ($admin)}&nbsp;<img src="{$picPath}menu/hammer.gif" width="24" height="20" class="middle">{/if}</p>
  <table cellspacing="0" cellpadding="0" class="tablemenu">
  {if $menuOptions->news && ($count.news > 0 || $admin)}
  <tr>
    <td width="120">
      <span class="menutext"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if $showMenu<>'preview'}<a href="{#PAGE_NEWS#}">{#menuNews#}</a>{else}{#menuNews#}{/if}</span>
    </td>
    <td>
      <span class="menutext">({$count.news})</span>
    </td>
  </tr>
  {/if}
  {if $menuOptions->notebook && ($count.notebook > 0 || $admin)}
  <tr>
    <td width="120">
      <span class="menutext"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if $showMenu<>'preview'}<a href="{#PAGE_NOTEBOOK#}">{#menuNotebook#}</a>{else}{#menuNotebook#}{/if}</span>
    </td>
    <td>
      <span class="menutext">({$count.notebook})</span>
    </td>
  </tr>
  {/if}
  {if $menuOptions->notebook && ($count.video > 0 || $admin)}
  <tr>
    <td width="120">
      <span class="menutext"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if $showMenu<>'preview'}<a href="{#PAGE_VIDEO#}">{#menuVideo#}</a>{else}{#menuVideo#}{/if}</span>
    </td>
    <td>
      <span class="menutext">({$count.video})</span>
    </td>
  </tr>
  {/if}
  {if $menuOptions->humour && ($count.humour > 0 || $admin)}
  <tr>
    <td width="120">
      <span class="menutext"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if $showMenu<>'preview'}<a href="{#PAGE_HUMOUR#}">{#menuHumour#}</a>{else}{#menuHumour#}{/if}</span>
    </td>
    <td>
      <span class="menutext">({$count.humour})</span>
    </td>
  </tr>
  {/if}
  {if $menuOptions->preparation && ($count.prep > 0 || $admin)}
  <tr>
    <td width="120">
      <span class="menutext"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if $showMenu<>'preview'}<a href="{#PAGE_PREPARATION#}">{#menuPrep#}</a>{else}{#menuPrep#}{/if}</span>
    </td>
    <td>
      <span class="menutext">({$count.prep})</span>
    </td>
  </tr>
  {/if}
  {if $menuOptions->ranking && ($count.prep > 0 || $admin)}
  <tr>
    <td width="120">
      <span class="menutext"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if $showMenu<>'preview'}<a href="{#PAGE_RANKING#}">{#menuRank#}</a>{else}{#menuRank#}{/if}</span>
    </td>
    <td>
      <img src="{$picPath}menu/new.gif" width="30" class="middle" />
    </td>
  </tr>
  {/if}
  </table>
</div>
</div><div class="rbbotMenu"><div></div></div></div>


{if $menuOptions->chat}
<div class="rbroundboxMenu"><div class="rbtopMenu"><div></div></div><div class="rbcontentMenu">
<div class="cadremenutitle">
  <table cellspacing="0" cellpadding="0" class="tablemenu">
  <tr>
    <td width="120">
      <span class="menutitle">{if $showMenu<>'preview'}<a href="{#PAGE_CHATCLIENT#}">{#menuChat#}</a>{else}{#menuChat#}{/if}</span>
    </td>
  </tr>
  </table>
</div>
</div><div class="rbbotMenu"><div></div></div></div>
{/if}


{if $menuOptions->news}
<div class="rbroundboxMenu"><div class="rbtopMenu"><div></div></div><div class="rbcontentMenu">
<div class="cadremenutitle">
  <table cellspacing="0" cellpadding="0" class="tablemenu">
  <tr>
    <td width="120">
      <span class="menutitle">{if $showMenu<>'preview'}<a href="{#PAGE_REGISTERNEWS#}">{#menuNewsLetter#}</a>{else}{#menuNewsLetter#}{/if}{if ($admin)}&nbsp;<img src="{$picPath}menu/hammer.gif" width="24" height="20" class="middle">{/if}</span>
    </td>
  </tr>
  </table>
</div>
</div><div class="rbbotMenu"><div></div></div></div>
{/if}


{if (!$admin && $menuOptions->statistics && ($property->showBudget || $property->showStats))}
<div class="rbroundboxMenu"><div class="rbtopMenu"><div></div></div><div class="rbcontentMenu">
<div class="cadremenutitle">
  <p class="menutitle">{#menuBudget#}</p>
  <table cellspacing="0" cellpadding="0" class="tablemenu">
  {if $property->showBudget}
  <tr>
    <td width="130">
      <span class="menutext"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if $showMenu<>'preview'}<a href="{#PAGE_BUDGET_ACCOMODATION#}">{#menuAccommodation#}</a>{else}{#menuAccommodation#}{/if}</span>
    </td>
  </tr>
  <tr>
    <td width="130">
      <span class="menutext"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if $showMenu<>'preview'}<a href="{#PAGE_BUDGET_FOOD#}">{#menuFood#}</a>{else}{#menuFood#}{/if}</span>
    </td>
  </tr>
  <tr>
    <td width="130">
      <span class="menutext"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if $showMenu<>'preview'}<a href="{#PAGE_BUDGET_TRANSPORT#}">{#menuTransport#}</a>{else}{#menuTransport#}{/if}</span>
    </td>
  </tr>
  {/if}
  {if $property->showStats}
  <tr>
    <td width="130">
      <span class="menutext"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if $showMenu<>'preview'}<a href="{#PAGE_STATISTICS#}">{#menuSpending#}</a>{else}{#menuSpending#}{/if}</span>
    </td>
  </tr>
  {/if}
  </table>
</div>
</div><div class="rbbotMenu"><div></div></div></div>
{/if}


<div class="rbroundboxMenu"><div class="rbtopMenu"><div></div></div><div class="rbcontentMenu">
<div class="cadremenutitle">
  <table cellspacing="0" cellpadding="0" class="tablemenu">
  <tr>
    <td width="120">
      <span class="menutitle">{if $showMenu<>'preview'}<a href="{#PAGE_FORUM#}?first=true&album={$albumName}&session=active">{#menuContact#}</a>{else}{#menuContact#}{/if}</span>
    </td>
  </tr>
  </table>
</div>
</div><div class="rbbotMenu"><div></div></div></div>


{if ($links || $admin)}
<div class="rbroundboxMenu"><div class="rbtopMenu"><div></div></div><div class="rbcontentMenu">
<div class="cadremenutitle">
  {if ($admin)}
  <p class="menutitle">{if $showMenu<>'preview'}<a href="{#PAGE_LINKS#}">{#menuLinks#}</a>{else}{#menuLinks#}{/if}&nbsp;<img src="{$picPath}menu/hammer.gif" width="24" height="20" class="middle"></p>
  {else}
  <p class="menutitle">{#menuLinks#}</p>
  {/if}
  {if $links}
  <table cellspacing="0" cellpadding="0" class="tablemenu">
  {foreach from=$links key=keyLink item=link}
  <tr>
    <td width="135">
      <span class="menutext"><img src="{$picPath}menu/bullet.gif" width="12" height="12" class="middle">&nbsp;{if $showMenu<>'preview'}<a href="{$link->link}" target="_blank" title="{$link->description|sslash}">{$link->name}</a>{else}{$link->name}{/if}</span>
    </td>
  </tr>
  {/foreach}
  </table>
  {/if}
</div>
</div><div class="rbbotMenu"><div></div></div></div>
{/if}



