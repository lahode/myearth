{if ($emotes)}
<table border="0" width="90%" align="center">
  <tbody>
  {foreach key=keyEmote item=emote from=$emotes}
    {if ($keyEmote % 15 == 0)}
    <tr>
    {/if}
      <td align="center">
        <a href="#textentry" onclick="insertAtCaret({$textarea}, '{$emote->shortcut}');"><img border="0" src="{$picPath}/emoticon/{$emote->file}" /></a>
      </td>
    {if ($keyImage % 10 == 9 || $keyEmote==$nbEmote)}
    </tr>
    {/if}
  {/foreach}
  </tbody>
</table>
{/if}
