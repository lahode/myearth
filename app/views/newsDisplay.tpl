{config_load file=$userLanguage|cat:"/news_"|cat:$userLanguage|cat:".conf"}

{$headerNews}

<div class="cadresitetitle">
  <p class="sitetitle">
    {if $attachments}
    <img src="cid:{$attachments.$lastAttach->id}" width="22" height="21" class="middle" /> {#postedOn#} {$msgNews->date|showdate}
    {else}
    <img src="{$picPath}news.gif" width="22" height="21" class="middle" /> {#postedOn#} {$msgNews->date|showdate}
    {/if}
  </p>
  {if $msgNews->from}<p class="sitetitle">{#from#} : </span><span class="persotext">{$msgNews->from|sslash}</p>{/if}
  {if $msgNews->to}<p class="sitetitle">{#to#} : </span><span class="persotext">{$msgNews->to|sslash}</p>{/if}
  {if $msgNews->cc}<p class="sitetitle">{#cc#} : </span><span class="persotext">{$msgNews->cc|sslash}</p>{/if}
  {if $msgNews->bcc}<p class="sitetitle">{#bcc#} : </span><span class="persotext">{$msgNews->bcc|sslash}</p>{/if}
  <p class="sitetitle">{#subject#} : </span><span class="persotext">{$titleNews|sslash}</p>
  <p class="persotext">{$msgNews->body|sslash}{$msgNews->text|sslash}</p>
  {if $msgNews->images && $msgNews->images.0}
  <table cellpadding="0" cellspacing="0" align="center">
  {foreach from=$msgNews->images key=keyImage item=image}
    {if ($keyImage % 2 == 0)}
    <tr>
    {/if}
      <td align="center">
        <table border="0">
          <tr valign="middle">
            <td align="center">
              {if $attachments && $attachments.$keyImage->id}
              <img src="cid:{$attachments.$keyImage->id}" height="180" />
              {else}
              <img src="{$imgPath}{$image->name}" height="180" />
              {/if}
            </td>
          </tr>
          <tr>
            <td align="center" style="font-size:11px">
              {$image->description|sslash|nl2br}
              <br />
            </td>
          </tr>
        </table>
      </td>
    {if ($keyImage % 2 == 2 || $keyImage==($msgNews->images|@count)-1)}
    </tr>
    {/if}
  {/foreach}
  </table>
  {/if}
</div>

{$footerNews}

