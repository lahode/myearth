{if $details}
<div class="sectionP">
 {if $details}
 {foreach from=$details item=detail}
 <span class="sitetitle">{$detail.0} : </span><span class="sitetext">{$detail.1}</span><br />
 {/foreach}
 {/if}
</div>

<div class="sectionP">
  <p class="sitetext">{$description}</p>
</div>
{/if}
