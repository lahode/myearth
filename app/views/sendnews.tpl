{config_load file=$userLanguage|cat:"/news_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<form name="form" method="post">
<input type="hidden" name="send" value="false" />
<input type="hidden" name="save" value="false" />
<input type="hidden" name="selectedPicture" id="selectedPicture" value="{$images.0->name}" />
<input type="hidden" name="init_{$pageName}" value="true">

<br />
<p align="left" class="sitetitle">{$subjectTranslated}</p>
<br />
<p align="left" class="sitetext">{$messageTranslated|nl2br}</p>
<br />

<fieldset>
  <br />
  <label style="width:140px">{#email#}</label>
  {if ($emails)}
  {html_select tab=$emails name='email' selected=$selectedEmail}
  {else}
  <input type="text" name="email" value="{$email}" size="50" />
  {/if}
  <br /><br />
  <label style="width:140px">{#subject#}</label>
  <input type="text" name="subject" value="{$subject}" size="50" />
  <br /><br />
  <label style="width:140px">{#message#}</label>
  <textarea name="message" cols="55" rows="5">{$message}</textarea>
  <br /><br />
  <label style="width:140px">{#region#}</label>
  {html_select tab=$regions url="#" name='region' selected=$destination->id}
  <input type="image" src="{$picPath}admin/refresh.gif" title="{#inforefreshlist#}" class="imgButton" class="middle" />
  {if $destination->images}
  <br /><br />
  <label style="width:140px">{#picture#}</label>
  <div style="float:left;overflow-y: scroll; overflow-x: hidden; width: 110px; height: 100px; background-color: #000000">
    {foreach from=$destination->images item=image}
    <span align=left><img style="WIDTH: 100px;" src="{$webImgPath}{$image->name}" border="0" style="cursor:pointer" onclick="form.selectedPicture.value='{$image->name}';document.getElementById('showPicture').src='{$webImgPath}{$image->name}'"></span>
    {/foreach}
  </div>
  &nbsp;&nbsp;&nbsp;{if $images.0->name}<img id="showPicture" src="{$webImgPath}{$images.0->name}" height="100" />{else}<img id="showPicture" src="{$picPath}transparent.gif" height="100" />{/if}
  {/if}
</fieldset>

<table width="100%" border="0" align="center">
  <tr>
    <td align="center">
      <input type="submit" value="{#save#}" onclick="form.save.value='true'" />
    </td>
    <td align="center">
      <input type="submit" value="{#send#}" onclick="form.send.value='true'" />
    </td>
  </tr>
</table>

<br /><br />
<p class="sitetext">{#reminderNews#}</p>

</form>

{$showFooter}
