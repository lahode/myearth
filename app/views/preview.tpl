{config_load file=$userLanguage|cat:"/preview_"|cat:$userLanguage|cat:".conf"}

{$showTopHeader}

<script type="text/javascript" language="javascript">
{literal}
if (self.name != '_refreshed_')
{
    self.name = '_refreshed_';
    self.location.reload(true);
}
else self.name = '';
{/literal}
</script>

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

{$showHeader}

<p class="sitetitle">{#sitetitle#}</p>
<br /><br />
<p class="sitetext">{#sitetext#}</p>
<br /><br />
<p class="persotext">{#persotext#}</p>
<br /><br />

{* Split display for messages *}
{if $splitDisplay->total > $splitDisplay->length}
  <div class="navigation">
  {if $splitDisplay->start > 0}
    <a href="?next={$splitDisplay->start-$splitDisplay->maxNum}"><<<a>&nbsp;
  {/if}
  {section name=foo start=$splitDisplay->start loop=$splitDisplay->loop max=$splitDisplay->maxNum}
    {if ($smarty.section.foo.index==$splitDisplay->first)}
      {$smarty.section.foo.index+1}
    {else}
      <a href="?page={$smarty.section.foo.index*$splitDisplay->length}&next={$splitDisplay->start}">{$smarty.section.foo.index+1}<a>
    {/if}
  {/section}
  {if $splitDisplay->start+$splitDisplay->maxNum < $splitDisplay->loop}
    &nbsp;<a href="?next={$splitDisplay->start+$splitDisplay->maxNum}">>><a>
  {/if}
  </div>
{/if}

{$showFooter}
