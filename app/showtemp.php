<?php
  include_once ('Worldmap.php');
  include_once('../lib/init.php');


  // Affiche une destination
  function showDestination ($destInput) {
    // Initialisation des variables globales
    global $routeManager;
    global $dbQuery;
    global $language;
    global $smarty;
    global $webImgPath;
    global $msgHandler;
    global $property;
    global $mapImgPath;

    // Vérification des variables en entrée
    $response = new xajaxResponse('ISO-8859-1');
    $dest = $routeManager->getDestinationBy ('id', $destInput);
    if ($dest) $category = $routeManager->getCategoryBy ('id', $dest->category);
    else {$response->addRedirect(PAGE_DESTINATION);return $response->getXML();}

    // Initialisation des données
    $pageNameHeader = "showheader";
    $pageNameDescription = "showdescription";
    $pageNameContent = "showdest";
    $leavingDate = "";
    $description = "";
    $country = $category->country;
    $images = null;
    $descriptions = null;
    $decalTime = 0;
    $lat = 0;
    $long = 0;
    $details = null;
    $dbQuery->connect();
    $country_name = $dbQuery->getCountryName ($country);
    $countryDetails = $dbQuery->getCountryDetails ($country);
    $dbQuery->disconnect();
    $regionDetails = null;
    $cityDetails = null;

    // Récupère la date de départ
    $leavingDate = $dest->startingDate + (3600*24*$dest->nbDays);

    // Option de rafraichissement des images
    if ($dest->image && count ($dest->images) > 0) {
      $imgTemp = null;
      foreach ($dest->images as $key => $image) {
        $images[] = $image->name;
        $descriptions[] = htmlentities($image->description, ENT_QUOTES);
      }
    }

    // Récupère le nom des régions et villes de toutes les destinations
    $dbQuery->connect();
    $allDest =  $routeManager->getDestinations ($dest->category, true);
    $allDest = sortObjBy ($allDest, 'startingDate', SORT_ASC);
    if (count ($allDest) > 0) {
      foreach ($allDest as $keyDest => $itemDest) {
        $itemDest->region = $dbQuery->getRegionName($country, $itemDest->region);
        $itemDest->city = $dbQuery->getCityName($country, $itemDest->city);
        if ($itemDest->cityName <> '') $itemDest->city = $itemDest->cityName;
      }
    }

    // Récupère les informations d'une région
    if ($countryDetails) {
      $regionDetails = $dbQuery->getRegionDetails ($country, $dest->region);
      if ($regionDetails) {
        if ($property->showDescription) $description = $dbQuery->getDescription ($country, $dest->region, $language);
        $dest->region = $regionDetails->name;
        $lat = $regionDetails->lattitude;
        $long = $regionDetails->longitude;
      }
      $cityDetails = $dbQuery->getCityDetails ($country, $dest->city);
      if ($cityDetails) {
        if ($dest->cityName <> '')
          $dest->city = $dest->cityName;
        else
          $dest->city = $cityDetails->name;
        $lat = $cityDetails->lattitude;
        $long = $cityDetails->longitude;
      }
    }

    // Création des détails
    if ($property->showDescription && $countryDetails) {
      if ($regionDetails) {
        $decalTime = $dbQuery->getRegionTime ($country, $dest->region);
        $details[0][0] = $msgHandler->get('populationOf').' '.$dest->region;
        if ($regionDetails->population == 0)
          $details[0][1] = $msgHandler->get('noInfo');
        else
          $details[0][1] = nformat(utf8_decode ($regionDetails->population)).' '.$msgHandler->get('inhabitant');
        $details[1][0] = $msgHandler->get('capital');
        if ($regionDetails->capitale=='')
          $details[1][1] = $msgHandler->get('noInfo');
        else
          $details[1][1] = $regionDetails->capitale;
      }
      if ($cityDetails) {
        $decalTime = $dbQuery->getCountryTime ($country);
        $details[3][0] = $msgHandler->get('altitudeOf').' '.$dest->city;
        if ($cityDetails->altitude == 0 || $dest->cityName<>'')
          $details[3][1] = $msgHandler->get('noInfo');
        else
          $details[3][1] = nformat($cityDetails->altitude)."m.";
        $details[4][0] = $msgHandler->get('populationOf').' '.$dest->city;
        if ($cityDetails->population == 0 || $dest->cityName<>'')
          $details[4][1] = $msgHandler->get('noInfo');
        else
          $details[4][1] = nformat(utf8_decode ($cityDetails->population)).' '.$msgHandler->get('inhabitant');
        $details[5][0] = $msgHandler->get('lattitude');
        if ($lat >= 0)
          $details[5][1] = number_format (abs($lat), 2, '�', ''). "' N";
        else
          $details[5][1] = number_format (abs($lat), 2, '� ', ''). "' S";
        $details[6][0] = $msgHandler->get('longitude');
        if ($long >= 0)
          $details[6][1] = number_format (abs($long), 2, '� ', ''). "' E";
        else
          $details[6][1] = number_format (abs($long), 2, '� ', ''). "' O";
      }
    }
    $dbQuery->disconnect();

    // Gestion de la carte
    if ($dest->showmap) {
      $showWorld[0] = '';
      $showWorld[1] = '';
      $decalage['x'] = 0;
      $decalage['y'] = 0;
      $worldmap = new Worldmap ($smarty, $routeManager);
      if ($dest->map=='') {
        // Calcule de la lattitude du pays et de la valeur du zoom
        $showScrollBar = 0;
        $tailleX = 4600;
        $tailleY = 2340;
        $decalage = calculPosition ($lat, $long, $tailleX, $tailleY);
        $position = $decalage;
        $decalage['x'] = $decalage['x'] - ($tailleX/20);
        $decalage['y'] = $decalage['y'] - ($tailleY/20);
        // Affichage de la carte
        $showWorld = $worldmap->showPoint($position, 10, false);
      } else {
        // Initialisation des données pour la carte
        $showScrollBar = 1;
        // Affichage de la carte
        $showWorld = $worldmap->showMap ($mapImgPath, $dest->map, false);
      }
    }

    // Assignation de Smarty
    $smarty->assign ('leavingDate', $leavingDate);
    $smarty->assign ('dest', $dest);
    $smarty->assign ('picPath', PICS_PATH);
    $smarty->assign ('countryID', $country);
    $smarty->assign ('userLanguage', $language);
    $smarty->assign ('description', $description);
    $smarty->assign ('country', $country_name);
    $smarty->assign ('allDest', $allDest);
    $smarty->assign ('webImgPath', $webImgPath);
    $smarty->assign ('details', $details);
    $displayHeader = $smarty->fetch ($pageNameHeader.'.tpl');
    $displayContent = $smarty->fetch ($pageNameContent.'.tpl');
    $displayDescription = $smarty->fetch ($pageNameDescription.'.tpl');

    // Renvoi le contenu vers xajax pour affichage
    $response->addAssign ("headerShow", "innerHTML", $displayHeader);
    $response->addAssign ("contentShow", "innerHTML", $displayContent);
    $response->addAssign ("descriptionShow", "innerHTML", $displayDescription);
    if ($category->showmap) {
      $response->addAssign ("mapShow", "innerHTML", $showWorld[1]);
      $response->addScript ($showWorld[0]);
      $response->addScript ("initWorld(".-$decalage['x'].", ".-$decalage['y'].", ".$showScrollBar.");");
    } else {
      $response->addAssign ("mapShow", "innerHTML", '');
    }
    $response->addScriptCall ("clockajust", $decalTime);
    if ($images) $response->addScriptCall("addImageToSlide", $images, $descriptions);
    return $response->getXML();
  }//showDestination


  // Affiche un pays
  function showCountry ($catInput) {
    // Initialisation des variables globales
    global $routeManager;
    global $costsManager;
    global $dbQuery;
    global $property;
    global $msgHandler;
    global $language;
    global $smarty;
    global $webImgPath;
    global $categories;
    global $albumName;
    global $mapImgPath;
    global $tableQuality;

    // Vérification des variables en entrée
    $response = new xajaxResponse('ISO-8859-1');
    $category = $routeManager->getCategoryBy ('id', $catInput);
    $allDest =  $routeManager->getDestinations ($catInput, true);
    if (!$category || !$allDest) {$response->addRedirect(PAGE_DESTINATION);return $response->getXML();}
    $allDest = sortObjBy ($allDest, 'startingDate', SORT_ASC);

    // Initialisation des données
    $pageNameHeader = "showheader";
    $pageNameDescription = "showdescription";
    $pageNameContent = "showcountry";
    $country = $category->country;
    $costTransport = $costsManager->getCosts(0, $category->id);
    $costAccommodation = $costsManager->getCosts(1, $category->id);
    $costFood = $costsManager->getCosts(2, $category->id);
    $leavingDate = "";
    $description = "";
    $decalTime = 0;
    $details = null;
    $dbQuery->connect();
    $country_name = $dbQuery->getCountryName ($country);
    $countryDetails = $dbQuery->getCountryDetails ($country);
    $dbQuery->disconnect();

    // Récupère la date de départ
    if ($allDest[sizeof($allDest)-1]->startingDate > 0 && $allDest[sizeof($allDest)-1]->nbDays > 0) {
      $leavingDate = $allDest[sizeof($allDest)-1]->startingDate + (3600*24*$allDest[sizeof($allDest)-1]->nbDays);
    } else {
      $leavingDate = '('.$msgHandler->get('noInfo').')';
    }

    // Gestion du budget
    $total = array();
    if (($category->budget_accommodation_planed > 0 || $category->budget_food_planed > 0 || $category->budget_transport_planed > 0 &&
         $category->budget_other_planed > 0) && ($category->budget_accommodation_spent > 0 || $category->budget_food_spent > 0 ||
         $category->budget_transport_spent > 0 && $category->budget_other_spent > 0) && $property->showStats) {
      $stats = false;
      $total['planed'] = $category->budget_accommodation_planed+$category->budget_food_planed+$category->budget_transport_planed+$category->budget_other_planed;
      $total['spent'] = $category->budget_accommodation_spent+$category->budget_food_spent+$category->budget_transport_spent+$category->budget_other_spent;
    } else {
      $stats = $msgHandler->get('noInfo');
    }

    // Gestion des conseils
    if ($category->text == '') $category->text = $msgHandler->get('noInfo');

    // Récupère le nom des régions et villes
    $dbQuery->connect();
    foreach ($allDest as $keyDest => $itemDest) {
      $itemDest->region = $dbQuery->getRegionName($country, $itemDest->region);
      $itemDest->city = $dbQuery->getCityName($country, $itemDest->city);
      if ($itemDest->cityName <> '') $itemDest->city = $itemDest->cityName;
      if ($itemDest->images) {
        $itemDest->text = shorten ($itemDest->text, 50);
        if (count ($itemDest->images) > 3) $itemDest->images = array_slice ($itemDest->images, 0, 3);
      } else {
        $itemDest->text = shorten ($itemDest->text, 150);
      }
    }

    // Récupère la monnaie courante du pays
    $countrycurrency = $dbQuery->getCountryCurrency ($country);

    // Création des détails
    if ($property->showDescription && $countryDetails) {
      $decalTime = $dbQuery->getCountryTime ($country);
      $description = $dbQuery->getDescription ($country, 0, $language);
      $details[0][0] = $msgHandler->get('populationOf').' '.$country_name;
      if ($countryDetails->population == 0)
        $details[0][1] = $msgHandler->get('noInfo');
      else
        $details[0][1] = nformat(utf8_decode ($countryDetails->population)).' '.$msgHandler->get('inhabitant');
      $details[1][0] = $msgHandler->get('capital');
      if ($countryDetails->capitale=='')
        $details[1][1] = $msgHandler->get('noInfo');
      else
        $details[1][1] = $countryDetails->capitale;
      $details[2][0] = $msgHandler->get('surface');
      if ($countryDetails->capitale=='')
        $details[2][1] = $msgHandler->get('noInfo');
      else
        $details[2][1] = nformat($countryDetails->surface).' km2';
      $details[3][0] = $msgHandler->get('continent');
      if ($countryDetails->capitale=='')
        $details[3][1] = $msgHandler->get('noInfo');
      else
        $details[3][1] = $msgHandler->getTable('continent', $countryDetails->continent);
    }
    $dbQuery->disconnect();

    // Remplace les valeurs des Costs pour affichages
    if (count ($costTransport) > 0) {
      foreach ($costTransport as $keyCost => $costItem) {
        $costTransport[$keyCost]->type = $msgHandler->getTable ('arrivalBy', $costItem->type);
        $costTransport[$keyCost]->quality = $tableQuality[$costItem->quality];
      }
    }
    if (count ($costAccommodation) > 0) {
      foreach ($costAccommodation as $keyCost => $costItem) {
        $costAccommodation[$keyCost]->type = $msgHandler->getTable ('accommodation', $costItem->type);
        $costAccommodation[$keyCost]->quality = $tableQuality[$costItem->quality];
        $costAccommodation[$keyCost]->description = str_replace("\r\n", "<br />", $costAccommodation[$keyCost]->description);
        $costAccommodation[$keyCost]->description = str_replace("\n", "<br />", $costAccommodation[$keyCost]->description);
        $costAccommodation[$keyCost]->description = str_replace("\r", "<br />", $costAccommodation[$keyCost]->description);
        $costAccommodation[$keyCost]->description = str_replace("'", " ", $costAccommodation[$keyCost]->description);
        $costAccommodation[$keyCost]->address = str_replace("\r\n", "<br />", $costAccommodation[$keyCost]->address);
        $costAccommodation[$keyCost]->address = str_replace("\n", "<br />", $costAccommodation[$keyCost]->address);
        $costAccommodation[$keyCost]->address = str_replace("\r", "<br />", $costAccommodation[$keyCost]->address);
        $costAccommodation[$keyCost]->address = str_replace("'", "�", $costAccommodation[$keyCost]->address);
      }
    }
    if (count ($costFood) > 0) {
      foreach ($costFood as $keyCost => $costItem) {
        $costFood[$keyCost]->type = $msgHandler->getTable ('food', $costItem->type);
        $costFood[$keyCost]->quality = $tableQuality[$costItem->quality];
        $costFood[$keyCost]->description = str_replace("\r\n", "<br />", $costFood[$keyCost]->description);
        $costFood[$keyCost]->description = str_replace("\n", "<br />", $costFood[$keyCost]->description);
        $costFood[$keyCost]->description = str_replace("\r", "<br />", $costFood[$keyCost]->description);
        $costFood[$keyCost]->description = str_replace("'", " ", $costFood[$keyCost]->description);
        $costFood[$keyCost]->address = str_replace("\r\n", "<br />", $costFood[$keyCost]->address);
        $costFood[$keyCost]->address = str_replace("\n", "<br />", $costFood[$keyCost]->address);
        $costFood[$keyCost]->address = str_replace("\r", "<br />", $costFood[$keyCost]->address);
        $costFood[$keyCost]->address = str_replace("'", "�", $costFood[$keyCost]->address);
      }
    }

    // Gestion de la carte
    if ($category->showmap) {
      $showWorld[0] = '';
      $showWorld[1] = '';
      $decalage['x'] = 0;
      $decalage['y'] = 0;
      $worldmap = new Worldmap ($smarty, $routeManager);
      if ($category->map=='') {
        // Calcule de la lattitude du pays et de la valeur du zoom
        $showScrollBar = 0;
        $lat = 0;
        $long = 0;
        $dbQuery->connect();
        $extremity = $dbQuery->getExtremityCountry($country);
        $dbQuery->disconnect();
        if ($extremity) {
          $widthCountry = $extremity['maxlat'] - $extremity['minlat'];
          $heightCountry = $extremity['maxlong'] - $extremity['minlong'];
          $lat = ($widthCountry / 2) + $extremity['minlat'];
          $long = ($heightCountry / 2) + $extremity['minlong'];
        }
        // Initialisation des données pour la carte
        if ($widthCountry > $heightCountry) $maxLength = $widthCountry; else $maxLength = $heightCountry;
        if ($maxLength > 10 && $maxLength < 40)
          $zoom = 10 - (($maxLength-10) / 4.5);
        elseif ($maxLength <= 10)
          $zoom = 10;
        else
          $zoom = 3.5;
        $tailleX = 460 * $zoom;
        $tailleY = 234 * $zoom;
        $decalage = calculPosition ($lat, $long, $tailleX, $tailleY);
        $position = $decalage;
        $decalage['x'] = $decalage['x'] - ($tailleX/($zoom*2));
        $decalage['y'] = $decalage['y'] - ($tailleY/($zoom*2));
        $firstDestination = $routeManager->getFirstLastDestination($category->id, false);
        if ($firstDestination) $decal = $firstDestination->id; else $decal = 0;
        // Affichage de la carte
        $showWorld = $worldmap->showDestinations($category, $decal, $position, $zoom, false);
      } else {
        // Initialisation des données pour la carte
        $showScrollBar = 1;
        // Affichage de la carte
        $showWorld = $worldmap->showMap ($mapImgPath, $category->map, false);
      }
    }

    // Assignation de Smarty
    $smarty->assign ('picPath', PICS_PATH);
    $smarty->assign ('leavingDate', $leavingDate);
    $smarty->assign ('destinations', $allDest);
    $smarty->assign ('details', $details);
    $smarty->assign ('description', $description);
    $smarty->assign ('stats', $stats);
    $smarty->assign ('budget', $property->showBudget);
    $smarty->assign ('total', $total);
    $smarty->assign ('webImgPath', $webImgPath);
    $smarty->assign ('cat', $category);
    $smarty->assign ('countrycurrency', $countrycurrency);
    $smarty->assign ('countryID', $country);
    $smarty->assign ('costTransport', $costTransport);
    $smarty->assign ('costAccommodation', $costAccommodation);
    $smarty->assign ('costFood', $costFood);
    $smarty->assign ('userLanguage', $language);
    $displayHeader = $smarty->fetch ($pageNameHeader.'.tpl');
    $displayContent = $smarty->fetch ($pageNameContent.'.tpl');
    $displayDescription = $smarty->fetch ($pageNameDescription.'.tpl');

    // Renvoi le contenu vers xajax pour affichage
    $response->addAssign ("headerShow", "innerHTML", $displayHeader);
    $response->addAssign ("contentShow", "innerHTML", $displayContent);
    $response->addAssign ("descriptionShow", "innerHTML", $displayDescription);
    if ($category->showmap) {
      $response->addAssign ("mapShow", "innerHTML", $showWorld[1]);
      $response->addScript ($showWorld[0]);
      $response->addScript ("initWorld(".-$decalage['x'].", ".-$decalage['y'].", ".$showScrollBar.");");
    } else {
      $response->addAssign ("mapShow", "innerHTML", '');
    }
    $response->addScriptCall ("clockajust", $decalTime);
    return $response->getXML();
  }//showCountry

  // Créé un accès à la plateforme de fichiers sécurisés
  function accessSecure () {
    global $dbQuery;
    global $albumName;
    global $language;
    $response = new xajaxResponse('ISO-8859-1');
    $dbQuery->connect();
    $key = $dbQuery->createNewAccessToSecuredFiles($albumName);
    $dbQuery->disconnect();
    $response->addScript ("window.open('".PAGE_SECUREFILEVIEWER."?id=".$key."&album=".$albumName."&lang=".$language."', null, 'height=480,width=640,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes');");
    return $response->getXML();
  }

  $xajax->processRequests();

?>
