<?php
  include_once('../lib/init.php');


  // Initialisation des données
  $pageName = "addressbook";
  $contacts = $contactsManager->getContacts();
  foreach ($contacts as $keyCont => $contact) {
    if ($contact->email == '') unset ($contacts[$keyCont]);
  }
  if (!isset ($_SESSION['addressbook']) || !isset($_POST['init_'.$pageName])) {
    $_SESSION['addressbook']['to_address'] = array();
    $_SESSION['addressbook']['cc_address'] = array();
    $_SESSION['addressbook']['bcc_address'] = array();
  }


  // Met � jour le formulaire d'envoi d'email et ferme la fenêtre
  if (isset($_POST['selected']) && $_POST['selected']=='true') {
    $to_Str = '';
    $cc_Str = '';
    $bcc_Str = '';
    if ($_SESSION['addressbook']['to_address']) {
      foreach ($_SESSION['addressbook']['to_address'] as $to_adr) {
        if ($to_Str=='') $to_Str = $to_adr; else $to_Str .= ', '.$to_adr;
      }
    }
    if ($_SESSION['addressbook']['cc_address']) {
      foreach ($_SESSION['addressbook']['cc_address'] as $cc_adr) {
        if ($cc_Str=='') $cc_Str = $cc_adr; else $cc_Str .= ', '.$cc_adr;
      }
    }
    if ($_SESSION['addressbook']['bcc_address']) {
      foreach ($_SESSION['addressbook']['bcc_address'] as $bcc_adr) {
        if ($bcc_Str=='') $bcc_Str = $bcc_adr; else $bcc_Str .= ', '.$bcc_adr;
      }
    }
    unset ($_SESSION['addressbook']);
    echo ('<script language="javascript">self.close();</script>');
    if ($to_Str<>'') echo ('<script language="javascript">if (window.opener.form.to.value!=\'\') window.opener.form.to.value+=\', '.$to_Str.'\';else window.opener.form.to.value=\''.$to_Str.'\';</script>');
    if ($cc_Str<>'') echo ('<script language="javascript">if (window.opener.form.cc.value!=\'\') window.opener.form.cc.value+=\', '.$cc_Str.'\';else window.opener.form.cc.value=\''.$cc_Str.'\';</script>');
    if ($bcc_Str<>'') echo ('<script language="javascript">if (window.opener.form.bcc.value!=\'\') window.opener.form.bcc.value+=\', '.$bcc_Str.'\';else window.opener.form.bcc.value=\''.$bcc_Str.'\';</script>');
  }


  // Ajout d'un ou plusieurs contacts
  if (isset ($_POST['contacts'])) {
    $contactToAdd = array();
    foreach ($_POST['contacts'] as $itemContact) {
      foreach ($contacts as $contact) {
        if ($contact->id == $itemContact) $contactToAdd[$itemContact] = $contact->email;
      }
    }
    if ($contactToAdd && isset ($_POST['actions'])) {
      switch ($_POST['actions']) {
        case 'to+' : {$_SESSION['addressbook']['to_address'] = $contactToAdd+$_SESSION['addressbook']['to_address'];break;}
        case 'cc+' : {$_SESSION['addressbook']['cc_address'] = $contactToAdd+$_SESSION['addressbook']['cc_address'];break;}
        case 'bcc+' : {$_SESSION['addressbook']['bcc_address'] = $contactToAdd+$_SESSION['addressbook']['bcc_address'];break;}
      }
    }
  }


  // Retrait d'un ou plusieurs contacts dans le champs "To"
  if (isset ($_POST['to_address'])) {
    $contactToAdd = array();
    foreach ($_POST['to_address'] as $itemContact) {
      foreach ($contacts as $contact) {
        if ($contact->id == $itemContact && isset ($_SESSION['addressbook']['to_address'][$contact->id])) {
          unset ($_SESSION['addressbook']['to_address'][$contact->id]);
        }
      }
    }
  }


  // Retrait d'un ou plusieurs contacts dans le champs "Cc"
  if (isset ($_POST['cc_address'])) {
    $contactToAdd = array();
    foreach ($_POST['cc_address'] as $itemContact) {
      foreach ($contacts as $contact) {
        if ($contact->id == $itemContact && isset ($_SESSION['addressbook']['cc_address'][$contact->id])) {
          unset ($_SESSION['addressbook']['cc_address'][$contact->id]);
        }
      }
    }
  }


  // Retrait d'un ou plusieurs contacts dans le champs "Bcc"
  if (isset ($_POST['bcc_address'])) {
    $contactToAdd = array();
    foreach ($_POST['bcc_address'] as $itemContact) {
      foreach ($contacts as $contact) {
        if ($contact->id == $itemContact && isset ($_SESSION['addressbook']['bcc_address'][$contact->id])) {
          unset ($_SESSION['addressbook']['bcc_address'][$contact->id]);
        }
      }
    }
  }


  // Assignation de Smarty
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('pageName', $pageName);
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->assign ('contacts', $contacts);
  $smarty->assign ('addressbook', $_SESSION['addressbook']);
  $smarty->display($pageName.'.tpl');

?>

