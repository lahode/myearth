<?php
  include_once('../lib/init.php');


  // Si l'administrateur de l'album ne s'est pas authentifié, renvoie l'utilisateur à la page d'accueil
  if (!$admin) {header("Location:".PAGE_ACCUEIL);exit();}


  // Initialisation des données
  $pageName = "import";
  $tab = array();
  $nbCol = 0;


  // Récupère le contenu du fichier et le transforme en tableau
  if (isset ($_FILES['importfile']) && isset ($_POST['separator'])) {
    if (!isset ($_FILES['importfile']['size']) || $_FILES['importfile']['size'] > 50000) $error = $msgHandler->getError ('maxsize100');
    if ($_POST['separator'] == '') $error = $msgHandler->getError ('nofile');//"Aucun s�parateur n'a été indiqu�";
    if (!isset ($_FILES['importfile']['name']) || $_FILES['importfile']['name'] == '') $error = $msgHandler->getError ('nofileSelected');
    if (!$error) {
      if (is_uploaded_file($_FILES['importfile']['tmp_name'])) {
        $content = file($_FILES['importfile']['tmp_name']);
        if ($content) {
          foreach ($content as $keyContent => $itemContent) {
            $tab[$keyContent] = explode ($_POST['separator'], $itemContent);
            $tab[$keyContent][count ($tab[$keyContent])-1]=str_replace("\n", "", $tab[$keyContent][count ($tab[$keyContent])-1]);
            $tab[$keyContent][count ($tab[$keyContent])-1]=str_replace("\r", "", $tab[$keyContent][count ($tab[$keyContent])-1]);
            $nbCol = count ($tab[$keyContent]);
          }
        } else $error = $msgHandler->getError ('invalidFile');
      } else $error = $msgHandler->getError ('uploadImpossible');
    }
  }


  // Récupère toutes les données et créé les nouveaux contacts
  if (isset ($_POST['col']) && is_array ($_POST['col']) && isset ($_POST['element']) && is_array ($_POST['element'])) {
    $contact = null;
    $empty = true;
    foreach ($_POST['element'] as $keyElement => $element) {
      do {
        $id = genID();
      } while ($contactsManager->getContact($id));
      $contact[$keyElement] = new Contact ($id, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', time());
      $cols = $_POST['col'];
      $i = 0;
      do {
        if ($cols[$i] <> 'none') {
          if ($contact[$keyElement]->{$cols}[$i] <> '') $error = $msgHandler->getError ('oneTypeByColumn');
          $contact[$keyElement]->{$cols}[$i] = $element[$i];
          $empty = false;
        }
        if ($cols[$i] == 'email') {
          if ($element[$i]=='' || $element[$i] ==' ') $contact[$keyElement]->{$cols}[$i] = '';
          elseif (!is_email($element[$i])) $error = $msgHandler->getError ('invalidEmail');
        }
        $i++;
      } while (isset ($contact[$keyElement]) && $i<count ($cols));
    }
    if (!$contact || $error || $empty) {
      $tab = $_POST['element'];
      $nbCol = count ($_POST['col']);
      if ($empty) $error = $msgHandler->getError ('columnSelect');
      if (!$error) $error = $msgHandler->getError ('importContact');
    } else {
      foreach ($contact as $itemContact) {
        $contactsManager->editContact ($itemContact);
      }
      closeWindow();
    }
  }


  // Assignation de Smarty
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('tabContact', $msgHandler->getTable('contact'));
  $smarty->assign ('error', $error);
  $smarty->assign ('confirm', $confirm);
  $smarty->assign ('tab', $tab);
  $smarty->assign ('nbCol', $nbCol);
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->display($pageName.'.tpl');

?>

