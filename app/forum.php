<?php
  include_once('../lib/init.php');


  // Si le option du forum n'est pas activé, renvoie à la page d'accueil
  if (!$menuOption->forum) {header ("Location:".PAGE_ACCUEIL);exit();}


  // Initialisation des données
  $pageName = "forum";
  $splitDisplay = new SplitDisplay (4, 9);
  $name="";
  $email="";
  $subject="";
  $text="";
  $images=null;
  $imgForum = new Media ('forum', $msgHandler, $fileManager, $albumManager, $quota_path, $forumImgPath, false);


  // Gestion de l'affichage des résultats
  if (isset($_GET['next'])) {
    $splitDisplay->start=$_GET['next'];
    if (isset($_GET['page'])) {
      $splitDisplay->first=$_GET['page'];
    } else {
      $splitDisplay->first=$_GET['next']*$splitDisplay->length;
    }
  }


  // Initialisation des données renvoyées par le formulaire
  if (!isset($_POST['init_'.$pageName]) || (isset ($_POST['cancel']) && $_POST['cancel']=='true')) {
    $fileManager->init();
  } else {
    $name = addslash($_POST['personalName']);
    $email = addslash($_POST['personalEmail']);
    $subject = addslash($_POST['subject']);
    $text = addslash($_POST['text']);
    $images = $fileManager->getTempFiles('forum');
  }


  // Si le bouton "Save" a été pressé, sauvegarde les informations de la destination �dit�e
  if (isset ($_POST['save']) && $_POST['save']=='true') {
    if ($email<>'' && !is_email ($email)) $error = $msgHandler->getError ('invalidEmail');
    if ($subject=='') $error = $msgHandler->getError ('insertSubject');
    if ($name=='') $error = $msgHandler->getError ('insertName');
    if (!$error) {
      $images = $fileManager->saveTempFiles ('forum', $forumImgPath);
      $messageToSave = new MessageSender ($forumManager->getNewID(), $name, $email, time(), $subject, $text, $images);
      $forumManager->insertMessage ($messageToSave);
      if ($property->sendEmails) {
        if ($email=='') $email = USER_EMAIL;
        $newsToSend = new MessageObj (genID(), time(), 'public', $subject, $text, $images);
        $emailsToSend = $albumManager->getEmails();
        if ($emailsToSend) {
          $messageToSend = convertToNews ($smarty, $newsToSend, $language, $albumName, $webImgPath, '', true);
          foreach ($emailsToSend as $singlemail) {$mailSender->model->addTo($singlemail, $albumManager->getOwner());}
          $mailSender->model->addFROM ($email, $name);
          $mailSender->model->addHTML ($messageToSend[0]->text);
          $mailSender->model->addSubject ($messageToSend[0]->subject);
          if (isset ($messageToSend[1])) $mailSender->addAttachments ($messageToSend[1], true);
          if (!$mailSender->sender->send()) $error = $msgHandler->getError ('emailSent');
        }
      }
      if (isset ($_SESSION['menuCount'])) unset ($_SESSION['menuCount']);
      if ($error)
        header ("Location:".PAGE_REDIRECTION."?redirection=forum&error=".$error);
      else
        header ("Location:".PAGE_REDIRECTION."?redirection=forum");
      exit();
    }
  }


  // Si le bouton "Suppression du message" a été pressé, supprime le message
  if (isset ($_POST['deleteMessage']) && $_POST['deleteMessage'] <> '') {
    $forumManager->deleteMessage ($_POST['deleteMessage']);
    if (isset ($_SESSION['menuCount'])) unset ($_SESSION['menuCount']);
    header ("Location:".PAGE_REDIRECTION."?redirection=forum");
    exit();
  }


  // Si le bouton "Add Image" a été pressé, ajoute l'image à la liste
  if (isset ($_POST['addImage']) && $_POST['addImage']=='save') {
    $resultAdd = $imgForum->addImage($_POST['imageDescription']);
    if ($resultAdd[0] && count ($resultAdd[0]) > 0) $images = $resultAdd[0];
    if ($resultAdd[1]<>'') $error = $resultAdd[1];
  }


  // Si le bouton "Delete Image" a été pressé, efface l'image
  if (isset ($_POST['deleteImage']) && $_POST['deleteImage']<>'') {
    $images = $fileManager->deleteTempFile ('forum', $_POST['deleteImage']);
  }


  // Gestion de l'affichage des résultats
  $newMessage = new MessageSender ('', $name, $email, fdate(time()), $subject, $text, $images);
  $splitDisplay->total = $forumManager->getNbMessages ();
  $message = $forumManager->getMessages ($splitDisplay->first, $splitDisplay->length);
  if ($splitDisplay->length > 0) {
    $splitDisplay->loop = ceil(($splitDisplay->total)/$splitDisplay->length);
    $splitDisplay->first = ceil(($splitDisplay->first)/$splitDisplay->length);
  }


  // Gestion des emoticons
  $emotes = Emoticon::getEmotes (PICS_PATH.'/emoticon/');
  if ($message) {
    foreach ($message as $itemmessage) {
      $itemmessage->text = Emoticon::setEmotes (PICS_PATH.'/emoticon/', $itemmessage->text);
    }
  }
  $nbEmote = count ($emotes);


  // Assignation de Smarty
  $smarty->assign ('imgPath', $forumImgPath);
  $smarty->assign ('tempPath', $tempPath);
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuForum'), $admin, 3));
  $smarty->assign ('imgForum', $imgForum->output(''));
  $smarty->assign ('emotes', $emotes);
  $smarty->assign ('nbEmote', $nbEmote);
  $smarty->assign ('textarea', 'document.form.text');
  $smarty->assign ('showEmote', $smarty->fetch ('emoticon.tpl'));
  $smarty->assign ('splitDisplay', $splitDisplay);
  $smarty->assign ('message', $message);
  $smarty->assign ('newMessage', $newMessage);
  $smarty->assign ('pageName', $pageName);
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->assign ('admin', $admin);
  $smarty->display($pageName.'.tpl');

?>

