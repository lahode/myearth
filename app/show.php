<?php
  include_once('../lib/init.php');
  include_once('../xajax/xajax.inc.php');

  // Initialisation des données
  $idCat = "";
  $idDest = "";


  // Récupère les informations envoyées
  if (isset ($_GET['cat'])) {
    $idCat = $_GET['cat'];
    $category = $routeManager->getCategoryBy ('id', $idCat);
    if (!$category) {header("Location:".PAGE_DESTINATION);exit();}
  } elseif (isset ($_GET['dest'])) {
    $idDest = $_GET['dest'];
    $dest = $routeManager->getDestinationBy ('id', $idDest);
    if ($dest) {
      $category = $routeManager->getCategoryBy ('id', $dest->category);
      $idCat = $category->id;
    } else {header("Location:".PAGE_DESTINATION);exit();}
  } else {
    header("Location:".PAGE_DESTINATION);exit();
  }


  // Initialisation des données
  $pageName = "show";
  $country = $category->country;
  $dbQuery->connect();
  $country_name = $dbQuery->getCountryName ($country);
  $dbQuery->disconnect();
  $decalTime = 0;
  $player = null;
  if ($property->showDescription) {
    $dbQuery->connect();
    $decalTime = $dbQuery->getCountryTime ($country);
    $dbQuery->disconnect();
    $details = true;
  } else $details = false;


  // Initialisation de la musique
  if ($category->sound) {
    $player = $mediaManager->createSoundLink($soundPath.$category->sound, 1);
  }


  // Assignation de Smarty
  $smarty->assign ('cat', $idCat);
  $smarty->assign ('dest', $idDest);
  $smarty->assign ('decalTime', $decalTime);
  $smarty->assign ('details', $details);
  $smarty->assign ('webImgPath', $webImgPath);
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, '<span style="cursor:pointer" onclick="xajax_showCountry('.$idCat.');">'.$msgHandler->get('menuDestination').'-'.$country_name.'</span>', $admin, 3));
  $smarty->assign ('xajax_javascript', $xajax->getJavascript('../xajax/'));
  $smarty->assign ('player', $player);
  $smarty->display($pageName.'.tpl');

?>

