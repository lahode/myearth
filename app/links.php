<?php
  include_once('../lib/init.php');


  // Si l'administrateur de l'album ne s'est pas authentifié, renvoie l'utilisateur à la page d'accueil
  if (!$admin) {header("Location:".PAGE_ACCUEIL);exit();}
  $pageName = "links";
  $links = $albumManager->getLinks();


  // Initialisation des données renvoyées par le formulaire
  if (isset($_POST['init_'.$pageName])) {
    $link = new Link (addslash($_POST['name']), addslash($_POST['link']), addslash($_POST['description']));
    $action = $_POST['actions'];
  } else {
    $link = null;
    $action = "insert";
  }


  // Si le bouton "Save" a été pressé, sauvegarde le nouveau lien
  if (isset ($_POST['save']) && $_POST['save']=='true') {
    if ($link && $link->name<>'' && $link->link<>'') {
      if (is_url($link->link)) {
        if ($action == 'edit' && !$albumManager->getLink($link->name)) {
          $error = $msgHandler->getError ('inexistantLink');
        } elseif ($action == 'insert' && $albumManager->getLink($link->name)) {
          $error = $msgHandler->getError ('existantName');
        } else {
          $albumManager->editLink ($link);
          header ("Location:".PAGE_REDIRECTION."?redirection=links");
          exit();
        }
      } else {
        $error = $msgHandler->getError ('invalidLink');
      }
    } else {
      $error = $msgHandler->getError ('insertNameorLink');
    }
  }


  // Si le bouton "Edit" a été pressé, edite le lien
  if (isset ($_POST['editLink']) && $_POST['editLink']<>'') {
    $link = $albumManager->getLink($_POST['editLink']);
    $action = "edit";
  }


  // Si le bouton "Delete" a été pressé, supprime le lien
  if (isset ($_POST['deleteLink']) && $_POST['deleteLink']<>'') {
    $albumManager->deleteLink ($_POST['deleteLink']);
  }


  // Si le bouton "Moveup" a été pressé, remonte le lien d'un cran
  if (isset ($_POST['moveup']) && $_POST['moveup']<>'') {
    if (count ($links) > 1) {
      $lastLink = null;
      foreach ($links as $itemLink) {
        if ($itemLink->name == $_POST['moveup'] && $lastLink) $albumManager->switchLinks ($itemLink->name, $lastLink->name);
        $lastLink = $itemLink;
      }
    }
    $links = $albumManager->getLinks();
  }


  // Si le bouton "Movedown" a été pressé, descend le lien d'un cran
  if (isset ($_POST['movedown']) && $_POST['movedown']<>'') {
    if (count ($links) > 1) {
      $lastLink = null;
      foreach ($links as $itemLink) {
        if ($lastLink) {$albumManager->switchLinks ($lastLink->name, $itemLink->name);$lastLink = null;}
        if ($itemLink->name == $_POST['movedown']) $lastLink = $itemLink;
      }
    }
    $links = $albumManager->getLinks();
  }


  // Assignation de Smarty
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuLink'), $admin, 4));
  $smarty->assign ('link', $link);
  $smarty->assign ('action', $action);
  $smarty->assign ('links', $links);
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->assign ('pageName', $pageName);
  $smarty->display($pageName.'.tpl');

?>

