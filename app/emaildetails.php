<?php
  include_once('../lib/init.php');


  // Si l'administrateur de l'album ne s'est pas authentifié, renvoie l'utilisateur à la page d'accueil
  if (!$admin) {header ("Location:".PAGE_ACCUEIL);exit();}


  // Si le bouton "Annuler" a été pressé, renvoie à la page des contacts
  if ((isset ($_POST['xbb12back']) && $_POST['xbb12back'] == 'true') || (!isset ($_GET['id']))) {
    header("Location:".PAGE_MAILBOX."?box=".$_POST['xbb12backTo']."&saveOrder=true");exit();
  }


  // Initialisation des données
  $pageName = "emaildetails";
  if (isset ($_GET['box']) && $_GET['box']=='out') {
    $box = $_GET['box'];
    $attachmentPath = $attachedOutboxPath;
  } else {
    $box = 'in';
    $attachmentPath = $attachedInboxPath;
  }
  

  // Transfère une pièce jointe dans la bibliothèque
  if (isset ($_POST['xbb12transfer']) && $_POST['xbb12transfer']<>'' && isset ($_POST['xbb12transferName']) && $_POST['xbb12transferName']<>'') {
    $fileToTransfer = $attachmentPath.$_POST['xbb12transfer'];
    $fileName = $_POST['xbb12transferName'];
    if (@getimagesize($fileToTransfer)) {
      $originalImg = '';
      if ($origImgPath<>'') $originalImg = $origImgPath.$fileName;
      $error=$mediaManager->saveImage (array('filename' => $fileToTransfer), $webImgPath.$fileName, $originalImg, TAILLE_IMG, 0, 300);
    } elseif (strtoupper(substr ($fileName, -3)) == 'MP3') {
      if (!@copy($fileToTransfer, $soundPath.$fileName)) {
        $error = $msgHandler->getError('copyImpossible').' '.$fileName;
      }
    } elseif (strtoupper(substr ($fileName, -3)) == 'AVI' || strtoupper(substr ($fileName, -3)) == 'MPG' || strtoupper(substr ($fileName, -3)) == 'MOV' || strtoupper(substr ($fileName, -3)) == 'WMV' || strtoupper(substr ($fileName, -3)) == 'RM') {
      if (!@copy($fileToTransfer, $videoPath.$fileName)) {
        $error = $msgHandler->getError('copyImpossible').' '.$fileName;
      }
    } else {
      if (!@copy($fileToTransfer, $otherFilesPath.$fileName)) {
        $error = $msgHandler->getError('copyImpossible').' '.$fileName;
      }
    }
    $confirm = str_replace('%%%', $fileName, $msgHandler->getConfirm('uploadSuccess'));
  }


  // Supprime une pièce jointe
  if (isset ($_POST['xbb12deleteAttachment']) && $_POST['xbb12deleteAttachment']<>'') {
    $emailsManager->deleteAttachment ($_GET['id'], $_POST['xbb12deleteAttachment'], $box);
    deleteFile ($attachmentPath.$_POST['xbb12deleteAttachment']);
  }


  // Supprime l'email
  if (isset ($_POST['xbb12deleteEmail']) && $_POST['xbb12deleteEmail']<>'') {
    $tempEmail = $emailsManager->getEmail ($_POST['xbb12deleteEmail'], $box);
    if ($tempEmail) {
      $attachmentsToDelete = $tempEmail->attachments;
      $emailsManager->deleteEmail ($_POST['xbb12deleteEmail'], $box);
      if (count ($attachmentsToDelete) > 0) {
        foreach ($attachmentsToDelete as $att) {
          deleteFile ($attachmentPath.$att->content);
        }
      }
    }
    header("Location:".PAGE_MAILBOX."?box=".$_POST['xbb12backTo']."&saveOrder=true");exit();
    exit();
  }


  //Récupération des e-mails
  $email = $emailsManager->getEmail($_GET['id'], $box);
  $emailConverted = null;
  $contacts = $contactsManager->getContacts();
  if ($email) {
    $showTrash = $email->deleted;
    $email = $emailsManager->convertNamesofEmail ($email, $contacts, $box, $msgHandler, 1);
    if ($email->html==1) $showBr = false; else $showBr = true;
    $msgToNews = convertToNews ($smarty, $email, $language, $albumName, $newsImgPath, DNS_NAME, false, $showBr);
    $attachments = $email->attachments;
    $emailConverted = $msgToNews[0]->body;
  } else {
    header("Location:".PAGE_MAILBOX."?box=".$box."&saveOrder=true");exit();
    exit();
  }


  // Assignation de Smarty
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuEmailDetails'), $admin, 4));
  $smarty->assign ('idemail', $_GET['id']);
  $smarty->assign ('showTrash', $showTrash);
  $smarty->assign ('email', $emailConverted);
  $smarty->assign ('attachments', $attachments);
  $smarty->assign ('attachmentPath', $attachmentPath);
  $smarty->assign ('box', $box);
  $smarty->display($pageName.'.tpl');

?>

