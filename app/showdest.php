<?php
  include_once('../lib/init.php');


  // Affiche une destination
  function showDestination ($destInput) {
    // Initialisation des variables globales
    global $routeManager;
    global $dbQuery;
    global $language;
    global $smarty;
    global $webImgPath;
    global $msgHandler;
    global $property;

    // Vérification des variables en entrée
    $response = new xajaxResponse('ISO-8859-1');
    $dest = $routeManager->getDestinationBy ('id', $destInput);
    if ($dest) $category = $routeManager->getCategoryBy ('id', $dest->category);
    else {$response->addRedirect(PAGE_DESTINATION);return $response->getXML();}

    // Initialisation des données
    $pageNameHeader = "showheader";
    $pageNameDescription = "showdescription";
    $pageNameContent = "showdest";
    $leavingDate = "";
    $description = "";
    $country = $category->country;
    $images = null;
    $descriptions = null;
    $decalTime = 0;
    $details = null;
    $dbQuery->connect();
    $country_name = $dbQuery->getCountryName ($country);
    $countryDetails = $dbQuery->getCountryDetails ($country);
    $dbQuery->disconnect();
    $regionDetails = null;
    $cityDetails = null;

    // Récupère la date de départ
    $leavingDate = $dest->startingDate + (3600*24*$dest->nbDays);
    
    // Option de rafraichissement des images
    if (count ($dest->images) > 0) {
      $imgTemp = null;
      foreach ($dest->images as $key => $image) {
        $images[] = $image->name;
        $descriptions[] = htmlentities($image->description, ENT_QUOTES);
      }
    }

    // Récupère le nom des régions et villes de toutes les destinations
    $dbQuery->connect();
    $allDest =  $routeManager->getDestinations ($dest->category);
    $allDest = sortObjBy ($allDest, 'startingDate', SORT_ASC);
    if (count ($allDest) > 0) {
      foreach ($allDest as $keyDest => $itemDest) {
        $itemDest->region = $dbQuery->getRegionName($country, $itemDest->region);
        $itemDest->city = $dbQuery->getCityName($country, $itemDest->city);
        if ($itemDest->cityName <> '') $itemDest->city = $itemDest->cityName;
      }
    }

    // Récupère les informations d'une région
    if ($countryDetails) {
      $regionDetails = $dbQuery->getRegionDetails ($country, $dest->region);
      if ($regionDetails) {
        if ($property->showDescription) $description = $dbQuery->getDescription ($country, $dest->region, $language);
        $dest->region = $regionDetails->name;
        $lat = $regionDetails->lattitude;
        $long = $regionDetails->longitude;
      }
      $cityDetails = $dbQuery->getCityDetails ($country, $dest->city);
      if ($cityDetails) {
        if ($dest->cityName <> '')
          $dest->city = $dest->cityName;
        else
          $dest->city = $cityDetails->name;
        $lat = $cityDetails->lattitude;
        $long = $cityDetails->longitude;
      }
    }

    // Création des détails
    if ($property->showDescription && $countryDetails) {
      if ($regionDetails) {
        $decalTime = $dbQuery->getRegionTime ($country, $dest->region);
        $details[0][0] = $msgHandler->get('populationOf').' '.$dest->region;
        if ($regionDetails->population == 0)
          $details[0][1] = $msgHandler->get('noInfo');
        else
          $details[0][1] = nformat(utf8_decode ($regionDetails->population)).' '.$msgHandler->get('inhabitant');
        $details[1][0] = $msgHandler->get('capital');
        if ($regionDetails->capitale=='')
          $details[1][1] = $msgHandler->get('noInfo');
        else
          $details[1][1] = $regionDetails->capitale;
      }
      if ($cityDetails) {
        $decalTime = $dbQuery->getCountryTime ($country);
        $details[3][0] = $msgHandler->get('altitudeOf').' '.$dest->city;
        if ($cityDetails->altitude == 0 || $dest->cityName<>'')
          $details[3][1] = $msgHandler->get('noInfo');
        else
          $details[3][1] = nformat($cityDetails->altitude)."m.";
        $details[4][0] = $msgHandler->get('populationOf').' '.$dest->city;
        if ($cityDetails->population == 0 || $dest->cityName<>'')
          $details[4][1] = $msgHandler->get('noInfo');
        else
          $details[4][1] = nformat(utf8_decode ($cityDetails->population)).' '.$msgHandler->get('inhabitant');
        $details[5][0] = $msgHandler->get('lattitude');
        if ($lat >= 0)
          $details[5][1] = number_format (abs($lat), 2, '�', ''). "' N";
        else
          $details[5][1] = number_format (abs($lat), 2, '� ', ''). "' S";
        $details[6][0] = $msgHandler->get('longitude');
        if ($long >= 0)
          $details[6][1] = number_format (abs($long), 2, '� ', ''). "' E";
        else
          $details[6][1] = number_format (abs($long), 2, '� ', ''). "' O";
      }
    }
    $dbQuery->disconnect();

    // Assignation de Smarty
    $smarty->assign ('leavingDate', $leavingDate);
    $smarty->assign ('dest', $dest);
    $smarty->assign ('picPath', PICS_PATH);
    $smarty->assign ('countryID', $country);
    $smarty->assign ('userLanguage', $language);
    $smarty->assign ('description', $description);
    $smarty->assign ('country', $country_name);
    $smarty->assign ('allDest', $allDest);
    $smarty->assign ('webImgPath', $webImgPath);
    $smarty->assign ('details', $details);
    $displayHeader = $smarty->fetch ($pageNameHeader.'.tpl');
    $displayContent = $smarty->fetch ($pageNameContent.'.tpl');
    $displayDescription = $smarty->fetch ($pageNameDescription.'.tpl');

    // Renvoi le contenu vers xajax pour affichage
    $response->addAssign ("headerShow", "innerHTML", $displayHeader);
    $response->addAssign ("contentShow", "innerHTML", $displayContent);
    $response->addAssign ("descriptionShow", "innerHTML", $displayDescription);
    $response->addScriptCall ("clockajust", $decalTime);
    if ($images) $response->addScriptCall("addImageToSlide", $images, $descriptions);
    return $response->getXML();
  }//showDestination


  $xajax->processRequests();

?>
