<?php
  include_once('../lib/init.php');
  include_once('../lib/Availability.php');


  // Initialisation des données
  $pageName = "insert_tempdest";
  $country = "";
  $region = "";
  $city = "";
  $cityName = "";
  $startingDate = "";
  $arrivalBy = "";
  $country_name = "";
  $category = "";
  $dest = null;
  $availability = null;
  $destinat = null;
  $tableRegion = array();
  $tableCity = array();
  $availability_obj = new Availability ($routeManager);


  // Si l'administrateur de l'album ne s'est pas authentifié, renvoie l'utilisateur à la page d'accueil
  if (!$admin || !isset ($_GET['idCat']) || $_GET['idCat'] == '' || !isset ($_GET['idDest']) || $_GET['idDest'] == '') {header("Location:".PAGE_DESTINATION);exit();}


  // Si le bouton "Annuler" a été pressé, renvoie à la page d'accueil
  if (isset ($_POST['cancel']) && $_POST['cancel'] == 'true') {header("Location:".PAGE_DESTINATION);exit();}


  // Initialisation des données renvoyées par le formulaire
  if (!isset($_POST['init_'.$pageName])) {
    $fileManager->init();
    $destinations = $routeManager->getDestinations ($_GET['idCat']);
    if ($destinations) {
      $dest = $routeManager->getDestinationBy ('id', $_GET['idDest']);
      if ($dest) {
        $id = $dest->id;
        $category = $dest->category;
        $region = $dest->region;
        $city = $dest->city;
        $cityName = $dest->cityName;
        $startingDate = $dest->startingDate;
        $arrivalBy = $dest->arrivalBy;
      } else {
        $id = $_GET['idDest'];
        $category = $_GET['idCat'];
      }
    } else {
      $id = $_GET['idDest'];
      $category = $_GET['idCat'];
    }
    $country = $routeManager->getCategoryBy ('id', $category)->country;
  } else {
    $id = $_POST['id'];
    $category = $_POST['category'];
    $country = $_POST['country'];
    $region = $_POST['region'];
    if (isset ($_POST['city'])) $city = $_POST['city'];
    $cityName = addslash($_POST['cityName']);
    $startingDate = unfdate($_POST['startingDate']);
    $arrivalBy = $_POST['arrivalBy'];
  }


  // Si le bouton "Save" a été pressé, sauvegarde les informations de la destination éditée
  if (isset ($_POST['save']) && $_POST['save']=='true') {
    if ($id < 1) $error = $msgHandler->getError ('invalidID');
    if ($country=='0') $error = $msgHandler->getError ('selectCountry');
    if ($region=='0') $error = $msgHandler->getError ('selectRegion');
    if ($city=='0') $error = $msgHandler->getError ('selectCity');
    if (!$error) {
      $destination =  new DestinationObj ($id, $category, $region, $city, $cityName, $startingDate, 
                                          -1, $arrivalBy, '', '', '', null);
      $destination->setHide ('private');
      $routeManager->editDestination ($destination);
      if (isset ($_SESSION['countriesList'])) unset ($_SESSION['countriesList']);
      header("Location:".PAGE_DESTINATION);
      exit();
    }
  }


// ************** Initialisation des tableaux ******************************************

  $dbQuery->connect();

  // Si le bouton "search" a été pressé, recherche la ville recherch�e
  if (isset ($_POST['search']) && $_POST['search']=='true' && $_POST['searchCity']<>'') {
    $cityFound = $dbQuery->searchCityFromCountry($_POST['searchCity'], $country);
    $region = $cityFound['region'];
    $city = $cityFound['city'];
  }

  // Sélection du pays
  $country_name = $dbQuery->getCountryName ($country);

  $tableRegion = $dbQuery->getRegionsFromCountry ($country);
  $tableCity = $dbQuery->getCitiesFromRegion ($country, $region);

  $dbQuery->Disconnect();

// **************************************************************************************

  // Insère en session toutes les dates occupées pour le calendrier
  if (!isset($_POST['init_'.$pageName])) {
    $availability = $availability_obj->getDestinationAvailability($category, $id, true);
    $availability['lastDate']=(substr ($availability['lastDate'], 0, -3)+3600*24).'000';
    $_SESSION['datesBooked'] = $availability;
    if ($startingDate=='' && $availability['firstDate']<$availability['lastDate']) $startingDate = substr($availability['firstDate'], 0, -3);
  }


  // Réassigne les valeurs transformée à l'objet pour affichage
  if ($startingDate) {
    $destinat = new DestinationObj ($id, $category, $region, $city, $cityName, $startingDate, 0, $arrivalBy, '', '', '', null);
  } else {
    header("Location:".PAGE_DESTINATION);exit();
  }


  // Assignation de Smarty
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuInsert'), true, 2));
  $smarty->assign ('dest', $destinat);

  $smarty->assign ('country', $country);
  $smarty->assign ('category', $category);
  $smarty->assign ('country_name', $country_name);

  $smarty->assign ('tableRegion', $tableRegion);
  $smarty->assign ('tableCity', $tableCity);
  $smarty->assign ('tableArrivalBy', $msgHandler->getTable ('arrivalBy'));
  $smarty->assign ('pageName', $pageName);
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->assign ('dateFormat', $dateFormat);

  $smarty->display($pageName.'.tpl');

?>

