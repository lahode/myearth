<?php
  include_once('../lib/CSSCreator.php');
  include_once('../lib/init.php');


  // Si l'administrateur de l'album ne s'est pas authentifié, renvoie l'utilisateur à la page d'accueil
  if (!$admin) {header("Location:".PAGE_ACCUEIL);exit();}


  // Si le bouton "Annuler" a été pressé, renvoie à la page d'accueil
  if ((isset ($_POST['cancelFont']) && $_POST['cancelFont'] == 'true') || !isset($_GET['font'])) {
    header("Location:".PAGE_REDIRECTION."?redirection=config&getFromSession=1");
    exit();
  }


  // Initialisation des données
  $pageName = "config_font";


  // Initialisation des données renvoyées par le formulaire
  if (isset ($_POST['init_'.$pageName])) {
    if (isset ($_POST['font_textUnderline'])) $postedtextUnderline = 1; else $postedtextUnderline = 0;
    if (isset ($_POST['font_linkUnderline'])) $postedlinkUnderline = 1; else $postedlinkUnderline = 0;
    $fontstyle = new Fontstyle ($_POST['fontName'], $_POST['font_size'], $_POST['font_style'],
                                $postedtextUnderline, $postedlinkUnderline, $_POST['font_textColor'],
                                $_POST['font_linkColor'], $_POST['font_hoverColor'], $_POST['font_visitedColor']);
    if (isset ($_SESSION['fontstyles'])) $fontstyles = $_SESSION['fontstyles']['obj']; else $fontstyles = $albumManager->getFontStyles ();
    foreach ($fontstyles as $keyStyle => $itemStyle) {
      if ($itemStyle->name == $fontstyle->name) {
        $fontstyles[$keyStyle] = $fontstyle;
      }
    }
  } else {
    if (isset ($_SESSION['fontstyles'])) $fontstyles = $_SESSION['fontstyles']['obj']; else $fontstyles = $albumManager->getFontStyles ();
    foreach ($fontstyles as $itemStyle) {
      if ($itemStyle->name == $_GET['font']) {
        $fontstyle = $itemStyle;
      }
    }
  }


  // Si le bouton "SaveFont" a été pressé, sauvegarde les informations de l'élément de texte
  if (isset ($_POST['saveFont']) && $_POST['saveFont']=='true') {
    $_SESSION['fontstyles']['obj'] = $fontstyles;
    foreach ($fontstyles as $keyStyle => $itemStyle) {
      if ($itemStyle->name == $fontstyle->name) {
        $_SESSION['fontstyles']['changed'][$keyStyle] = true;
      }
    }
    unset ($fontstyle);
    header("Location:".PAGE_REDIRECTION."?redirection=config&getFromSession=1");
    exit();
  }


  // Assignation de Smarty
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get('menuConfiguration'), true, 1));
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->assign ('error', $error);
  $smarty->assign ('pageName', $pageName);
  $smarty->assign ('fontstyle', $fontstyle);
  $smarty->assign ('tableSize', $tableSize);
  $smarty->assign ('tableStyle', $msgHandler->getTable ('fontstyle'));
  $smarty->display($pageName.'.tpl');

?>