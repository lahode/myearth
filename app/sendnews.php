<?php
  include_once('../lib/init.php');
  include_once('../lib/NewsManager.php');


  // Si le option des news n'est pas activée, renvoie à la page d'accueil
  if (!$menuOption->news) {header ("Location:".PAGE_ACCUEIL);exit();}


  // Si l'administrateur de l'album ne s'est pas authentifié, renvoie l'utilisateur à la page d'accueil
  if (!$admin) {header ("Location:".PAGE_ACCUEIL);exit();}


  //Initialisation des données
  $pageName = "sendnews";
  $emails = $albumManager->getEmails();
  $destination = null;
  $selectedEmail = "";
  $regionName = "";
  if (!isset($_POST['init_'.$pageName])) {
    $message = $registerNewsManager->getSendMessage();
  } else {
    $message = new MessageSender ('5345345', '', '', time(), addslash($_POST['subject']), addslash($_POST['message']), null);
    if (isset ($_POST['email'])) {
      if (isset ($emails[$_POST['email']])) {
        $message->email = $emails[$_POST['email']];
      } else {
        $message->email = addslash($_POST['email']);
      }
    }
    if (isset ($_POST['selectedPicture']) && $_POST['selectedPicture']<>'') {
      $image = new ImageObj ($_POST['selectedPicture'], '', '');
    } else {
      $image = null;
    }
  }


  // Récupère toutes les régions
  $destinations = $routeManager->getDestinations ();
  if ($destinations) {
    $destinations = sortObjBy ($destinations, 'startingDate', SORT_ASC);
    $dbQuery->connect();
    foreach ($destinations as $itemDest) {
      $countryTemp = $routeManager->getCategoryBy ('id', $itemDest->category)->country;
      if ($itemDest->cityName)
        $regions[$itemDest->id] = $itemDest->cityName;
      else {
        $regions[$itemDest->id] = $dbQuery->getCityName($countryTemp, $itemDest->city);
      }
    }
    $dbQuery->disconnect();
  } else {
    header("Location:".PAGE_ACCUEIL."?error=*noDestinationFound");
  }


  // Récupère la destination choisie
  if (isset($_GET['dest'])) {
    $destination = $routeManager->getDestinationBy ('id', $_GET['dest']);
  } elseif (isset($_POST['dest'])) {
    $destination = $routeManager->getDestinationBy ('id', $_POST['dest']);
  } elseif (isset($_POST['region'])) {
    $destination = $routeManager->getDestinationBy ('id', $_POST['region']);
  } else {
    $destination = $destinations[sizeof ($destinations)-1];
  }


  // Récupère les information de la destination trouvée
  if ($destination) {
    $category = $routeManager->getCategoryBy ('id', $destination->category)->country;
    $dbQuery->connect();
    $regionName = $dbQuery->getCityName($category, $destination->city);
    $countryName = $dbQuery->getCountryName($category);
    $dbQuery->disconnect();
  }


  // Si le bouton "Save" a été pressé, sauvegarde le message
  if (isset ($_POST['save']) && $_POST['save']=='true') {
    $registerNewsManager->editSendMessage ($message);
    header ("Location:".PAGE_SENDNEWS."?confirm=*selfUpdated");
  }


  // Récupère l'e-mail sélectionné
  if (count ($emails) > 0) {
    foreach ($emails as $keyEmail => $itemEmail) {
      if ($itemEmail == stripslashes($message->email)) $selectedEmail = $keyEmail;
    }
  }


  // Replace les balises #region# et #country# pour affichage
  $subjectTranslated = preg_replace('#region#', $regionName, $message->subject);
  $subjectTranslated = preg_replace('#country#', $countryName, $subjectTranslated);
  $messageTranslated = preg_replace('#region#', $regionName, $message->text);
  $messageTranslated = preg_replace('#country#', $countryName, $messageTranslated);



  // Si le bouton "Send" a été pressé, sauvegarde le message et envoie un e-mail au personnes inscrites
  if (isset ($_POST['send']) && $_POST['send']=='true') {
    if (is_email ($message->email)) {
      $registerNewsManager->editSendMessage ($message);
      $registeredPersons = $registerNewsManager->getPersons();
      if (count ($registeredPersons) > 0) {
        $newsToSend = new MessageObj (genID(), time(), 'public', $subjectTranslated, $messageTranslated, array($image));
        $messageToSend = convertToNews ($smarty, $newsToSend, $language, $albumName, $webImgPath, DNS_NAME, true);
        foreach ($registeredPersons as $person) {
          $newsSender = new MailSender ();
          if (isset ($person->id)) {
            $stringId = '?unregister='.$person->id.'&album=';
            $messageToSend[0]->text = strtr($messageToSend[0]->text, array('?unregister=******&album=' => $stringId));
          }
          $newsSender->model->addTo ($person->email, $person->name);
          $newsSender->model->addFROM ($message->email);
          $newsSender->model->addHTML ($messageToSend[0]->text);
          $newsSender->model->addSubject ($messageToSend[0]->subject);
          if (isset ($messageToSend[1])) $newsSender->addAttachments ($messageToSend[1], true);
          if (!$newsSender->sender->send()) $error = $msgHandler->getError ('newsSent');
        }
        if (!$error) {
          header ("Location:".PAGE_ACCUEIL."?confirm=*newsSent");
          exit();
        }  
      } else {
        $error = $msgHandler->getError ('noRegisteredPersons');
      }
    } else {
      $error = $msgHandler->getError ('invalidEmail');
    }
  }


  // Assignation de Smarty
  $smarty->assign ('emails', $emails);
  $smarty->assign ('selectedEmail', $selectedEmail);
  $smarty->assign ('regions', $regions);
  $smarty->assign ('destination', $destination);
  $smarty->assign ('webImgPath', $webImgPath);
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuSendNews'), $admin, 1));
  $smarty->assign ('message', stripslashes($message->text));
  $smarty->assign ('email', stripslashes($message->email));
  $smarty->assign ('subject', stripslashes($message->subject));
  $smarty->assign ('subjectTranslated', $subjectTranslated);
  $smarty->assign ('messageTranslated', $messageTranslated);
  $smarty->assign ('pageName', $pageName);
  $smarty->display($pageName.'.tpl');

?>

