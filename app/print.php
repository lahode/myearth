<?php
  include_once('../lib/init.php');

  if (isset ($_GET['print'])) {
    switch ($_GET['print']) {
      case 'selectedcontacts' : {
        if (isset ($_SESSION['contact']) && isset ($_SESSION['contact']['contacts']) && count($_SESSION['contact']['contacts']) > 0) {
          $i = 0;
          $contacts = null;
          foreach ($_SESSION['contact']['contacts'] as $keyContact => $itemContact) {
            $contacts[] = $contactsManager->getContact($keyContact);
          }
          if (is_array($contacts)) {
            if (isset ($_SESSION['contact']['sortOrder']) && isset ($_SESSION['contact']['sortBy'])) {
              if ($_SESSION['contact']['sortOrder'] == 'asc') {
                $contacts = sortObjBy ($contacts, $_SESSION['contact']['sortBy'], SORT_ASC);
              } else {
                $contacts = sortObjBy ($contacts, $_SESSION['contact']['sortBy'], SORT_DESC);
              }
            }
            foreach ($contacts as $contact) {
              $monthList = $msgHandler->getTable ('month');
              $dayList = array ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31');
              if ($contact->lastname<>'') $nicknameContact = ' ('.$contact->nickname.')'; else $nicknameContact = '';
              if ($contact->country<>'') $countryContact = '<br />'.$contact->country; else $countryContact = '';
              if ($contact->tel<>'') $telContact = '<br />'.$msgHandler->get('tel').$contact->tel; else $telContact = '';
              if ($contact->fax<>'') $faxContact = '<br />'.$msgHandler->get('fax').$contact->fax; else $faxContact = '';
              if ($contact->mobile<>'') $mobileContact = '<br />'.$msgHandler->get('mobile').$contact->mobile; else $mobileContact = '';
              if ($contact->messenger<>'') $messengerContact = '<br />'.$msgHandler->get('messenger').$contact->messenger; else $messengerContact = '';
              if ($contact->comments<>'') $commentsContact = '<br />'.$msgHandler->get('comments').$contact->comments; else $commentsContact = '';
              $birthdate = explode ('-', $contact->birthdate);
              $birthday = '1';
              $birthmonth = '01';
              $birthyear = '1900';
              if (count ($birthdate)==3) {
                $birthday = $birthdate[0]-1;
                $birthmonth = $birthdate[1];
                $birthyear = $birthdate[2];
              }
              $element[$i] = $contact->title.' '.$contact->firstname.' '.$contact->lastname.$nicknameContact.'<br />'.$contact->address.'<br />'.$contact->zipcode.' '.$contact->city.$countryContact.
                            '<br />'.$contact->email.'<br />'.$msgHandler->get('born').$dayList[$birthday].' '.$monthList[$birthmonth].' '.$birthyear.'<br />'.$telContact.$faxContact.$mobileContact.$messengerContact.$commentsContact.'<br /><br />';
              $i++;
            }
          }
        }
        break;
      }
      case 'selectedemails' : {
        if (isset ($_SESSION['msg']) && isset ($_SESSION['msg']['msgs']) && count($_SESSION['msg']['msgs']) > 0) {
          $i = 0;
          $emails = null;
          $contacts = $contactsManager->getContacts();
          if (isset ($_GET['box']) && $_GET['box']=='out') $box = $_GET['box']; else $box = 'in';
          foreach ($_SESSION['msg']['msgs'] as $keyEmail => $itemEmail) {
            $emails[] = $emailsManager->getEmail($keyEmail, $box);
          }
          if (is_array($emails)) {
            if (isset ($_SESSION['msg']['sortOrder']) && isset ($_SESSION['msg']['sortBy'])) {
              if ($_SESSION['msg']['sortOrder'] == 'asc') {
                $emails = sortObjBy ($emails, $_SESSION['msg']['sortBy'], SORT_ASC);
              } else {
                $emails = sortObjBy ($emails, $_SESSION['msg']['sortBy'], SORT_DESC);
              }
            }
            foreach ($emails as $email) {
              $email = $emailsManager->convertNamesofEmail ($email, $contacts, $box, null, 2);
              $msgToNews = convertToNews ($smarty, $email, $language, $albumName, $newsImgPath, DNS_NAME, false);
              $element[$i] = $msgToNews[0]->body;
              $i++;
            }
          }
        }
        break;
      }
    }
  } else {
    closeWindow();
  }
  
  if (isset ($element) && count ($element)) {
    foreach ($element as $el) {
      print ($el.'<hr />');
    }
  }

?>