<?php
  include_once('../lib/init.php');
  include_once('Videoplayer.php');


  // Si le option des vidéos n'est pas activé, renvoie à la page d'accueil
  if (!$menuOption->video) {header ("Location:".PAGE_ACCUEIL);exit();}


  // Si le bouton "Add Video" a été pressé, ajoute l'image à la liste
  function uploadVideo($videoFromAlbum, $videoFile, $videoPath, $albumManager, $albumName, $msgHandler) {
    $video = null;
    $error = "";
    if ($videoFromAlbum<>'') {
      $video = new ImageObj ($videoFromAlbum, "");
    } elseif (isset ($videoFile['name']) && $videoFile['name']<>'') {
      if (getFreeSpace($albumManager->getQuota(), $quota_path)-$videoFile['size'] > 0) {
        $fileName = checkFileName($videoFile['name']);
        if (!$error=$mediaManager->saveVideo ($videoFile, $videoPath.$fileName, TAILLE_VID)) {
          $video = new ImageObj ($fileName, "");
        }
      } else {
        $error = $msgHandler->getError('quotaExceeded');
      }
    }
    if (!$video) $video = $error;
    return $video;
  }


  // Initialisation des données
  $pageName = "video";
  $splitDisplay = new SplitDisplay (3, 9);
  $access="";
  $name="";
  $description="";
  $videoToEdit=null;
  $videoObj=null;
  $screenWidth = 300;
  $screenHeight = 200;
  if (isset ($_GET['showvideo'])) $showvideo = $_GET['showvideo']; else $showvideo = '';
  if (isset ($_POST['videoFromAlbum'])) $videoFromAlbum = $_POST['videoFromAlbum']; else $videoFromAlbum = '';
  if (isset ($_FILES['videoName'])) $videoFile = $_FILES['videoName']; else $videoFile = '';
  if (isset ($_POST['actions']) && $_POST['actions']<>'') $action = $_POST['actions']; else $action = 'new';


  // Gestion de l'affichage des résultats
  if (isset($_GET['next'])) {
    $splitDisplay->start=$_GET['next'];
    if (isset($_GET['page'])) {
      $splitDisplay->first=$_GET['page'];
    } else {
      $splitDisplay->first=$_GET['next']*$splitDisplay->length;
    }
  }

  // Initialisation des données renvoyées par le formulaire
  if (!isset($_POST['init_'.$pageName]) || (isset ($_POST['cancel']) && $_POST['cancel']=='true') || !$admin) {
    $action = 'new';
    $id = $videoManager->getNewID();
  } else {
    $singleMessage = $videoManager->getMessage ($_POST['id']);
    $id = $_POST['id'];
    $access = 'public';
    $name = addslash($_POST['name']);
    $description = addslash($_POST['description']);
    if (isset ($_POST['videoToEdit']) && $_POST['videoToEdit']<>'') $videoObj = new ImageObj ($_POST['videoToEdit'], "");
  }


  // Si le bouton "Changer d'accès" a été pressé, modifie l'accès du message
  if (isset ($_POST['changeAccess']) && $_POST['changeAccess'] <> '') {
    $singleVideo = $videoManager->getMessage ($_POST['changeAccess']);
    if ($singleVideo->access == 'public') {
      $access = 'private';
    } else {
      $access = 'public';
    }
    $videoToSave = new MessageObj ($singleVideo->id, $singleVideo->date, $access, $singleVideo->subject, $singleVideo->text, $singleVideo->images);
    $videoManager->editMessage ($videoToSave);
    $id = $videoManager->getNewID();
    header ("Location:".PAGE_REDIRECTION."?redirection=video");
    exit();
  }


  // Si le bouton "Edition du message" a été pressé, modifie l'accès du message
  if (isset ($_POST['editVideo']) && $_POST['editVideo'] <> '') {
    $singleVideo = $videoManager->getMessage ($_POST['editVideo']);
    $id = $singleVideo->id;
    $name = $singleVideo->subject;
    $description = $singleVideo->text;
    if (isset($singleVideo->images[0])) $videoToEdit = $singleVideo->images[0];
    $action="edit";
  }


  // Si le bouton "Suppression du message" a été pressé, modifie l'accès du message
  if (isset ($_POST['deleteVideo']) && $_POST['deleteVideo'] <> '') {
    $videoManager->deleteMessage ($_POST['deleteVideo']);
    $id = $videoManager->getNewID();
    if (isset ($_SESSION['menuCount'])) unset ($_SESSION['menuCount']);
    header ("Location:".PAGE_REDIRECTION."?redirection=video");
    exit();
  }


  // Si le bouton "Save" a été pressé, sauvegarde les informations de la destination éditée
  if (isset ($_POST['save']) && $_POST['save']=='true') {
    if ($name=='') $error = $msgHandler->getError ('insertName');
    $videoUploaded = null;
    if (!$error) {
      if ($action=='new') {
        $checkUploaded = uploadVideo($videoFromAlbum, $videoFile, $videoPath, $albumManager, $albumName, $msgHandler);
        if (is_string($checkUploaded)) $error = $checkUploaded; else $videoUploaded = array ($checkUploaded);
      } else {
        if ($videoObj) $videoUploaded = array($videoObj);
      }
    }
    if (!$error) {
      $videoToSave = new MessageObj ($id, time(), $access, $name, $description, $videoUploaded);
      $videoManager->editMessage ($videoToSave);
      $id = $videoManager->getNewID();
      if (isset ($_SESSION['menuCount'])) unset ($_SESSION['menuCount']);
      header ("Location:".PAGE_REDIRECTION."?redirection=video");
      exit();
    }
  }


  // Gestion de l'affichage des résultats
  $newVideo = new MessageObj ($id, fdate(time()), $access, $name, $description, $videoToEdit);
  $splitDisplay->total = $videoManager->getNbMessages ($admin);
  $video = $videoManager->getMessages ($showvideo+$splitDisplay->first, $splitDisplay->length, $admin, 'date');
  if ($splitDisplay->length > 0) {
    $splitDisplay->loop = ceil(($splitDisplay->total)/$splitDisplay->length);
    $splitDisplay->first = ceil(($splitDisplay->first)/$splitDisplay->length);
  }


  // Lance la vidéo
  if (isset ($_GET['play']) && $_GET['play']<>'') {
    $videoToPlay = $videoManager->getMessage ($_GET['play']);
    if ($videoToPlay && isset ($videoToPlay->images[0])) {
      $player = new Videoplayer ($smarty, $_GET['play'], $screenWidth, $screenHeight);
      $smarty->assign ('player', $player->show($videoPath.$videoToPlay->images[0]->name));
    }
  }

  // Assignation de Smarty
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('splitDisplay', $splitDisplay);
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuVideo'), $admin, 3));
  $smarty->assign ('action', $action);
  $smarty->assign ('video', $video);
  $smarty->assign ('newVideo', $newVideo);
  $smarty->assign ('pageName', $pageName);
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->assign ('admin', $admin);
  $smarty->display($pageName.'.tpl');

?>

