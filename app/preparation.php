<?php
  include_once('../lib/init.php');


  // Si le option des préparatifs n'est pas activée, renvoie à la page d'accueil
  if (!$menuOption->preparation) {header ("Location:".PAGE_ACCUEIL);exit();}


  // Initialisation des données
  $pageName = "preparation";
  $prepareImgPath = $webImgPath;
  $tablePreparation = $msgHandler->getTable ('preparation');
  $newMessage = null;
  $access="";
  $text="";
  $id="";
  $images=null;
  $showEdit=false;
  $imgPrep = new Media ('preparation', $msgHandler, $fileManager, $albumManager, $quota_path, $prepareImgPath, true, 4);
  $fileManager->setOriginal ($origImgPath);


  // Initialisation des données renvoyées par le formulaire
  if (!isset($_POST['init_'.$pageName]) || (isset ($_POST['cancel']) && $_POST['cancel']=='true') || !$admin) {
    $fileManager->init();
  } elseif (isset ($_POST['editMessage']) && $_POST['editMessage'] <> '') {
    $singleMessage = $prepareManager->getMessage ($_POST['editMessage']);
    $id = $singleMessage->id;
    $text = $singleMessage->text;
    $fileManager->init();
    if ($singleMessage->images && count ($singleMessage->images) > 0) {
      foreach ($singleMessage->images as $itemImg) {
        $fileManager->addImage ('preparation', $prepareImgPath, $itemImg->name, $itemImg->description);
      }
      $images = $fileManager->getTempFiles('preparation');
    }
    $showEdit = true;
  } else {
    $singleMessage = $prepareManager->getMessage ($_POST['id']);
    $id = $_POST['id'];
    $access = 'public';
    $text = addslash($_POST['text']);
    $images = $fileManager->getTempFiles('preparation');
  }


  // Si le bouton "Save" a été pressé, sauvegarde les informations de la destination éditée
  if (isset ($_POST['save']) && $_POST['save']=='true') {
    if (isset($_POST['email']) && $_POST['email']<>'' && !is_email ($_POST['email'])) $error = $msgHandler->getError ('invalidEmail');
    if (!$error) {
      $images = $fileManager->saveTempFiles ('preparation', $prepareImgPath);
      $messageToSave = new MessageObj ($id, mktime(), $access, "", $text, $images);
      $prepareManager->editMessage ($messageToSave);
      if (isset ($_SESSION['menuCount'])) unset ($_SESSION['menuCount']);
      header ("Location:".PAGE_REDIRECTION."?redirection=preparation");
      exit();
    }
  }


  // Si le bouton "Add Image" a été pressé, ajoute l'image à la liste
  if (isset ($_POST['addImage']) && $_POST['addImage']=='save') {
    $resultAdd = $imgPrep->addImage($_POST['imageDescription']);
    if ($resultAdd[0] && count ($resultAdd[0]) > 0) $images = $resultAdd[0];
    if ($resultAdd[1]<>'') $error = $resultAdd[1];
    $showEdit = true;
  }


  // Si le bouton "Delete Image" a été pressé, efface l'image
  if (isset ($_POST['deleteImage']) && $_POST['deleteImage']<>'') {
    $images = $fileManager->deleteTempFile ('preparation', $_POST['deleteImage']);
    $showEdit = true;
  }


  // Gestion de l'affichage des résultats
  if ($showEdit) {
    $newMessage = new MessageObj ($id, fdate(mktime()), $access, $tablePreparation[$id], $text, $images);
  }
  $message = $prepareManager->getMessages ();
  foreach ($message as $keyMsg => $msg) {
    $msg->subject = $tablePreparation[$msg->id];
    if ($msg->text=='' && $msg->images==null && !$admin) {unset ($message[$keyMsg]);unset($tablePreparation[$msg->id]);}
  }


  // Assignation de Smarty
  $smarty->assign ('imgPath', $prepareImgPath);
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuPrepare'), $admin, 4));
  $smarty->assign ('imgPrep', $imgPrep->output('web'));
  $smarty->assign ('message', $message);
  $smarty->assign ('newMessage', $newMessage);
  $smarty->assign ('tablePreparation', $tablePreparation);
  $smarty->assign ('pageName', $pageName);
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->assign ('showEdit', $showEdit);
  $smarty->assign ('albumTitle', $albumTitle);
  $smarty->assign ('admin', $admin);
  $smarty->display($pageName.'.tpl');

?>

