<?php
  include_once('../lib/init.php');
  include_once ('../lib/ChatMessage.php');


  // Initialise les données principales du serveur chat
  $chatUsersFile = USERS_PATH.$albumName.'/chat/users';
  $chatMessagesFile = USERS_PATH.$albumName.'/chat/messages';
  $chatCallsFile = USERS_PATH.$albumName.'/chat/calls';
  $chatusers = new ChatMessage($chatUsersFile);
  $chatmessages = new ChatMessage($chatMessagesFile);
  $chatcalls = new ChatMessage($chatCallsFile);
  header("Expires: Mon, 26 Jul 1997 05:00:00 GMT" );
  header("Last-Modified: " . gmdate( "D, d M Y H:i:s" ) . "GMT" );
  header("Cache-Control: no-cache, must-revalidate" );  // S'assure que les requetes ne sont pas mis en cache //
  header("Pragma: no-cache" );
  header("Content-Type: text/xml"); // Tout est retourner au format XML //


  // Récupère tous la conversation existante du chat
  if (isset ($_GET['action']) && $_GET['action']=='getmessage') {
    $filename = $chatMessagesFile.'.xml';
    $handle = fopen($filename, "r");
    $content = fread ($handle, filesize ($filename));
    fclose($handle);
    echo ($content);
  }


  // Récupère tous les utilisateurs connectés au chat
  if (isset ($_GET['action']) && $_GET['action']=='getusers') {
    $filename = $chatUsersFile.'.xml';
    $handle = fopen($filename, "r");
    $content = fread ($handle, filesize ($filename));
    fclose($handle);
    echo ($content);
  }


  // Récupère tous les appels effectué au chat
  if (isset ($_GET['action']) && $_GET['action']=='getcalls') {
    $result = '';
    $username = $_SESSION['login'];
    if ($chatusers->isUser($username)) $chatusers->editUser($username);
    $chatusers->cleanUsers($chatmessages, $msgHandler->get('isDisconnected'));
    $chatcalls->cleanCalls();
    $calls = $chatcalls->getCalls();
    if (count($calls) > 0) {
      foreach ($calls as $call) {
        if ($call['to']==$_SESSION['login']) {
          if ($call['new']==1)
            $result = '#'.$call['from'].convertToUTF8($msgHandler->get('hasConnected'));
          else
            $result = '#'.$call['from'].convertToUTF8($msgHandler->get('isCalling'));
          $chatcalls->deleteCall ($call['from'], $call['to']);
        }
      }
    }
    if ($result=='') $result = count ($chatusers->getUsers()).convertToUTF8($msgHandler->get('connectedPeople'));
    echo ($result);
  }


  // Lance un appel à l'utilisateur sélectionné
  if (isset ($_GET['action']) && $_GET['action']=='call' && isset ($_POST['user']) && $_POST['user']<>'') {
    if ($_SESSION['login']<>$_POST['user']) $chatcalls->editCall ($_SESSION['login'], $_POST['user'], 0);
  }


  // Insert un nouveau message dans la conversation du chat
  if (isset ($_GET['action']) && $_GET['action']=='sendmessage' && isset ($_POST['message']) && $_POST['message']<>'' && $chatusers->isUser($_SESSION['login'])) {
    $chatmessages->insertMessage($_SESSION['login'], utf8_decode($_POST['message']), MAXLINESCHAT);
  }

?>
