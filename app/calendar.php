<?php
  include_once ('../lib/declare.php');
  include_once ('../lib/functions.php');


  // Récupération des variables de Session pour le calendrier
  initSession (SESSION_NAME);
  $pageName = "calendar";
  $datesBooked = "";
  if (isset ($_SESSION['datesBooked'])) {
    if (isset ($_GET['calNum']) && $_GET['calNum'] <> '' && isset($_SESSION['datesBooked'][$_GET['calNum']])) {
      $datesBooked = htmlspecialchars(serialize($_SESSION['datesBooked'][$_GET['calNum']]));
    } else {
      $datesBooked = htmlspecialchars(serialize($_SESSION['datesBooked']));
    }
  }
  $smarty->assign ('headercal', "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n".
                                "<html>\n<head>\n<title>Choisir la date</title>\n<style>\n  td {font-family: Tahoma, Verdana, sans-serif; font-size: 12px;}\n</style>\n".
                                "<script language=\"javascript\" src=\"../lib/js/phpTab2js.js\" type=\"text/javascript\"></script>\n".
                                "<script language=\"javascript\" src=\"../lib/js/cal.js\" type=\"text/javascript\"></script>\n".
                                "</head>\n<body bgcolor=\"#FFFFFF\" marginheight=\"5\" marginwidth=\"5\" topmargin=\"5\" leftmargin=\"5\" rightmargin=\"5\">");
  $smarty->assign ('footercal', "</body>\n</html>");
  $smarty->assign ('datesBooked' , $datesBooked);
  $smarty->display($pageName.'.tpl');
?>