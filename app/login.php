<?php
  include_once ('../lib/ChatMessage.php');


  // Récupère les valeur envoyée
  $pageName = "login";
  if (isset($_GET['admin']) && $_GET['admin']=='true') $adminOnly = 'true'; else $adminOnly = '';
  if (isset($_POST['login'])) $username = $_POST['login']; else $username = '';
  if (isset($_POST['language']) && $_POST['language']<>'') $languageChosen = $_POST['language']; else $languageChosen = '';
  if (isset($_POST['psw'])) $psw = $_POST['psw']; else $psw = '';
  if (isset($_POST['connectToChat'])) $isChat = 1; else $isChat = 0;
  if (isset($_POST['init_'.$pageName])) $firstTime = $_POST['init_'.$pageName]; else $firstTime = '';


  // Vérifie si l'accès doit être vérifié uniquement pour l'administrateur (Accès interne)
  if ($adminOnly=='true') {
    include_once ('../lib/init.php');
    $showMenu = 1;
    $showJSError = true;
  } else {
    include_once ('../lib/initNoSession.php');
    $showMenu = -1;
    $showJSError = false;
  }


  // Récupère la variable de l'utilisateur stockée en Cookie
  if ($username == '' && isset ($_COOKIE[COOKIES_NAME]) && isset ($_COOKIE[COOKIES_NAME]['username']) && $_COOKIE[COOKIES_NAME]['username'] != "") $username = $_COOKIE[COOKIES_NAME]['username'];
  if ($languageChosen == '' && isset ($_COOKIE[COOKIES_NAME]) && isset ($_COOKIE[COOKIES_NAME]['language']) && $_COOKIE[COOKIES_NAME]['language'] != "") $languageChosen = $_COOKIE[COOKIES_NAME]['language'];
  if ($isChat == 0 && isset ($_COOKIE[COOKIES_NAME]) && isset ($_COOKIE[COOKIES_NAME]['chat']) && $_COOKIE[COOKIES_NAME]['chat'] != "") $isChat = $_COOKIE[COOKIES_NAME]['chat'];


  // Initialisation des données générale
  $duree = 3600 * 24 * 30;
  $dbQuery->connect();
  $accessGranted = false;
  $adminCode = "";
  $adminLogin = false;
  $hidePassword = false;


  // Vérifie si le site est sécurisé
  if ($dbQuery->getGuestPsw($albumName)=='' && $adminOnly<>'true' && !$dbQuery->forbiddenLogin ($albumName, $username)) {
    if ($username != "") {
      $accessGranted = true;
      $firstTime = 1;
    }
    elseif (!$menuOption->chat) {
      $username=genID();
      $accessGranted = true;
      $firstTime = 1;
    }
    $hidePassword = true;
  }


  // Vérifie les données renvoyées par le formulaire et que le nom de l'album est envoyé
  if ($firstTime) {

    // Vérifie qu'un mot de passe et login ont été entré
    if (($username <> '' && ($psw <> '' || $accessGranted)) || $albumName == 'test') {
      // Vérifie si un mot de passe est nécessité pour l'entrée
      if (!$accessGranted) {
        // Vérifie que le mot de passe entré est valide
        if ($psw==$dbQuery->getGuestPsw ($albumName) && $adminOnly<>'true') {
          $accessGranted = true;
        } elseif ($dbQuery->checkAdminPsw ($username, $psw, $albumName) || $dbQuery->checkSuperAdminPsw ($username, $psw)) {
          $adminCode = $dbQuery->initAdminAccess ();
          $adminLogin = true;
          $accessGranted = true;
        }
      }

      if ($albumName == 'test') {
        $adminCode = $dbQuery->initAdminAccess ();
        $adminLogin = true;
        $accessGranted = true;
	}

      $dbQuery->disconnect();

      if ($accessGranted) {

          // Création et initialisation de la session
          if (!isset ($_SESSION)) initSession (SESSION_NAME);
          unset ($_SESSION['mails']);
          unset ($_SESSION['countriesList']);
          unset ($_SESSION['menuCount']);
          unset ($_SESSION['datesBooked']);
          unset ($_SESSION['dateFormat']);
          unset ($_SESSION['login']);
          unset ($_SESSION['adminCode']);
          unset ($_SESSION['albumName']);

          // Initialisation du chat
          $chatusers = new ChatMessage('../tmp/'.$albumName.'/chat/users');
          $chatmessages = new ChatMessage('../tmp/'.$albumName.'/chat/messages');
          $chatcalls = new ChatMessage('../tmp/'.$albumName.'/chat/calls');
          $chatusers->cleanUsers($chatmessages, $msgHandler->get('isDisconnected'));

          // Vérifie qu'une personne avec le même nom d'utilisateur n'est pas déjà connecté
          if ((!$chatusers->isUser($username) && $menuOption->chat) || !$menuOption->chat) {

            // Met en session et cookie l'utilisateur, initialise l'album et le compteur
            setcookie(COOKIES_NAME.'[username]', stripslashes($username), (time() + $duree));
            if ($languageChosen<>'' && $languageChosen<>0) {
              setcookie(COOKIES_NAME.'[language]', $languageChosen, (time() + $duree));
              $_SESSION['languageChosen'] = $languageChosen;
            } else {
              setcookie(COOKIES_NAME.'[language]', '', (time() - 1000));
            }
            $_SESSION['login'] = $username;
            $_SESSION['adminCode'] = $adminCode;
            $_SESSION['albumName'] = $albumName;
            $_SESSION['counter'] = hitcount("../tmp/".$albumName."/counter.txt", !$adminLogin);

            // Connexion au chat
            if ($menuOption->chat) {
              $chatmessages->updateDate("../tmp/".$albumName."/chat/date.dat");
              if ($username<>'superadmin' && $isChat == 1) {
                $chatusers->editUser($username);
                $chatmessages->insertMessage($username.$msgHandler->get('isConnected'),' ', MAXLINESCHAT);
                $users = $chatusers->getUsers ();
                if (count ($users) > 0)
                foreach ($users as $user) {
                  if ($user['name']<>$username) $chatcalls->editCall ($username, $user['name'], 1);
                }
                setcookie(COOKIES_NAME.'[chat]', 1, (time() + $duree));
              } else {
                setcookie(COOKIES_NAME.'[chat]', '', (time() - 1000));
              }
            }
            //$mailReceiver = new mailReceiver (null, EMAIL_HOST, EMAIL_USER, EMAIL_PSW, 0, '');
            //if ($mailReceiver->checkMails()) {
              //$smarty->assign ('waitingMsg', 1);
            //} else {
              // Renvoie à la page d'accueil
              header("Location:".PAGE_ACCUEIL);
              exit();
            //}
          } else {
            $accessGranted = false;
            $error = $msgHandler->getError ('userAlreadyConnected');
          }
        } else {
          $error = $msgHandler->getError ('passwordCheck');
        }
      } else {
        $error = $msgHandler->getError ('fieldError');
        $dbQuery->disconnect();
      }
  } else {
    $dbQuery->disconnect();
  }


  // Assignation de Smarty
  $smarty->assign ('hidePassword', $hidePassword);
  $smarty->assign ('languageChosen', $languageChosen);
  $smarty->assign ('tableLanguage', $tableLanguage);
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, '', $showJSError, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, '', false, $showMenu));
  $smarty->assign ('displayChat', $menuOption->chat);
  $smarty->assign ('album', $albumName);
  $smarty->assign ('admin', $adminOnly);
  $smarty->assign ('error', $error);
  $smarty->assign ('confirm', $confirm);
  $smarty->assign ('username', $username);
  $smarty->assign ('pageName', $pageName);
  $smarty->display($pageName.'.tpl');
?>

