<?php
  include_once('../lib/init.php');
  include_once('../lib/ChatMessage.php');


  // Déclaration des variables
  $pageName = "chatclient";
  $chatusers = new ChatMessage(USERS_PATH.$albumName.'/chat/users');


  // Assignation de Smarty
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuChat'), $admin, 4));
  $smarty->assign ('user', $_SESSION['login']);
  $smarty->assign ('isConnected', $chatusers->isUser ($_SESSION['login']));
  $smarty->display($pageName.'.tpl');
  
?>
