<?php
  include_once('../lib/init.php');


  // Si l'administrateur de l'album ne s'est pas authentifié, renvoie l'utilisateur à la page d'accueil
  if (!$admin) {header ("Location:".PAGE_ACCUEIL);exit();}


  // Si le bouton "Annuler" a été pressé, renvoie à la page des contacts
  if (isset ($_POST['cancel']) && $_POST['cancel'] == 'true') {header("Location:".PAGE_CONTACTS);exit();}


  // Initialisation des données
  $dayList = array ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31');
  $contact = null;
  $action = 'view';
  if (isset ($_POST['actions'])) $action = $_POST['actions'];
  elseif (isset ($_GET['actions'])) $action = $_GET['actions'];
  if (isset ($_POST['id'])) {
    $contact = new Contact ($_POST['id'], $_POST['title'], $_POST['firstname'],
                            $_POST['lastname'], $_POST['nickname'], $_POST['email'],
                            ($_POST['birthday']+1).'-'.$_POST['birthmonth'].'-'.$_POST['birthyear'],
                            $_POST['address'], $_POST['zipcode'], $_POST['city'], $_POST['country'],
                            $_POST['tel'], $_POST['fax'], $_POST['mobile'], $_POST['messenger'], $_POST['comments'], time());
  }
  elseif (isset ($_GET['id'])) {
    if ($_GET['id']==-1) {
      do {
        $id = genID();
      } while ($contactsManager->getContact($id));
      $contact = new Contact ($id, '', '', '', '', '', '01-01-1900', '', '', '', '', '', '', '', '', '', time());
      if (isset ($_GET['email'])) $contact->email = $_GET['email'];
    } else {
      $contact = $contactsManager->getContact($_GET['id']);
    }
  }
  if (!$contact) {header ("Location:".PAGE_CONTACTS);exit();}
  $birthdate = explode ('-', $contact->birthdate);
  $birthday = 0;
  $birthmonth = '01';
  $birthyear = '1900';
  if (count ($birthdate)==3) {
    $birthday = $birthdate[0]-1;
    $birthmonth = $birthdate[1];
    $birthyear = $birthdate[2];
  }


  // Si le bouton "Save" a été pressé, sauvegarde le contact et renvoie à la liste des contacts
  if (isset ($_POST['save']) && $_POST['save']=='true') {
    if (($_POST['birthday'] > 29 && ($_POST['birthmonth']=='04' || $_POST['birthmonth']=='06' || $_POST['birthmonth']=='09' || $_POST['birthmonth']=='11')) ||
        ($_POST['birthday'] > 28 && $_POST['birthmonth']=='02') ||
        ($_POST['birthday'] > 27 && $_POST['birthmonth']=='02' && ($_POST['birthyear']%4<>0 || $_POST['birthyear']%100==0))) $error = $msgHandler->getError ('date');
    if (!is_email ($_POST['email']) && $_POST['email']<>'') $error = $msgHandler->getError ('invalidEmail');
    if ($_POST['firstname']=='' && $_POST['lastname']=='' && $_POST['nickname']=='') $error = $msgHandler->getError ('insertName');
    if (!$error) {
      $contactsManager->editContact ($contact);
      header("Location:".PAGE_CONTACTS."?confirm=*contactUpdated");
      exit();
    }
  }


  // Assignation de Smarty
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuContactDetails'), $admin, 4));
  $smarty->assign ('contact', $contact);
  $smarty->assign ('birthday', $birthday);
  $smarty->assign ('birthmonth', $birthmonth);
  $smarty->assign ('birthyear', $birthyear);
  $smarty->assign ('action', $action);
  $smarty->assign ('dayList', $dayList);
  $smarty->assign ('monthList', $msgHandler->getTable ('month'));
  if ($action=='edit') {
    $smarty->display('editcontact.tpl');
  } else {
    $smarty->display('viewcontact.tpl');
  }

?>

