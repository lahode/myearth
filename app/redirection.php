<?php
  include_once ("../lib/declare.php") ;
  include_once ('../lib/GlobalClasses.php');
  include_once ("../lib/functions.php") ;
  include_once ("../lib/Language.php") ;
  include_once ('../lib/AlbumManager.php');
  include_once ('../lib/ChatMessage.php');


  // Création de la session
  initSession (SESSION_NAME);

  function destroySession() {
    // Récupère les messages dans la langue configurée
    if (isset ($_SESSION['albumName'])) {
      $albumName = $_SESSION['albumName'];
      $albumFile = USERS_PATH.$albumName.'/xml/album';
      $albumManager = new AlbumManager ($albumFile, $_SESSION['albumName']);
      $property=$albumManager->getProperties ();
      $language = $property->language;
      $msgHandler = new Language ($language);

      // Suppression du chat et de la session
      $chatUsersFile = USERS_PATH.$albumName.'/chat/users';
      $chatMessagesFile = USERS_PATH.$albumName.'/chat/messages';
      $chatusers = new ChatMessage($chatUsersFile);
      $chatmessages = new ChatMessage($chatMessagesFile);
      $username = $_SESSION['login'];
      if ($chatusers->isUser($username)) $chatusers->deleteUser($username, $chatmessages, $msgHandler->get('isDisconnected'));
      unset ($_SESSION);
      @session_destroy();
    }
    header ("Location:http://".WEB_PAGE);
    exit();
  }


  $displayMsg = '';
  if (isset ($_GET)) {
    if (isset ($_GET['redirection']) && ($_GET['redirection']=='login') && isset ($_GET['logout']) && ($_GET['logout']='true')) destroySession();
    $i=0;
    foreach ($_GET as $keyGet => $getItem) {
      if ($keyGet<>'redirection') {
        if ($i == 0) $displayMsg = '?'.$keyGet.'='.$getItem;
        else $displayMsg .= '&'.$keyGet.'='.$getItem;
        $i++;
      }
    }
  }
  header ("Location:".$_GET['redirection'].".php".$displayMsg);
?>
