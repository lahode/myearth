<?php
  include_once('../lib/init.php');


  // Nettoie le dossier des fichiers temporaires
  $dbQuery->connect();
  $dbQuery->cleanTempFiles ($tempPath);
  $dbQuery->disconnect();


  // Récupère toutes les information de l'album
  $home = $albumManager->getHome ();
  $pageName = "home";


  // Récupère le sujet de la dernière news envoyée
  $newsSent = $newsManager->getMessages(-1, -1, true, 'date');
  if ($newsSent) {
    $lastNewsDate = '<div class="sitetext">'.$msgHandler->get('senton').': '.showdate($newsSent[0]->date).'</div>';
    $lastNewsTitle = addslashes ($newsSent[0]->subject);
  } else {
    $lastNewsDate = "";
    $lastNewsTitle = "";
  }


  // Remplissage des informations des menus de raccourci
  $elements = null;
  $incElement = 0;
  if ($property->showShortCut)  {

    // Récupération de la dernière visite
    if (count ($categories) > 0) {
      $dbQuery->connect();
      foreach ($categories as $keyCat => $itemCat) {
        $lastDestination = $routeManager->getFirstLastDestination($itemCat->id, true);
        if ($lastDestination) {
          $elements[$incElement] = null;
          if ($lastDestination->cityName=='') {
            $elements[$incElement]['subject'] = $dbQuery->getCityName($itemCat->country, $lastDestination->city);
          } else {
            $elements[$incElement]['subject'] = $lastDestination->cityName;
          }
          if ($lastDestination->images) {
            $elements[$incElement]['picture'] = $webImgPath.$lastDestination->images->name;
            $elements[$incElement]['text'] = cutWords(shorten ($lastDestination->text, 50), 15);
          } else {
            $elements[$incElement]['text'] = cutWords(shorten ($lastDestination->text, 150), 15);
          }
          $elements[$incElement]['link'] = PAGE_SHOW.'?dest='.$lastDestination->id;
          $elements[$incElement]['title'] = $msgHandler->get('menuLastVisited');
        }
      }
      if ($elements[$incElement]) $incElement++;
      $dbQuery->disconnect();
    }

    // Récupération de la dernière news
    if ($newsManager->count() > 0 && $menuOption->news) {
      $elements[$incElement]['title'] = $msgHandler->get('menuNews');
      $messages = $newsManager->getMessages (0, 1, true, 'date');
      $elements[$incElement]['subject'] = $messages[0]->subject;
      if ($messages[0]->images[0]) {
        $elements[$incElement]['picture'] = $newsImgPath.$messages[0]->images[0]->name;
        $elements[$incElement]['text'] = cutWords(shorten (Emoticon::setEmotes (PICS_PATH.'/emoticon/', $messages[0]->text), 50), 15);
      } else {
        $elements[$incElement]['text'] = cutWords(shorten (Emoticon::setEmotes (PICS_PATH.'/emoticon/', $messages[0]->text), 150), 15);
      }
      $elements[$incElement]['link'] = PAGE_NEWS;
      if ($elements[$incElement]) $incElement++;
    }

    // Récupération du dernier message du livre d'or
    if ($forumManager->count() > 0 && $menuOption->forum) {
      $elements[$incElement]['title'] = $msgHandler->get('menuForum');
      $messages = $forumManager->getMessages (0, 1, true, 'date');
      $elements[$incElement]['subject'] = $messages[0]->subject;
      if ($messages[0]->images[0]) {
        $elements[$incElement]['picture'] = $forumImgPath.$messages[0]->images[0]->name;
        $elements[$incElement]['text'] = cutWords(shorten ($messages[0]->text, 50), 15);//cutWords(shorten (Emoticon::setEmotes (PICS_PATH.'emoticon/', $messages[$j-1]->text), 50), 15);
      } else {
        $elements[$incElement]['text'] = cutWords(shorten ($messages[0]->text, 150), 15);//cutWords(shorten (Emoticon::setEmotes (PICS_PATH.'emoticon/', $messages[$j-1]->text), 150), 15);
      }
      $elements[$incElement]['link'] = PAGE_FORUM;
      if ($elements[$incElement]) $incElement++;
    }

    // Récupération du dernier message du carnet
    if ($notebookManager->count() > 0 && $menuOption->notebook) {
      $j = 0;
      $messages = null;
      do {
        $tempMessage = $notebookManager->getMessages ($j, 1, true, 'date');
        if ($tempMessage[$j]->access == 'public' || $admin) $messages = $tempMessage;
        $j++;
      } while ($tempMessage[$j-1]->access <> 'public' &&  $j < $notebookManager->count());
      if ($messages && $j > 0) {
        $elements[$incElement]['title'] = $msgHandler->get('menuNotebook');
        $elements[$incElement]['subject'] = $messages[$j-1]->subject;
        if ($messages[$j-1]->images[0]) {
          $elements[$incElement]['picture'] = $notebookImgPath.$messages[$j-1]->images[0]->name;
          $elements[$incElement]['text'] = cutWords(shorten ($messages[$j-1]->text, 50), 15);//cutWords(shorten (Emoticon::setEmotes (PICS_PATH.'emoticon/', $messages[$j-1]->text), 50), 15);
        } else {
          $elements[$incElement]['text'] = cutWords(shorten ($messages[$j-1]->text, 150), 15);//cutWords(shorten (Emoticon::setEmotes (PICS_PATH.'emoticon/', $messages[$j-1]->text), 150), 15);
        }
        $elements[$incElement]['link'] = PAGE_NOTEBOOK;
        if ($elements[$incElement]) $incElement++;
      }
    }

    // Récupération du dernier message du bétisier
    if ($humourManager->count() > 0 && $menuOption->humour) {
      $j = 0;
      $messages = null;
      do {
        $tempMessage = $humourManager->getMessages ($j, 1, true, 'date');
        if ($tempMessage[$j]->access == 'public' || $admin) $messages = $tempMessage;
        $j++;
      } while ($tempMessage[$j-1]->access <> 'public' &&  $j < $humourManager->count());
      if ($messages && $j > 0) {
        $elements[$incElement]['title'] = $msgHandler->get('menuHumour');
        $elements[$incElement]['subject'] = $messages[$j-1]->subject;
        if ($messages[$j-1]->images[0]) {
          $elements[$incElement]['picture'] = $humourImgPath.$messages[$j-1]->images[0]->name;
          $elements[$incElement]['text'] = cutWords(shorten (Emoticon::setEmotes (PICS_PATH.'/emoticon/', $messages[$j-1]->text), 50), 15);
        } else {
          $elements[$incElement]['text'] = cutWords(shorten (Emoticon::setEmotes (PICS_PATH.'/emoticon/', $messages[$j-1]->text), 150), 15);
        }
        $elements[$incElement]['link'] = PAGE_HUMOUR;
        if ($elements[$incElement]) $incElement++;
      }
    }

    // Récupération du dernier message des vidéos
    if ($videoManager->count() > 0 && $menuOption->video) {
      $j = 0;
      $messages = null;
      do {
        $tempMessage = $videoManager->getMessages (0, 1, true, 'date');
        if ($tempMessage[$j]->access == 'public' || $admin) $messages = $tempMessage;
        $j++;
      } while ($tempMessage[$j-1]->access <> 'public' &&  $j < $videoManager->count());
      if ($messages && $j > 0) {
        $elements[$incElement]['title'] = $msgHandler->get('menuVideo');
        $elements[$incElement]['subject'] = $messages[0]->subject;
        $elements[$incElement]['text'] = shorten (Emoticon::setEmotes (PICS_PATH.'/emoticon/', $messages[0]->text), 150);
        $elements[$incElement]['link'] = PAGE_VIDEO;
        if ($elements[$incElement]) $incElement++;
      }
    }
  }
  

  // Supprime les éventuelles alertes
  if (isset($_POST['deleteCurrentTask']) && $_POST['deleteCurrentTask']=='true') {
    $taskManager->deleteCurrentTasks();
    header ("Location:".PAGE_REDIRECTION."?redirection=tasks");
  }


  // Assignation de Smarty
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->assign ('userLanguage', $language);

  // Si l'option de gestion des message est activée, affiche le dernier e-mail envoyé dans la barre déroulante
  if ($lastNewsTitle<>'') {
    $smarty->assign ('scrollMessage', $lastNewsTitle);
    $scrollMessage = $smarty->fetch ('scrollMessage.tpl');
    $scrollActive = true;
  } else {
    $scrollMessage = "";
    $scrollActive = false;
  }

  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $lastNewsDate, $admin, 1, $scrollMessage));
  $smarty->assign ('elements', $elements);
  $smarty->assign ('scrollActive', $scrollActive);
  $smarty->assign ('home', $home);
  $smarty->assign ('speed', 50);
  $smarty->assign ('webImgPath', $webImgPath);
  $smarty->assign ('counter', $_SESSION['counter']);
  $smarty->assign ('alertToDisplay', $alertToDisplay);
  $smarty->display($pageName.'.tpl');

?>

