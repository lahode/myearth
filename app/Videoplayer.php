<?php
  class Videoplayer {

    var $smarty;
    var $videoID;
    var $width;
    var $height;
    var $autostart;


    function __construct ($smarty, $videoID, $width=100, $height=100, $autostart = "true") {
      $this->smarty = $smarty;
      $this->videoID = $videoID;
      $this->width = $width;
      $this->height = $height;
      $this->autostart = $autostart;
    }


    function show ($file) {
      $player = "";
      $ext = strtoupper(substr ($file, -3));
      switch ($ext) {
        case "AVI" : {$player="wmp";break;}
        case "MPG" : {$player="wmp";break;}
        case "MPE" : {$player="wmp";break;}
        case "WMV" : {$player="wmp";break;}
        case "MOV" : {$player="quicktime";break;}
        case "RM" : {$player="real";break;}
      }
      $this->smarty->assign ('file', $file);
      $this->smarty->assign ('player', $player);
      $this->smarty->assign ('width', $this->width);
      $this->smarty->assign ('height', $this->height);
      $this->smarty->assign ('autostart', $this->videoID);
      $this->smarty->assign ('autostart', $this->autostart);
      return $this->smarty->fetch ('videoplayer.tpl');
    }
  }

?>
