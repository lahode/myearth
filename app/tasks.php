<?php
  include_once('../lib/init.php');


  // Si l'administrateur de l'album ne s'est pas authentifié, renvoie l'utilisateur à la page d'accueil
  if (!$admin) {header("Location:".PAGE_ACCUEIL);exit();}
  $pageName = "tasks";
  $action = "insert";
  $fontStyle1 = $albumManager->getFontstyle ('sitetitle');
  $fontStyle2 = $albumManager->getFontstyle ('navigation');


  // Récupération des données
  if (!isset($_POST['init_'.$pageName]) || (isset ($_POST['cancel']) && $_POST['cancel']=='true')) {
    $task = null;
  } else {
    if (isset ($_POST['block'])) $block=1; else $block=0;
    if ($_POST['start']<>'') {$dateTemp = unfdate($_POST['start']); $start = mktime ($_POST['hourStart'], $_POST['minuteStart'], 0, date ('m', $dateTemp), date ('d', $dateTemp), date ('Y', $dateTemp));} else $start = "";
    if ($_POST['end']<>'') {$dateTemp = unfdate($_POST['end']); $end = mktime ($_POST['hourEnd'], $_POST['minuteEnd'], 0, date ('m', $dateTemp), date ('d', $dateTemp), date ('Y', $dateTemp));} else $end = "";
    $task = new Task ($_POST['id'], $_POST['type'], $_POST['subject'], $_POST['description'], $start, $end, $block, $_POST['reminder']);
  }

/*****************************************************************************/
  // Initialisation du calendrier
  $currentDay = date("j", time());
  $currentMonth = date("m", time());
  $currentYear = date("Y", time());

  // Récupère les dates renvoyées
  if (isset($_GET["month"])) $month=$_GET["month"]; else $month = $currentMonth;
  if(isset($_GET["year"])) $year=$_GET["year"]; else $year = $currentYear;
  if(isset($_GET["day"])) $day=$_GET["day"]; else $day = $currentDay;

  //defini le mois suivant
  $nextMonth = $month + 1;
  $nextYear = $year;
  if ($nextMonth == 13) {
    $nextMonth = 1;
    $nextYear = $year + 1;
  }

  //defini le mois précédent
  $lastMonth = $month - 1;
  $lastYear = $year;
  if ($lastMonth == 0) {
    $lastMonth = 12;
    $lastYear = $year - 1;
  }

  // creation d'un tableau sans entrée à 31 entrée (1 pour chaues jours)
  for($j = 1; $j < 32; $j++) {
    $dayTab[$j] = 0;
  }

  //Détection du 1er et dernier jour du mois
  $dateNumber = mktime(0,0,0, $month, 1, $year);
  $firstDay = date('w', $dateNumber);
  $lastDay = 28;
  while (checkdate($month, $lastDay + 1, $year)) {$lastDay++;}
  if ($day > $lastDay) $day = $lastDay;
  $fullDate = mktime (0, 0, 0, $month, $day, $year);


/*****************************************************************************/

  // Création d'une tâche pour chaque anniversaire des contacts
  if (isset ($_POST['synchroBirthday']) && $_POST['synchroBirthday']=='true') {
    $contacts = $contactsManager->getContacts();
    if (count ($contacts) > 0) {
      foreach ($contacts as $contact) {
        $tabBirthday = explode ('-', $contact->birthdate);
        if (count ($tabBirthday) == 3) {
          if (($tabBirthday[1]+0) > 1 || ($tabBirthday[0]+0) > 1 || ($tabBirthday[0]+0) > 1900) {
            $birthday = mktime (0, 0, 0, $tabBirthday[1]+0, $tabBirthday[0]+0, date('Y'));
            if ($birthday < time()) $birthday = mktime (0, 0, 0, $tabBirthday[1]+0, $tabBirthday[0]+0, date('Y')+1);
            if ($contact->nickname=="") $contact->nickname = $contact->firstname.' '.$contact->lastname;
            $taskManager->editTask (new Task ($contact->nickname.($tabBirthday[0]+0).($tabBirthday[1]+0), 'anniversary', $msgHandler->getTable('taskType', 'anniversary').' '.$contact->nickname, '', $birthday, $birthday+(3600*24)-1, 0, 3));
          }
        }
      }
    }
  }


  // Si le bouton "Edit" a été pressé, edite le lien
  if (isset ($_POST['editTask']) && $_POST['editTask']<>'') {
    $task = $taskManager->getTask($_POST['editTask']);
    $action = "edit";
  }


  // Si le bouton "Delete" a été pressé, supprime le lien
  if (isset ($_POST['deleteTask']) && $_POST['deleteTask']<>'') {
    $taskManager->deleteTask ($_POST['deleteTask']);
  }


  // Si le bouton "Save" a été pressé, sauvegarde le nouveau lien
  if (isset ($_POST['save']) && $_POST['save']=='true') {
    if ($task->start > $task->end) $error = $msgHandler->getError ('dateStartInvalid');
    if ($task->end == '') $error = $msgHandler->getError ('dateEnd');
    if ($task->start == '') $error = $msgHandler->getError ('dateStart');
    if ($task->subject == '') $error = $msgHandler->getError ('insertSubject');
    if (!$error) {
      if ($task->id=='') $task->id = $taskManager->getNewID();
      $taskManager->editTask ($task);
      $task = null;
      header ("Location:".PAGE_REDIRECTION."?redirection=tasks&month=".$month."&year=".$year."&day=".$day);
      exit();
    }
  }


  // Récupération de toutes les tâches
  $tasks = $taskManager->getTasks ($month, $year);
  $tasksToShow = null;
  $k=0;
  if ($tasks && count ($tasks) > 0) {
    foreach ($tasks as $itemtask) {
      for ($j=date ('d', $itemtask->start);$j <= date ('d', $itemtask->end); $j++) {
        $dayTab[$j+0]++;
        if ($j+0 == $day) {$tasksToShow[$k]=$itemtask;$k++;}
      }
    }
  }


  // Initialisation des tableaux
  for ($i=0;$i<24;$i++) {if ($i<10) $tabHour[$i]='0'.$i; else $tabHour[$i]=$i;}
  for ($i=0;$i<60;$i++) {if ($i<10) $tabMinute[$i]='0'.$i; else $tabMinute[$i]=$i;}
  for ($i=-1;$i<21;$i++) {if ($i==-1) $tabReminder[$i]=$msgHandler->get('noReminder');
                          elseif ($i==0) $tabReminder[$i]=$msgHandler->get('sameDay');
                          elseif ($i==1) $tabReminder[$i]=$i.' '.$msgHandler->get('dayBefore');
                          else $tabReminder[$i]=$i.' '.$msgHandler->get('daysBefore');}


  // Assignation de Smarty
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuLink'), $admin, 4));
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->assign ('pageName', $pageName);
  $smarty->assign ('fullDate', $fullDate);
  $smarty->assign ('lastMonth', $lastMonth);
  $smarty->assign ('lastYear', $lastYear);
  $smarty->assign ('nextMonth', $nextMonth);
  $smarty->assign ('nextYear', $nextYear);
  $smarty->assign ('firstDay', $firstDay);
  $smarty->assign ('lastDay', $lastDay);
  $smarty->assign ('dayTab', $dayTab);
  $smarty->assign ('year', $year);
  $smarty->assign ('month', $month);
  $smarty->assign ('day', $day);
  $smarty->assign ('property', $property);
  $smarty->assign ('borderColor', $fontStyle1->textColor);
  $smarty->assign ('occupedColor', $fontStyle2->textColor);
  $smarty->assign ('font', $property);
  $smarty->assign ('tabHour', $tabHour);
  $smarty->assign ('tabMinute', $tabMinute);
  $smarty->assign ('tabReminder', $tabReminder);
  $smarty->assign ('tabType', $msgHandler->getTable('taskType'));
  $smarty->assign ('task', $task);
  $smarty->assign ('tasksToShow', $tasksToShow);
  $smarty->assign ('action', $action);
  $smarty->display($pageName.'.tpl');

?>

