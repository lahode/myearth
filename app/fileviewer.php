<?php
  include_once('../lib/init.php');


  // Function supprimant les images non classees
  function getWebImages () { 
    global $webImgPath;
    global $routeManager;
    global $notebookManager;
    global $humourManager;
    global $dbQuery;
    global $msgHandler;
    $images = null;
    $i=0;
    $j=0;
    if ($handle = opendir($webImgPath)) {
      $file = '';
      while (false !== ($file = readdir($handle))) {
        if (strtoupper(substr ($file, -3)) == 'JPG' || strtoupper(substr ($file, -3)) == 'GIF' || strtoupper(substr ($file, -3)) == 'PNG') {
          $images[$j] = $file;
          $j++;
        }
      }
      closedir($handle);

      // Classement des images par pays
      $imageByCountry = $routeManager->getImageByCountry();
      if (count ($imageByCountry) > 0) {
        $dbQuery->connect();
        foreach ($imageByCountry as $keyImgByCountry => $imgByCountry) {
          $i=0;
          $country = $dbQuery->getCountryName($keyImgByCountry);
          $tabImgCategory[$keyImgByCountry] = $country;
          foreach ($images as $keyImg => $img) {
            $j=0;
            do {
              if (isset ($imgByCountry[$j]) && $img == $imgByCountry[$j]->name) {
                $resultImages[$keyImgByCountry][$i] = new ImageObj ($imgByCountry[$j]->name, $imgByCountry[$j]->description, $webImgPath);
                unset ($images[$keyImg]);
              }
              $j++;
            } while (isset ($imgByCountry[$j]) && $imgByCountry[$j-1]->name <> $img);
            $i++;
          }
          $nbImage[$keyImgByCountry] = count ($resultImages[$keyImgByCountry]);
        }
        $dbQuery->disconnect();
      }
      $imgsNotebook = array_flip($notebookManager->getImages());
      $imgsHumour = array_flip($humourManager->getImages());
      foreach ($images as $keyImg => $img) {
        if ($imgsNotebook[$img]) $resultImages['notebook'][$keyImg] = new ImageObj ($img, '', $webImgPath);
        elseif ($imgsHumour[$img]) $resultImages['humour'][$keyImg] = new ImageObj ($img, '', $webImgPath);
        else $resultImages['unclassified'][$keyImg] = new ImageObj ($img, '', $webImgPath);
      }
      $tabImgCategory['notebook'] = $msgHandler->get ('menuNotebook');
      $tabImgCategory['humour'] = $msgHandler->get ('menuHumour');
      $tabImgCategory['unclassified'] = $msgHandler->get ('unclassified');
      $nbImage['notebook'] = count ($resultImages['notebook']);
      $nbImage['humour'] = count ($resultImages['humour']);
      $nbImage['unclassified'] = count ($resultImages['unclassified']);
      return array ('tabImgCategory' => $tabImgCategory, 'resultImages' => $resultImages, 'nbImage' => $nbImage);
    }
  }


  // Vérifie que l'action et l'identifiant ont été envoyés
  if (isset ($_GET['actions']) && (($_GET['actions']=='selectPicture' && isset ($_GET['id'])) || ($_GET['actions']=='selectSound' && isset ($_GET['id'])) || ($_GET['actions']=='selectVideo' && isset ($_GET['id'])) || ($_GET['actions']=='showDestination' && isset ($_GET['id'])) || $_GET['actions']=='download')) {
    // Initialisation des données
    $pageName = "fileviewer";
    $action = $_GET['actions'];
    if (!isset ($_SESSION['currentDir']) || !isset ($_POST['init_'.$pageName])) $_SESSION['currentDir']='';
    if (!isset ($_SESSION['actionfiles']) || !isset ($_POST['init_'.$pageName])) $_SESSION['actionfiles'] = array();
    if (isset ($_GET['selected'])) $selected = $_GET['selected']; else $selected='';
    if (isset ($_GET['show'])) $show = $_GET['show']; else $show='';
    if (isset ($_GET['clean'])) $clean = $_GET['clean']; else $clean='';
    if (isset ($_GET['title'])) $title = $_GET['title']; else $title='';
    if (isset ($_GET['id'])) $id = $_GET['id']; else $id='';
    if (isset ($_GET['viewSingle'])) $viewSinglePicture = $_GET['viewSingle']; else $viewSinglePicture='';
    if (isset ($_GET['type'])) $type = $_GET['type']; else $type = 'web';
    $action_name = '';
    $origImage = '';
    $singlePicture = null;
    $tabImgCategory = array();
    $sounds = array();
    $videos = array();
    $files = array();
    $dir = array();
    $images = array();
    $nbImage = array();
    $resultImages = array();
    $directories = array();
    $nbSound = 0;
    $nbVideo = 0;
    $nbFile = 0;
    $createDir = '';
    $renameDir = '';
    $showReturn = false;


    // Supprime tous les fichiers d
    if (isset ($_GET['del']) && $_GET['del']=='all') {
      $webImgToDelete = getWebImages ();
      if ($webImgToDelete && $webImgToDelete['resultImages']['unclassified'])
        foreach ($webImgToDelete['resultImages']['unclassified'] as $imgToDelete) {
          deleteFile ($imgToDelete->temp.$imgToDelete->name);
        }
    }

    // Change de dossier
    if (isset ($_POST['selectdir']) && $_POST['selectdir']<>'') {
      if ($_POST['selectdir'] == '..') {
        $tabDir = explode ('/', $_SESSION['currentDir']);
        $_SESSION['currentDir']='';
        if (count ($tabDir) > 1 && $tabDir[1]<>'') {
          foreach ($tabDir as $kDir => $elDir) {
            if ($kDir < sizeof($tabDir)-2) {
              $_SESSION['currentDir'].=$elDir.'/';
            }
          }
        }
      } else {
        $_SESSION['currentDir'].=$_POST['selectdir'].'/';
      }
    }


    // Nomme ou renomme le dossier choisi
    if (isset ($_POST['newdirname']) && $_POST['newdirname']<>'') {
      if (!is_dir ($otherFilesPath.$_SESSION['currentDir'].$_POST['newdirname'].'/')) {
        if (isset ($_POST['olddirname']) && $_POST['olddirname']<>'') {
          rename($otherFilesPath.$_SESSION['currentDir'].$_POST['olddirname'], $otherFilesPath.$_SESSION['currentDir'].$_POST['newdirname']);
        } else {
          mkdir ($otherFilesPath.$_SESSION['currentDir'].$_POST['newdirname']);
        }
      }
    }


    // Choisir le dossier à renommer
    if (isset ($_POST['renamedir']) && $_POST['renamedir']<>'') {
      $renameDir = $_POST['renamedir'];
    }


    // Choisir le dossier à créer
    if (isset ($_POST['createdir']) && $_POST['createdir']=='true') {
      $createDir = 'new';
    }


    // Supprime les fichiers ou dossiers sélectionnés
    if (isset ($_POST['deletefiles']) && $_POST['deletefiles']=='true') {
      if (isset ($_POST['directories']) && count ($_POST['directories']) > 0) {
        foreach ($_POST['directories'] as $keydirToSelect => $dirToSelect) {
          deleteDir ($otherFilesPath.$_SESSION['currentDir'].$dirToSelect);
        }
      }
      if (isset ($_POST['files']) && count ($_POST['files']) > 0) {
        foreach ($_POST['files'] as $keyfileToSelect => $fileToSelect) {
          deleteFile ($otherFilesPath.$_SESSION['currentDir'].$fileToSelect);
        }
      }
    }


    // Coupe les fichiers ou dossiers sélectionnés
    if (isset ($_POST['cutfiles']) && $_POST['cutfiles']=='true') {
      if (isset ($_POST['directories']) && count ($_POST['directories']) > 0) {
        foreach ($_POST['directories'] as $keydirToSelect => $dirToSelect) {
          $_SESSION['actionfiles']['files'][] = $dirToSelect;
        }
      }
      if (isset ($_POST['files']) && count ($_POST['files']) > 0) {
        foreach ($_POST['files'] as $keyfileToSelect => $fileToSelect) {
          $_SESSION['actionfiles']['files'][] = $fileToSelect;
        }
      }
      $_SESSION['actionfiles']['path'] = $_SESSION['currentDir'];
      $_SESSION['actionfiles']['action'] = 'cut';
    }


    // Copie les fichiers ou dossiers sélectionnés
    if (isset ($_POST['copyfiles']) && $_POST['copyfiles']=='true') {
      if (isset ($_POST['directories']) && count ($_POST['directories']) > 0) {
        foreach ($_POST['directories'] as $keydirToSelect => $dirToSelect) {
          $_SESSION['actionfiles']['files'][] = $dirToSelect;
        }
      }
      if (isset ($_POST['files']) && count ($_POST['files']) > 0) {
        foreach ($_POST['files'] as $keyfileToSelect => $fileToSelect) {
          $_SESSION['actionfiles']['files'][] = $fileToSelect;
        }
      }
      $_SESSION['actionfiles']['path'] = $_SESSION['currentDir'];
      $_SESSION['actionfiles']['action'] = 'copy';
    }


    // Copie les fichiers ou dossiers sélectionnés
    if (isset ($_POST['pastefiles']) && $_POST['pastefiles']=='true') {
      if (isset ($_SESSION['actionfiles']['path']) && $_SESSION['actionfiles']['path']<>$_SESSION['currentDir'] && isset ($_SESSION['actionfiles']['files'])) {
        foreach ($_SESSION['actionfiles']['files'] as $fileonaction) {
          switch ($_SESSION['actionfiles']['action']) {
            case 'cut' : {moveDir($otherFilesPath.$_SESSION['actionfiles']['path'].$fileonaction, $otherFilesPath.$_SESSION['currentDir'].$fileonaction);break;}
            case 'copy' : {copyDir($otherFilesPath.$_SESSION['actionfiles']['path'].$fileonaction, $otherFilesPath.$_SESSION['currentDir'].$fileonaction);break;}
          }
        }
      }
      $_SESSION['actionfiles'] = array();
    }
    

    // Détermine le nom de l'action
    if ($action == 'download' || $action == 'showDestination') {
      $action_name = $msgHandler->get('download');
    } elseif ($action == 'selectPicture' || $action == 'selectSound' || $action == 'selectVideo') {
      $action_name = $msgHandler->get('select');
    }


    // Récupère toutes les images d'une destination
    if ($action == 'showDestination') {
      $dest = $routeManager->getDestinationBy ('id', $id);
      if (isset ($dest->images) && count ($dest->images) > 0) {
        foreach ($dest->images as $keyImg => $img) {
          $resultImages['destination'][$keyImg] = $img;
          $resultImages['destination'][$keyImg]->temp = $webImgPath;
        }
        $nbImage['destination'] = count ($resultImages['destination']);
        if ($selected <> '') $showReturn = true;
        $selected = 'destination';
      }
    }

    // Récupère toutes les images de la librairie
    if ($action == 'download' || $action == 'selectPicture') {
      if ($type=='web' || $action == 'download') {
        $webImgResults = getWebImages();
        if ($webImgResults) {
          $tabImgCategory = $webImgResults['tabImgCategory'];
          $resultImages = $webImgResults['resultImages'];
          $nbImage = $webImgResults['nbImage'];
          $directories = count ($resultImages);
          if ($selected <> '') $showReturn = true;
        }
      }
      if ($type=='bg' || $action == 'download') {
        if ($handle = opendir($backgroundImgPath)) {
          $j=0;
          $file = '';
          while (false !== ($file = readdir($handle))) {
            if (strtoupper(substr ($file, -3)) == 'JPG' || strtoupper(substr ($file, -3)) == 'GIF' || strtoupper(substr ($file, -3)) == 'PNG') {
              $resultImages["background"][] = new ImageObj ($file, '', $backgroundImgPath);
              $j++;
            }
          }
        }
      }
      if ($type=='map' || $action == 'download') {
        if ($handle = opendir($mapImgPath)) {
          $j=0;
          $file = '';
          while (false !== ($file = readdir($handle))) {
            if (strtoupper(substr ($file, -3)) == 'JPG' || strtoupper(substr ($file, -3)) == 'GIF' || strtoupper(substr ($file, -3)) == 'PNG') {
              $resultImages["map"][] = new ImageObj ($file, '', $mapImgPath);
              $j++;
            }
          }
        }
      }
      if (isset ($resultImages["background"]) && count ($resultImages["background"]) > 0) {
        $nbImage["background"] = count ($resultImages["background"]);
        $tabImgCategory['background'] = $msgHandler->get ('background');
      }
      if (isset ($resultImages["map"]) && count ($resultImages["map"]) > 0) {
        $nbImage["map"] = count ($resultImages["map"]);
        $tabImgCategory['map'] = $msgHandler->get ('maps');
      }
    }


    // Récupère tous les sons de la librairie
    if ($action == 'download' || $action == 'selectSound') {
      if ($handle = opendir($soundPath)) {
        $i=0;
        $j=0;
        $file = '';
        while (false !== ($file = readdir($handle))) {
          if (strtoupper(substr ($file, -3)) == 'MP3') {
            $sounds[$j] = $file;
            $j++;
          }
        }
        closedir($handle);
        $nbSound = count ($sounds);
      }
    }


    // Récupère toutes les videos de la librairie
    if ($action == 'download' || $action == 'selectVideo') {
      if ($handle = opendir($videoPath)) {
        $i=0;
        $j=0;
        $file = '';
        while (false !== ($file = readdir($handle))) {
          if (strtoupper(substr ($file, -3)) == 'AVI' || strtoupper(substr ($file, -3)) == 'MPG'  || strtoupper(substr ($file, -3)) == 'MPE' || strtoupper(substr ($file, -3)) == 'MOV' || strtoupper(substr ($file, -3)) == 'WMV' || strtoupper(substr ($file, -3)) == 'RM') {
            $videos[$j] = $file;
            $j++;
          }
        }
        closedir($handle);
        $nbVideo = count ($videos);
      }
    }


    // Récupère toutes les fichiers qui ne sont pas des images ni des sons de la librairie
    if ($action == 'download') {
      if ($handle = opendir($otherFilesPath.$_SESSION['currentDir'])) {
        $i=0;
        $j=0;
        $file = '';
        if ($_SESSION['currentDir'] <> '') {$dir[0]='..';$i++;}
        while (false !== ($file = readdir($handle))) {
          if ($file != '..' && $file!='.') {
            if (is_dir ($otherFilesPath.$_SESSION['currentDir'].$file)) {
              $dir[$i] = $file;
              $i++;
            } else {
              $files[$j] = $file;
              $j++;
            }
          }
        }
        closedir($handle);
        $nbFile = count ($files);
      }
    }


    // Si une image a été sélectionnée, affiche l'image et sa description
    if ($selected<>'' && $viewSinglePicture && isset ($resultImages[$selected])) {
      if ($selected=='background') $tempPath = $backgroundImgPath; else $tempPath = $webImgPath;
      foreach ($resultImages[$selected] as $keyImg => $img) {
        if ($viewSinglePicture == $img->name) {
          $singlePicture = new ImageObj ($img->name, $img->description, $tempPath);
          if (file_exists($origImgPath.$img->name)) $origImage = $origImgPath.$img->name;
        }
      }
    }


    // Assignation de Smarty
    $smarty->assign ('images', $resultImages);
    $smarty->assign ('nbImage', $nbImage);
    $smarty->assign ('directories', $directories);
    $smarty->assign ('showReturn', $showReturn);
    $smarty->assign ('webImgPath', $webImgPath);
    $smarty->assign ('selected', $selected);
    $smarty->assign ('viewSinglePicture', $singlePicture);
    $smarty->assign ('downloadOrigImage', $origImage);
    $smarty->assign ('tabImgCategory', $tabImgCategory);
    $smarty->assign ('sounds', $sounds);
    $smarty->assign ('nbSound', $nbSound);
    $smarty->assign ('soundPath', $soundPath);

    $smarty->assign ('videos', $videos);
    $smarty->assign ('nbVideo', $nbVideo);
    $smarty->assign ('videoPath', $videoPath);

    $smarty->assign ('dir', $dir);
    $smarty->assign ('files', $files);
    $smarty->assign ('nbFile', $nbFile);
    $smarty->assign ('otherFilesPath', $otherFilesPath.$_SESSION['currentDir']);
    $smarty->assign ('currentDir', '/'.$_SESSION['currentDir']);
    $smarty->assign ('renameDir', $renameDir);
    $smarty->assign ('createDir', $createDir);
    $smarty->assign ('filestopaste', $_SESSION['actionfiles']);

    $smarty->assign ('id', $id);
    $smarty->assign ('type', $type);
    $smarty->assign ('action', $action);
    $smarty->assign ('action_name', $action_name);
    $smarty->assign ('maxPerRow', 3);
    $smarty->assign ('showImage', $show);
    $smarty->assign ('clean', $clean);
    $smarty->assign ('title', $title);
    $smarty->assign ('showTopHeader', $header->showTopHeader ());
    $smarty->assign ('picPath', PICS_PATH);
    $smarty->assign ('pageName', $pageName);
    $smarty->assign ('admin', $admin);
    $smarty->assign ('xajax_javascript', $xajax->getJavascript('../xajax/'));
    $smarty->display($pageName.'.tpl');
  }

?>

