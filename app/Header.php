<?php
  class Header {

    var $language;    // Langue
    var $smarty;      // Objet smarty
    var $headerPath;  // Dossier des styles CSS personnelles
    var $countries;   // Liste des pays du menu
    var $links;       // Liens pratiques
    var $soundPath;   // Dossier des sons personnelles
    var $menuOptions; // Options du menu
    var $menuCount;   // Nombre d'élément dans chaque option du menu
    var $albumName;   // Nom de l'album
    var $skin;        // Skin de l'album
    var $perso;       // Page perso de l'album


    function __construct ($lang, $smart, $path, $countries, $links, $soundPath, $menuOptions, $property, $menuCount, $albumName, $skin) {
      $this->language = $lang;
      $this->smarty = $smart;
      $this->headerPath = $path;
      $this->countries = $countries;
      $this->links = $links;
      $this->soundPath = $soundPath;
      $this->menuOptions = $menuOptions;
      $this->property = $property;
      $this->menuCount = $menuCount;
      $this->albumName = $albumName;
      $this->skin = $skin;
    }


    function showHeader ($title, $pagetitle, $admin, $showMenu=0, $othertitle="") {
      $this->smarty->assign ('title', $title);
      $this->smarty->assign ('pagetitle', $pagetitle);
      $this->smarty->assign ('othertitle', $othertitle);
      $this->smarty->assign ('userLanguage', $this->language);
      $this->smarty->assign ('imgLogoff', $this->skin->imgLogoff);
      $this->smarty->assign ('imgAdmin', $this->skin->imgAdmin);

      if ($showMenu >= 0 || $showMenu=='preview') {
        $lastDestination = array();
        if (count ($this->countries) > 1) {
          $lastDestination = array_pop($this->countries);
        }
        $this->smarty->assign ('property', $this->property);
        $this->smarty->assign ('menuOptions', $this->menuOptions);
        $this->smarty->assign ('countries', $this->countries);
        $this->smarty->assign ('lastDestination', $lastDestination);
        $this->smarty->assign ('soundPath', $this->soundPath);
        $this->smarty->assign ('admin', $admin);
        $this->smarty->assign ('showMenu', $showMenu);
        $this->smarty->assign ('links', $this->links);
        $this->smarty->assign ('count', $this->menuCount);
        $this->smarty->assign ('skinPath', $this->skin->path);
        $this->smarty->assign ('albumName', $this->albumName);
        if (file_exists(USERS_PATH.$this->albumName.'/perso.tpl')) $this->smarty->assign ('showPerso', 'true');
        if (file_exists('../rss/'.$this->albumName.'.xml')) $this->smarty->assign ('showRSS', '../rss/'.$this->albumName.'.xml');
        $this->smarty->assign ('menu', $this->smarty->fetch('menu.tpl'));
        $this->smarty->assign ('path', $this->headerPath);
      }

      return $this->smarty->fetch ('header.tpl');
    }


    function showTopHeader ($cssOutput='') {
      $this->smarty->assign ('skinPath', $this->skin->path);
      $this->smarty->assign ('cssOutput', $cssOutput);
      $this->smarty->assign ('path', $this->headerPath);
      $this->smarty->assign ('userLanguage', $this->language);

      return $this->smarty->fetch ('topheader.tpl');
    }


    function showFooter ($error, $confirm, $sendNews, $jsErrorOn=false, $chatOn=false) {
      $this->smarty->assign ('error', $error);
      $this->smarty->assign ('confirm', $confirm);
      $this->smarty->assign ('sendNews', $sendNews);
      $this->smarty->assign ('jsErrorOn', $jsErrorOn);
      $this->smarty->assign ('chatOn', $chatOn);
      $this->smarty->assign ('albumName', $this->albumName);
      $this->smarty->assign ('userLanguage', $this->language);
      $this->smarty->assign ('path', $this->headerPath);
      $this->smarty->assign ('skinPath', $this->skin->path);
      $this->smarty->assign ('picPath', PICS_PATH);
      if ($this->menuOptions) $this->smarty->assign ('session', '&session=active');

      return $this->smarty->fetch ('footer.tpl');
    }

  }
?>
