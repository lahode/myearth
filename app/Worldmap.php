<?php
  class Worldmap {
    var $smarty;
    var $routeManager;
    var $imageSizeX;
    var $imageSizeY;
    var $dbQuery;
    var $distance;
    var $coordinate;
    var $firstcoordinate;


    function __construct ($smart, $routeManager) {
      $this->smarty = $smart;
      $this->routeManager = $routeManager;
      $this->imageSizeX = 460;
      $this->imageSizeY = 234;
      $this->dbQuery = new DatabaseQuery (BD_HOST, BD_USER, BD_PSW, BD_NAME);
      $this->distance = 0;
      $this->coordinate = null;
      $this->firstcoordinate = null;
    }

    
    function HeaderWorldJS ($zoom, $largImage, $hautImage, $posImgX, $posImgY, $sizeDest, $arrayDest) {
      if ($arrayDest) $arrayDest = "new PhpArray2Js('".$arrayDest."')"; else $arrayDest="null";
      return "<link rel=\"stylesheet\" type=\"text/css\" href=\"../lib/css/worldmap.css\">\n".
             "<script language=\"javascript\" src=\"../lib/js/dw_scrollObj.js\" type=text/javascript></script>\n".
             "<script language=\"javascript\" src=\"../lib/js/dw_hoverscroll.js\" type=text/javascript></script>\n".
             "<script language=\"javascript\" src=\"../lib/js/world.js\" type=text/javascript></script>\n".
             "<script language=\"javascript\" src=\"../lib/js/phpTab2js.js\"></script>\n".
             "<script language=\"javaScript\">\n".
             "<!--\n".
             "  initWorldVariable (".$zoom.", ".$largImage.", ".$hautImage.", '".$posImgX."', '".$posImgY."', ".$sizeDest.", ".$arrayDest.");\n".
             "//-->\n".
             "</script>\n";
    }


    function getAllDestinations ($categories) {
      include_once('../lib/DatabaseQuery.php');
      $destinations = array();
      $firstCountryCoordinate = null;

      // Récupère les coordonnées de chaque destination
      if (count ($categories) > 0) {
        $this->dbQuery->connect();
        $i=0;
        foreach ($categories as $keyCat => $category) {
          $result = $this->dbQuery->getCityDetails ($category->country, $category->arrivalCity);
          $result2 = $this->dbQuery->getCityDetails ($category->country, $category->departureCity);
          if ($result) {
            if ($i > 0 && $destinations[$i-1]['longitude'] > 85 && $result->longitude < -50) {
              $lat = ($destinations[$i-1]['lattitude'] + $result->lattitude) / 2;
              $destinations[$i]['lattitude'] = $lat;
              $destinations[$i]['longitude'] = 178+abs($lat/8);
              $destinations[$i]['print'] = 'false';
              $destinations[$i]['arrivalOn'] = $category->startingDate;
              $i++;
              $destinations[$i]['lattitude'] = $lat;
              $destinations[$i]['longitude'] = -178+($lat/8);
              $destinations[$i]['print'] = 'none';
              $destinations[$i]['arrivalOn'] = $category->startingDate;
              $i++;
            } elseif ($i > 0 && $destinations[$i-1]['longitude'] < -50 && $result->longitude > 85) {
              $lat = ($destinations[$i-1]['lattitude'] + $result->lattitude) / 2;
              $destinations[$i]['lattitude'] = $lat;
              $destinations[$i]['longitude'] = -178+($lat/8);
              $destinations[$i]['print'] = 'false';
              $destinations[$i]['arrivalOn'] = $category->startingDate;
              $i++;
              $destinations[$i]['lattitude'] = $lat;
              $destinations[$i]['longitude'] = 178+abs($lat/8);
              $destinations[$i]['print'] = 'none';
              $destinations[$i]['arrivalOn'] = $category->startingDate;
              $i++;
            }
            $destinations[$i]['lattitude'] = $result->lattitude;
            $destinations[$i]['longitude'] = $result->longitude;
            $destinations[$i]['print'] = 'false';
            $destinations[$i]['arrivalOn'] = $category->startingDate;
            if ($destinations[$i]['arrivalOn'] < time()) {
              if ($this->coordinate) $this->distance += calculDistance ($this->coordinate, array ('x' => $destinations[$i]['lattitude'], 'y' => $destinations[$i]['longitude']));
              $this->coordinate['x'] = $destinations[$i]['lattitude'];
              $this->coordinate['y'] = $destinations[$i]['longitude'];
            }
            $i++;
          }
          $readDests = $this->routeManager->getDestinations ($category->id);
          $readDests = sortObjBy ($readDests, 'startingDate', SORT_ASC);
          if ($readDests) {
            if ($this->coordinate && $readDests[0]->startingDate < time()) $firstCountryCoordinate = $this->coordinate;
            foreach ($readDests as $keyDest => $destination) {
              $country = $this->routeManager->getCategoryBy ('id', $destination->category)->country;
              if ($destination->city <> '') {
                $result = $this->dbQuery->getCityDetails ($country, $destination->city);
                if ($result) {
                  $destinations[$i]['lattitude'] = $result->lattitude;
                  $destinations[$i]['longitude'] = $result->longitude;
                  $destinations[$i]['arrivalBy'] = $destination->arrivalBy;
                  $destinations[$i]['print'] = 'true';
                  $destinations[$i]['name'] = $this->dbQuery->getCityName($country, $destination->city);
                  if ($destination->cityName <> '') $destinations[$i]['name'] = $destination->cityName;
                  $destinations[$i]['arrivalOn'] = $destination->startingDate;
                  if ($destinations[$i]['arrivalOn'] < time()) {
                    if ($this->coordinate) $this->distance += calculDistance ($this->coordinate, array ('x' => $destinations[$i]['lattitude'], 'y' => $destinations[$i]['longitude']));
                    $this->coordinate['x'] = $destinations[$i]['lattitude'];
                    $this->coordinate['y'] = $destinations[$i]['longitude'];
                  }  
                  $i++;
                }
              }
            }
            if ($firstCountryCoordinate && $this->coordinate) {
              $this->distance += calculDistance ($this->coordinate, $firstCountryCoordinate);
              $this->coordinate = $firstCountryCoordinate;
              $firstCountryCoordinate = null;
            }
          }
          if ($result2) {
            $destinations[$i]['lattitude'] = $result2->lattitude;
            $destinations[$i]['longitude'] = $result2->longitude;
            $destinations[$i]['print'] = 'false';
            $destinations[$i]['arrivalOn'] = $category->startingDate;
            $i++;
          }
        }
        $this->dbQuery->disconnect();
        $totDest = count($destinations)-1;
      }
      return $destinations;
    }


    function getDeparture ($first) {
      include_once('../lib/DatabaseQuery.php');
      $startDest = array();
      $eachDest = array();

      // Récupère les coordonnées de la ville de départ
      $departure = $this->routeManager->getDeparture();
      if ($departure) {
        $this->dbQuery->connect();
        $result = $this->dbQuery->getCityDetails ($departure->country, $departure->city);
        if ($result) {
          $startDest['lattitude'] = $result->lattitude;
          $startDest['longitude'] = $result->longitude;
          if ($first) {
            if ($departure->startingDate < time()) {
              $this->coordinate['x'] = $result->lattitude;
              $this->coordinate['y'] = $result->longitude;
              $this->firstcoordinate = $this->coordinate;
            }
          } else {
            if ($this->coordinate && $this->firstcoordinate && $departure->endingDate < time()) $this->distance += calculDistance ($this->coordinate, $this->firstcoordinate);
          }
        }
        if (count ($startDest) > 0) {
          $eachDest = calculPosition ($startDest['lattitude'], $startDest['longitude'], $this->imageSizeX, $this->imageSizeY);
          $eachDest['color'] = '#FF2222';
          $eachDest['print'] = 'true';
          if ($first) $eachDest['arrivalOn'] = $departure->startingDate;
          else $eachDest['arrivalOn'] = $departure->endingDate;
          $eachDest['name'] = $this->dbQuery->getCityName($departure->country, $departure->city);
          if ($departure->cityName <> '') $eachDest['name'] = $departure->cityName;
        }
        $this->dbQuery->disconnect();
      }
      if ($first || $departure->endingDate < time()) return $eachDest;
    }


    function show ($categories, $path='', $showHeaders) {
      include_once('../lib/declare.php');
      include_once('../lib/functions.php');
      $pics_path = PICS_PATH.'world/';
      $eachDest = array();
      $eachDest[0] = $this->getDeparture (true);
      $destinations = $this->getAllDestinations($categories);
      $line = array();
      $i=0;
      $lastKeyDest=0;
      $zoom = 10;

      // Calcul des lignes et points de destinations
      if (count ($destinations) > 0) {
        foreach ($destinations as $keyDest => $dest) {
          $eachDest[$keyDest+1] = calculPosition ($dest['lattitude'], $dest['longitude'], $this->imageSizeX, $this->imageSizeY);
//          if ($keyDest+1 == count ($destinations)-1 && $this->getDeparture (false)) $eachDest[$keyDest+1] = $this->getDeparture (false);
          if ($dest['arrivalOn'] > mktime (0, 0, 0, date('m'), date('d'), date ('Y'))) {
            $eachDest[$keyDest+1]['color'] = '#BBBBBB';
          } elseif (isset ($dest['arrivalBy'])) {
            switch ($dest['arrivalBy']) {
              case 1  : {$eachDest[$keyDest+1]['color'] = '#FF2222';break;};
              case 2  : {$eachDest[$keyDest+1]['color'] = '#6677FF';break;};
              case 5  : {$eachDest[$keyDest+1]['color'] = '#FFFF55';break;};
              default : {$eachDest[$keyDest+1]['color'] = '#55FF55';break;};
            }
          } else {
            $eachDest[$keyDest+1]['color'] = '#FF2222';
          }
          $eachDest[$keyDest+1]['print'] = $dest['print'];
          if (isset ($dest['name'])) {
            $eachDest[$keyDest+1]['name'] = $dest['name'];
          }
          if ($i>0 && $eachDest[$keyDest]['print']<>'none') {
            $line[$i]['x1'] = $eachDest[$lastKeyDest]['x']*5;
            $line[$i]['y1'] = $eachDest[$lastKeyDest]['y']*5;
            $line[$i]['x2'] = $eachDest[$keyDest]['x']*5;
            $line[$i]['y2'] = $eachDest[$keyDest]['y']*5;
            $line[$i]['color'] = $eachDest[$keyDest]['color'];
          }
          $lastKeyDest=$keyDest;
          $i++;
        }
        if ($this->getDeparture (false)) {
          $endDest = $this->getDeparture (false);
          $endDest['color'] = '#FF2222';
          $endDest['print'] = 'true';
          $line[$i]['x1'] = $eachDest[$lastKeyDest]['x']*5;
          $line[$i]['y1'] = $eachDest[$lastKeyDest]['y']*5;
          $line[$i]['x2'] = $endDest['x']*5;
          $line[$i]['y2'] = $endDest['y']*5;
          $line[$i]['color'] = $endDest['color'];
          $lastKeyDest++;
        }
      }

      // Traçage des lignes de trajet sur la carte
      if ($line && $path<>'') {
        createLines($line, $path.'world.jpg');
      } else {
        $path = $pics_path;
      }

      // Assignation de Smarty
      $this->smarty->assign ('destinations', $eachDest);
      $this->smarty->assign ('path', $path);
      $this->smarty->assign ('pics', $pics_path);
      $this->smarty->assign ('idCache', genID());
      $this->smarty->assign ('imgName', 'world.jpg');
      $this->smarty->assign ('displayScrollBar', true);
      if ($showHeaders)
        return array ($this->HeaderWorldJS ($zoom, $zoom * $this->imageSizeX, $zoom * $this->imageSizeY, '', '', 18, serialize ($eachDest)), $this->smarty->fetch('worldmap.tpl'));
      else
        return array ("initWorldVariable (".$zoom.", ".($zoom * $this->imageSizeX).", ".($zoom * $this->imageSizeY).", '', '', 18, new PhpArray2Js('".serialize ($eachDest)."'));", $this->smarty->fetch('worldmap.tpl'));
    }


    function showAllUsers ($destinations, $countries, $names, $showHeaders) {
      $path = PICS_PATH.'world/';
      $pics_path = PICS_PATH.'world/';
      $eachDest = null;
      $zoom = 10;

      $this->dbQuery->connect();
      if ($destinations) {
        foreach ($destinations as $keyDest => $destination) {
          if ($destination) {
            $result = $this->dbQuery->getCityDetails ($countries[$keyDest], $destination->city);
            $eachDest[$keyDest] = calculPosition ($result->lattitude, $result->longitude, $this->imageSizeX, $this->imageSizeY);
            $eachDest[$keyDest]['print'] = 'true';
            $eachDest[$keyDest]['link'] = $names[$keyDest]->link;
            if ($destination->category == -1) {
              $eachDest[$keyDest]['name'] = 'Départ de '.$names[$keyDest]->name.' de '.$this->dbQuery->getCityName($countries[$keyDest], $destination->city).' le '.showdate ($destination->startingDate);
            } elseif ($destination->category == -2) {
              $eachDest[$keyDest]['name'] = 'Retour de '.$names[$keyDest]->name.' à '.$this->dbQuery->getCityName($countries[$keyDest], $destination->city).' le '.showdate ($destination->startingDate);
            } else {
              if ($destination->cityName <> '') $arrivalName = $destination->cityName; else $arrivalName = $this->dbQuery->getCityName($countries[$keyDest], $destination->city);
              $eachDest[$keyDest]['name'] = 'Arrivée de '.$names[$keyDest]->name.' à '.$arrivalName.' le '.showdate ($destination->startingDate);
            }
          }
        }
      }
      $this->dbQuery->disconnect();

      // Assignation de Smarty
      $this->smarty->assign ('destinations', $eachDest);
      $this->smarty->assign ('path', $path);
      $this->smarty->assign ('pics', $pics_path);
      $this->smarty->assign ('idCache', genID());
      $this->smarty->assign ('imgName', 'world.jpg');
      $this->smarty->assign ('displayScrollBar', true);
      if ($showHeaders)
        return array ($this->HeaderWorldJS ($zoom, $zoom * $this->imageSizeX, $zoom * $this->imageSizeY, 'left', 'down', 48, serialize ($eachDest)), $this->smarty->fetch('worldmap.tpl'));
      else
        return array ("initWorldVariable (".$zoom.", ".($zoom * $this->imageSizeX).", ".($zoom * $this->imageSizeY).", 'left', 'down', 48, new PhpArray2Js('".serialize ($eachDest)."'));", $this->smarty->fetch('worldmap.tpl'));
    }


    function showMap ($pathName, $imgName, $showHeaders) {
      if(!list($width_orig, $height_orig,$ext) = getimagesize($pathName.$imgName)) {
        return false;
      }
      $zoomX = $width_orig / $this->imageSizeX;
      $zoomY = $width_orig / $this->imageSizeX;
      if ($zoomX < $zoomY) $zoom = $zoomX; else $zoom = $zoomY;

      // Assignation de Smarty
      $this->smarty->assign ('path', $pathName);
      $this->smarty->assign ('pics', PICS_PATH.'world/');
      $this->smarty->assign ('idCache', genID());
      $this->smarty->assign ('imgName', $imgName);
      $this->smarty->assign ('displayScrollBar', true);
      if ($showHeaders)
        return array ($this->HeaderWorldJS ($zoom, $width_orig, $height_orig, '', '', 18, null), $this->smarty->fetch('worldmap.tpl'));
      else
        return array ("initWorldVariable (".$zoom.", ".$width_orig.", ".$height_orig.", '', '', 18, null);", $this->smarty->fetch('worldmap.tpl'));
    }


    function showPoint ($position, $zoom, $showHeaders) {
      $path = PICS_PATH.'world/';
      $pics_path = PICS_PATH.'world/';
      $this->smarty->assign ('position', $position);
      $this->smarty->assign ('path', $path);
      $this->smarty->assign ('pics', $pics_path);
      $this->smarty->assign ('idCache', genID());
      $this->smarty->assign ('imgName', 'world.jpg');
      if ($showHeaders)
        return array ($this->HeaderWorldJS (10 / $zoom, 10 * $this->imageSizeX, 10 * $this->imageSizeY, '', '', 18, null), $this->smarty->fetch('worldmap.tpl'));
      else
        return array ("initWorldVariable (".(10 / $zoom).", ".(10 * $this->imageSizeX).", ".(10 * $this->imageSizeY).", '', '', 18, null);", $this->smarty->fetch('worldmap.tpl'));
    }


    function showDestinations ($category, $decal, $position, $zoom, $showHeaders) {
      $destinations = $this->getAllDestinations(array(0 => $category));
      $eachDest = array();

      // Calcul des lignes et points de destinations
      if (count ($destinations) > 0) {
        foreach ($destinations as $keyDest => $dest) {
          $eachDest[$keyDest] = calculPosition ($dest['lattitude'], $dest['longitude'], $this->imageSizeX * $zoom, $this->imageSizeY * $zoom);
          if ($dest['arrivalOn'] > mktime (0, 0, 0, date('m'), date('d'), date ('Y'))) {
            $eachDest[$keyDest]['color'] = '#777777';
          } elseif (isset ($dest['arrivalBy'])) {
            switch ($dest['arrivalBy']) {
              case 1  : {$eachDest[$keyDest]['color'] = '#FF2222';break;};
              case 2  : {$eachDest[$keyDest]['color'] = '#6677FF';break;};
              case 5  : {$eachDest[$keyDest]['color'] = '#FFFF55';break;};
              default : {$eachDest[$keyDest]['color'] = '#55FF55';break;};
            }
          } else {
            $eachDest[$keyDest]['color'] = '#FF2222';
          }
          $eachDest[$keyDest]['print'] = $dest['print'];
        }
      }

      // Assignation de Smarty
      $this->smarty->assign ('number', $decal-1);
      $this->smarty->assign ('destinations', $eachDest);
      $this->smarty->assign ('idCache', genID());
      $this->smarty->assign ('path', PICS_PATH.'world/');
      $this->smarty->assign ('pics', PICS_PATH.'world/');
      $this->smarty->assign ('imgName', 'world.jpg');
      if ($showHeaders)
        return array ($this->HeaderWorldJS (10 / $zoom, 10 * $this->imageSizeX, 10 * $this->imageSizeY, '', '', 15, serialize ($eachDest)), $this->smarty->fetch('worldmap.tpl'));
      else
        return array ("initWorldVariable (".(10 / $zoom).", ".(10 * $this->imageSizeX).", ".(10 * $this->imageSizeY).", '', '', 15, new PhpArray2Js('".serialize ($eachDest)."'));", $this->smarty->fetch('worldmap.tpl'));
    }
    
  }


?>
