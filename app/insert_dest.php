<?php
  include_once('../lib/init.php');
  include_once('../lib/Availability.php');


  // Initialisation des données
  $pageName = "insert_dest";
  $nbDays = 0;
  $addImage = "";
  $deleteImage = "";
  $editImage = "";
  $rotateImage = "";
  $moveLeftImage = "";
  $moveRightImage = "";
  $picUpdate = "";
  $country = "";
  $region = "";
  $city = "";
  $cityName = "";
  $startingDate = "";
  $text = "";
  $arrivalBy = "";
  $country_name = "";
  $category = "";
  $tableNbDays = "";
  $dest = null;
  $map = null;
  $access = "public";
  $availability = null;
  $destinat = null;
  $uploadedMap = false;
  $images = array();
  $tableRegion = array();
  $tableCity = array();
  $showmap = $property->showMap;
  $mapDest = new Media ('map', $msgHandler, $fileManager, $albumManager, $quota_path, $mapImgPath, true);
  $imagesDest = new Media ('images', $msgHandler, $fileManager, $albumManager, $quota_path, $webImgPath, true);
  $availability_obj = new Availability ($routeManager);


  // Si l'administrateur de l'album ne s'est pas authentifié, renvoie l'utilisateur à la page d'accueil
  if (!$admin || !isset ($_GET['idCat']) || $_GET['idCat'] == '' || !isset ($_GET['idDest']) || $_GET['idDest'] == '') {header("Location:".PAGE_DESTINATION);exit();}


  // Si le bouton "Annuler" a été pressé, renvoie à la page d'accueil
  if (isset ($_POST['cancel']) && $_POST['cancel'] == 'true') {header("Location:".PAGE_DESTINATION);exit();}


  // Initialisation des données renvoyées par le formulaire
  if (!isset($_POST['init_'.$pageName])) {
    $fileManager->init();
    $destinations = $routeManager->getDestinations ($_GET['idCat']);
    if ($destinations) {
      $dest = $routeManager->getDestinationBy ('id', $_GET['idDest']);
      if ($dest) {
        $id = $dest->id;
        $category = $dest->category;
        $region = $dest->region;
        $city = $dest->city;
        $cityName = $dest->cityName;
        $startingDate = $dest->startingDate;
        $nbDays = $dest->nbDays;
        $arrivalBy = $dest->arrivalBy;
        $text = $dest->text;
        $showmap = $dest->showmap;
        $access = $dest->access;
        if ($dest->map<>'') $map = $fileManager->addImage ('map', $mapImgPath, $dest->map, '');
        if ($dest->images && count ($dest->images) > 0) {
          foreach ($dest->images as $itemImg) {
            $fileManager->addImage ('images', $webImgPath, $itemImg->name, $itemImg->description);
          }
          $images = $fileManager->getTempFiles('images');
        }
      } else {
        $id = $_GET['idDest'];
        $category = $_GET['idCat'];
      }
    } else {
      $id = $_GET['idDest'];
      $category = $_GET['idCat'];
    }
    $country = $routeManager->getCategoryBy ('id', $category)->country;
  } else {
    $id = $_POST['id'];
    $category = $_POST['category'];
    $country = $_POST['country'];
    $region = $_POST['region'];
    if (isset ($_POST['showmap'])) $showmap = 1; else $showmap = 0;
    if (isset ($_POST['city'])) $city = $_POST['city'];
    $cityName = addslash($_POST['cityName']);
    $startingDate = unfdate($_POST['startingDate']);
    $nbDays = $_POST['nbDays'];
    $arrivalBy = $_POST['arrivalBy'];
    $text = addslash($_POST['text']);
    $addImage = $_POST['addImage'];
    $deleteImage = $_POST['deleteImage'];
    $editImage = $_POST['editImage'];
    $rotateImage = $_POST['rotateImage'];
    $moveLeftImage = $_POST['moveLeftImage'];
    $moveRightImage = $_POST['moveRightImage'];
    $result_map = $mapDest->addMap('', true);
    if ($result_map[0]) $map = $result_map[0];
    if ($result_map[1]<>'') $error = $result_map[1];
    $images = $fileManager->getTempFiles('images');
  }


  // Si le bouton "Save" a été pressé, sauvegarde les informations de la destination éditée
  if (isset ($_POST['save']) && $_POST['save']=='true') {
    if ($id < 1) $error = $msgHandler->getError ('invalidID');
    if ($country=='0') $error = $msgHandler->getError ('selectCountry');
    if ($region=='0') $error = $msgHandler->getError ('selectRegion');
    if ($city=='0') $error = $msgHandler->getError ('selectCity');
    if (!$error) {
      $map = $fileManager->saveTempFiles ('map', $mapImgPath);
      if ($map && isset (current($map)->name)) $mapName = current($map)->name; else $mapName = "";
      $images = $fileManager->saveTempFiles ('images', $notebookImgPath);
      $destination =  new DestinationObj ($id, $category, $region, $city, $cityName, $startingDate+1, 
                                          $nbDays, $arrivalBy, $text, $mapName, $showmap, $images);
      $destination->setHide ($access);
      $routeManager->editDestination ($destination);
      if (isset ($_SESSION['countriesList'])) unset ($_SESSION['countriesList']);
      header("Location:".PAGE_DESTINATION."?sendNewsDest=".$id);
      exit();
    }
  }


  // Si le bouton "Delete Map" a été pressé, efface la carte
  if (isset ($_POST['deleteMap']) && $_POST['deleteMap']<>'') {
    $map = $fileManager->deleteTempFile ('map', $_POST['deleteMap']);
  }

// ***********************Gestion des images pour une destination***********************

  // Si le bouton "Save Image" a été pressé, enregistre l'image dans la bibliothèque
  if ($addImage=='save') {
    $resultAdd = $imagesDest->addImage($_POST['imageDescription']);
    if ($resultAdd[0] && count ($resultAdd[0]) > 0) $images = $resultAdd[0];
    if ($resultAdd[1]<>'') $error = $resultAdd[1];
    $addImage = 'false';
  }

  // Si le bouton "Delete Image" a éé pressé, efface l'image
  if ($deleteImage<>'') {
    $images = $fileManager->deleteTempFile ('images', $_POST['deleteImage']);
    $deleteImage = '';
    $addImage = 'false';
  }

  // Si le bouton "Edit Image" a été pressé, affiche l'option de modification
  if ($editImage<>'') {
    if (isset ($images[$editImage])) {$picUpdate = clone ($images[$editImage]);$picUpdate->temp = $editImage;}
    $addImage = 'modify';
    $editImage = '';
  }

  // Si le bouton "Add Image" a été pressé et vaut "update", modifie la description l'image
  if ($addImage=='update') {
    $imageToChange = $fileManager->getTempFiles('images', $_POST['imageFromAlbum']);
    if ($imageToChange) {
      $imageToChange->description = addslash($_POST['imageDescription']);
      $images = $fileManager->setTempFile('images', $_POST['imageFromAlbum'], $imageToChange);
    }
    $addImage = 'false';
  }

  // Si le bouton "Rotate Image" a été pressé, pivote l'image de 90°
  if ($rotateImage<>'') {
    $imageToChange = $fileManager->getTempFiles('images', $rotateImage);
    if ($imageToChange) {
      if ($imageToChange->temp == $fileManager->tempPath) $nameofImg = $rotateImage.$fileManager->tempExt; else $nameofImg = $imageToChange->name;
      $mediaManager->rotatePicture ($imageToChange->temp.$nameofImg, 90);
      $imageToChange->noCache = '?noCache='.genID();
      $images = $fileManager->setTempFile('images', $rotateImage, $imageToChange);
    }
    $rotateImage = '';
  }

  // Si le bouton "Move Left Image" a été pressé, remonte des images d'un cran
  if ($moveLeftImage<>'') {
    $images = $fileManager->moveLeft ('images', $moveLeftImage);
  }

  // Si le bouton "Move Left Image" a été pressé, descend l'ordre des images d'un cran
  if ($moveRightImage<>'') {
    $images = $fileManager->moveRight ('images', $moveRightImage);
  }

// ************** Initialisation des tableaux ******************************************

  $dbQuery->connect();

  // Si le bouton "search" a été pressé, recherche la ville recherchée
  if (isset ($_POST['search']) && $_POST['search']=='true' && $_POST['searchCity']<>'') {
    $cityFound = $dbQuery->searchCityFromCountry($_POST['searchCity'], $country);
    $region = $cityFound['region'];
    $city = $cityFound['city'];
  }

  // Sélection du pays
  $country_name = $dbQuery->getCountryName ($country);

  $tableRegion = $dbQuery->getRegionsFromCountry ($country);
  $tableCity = $dbQuery->getCitiesFromRegion ($country, $region);

  // Récupère la monnaie par défaut et celle du pays en cours
  $departure = $routeManager->getDeparture ();
  if ($departure) $maincurrency = $dbQuery->getCountryCurrency ($departure->country); else $maincurrency = '';
  $countrycurrency = $dbQuery->getCountryCurrency ($country);

  $dbQuery->Disconnect();

// **************************************************************************************

  // Insère en session toutes les dates occupées pour le calendrier
  if (!isset($_POST['init_'.$pageName])) {
    $availability = $availability_obj->getDestinationAvailability($category, $id, $routeManager->getCategoryBy ('id', $category)->allowsametime);
    $_SESSION['datesBooked'] = $availability;
    if ($startingDate=='' && $availability['firstDate']<$availability['lastDate']) $startingDate = substr($availability['firstDate'], 0, -3);
  }


  // Réassigne les valeurs transformée à l'objet pour affichage
  if ($startingDate) {
    $destinat = new DestinationObj ($id, $category, $region, $city, $cityName, $startingDate, $nbDays, $arrivalBy, $text, '', $showmap, $images);
  } else {
    header("Location:".PAGE_DESTINATION);exit();
  }


  // Formate la table du nombre de jours
  $tableNbDays = '<select name="nbDays">';
  for ($i=1;$i<=$availability_obj->getNbDays($startingDate, $_SESSION['datesBooked']);$i++) {
    if ($nbDays == $i) {
      $tableNbDays.= '<option selected value="'.$i.'">'.$i.'</option>';
    } else {
      $tableNbDays.= '<option value="'.$i.'">'.$i.'</option>';
    }
  }
  $tableNbDays.= '</select>';


  // Récupère les petites images à afficher
  if ($map &&isset (current($map)->name)) $mapName = current($map)->name; else $mapName = "";
  if ($mapName=='') $displayMap = ''; elseif (file_exists ($mapImgPath.$mapName)) $displayMap = $mapName; else {$displayMap = '';$uploadedMap=true;}


  // Assignation de Smarty
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuInsert'), true, 2));
  $smarty->assign ('displayMap', $mapDest->output('map', $displayMap, $uploadedMap));
  $smarty->assign ('imgDest', $imagesDest->output('web'));
  $smarty->assign ('dest', $destinat);

  if ($map) {
    $smarty->assign ('map', current ($map));
    $smarty->assign ('mapKey', key($map));
  }

  $smarty->assign ('country', $country);
  $smarty->assign ('category', $category);
  $smarty->assign ('country_name', $country_name);

  $smarty->assign ('tableRegion', $tableRegion);
  $smarty->assign ('tableCity', $tableCity);
  $smarty->assign ('tableNbDays', $tableNbDays);
  $smarty->assign ('tableArrivalBy', $msgHandler->getTable ('arrivalBy'));
  $smarty->assign ('webImgPath', $webImgPath);
  $smarty->assign ('pageName', $pageName);
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->assign ('addImage', $addImage);
  $smarty->assign ('dateFormat', $dateFormat);

  $smarty->assign ('picUpdate', $picUpdate);
  $smarty->display($pageName.'.tpl');

?>

