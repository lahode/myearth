<?php
  include_once('../lib/init.php');
  include_once('../lib/NewsManager.php');


  // Si le option des news n'est pas activée, renvoie à la page d'accueil
  if (!$menuOption->news) {header ("Location:".PAGE_ACCUEIL);exit();}


  // Initialisation des données
  $pageName = "news";
  $splitDisplay = new SplitDisplay (4, 9);


  // Gestion de l'affichage des résultats
  if (isset($_GET['next'])) {
    $splitDisplay->start=$_GET['next'];
    if (isset($_GET['page'])) {
      $splitDisplay->first=$_GET['page'];
    } else {
      $splitDisplay->first=$_GET['next']*$splitDisplay->length;
    }
  }


  // Gestion de l'affichage des résultats
  $splitDisplay->total = $newsManager->getNbMessages (true);
  $messages = $newsManager->getMessages ($splitDisplay->first, $splitDisplay->length, true, 'date');
  if ($messages && count ($messages) > 0) {
    foreach ($messages as $keyMessage => $message) {
      $message->text = cutWords ($message->text, 40);
      $newsTemp = convertToNews($smarty, $message, $language, $albumName, $newsImgPath);
      $message = $newsTemp[0];
    }
  }
  if ($splitDisplay->length > 0) {
    $splitDisplay->loop = ceil(($splitDisplay->total)/$splitDisplay->length);
    $splitDisplay->first = ceil(($splitDisplay->first)/$splitDisplay->length);
  }


  // Assignation de Smarty
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuNews'), $admin, 3));
  $smarty->assign ('splitDisplay', $splitDisplay);
  $smarty->assign ('messages', $messages);
  $smarty->display($pageName.'.tpl');

?>

