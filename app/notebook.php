<?php
  include_once('../lib/init.php');


  // Si le option du journal n'est pas activée, renvoie à la page d'accueil
  if (!$menuOption->notebook) {header ("Location:".PAGE_ACCUEIL);exit();}


  // Initialisation des données
  $pageName = "notebook";
  $titles = null;
  $message = null;
  $splitDisplay = new SplitDisplay (1, 1);
  if (!isset($_GET['next'])) $splitPage = new SplitDisplay (15, 1); else $splitPage = new SplitDisplay (1, 1);
  $maxlinesPerPage = 30;
  $access="";
  $newdate="";
  $subject="";
  $text="";
  $next="";
  $images=null;
  $imgNotebook = new Media ('notebook', $msgHandler, $fileManager, $albumManager, $quota_path, $notebookImgPath, true, 4);
  $fileManager->setOriginal ($origImgPath);
  if (isset ($_POST['actions']) && $_POST['actions']<>'') $action = $_POST['actions']; else $action = 'new';


  // Gestion de l'affichage des résultats
  if (isset($_GET['next'])) {
    $next = '&next='.$_GET['next'];
    $splitDisplay->start=$_GET['next'];
    $splitDisplay->first=$_GET['next']*$splitDisplay->length;
  }
  

  // Gestion de l'affichage des résultats
  if (isset($_GET['nextpage'])) {
    $splitPage->start=$_GET['nextpage'];
    $splitPage->first=$_GET['nextpage']*$splitPage->length;
  }


  // Initialisation des données renvoyées par le formulaire
  if (!isset($_POST['init_'.$pageName]) || (isset ($_POST['cancel']) && $_POST['cancel']=='true') || !$admin) {
    $fileManager->init();
    $action = 'new';
    $id = $notebookManager->getNewID();
  } else {
    $singleMessage = $notebookManager->getMessage ($_POST['id']);
    $action='edit';
    $access = 'public';
    $newdate = $_POST['newdate'];
    $id = $_POST['id'];
    $subject = addslash($_POST['subject']);
    $text = addslash($_POST['text']);
    $images = $fileManager->getTempFiles('notebook');
  }


  // Si le bouton "Changer d'accès" a été pressé, modifie l'accès du message
  if (isset ($_POST['changeAccess']) && $_POST['changeAccess'] <> '') {
    $singleMessage = $notebookManager->getMessage ($_POST['changeAccess']);
    if ($singleMessage->access == 'public') {
      $access = 'private';
    } else {
      $access = 'public';
    }
    $messageToSave = new MessageObj ($singleMessage->id, $singleMessage->date, $access, $singleMessage->subject, $singleMessage->text, $singleMessage->images);
    $notebookManager->editMessage ($messageToSave);
    $id = $notebookManager->getNewID();
    header ("Location:".PAGE_REDIRECTION."?redirection=notebook");
    exit();
  }


  // Si le bouton "Edition du message" a été pressé, modifie l'accès du message
  if (isset ($_POST['editMessage']) && $_POST['editMessage'] <> '') {
    $singleMessage = $notebookManager->getMessage ($_POST['editMessage']);
    $id = $singleMessage->id;
    $subject = $singleMessage->subject;
    $text = $singleMessage->text;
    $newdate = $singleMessage->date;
    $fileManager->init();
    if ($singleMessage->images && count ($singleMessage->images) > 0) {
      foreach ($singleMessage->images as $itemImg) {
        $fileManager->addImage ('notebook', $notebookImgPath, $itemImg->name, $itemImg->description);
      }
      $images = $fileManager->getTempFiles('notebook');
    }
  }


  // Si le bouton "Suppression du message" a été pressé, modifie l'accès du message
  if (isset ($_POST['deleteMessage']) && $_POST['deleteMessage'] <> '') {
    $notebookManager->deleteMessage ($_POST['deleteMessage']);
    $id = $notebookManager->getNewID();
    if (isset ($_SESSION['menuCount'])) unset ($_SESSION['menuCount']);
    header ("Location:".PAGE_REDIRECTION."?redirection=notebook");
    exit();
  }


  // Si le bouton "Save" a été pressé, sauvegarde les informations de la destination éditée
  if (isset ($_POST['save']) && $_POST['save']=='true') {
    if ($subject=='') $error = $msgHandler->getError ('insertSubject');
    if (isset($_POST['email']) && $_POST['email']<>'' && !is_email ($_POST['email'])) $error = $msgHandler->getError ('invalidEmail');
    if (!$error) {
      $images = $fileManager->saveTempFiles ('notebook', $notebookImgPath);
      $messageToSave = new MessageObj ($id, time(), $access, $subject, $text, $images);
      $notebookManager->editMessage ($messageToSave);
      $id = $notebookManager->getNewID();
      if (isset ($_SESSION['menuCount'])) unset ($_SESSION['menuCount']);
      header ("Location:".PAGE_REDIRECTION."?redirection=notebook");
      exit();
    }
  }


  // Si le bouton "Add Image" a été pressé, ajoute l'image à la liste
  if (isset ($_POST['addImage']) && $_POST['addImage']=='save') {
    $resultAdd = $imgNotebook->addImage($_POST['imageDescription']);
    if ($resultAdd[0] && count ($resultAdd[0]) > 0) $images = $resultAdd[0];
    if ($resultAdd[1]<>'') $error = $resultAdd[1];
  }


  // Si le bouton "Delete Image" a été pressé, efface l'image
  if (isset ($_POST['deleteImage']) && $_POST['deleteImage']<>'') {
    $images = $fileManager->deleteTempFile ('notebook', $_POST['deleteImage']);
  }


  // Gestion de l'affichage des résultats(1)
  $newMessage = new MessageObj ($id, $newdate, $access, $subject, $text, $images);
  $splitDisplay->total = $notebookManager->getNbMessages ($admin);
  $splitPage->total = $splitDisplay->total;
  if (!isset($_GET['next'])) {
    $titles = $notebookManager->getTitles($splitPage->first, $splitPage->length, $admin, 'date');
    $titles = sortObjBy ($titles, 'date', SORT_DESC);
  } else {
    $message = $notebookManager->getMessages ($splitDisplay->first, $splitDisplay->length, $admin, 'date');
  }


  // Gestion de l'affichage du texte
  $emotes = Emoticon::getEmotes (PICS_PATH.'/emoticon/');
  $currentMsg = null;
  if ($message) {
    $sizeToReduce = ceil (78 - ($albumManager->getFontstyle('persotext')->size*3));
    $currentMsg = current ($message);
    $inc = 0;
    $nb_mot = 0;
    $nb_lignes = 0;
    $txtToAdd = "";
    $msgOutput = array();
    $texttosplit=str_replace("\r\n", "\n", cutWords($currentMsg->text, 50));
    $texttosplit=str_replace("\r", "\n", $texttosplit);
    $explodedTT=explode("\n", $texttosplit);
    $emoteOn = false;
    foreach ($explodedTT as $explodedT) {
      for ($keyChar=0;$keyChar<strlen($explodedT);$keyChar++) {
        if ($nb_mot >= $sizeToReduce && $explodedT[$keyChar]==' ') {$nb_lignes++;$nb_mot=0;}
        if ($explodedT[$keyChar]=='[' && isset ($explodedT[$keyChar+1]) && $explodedT[$keyChar+1]==':') $emoteOn=true;
        if ($explodedT[$keyChar]==':' && isset ($explodedT[$keyChar+1]) && $explodedT[$keyChar+1]==']') $emoteOn=false;
        if (!$emoteOn && $nb_lignes > $maxlinesPerPage) {
          $nb_lignes = 0;
          $msgOutput[$inc] = Emoticon::setEmotes (PICS_PATH.'/emoticon/', $msgOutput[$inc]);
          $inc++;
        }
        $msgOutput[$inc].= $explodedT[$keyChar];
        $nb_mot++;
      }
      $msgOutput[$inc].="\n";
      $nb_lignes++;
      $nb_mot=0;
    }
    $msgOutput[$inc] = Emoticon::setEmotes (PICS_PATH.'/emoticon/', $msgOutput[$inc]);
    if ((ceil (count($currentMsg->images) / 2)*12) + $nb_lignes > $maxlinesPerPage) {$inc++;$msgOutput[$inc] = "";}
    $splitPage->total = $inc+1;
    $currentMsg->text = $msgOutput[$splitPage->first];
    if ($splitPage->first <> $inc) $currentMsg->images = null;
  }
  $nbEmote = count ($emotes);


  // Gestion de l'affichage des résultats(1)
  if ($splitDisplay->length > 0) {
    $splitDisplay->loop = ceil(($splitDisplay->total)/$splitDisplay->length);
    $splitDisplay->first = ceil(($splitDisplay->first)/$splitDisplay->length);
  }
  if ($splitPage->length > 0) {
    $splitPage->loop = ceil(($splitPage->total)/$splitPage->length);
    $splitPage->first = ceil(($splitPage->first)/$splitPage->length);
  }

  // Assignation de Smarty
  $smarty->assign ('imgPath', $notebookImgPath);
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get ('menuNotebook'), $admin, 3));
  $smarty->assign ('imgNotebook', $imgNotebook->output('web'));
  $smarty->assign ('emotes', $emotes);
  $smarty->assign ('nbEmote', $nbEmote);
  $smarty->assign ('textarea', 'document.form.text');
  $smarty->assign ('showEmote', $smarty->fetch ('emoticon.tpl'));
  $smarty->assign ('splitDisplay', $splitDisplay);
  $smarty->assign ('splitPage', $splitPage);
  $smarty->assign ('message', $currentMsg);
  $smarty->assign ('action', $action);
  $smarty->assign ('newMessage', $newMessage);
  $smarty->assign ('pageName', $pageName);
  $smarty->assign ('titles', $titles);
  $smarty->assign ('next', $next);
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->assign ('albumTitle', $albumTitle);
  $smarty->assign ('admin', $admin);
  $smarty->display($pageName.'.tpl');

?>

