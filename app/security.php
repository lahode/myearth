<?php
  include_once('../lib/init.php');


  // Si l'administrateur de l'album ne s'est pas authentifié, renvoie l'utilisateur à la page d'accueil
  if (!$admin) {header("Location:".PAGE_ACCUEIL);exit();}


  // Si le bouton "Annuler" a été pressé, renvoie à la page d'accueil
  if (isset ($_POST['cancel']) && $_POST['cancel'] == 'true') {header("Location:".PAGE_ACCUEIL);exit();}


  // Récupère le mot de passe de l'invité
  $pageName = "security";
  $dbQuery->connect();
  $guestPassword = $dbQuery->getGuestPsw ($albumName);
  $oldGuestPassword = $guestPassword;
  $dbQuery->disconnect();


  // Récupère les données envoyées par le formulaire
  if (isset ($_POST['presentPassword'])) $presentPassword = $_POST['presentPassword']; else $presentPassword = "";
  if (isset ($_POST['newPassword'])) $newPassword = $_POST['newPassword']; else $newPassword = "";
  if (isset ($_POST['confirmPassword'])) $confirmPassword = $_POST['confirmPassword']; else $confirmPassword = "";
  if (isset ($_POST['guestPassword'])) $guestPassword = $_POST['guestPassword'];


  // Si le bouton "Save" a été pressé, sauvegarde les modifications
  if (isset ($_POST['save']) && $_POST['save']=='true') {
    $dbQuery->connect();

    // Modifie le mot de passe de l'invité
    if ($presentPassword <> '' || $newPassword <> '' || $confirmPassword <> '') {
      if ($presentPassword <> '' && $newPassword <> '' && $confirmPassword <> '') {
        if ($dbQuery->checkAdminPsw($_SESSION['login'], $presentPassword, $albumName)) {
          if ($newPassword == $confirmPassword) {
            if (checkPassword ($newPassword)) {
              if ($dbQuery->changeAdminPsw ($newPassword, $albumName)) {
                header ("Location:".PAGE_ACCUEIL."?confirm=*adminPasswordChanged");
                exit();
              } else {
                $error = $msgHandler->getError ('dbError');
              }
            } else {
              $error = $msgHandler->getError ('passwordValidation');
            }
          } else {
            $error = $msgHandler->getError ('passwordConfirmation');
          }
        } else {
          $error = $msgHandler->getError ('passwordCheck');
        }
      } else {
        $error = $msgHandler->getError ('passwordField');
      }
    }

    // Modifie le mot de passe de l'invité
    if ($oldGuestPassword <> $guestPassword && $error=='') {
      if ($dbQuery->changeGuestPsw ($guestPassword, $albumName)) {
        header ("Location:".PAGE_ACCUEIL."?confirm=*guestPasswordChanged");
        exit();
      } else {
        $error = $msgHandler->getError ('dbError');
      }
    }
    $dbQuery->disconnect();
    
    if (!$error) header ("Location:".PAGE_ACCUEIL);
  }


  // Assignation de Smarty
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get('menuFileManager'), $admin, 1));
  $smarty->assign ('guestPassword', $guestPassword);
  $smarty->assign ('albumTitle', $albumTitle);
  $smarty->assign ('admin', $admin);
  $smarty->display($pageName.'.tpl');

?>

