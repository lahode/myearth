<?php
  include_once ('Worldmap.php');
  include_once('../lib/init.php');


  // Initialisation des données
  $pageName = "destination";
  $lastDestID = 0;
  $lastCatID = 0;
  $country = array();
  $cat = array();
  $catImg = array();
  $countryImage = array();
  $destinations = array();
  $dest = array();
  $worldmap = new Worldmap ($smarty, $routeManager);


  // Si le bouton "Effacer catégorie" a été pressé, supprime la catégorie ainsi que les destinations liées
  if (isset ($_POST['deleteCat']) && $_POST['deleteCat'] <> '') {
    $routeManager->deleteCategory ($_POST['deleteCat']);
    if (isset ($_SESSION['countriesList'])) unset ($_SESSION['countriesList']);
    header("Location:".PAGE_DESTINATION);
    exit();
  }


  // Si le bouton "Effacer destination" a été pressé, supprime la destination
  if (isset ($_POST['deleteDest']) && $_POST['deleteDest'] <> '') {
    $routeManager->deleteDestinations ($_POST['deleteCat'], $_POST['deleteDest']);
    if (isset ($_SESSION['countriesList'])) unset ($_SESSION['countriesList']);
    header("Location:".PAGE_DESTINATION);
    exit();
  }


  // Récupère toutes les information de l'album
  if (count ($categories) > 0) {
    foreach ($categories as $keyCat => $category) {
      $destinations[$keyCat] = $routeManager->getDestinations ($category->id);
      $destinations[$keyCat] = sortObjBy ($destinations[$keyCat], 'startingDate', SORT_ASC);
    }
  }


  // Si le bouton "Changer d'accès" a été pressé, modifie l'accès du message
  if (isset ($_POST['changeAccessCat']) && $_POST['changeAccessCat'] <> '' &&
      isset ($_POST['changeAccess']) && $_POST['changeAccess'] <> '') {
    $destToChange = $routeManager->getDestinations ($_POST['changeAccessCat']);
    if ($destToChange[$_POST['changeAccess']]->access == 'public') {
      $destToChange[$_POST['changeAccess']]->setHide('private');
    } else {
      $destToChange[$_POST['changeAccess']]->setHide('public');
    }
    $routeManager->editDestination ($destToChange[$_POST['changeAccess']]);
    header ("Location:".PAGE_REDIRECTION."?redirection=destination");
    exit();
  }


  // Remplace les informations des pays et destination  de l'album par les noms de la base de données
  $dbQuery->connect();
  if (count ($categories) > 0) {
    $incDest=1;
    foreach ($categories as $keyCat => $category) {
      $categoryChanged = clone $category;
      if ($categoryChanged->summary->general) $categoryChanged->summary->general = shorten ($categoryChanged->summary->general, 150);
      $categoryChanged->arrivalCity = $dbQuery->getCountryName($category->country);
      if ($categoryChanged->arrivalCity) {
        $country[$category->id] = $category->country;
      }
      $category->order = $keyCat;
      $cat[$category->id] = $categoryChanged;
      $cat[$category->id]->access = 'private';
      if ($category->id > $lastCatID) $lastCatID = $category->id;
    }
  }
  if (count ($destinations) > 0) {
    foreach ($destinations as $keyDestination => $destination) {
      if (count ($destination) > 0) {
        foreach ($destination as $keyDest => $itemDest) {
          if (count ($country) > 0) {
            $itemDest->region = $dbQuery->getRegionName($country[$itemDest->category], $itemDest->region);
            $itemDest->city = $dbQuery->getCityName($country[$itemDest->category], $itemDest->city);
            if ($itemDest->cityName <> '') $itemDest->city = $itemDest->cityName;
          }
          $itemDest->text = "";
          if ($itemDest->images) {
            if (!isset($catImg[$itemDest->category])) $incCatImg = 0;
            foreach ($itemDest->images as $img) {
              $catImg[$itemDest->category][$incCatImg] = $img->name;
              $incCatImg++;
            }
            $itemDest->images = null;
          }
          $itemDest->order = $incDest;
          if ($itemDest->access=='public') $cat[$itemDest->category]->access = 'public';
          $dest[$itemDest->category][$keyDest] = $itemDest;
          if ($itemDest->id > $lastDestID) $lastDestID = $itemDest->id;
          $incDest++;
        }
      }
    }
  }
  $dbQuery->disconnect();
  $lastCatID++;
  $lastDestID++;


  // Sélection aléatoire de la photo du pays
  if (count ($catImg) > 0) {
    foreach ($catImg as $keycatIm => $catIm) {
      if (sizeof ($catIm) > 3) {
        $x1 = rand(0, sizeof ($catIm)-1);
        do {
          $x2 = rand(0, sizeof ($catIm)-1);
        } while ($x2 == $x1);
        do {
          $x3 = rand(0, sizeof ($catIm)-1);
        } while ($x3 == $x1 || $x3 == $x2);
        $countryImage[$keycatIm][0] = $catIm[$x1];
        $countryImage[$keycatIm][1] = $catIm[$x2];
        $countryImage[$keycatIm][2] = $catIm[$x3];
      } elseif (sizeof ($catIm) == 3) {
        $countryImage[$keycatIm][0] = $catIm[0];
        $countryImage[$keycatIm][1] = $catIm[1];
        $countryImage[$keycatIm][2] = $catIm[2];
      } elseif (sizeof ($catIm) == 2) {
        $countryImage[$keycatIm][0] = $catIm[0];
        $countryImage[$keycatIm][1] = $catIm[1];
      } elseif (sizeof ($catIm) == 1) {
        $countryImage[$keycatIm][0] = $catIm[0];
      }
    }
  }


  // Affichage de la carte
  $showWorld[0] = '';
  $showWorld[1] = '';
  if ($property->showMap) {
    $showWorld = $worldmap->show($categories, USERS_PATH.$albumName.'/', true);
  }


  // Récupère l'option d'envoie du message pour le newsgroup
  if (isset ($_GET['sendNewsDest']) && $_GET['sendNewsDest']<>'') {
    if ($menuOption->news) $sendNews = $_GET['sendNewsDest'];
  }


  // Assignation de Smarty
  $smarty->assign ('showFooter', $header->showFooter ($error, $confirm, $sendNews, true, $menuOption->chat));
  $smarty->assign ('showTopHeader', $header->showTopHeader ());
  $smarty->assign ('showHeader', $header->showHeader ($albumTitle, $msgHandler->get('menuDestination'), $admin, 2));
  $smarty->assign ('albumTitle', $albumTitle);
  $smarty->assign ('distance', fPrice($worldmap->distance));
  $smarty->assign ('headerWorldmap', $showWorld[0]);
  $smarty->assign ('worldmap', $showWorld[1]);
  $smarty->assign ('categories', $cat);
  $smarty->assign ('destinations', $dest);
  $smarty->assign ('lastDestID', $lastDestID);
  $smarty->assign ('lastCatID', $lastCatID);
  $smarty->assign ('countryImage', $countryImage);
  $smarty->assign ('webImgPath', $webImgPath);
  $smarty->assign ('picPath', PICS_PATH);
  $smarty->assign ('admin', $admin);
  $smarty->display($pageName.'.tpl');

?>

